//
//  ValidationsClass.h
//  PayMobile
//
//  Created by Integra Micromicro on 03/06/14.
//  Copyright (c) 2014 Integra. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <math.h>
#import <UIKit/UIKit.h>

/**
 * This class used to handle Validations of entire application.
 *
 * @author Integra
 *
 */
@interface ValidationsClass : NSObject
/*
 * This method used to Convert Hex code to color.
 */
+ (NSArray *)colorWithHexString:(NSString *)stringToConvert;
+ (UIColor *) colorWithHexStringValue: (NSString *) hexString;
+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length;

+ (NSString *)validateInputs:(NSArray *)validationModelList;
+(NSString *)getDateStringFromDate :(NSDate *)date withFormate:(NSString *)fromate;
+(NSDate *)getDateFromDateString :(NSString *)dateString withFormate:(NSString *)fromate;

+(NSString *)convertTorequiredFormatte :(NSString *)dateString withFormate:(NSString *)fromate;

/*
 * This method used to Encryption process.
 */
+(NSString *)encryptString : (NSString *)value;
+(NSString *)encryptStringForRequestID:(NSString *)value;
+(BOOL)isMPINField : (NSString *)mPIN;
+ (BOOL)isMPINReEnterField : (NSString *)mPIN;
+ (BOOL)isActivationCodeField : (NSString *)activationCode;

+(NSDate *)getFormattedCurrentDate;
+ (NSDate *)getFormattedDate : (NSString *)date;
/*
 * This method used to generate OTP for date format.
 */
+(NSDate *)getDateFormateForOTPGeneration : (NSString *)value;
+(NSTimeInterval)getMilliSecondDateFormateForOTPGeneration : (NSString *)value;



@end
