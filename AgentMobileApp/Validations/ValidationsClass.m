//
//  ValidationsClass.m
//  PayMobile
//
//  Created by Integra Micromicro on 03/06/14.
//  Copyright (c) 2014 Integra. All rights reserved.
//

#import "ValidationsClass.h"
#import "Constants.h"
//susmit
#import "NSData+CommonCrypto.h"
//susmit
#import "Localization.h"
#import "GenerateOTP.h"
#import "Reachability.h"
static NSString *mPin = nil;

@implementation ValidationsClass

/*
 * Constatnts for validation inputs.
 */
#define EMAIL_REGEX "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
#define PHONE_REGEX "\\d{3}-\\d{7}"
#define ALPHANUMRIC_WITH_SPACE_REGEX "^[a-zA-Z0-9_ ]*$"
#define ALPHANUMRIC_WITHOUT_SPACE_REGEX "^[a-zA-Z0-9]+$"
#define ALPHABET_REGEX "^[a-zA-Z]+$"
#define ALPHABET_WITH_SPACE_REGEX "^[a-zA-Z\\-\\s]+$"
#define NUMBER_REGEX "^[0-9]+$"
#define NUMBER_WITH_SPACE_REGEX "^[0-9\\-\\s]+$"
#define NUMBER_WITH_DECIMAL_REGEX "^[0-9\\-\\.]+$"
#define DEFAULT_PHONENUMBER_MAXLENGTH 12
#define DEFAULT_PHONENUMBER_MINLENGTH 8

#define DEFAULT_ACCOUNTNUMBER_MINLENGTH 5
#define DEFAULT_ACCOUNTNUMBER_MAXLENGTH 20

#define CONVERT_MILLISECOND(second) (second * 1000.0)
#define CONVERT_SECOND(millisecond) (millisecond / 1000.0)

#pragma mark - Colors from Hexa value.
/*
 * This method is used to get the color from hiven hex code.
 */
+ (NSArray *)colorWithHexString:(NSString *)stringToConvert
{
    NSString *noHashString = [stringToConvert stringByReplacingOccurrencesOfString:@"#" withString:@""];    NSScanner *scanner = [NSScanner scannerWithString:noHashString];
    [scanner setCharactersToBeSkipped:[NSCharacterSet symbolCharacterSet]];
    unsigned hex;
    if (![scanner scanHexInt:&hex]) return nil;
    int red = (hex >> 16) & 0xFF;
    int green = (hex >> 8) & 0xFF;
    int blue = (hex) & 0xFF;
    
    NSMutableArray *colorsArray = [[NSMutableArray alloc] init];
    [colorsArray addObject:[NSString stringWithFormat:@"%f",red/255.0f]];
    [colorsArray addObject:[NSString stringWithFormat:@"%f",green/255.0f]];
    [colorsArray addObject:[NSString stringWithFormat:@"%f",blue/255.0f]];
    
    return colorsArray;
}


+ (UIColor *) colorWithHexStringValue: (NSString *) hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}


#pragma mark - Validations method.
/*
 * This method is used to get the validation inputs from view.
 */
+ (NSString *)validateInputs:(NSArray *)globalValidationsArray
{
    NSString *validationMessage = @"";
    
    int length = (int)[globalValidationsArray count];
    BOOL breakInnerLoopIf = NO;
    BOOL breakInnerLoopElse = NO;
    for (int i = 0; i < length; i++)
    {
        NSString *validationType = [[globalValidationsArray objectAtIndex:i] objectForKey:@"validation_type"];
        NSArray *localValidationsArray = [validationType componentsSeparatedByString:@"$"];

        if ([[validationType componentsSeparatedByString:@"|"] count] >= 2) {
            if ([[[validationType componentsSeparatedByString:@"|"] objectAtIndex:0] isEqualToString:@"1000$1008"]) {
                mPin = [[globalValidationsArray objectAtIndex:i] objectForKey:@"value"];
            }
        }
        for (int j=0; j<[localValidationsArray count]; j++)
        {
            NSArray *innerValidationsArray;
            NSRange range = [[localValidationsArray objectAtIndex:j] rangeOfString:@"|"];
            if (range.location != NSNotFound)
            {
                innerValidationsArray = [[localValidationsArray objectAtIndex:j] componentsSeparatedByString:@"|"];
                NSMutableDictionary *tempDict = [globalValidationsArray objectAtIndex:i];
                [tempDict setValue:[innerValidationsArray objectAtIndex:1] forKey:@"validation_params"];
                int type = (int)[[innerValidationsArray objectAtIndex:0] integerValue];
                NSString *message = [self isValid:type andValueDictionary:tempDict];
                
                if (message)
                {
                    validationMessage = message;
                    breakInnerLoopIf = YES;
                }
            }
            else
            {
                NSMutableDictionary *tempDict = [globalValidationsArray objectAtIndex:i];
                int type = (int)[[localValidationsArray objectAtIndex:j] integerValue];
                NSString *message = [self isValid:type andValueDictionary:tempDict];
                if (message)
                {
                    validationMessage = message;
                    breakInnerLoopElse = YES;
                }
            }
            if (breakInnerLoopElse)
                break;
            if (breakInnerLoopIf)
                break;
            
        }
        if (breakInnerLoopElse)
            break;
        if (breakInnerLoopIf)
            break;
    }
    
    return validationMessage;
}

#pragma mark - isValid validations
/*
 * This method is used to get the validation inputs validates based on validation type.
 */

+ (NSString *) isValid:(int)type andValueDictionary:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    
//    Mobile number – Empty check, No special characters allowed.
//    Amount – Empty check, No negative values, Decimal not allowed, Zero not allowed.
//    MPIN – No special characters, only numbers allowed.
//    First name and last name - Alphabets with space only.
//    Agent Id - No special characters, only numbers allowed.
    
    switch (type)
    {
        case 1000: // Empty check
        {
            message = [self isEmptyField:valueDictionary];
        }
            break;
        case 1001: // Alphanumeric with space check
        {
            message = [self isAlphaNumericWithSpace:valueDictionary];
        }
        break;
        case 1002:// Alphabet without space check
        {
            message = [self isAlphabetWithoutSpace:valueDictionary];
        }
            break;
        case 1003:// Alphabet with space check
        {
            message = [self isAlphabetWithSpace:valueDictionary];
        }
            break;
        case 1004: // Numbers only check
        {
            message = [self isNumberOnly:valueDictionary];
        }
        break;
        case 1005: // Alphanumeric without space check
        {
            message = [self isAlphaNumericWithoutSpace:valueDictionary];
        }
            break;
        case 1006: // Valid first & last name check
        {
            message = [self isValidFirstLastName:valueDictionary];
        }
            break;
        case 1007: // Valid nick name check
        {
            message = [self isValidNickName:valueDictionary];
        }
            break;
        case 1008: // isValidMPIN
        {
            message = [self isValidMPIN:valueDictionary];
        }
            break;
        case 1009: // isValidActivationCode
        {
            message = [self isValidActivationCode:valueDictionary];
        }
            break;
        case 1010:// isEmailID
        {
            message = [self isValidEmailID:valueDictionary];
        }
        break;
        case 1011: // isMPINMatched
        {
            message = [self isMPINMatched:valueDictionary];
        }
            break;
        case 1012: // isValidMessage
        {
            message = [self isValidMessage:valueDictionary];
        }
            break;
        case 1013: // isValidMobileNumber
        {
            message = [self isValidPhoneNumber:valueDictionary];
        }
            break;
        case 1014: //isValidAddPayee
        {
            message = [self isValidAddPayee:valueDictionary];
        }
            break;
        case 1015: // isValidAccountNumber
        {
            message = [self isValidAccountNumber:valueDictionary];
        }
            break;
        case 1016: // isValidMerchantId
        {
            message = [self isValidMerchantId:valueDictionary];
        }
            break;
        case 1017:// isValidAgentId
        {
            message = [self isValidAgentId:valueDictionary];
        }
            break;
        case 1018: // fromDate
        {
            message = [self fromDate:valueDictionary];
        }
            break;
        case 1019: // toDate
        {
            message = [self toDate:valueDictionary];
        }
            break;
        case 1020: //isValidOTP
        {
            message = [self isValidOTP:valueDictionary];
        }
            break;
        case 1021: // isValidAmount
        {
            message = [self isValidAmount:valueDictionary];
        }
            break;
        case 1022: //isImageSizeExceeded
        {
            message = [self isImageSizeExceeded:valueDictionary];
        }
            break;
        case 1023: //isMaxMinLength
        {
            message = [self isCheckMinMaxLength:valueDictionary];
        }
            break;
            
        default:
            break;
    }
    
    return message;
}

#pragma mark - Empty validations
/*
 * This method is used to get the empty field.
 */
+ (NSString *)isEmptyField:(NSDictionary *)value
{
    NSString *message = nil;
    if ([[value objectForKey:@"value"] length] == 0)
    {
         NSString *replaceString = [NSString stringWithFormat:@"{%@}",[Localization languageSelectedStringForKey:NSLocalizedString(@"fieldname", nil)]];
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10001", nil)];
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[Localization languageSelectedStringForKey:[value objectForKey:@"labelName"]]];
    }
    return message;
}

#pragma mark-  AlphaNumeric With Space validations
/*
 * This method is used to get the AlphaNumeric With Space validations.
 */
+ (NSString *)isAlphaNumericWithSpace:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    
    if (![self generalValidationWithRegularExpression:@ALPHANUMRIC_WITH_SPACE_REGEX withText:[valueDictionary objectForKey:@"value"]])
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    return message;
}

#pragma mark - Alphabet validations
/*
 * This method is used to get the Alphabet validations.
 */
+ (NSString *)isAlphabetWithoutSpace:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    if (![self generalValidationWithRegularExpression:@ALPHABET_REGEX withText:[valueDictionary objectForKey:@"value"]])
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    return message;
}

#pragma mark -  Alphabet With Space validations
/*
 * This method is used to get the Alphabet With Space validations.
 */
+ (NSString *)isAlphabetWithSpace:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    if (![self generalValidationWithRegularExpression:@ALPHABET_WITH_SPACE_REGEX withText:[valueDictionary objectForKey:@"value"]])
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    return message;
}

#pragma mark - Number only validations
/*
 * This method is used to get the Number only validations.
 */
+ (NSString *)isNumberOnly:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    
    if (![self generalValidationWithRegularExpression:@NUMBER_REGEX withText:[valueDictionary objectForKey:@"value"]])
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    return message;
}

#pragma mark - AlphaNumeric With out Space validations
/*
 * This method is used to get the AlphaNumeric With out Space validations.
 */
+ (NSString *)isAlphaNumericWithoutSpace:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    
    if (![self generalValidationWithRegularExpression:@ALPHANUMRIC_WITHOUT_SPACE_REGEX withText:[valueDictionary objectForKey:@"value"]])
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    return message;
}

#pragma mark - First & Last Name validations
/*
 * This method is used to get the First & Last Name validations.
 */
+ (NSString *)isValidFirstLastName:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    
    NSArray *specificValidationArray;
    if ([valueDictionary objectForKey:@"validation_params"])
        specificValidationArray = [[valueDictionary objectForKey:@"validation_params"] componentsSeparatedByString:@","];
    
    int inputType;
    if ([specificValidationArray count] > 0 && [specificValidationArray objectAtIndex:0])
        inputType = (int)[[specificValidationArray objectAtIndex:0] integerValue];
    else
        inputType = (int)[NSLocalizedStringFromTableInBundle(@"application_default_validation_parameter1_input_type",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
    
    BOOL isValid = [self isValidTextTypeWithText:[valueDictionary objectForKey:@"value"] ofType:inputType];
    
//    NSString *spaceAllowed = nil;
//    if ([specificValidationArray count] > 1 && [specificValidationArray objectAtIndex:1])
//        spaceAllowed = [specificValidationArray objectAtIndex:1];
//    else
//        spaceAllowed = NSLocalizedStringFromTableInBundle(@"application_default_validation_parameter2_space_allowed",@"GeneralSettings",[NSBundle mainBundle], nil);
//    
//    BOOL isSpaceAllowed = [self isSpaceAllowed:[valueDictionary objectForKey:@"value"] ofType:(int)[spaceAllowed integerValue]];
    
    int maxLemgth = 0;
    if ([specificValidationArray count] > 1 && [specificValidationArray objectAtIndex:1])
        maxLemgth = (int)[[specificValidationArray objectAtIndex:1] integerValue];
    else
        maxLemgth = (int)[NSLocalizedStringFromTableInBundle(@"application_default_validation_parameter3_max_length",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
    
    if (maxLemgth < [[valueDictionary objectForKey:@"value"] length] || !isValid)
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }

    return message;
}

#pragma mark - Nick Name validations
/*
 * This method is used to get the Nick Name validations.
 */
+ (NSString *)isValidNickName:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    
    BOOL isValid = [self isValidTextTypeWithText:[valueDictionary objectForKey:@"value"] ofType:2];
    BOOL isSpaceAllowed = [self isSpaceAllowed:[valueDictionary objectForKey:@"value"] ofType:0];
    
    int maxLemgth = 0;
    if ([NSLocalizedStringFromTableInBundle(@"application_default_validation_parameter3_max_length",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue])
        maxLemgth = (int)[NSLocalizedStringFromTableInBundle(@"application_default_validation_parameter3_max_length",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
    
    if (maxLemgth != [[valueDictionary objectForKey:@"value"] length] || !isValid || !isSpaceAllowed)
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }

    return message;
}

#pragma mark - MPIN validations.
/*
 * This method is used to get the MPIN validations.
 */
+ (NSString *)isValidMPIN:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    
    NSArray *specificValidationArray;
    if ([valueDictionary objectForKey:@"validation_params"])
        specificValidationArray = [[valueDictionary objectForKey:@"validation_params"] componentsSeparatedByString:@","];

    int pinLength;
    if ([specificValidationArray count] > 0 && [specificValidationArray objectAtIndex:0])
        pinLength = (int)[[specificValidationArray objectAtIndex:0] integerValue];
    else
        pinLength = (int)[NSLocalizedStringFromTableInBundle(@"application_mpin_length",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
    
    int inputType;
    if ([specificValidationArray count] > 1 && [specificValidationArray objectAtIndex:1])
        inputType = (int)[[specificValidationArray objectAtIndex:1] integerValue];
    else
        inputType = (int)[NSLocalizedStringFromTableInBundle(@"application_default_validation_parameter1_input_type",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
    
    BOOL isValid = [self isValidTextTypeWithText:[valueDictionary objectForKey:@"value"] ofType:inputType];
  //  id strValue=[valueDictionary objectForKey:@"labelName"];
    NSLog(@"Specification Values are...%@",[valueDictionary description]);
    
     int minLengthofMpin=4;
    
    int sLength = (int)[[valueDictionary objectForKey:@"value"] length];
     NSLog(@"PIN LENGTH...%d,%d",pinLength,sLength);
//    ((pinLength == sLength && isValid) && (pinLength <= sLength && isValid))
    if((pinLength == sLength && isValid) || (minLengthofMpin== sLength && isValid)|| (minLengthofMpin >= sLength && isValid))  {
        
    }
    else if((minLengthofMpin >sLength) || (minLengthofMpin > pinLength) || (pinLength < sLength))
    {
        //                if ([strValue containsString:@"OTP"])
        //                {
        //                    message =@"Invalid OTP Format";
        //                }
        //                else
        //                {
        message =[Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10004", nil)];
        //                }
    }
    
    
    return message;
}

#pragma mark - Activation Code validations.
/*
 * This method is used to get the Activation Code validations.
 */
+ (NSString *)isValidActivationCode:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    
    NSArray *specificValidationArray;
    if ([valueDictionary objectForKey:@"validation_params"])
        specificValidationArray = [[valueDictionary objectForKey:@"validation_params"] componentsSeparatedByString:@","];

    int activationCodeLength;
    if ([specificValidationArray count] > 0 && [specificValidationArray objectAtIndex:0])
        activationCodeLength = (int)[[specificValidationArray objectAtIndex:0] integerValue];
    else
        activationCodeLength = (int)[NSLocalizedStringFromTableInBundle(@"application_mpin_length",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
    
    int inputType;
    if ([specificValidationArray count] > 1 && [specificValidationArray objectAtIndex:1])
        inputType = (int)[[specificValidationArray objectAtIndex:1] integerValue];
    else
        inputType = (int)[NSLocalizedStringFromTableInBundle(@"application_default_validation_parameter1_input_type",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
    
    BOOL isValid = [self isValidTextTypeWithText:[valueDictionary objectForKey:@"value"] ofType:inputType];
    
    
    if (activationCodeLength != [[valueDictionary objectForKey:@"value"] length] || !isValid)
        message=[Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10005", nil)];
    
        
        //message = @"Invalid Temporary code format";
    
    return message;
}

#pragma mark - EmailID validations.
/*
 * This method is used to get the Activation Code validations.
 */
+ (NSString *)isValidEmailID:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    
    if (![self generalValidationWithRegularExpression:@EMAIL_REGEX withText:[valueDictionary objectForKey:@"value"]])
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = [NSString stringWithFormat:@"{%@}",[valueDictionary objectForKey:@"labelName"]];
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    return message;
}

#pragma mark-MPIN Matching validations
/*
 * This method is used to get the MPIN Matching validations.
 */
+ (NSString *)isMPINMatched:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    NSString *reEnterValue = [valueDictionary objectForKey:@"value"];
        
    if ([mPin compare:reEnterValue] == NSOrderedSame) {
    }
    else {
        message =[Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10006", nil)];
    }
    return message;
}

#pragma mark - Message validations.
/*
 * This method is used to get the Message validations.
 */
+ (NSString *)isValidMessage:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    if (![self generalValidationWithRegularExpression:@ALPHANUMRIC_WITHOUT_SPACE_REGEX withText:[valueDictionary objectForKey:@"value"]])
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    
    return message;
}

#pragma mark - Phone validations.
/*
 * This method is used to get the Phone number validations.
 */
+ (NSString *)isValidPhoneNumber:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    int min = 0;
    int max = 0;
    NSString *prefix;
    
    NSArray *specificValidationArray;
    if ([valueDictionary objectForKey:@"validation_params"])
        specificValidationArray = [[valueDictionary objectForKey:@"validation_params"] componentsSeparatedByString:@","];
    
    if ([specificValidationArray count] > 0 && [specificValidationArray objectAtIndex:0])
        min = (int)[[specificValidationArray objectAtIndex:0] integerValue];
    else
        min = (int)[NSLocalizedStringFromTableInBundle(@"application_phonenumber_minlength",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
    
    if ([specificValidationArray count] > 1 && [specificValidationArray objectAtIndex:1])
        max = (int)[[specificValidationArray objectAtIndex:1] integerValue];
    else
        max = (int)[NSLocalizedStringFromTableInBundle(@"application_phonenumber_maxlength",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];

    BOOL isMinimum = [[valueDictionary objectForKey:@"value"] length] >= min;
    BOOL isMaximum = [[valueDictionary objectForKey:@"value"] length] <= max;
    
    BOOL isPrefixMatched = YES;
    BOOL isFirstLetterMatched = NO;
//    if ([[valueDictionary objectForKey:@"value"] length]>2)
//    {
//        if ([prefix isEqualToString:@""])
//        {
//        }
//        else
//        {
//            isPrefixMatched = [[[valueDictionary objectForKey:@"value"] substringToIndex:2] isEqualToString:prefix];
//        }
//
//        isFirstLetterMatched = [[[valueDictionary objectForKey:@"value"] substringToIndex:1] isEqualToString:@"0"];
//    }
    BOOL isDecimal = NO;
    NSRange textRange =[[valueDictionary objectForKey:@"value"] rangeOfString:@"."];
    if(textRange.location != NSNotFound)
        isDecimal = YES;
    
    BOOL isNumbericCharsAllowed = [self isValidTextTypeWithText:[valueDictionary objectForKey:@"value"] ofType:3];

    if (isMinimum && isMaximum && isPrefixMatched && !isFirstLetterMatched && !isDecimal && isNumbericCharsAllowed)
    {
    }
    else
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    return message;
}

#pragma mark - AddPayee validations
/*
 * This method is used to get the AddPayee validations.
 */
+ (NSString *)isValidAddPayee:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    return message;
}

#pragma mark - AccountNumber validations
/*
 * This method is used to get the AccountNumber validations.
 */
+ (NSString *)isValidAccountNumber:(NSDictionary *)valueDictionary
{
      // Minimum length 5 Maximum length 20  Only number.
    NSString *value = [valueDictionary objectForKey:@"value"];
    int minValue;
    int maxValue;
    NSString *errorMessage;
    NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
    NSString *replaceString = @"{fieldname}";
    
    if ([[[[[valueDictionary objectForKey:@"validation_type"] componentsSeparatedByString:@"|"] objectAtIndex:1] componentsSeparatedByString:@","] count] > 1)
    {
        minValue = [[[[[[valueDictionary objectForKey:@"validation_type"] componentsSeparatedByString:@"|"] objectAtIndex:1] componentsSeparatedByString:@","] objectAtIndex:0] intValue];
        maxValue = [[[[[[valueDictionary objectForKey:@"validation_type"] componentsSeparatedByString:@"|"] objectAtIndex:1] componentsSeparatedByString:@","] objectAtIndex:1] intValue];

        if (value.length < minValue) {
            errorMessage = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
        }
        else
        if (value.length > maxValue) {
            errorMessage = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
        }
        else
        if([[valueDictionary objectForKey:@"value"] rangeOfString:@"."].location != NSNotFound)
        {
            errorMessage = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
        }
        else
            if([[valueDictionary objectForKey:@"value"] rangeOfString:@"."].location != NSNotFound)
            {
                errorMessage = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
            }
        else if (![self generalValidationWithRegularExpression:@NUMBER_REGEX withText:[valueDictionary objectForKey:@"value"]])
        {
            errorMessage = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
        }
    }
    else
    if (value.length < DEFAULT_ACCOUNTNUMBER_MINLENGTH)
    {
        errorMessage = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    else
    if (value.length > DEFAULT_ACCOUNTNUMBER_MAXLENGTH) {
        errorMessage = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    else
    if([[valueDictionary objectForKey:@"value"] rangeOfString:@"."].location != NSNotFound)
    {
        errorMessage = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    else if (![self generalValidationWithRegularExpression:@NUMBER_REGEX withText:[valueDictionary objectForKey:@"value"]])
    {
        errorMessage = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    return errorMessage;
}

#pragma mark - MerchantId validations
/*
 * This method is used to get the MerchantId validations.
 */
+ (NSString *)isValidMerchantId:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    
    NSArray *specificValidationArray;
    if ([valueDictionary objectForKey:@"validation_params"])
        specificValidationArray = [[valueDictionary objectForKey:@"validation_params"] componentsSeparatedByString:@","];
    
    
    int maxLemgth = 0;
    if ([specificValidationArray count] > 0 && [specificValidationArray objectAtIndex:0])
        maxLemgth = (int)[[specificValidationArray objectAtIndex:0] integerValue];
    else
        maxLemgth = (int)[NSLocalizedStringFromTableInBundle(@"application_default_validation_parameter3_max_length",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
    
    NSString *type = nil;
    if ([specificValidationArray count] > 1 && [specificValidationArray objectAtIndex:1])
        type = [specificValidationArray objectAtIndex:1];
    else
        type = NSLocalizedStringFromTableInBundle(@"application_default_validation_parameter1_input_type",@"GeneralSettings",[NSBundle mainBundle], nil);
    
    BOOL isValidType = [self isValidTextTypeWithText:[valueDictionary objectForKey:@"value"] ofType:(int)[type integerValue]];
    
    if (maxLemgth != [[valueDictionary objectForKey:@"value"] length] || !isValidType)
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    return message;
}


#pragma mark - AgentId validations.
/*
 * This method is used to get the AgentId validations.
 */
+ (NSString *)isValidAgentId:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    
    NSArray *specificValidationArray;
    if ([valueDictionary objectForKey:@"validation_params"])
        specificValidationArray = [[valueDictionary objectForKey:@"validation_params"] componentsSeparatedByString:@","];
    
    int maxLemgth = 0;
    if ([specificValidationArray count] > 0 && [specificValidationArray objectAtIndex:0])
        maxLemgth = (int)[[specificValidationArray objectAtIndex:0] integerValue];
    else
        maxLemgth = (int)[NSLocalizedStringFromTableInBundle(@"application_default_validation_parameter3_max_length",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
    
    NSString *type = nil;
    if ([specificValidationArray count] > 1 && [specificValidationArray objectAtIndex:1])
        type = [specificValidationArray objectAtIndex:1];
    else
        type = NSLocalizedStringFromTableInBundle(@"application_default_validation_parameter1_input_type",@"GeneralSettings",[NSBundle mainBundle], nil);
    
    BOOL isValidType = [self isValidTextTypeWithText:[valueDictionary objectForKey:@"value"] ofType:(int)[type integerValue]];
    
    if (maxLemgth < [[valueDictionary objectForKey:@"value"] length] || !isValidType)
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    return message;
}

#pragma mark - From Date validations.
/*
 * This method is used to get the date validations.
 */
+ (NSString *)fromDate:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    
    NSString *dateStr =  [valueDictionary objectForKey:@"value"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:SETDATEFORMATDATEMONTHYEAR];
    NSDate *userDate = [dateFormatter dateFromString:dateStr];
    NSDate *currentdate = [NSDate date];
    if ([userDate compare:currentdate] == NSOrderedDescending)
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    else if ([userDate compare:currentdate] == NSOrderedAscending)
    {
    }
    else if ([currentdate compare:userDate] == NSOrderedSame)
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
      }
    
    return message;
}

#pragma mark - To Date validations.
/*
 * This method is used to get the date validations.
 */
+ (NSString *)toDate:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    
    NSString *dateStr =  [valueDictionary objectForKey:@"value"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DATE_TIME_FORMAT_WITH_TIMEZONE];
    NSDate *userDate = [dateFormatter dateFromString:dateStr];
    NSDate *currentdate = [NSDate date];
    
    if ([currentdate compare:userDate] == NSOrderedDescending)
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    else if ([currentdate compare:userDate] == NSOrderedAscending)
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    else
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString =@"{fieldname}";
        //;[Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
//        @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    return message;
}

#pragma mark - OTP validations.
/*
 * This method is used to get the OTP validations.
 */
+ (NSString *)isValidOTP:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    
    NSArray *specificValidationArray;
    if ([valueDictionary objectForKey:@"validation_params"])
        specificValidationArray = [[valueDictionary objectForKey:@"validation_params"] componentsSeparatedByString:@","];
    
    NSLog(@"specificValidationArray: %@",specificValidationArray);
    
    int inputType;
    if ([specificValidationArray count] > 0 && [specificValidationArray objectAtIndex:0])
        inputType = (int)[[specificValidationArray objectAtIndex:0] integerValue];
    else
        inputType = (int)[NSLocalizedStringFromTableInBundle(@"application_default_validation_parameter1_input_type",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
    
    BOOL isValid = [self isValidTextTypeWithText:[valueDictionary objectForKey:@"value"] ofType:inputType];
    
    int maxLemgth = 0;
    if ([specificValidationArray count] > 1 && [specificValidationArray objectAtIndex:1])
        maxLemgth = (int)[[specificValidationArray objectAtIndex:1] integerValue];
    else
        maxLemgth = (int)[NSLocalizedStringFromTableInBundle(@"application_default_validation_parameter3_max_length",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
    
    int decimalSupprt = 0;
    if ([specificValidationArray count] > 2 && [specificValidationArray objectAtIndex:2])
        decimalSupprt = (int)[[specificValidationArray objectAtIndex:2] integerValue];
    else
        decimalSupprt = (int)[NSLocalizedStringFromTableInBundle(@"application_default_validation_parameter4_decimal_support",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
    
    BOOL isDecimalSupprt = [self isDecimalsAllowed:[valueDictionary objectForKey:@"value"] ofType:decimalSupprt];
    
    if (maxLemgth != [[valueDictionary objectForKey:@"value"] length] || !isValid || !isDecimalSupprt)
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    
    return message;
}

#pragma mark - Amount validation.
/*
 * This method is used to get the Amount validation.
 */
+ (NSString *)isValidAmount:(NSDictionary *)valueDictionary
{
    NSString *value = [valueDictionary objectForKey:@"value"];
    NSString *totalCharactersAllowed;
    NSString *decimalAllowed;
    if ([[[valueDictionary objectForKey:@"validation_type"] componentsSeparatedByString:@"|"] count] > 1) {
        if ([[[[[valueDictionary objectForKey:@"validation_type"] componentsSeparatedByString:@"|"] objectAtIndex:1] componentsSeparatedByString:@","] count] > 1) {
            totalCharactersAllowed = [[[[[valueDictionary objectForKey:@"validation_type"] componentsSeparatedByString:@"|"] objectAtIndex:1] componentsSeparatedByString:@","] objectAtIndex:1];
            decimalAllowed = [[[[[valueDictionary objectForKey:@"validation_type"] componentsSeparatedByString:@"|"] objectAtIndex:1] componentsSeparatedByString:@","] objectAtIndex:0];
        }
        else
        {
            totalCharactersAllowed = [[[[[valueDictionary objectForKey:@"validation_type"] componentsSeparatedByString:@"|"] objectAtIndex:1] componentsSeparatedByString:@","] objectAtIndex:0];
            decimalAllowed = NSLocalizedStringFromTableInBundle(@"application_default_validation_parameter4_decimal_support",@"GeneralSettings",[NSBundle mainBundle], nil);
        }
    }

    BOOL isDecimalSupprt = [self isDecimalsAllowed:value ofType:(int)[decimalAllowed integerValue]];
    NSString *errorMessage;
    
    if (!isDecimalSupprt || ([value integerValue] <= 0 || [value floatValue] <= 0)||[value isEqualToString:@"."] || [value isEqualToString:@"0"] || [value length] > [totalCharactersAllowed integerValue])
    {
    NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
    NSString *replaceString = @"{fieldname}";
    errorMessage = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    return errorMessage;
}

+ (BOOL) isNumeric:(NSString*)string
{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return [scan scanInt:&val] && [scan isAtEnd];
}

+ (BOOL) isFloat:(NSString*)string
{
    NSScanner* scan = [NSScanner scannerWithString:string];
    float val;
    return [scan scanFloat:&val] && [scan isAtEnd];
}

#pragma mark - Image Size Exceeded validation.
/*
 * This method is used to get the Image Size Exceeded validation.
 */
+ (NSString *)isImageSizeExceeded:(NSDictionary *)valueDictionary
{
    NSString *message = nil;
    
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] initWithDictionary:valueDictionary];
    
    UIImage *image = [UIImage imageNamed:@"send_green.png"];
    NSData *imageData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(image,0.5)];
    
    int imageSize = (int)imageData.length;
    [tempDict setObject:[NSString stringWithFormat:@"%d",imageSize] forKey:@"value"];
    return message;
}

#pragma mark - Phone number min,max length Validation.
/*
 * This method is used to get the Phone number min,max length validation.
 */
+ (NSString *)isCheckMinMaxLength:(NSDictionary *)valueDictionary
{
    NSString *value = [valueDictionary objectForKey:@"value"];
    int minValue;
    int maxValue;
    NSString *errorMessage;
    
    if ([[[[[valueDictionary objectForKey:@"validation_type"] componentsSeparatedByString:@"|"] objectAtIndex:1] componentsSeparatedByString:@","] count] > 1)
    {
        minValue = [[[[[[valueDictionary objectForKey:@"validation_type"] componentsSeparatedByString:@"|"] objectAtIndex:1] componentsSeparatedByString:@","] objectAtIndex:0] intValue];
        maxValue = [[[[[[valueDictionary objectForKey:@"validation_type"] componentsSeparatedByString:@"|"] objectAtIndex:1] componentsSeparatedByString:@","] objectAtIndex:1] intValue];
        
        if (!((value.length >= minValue) && (value.length <= maxValue))) {
            NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
            NSString *replaceString = @"{fieldname}";
            errorMessage = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
        }
    }
    else
    if (value.length < DEFAULT_PHONENUMBER_MINLENGTH)
    {
            NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
            NSString *replaceString = @"{fieldname}";
            errorMessage = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    else
    if (value.length > DEFAULT_PHONENUMBER_MAXLENGTH) {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10003", nil)];
        NSString *replaceString = @"{fieldname}";
        errorMessage = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:[valueDictionary objectForKey:@"labelName"]];
    }
    
    return errorMessage;
}

#pragma mark - Text validations.
/*
 * This method is used to get Validation inputs..
 */
+(BOOL)isValidTextTypeWithText:(NSString *)text ofType:(int)type
{
    BOOL isValid = NO;
    
    switch (type)
    {
        case 1:
        {
            isValid = [self generalValidationWithRegularExpression:@ALPHANUMRIC_WITH_SPACE_REGEX withText:text];
        }
            break;
        case 2:
        {
            isValid = [self generalValidationWithRegularExpression:@ALPHABET_REGEX withText:text];
        }
            break;
        case 3:
        {
            isValid = [self generalValidationWithRegularExpression:@NUMBER_REGEX withText:text];
        }
            break;
        case 4:
        {
            isValid = [self generalValidationWithRegularExpression:@ALPHABET_REGEX withText:text];
        }
            break;

        default:
            break;
    }
    
    return isValid;
}

#pragma mark - Space validation.
/*
 * This method is used to get Space validation.
 */
+(BOOL)isSpaceAllowed:(NSString *)text ofType:(int)type
{
    BOOL isValid = NO;
    
    switch (type)
    {
        case 0: //alphanumeric vth out space
        {
            isValid = [self generalValidationWithRegularExpression:@ALPHANUMRIC_WITHOUT_SPACE_REGEX withText:text];
        }
        break;
        case 1:// alphanumeric vth space
        {
            isValid = [self generalValidationWithRegularExpression:@ALPHANUMRIC_WITH_SPACE_REGEX withText:text];
        }
        break;
        default:
            break;
    }
    
    return isValid;
}

#pragma mark - Decimal validation.
/*
 * This method is used to get Decimal validation.
 */
+(BOOL)isDecimalsAllowed:(NSString *)text ofType:(int)type
{
    BOOL isValid = NO;
    
    switch (type)
    {
        case 1: // decimals not allowed
        {
            isValid = [self generalValidationWithRegularExpression:@NUMBER_REGEX withText:text];
        }
            break;
        case 0: // decimals allowed
        {
            isValid = [self generalValidationWithRegularExpression:@NUMBER_WITH_DECIMAL_REGEX withText:text];
        }
            break;
        default:
            break;
    }
    
    return isValid;
}

#pragma mark - Regular expression validations.
/*
 * This method is used to get Regularexpression validation.
 */
+(BOOL)generalValidationWithRegularExpression:(NSString *)regExpression withText:(NSString *)text
{
    BOOL isValid = NO;
    
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regExpression options:NSRegularExpressionCaseInsensitive error:&error];
    NSRange textRange = NSMakeRange(0, text.length);
    NSRange matchRange = [regex rangeOfFirstMatchInString:text options:NSMatchingReportProgress range:textRange];
    
    if (matchRange.location != NSNotFound)
        isValid = YES;
    return isValid;
}

+(NSString *)getDateStringFromDate :(NSDate *)date withFormate:(NSString *)fromate{
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:fromate];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

+(NSDate *)getDateFromDateString :(NSString *)dateString withFormate:(NSString *)fromate{
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:fromate];
    NSDate *date = [dateFormatter dateFromString:dateString];
    return date;
}

+(NSString *)convertTorequiredFormatte :(NSString *)dateString withFormate:(NSString *)fromate
{
    NSString *returnDate = nil;
    
    NSString *timeStr = dateString;
    timeStr = [timeStr stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    NSArray *tempArray;
    NSString *str;
    
    if ([timeStr componentsSeparatedByString:@"."])
    {
        tempArray = [timeStr componentsSeparatedByString:@"."];
        str = [tempArray objectAtIndex:0];
    }
    else
    {
        str = timeStr;
    }
    NSDate *date = [self getDateFromDateString:str withFormate:fromate];
    returnDate = [self getDateStringFromDate:date withFormate:fromate];
    return returnDate;
}

#pragma mark - Encryption Methods.
/*
 * This method is used to get Encryption Methods.
 */
+ (NSString *)encryptString : (NSString *)value
{

    NSString *encrptedString = nil;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [reachability  currentReachabilityStatus];
    NSString *mode=((netStatus==NotReachable)?SMS_MODE:GPRS_MODE);
    
    
    if ([NSLocalizedStringFromTableInBundle(@"application_mpin_encryption_is_required",@"GeneralSettings",[NSBundle mainBundle], nil)  caseInsensitiveCompare:YES_TEXT] == NSOrderedSame)
    {
        if (([mode caseInsensitiveCompare:SMS_MODE] == NSOrderedSame) && ([NSLocalizedStringFromTableInBundle(@"application_sms_encryption_type",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue] == 2))
        {
            NSString *key = [NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"application_sms_encription_key", @"GeneralSettings", [NSBundle mainBundle], nil)];            
            NSData *toencrypt = [value dataUsingEncoding:NSUTF8StringEncoding];
            NSData *keyData = [[key substringWithRange:NSMakeRange(0, 16)] dataUsingEncoding:NSUTF8StringEncoding];
            CCCryptorStatus status;
            
            NSData *encrypted = [toencrypt dataEncryptedUsingAlgorithm:kCCAlgorithmAES key:keyData options:kCCOptionPKCS7Padding error:&status];
            encrptedString = [encrypted  base64EncodedStringWithOptions:0];
            
            NSLog(@"Encrypted String : %@",encrptedString);
            return encrptedString;
        }
        else
        {
            NSTimeInterval lastServerResponseTime = [[NSUserDefaults standardUserDefaults] doubleForKey:SERVER_LAST_TRANSACTION_TIME];
            NSTimeInterval lastDeviceResponseTime = [[NSUserDefaults standardUserDefaults] doubleForKey:DEVICE_LAST_TRANSACTION_TIME];
            
            NSString *timestamp;
            
            
            if ((lastServerResponseTime == 0.0) || (lastDeviceResponseTime == 0.0))
            {
                timestamp=[[ValidationsClass getTimeStampConvesionFormatter] stringFromDate:[NSDate date]];
            }
            
            else
            {
                NSTimeInterval currentDeviceResponseTime=CONVERT_MILLISECOND([[ValidationsClass getFormattedCurrentDate] timeIntervalSince1970]);
                NSTimeInterval diff = currentDeviceResponseTime-lastDeviceResponseTime;
                NSDate *newDate = [[NSDate dateWithTimeIntervalSince1970:CONVERT_SECOND(lastServerResponseTime)] dateByAddingTimeInterval:CONVERT_SECOND(diff)];
                timestamp = [[ValidationsClass getTimeStampConvesionFormatter] stringFromDate:newDate];
            }

            NSString *key = [NSString stringWithFormat:@"%@%@",timestamp,NSLocalizedStringFromTableInBundle(@"application_encription_key", @"GeneralSettings", [NSBundle mainBundle], nil)];
            NSLog(@"Key : %@",key);
            
            NSData *toencrypt = [value dataUsingEncoding:NSUTF8StringEncoding];
            NSData *keyData = [[key substringWithRange:NSMakeRange(0, 16)] dataUsingEncoding:NSUTF8StringEncoding];
            CCCryptorStatus status;
            
            NSData *encrypted = [toencrypt dataEncryptedUsingAlgorithm:kCCAlgorithmAES key:keyData options:kCCOptionPKCS7Padding error:&status];
            encrptedString = [encrypted  base64EncodedStringWithOptions:0];
            NSLog(@"Encrypted String : %@",encrptedString);
            
            return encrptedString;
        }
    }
    else
    return value;
}

+(NSString *)encryptStringForRequestID:(NSString *)value
{
    NSString *encrptedString = nil;
    if([NSLocalizedStringFromTableInBundle(@"application_request_id_encryption_is_required",@"GeneralSettings",[NSBundle mainBundle], nil)  caseInsensitiveCompare:YES_TEXT] == NSOrderedSame)
    {
            NSString *key = [NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"application_request_id_encription_key", @"GeneralSettings", [NSBundle mainBundle], nil)];
            NSLog(@"Key : %@",key);
            
            NSData *toencrypt = [value dataUsingEncoding:NSUTF8StringEncoding];
            NSData *keyData = [[key substringWithRange:NSMakeRange(0, 16)] dataUsingEncoding:NSUTF8StringEncoding];
            CCCryptorStatus status;
            
            NSData *encrypted = [toencrypt dataEncryptedUsingAlgorithm:kCCAlgorithmAES key:keyData options:kCCOptionPKCS7Padding error:&status];
            encrptedString = [encrypted  base64EncodedStringWithOptions:0];
            NSLog(@"Encrypted String : %@",encrptedString);
            return encrptedString;
    }
    else
        return value;
}

#pragma mark - MPIN.
/*
 * This method is used to get MPIN validation.
 */
+ (BOOL)isMPINField : (NSString *)mPIN
{
    BOOL ismPin = NO;
    NSRange mPinRange = [mPIN rangeOfString:@"1008"]; //for MPIN checking
    if(mPinRange.location != NSNotFound)
    {
        ismPin = YES;
    }
    return ismPin;
}

#pragma mark - Activation code.
/*
 * This method is used to get Activation code validation.
 */
+ (BOOL)isActivationCodeField : (NSString *)activationCode
{
    BOOL isActivationCode = NO;
    NSRange activationCodeRange = [activationCode rangeOfString:@"1009"]; //for Activation Code checking
    if(activationCodeRange.location != NSNotFound)
    {
        isActivationCode = YES;
    }
    return isActivationCode;
}
#pragma mark - reenter MPIN.
/*
* This method is used to get Reenter MPIN validation.
*/
+ (BOOL)isMPINReEnterField : (NSString *)mPIN
{
    BOOL ismPin = NO;
    NSRange mPinRange = [mPIN rangeOfString:@"1011"]; //for MPIN checking
    if(mPinRange.location != NSNotFound)
    {
        ismPin = YES;
    }
    return ismPin;
}
#pragma mark - Date Formatter.
/*
 * This method is used to get Date formatter.
 */
+ (NSDate *)getFormattedCurrentDate
{
    NSDateFormatter *formatter=[ValidationsClass getDateConvesionFormatter];
    NSString *currentDate = [formatter stringFromDate:[NSDate date]];
    NSDate *formattedCurrentDate = [formatter dateFromString:currentDate];
    return formattedCurrentDate;
}

+ (NSDate *)getFormattedDate : (NSString *)serverDate
{
    NSDate *lastResponseTime = nil;
    NSDate *tempDate = [[ValidationsClass getDateComparisonFormatter] dateFromString:[[[serverDate stringByReplacingOccurrencesOfString:@"T" withString:@" "] componentsSeparatedByString:@"+"] objectAtIndex:0]];
    NSString *tempStrDate = [[ValidationsClass getDateConvesionFormatter] stringFromDate:tempDate];
    lastResponseTime = [[ValidationsClass getDateConvesionFormatter] dateFromString:tempStrDate];
    return lastResponseTime;
}

+(NSDate *)getDateFormateForOTPGeneration : (NSString *)value
{
    NSLog(@"value:%@",value);
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[NSLocale currentLocale]];
    NSString *timeZone = NSLocalizedStringFromTableInBundle(@"applicaiton_time_zone",@"GeneralSettings",[NSBundle mainBundle], nil);
    [dateFormat  setTimeZone:[NSTimeZone timeZoneWithAbbreviation:timeZone]];
    [dateFormat setDateFormat:SERVER_DATE_FORMAT];
    NSDate* dateTime = [dateFormat dateFromString:value];
    return dateTime;
}


+(NSTimeInterval)getMilliSecondDateFormateForOTPGeneration : (NSString *)value
{
    NSLog(@"value:%@",value);
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[NSLocale currentLocale]];
    NSString *timeZone = NSLocalizedStringFromTableInBundle(@"applicaiton_time_zone",@"GeneralSettings",[NSBundle mainBundle], nil);
    [dateFormat  setTimeZone:[NSTimeZone timeZoneWithAbbreviation:timeZone]];
    [dateFormat setDateFormat:SERVER_DATE_FORMAT];
    NSDate* dateTime = [dateFormat dateFromString:value];
    NSTimeInterval interval=CONVERT_MILLISECOND([dateTime timeIntervalSince1970]);
    NSLog(@"interval:%f",interval);
    return interval;
}

+ (NSDateFormatter *)getDateConvesionFormatter
{
    NSDateFormatter *dateConvesionFormatter = [[NSDateFormatter alloc]init];
    dateConvesionFormatter.locale = [NSLocale currentLocale];
    NSString *timeZone = NSLocalizedStringFromTableInBundle(@"applicaiton_time_zone",@"GeneralSettings",[NSBundle mainBundle], nil);
    [dateConvesionFormatter  setTimeZone:[NSTimeZone timeZoneWithAbbreviation:timeZone]];
    dateConvesionFormatter.dateFormat =SERVER_DATE_FORMAT;
    return dateConvesionFormatter;
}

+ (NSDateFormatter *)getTimeStampConvesionFormatter
{
    NSDateFormatter *dateConvesionFormatter = [[NSDateFormatter alloc]init];
    dateConvesionFormatter.locale = [NSLocale currentLocale];
    NSString *timeZone = NSLocalizedStringFromTableInBundle(@"applicaiton_time_zone",@"GeneralSettings",[NSBundle mainBundle], nil);
    [dateConvesionFormatter  setTimeZone:[NSTimeZone timeZoneWithAbbreviation:timeZone]];
    [dateConvesionFormatter setDateFormat:SETDATEFORMATDATEMONTHYEARWITHTIME];
    return dateConvesionFormatter;
}

+ (NSDateFormatter *)getDateComparisonFormatter
{
    NSDateFormatter *dateComparisonFormatter = [[NSDateFormatter alloc]init];
    dateComparisonFormatter.locale = [NSLocale currentLocale];
    dateComparisonFormatter.dateFormat =SETDATEFORMATYEARMONTHDATE;
    return dateComparisonFormatter;
}


@end
