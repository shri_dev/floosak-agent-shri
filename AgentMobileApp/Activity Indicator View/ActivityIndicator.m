//
//  ActivityIndicator.m
//  Consumer Client
//
//  Created by android on 6/9/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "ActivityIndicator.h"
#import "Constants.h"
#import "Localization.h"

@implementation ActivityIndicator

@synthesize activityView,loadingView,loadingLabel;

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        
        loadingView = [[UIView alloc] init];
        loadingView.frame=CGRectMake(50, (SCREEN_HEIGHT/2)-25, SCREEN_WIDTH-100, 50);
        loadingView.backgroundColor = [UIColor whiteColor];
        loadingView.clipsToBounds = YES;
//        loadingView.center = self.center;
        
        activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityView.frame = CGRectMake(10, 10, 30, 30);
        [loadingView addSubview:activityView];
        
        loadingLabel = [[UILabel alloc] init];
        loadingLabel.frame=CGRectMake(activityView.frame.origin.x+activityView.frame.size.width+2, 10, loadingView.frame.size.width-(activityView.frame.origin.x+activityView.frame.size.width+2), 30);
        loadingLabel.backgroundColor = [UIColor clearColor];
        loadingLabel.textColor = [UIColor lightGrayColor];
        loadingLabel.adjustsFontSizeToFitWidth = YES;
        loadingLabel.textAlignment = NSTextAlignmentLeft;
        loadingLabel.font = [UIFont systemFontOfSize:15.0];
        loadingLabel.text =[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_loading_text", nil)];
        [loadingView addSubview:loadingLabel];
        [self addSubview:loadingView];
    }
    return self;
}

-(void)startActivityIndicator
{
    [activityView startAnimating];
}

-(void)stopActivityIndicator
{
    [activityView stopAnimating];
}

@end
