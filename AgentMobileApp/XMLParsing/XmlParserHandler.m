//
//  XmlParserHandler.m
//  Consumer Client
//
//  Created by android on 6/4/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "XmlParserHandler.h"
#import "WebServiceConstants.h"
#import "WebServiceResponseProcessing.h"
//susmit
#import "ValidationsClass.h"
#import "Constants.h"
#import "Localization.h"
//susmit
#import "AppDelegate.h"
#import "GenerateOTP.h"
#import <objc/message.h>

@implementation XmlParserHandler
{
    NSMutableDictionary *requestDictionary;
}

@synthesize delegate;

-(void)parseResponseData:(NSData *)pResponse withReqDictionary:(NSMutableDictionary *)reqDict withDelegate:(id)sendDelegate;
{
    requestDictionary = reqDict;
    senderDelegate = sendDelegate;
    isFalutResponse = false;
    self.xmlParser = [[NSXMLParser alloc] initWithData:pResponse];
    self.xmlParser.delegate = self;
    [self.xmlParser parse]; // Start parsing.
}

-(void)parserDidStartDocument:(NSXMLParser *)parser
{
    
}
-(NSString *)descriptionForObject:(id)objct
{
    unsigned int varCount;
    NSMutableString *descriptionString = [[NSMutableString alloc]init];
    
    
    objc_property_t *vars = class_copyPropertyList(object_getClass(objct), &varCount);
    
    for (int i = 0; i < varCount; i++)
    {
        objc_property_t var = vars[i];
        
        const char* name = property_getName (var);
        
        NSString *keyValueString = [NSString stringWithFormat:@"\n%@ = %@",[NSString stringWithUTF8String:name],[objct valueForKey:[NSString stringWithUTF8String:name]]];
        [descriptionString appendString:keyValueString];
    }
    
    free(vars);
    return descriptionString;
}
-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    NSString *responseData=[self descriptionForObject:webServiceDataObject];
    NSLog(@"REQUEST_DATA: %@",requestDictionary);
    NSLog(@"RESPONSE_DATA: %@",responseData);
    if(IS_DEBUGGING){
        if([[requestDictionary objectForKey:PARAMETER15] isEqualToString:@"0044"]){
            UITextView *txtView = [[UITextView alloc] initWithFrame:CGRectMake(0, 260, SCREEN_WIDTH, 60)];
            NSString *str = [NSString stringWithFormat:@"Completed parsing.. Processor Code=%@ Request id:%@", [requestDictionary objectForKey:PARAMETER15], [requestDictionary objectForKey:@"RequestId"]];
            [txtView setBackgroundColor:[UIColor whiteColor]];
            [txtView setUserInteractionEnabled:NO];
            [txtView setText:str];
            [txtView accessibilityScroll:UIAccessibilityScrollDirectionUp];
            [[UIApplication sharedApplication].keyWindow addSubview:txtView];
        }
    }
    NSString *transactionStatus = webServiceDataObject.TxnStatus;
    if (([transactionStatus compare:@"(null)"] == NSOrderedSame) || (transactionStatus.integerValue == 1) || (transactionStatus.length == 0)) {
        NSLog(@"[transactionStatus compare:");
        if ([webServiceDataObject.ProcessorCode isEqualToString:PROCESSOR_CODE_SELF_REG_DIGITAL_KYC]) {
            if (webServiceDataObject.PaymentDetails4) {
                WebServiceResponseProcessing *processRequest = [[WebServiceResponseProcessing alloc] init];
                processRequest.delegate = senderDelegate;
                [processRequest processResponseData:webServiceDataObject withRequestDictionary:requestDictionary];
                return;
            }
        }
        
        NSString *errorCode = webServiceDataObject.ErrorCode;
        NSLog(@"errorCode %@", errorCode);
        NSString *appendingString;
        NSString *message;
        NSString *technicalErrorMessage = NSLocalizedStringFromTableInBundle(@"999",[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil);
        
        if (errorCode.length != 0) {
            if([NSLocalizedStringFromTableInBundle(@"application_is_error_code_required_for_error_message",@"GeneralSettings",[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                appendingString = [[[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_label_prefix", nil)] stringByAppendingString:errorCode] stringByAppendingString:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_error_msg_seperator", nil)]];
            else
                appendingString=@"";
        }
        else
        {
        if([NSLocalizedStringFromTableInBundle(@"application_is_error_code_required_for_error_message",@"GeneralSettings",[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                appendingString = [[[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_label_prefix", nil)] stringByAppendingString:@"999"] stringByAppendingString:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_error_msg_seperator", nil)]];
            else
                appendingString=@"";
        }
        
        if (webServiceDataObject.Reference5)
            message = [self formatMessageWithMessage:NSLocalizedStringFromTableInBundle(webServiceDataObject.ErrorCode,[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil) withReferenceValues:webServiceDataObject.Reference5];
        else
            message =[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(webServiceDataObject.ErrorCode,[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil)];
        
        NSString *propertyFileString = NSLocalizedStringFromTableInBundle(errorCode,[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil);
        
        if ([propertyFileString isEqualToString:webServiceDataObject.ErrorCode] || propertyFileString.length == 0) {
            NSString *displayString = [appendingString stringByAppendingString:technicalErrorMessage];
            [delegate errorCodeHandlingInXML:displayString andMessgae:nil withProcessorCode:webServiceDataObject.ProcessorCode];
            
            NSLog(@"Error Message is..%@",displayString);
        }
        else
        {
            [delegate errorCodeHandlingInXML:nil andMessgae:[NSString stringWithFormat:@"%@%@",appendingString,message] withProcessorCode:webServiceDataObject.ProcessorCode];
            NSLog(@"Error Message is..%@",[NSString stringWithFormat:@"%@ %@",appendingString,message]);
        }
    }
    else
    {
        NSLog(@"No errors found..");
        WebServiceResponseProcessing *processRequest = [[WebServiceResponseProcessing alloc] init];
        processRequest.delegate = senderDelegate;
        [processRequest processResponseData:webServiceDataObject withRequestDictionary:requestDictionary];
    }
}

-(NSString *)formatMessageWithMessage:(NSString *)error withReferenceValues:(NSString *)reference
{
    NSString *replaceString;
    NSArray *referenceValues = [reference componentsSeparatedByString:@"|"];

    for (int i = 0; i < referenceValues.count; i++)
    {
        replaceString = [NSString stringWithFormat:@"{%d}",i];
        error = [error stringByReplacingOccurrencesOfString:replaceString withString:[referenceValues objectAtIndex:i]];
    }
    return error;
}

-(void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    NSString *appendStringval=nil;
    if([NSLocalizedStringFromTableInBundle(@"application_is_error_code_required_for_error_message",@"GeneralSettings",[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        appendStringval = [[[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_label_prefix", nil)] stringByAppendingString:[NSString stringWithFormat:@"_1003"]] stringByAppendingString:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_error_msg_seperator", nil)]];
    else
        appendStringval=@"";
        
    [delegate errorCodeHandlingInXML:[NSString stringWithFormat:@"%@%@",appendStringval,NSLocalizedStringFromTableInBundle(@"_1003",[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil)] andMessgae:[parseError localizedDescription] withProcessorCode:webServiceDataObject.ProcessorCode];
    NSLog(@"Append Values are...%@",appendStringval);
    
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"soapenv:Body"])
    {
        webServiceDataObject = [[WebServiceDataObject alloc] init];
    }
    else if ([elementName isEqualToString:@"env:Body"])
    {
        webServiceDataObject = [[WebServiceDataObject alloc] init];
    }
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:RES_FAULTCODE])
    {
        webServiceDataObject.faultCode = foundValue;
    }
    else if ([elementName isEqualToString:RES_FAULTSTRING])
    {
        webServiceDataObject.faultString = foundValue;
    }
    else if ([elementName isEqualToString:RES_FAULT])
    {
        isFalutResponse = true;
    }
    else if ([elementName isEqualToString:RES_PARAMETER1])
    {
        webServiceDataObject.GwalTraceId = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER2])
    {
        webServiceDataObject.ApiIdType = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER3])
    {
        webServiceDataObject.Tier1AgentId = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER4])
    {
        webServiceDataObject.Tier1AgentName = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER5])
    {
        webServiceDataObject.Tier2AgentId = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER6])
    {
        webServiceDataObject.Tier3AgentId = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER7])
    {
        webServiceDataObject.Tier1AgentPassword = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER8])
    {
        webServiceDataObject.Tier3AgentPassword = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER9])
    {
        webServiceDataObject.CustomerPhoneNumber = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER10])
    {
        webServiceDataObject.NationalID = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER11])
    {
        webServiceDataObject.SenderFirstName = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER12])
    {
        webServiceDataObject.SenderLastName = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER13])
    {
        webServiceDataObject.TransactionType = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER14])
    {
        webServiceDataObject.InstrumentType = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER15])
    {
        webServiceDataObject.ProcessorCode = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER16])
    {
        webServiceDataObject.CashInAmount = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER17])
    {
        webServiceDataObject.PaymentDetails1 = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER18])
    {
        webServiceDataObject.PaymentDetails2 = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER19])
    {
        webServiceDataObject.PaymentDetails3 = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER20])
    {
        webServiceDataObject.PaymentDetails4 = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER21])
    {
        webServiceDataObject.TxnAmount = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER22])
    {
        webServiceDataObject.CurrencyType = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER23])
    {
        webServiceDataObject.NetTxnAmount = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER24])
    {
        webServiceDataObject.FeeAmount = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER25])
    {
        webServiceDataObject.TaxAmount = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER26])
    {
        webServiceDataObject.Country = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER27])
    {
        webServiceDataObject.RequestId = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER28])
    {
        webServiceDataObject.TerminalID = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER29])
    {
        webServiceDataObject.TxnStatus = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER30])
    {
        webServiceDataObject.ErrorCode = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER31])
    {
        webServiceDataObject.OpsTransactionId = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER32])
    {
        webServiceDataObject.TransactionDate = foundValue;
        NSString *removingTimeOffset=[[foundValue componentsSeparatedByString:@"+"] objectAtIndex:0];
        
        [[NSUserDefaults standardUserDefaults] setDouble:[ValidationsClass getMilliSecondDateFormateForOTPGeneration:removingTimeOffset] forKey:SERVER_LAST_TRANSACTION_TIME];
        
        NSString* curDate = [GenerateOTP toStringFromDateTime:[NSDate date]];
        [[NSUserDefaults standardUserDefaults] setDouble:[ValidationsClass getMilliSecondDateFormateForOTPGeneration:curDate] forKey:DEVICE_LAST_TRANSACTION_TIME];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if ([elementName isEqualToString:RES_PARAMETER33])
    {
        webServiceDataObject.Remark = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER34])
    {
        webServiceDataObject.Reference1 = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER35])
    {
        webServiceDataObject.Reference2 = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER36])
    {
        webServiceDataObject.Reference3 = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER37])
    {
        webServiceDataObject.Reference4 = foundValue;
    }
    else if ([elementName isEqualToString:RES_PARAMETER38])
    {
        webServiceDataObject.Reference5 = foundValue;
    }

    if (isFalutResponse)
    {
                 // Needed to check PLS un comment
        // NSLog(@"webServiceDataObject.faultString: %@",webServiceDataObject.faultString);
         webServiceDataObject.ErrorCode = @"-1";
    }
    // Clear the mutable string.
    foundValue = nil;
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if (!foundValue)
        foundValue=[[NSMutableString alloc] init];
        [foundValue appendString:string];
}

@end
