//
//  Template.m
//  Consumer Client
//
//  Created by android on 7/24/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "Template.h"
#import "WebServiceConstants.h"

@implementation Template

#pragma mark - Template Object.

/**
 * This method is used to get define Template.
 */
+(Template *)sharedTemplate
{
    static Template *sharedInstance=nil;
    static dispatch_once_t  oncePredecate;
    
    dispatch_once(&oncePredecate,^{
        sharedInstance=[[Template alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Template Object Related Data.
/**
 * This method is used to define Template Realated Properties (Feature - NextTemplate and NextTemplate Property file).
 */
+(Template *) initWithactionType:(NSString *)actionType nextTemplate:(NSString *)nextTemplate nextTemplatePropertyFile:(NSString *)nextTemplatePropertyFile currentTemplatePropertyFile:(NSString *)currentTemplatePropertyFile  ProcessorCode:(NSString *)ProcessorCode Dictionary:(NSMutableDictionary *)Dictionary contentArray:(NSMutableArray *)contentArray alertType:(NSString *)alertType ValidationType:(NSString *)ValidationType inClass :(id)classType withApiParameter:(NSString *)apiParameterType withLableValidation :(NSMutableArray *)labelValidationArray withSelectedLocalProcessorCode:(NSString *)selectedLocalProcessorCode withSelectedIndex:(int)selectedIndex withTransactionType:(NSString *)transactionType withCurrentClass:(NSString *)currentClass{
  
    NSLog(@"CurrentPropertyFileName:%@",currentTemplatePropertyFile);
    NSLog(@"NextPropertyFileName:%@",nextTemplatePropertyFile);
    
    Template *templeteObj = [Template sharedTemplate];
    templeteObj.actionType = actionType;
    templeteObj.nextTemplateName = nextTemplate;
    templeteObj.nextTemplatepropertyFile = nextTemplatePropertyFile;
    templeteObj.currentTemplatepropertyFile = currentTemplatePropertyFile;
    templeteObj.processorCode = ProcessorCode;
    templeteObj.dictionary = Dictionary;
    templeteObj.contentArray = contentArray;
    templeteObj.alertType = alertType;
    templeteObj.validationType = ValidationType;
    templeteObj.classType = classType;
    templeteObj.apiParameterType = apiParameterType;
    templeteObj.labelValidationArray = labelValidationArray;
    templeteObj.selectedLocalProcessorCode = selectedLocalProcessorCode;
    templeteObj.selectedIndex = selectedIndex;
    templeteObj.transactionType = transactionType;
    templeteObj.currentClass = currentClass;
    return templeteObj;
}
#pragma mark - get FeatureMenu Template Index.
/**
 * This method is used to get nextTemplate feature menu Index
 */
+(NSDictionary *) getNextFeatureTemplateWithIndex:(int) tag
{
    NSDictionary *dict;
    NSString *featurePropertFileName;

    
    NSString *profileID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"] objectForKey:PARAMETER36];
  //  NSLog(@"Profile ID : %@",profileID);
    
    NSMutableArray *mainTemplates;
    NSMutableArray *templatesImgs;
    NSMutableArray *templates;
    NSMutableArray *templateProperties;
    NSString *selectedProfile;
    NSMutableArray *profileFeatureArray;
    
 //    featurePropertFileName
    featurePropertFileName=@"FeatureMenu_ListT1";
    
    NSMutableArray *profilesList = [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(@"profiles_list", featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
    NSMutableArray *profileKeys =  [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(@"profiles_keys", featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];

    for (int i=0; i < profileKeys.count; i++) {
        if ([profileID isEqualToString:[profilesList objectAtIndex:i]]) {
            selectedProfile = [profileKeys objectAtIndex:i];
            break;
        }
    }
   
    if (selectedProfile) {
        profileFeatureArray =[[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(selectedProfile, featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
    }
    else if ([selectedProfile compare:@"(null)"]== NSOrderedSame) {
        selectedProfile=@"sms_default_profile";
        profileFeatureArray =[[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(selectedProfile,featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
    }
    
    NSMutableArray *finalMainTemplates = [[NSMutableArray alloc] init];
    NSMutableArray *finalImages = [[NSMutableArray alloc] init];
    NSMutableArray *finalTemplates = [[NSMutableArray alloc] init];
    NSMutableArray *finalTemplateProperties = [[NSMutableArray alloc] init];
    
    if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"0"])
    {
        if (selectedProfile) {
            mainTemplates =[[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(selectedProfile, featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
            templatesImgs = [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(([selectedProfile stringByAppendingString:@"_template_images"]), featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
            templates = [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(([selectedProfile stringByAppendingString:@"_template"]), featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
            templateProperties = [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(([selectedProfile stringByAppendingString:@"_template_properties"]), featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
            int min = (int)MIN(MIN(mainTemplates.count, templatesImgs.count),MIN(templates.count,templateProperties.count));
            for (int i = 0; i < min; i++) {
                [finalMainTemplates addObject:[mainTemplates objectAtIndex:i]];
                [finalImages addObject:[templatesImgs objectAtIndex:i]];
                [finalTemplates addObject:[templates objectAtIndex:i]];
                [finalTemplateProperties addObject:[templateProperties objectAtIndex:i]];
            }
        }
        dict = [[NSDictionary alloc] initWithObjectsAndKeys:finalMainTemplates,KEY_TEMPLATES,finalImages,IMAGES,finalTemplates,SUB_TEMPLATES,finalTemplateProperties,SUB_TEMPLATES_PROPERTIES,nil];
     
    }
    if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"1"])
    {
        NSMutableArray *categoryNames =  [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(@"category_names", featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
        NSMutableArray *categoryTemplateNames =  [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(@"category_template_name", featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
        NSMutableArray *categoryImages =  [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(@"category_template_images", featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
        NSMutableArray *categoryPropertyFiles =  [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(@"category_template_property_files", featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
        NSMutableArray *keyCategories =  [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(@"category_key_names", featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
        
        NSMutableArray *tempCategoryNames = [[NSMutableArray alloc] init];
        NSMutableArray *tempCategoryTemplateNames = [[NSMutableArray alloc] init];
        NSMutableArray *tempCategoryImages = [[NSMutableArray alloc] init];
        NSMutableArray *tempKeyCategories = [[NSMutableArray alloc] init];
        NSMutableArray *tempCategoriesPropertyFiles = [[NSMutableArray alloc] init];
        
        int minValue = (int)MIN(MIN(MIN(categoryNames.count, categoryTemplateNames.count),MIN(categoryImages.count, keyCategories.count)),categoryPropertyFiles.count);

        for (int i = 0; i < minValue; i++) {
            
            NSString *catFeature = NSLocalizedStringFromTableInBundle([[keyCategories objectAtIndex:i] stringByAppendingString:@"_features"], featurePropertFileName,[NSBundle mainBundle], nil);
            NSString *catTemplates = NSLocalizedStringFromTableInBundle([[keyCategories objectAtIndex:i] stringByAppendingString: @"_templates"], featurePropertFileName,[NSBundle mainBundle], nil);
            NSString *catTemplateImages = NSLocalizedStringFromTableInBundle([[keyCategories objectAtIndex:i] stringByAppendingString:@"_template_images"], featurePropertFileName,[NSBundle mainBundle], nil);
            NSString *catTemplatePropertyFileName = NSLocalizedStringFromTableInBundle([[keyCategories objectAtIndex:i] stringByAppendingString:@"_templates_property_file_name"], featurePropertFileName,[NSBundle mainBundle], nil);
            
            if (((catFeature.length > 0) && (![catFeature isEqualToString:[[keyCategories objectAtIndex:i] stringByAppendingString:@"_features"]])) &&
                ((catTemplates.length > 0) && (![catTemplates isEqualToString:[[keyCategories objectAtIndex:i] stringByAppendingString:@"_templates"]])) &&
                ((catTemplateImages.length > 0) && (![catTemplateImages isEqualToString:[[keyCategories objectAtIndex:i] stringByAppendingString:@"_template_images"]])) &&
                ((catTemplatePropertyFileName.length > 0) && (![catTemplatePropertyFileName isEqualToString:[[keyCategories objectAtIndex:i] stringByAppendingString:@"_templates_property_file_name"]])))
            {
                int count = 0;
                NSMutableArray *catFeaturesArray = [[NSMutableArray alloc] initWithArray:[catFeature componentsSeparatedByString:@","]];
                for (int i=0; i < catFeaturesArray.count ; i++) {
                    if ([profileFeatureArray containsObject:[catFeaturesArray objectAtIndex:i]]) {
                        count++;
                    }
                }
                
                if (count) {
                    [tempCategoryNames addObject:[categoryNames objectAtIndex:i]];
                    [tempCategoryTemplateNames addObject:[categoryTemplateNames objectAtIndex:i]];
                    [tempCategoryImages addObject:[categoryImages objectAtIndex:i]];
                    [tempKeyCategories addObject:[keyCategories objectAtIndex:i]];
                    [tempCategoriesPropertyFiles addObject:[categoryPropertyFiles objectAtIndex:i]];
                }
            }
        }
        
        NSMutableArray *finalCatFeature = [[NSMutableArray alloc] init];
        NSMutableArray *finalCatTemplates = [[NSMutableArray alloc] init];
        NSMutableArray *finalCatTemplateImages = [[NSMutableArray alloc] init];
        NSMutableArray *finalCatTemplatePropertyFileName = [[NSMutableArray alloc] init];

        
        if (tag > 0) {
            tag = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"topLevelNavigation"];
            NSMutableArray *catFeature = [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(([[tempKeyCategories objectAtIndex:tag-1] stringByAppendingString:@"_features"]), featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
            NSMutableArray *catTemplates = [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(([[tempKeyCategories objectAtIndex:tag-1] stringByAppendingString: @"_templates"]), featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
            NSMutableArray *catTemplateImages = [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(([[tempKeyCategories objectAtIndex:tag-1] stringByAppendingString:@"_template_images"]), featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
            NSMutableArray *catTemplatePropertyFileName = [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(([[tempKeyCategories objectAtIndex:tag-1] stringByAppendingString:@"_templates_property_file_name"]), featurePropertFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
            
            int min = (int)MIN(MIN(catFeature.count, catTemplates.count),MIN(catTemplateImages.count,catTemplatePropertyFileName.count));
            
            for (int i = 0; i < min; i++) {
                if ([profileFeatureArray containsObject:[catFeature objectAtIndex:i]]) {
                    [finalCatFeature addObject:[catFeature objectAtIndex:i]];
                    [finalCatTemplates addObject:[catTemplates objectAtIndex:i]];
                    [finalCatTemplateImages addObject:[catTemplateImages objectAtIndex:i]];
                    [finalCatTemplatePropertyFileName addObject:[catTemplatePropertyFileName objectAtIndex:i]];
                }
            }
        }
        
        dict = [[NSDictionary alloc] initWithObjectsAndKeys:tempCategoryNames,CATEGORIES_NAMES_STRING,tempCategoryImages,CATEGORIES_IMAGES,tempCategoryTemplateNames,CATEGORIES_NAMES,finalCatFeature,KEY_CATEGORY_FEATURES,finalCatTemplates,KEY_CATEGORY_TEMPLATES,finalCatTemplateImages,IMAGES,finalCatTemplatePropertyFileName,KEY_CATEGORY_TEMPLATE_PROPERTY_FILES,nil];
        
         NSLog(@"Dictionary : %@",dict);
    }
    return dict;
}
#pragma mark - get Processor Code.
/**
 * This method is used to get processor code of the Property file.
 */
+(NSString *)getProcessorCodeWithData :(NSString *)data
{
    return [[data componentsSeparatedByString:@","] objectAtIndex:0];
}
#pragma mark - get Transaction Code.
/**
 * This method is used to get transaction code of the Property file.
 */
+(NSString *)getTransactionCodeWithData :(NSString *)data
{
    NSArray *codeArr = [data componentsSeparatedByString:@","];
    
    if (codeArr.count >= 2) {
        return [codeArr objectAtIndex:1];
    }
    else
    return nil;
}

#pragma mark - Get Reference Paramter.
/**
 * This method is used to get Reference Parameter of Template
 */
+(NSString *)getReferenceParameter :(NSString *)data
{
    NSArray *codeArr = [data componentsSeparatedByString:@","];
    
    if (codeArr.count > 2) {
        return [codeArr objectAtIndex:2];
    }
    else
    return nil;
}
#pragma mark - Get Sms format text.(Application is in SMS Mode).
/**
 * This method is used to get SMS mode Template Data.
 @param Type - Processor Code,Transaction code,PaymentDetails1...etc
 */
+(NSString *)getSMSFormatTextWithDictionary:(NSMutableDictionary *)valueDictionary
{
    NSString *formattedText;
//    [valueDictionary setObject:[self getRequestId] forKey:PARAMETER27];
//    NSString *customerNumber;
//    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
//    
//    if ([[valueDictionary objectForKey:PARAMETER15] isEqualToString:PROCESSOR_CODE_SELF_REG] || [[valueDictionary objectForKey:PARAMETER15] isEqualToString:PROCESSOR_CODE_ACTIVATION] || [[valueDictionary objectForKey:PARAMETER15] isEqualToString:PROCESSOR_CODE_RESET_PIN_FORGOT_MPIN]) {
//        customerNumber = [valueDictionary objectForKey:PARAMETER9];
//    }
//    else {
//        customerNumber = [[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9];
//    }
//    
//    
//    
//        formattedText = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@",
//                         customerNumber,SMS_TIER1_AGENT_ID,[valueDictionary objectForKey:PARAMETER11],[valueDictionary objectForKey:PARAMETER12],[valueDictionary objectForKey:PARAMETER10],[valueDictionary objectForKey:PARAMETER13],
//                         [valueDictionary objectForKey:PARAMETER14],[valueDictionary objectForKey:PARAMETER15],
//                         [valueDictionary objectForKey:PARAMETER21],[valueDictionary objectForKey:PARAMETER24],
//                         [valueDictionary objectForKey:PARAMETER27],[valueDictionary objectForKey:PARAMETER17],
//                         [valueDictionary objectForKey:PARAMETER18],[valueDictionary objectForKey:PARAMETER19],
//                         [valueDictionary objectForKey:PARAMETER20],[valueDictionary objectForKey:PARAMETER34],
//                         [valueDictionary objectForKey:PARAMETER35],[valueDictionary objectForKey:PARAMETER36],
//                         [valueDictionary objectForKey:PARAMETER37],[valueDictionary objectForKey:PARAMETER38]];
//    
    
    NSString *compareStr=[NSString stringWithFormat:@"%@_%@",[valueDictionary objectForKey:PARAMETER15],[valueDictionary objectForKey:PARAMETER13]];
    
    if (compareStr)
    {
        NSArray *paramList=[NSLocalizedStringFromTableInBundle(compareStr,@"GeneralSettings",[NSBundle mainBundle], nil) componentsSeparatedByString:@","];
        NSString *str=[NSString stringWithFormat:@"%@%@",[valueDictionary objectForKey:PARAMETER15],[valueDictionary objectForKey:PARAMETER13]];
        if (paramList.count>0)
        {
            for (NSString *ele in paramList)
            {
                str=[str stringByAppendingString:[NSString stringWithFormat:@" %@",[valueDictionary objectForKey:ele]]];
            }
            formattedText=str;
        }
        else
        {
            formattedText=@"";
        }
    }
    else
    {
        formattedText=@"";
    }
    return [formattedText stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
}

#pragma mark - Get Formated Request.
/**
 * This method is used to get Formated key values dictionary.
 */
+(NSString *)getFormatedKey:(NSMutableDictionary *)valueDictionary
{
    //Format Text For SMS & Reply
    NSString *formattedText;

    formattedText = [NSString stringWithFormat:@"%@|%@|%@|%@|%@||%@|%@|%@|%@|%@|%@|%@|%@|%@",
                     @"000",[valueDictionary objectForKey:PARAMETER13],
                     [valueDictionary objectForKey:PARAMETER14],[valueDictionary objectForKey:PARAMETER15],
                     [valueDictionary objectForKey:PARAMETER25],
                     [valueDictionary objectForKey:PARAMETER17],[valueDictionary objectForKey:PARAMETER18],
                     [valueDictionary objectForKey:PARAMETER19],[valueDictionary objectForKey:PARAMETER20],
                     [valueDictionary objectForKey:PARAMETER34],[valueDictionary objectForKey:PARAMETER35],
                     [valueDictionary objectForKey:PARAMETER36],[valueDictionary objectForKey:PARAMETER37],
                     [valueDictionary objectForKey:PARAMETER38]];
    
    return [formattedText stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
}

#pragma mark - get Request Id.
/**
 * This method is used to get request Id based on current time.
 */
+(NSString *)getRequestId
{
    NSString *requestId = nil;
    long currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
    requestId = [NSString stringWithFormat:@"%ld",currentTime];
    return requestId;
}

#pragma mark - Application Mode(GPRS or SMS).
/**
 * This method is used to get Application mode (SMS or GPRS).
 */
+(BOOL) checkMode
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *mode = [standardUserDefaults objectForKey:@"applicationMode"];
    if ([mode isEqualToString:@"SMS"]) {
        return YES;
    }
    else
    return NO;
}
#pragma mark - Calculate Fee for given Amount.
/**
 * This method is used to get Fee for amount based Transaction Type.
 */
+(NSString *) getFeeForAmount:(NSString *)feeAmount andTransactionType:(NSString *)transactionType
{
    if ([transactionType isEqualToString:@"DTH"]) {
        return @"0.00";
    }
    else
    if ([transactionType isEqualToString:@"NP2M-NA"]) {
        return @"0.00";
    }
    else
    if ([transactionType isEqualToString:@"BILLPAY"]) {
        return @"0.00";
    }
    else
    if([transactionType isEqualToString:@"TOPUP"])
    {
        return @"0.00";
    }
    else
    if ([transactionType isEqualToString:@"OBOANY"]) {
        
        if ((feeAmount.doubleValue >= 0) && (feeAmount.doubleValue < 500)) {
            return @"0.00";
        }
        else
        if (feeAmount.doubleValue == 500) {
            return @"10.00";
        }
        else
        if ((feeAmount.doubleValue >= 501) && (feeAmount.doubleValue <= 1000)) {
            return @"15.00";
        }
        else
        if ((feeAmount.doubleValue >= 1001) && (feeAmount.doubleValue <= 3000)) {
            return @"20.00";
        }
        else
        if ((feeAmount.doubleValue >= 3001) && (feeAmount.doubleValue <= 5000)) {
            return @"25.00";
        }
        else
        return @"0.00";
    }
    else
    if ([transactionType isEqualToString:@"P2M"]) {
        return @"0.00";
    }
    else
    if ([transactionType isEqualToString:@"P2P"]) {
        return @"0.00";
    }
    else
    if ([transactionType isEqualToString:@"NP2P"]) {

        if ((feeAmount.doubleValue >= 0) && (feeAmount.doubleValue < 500)) {
            return @"0.00";
        }
        else
        if (feeAmount.doubleValue == 500) {
            return @"10.00";
        }
        else
        if ((feeAmount.doubleValue >= 501) && (feeAmount.doubleValue <= 1000)) {
            return @"15.00";
        }
        else
        if ((feeAmount.doubleValue >= 1001) && (feeAmount.doubleValue <= 3000)) {
            return @"20.00";
        }
        else
        if ((feeAmount.doubleValue >= 3001) && (feeAmount.doubleValue <= 5000)) {
            return @"25.00";
        }
        else
        return @"0.00";
    }
    else
    if ([transactionType isEqualToString:@"BCASHOUT"]) {
        if (feeAmount.doubleValue == 5000) {
            return @"33.71";
        }
        else
        return @"0.00";
    }
    else
    return @"0.00";
}

#pragma mark - External Error message Format.
/**
 * This method is used to Format the message.
 */
//Format External Message
+(NSString *)formatMessageWithMessage:(NSString *)error withReferenceValues:(NSString *)reference
{
    NSString *replaceString;
    NSArray *referenceValues = [reference componentsSeparatedByString:@"|"];
    
    for (int i = 0; i < referenceValues.count; i++)
    {
        replaceString = [NSString stringWithFormat:@"{%d}",i];
        error = [error stringByReplacingOccurrencesOfString:replaceString withString:[referenceValues objectAtIndex:i]];
    }
    return error;
}

@end
