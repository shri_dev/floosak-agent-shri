//
//  BackgroundViewTemplate1.m
//  AgentMobileApp
//
//  Created by Gokul Krishnan on 29/01/18.
//  Copyright © 2018 Soumya. All rights reserved.
//

#import "BackgroundViewTemplate1.h"

@interface BackgroundViewTemplate1()

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *childContentView;

@end

@implementation BackgroundViewTemplate1

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)customInit {
    
    [[NSBundle mainBundle] loadNibNamed:@"BackgroundViewTemplate1" owner:self options:nil];
    
    [self.contentView setFrame:self.bounds];
    self.contentView.layer.borderWidth = self.contentViewBorderWidth;
    self.contentView.layer.borderColor = self.contentViewBorderColor.CGColor;
    self.contentView.layer.cornerRadius = 15.0;
    
    [self addSubview:self.contentView];
}

@end
