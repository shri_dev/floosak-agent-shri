//
//  BaseView.h
//  Consumer Client
//
//  Created by android on 12/30/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityIndicator.h"
/**
 * This class used to handle functionality and view of Base View.
 *
 * @author Integra
 *
 */
@interface BaseView : UIView<UIActionSheetDelegate,UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    
    /**
     * declarations are used to set the UIConstraints Of Popup template1.
     * @Param type - Feature (processor code,transactiontype,action type,next template and nexttemplate Propertyfile)
     */
    NSString *action_type;
    NSString *data;
    NSString *processorCodeStr;
    NSString *transactionType;
    NSString *referenceParam;
    NSString *alertview_Type;
    NSString *nextTemplateProperty;
    NSString *nextTemplate;
    NSString *paramType;
    NSString *validation_Type;
    NSString *scannedData;
    NSString *imagePickerStatus;
    NSMutableDictionary *datavalueDictionary;
    NSMutableArray *validationsArray;
    NSString *buttonActionString;
    NSString *currentClass;
    ActivityIndicator *activityIndicator;
    BOOL isTransformed;
    NSData *imageData;
}

@property(nonatomic,strong) NSString *propertyFileName;

/**
 * This method is used to configure view of Base View.
*/
-(void) configureWithProcessorCode;
// Add Activity Indicator.
-(void) setActivityIndicator;
// Remove Activity Indicator.
-(void) removeActivityIndicator;

@end
