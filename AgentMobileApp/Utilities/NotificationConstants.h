//
//  NotificationConstants.h
//  Consumer Client
//
//  Created by android on 7/27/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

/**
 *Notification Constants For PopUptemplate1 Button Action.
 */
#define PPT1_BUTTON1_ACTION @"ppt1_button1Action"
#define PPT1_BUTTON2_ACTION @"ppt1_button2Action"

#define POPUPTEMPLATE1 @"popUp_template1"
#define POPUPTEMPLATE6 @"popUp_template6"
/**
 *Notification Constants For PopUptemplate2 Button Action.
 */
#define PPT2_BUTTON2_ACTION @"ppt2_button2Action"
#define PPT2_BUTTON1_ACTION @"ppt2_button1Action"

/**
 *Notification Constants For PopUptemplate3 Button Action.
 */
#define PPT3_BUTTON1_ACTION @"ppt3_button1Action"

/**
 *Notification Constants For PopUptemplate5 Button Action.
 */
#define PPT5_BUTTON1_ACTION @"ppt5_button1Action"
#define PPT5_BUTTON2_ACTION @"ppt5_button2Action"
#define PPT5_BUTTON3_ACTION @"ppt5_button3Action"
/**
 *Notification Constants For PopUptemplate6 Button Action.
 */
#define PPT6_BUTTON1_ACTION @"ppt6_button1Action"
#define PPT6_BUTTON2_ACTION @"ppt6_button2Action"
/**
 *Notification Constants For Childtemplate1 Button Action.
 */
#define CT1_BUTTON1_ACTION @"ct1_button1Action"
#define CT1_BUTTON2_ACTION @"ct1_button2Action"

#define CHILD_TEMPLATE1 @"ct1SetValue"
/**
 *Notification Constants For Childtemplate2.
 */
#define CHILD_TEMPLATE2 @"ct2SetValue"
/**
 *Notification Constants For Childtemplate4.
 */
#define CT4_LIST_ACTION @"ct4listAction"
#define CHILD_TEMPLATE4_CANCEL @"child_template4_cancel"
#define CHILD_TEMPLATE4 @"ct4SetValue"
/**
 *Notification Constants For Childtemplate9 Action.
 */
#define CT9_BUTTON2_ACTION @"ct9_button2Action"
/**
*Notification Constants For Childtemplate11 Button Action.
*/
#define CT11_BUTTON1_ACTION @"ct11_button1Action"
#define CT11_BUTTON2_ACTION @"ct11_button2Action"
/**
 *Notification Constants For Childtemplate12 Button Action.
 */
#define CT12_BUTTON1_ACTION @"ct12_button1Action"
#define CT12_BUTTON2_ACTION @"ct12_button2Action"
/**
 *Notification Constants For PostContent Data.
 */
#define POST_CONTENT @"post_content"

/**
 *Notification Constants For Observers of All the data.
 */
#define OBSERVERS [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@",PPT1_BUTTON2_ACTION,PPT1_BUTTON1_ACTION,PPT2_BUTTON2_ACTION,PPT2_BUTTON1_ACTION,PPT3_BUTTON1_ACTION,PPT6_BUTTON2_ACTION,PPT6_BUTTON1_ACTION,CT1_BUTTON1_ACTION,CT1_BUTTON2_ACTION,CT11_BUTTON1_ACTION,CT11_BUTTON2_ACTION,CT4_LIST_ACTION,PT4_BUTTON1_ACTION,PT4_BUTTON2_ACTION,PT4_BUTTON3_ACTION,CT9_BUTTON2_ACTION,CT12_BUTTON1_ACTION,CT12_BUTTON2_ACTION]


/**
 *Notification Constants For Parenttemplate2.
 */
#define PARENT_TEMPLATE2 @"parent_template2"

#define PARENT_TEMPLATE2_CANCEL @"parent_template2_cancel"

/**
 *Notification Constants For Parenttemplate4.
 */
#define PT4_BUTTON1_ACTION @"pt4_button1Action"
#define PT4_BUTTON2_ACTION @"pt4_button2Action"
#define PT4_BUTTON3_ACTION @"pt4_button3Action"

/**
 *Notification Constants For Parenttemplate5.
 */
#define PARENT_TEMPLATE5 @"parentTemplate5"


/**
 *Notification Constants For Parenttemplate2.
 */
#define PARENT_TEMPLATE7 @"parent_template7"

#define PARENT_TEMPLATE7_CANCEL @"parent_template7_cancel"

/**
 *Notification Constants For AddressBook details(Contact Information).
 */
#define CONTACTS @"contacts"

/**
 *Notification Constants For Parenttemplate Class.
 */
//Class Constants
#define PARENT_TEMPLATE_1 @"ParentTemplate1"
#define PARENT_TEMPLATE_2 @"ParentTemplate2"
#define PARENT_TEMPLATE_3 @"ParentTemplate3"
#define PARENT_TEMPLATE_4 @"ParentTemplate4"
#define PARENT_TEMPLATE_5 @"ParentTemplate5"
#define PARENT_TEMPLATE_6 @"ParentTemplate6"
#define PARENT_TEMPLATE_7 @"ParentTemplate7"
#define PARENT_TEMPLATE_8 @"ParentTemplate8"
#define PARENT_TEMPLATE_9 @"ParentTemplate9"

/**
 *Notification Constants For Childtemplate Class.
 */
#define CHILD_TEMPLATE_1 @"ChildTemplate1"
#define CHILD_TEMPLATE_2 @"ChildTemplate2"
#define CHILD_TEMPLATE_3 @"ChildTemplate3"
#define CHILD_TEMPLATE_4 @"ChildTemplate4"
#define CHILD_TEMPLATE_9 @"ChildTemplate9"
#define CHILD_TEMPLATE_11 @"ChildTemplate11"
#define CHILD_TEMPLATE_12 @"ChildTemplate12"
#define CHILD_TEMPLATE_13 @"ChildTemplate13"
#define CHILD_TEMPLATE_14 @"ChildTemplate14"

/**
 *Notification Constants For popUptemplate Class.
 */
#define POPUP_TEMPLATE_1 @"PopUpTemplate1"
#define POPUP_TEMPLATE_2 @"PopUpTemplate2"
#define POPUP_TEMPLATE_3 @"PopUpTemplate3"
#define POPUP_TEMPLATE_5 @"PopUpTemplate5"
#define POPUP_TEMPLATE_6 @"PopUpTemplate6"
#define POPUP_TEMPLATE_7 @"PopUpTemplate7"
#define POPUP_TEMPLATE_8 @"PopUpTemplate8"

/**
 *Notification Constants For ImagePicker Class.
 */
#define IMAGE_PICKER_METHOD @"ImagePickerSelected"

/**
 *Notification Constants For Current View.
 */
#define CURRENT_IMPLEMENTATION_TEMPLATE @"currentImplementationTemplate"

/**
 *Notification Constants For Type2 Notification.
 */
#define NOTIFICATION_CHILD_TEMPLATE @"PushNotificationDynamicTemplateCT11"



