
//
//  BaseView.m
//  Consumer Client
//
//  Created by android on 12/30/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "BaseView.h"
#import "WebServiceConstants.h"
#import "Template.h"
#import "NotificationConstants.h"
#import "AppDelegate.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import "UIView+Toast.h"
#import "Localization.h"
#import "ValidationsClass.h"
#import "Constants.h"

@implementation BaseView

/**
 * This method is used to configure view of Base View.
 * @param For Configuring
 * Merchant QRCode,BarCode and Upload image functionality configure By using BaseView.
 */
-(void) configureWithProcessorCode
{
    if ([processorCodeStr isEqualToString:PROCESSOR_CODE_PAY_MERCHANT_QRCODE])
    {
        Template *obj = [Template initWithactionType:action_type nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:self.propertyFileName ProcessorCode:processorCodeStr Dictionary:datavalueDictionary contentArray:validationsArray alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:paramType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:nil withCurrentClass:currentClass];
        obj.selector = @selector(processData);
        obj.target = self;
        [[NSNotificationCenter defaultCenter] postNotificationName:buttonActionString object:obj];
        
    }
    else if ([processorCodeStr isEqualToString:PROCESSOR_CODE_PAY_MERCHANT_BARCODE])
    {
        Template *obj = [Template initWithactionType:action_type nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:self.propertyFileName ProcessorCode:processorCodeStr Dictionary:datavalueDictionary contentArray:validationsArray alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:paramType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:nil withCurrentClass:currentClass];
        obj.selector = @selector(processData);
        obj.target = self;
        [[NSNotificationCenter defaultCenter] postNotificationName:buttonActionString object:obj];
    }
    else if ([processorCodeStr isEqualToString:PROCESSOR_CODE_IMAGE_CROP])
    {

        action_type = @"1";
        Template *obj = [Template initWithactionType:action_type nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:self.propertyFileName ProcessorCode:processorCodeStr Dictionary:datavalueDictionary contentArray:validationsArray alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:paramType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:nil withCurrentClass:currentClass];
        
        obj.selector = @selector(selectPhoto);
        obj.target = self;
        [[NSNotificationCenter defaultCenter] postNotificationName:buttonActionString object:obj];
    }
    else
    {
        if(!([[NSUserDefaults standardUserDefaults] objectForKey:@"topupType"] == (id)[NSNull null] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"topupType"] length]== 0 || [[[NSUserDefaults standardUserDefaults] objectForKey:@"topupType"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length ==0))
                [datavalueDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"topupType"] forKey:@"message4"];
        
         if(!([[NSUserDefaults standardUserDefaults]objectForKey:@"selectedPPT7_TemplateId"] == (id)[NSNull null] || [[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedPPT7_TemplateId"] length]== 0 || [[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedPPT7_TemplateId"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length ==0))
                 [datavalueDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedPPT7_TemplateId"] forKey:PARAMETER35];
        
        NSUserDefaults *userDefaultsData=[NSUserDefaults standardUserDefaults];
        
        if ([userDefaultsData objectForKey:@"staticListValue"]) {
            [datavalueDictionary setObject:[userDefaultsData objectForKey:@"staticListValue"] forKey:@"staticListValue"];
        }
        
        Template *obj = [Template initWithactionType:action_type nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:self.propertyFileName ProcessorCode:processorCodeStr Dictionary:datavalueDictionary contentArray:validationsArray alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:paramType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:nil withCurrentClass:currentClass];
    
        obj.selector = @selector(processData);
        obj.target = self;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:buttonActionString object:obj];
    }
}
/**
 * This method is used to set baseviewcontroller Process data notification.
 */
-(void) processData
{
    [self openQRCodeAndBarCodeScanner];
}

#pragma mark - Add Action Sheet (Take photo, Camera)
/**
 * This method is used to set Open camera when user tap on Upload button.
 *@param type - Select From Options(Take photo and Select From gallery).
 */
-(void) selectPhoto
{
    [self endEditing:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil delegate:self cancelButtonTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancelbutton", nil)] destructiveButtonTitle: nil otherButtonTitles:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_take_Photo", nil)],[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_choose_from_gallery", nil)], nil];
    [actionSheet showInView:self];
}

#pragma mark - get scanner Data.

/**
 * This method is used to set get scanner Data.
 *@param type - Qr code or Bar code data.
 */
-(void)scannerManipulation
{
    [datavalueDictionary removeAllObjects];
    [datavalueDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"topupType"] forKey:@"message4"];
    if ([datavalueDictionary objectForKey:@"merchantId"]) {
        [datavalueDictionary setObject:[datavalueDictionary objectForKey:@"merchantId"] forKey:PARAMETER18];
    }
    // Get Data From NumberValue.
    if (scannedData !=nil) {
        NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        if ([scannedData rangeOfCharacterFromSet:notDigits].location == NSNotFound)
        {
            [datavalueDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedPPT7_TemplateId"] forKey:PARAMETER35];
            action_type=@"2";
            processorCodeStr = PROCESSOR_CODE_PRODUCT_DETAILS;
            transactionType = TRANSACTION_CODE_PRODUCT_DETAILS;
            [datavalueDictionary setObject:TRANSACTION_CODE_PRODUCT_DETAILS forKey:PARAMETER13];
            [datavalueDictionary setObject:PROCESSOR_CODE_PRODUCT_DETAILS forKey:PARAMETER15];
            [datavalueDictionary setObject:scannedData forKey:PARAMETER18];
            
            Template *obj = [Template initWithactionType:action_type nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:self.propertyFileName ProcessorCode:processorCodeStr Dictionary:datavalueDictionary contentArray:validationsArray alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:paramType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:nil withCurrentClass:currentClass];
            obj.selector = @selector(processData);
            obj.target = self;
            [[NSNotificationCenter defaultCenter] postNotificationName:buttonActionString object:obj];
            return;
        }
    }
    
    // Get Data From URL and Json Data Directly.
    if (scannedData !=nil) {
        [self removeActivityIndicator];
        NSURL *url = [NSURL URLWithString:scannedData];
        if (url && url.scheme && url.host)
        {
            //the url looks ok, do something with it
            action_type=@"1";
            NSData *jsonData = [NSData dataWithContentsOfURL:url];
            NSDictionary *jsonDict=[[NSDictionary alloc]init];
            
            if (!([jsonData length]==0)) {
                 jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil];
            }
            else{
//                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Consumer client" message:@"Scanned data nil" delegate:nil cancelButtonTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancel", nil)] otherButtonTitles:nil, nil];
//                [alert show];
                
                UIAlertController *alert= [UIAlertController alertControllerWithTitle:@"Consumer client" message:@"Scanned data nil" preferredStyle:UIAlertControllerStyleAlert];
                
                [alert addAction:[UIAlertAction actionWithTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancel", nil)] style:UIAlertActionStyleCancel handler:nil]];
                
                UIViewController *topController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
                while (topController.presentedViewController)
                {
                    topController = topController.presentedViewController;
                }
                [topController presentViewController:alert animated:NO completion:nil];
                
            }
            [datavalueDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedPPT7_TemplateId"] forKey:PARAMETER35];
            [datavalueDictionary addEntriesFromDictionary:jsonDict];
            
            if ([datavalueDictionary objectForKey:@"merchantName1"]) {
                [datavalueDictionary setObject:[datavalueDictionary objectForKey:@"merchantName1"] forKey:@"merchantName"];
            }
            
            Template *obj = [Template initWithactionType:action_type nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:self.propertyFileName ProcessorCode:nil Dictionary:datavalueDictionary contentArray:validationsArray alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:paramType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:nil withCurrentClass:currentClass];
            obj.selector = @selector(processData);
            obj.target = self;
            [[NSNotificationCenter defaultCenter] postNotificationName:buttonActionString object:obj];
            return;
        }
    }
    
    // Get Json Data Directly.
    if (scannedData !=nil) {
        
        [self removeActivityIndicator];
        action_type=@"1";
        NSData *objectData = [scannedData dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json=[[NSDictionary alloc]init];
        if (!([objectData length]==0)) {
            json = [NSJSONSerialization JSONObjectWithData:objectData options:kNilOptions error:nil];
        }
        else{
//            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Consumer client" message:@"Scanned data nil" delegate:nil cancelButtonTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancel", nil)] otherButtonTitles:nil, nil];
//            [alert show];
            
            UIAlertController *alert= [UIAlertController alertControllerWithTitle:@"Consumer client" message:@"Scanned data nil" preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancel", nil)] style:UIAlertActionStyleCancel handler:nil]];
            
            UIViewController *topController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
            while (topController.presentedViewController)
            {
                topController = topController.presentedViewController;
            }
            [topController presentViewController:alert animated:NO completion:nil];
        }
        [datavalueDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedPPT7_TemplateId"] forKey:PARAMETER35];
        [datavalueDictionary addEntriesFromDictionary:json];
        
        if ([datavalueDictionary objectForKey:@"merchantName1"]) {
            [datavalueDictionary setObject:[datavalueDictionary objectForKey:@"merchantName1"] forKey:@"merchantName"];
        }
        Template *obj = [Template initWithactionType:action_type nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:self.propertyFileName ProcessorCode:nil Dictionary:datavalueDictionary contentArray:validationsArray alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:paramType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:nil withCurrentClass:currentClass];
        obj.selector = @selector(processData);
        obj.target = self;
        [[NSNotificationCenter defaultCenter] postNotificationName:buttonActionString object:obj];
        return;
    }
}

#pragma mark - Initialize QR code,bar code Scanner.
/**
 * This method is used to set open source camera for scan  Qr code and Bar code.
 *@param type - Qr code or Bar code data.
 */
-(void)openQRCodeAndBarCodeScanner{
    
//    ZBarReaderViewController *codeReader = [ZBarReaderViewController new];
//    codeReader.readerDelegate=self;
//    codeReader.supportedOrientationsMask = ZBarOrientationMaskAll;
//    
//    ZBarImageScanner *scanner = codeReader.scanner;
//    
//    //Only For QRCode Reading
//    if ([processorCodeStr isEqualToString:PROCESSOR_CODE_PAY_MERCHANT_QRCODE]) {
//        [scanner setSymbology: 0 config: ZBAR_CFG_ENABLE to: 0];
//        [scanner setSymbology: ZBAR_QRCODE config: ZBAR_CFG_ENABLE to: 1];
//
//    }
//    
//    //Only For BarCode Reading
//    if ([processorCodeStr isEqualToString:PROCESSOR_CODE_PAY_MERCHANT_BARCODE]) {
//            [scanner setSymbology: ZBAR_I25 config: ZBAR_CFG_ENABLE to: 1];
//            [scanner setSymbology: ZBAR_QRCODE config: ZBAR_CFG_ENABLE to: 0];
//    }

    
//    AppDelegate *apDlgt = (AppDelegate *)[[UIApplication sharedApplication]delegate];
//    UINavigationController *navCntrl = (UINavigationController *)apDlgt.window.rootViewController;
//    
//    if (navCntrl && [navCntrl isKindOfClass:[UINavigationController class]])
//        [navCntrl presentViewController:codeReader animated:YES completion:nil];
}

#pragma mark - image Picker delegate Method.

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    NSString *str = [[NSUserDefaults standardUserDefaults] valueForKey:userLanguage];
    if ([str isEqualToString:@"Arabic"])
    {
        self.layer.affineTransform = CGAffineTransformMakeScale(1.0, 1.0);
    }
    [picker dismissViewControllerAnimated:NO completion:^{
        
    }];
}



/**
 * This method is used to set by using this delegate Open The camera Source Type.
 *@param type- Image Source Type(JPEG or PNG)
 *           - Image Size(KB).
 */
- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    NSString *str = [[NSUserDefaults standardUserDefaults] valueForKey:userLanguage];
    if ([str isEqualToString:@"Arabic"])
    {
        self.layer.affineTransform = CGAffineTransformMakeScale(1.0, 1.0);
    }
    __block NSData *imageData;
//    if ([processorCodeStr isEqualToString:PROCESSOR_CODE_PAY_MERCHANT_QRCODE] || [processorCodeStr isEqualToString:PROCESSOR_CODE_PAY_MERCHANT_BARCODE]) {
//        id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
//        ZBarSymbol *symbolStr = nil;
//        for(symbolStr in results)
//            // just grab the first barcode
//            break;
//        scannedData = symbolStr.data;
//        
//        [reader dismissViewControllerAnimated:YES completion:^(void){
//            [self scannerManipulation];
//        }];
//    }
    if ([processorCodeStr isEqualToString:PROCESSOR_CODE_IMAGE_CROP]) {
        [reader dismissViewControllerAnimated:YES completion:^(void){
            
            //Convert to encoded
            UIImage * pickedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
             imageData=nil;
            
            NSString *imageTypeStr=NSLocalizedStringFromTableInBundle(@"application_upload_image_type",@"GeneralSettings",[NSBundle mainBundle], nil);
            
            // Convert an base64 Encoding Value.
            if ([[imageTypeStr uppercaseString] isEqualToString:@"JPEG"]) {
                imageData= UIImageJPEGRepresentation(pickedImage, 0.2);
                [[UIPasteboard generalPasteboard] setData:imageData forPasteboardType:(id)kUTTypeJPEG];
            }
            else if ([imageTypeStr isEqualToString:@"PNG"]) {
                imageData =UIImagePNGRepresentation(pickedImage);
                [[UIPasteboard generalPasteboard] setData:imageData forPasteboardType:(id)kUTTypeJPEG];
            }
//            else if ([imageTypeStr isEqualToString:@""]){
//                imageData =UIImageJPEGRepresentation(pickedImage, 0.2);
//            }
            else
            {
                        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10010", nil)];
                        NSString *replaceString = @"<size>";
                        NSString *message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:imageTypeStr];
                
                        // Backgroundcolor of toast.
                        NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
                
                        // textColor of toast
                        NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                        [self makeToast:message duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
            }

          
            NSString *base64ImageData = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            [datavalueDictionary setObject:base64ImageData forKey:PARAMETER19];
            
            NSString *imageSizeStr=[NSByteCountFormatter stringFromByteCount:imageData.length countStyle:NSByteCountFormatterCountStyleFile];

            if ([[[imageSizeStr componentsSeparatedByString:@" KB"] objectAtIndex:0] integerValue] < [NSLocalizedStringFromTableInBundle(@"application_upload_image_size",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue]) {
                action_type = nil;
                Template *obj = [Template initWithactionType:action_type nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:self.propertyFileName ProcessorCode:processorCodeStr Dictionary:datavalueDictionary contentArray:validationsArray alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:paramType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:nil withCurrentClass:currentClass];
                obj.selector = @selector(processData);
                obj.target = self;
                [[NSNotificationCenter defaultCenter] postNotificationName:buttonActionString object:obj];
            }
            else{
                NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10011", nil)];
                NSString *replaceString = @"<size>";
                NSString *message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:imageSizeStr];
                
                // Backgroundcolor of toast.
                NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
                
                // textColor of toast
                NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                [self makeToast:message duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
            }

        }];
    }
    
    if(!imageData){
        UIImage *tempImage = [info objectForKey:UIImagePickerControllerOriginalImage];
        UIImage * pickedImage = [self compressImage:tempImage];
        NSString *imageTypeStr=NSLocalizedStringFromTableInBundle(@"application_upload_image_type",@"GeneralSettings",[NSBundle mainBundle], nil);
        
        // Convert an base64 Encoding Value.
        if ([[imageTypeStr uppercaseString] isEqualToString:@"JPEG"]) {
            imageData = UIImageJPEGRepresentation(pickedImage, 0.2);
        }
        else if ([imageTypeStr isEqualToString:@"PNG"]) {
            imageData = UIImagePNGRepresentation(pickedImage);
        }        NSString *base64ImageData = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        if(base64ImageData){
            //            NSLog(@"Setting last obtained image: %@", base64ImageData);
            NSDictionary *imageDict = [NSDictionary dictionaryWithObject:base64ImageData forKey:@"base64ImageData"];
            [[NSUserDefaults standardUserDefaults] setObject:base64ImageData forKey:@"PickerImage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"PickerViewObtainedImage" object:self userInfo:imageDict];
        }
    }

    
}


-(UIImage *)compressImage:(UIImage *)image{
    
    NSData *imgData = UIImageJPEGRepresentation(image, 1); //1 it represents the quality of the image.
    NSLog(@"Size of Image(bytes):%ld",(unsigned long)[imgData length]);
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    NSLog(@"Size of Image(bytes):%ld",(unsigned long)[imageData length]);
    
    return [UIImage imageWithData:imageData];
}


#pragma mark UIAction Sheet Delegate
/**
 * This method is used to set delegate Of Action Sheet.
 */
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex)
    {
        case 0:
            [self scanPicture:1];
            break;
        case 1:
            [self scanPicture:2];
            break;
        default:
            break;
    }
}

#pragma mark - Select camera Source Type.
/**
 * This method is used to set Selected camera Source type.
 */
- (void)scanPicture : (int)type
{
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.allowsEditing = YES;
    controller.delegate = self;
    if(type == 1)
    {
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
            controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeCamera];
            controller.mediaTypes = [NSArray arrayWithObject:(NSString*)kUTTypeImage];
            [self presentImageController:controller];
        }
        else
        {
//            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_consumer_client", nil)]] message:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_no_camera_found_in_this_device", nil)]] delegate:nil cancelButtonTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancel", nil)] otherButtonTitles:nil, nil];
//            [alert show];
            
            
            UIAlertController *alert= [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_consumer_client", nil)]] message:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_no_camera_found_in_this_device", nil)]] preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancel", nil)] style:UIAlertActionStyleCancel handler:nil]];
                    
            UIViewController *topController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
            while (topController.presentedViewController)
            {
                topController = topController.presentedViewController;
            }
            [topController presentViewController:alert animated:NO completion:nil];
            
        }
    }
    else if (type == 2)
    {
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary])
        {
            controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
            controller.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
            [self presentImageController:controller];
        }
        else if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeSavedPhotosAlbum])
        {
            controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeSavedPhotosAlbum];
            controller.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
            [self presentImageController:controller];
         }
    }
    NSString *str = [[NSUserDefaults standardUserDefaults] valueForKey:userLanguage];
    if ([str isEqualToString:@"Arabic"])
    {
        self.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TransformScreen" object:self userInfo:nil];
    }
}

-(void) presentImageController :(id)imageController
{
    AppDelegate *apDlgt = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    UINavigationController *navCntrl = (UINavigationController *)apDlgt.window.rootViewController;
    [navCntrl presentViewController: imageController animated: YES completion: nil];
    
}

#pragma mark - Add ctivity indicator.
/**
 * This method is used to set Add Activity indicator for View.
 */
-(void) setActivityIndicator
{
    [[[[UIApplication sharedApplication] delegate] window] addSubview:activityIndicator];
    activityIndicator.hidden = NO;
    [activityIndicator startActivityIndicator];
}
#pragma mark - remove ctivity indicator.
/**
 * This method is used to set Activity indicator Remove from View.
 */
-(void) removeActivityIndicator
{
    [activityIndicator removeFromSuperview];
    [activityIndicator stopActivityIndicator];
}

@end
