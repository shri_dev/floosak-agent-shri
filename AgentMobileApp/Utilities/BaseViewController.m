//
//  BaseViewController.m
//  Consumer Client
//
//  Created by android on 7/23/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//


#import "BaseViewController.h"
#import "AppDelegate.h"
#import "UIView+Toast.h"
#import "Reachability.h"
#import "Template.h"
#import "Constants.h"
#import "NotificationConstants.h"
#import "DatabaseConstatants.h"
#import "WebSericeUtils.h"
#import "WebServiceConstants.h"
#import "WebServiceRequestFormation.h"
#import "WebServiceRunning.h"
#import "WebServiceDataObject.h"
#import "ValidationsClass.h"
#import "ParentTemplate1.h"
#import "ParentTemplate2.h"
#import "ParentTemplate3.h"
#import "ParentTemplate4.h"
#import "ParentTemplate5.h"
#import "ParentTemplate6.h"
#import "ParentTemplate7.h"
#import "PopUpTemplate5.h"
#import "PopUpTemplate1.h"
#import "PopUpTemplate2.h"
#import "PopUpTemplate5.h"
#import "PopUpTemplate6.h"
#import "ChildTemplate1.h"
#import "ChildTemplate9.h"
#import "ChildTemplate12.h"
#import "ChildTemplate11.h"
#import "TemplateData.h"
#import "ConvertDetailsToSrverRequestFormate.h"
#import "Localization.h"
#import <MessageUI/MessageUI.h>
#import "PushNotifiactionViewController.h"
#import <Messages/Messages.h>
#import "CustomTextField.h"
//#import "Log.h"

@interface BaseViewController () <MFMessageComposeViewControllerDelegate,ABPeoplePickerNavigationControllerDelegate,UINavigationControllerDelegate>

@property(strong,nonatomic) NSArray *propertyFilesArray;
@end

@implementation BaseViewController
{
    NSString *smsText;
    NSString *mode;
    NSString *customerPhoneNumber;
    NSMutableDictionary *smsFormatterDictionary;
    
    // Network Status
    NetworkStatus netStatus;
    Reachability *reachability;
}

@synthesize propertyFilesArray;

#pragma mark - ViewController LifeCycle method.


- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Based On action types Perform Action Types.

-(void)performAction
{
    if ([local_actionType isEqualToString:ACTION_TYPE_1])
    {
        if ([local_validationType integerValue] == 0 && (local_validationType != nil))
        {
            if ( local_ContentArray )
            {
                UIScrollView *scrollView;
                if ([local_id subviews] > 0)
                {
                    for (int i=0; i<[[local_id subviews] count]; i++)
                    {
                        if ([[[local_id subviews] objectAtIndex:i] isKindOfClass:[UIScrollView class]]) {
                            scrollView = [[local_id subviews] objectAtIndex:i];
                        }
                    }
                    NSMutableArray *array = [[NSMutableArray alloc] init];
                    int count = 1;
                    for (int j = 0; j < [[scrollView subviews] count]; j++) {
                        if ([[[scrollView subviews] objectAtIndex:j] isKindOfClass:[UITextField class]])
                        {
                            CustomTextField *textField = [[scrollView subviews] objectAtIndex:j];
                            NSString *fieldValue = textField.text;
                            
                            if (fieldValue)
                                [array addObject:fieldValue];
                            else
                                [array addObject:@""];
                        }
                        else if ([[[scrollView subviews] objectAtIndex:j] isKindOfClass:[UIButton class]])
                        {
                            UIButton *buttonText = [[scrollView subviews] objectAtIndex:j];
                            NSString *buttonTextValue = buttonText.titleLabel.text;
                            NSString *keyVal = nil;
                            if ([local_currentClass isEqualToString:PARENT_TEMPLATE_4]) {
                                keyVal = [NSString stringWithFormat:@"parent_template4_drop_down_value%ld_hint",(long)buttonText.tag];
                            }
                            
                            if ([local_currentClass isEqualToString:CHILD_TEMPLATE_1]) {
                                keyVal = [NSString stringWithFormat:@"child_template1_drop_down_value%ld_hint",(long)buttonText.tag];
                            }
                            
                            if ([local_currentClass isEqualToString:CHILD_TEMPLATE_12]) {
                                keyVal = [NSString stringWithFormat:@"child_template12_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            if ([local_currentClass isEqualToString:POPUP_TEMPLATE_1]) {
                                keyVal = [NSString stringWithFormat:@"popup_template1_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            if ([local_currentClass isEqualToString:POPUP_TEMPLATE_6]) {
                                keyVal = [NSString stringWithFormat:@"popup_template6_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            if ([local_currentClass isEqualToString:CHILD_TEMPLATE_11]) {
                                keyVal = [NSString stringWithFormat:@"child_template11_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(keyVal,local_currentTemplatePropertyFileName,[NSBundle mainBundle], nil)];
                            if ([buttonTextValue isEqualToString:buttonStr])
                            {
                                [array addObject:@""];
                            }
                            else
                            {
                                [array addObject:buttonTextValue];
                            }
                            count ++;
                        }
                    }
                    for (int i = 0; i < array.count; i++) {
                        [[local_ContentArray objectAtIndex:i] setObject:[array objectAtIndex:i] forKey:@"value"];
                    }
                }
                else
                {
                    for (int i=0; i<[local_ContentArray count]; i++)
                    {
                        if ([[local_id viewWithTag:i+100] isKindOfClass:[UITextField class]])
                        {
                            CustomTextField *textField = (CustomTextField *)[local_id viewWithTag:i+100];
                            NSString *fieldValue = textField.text;
                            [[local_ContentArray objectAtIndex:i] setObject:fieldValue forKey:@"value"];
                        }
                    }
                }
            }
            NSString *errorMessage  = [ValidationsClass validateInputs:local_ContentArray];
            if (![errorMessage isEqualToString:@""])
            {
                if([local_alertType compare:TICKER_TEXT] == NSOrderedSame)
                {
                    NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
                    
                    NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    [[[local_id superview] superview] makeToast:errorMessage duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
                }
                else if ([local_alertType compare:POPUP_TEXT] == NSOrderedSame)
                {
                    PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]]  andMessage:errorMessage withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                    
                    [[[local_id superview] superview] addSubview:popup];
                }
            }
            else
            {
                webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
                if (local_dataDictionary)
                {
                    webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                }
                
                for (int i=0; i<[local_ContentArray count]; i++)
                {
                    //MPIN Validation
                    if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                    {
                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                    }
                    else
                        if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                        {
                            [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                        }
                        else
                            if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                            {
                                [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                }
                
                if ([local_processorCode isEqualToString:PROCESSOR_CODE_IMAGE_CROP]) {
                    [basetarget performSelectorOnMainThread:baseSelector withObject:nil waitUntilDone:YES];
                }
                else
                    if ([local_processorCode isEqualToString:PROCESSOR_CODE_PAY_MERCHANT_QRCODE] || [local_processorCode isEqualToString:PROCESSOR_CODE_PAY_MERCHANT_BARCODE])
                    {
                        [basetarget performSelectorOnMainThread:baseSelector withObject:nil waitUntilDone:YES];
                    }
                    else
                        if ([local_processorCode isEqualToString:PROCESSOR_CODE_ACTIVATION]) {
                            
                            Class nextClass = NSClassFromString(local_nextTemplate);
                            id object = nil;
                            
                            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                            {
                                if (local_selectedIndex >=0)
                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails?webServiceRequestInputDetails:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                else
                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails?webServiceRequestInputDetails:local_dataDictionary withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                [[local_id superview] addSubview:(UIView *)object];
                                
                            }
                            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                            {
                                object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:local_processorCode?local_processorCode:nil dataArray:nil dataDictionary:webServiceRequestInputDetails?webServiceRequestInputDetails:nil];
                                [self.navigationController pushViewController:object animated:NO];
                            }
                        }
                        else
                            if ([local_processorCode isEqualToString:PROCESSOR_CODE_ACTIVATION_T3]) {
                                
                                Class nextClass = NSClassFromString(local_nextTemplate);
                                id object = nil;
                                
                                if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                {
                                    if (local_selectedIndex >=0)
                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails?webServiceRequestInputDetails:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                    else
                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails?webServiceRequestInputDetails:local_dataDictionary withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                    [[local_id superview] addSubview:(UIView *)object];
                                    
                                }
                                else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                {
                                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:local_processorCode?local_processorCode:nil dataArray:nil dataDictionary:webServiceRequestInputDetails?webServiceRequestInputDetails:nil];
                                    [self.navigationController pushViewController:object animated:NO];
                                }
                            }
                            else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CASH_IN_NONSEAMLESS])
                            {
                                Class nextClass = NSClassFromString(local_nextTemplate);
                                id object = nil;
                                if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                {
                                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:nil  withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:webServiceRequestInputDetails?webServiceRequestInputDetails:nil];
                                    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                    UINavigationController *navController = (UINavigationController *)delegate.window.rootViewController;
                                    [navController pushViewController:object animated:NO];
                                }
                                else if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                {
                                    if (local_selectedIndex >=0)
                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                    else
                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                    
                                    [self.view addSubview:(UIView *)object];
                                }
                            }
                            else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CASH_IN_SEAMLESS])
                            {
                                [self removeViewsWithClassType:YES];
                                Class nextClass = NSClassFromString(local_nextTemplate);
                                id object = nil;
                                
                                if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                {
                                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:nil  withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                    UINavigationController *navController = (UINavigationController *)delegate.window.rootViewController;
                                    [navController pushViewController:object animated:NO];
                                }
                                
                                else if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                {
                                    if (local_selectedIndex >=0)
                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                    else
                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                    [self.view addSubview:(UIView *)object];
                                }
                            }
                            else
                            {
                                Class nextClass = NSClassFromString(local_nextTemplate);
                                id object = nil;
                                
                                if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                {
                                    if (local_selectedIndex >=0)
                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                    else
                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                    
                                    [self.view addSubview:(UIView *)object];
                                    
                                }
                                else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                {
                                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:local_processorCode?local_processorCode:nil dataArray:nil dataDictionary:webServiceRequestInputDetails?webServiceRequestInputDetails:nil];
                                    [self.navigationController pushViewController:object animated:NO];
                                }
                            }
            }
        }
        else if ([local_validationType integerValue] == 1)
        {
            
        }
        else
        {
            
        }
    }
    else if ([local_actionType isEqualToString:ACTION_TYPE_2])
    {
        if ([local_validationType integerValue] == 0 && (local_validationType != nil))
        {
            if ( local_ContentArray )
            {
                UIScrollView *scrollView;
                if ([local_id subviews] > 0)
                {
                    for (int i=0; i<[[local_id subviews] count]; i++)
                    {
                        if ([[[local_id subviews] objectAtIndex:i] isKindOfClass:[UIScrollView class]]) {
                            scrollView = [[local_id subviews] objectAtIndex:i];
                        }
                    }
                    
                    NSMutableArray *array = [[NSMutableArray alloc] init];
                    int count = 1;
                    
                    for (int j = 0; j < [[scrollView subviews] count]; j++) {
                        if ([[[scrollView subviews] objectAtIndex:j] isKindOfClass:[UITextField class]])
                        {
                            CustomTextField *textField = [[scrollView subviews] objectAtIndex:j];
                            NSString *fieldValue = textField.text;
                            
                            if (fieldValue)
                            {
                                [array addObject:fieldValue];
                            }
                            else
                                [array addObject:@""];
                        }
                        else if ([[[scrollView subviews] objectAtIndex:j] isKindOfClass:[UIButton class]])
                        {
                            UIButton *buttonText = [[scrollView subviews] objectAtIndex:j];
                            NSString *buttonTextValue = buttonText.titleLabel.text;
                            NSString *keyVal = nil;
                            if ([local_currentClass isEqualToString:PARENT_TEMPLATE_4]) {
                                keyVal = [NSString stringWithFormat:@"parent_template4_drop_down_value%ld_hint",(long)buttonText.tag];
                            }
                            
                            if ([local_currentClass isEqualToString:CHILD_TEMPLATE_1]) {
                                keyVal = [NSString stringWithFormat:@"child_template1_drop_down_value%ld_hint",(long)buttonText.tag];
                            }
                            
                            if ([local_currentClass isEqualToString:CHILD_TEMPLATE_12]) {
                                keyVal = [NSString stringWithFormat:@"child_template12_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            if ([local_currentClass isEqualToString:POPUP_TEMPLATE_1]) {
                                keyVal = [NSString stringWithFormat:@"popup_template1_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            if ([local_currentClass isEqualToString:POPUP_TEMPLATE_6]) {
                                keyVal = [NSString stringWithFormat:@"popup_template6_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            if ([local_currentClass isEqualToString:CHILD_TEMPLATE_11]) {
                                keyVal = [NSString stringWithFormat:@"child_template11_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            
                            NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(keyVal,local_currentTemplatePropertyFileName,[NSBundle mainBundle], nil)];
                            if ([buttonTextValue isEqualToString:buttonStr])
                            {
                                [array addObject:@""];
                            }
                            else
                            {
                                [array addObject:buttonTextValue];
                            }
                            count ++;
                        }
                    }
                    for (int i = 0; i < array.count; i++) {
                        [[local_ContentArray objectAtIndex:i] setObject:[array objectAtIndex:i] forKey:@"value"];
                    }
                }
                else
                {
                    for (int i=0; i<[local_ContentArray count]; i++)
                    {
                        if ([[local_id viewWithTag:i+100] isKindOfClass:[UITextField class]])
                        {
                            CustomTextField *textField = (CustomTextField *)[local_id viewWithTag:i+100];
                            NSString *fieldValue = textField.text;
                            [[local_ContentArray objectAtIndex:i] setObject:fieldValue forKey:@"value"];
                        }
                    }
                }
            }
            NSString *errorMessage  = [ValidationsClass validateInputs:local_ContentArray];
            
            if (![errorMessage isEqualToString:@""])
            {
                if([local_alertType compare:TICKER_TEXT] == NSOrderedSame)
                {
                    NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
                    
                    NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    [[[local_id superview] superview] makeToast:errorMessage duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
                }
                else if ([local_alertType compare:POPUP_TEXT] == NSOrderedSame)
                {
                    PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]]  andMessage:errorMessage withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                    [[[local_id superview] superview] addSubview:popup];
                }
                else
                {
                }
            }
            else
            {
                webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
                
                if (local_dataDictionary)
                {
                    webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                }
                
                for (int i=0; i<[local_ContentArray count]; i++)
                {
                    
                    if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                    {
                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                    }
                    else
                        if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                        {
                            [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                        }
                        else
                            if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                            {
                                [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                }
                NSLog(@"%@",webServiceRequestInputDetails.description);
                if ([local_processorCode isEqualToString:PROCESSOR_CODE_FETCH_FEE])
                {
                    if ([local_dataDictionary objectForKey:PARAMETER9])
                        [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:PARAMETER9] forKey:PARAMETER9];
                    else if ([local_dataDictionary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER])
                        [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER] forKey:PARAMETER9];
                    
                    if ([local_dataDictionary objectForKey:PARAMETER6])
                        [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:PARAMETER6] forKey:PARAMETER6];
                    
                    if ([local_apiType isEqualToString:PARAMETER_P2P] || [local_apiType isEqualToString:PARAMETER_TOPUP] || [local_apiType isEqualToString:PARAMETER_DTH]  || [local_apiType isEqualToString:PARAMETER_BCASHOUT] || [local_apiType isEqualToString:PARAMETER_WALLET_TO_BANK])
                    {
                        [webServiceRequestInputDetails setObject:local_apiType forKey:PARAMETER19];
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else
                        if ([local_apiType isEqualToString:PARAMETER_OBOANY] || [local_apiType isEqualToString:PARAMETER_NP2P] || [local_apiType isEqualToString:PARAMETER_PAY_TO_MERCHANT]) {
                            [webServiceRequestInputDetails setObject:local_apiType forKey:PARAMETER19];
                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                        }
                        else if ([local_apiType isEqualToString:PARAMETER_BILL_PAY])
                        {
                            if (local_labelValidationArray)
                            {
                                for (int i=0; i<[local_labelValidationArray count]; i++)
                                {
                                    if ([[local_id viewWithTag:i+100] isKindOfClass:[UILabel class]]) {
                                        UILabel *textField = (UILabel *)[local_id viewWithTag:i+100];
                                        NSString *fieldValue = textField.text;
                                        
                                        if (fieldValue)
                                            [[local_labelValidationArray objectAtIndex:i] setObject:fieldValue forKey:@"value"];
                                        else
                                            [[local_labelValidationArray objectAtIndex:i] setObject:@"" forKey:@"value"];
                                        
                                        [local_ContentArray addObject:[local_labelValidationArray objectAtIndex:i]];
                                    }
                                }
                            }
                            //susmit
                            if(local_dataDictionary)
                            {
                                webServiceRequestInputDetails = [[NSMutableDictionary alloc]initWithDictionary:local_dataDictionary];
                            }
                            
                            for (int i=0; i<[local_ContentArray count]; i++)
                            {
                                if (![[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] isEqualToString:application_default_no_value_available])
                                {
                                    //susmit
                                    //                                if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    //                                {
                                    //                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    //                                }
                                    //                                else
                                    //                                {
                                    [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    //                                }
                                }
                                //susmit
                            }
                            
                            [webServiceRequestInputDetails setObject:local_apiType forKey:PARAMETER19];
                            if ([webServiceRequestInputDetails objectForKey:PARAMETER21]) {
                                [local_dataDictionary setObject:[webServiceRequestInputDetails objectForKey:PARAMETER21] forKey:PARAMETER21];
                            }
                            ConvertDetailsToSrverRequestFormate *tempConversion = [[ConvertDetailsToSrverRequestFormate alloc] init];
                            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:[tempConversion convertTheFetchFeeValues:webServiceRequestInputDetails]];
                            [webServiceRequestInputDetails addEntriesFromDictionary:tempDic];
                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                        }
                        else
                        {
                            [webServiceRequestInputDetails setObject:local_apiType forKey:PARAMETER19];
                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                        }
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_FETCH_FEE_T3])
                {
                    ConvertDetailsToSrverRequestFormate *tempConversion = [[ConvertDetailsToSrverRequestFormate alloc] init];
                    NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:[tempConversion convertTheCASHINValues:webServiceRequestInputDetails]];
                    [webServiceRequestInputDetails addEntriesFromDictionary:tempDic];
                    if ([local_apiType isEqualToString:PARAMETER_TOPUP_T3] || [local_apiType isEqualToString:PARAMETER_CASH_IN]|| [local_apiType isEqualToString:PARAMETER_DTH_T3] || [local_apiType isEqualToString:PARAMETER_CASH_OUT_T3] || [local_apiType isEqualToString:PARAMETER_VIRAL_CASH_OUT_T3] || [local_apiType isEqualToString:PARAMETER_PAYBILL_T3] || [local_apiType isEqualToString:PARAMETER_BANK_UNLOAD_T3] || [local_apiType isEqualToString:PARAMETER_BANK_LOAD_T3] || [local_apiType isEqualToString:PARAMETER_BANK_UNLOAD_OTHERS_T3])
                    {
                        if ([local_apiType length]>0)
                            [webServiceRequestInputDetails setObject:local_apiType forKey:PARAMETER18];
                    }
                    
                    [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    
                }
                else
                    if([local_processorCode isEqualToString:PROCESSOR_CODE_PAYU_LOAD_AUTHENTICATE])
                    {
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else
                        if ([local_processorCode isEqualToString:PROCESSOR_CODE_BANK])
                        {
                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                        }
                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_P2P_REGISTERED_BENEFICIARY_MOBILE_NUMBER])
                        {
                            if (![local_apiType isEqualToString:@""])
                                [webServiceRequestInputDetails setObject:local_apiType forKey:PARAMETER19];
                            
                            ConvertDetailsToSrverRequestFormate *tempCOn = [[ConvertDetailsToSrverRequestFormate alloc] init];
                            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:[tempCOn convertTheP2PValues:webServiceRequestInputDetails]];
                            [self commonWebServiceCall:tempDic withProcessorCode:local_processorCode];
                        }
                        else if([local_processorCode isEqualToString:PROCESSOR_CODE_AGENT_DETAILS])
                        {
                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                        }
                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_FETCH_IFSC])
                        {
                            [webServiceRequestInputDetails addEntriesFromDictionary:local_dataDictionary];
                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                        }
                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CAHNGE_PIN])
                        {
                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                        }
                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CHANGE_MPIN_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_CHANGE_MPIN_T3])
                        {
                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                        }
                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_FORGOT_MPIN_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_FORGOT_MPIN_T3])
                        {
                            [self netConnectionStatusMessage];
                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                        }
                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_FETCH_BILLER_ALL])
                        {
                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                        }
                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_PRODUCT_DETAILS])
                        {
                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                        }
                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_RESET_PIN_FORGOT_MPIN])
                        {
                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                        }
                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_SELF_REG])
                        {
                            if ([local_dataDictionary objectForKey:PARAMETER10])
                                [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:PARAMETER10] forKey:PARAMETER10];
                            
                            if ([local_dataDictionary objectForKey:PARAMETER20])
                                [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:PARAMETER20] forKey:PARAMETER20];
                            
                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                        }
                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_SELF_REG_T3])
                        {
                            NSLog(@"local Data Dictionary...%@",local_dataDictionary);
                            
                            if ([local_dataDictionary objectForKey:PARAMETER10])
                                [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:PARAMETER10] forKey:PARAMETER10];
                            
                            
                            
                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                        }
                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_SELF_REG_DIGITAL_KYC]) {
                            if ([local_dataDictionary objectForKey:PARAMETER19]) {
                                [webServiceRequestInputDetails addEntriesFromDictionary:local_dataDictionary];
                                [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                            }
                            else
                            {
                                [self showAlertForErrorWithMessage:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10015", nil)]] andErrorCode:nil];
                            }
                        }
                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CHECK_MY_VELOCITY])
                        {
                            NSMutableDictionary *selfRegisDataDict = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                            
                            for (int i=0; i<[local_ContentArray count]; i++)
                            {
                                if (![[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"] isEqualToString:PARAMETER19] && ![[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"] isEqualToString:PARAMETER20])
                                {
                                    //susmit
                                    //                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    //                            {
                                    //                                [selfRegisDataDict setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    //                            }
                                    //                            else
                                    //                            {
                                    [selfRegisDataDict setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    //                            }
                                    //susmit
                                }
                            }
                            [self commonWebServiceCall:selfRegisDataDict withProcessorCode:local_processorCode];
                        }
                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CHECK_VELOCITY_LIMITS_T3])
                        {
                            NSMutableDictionary *selfRegisDataDict = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                            
                            for (int i=0; i<[local_ContentArray count]; i++)
                            {
                                if (![[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"] isEqualToString:PARAMETER19] && ![[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"] isEqualToString:PARAMETER20])
                                {
                                    
                                    if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [selfRegisDataDict setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                                    else
                                    {
                                        [selfRegisDataDict setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                                    
                                }
                            }
                            [self commonWebServiceCall:selfRegisDataDict withProcessorCode:local_processorCode];
                        }
                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_TRANSACTION_STATUS_T3])
                        {
                            //                            webServiceRequestInputDetails=[[NSMutableDictionary alloc]initWithDictionary:local_dataDictionary];
                            
                            NSLog(@"Webservice utilities ..%@",webServiceRequestInputDetails);
                            
                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                        }
                        else
                            if ([local_selected_processorCode isEqualToString:PROCESSOR_CODE_PENDING_BILLS])
                            {
                                
                                if ([[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID] || [[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_FIXED])
                                {
                                    webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                                    [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:BILLERNICKNAME] forKey:PARAMETER18];
                                    [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                                }
                                else if ([[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID])
                                {
                                    if ([local_dataDictionary objectForKey:BILLERNICKNAME])
                                    {
                                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                                        [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:BILLERNICKNAME] forKey:PARAMETER18];
                                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                                    }
                                    else
                                    {
                                        Class myclass = NSClassFromString(local_nextTemplate);
                                        id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_currentTemplatePropertyFileName andDelegate:local_id withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:local_selectedIndex withDataArray:local_ContentArray withSubIndex:0];
                                        [[[local_id superview] superview] addSubview:(UIView *)obj];
                                    }
                                }
                                else if ([[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_VARIABLE])
                                {
                                    if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""]){
                                        Class nextClass = NSClassFromString(local_nextTemplate);
                                        id object = nil;
                                        
                                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                        {
                                            if (local_selectedIndex >=0)
                                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_currentTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:((int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                            
                                            else
                                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                            
                                            [[[local_id superview] superview] addSubview:(UIView *)object];
                                        }
                                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                        {
                                            object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                            [self.navigationController pushViewController:object animated:NO];
                                        }
                                    }
                                    
                                }
                            }
                            else if ([local_selected_processorCode isEqualToString:PROCESSOR_CODE_FETCH_MY_BILLERS])
                            {
                                if ([[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID] || [[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_FIXED])
                                {
                                    webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                                    [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:BILLERNICKNAME] forKey:PARAMETER18];
                                    [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                                }
                                else if ([[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_VARIABLE])
                                {
                                    if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""]){
                                        Class nextClass = NSClassFromString(local_nextTemplate);
                                        id object = nil;
                                        
                                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)]){
                                            if (local_selectedIndex >=0)
                                                object =[[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName  andDelegate:local_id withDataDictionary:local_dataDictionary withProcessorCode:local_processorCode withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                            else
                                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                            [[[local_id superview] superview] addSubview:(UIView *)object];
                                        }
                                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                        {
                                            object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                            [self.navigationController pushViewController:object animated:NO];
                                        }
                                    }
                                }
                            }
                            else if ([local_processorCode isEqualToString:PROCESSOR_CODE_ACTIVATION])
                            {
                                [self commonWebServiceCall:(NSMutableDictionary *)local_dataDictionary withProcessorCode:local_processorCode];
                                
                            }
                            else if ([local_processorCode isEqualToString:PROCESSOR_CODE_LOGIN_T3])
                            {
                                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                                NSMutableDictionary *userDetails = [[NSMutableDictionary alloc] init];
                                
                                NSString *smsMode = NSLocalizedStringFromTableInBundle(@"application_deafault_communication_mode",@"GeneralSettings",[NSBundle mainBundle], nil);
                                reachability = [Reachability reachabilityForInternetConnection];
                                netStatus = [reachability  currentReachabilityStatus];
                                if ([smsMode isEqualToString:@"SMS"] || (netStatus == NotReachable)) {
                                    [userDetails setObject:smsMode forKey:PARAMETER36];
                                    [userDetails setObject:[webServiceRequestInputDetails objectForKey:PARAMETER6] forKey:PARAMETER6];
                                    [userDetails setObject:[webServiceRequestInputDetails objectForKey:PARAMETER7] forKey:PARAMETER7];
                                    if ([webServiceRequestInputDetails objectForKey:PARAMETER34])
                                    {
                                        [userDetails setObject:[webServiceRequestInputDetails objectForKey:PARAMETER34] forKey:PARAMETER34];
                                    }
                                    [userDefaults setObject:userDetails forKey:@"userDetails"];
                                    [userDefaults synchronize];
                                }
                                
                                [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                                
                                //                                 int userLevel = (int)[NSLocalizedStringFromTableInBundle(@"application_default_user_support_mode",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
                                //
                                //                                if ([[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER]length]==0 && userLevel==1)
                                //                                {
                                //                                    local_actionType=ACTION_TYPE_1;
                                //                                    local_nextTemplate=POPUP_TEMPLATE_2;
                                //                                    local_nextTemplatePropertyFileName=UPDATE_USER_MOBILE_NUMBER_T3;
                                //                                    Class nextClass=NSClassFromString(local_nextTemplate);
                                //                                    id object = nil;
                                //                                    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                //                                    {
                                //                                        if (local_selectedIndex >=0)
                                //                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                //                                        else
                                //                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:local_ContentArray withSubIndex:0];
                                //
                                //                                        [self.view addSubview:(UIView *)object];
                                //                                    }
                                //                                    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                //                                    {
                                //                                        object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_nextTemplatePropertyFileName withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                //                                        [self.navigationController pushViewController:object animated:NO];
                                //                                    }
                                //                                }
                                //                                else
                                //                                [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                            }
                            else if ([local_processorCode isEqualToString:PROCESSOR_CODE_ACTIVATION_T3])
                            {
                                [self netConnectionStatusMessage];
                                [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                            }
                            else if ([local_processorCode isEqualToString:PROCESSOR_CODE_PULL_ACCEPT_REQUEST_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_PULL_ACCEPT_REQUEST_T3])
                            {
                                [self netConnectionStatusMessage];
                                webServiceRequestInputDetails=[[NSMutableDictionary alloc]initWithDictionary:local_dataDictionary];
                                
                                if ([webServiceRequestInputDetails objectForKey:PARAMETER17] ) {
                                    [webServiceRequestInputDetails setObject:[webServiceRequestInputDetails objectForKey:PARAMETER17] forKey:PARAMETER7];
                                }
                                if ([webServiceRequestInputDetails objectForKey:@"transaction_agentId"] ) {
                                    [webServiceRequestInputDetails setObject:[[webServiceRequestInputDetails objectForKey:@"transaction_agentId"]stringValue] forKey:PARAMETER18];
                                }
                                if ([webServiceRequestInputDetails objectForKey:@"transaction_reqId"] ) {
                                    [webServiceRequestInputDetails setObject:[[webServiceRequestInputDetails objectForKey:@"transaction_reqId"] stringValue] forKey:PARAMETER19];
                                }
                                NSLog(@"Accept localData Dictionary....%@",webServiceRequestInputDetails);
                                [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                            }
                            else if ([local_processorCode isEqualToString:PROCESSOR_CODE_PULL_DECLINE_REQUEST_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_PULL_DECLINE_REQUEST_T3])
                            {
                                webServiceRequestInputDetails=[[NSMutableDictionary alloc]initWithDictionary:local_dataDictionary];
                                
                                if ([webServiceRequestInputDetails objectForKey:PARAMETER17] ) {
                                    [webServiceRequestInputDetails setObject:[webServiceRequestInputDetails objectForKey:PARAMETER17] forKey:PARAMETER7];
                                }
                                if ([webServiceRequestInputDetails objectForKey:@"transaction_agentId"] ) {
                                    [webServiceRequestInputDetails setObject:[[webServiceRequestInputDetails objectForKey:@"transaction_agentId"]stringValue] forKey:PARAMETER18];
                                }
                                if ([webServiceRequestInputDetails objectForKey:@"transaction_reqId"] ) {
                                    [webServiceRequestInputDetails setObject:[[webServiceRequestInputDetails objectForKey:@"transaction_reqId"] stringValue] forKey:PARAMETER19];
                                }
                                NSLog(@"Decline localData Dictionary....%@",webServiceRequestInputDetails);
                                [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                            }
                            else if ([[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID] || [[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_FIXED])
                            {
                                webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                                [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:BILLERNICKNAME] forKey:PARAMETER18];
                                [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                            }
                            else
                                if ([[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID])
                                {
                                    if ([local_dataDictionary objectForKey:BILLERNICKNAME])
                                    {
                                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
                                        [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:BILLERNICKNAME] forKey:PARAMETER18];
                                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                                    }
                                    else
                                    {
                                        if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""]){
                                            Class nextClass = NSClassFromString(local_nextTemplate);
                                            id object = nil;
                                            
                                            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)]){
                                                if (local_selectedIndex >=0)
                                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                else
                                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                
                                                [[[local_id superview] superview] addSubview:(UIView *)object];
                                            }
                                            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                            {
                                                object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                [self.navigationController pushViewController:object animated:NO];
                                            }
                                        }
                                    }
                                }
                                else
                                    if ([[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_VARIABLE])
                                    {
                                        if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""]){
                                            Class nextClass = NSClassFromString(local_nextTemplate);
                                            id object = nil;
                                            
                                            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)]){
                                                if (local_selectedIndex >=0)
                                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                else
                                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                
                                                [[[local_id superview] superview] addSubview:(UIView *)object];
                                                
                                            }
                                            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                            {
                                                object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                [self.navigationController pushViewController:object animated:NO];
                                            }
                                        }
                                    }
                                    else
                                        if (local_processorCode) {
                                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                                        }
                                        else
                                        {
                                            if ([local_id isKindOfClass:[UIViewController class]])
                                            {
                                                if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""]){
                                                    Class nextClass = NSClassFromString(local_nextTemplate);
                                                    id object = nil;
                                                    
                                                    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)]){
                                                        if (local_selectedIndex >=0)
                                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                        else
                                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                        [[local_id superview] addSubview:(UIView *)object];
                                                    }
                                                    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                                    {
                                                        if ([nextClass isSubclassOfClass:[UIViewController class]]){
                                                            object = [[NSClassFromString(local_nextTemplate) alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                            [self.navigationController pushViewController:object animated:NO];
                                                        }
                                                    }
                                                }
                                            }
                                            else if ([local_id isKindOfClass:[UIView class]])
                                            {
                                                if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""]){
                                                    Class nextClass = NSClassFromString(local_nextTemplate);
                                                    id object = nil;
                                                    
                                                    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)]){
                                                        if (local_selectedIndex >=0)
                                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:((int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                        else
                                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                        
                                                        [[local_id superview] addSubview:(UIView *)object];
                                                    }
                                                    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                                    {
                                                        if ([nextClass isSubclassOfClass:[UIViewController class]])
                                                        {
                                                            BaseViewController *object = [[NSClassFromString(local_nextTemplate) alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                            if (self.topController == nil)
                                                            {
                                                                object.topController = self;
                                                                
                                                            }
                                                            else
                                                                object.topController = self.topController;
                                                            
                                                            [self.navigationController pushViewController:object animated:NO];
                                                        }
                                                    }
                                                }
                                            }
                                        }
            }
        }
        else if([local_validationType integerValue] == 1 && (local_validationType != nil))
        {
            if ( local_ContentArray )
            {
                for (int i=0; i<[local_ContentArray count]; i++)
                {
                    if ([[local_id viewWithTag:i+100] isKindOfClass:[UITextField class]]) {
                        CustomTextField *textField = (CustomTextField *)[local_id viewWithTag:i+100];
                        NSString *fieldValue = textField.text;
                        [[local_ContentArray objectAtIndex:i] setObject:fieldValue forKey:@"value"];
                    }
                }
            }
        }
        else
        {
            if ([local_selected_processorCode isEqualToString:PROCESSOR_CODE_PENDING_BILLS])
            {
                if ([[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID] || [[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_FIXED])
                {
                    webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                    [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:BILLERNICKNAME] forKey:PARAMETER18];
                    [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                }
                else if ([[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID])
                {
                    if ([local_dataDictionary objectForKey:BILLERNICKNAME])
                    {
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                        [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:BILLERNICKNAME] forKey:PARAMETER18];
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else
                    {
                        Class nextClass = NSClassFromString(local_nextTemplate);
                        id object = nil;
                        
                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)]){
                            if (local_selectedIndex >=0)
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:((int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                            else
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                            
                            [[[local_id superview] superview] addSubview:(UIView *)object];
                        }
                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                        {
                            if ([nextClass isSubclassOfClass:[UIViewController class]])
                            {
                                BaseViewController *object = [[NSClassFromString(local_nextTemplate) alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                if (self.topController == nil)
                                {
                                    object.topController = self;
                                    
                                }
                                else
                                    object.topController = self.topController;
                                
                                [self.navigationController pushViewController:object animated:NO];
                            }
                        }
                    }
                }
                else if ([[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_VARIABLE])
                {
                    Class nextClass = NSClassFromString(local_nextTemplate);
                    id object = nil;
                    
                    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)]){
                        if (local_selectedIndex >=0)
                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                        else
                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                        
                        [[[local_id superview] superview] addSubview:(UIView *)object];
                    }
                    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                    {
                        if ([nextClass isSubclassOfClass:[UIViewController class]])
                        {
                            BaseViewController *object = [[NSClassFromString(local_nextTemplate) alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                            if (self.topController == nil)
                            {
                                object.topController = self;
                                
                            }
                            else
                                object.topController = self.topController;
                            
                            [self.navigationController pushViewController:object animated:NO];
                        }
                    }
                }
            }
            else if ([local_selected_processorCode isEqualToString:PROCESSOR_CODE_FETCH_MY_BILLERS])
            {
                if ([[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID] || [[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_FIXED])
                {
                    webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                    [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:BILLERNICKNAME] forKey:PARAMETER18];
                    [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                }
                else if ([[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_VARIABLE])
                {
                    Class nextClass = NSClassFromString(local_nextTemplate);
                    id object = nil;
                    
                    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)]){
                        if (local_selectedIndex >=0)
                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                        else
                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                        
                        [[[local_id superview] superview] addSubview:(UIView *)object];
                    }
                    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                    {
                        if ([nextClass isSubclassOfClass:[UIViewController class]])
                        {
                            BaseViewController *object = [[NSClassFromString(local_nextTemplate) alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                            if (self.topController == nil)
                            {
                                object.topController = self;
                                
                            }
                            else
                                object.topController = self.topController;
                            
                            [self.navigationController pushViewController:object animated:NO];
                        }
                    }
                }
            }
            else if ([local_processorCode isEqualToString:PROCESSOR_CODE_ACTIVATION]) {
                NSMutableDictionary *webserviceDict = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                [self commonWebServiceCall:webserviceDict withProcessorCode:local_processorCode];
            }
            else if ([local_processorCode isEqualToString:PROCESSOR_CODE_ACTIVATION_T3]) {
                NSMutableDictionary *webserviceDict = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                [self commonWebServiceCall:webserviceDict withProcessorCode:local_processorCode];
            }
            else if ([[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID] || [[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_FIXED])
            {
                webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:BILLERNICKNAME] forKey:PARAMETER18];
                [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
            }
            else if ([[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID])
            {
                if ([local_dataDictionary objectForKey:BILLERNICKNAME])
                {
                    webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
                    [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:BILLERNICKNAME] forKey:PARAMETER18];
                    [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                }
                else
                {
                    Class nextClass = NSClassFromString(local_nextTemplate);
                    id object = nil;
                    
                    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)]){
                        if (local_selectedIndex >=0)
                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                        else
                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                        
                        [[[local_id superview] superview] addSubview:(UIView *)object];
                    }
                    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                    {
                        if ([nextClass isSubclassOfClass:[UIViewController class]])
                        {
                            BaseViewController *object = [[NSClassFromString(local_nextTemplate) alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                            if (self.topController == nil)
                            {
                                object.topController = self;
                                
                            }
                            else
                                object.topController = self.topController;
                            
                            [self.navigationController pushViewController:object animated:NO];
                        }
                    }
                    
                }
            }
            else if ([[local_dataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_VARIABLE])
            {
                
                Class nextClass = NSClassFromString(local_nextTemplate);
                id object = nil;
                
                if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)]){
                    if (local_selectedIndex >=0)
                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                    else
                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                    
                    [[[local_id superview] superview] addSubview:(UIView *)object];
                }
                else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                {
                    if ([nextClass isSubclassOfClass:[UIViewController class]])
                    {
                        BaseViewController *object = [[NSClassFromString(local_nextTemplate) alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                        if (self.topController == nil)
                        {
                            object.topController = self;
                            
                        }
                        else
                            object.topController = self.topController;
                        
                        [self.navigationController pushViewController:object animated:NO];
                    }
                }
            }
        }
    }
    else if ([local_actionType isEqualToString:ACTION_TYPE_3])
    {
        if ([local_validationType integerValue] == 0 && (local_validationType != nil))
        {
            if ( local_ContentArray )
            {
                UIScrollView *scrollView;
                if ([local_id subviews] > 0)
                {
                    for (int i=0; i<[[local_id subviews] count]; i++)
                    {
                        if ([[[local_id subviews] objectAtIndex:i] isKindOfClass:[UIScrollView class]]) {
                            scrollView = [[local_id subviews] objectAtIndex:i];
                        }
                    }
                    
                    int count = 1;
                    NSMutableArray *array = [[NSMutableArray alloc] init];
                    for (int j = 0; j < [[scrollView subviews] count]; j++) {
                        if ([[[scrollView subviews] objectAtIndex:j] isKindOfClass:[UITextField class]])
                        {
                            CustomTextField *textField = [[scrollView subviews] objectAtIndex:j];
                            NSString *fieldValue = textField.text;
                            
                            if (fieldValue)
                                [array addObject:fieldValue];
                            else
                                [array addObject:@""];
                        }
                        else if ([[[scrollView subviews] objectAtIndex:j] isKindOfClass:[UIButton class]])
                        {
                            UIButton *buttonText = [[scrollView subviews] objectAtIndex:j];
                            NSString *buttonTextValue = buttonText.titleLabel.text;
                            NSString *keyVal = nil;
                            if ([local_currentClass isEqualToString:PARENT_TEMPLATE_4]) {
                                keyVal = [NSString stringWithFormat:@"parent_template4_drop_down_value%ld_hint",(long)buttonText.tag];
                            }
                            
                            if ([local_currentClass isEqualToString:CHILD_TEMPLATE_1]) {
                                keyVal = [NSString stringWithFormat:@"child_template1_drop_down_value%ld_hint",(long)buttonText.tag];
                            }
                            
                            if ([local_currentClass isEqualToString:CHILD_TEMPLATE_12]) {
                                keyVal = [NSString stringWithFormat:@"child_template12_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            if ([local_currentClass isEqualToString:POPUP_TEMPLATE_1]) {
                                keyVal = [NSString stringWithFormat:@"popup_template1_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            if ([local_currentClass isEqualToString:POPUP_TEMPLATE_6]) {
                                keyVal = [NSString stringWithFormat:@"popup_template6_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            if ([local_currentClass isEqualToString:CHILD_TEMPLATE_11]) {
                                keyVal = [NSString stringWithFormat:@"child_template11_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(keyVal,local_currentTemplatePropertyFileName,[NSBundle mainBundle], nil)];
                            if ([buttonTextValue isEqualToString:buttonStr])
                            {
                                [array addObject:@""];
                            }
                            else
                            {
                                [array addObject:buttonTextValue];
                            }
                            count ++;
                        }
                    }
                    for (int i = 0; i < array.count; i++) {
                        [[local_ContentArray objectAtIndex:i] setObject:[array objectAtIndex:i] forKey:@"value"];
                    }
                }
                else
                {
                    for (int i=0; i<[local_ContentArray count]; i++)
                    {
                        if ([[local_id viewWithTag:i+100] isKindOfClass:[UITextField class]])
                        {
                            CustomTextField *textField = (CustomTextField *)[local_id viewWithTag:i+100];
                            NSString *fieldValue = textField.text;
                            [[local_ContentArray objectAtIndex:i] setObject:fieldValue forKey:@"value"];
                        }
                    }
                }
            }
            
            NSString *errorMessage  = [ValidationsClass validateInputs:local_ContentArray];
            
            if (![errorMessage isEqualToString:@""])
            {
                if([local_alertType compare:TICKER_TEXT] == NSOrderedSame)
                {
                    NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
                    
                    NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    [[[local_id superview] superview] makeToast:errorMessage duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
                }
                else if ([local_alertType compare:POPUP_TEXT] == NSOrderedSame)
                {
                    PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]]  andMessage:errorMessage withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                    
                    [[[local_id superview] superview] addSubview:popup];
                    //                    NSLog(@"Error Message is...%@",errorMessage);
                }
                else
                {
                }
            }
            else
            {
                if ([local_processorCode isEqualToString:PROCESSOR_CODE_ADD_BILLER])
                {
                    webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                    for (int i=0; i<[local_ContentArray count]; i++)
                    {
                        if (![[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] isEqualToString:application_default_no_value_available])
                        {
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                    }
                    ConvertDetailsToSrverRequestFormate *tempConversion = [[ConvertDetailsToSrverRequestFormate alloc] init];
                    NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:[tempConversion convertTheAddBillerValues:webServiceRequestInputDetails]];
                    [self commonWebServiceCall:tempDic withProcessorCode:local_processorCode];
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG])
                {
                    webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
                    
                    for (int i=0; i<[local_ContentArray count]; i++)
                    {
                        if (![[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] isEqualToString:application_default_no_value_available])
                        {
                            //MPIN Validation
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                    }
                    [webServiceRequestInputDetails addEntriesFromDictionary:postDictionary];
                    [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG_T3])
                {
                    webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
                    for (int i=0; i<[local_ContentArray count]; i++)
                    {
                        if (![[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] isEqualToString:application_default_no_value_available])
                        {
                            //MPIN Validation
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                    }
                    [webServiceRequestInputDetails setObject:[NSString stringWithFormat:@"%@",TRANSACTION_CODE_CHANGE_LANGUAGE_T3] forKey:PARAMETER13];
                    [webServiceRequestInputDetails setObject:[NSString stringWithFormat:@"%@",PROCESSOR_CODE_CHANGE_LANG_T3] forKey:PARAMETER15];
                    [webServiceRequestInputDetails addEntriesFromDictionary:postDictionary];
                    NSLog(@"Webservicedetails are,..%@",[webServiceRequestInputDetails description]);
                    [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_PAY_BILLER_NICK_NAME])
                {
                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                    [userDefaults setObject:local_dataDictionary forKey:@"PayBillsByNickName"];
                    
                    webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                    
                    for (int i=0; i<[local_ContentArray count]; i++)
                    {
                        if (![[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] isEqualToString:application_default_no_value_available])
                        {
                            //MPIN Validation
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:PARAMETER17];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                    }
                    ConvertDetailsToSrverRequestFormate *tempConversion = [[ConvertDetailsToSrverRequestFormate alloc] init];
                    NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:[tempConversion convertThePayBillValues:webServiceRequestInputDetails]];
                    [self commonWebServiceCall:tempDic withProcessorCode:local_processorCode];
                }
                else
                    if([local_processorCode isEqualToString:PROCESSOR_CODE_CASH_OUT_WITHOUT_OTP])
                    {
                        
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
                        
                        for (int i=0; i<[local_ContentArray count]; i++)
                        {
                            if (![[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] isEqualToString:application_default_no_value_available])
                            {
                                //MPIN Validation
                                if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                                    else
                                        if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                        {
                                            [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                            
                                        }
                                        else
                                        {
                                            [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        }
                            }
                        }
                        [webServiceRequestInputDetails addEntriesFromDictionary:local_dataDictionary];
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_BALANCE_ENQUIRY] || [local_processorCode isEqualToString:PROCESSOR_CODE_MINI_STATEMENT] ||[local_processorCode isEqualToString:PROCESSOR_CODE_CHECK_BALANCE_T3] || ([local_processorCode isEqualToString:PROCESSOR_CODE_TXN_HISTORY_T3]&& [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_TXN_HISTORY_T3]) || ([local_processorCode isEqualToString:PROCESSOR_CODE_BANK_TXN_HISTORY_T3]&& [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_BANK_TXN_HISTORY_T3]))
                    {
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                        for (int i=0; i<[local_ContentArray count]; i++)
                        {
                            if (![[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] isEqualToString:application_default_no_value_available])
                            {
                                //MPIN Validation
                                if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                                    else
                                        if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                        {
                                            [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                            
                                        }
                                        else
                                        {
                                            [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        }
                            }
                        }
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CHECK_BANK_BALANCE_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_CHECK_BANK_BALANCE_T3])
                    {
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                        
                        for (int i=0; i<[local_ContentArray count]; i++)
                        {
                            if (![[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] isEqualToString:application_default_no_value_available])
                            {
                                //MPIN Validation
                                if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                                    else
                                        if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                        {
                                            [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                            
                                        }
                                        else
                                        {
                                            [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        }
                            }
                        }
                        NSLog(@"Local Data Dictionary...%@,%@",webServiceRequestInputDetails,local_dataDictionary);
                        
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_REPORTS_SUMMARY_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_REPORTS_SUMMARY_T3])
                    {
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                        
                        for (int i=0; i<[local_ContentArray count]; i++)
                        {
                            if (![[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] isEqualToString:application_default_no_value_available])
                            {
                                //MPIN Validation
                                if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                                    else
                                        if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                        {
                                            [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                            
                                        }
                                        else
                                        {
                                            [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        }
                            }
                        }
                        NSLog(@"Local Data Dictionary...%@,%@",webServiceRequestInputDetails,local_dataDictionary);
                        
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_TOP_UP_PREPAID_MOBILE_OPERATOR])
                    {
                        [self netConnectionStatusMessage];
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                        for (int i=0; i<[local_ContentArray count]; i++)
                        {
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                        
                        if ([[local_dataDictionary objectForKey:@"staticListValue"] isEqualToString:@"Mobile"] ||[[local_dataDictionary objectForKey:@"topupType"] isEqualToString:@"Mobile"]) {
                            [webServiceRequestInputDetails setObject:@"VOICE" forKey:PARAMETER20];
                        }
                        else
                        {
                            [webServiceRequestInputDetails setObject:@"DTH" forKey:PARAMETER20];
                        }
                        
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else if ([local_processorCode isEqualToString: PROCESSOR_CODE_TOP_UP_PREPAID_DTH_OPERATOR])
                    {
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                        for (int i=0; i<[local_ContentArray count]; i++)
                        {
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_P2P_REGISTERED_BENEFICIARY_MOBILE_NUMBER])
                    {
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
                        
                        if (local_dataDictionary)
                        {
                            webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                        }
                        for (int i=0; i<[local_ContentArray count]; i++)
                        {
                            //MPIN Validation
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                        
                        if (![local_apiType isEqualToString:@""])
                            [webServiceRequestInputDetails setObject:local_apiType forKey:PARAMETER19];
                        
                        ConvertDetailsToSrverRequestFormate *tempCOn = [[ConvertDetailsToSrverRequestFormate alloc] init];
                        NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:[tempCOn convertTheP2PValues:webServiceRequestInputDetails]];
                        [self commonWebServiceCall:tempDic withProcessorCode:local_processorCode];
                        
                    }
                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CUSTOMER_CASH_IN_T3] &&[[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_CUSTOMER_CASH_IN_T3])
                    {
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
                        
                        if (local_dataDictionary)
                        {
                            webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                        }
                        for (int i=0; i<[local_ContentArray count]; i++)
                        {
                            //MPIN Validation
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                        
                        if (![local_apiType isEqualToString:@""])
                            [webServiceRequestInputDetails setObject:local_apiType forKey:PARAMETER19];
                        
                        ConvertDetailsToSrverRequestFormate *tempCOn = [[ConvertDetailsToSrverRequestFormate alloc] init];
                        NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:[tempCOn convertTheP2PValues:webServiceRequestInputDetails]];
                        [self commonWebServiceCall:tempDic withProcessorCode:local_processorCode];
                        
                    }
                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_GROUPSAVING_CASH_IN_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_GROUPSAVING_CASH_IN_T3])
                    {
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
                        
                        if (local_dataDictionary)
                        {
                            webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                        }
                        for (int i=0; i<[local_ContentArray count]; i++)
                        {
                            //MPIN Validation
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                        
                        if (![local_apiType isEqualToString:@""])
                            [webServiceRequestInputDetails setObject:local_apiType forKey:PARAMETER19];
                        
                        ConvertDetailsToSrverRequestFormate *tempCOn = [[ConvertDetailsToSrverRequestFormate alloc] init];
                        NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:[tempCOn convertTheP2PValues:webServiceRequestInputDetails]];
                        [self commonWebServiceCall:tempDic withProcessorCode:local_processorCode];
                        
                    }
                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CASHOUT_REGISTERED_T3])
                    {
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
                        if (local_dataDictionary)
                        {
                            webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                        }
                        for (int i=0; i<[local_ContentArray count]; i++)
                        {
                            //MPIN Validation
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CASHOUT_VIRAL_T3])
                    {
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
                        if (local_dataDictionary)
                        {
                            webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                        }
                        for (int i=0; i<[local_ContentArray count]; i++)
                        {
                            //MPIN Validation
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_LOAD_BANK_T3])
                    {
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
                        if (local_dataDictionary)
                        {
                            webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                        }
                        for (int i=0; i<[local_ContentArray count]; i++)
                        {
                            //MPIN Validation
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                        if ([local_dataDictionary objectForKey:@"bank_name"]) {
                            [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:@"bank_name"] forKey:PARAMETER18];
                            
                        }
                        if ([local_dataDictionary objectForKey:@"bank_accno"]) {
                            [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:@"bank_accno"] forKey:PARAMETER19];
                            
                        }
                        NSLog(@"LOcal Data Dictionary is...%@,%@",local_dataDictionary,webServiceRequestInputDetails);
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_UNLOAD_LINKED_BANK_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_UNLOAD_LINKED_BANK_T3])
                    {
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
                        if (local_dataDictionary)
                        {
                            webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                        }
                        for (int i=0; i<[local_ContentArray count]; i++)
                        {
                            //MPIN Validation
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                        if ([local_dataDictionary objectForKey:@"bank_name"]) {
                            [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:@"bank_name"] forKey:PARAMETER18];
                            
                        }
                        if ([local_dataDictionary objectForKey:@"bank_accno"]) {
                            [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:@"bank_accno"] forKey:PARAMETER19];
                            
                        }
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_UNLOAD_OTHER_BANK_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_UNLOAD_OTHER_BANK_T3])
                    {
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
                        if (local_dataDictionary)
                        {
                            webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                        }
                        for (int i=0; i<[local_ContentArray count]; i++)
                        {
                            //MPIN Validation
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                        if ([local_dataDictionary objectForKey:@"bank"]) {
                            [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:@"bank"] forKey:PARAMETER18];
                        }
                        if ([local_dataDictionary objectForKey:@"bank_accno"]) {
                            [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:@"bank_accno"] forKey:PARAMETER19];
                            
                        }
                        NSLog(@"LOcal Data Dictionary is...%@,%@",local_dataDictionary,webServiceRequestInputDetails);
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else if([local_processorCode isEqualToString:PROCESSOR_CODE_PAY_MERCHANT])
                    {
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                        for (int i=0; i<[local_ContentArray count]; i++)
                        {
                            //MPIN Validation
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                        
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_ADD_PAYEE])
                    {
                        webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                        
                        if ([local_dataDictionary objectForKey:PARAMETER13]) {
                            [webServiceRequestInputDetails setObject:[local_dataDictionary objectForKey:PARAMETER13] forKey:PARAMETER13];
                        }
                        
                        for (int i=0; i<[local_ContentArray count]; i++)
                        {
                            //MPIN Validation
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else
                        if ([local_processorCode isEqualToString:PROCESSOR_CODE_CASHOUT_BANK_AGGREGATOR])
                        {
                            //To be uncommented once api is ready
                            webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                            for (int i=0; i<[local_ContentArray count]; i++)
                            {
                                [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            
                            [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                        }
                        else
                        {
                            //Default Processor Code Implementation
                        }
            }
        }
    }
    
    
    else if ([local_actionType isEqualToString:ACTION_TYPE_4])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:PARENT_TEMPLATE2_CANCEL object:local_processorCode];
        [[NSNotificationCenter defaultCenter] postNotificationName:PARENT_TEMPLATE7_CANCEL object:local_processorCode];
        
        if ([local_processorCode isEqualToString:PROCESSOR_CODE_BALANCE_ENQUIRY] || [local_processorCode isEqualToString:PROCESSOR_CODE_MINI_STATEMENT] || [local_processorCode isEqualToString:PROCESSOR_CODE_CHECK_BALANCE_T3] || ([local_processorCode isEqualToString:PROCESSOR_CODE_TXN_HISTORY_T3]&& [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_TXN_HISTORY_T3]) || [local_processorCode isEqualToString:PROCESSOR_CODE_CHECK_BALANCE_T3]) {
            NSArray *viewsArray = [[NSArray alloc] initWithArray:[self.view subviews]];
            for (int i = 0; i<[viewsArray count]; i++)
            {
                UIView *lView = [viewsArray objectAtIndex:i];
                if (lView.tag == -3)
                {
                    [lView removeFromSuperview];
                }
            }
        }
        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG])
        {
            NSArray *viewsArray = [[NSArray alloc] initWithArray:[[local_id superview] subviews]];
            
            for (int i = 0; i<[viewsArray count]; i++)
            {
                UIView *lView = [viewsArray objectAtIndex:i];
                if (lView.tag == -3)
                {
                    [lView removeFromSuperview];
                }
            }
        }
        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG_T3])
        {
            NSArray *viewsArray = [[NSArray alloc] initWithArray:[[local_id superview] subviews]];
            
            for (int i = 0; i<[viewsArray count]; i++)
            {
                UIView *lView = [viewsArray objectAtIndex:i];
                if (lView.tag == -3)
                {
                    [lView removeFromSuperview];
                }
            }
        }
        else
            [self removeViewsWithClassType:NO];
        
    }
    else if ([local_actionType isEqualToString:ACTION_TYPE_5])
    {
        Class local_Class = NSClassFromString(local_currentClass);
        
        if ([local_Class instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)] || [local_Class instancesRespondToSelector:@selector(initWithFrame:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)])
        {
            [self.navigationController popViewControllerAnimated:NO];
            
        }
        else if ([local_Class instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray:withSubIndex:)])
        {
            [self removeViewsWithClassType:NO];
        }
    }
    else if ([local_actionType isEqualToString:ACTION_TYPE_6])
    {
        if ([local_processorCode isEqual:PROCESSOR_CODE_PAY_BILLER_NICK_NAME] || [local_processorCode isEqual:PROCESSOR_CODE_PULL_DECLINE_REQUEST_T3])
        {
            if (local_ContentArray )
            {
                for (int i=0; i<[local_ContentArray count]; i++)
                {
                    if ([[local_id viewWithTag:i+100] isKindOfClass:[UITextField class]])
                    {
                        CustomTextField *textField = (CustomTextField *)[local_id viewWithTag:i+100];
                        NSString *fieldValue = textField.text;
                        [[local_ContentArray objectAtIndex:i] setObject:fieldValue forKey:@"value"];
                    }
                }
            }
            
            if ([local_validationType integerValue] == 0)
            {
                NSString *errorMessage  = [ValidationsClass validateInputs:local_ContentArray];
                if (![errorMessage isEqualToString:@""])
                {
                    if([local_alertType compare:TICKER_TEXT] == NSOrderedSame)
                    {
                        NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
                        
                        NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
                        
                        [[[local_id superview] superview] makeToast:errorMessage duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
                    }
                    else if ([local_alertType compare:POPUP_TEXT] == NSOrderedSame)
                    {
                        PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]] andMessage:errorMessage withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                        
                        [[[local_id superview] superview] addSubview:popup];
                    }
                    else
                    {
                        
                    }
                }
                else
                {
                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                    [userDefaults setObject:local_dataDictionary forKey:@"PayBillsByNickName"];
                    webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                    
                    for (int i=0; i<[local_ContentArray count]; i++)
                    {
                        if (![[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] isEqualToString:application_default_no_value_available])
                        {
                            //MPIN Validation
                            if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                            else
                                if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                {
                                    [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                }
                                else
                                    if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                                    {
                                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                        
                                    }
                                    else
                                    {
                                        [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                    }
                        }
                    }
                    
                    ConvertDetailsToSrverRequestFormate *tempConversion = [[ConvertDetailsToSrverRequestFormate alloc] init];
                    NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:[tempConversion convertThePayBillValues:webServiceRequestInputDetails]];
                    [self commonWebServiceCall:tempDic withProcessorCode:local_processorCode];
                }
            }
        }
        else
            [self removeViewsWithClassType:YES];
    }
    else if ([local_actionType isEqualToString:ACTION_TYPE_7])
    {
        if ([local_validationType integerValue] == 0 && (local_validationType != nil))
        {
            if ( local_ContentArray )
            {
                UIScrollView *scrollView;
                if ([local_id subviews] > 0)
                {
                    for (int i=0; i<[[local_id subviews] count]; i++)
                    {
                        if ([[[local_id subviews] objectAtIndex:i] isKindOfClass:[UIScrollView class]]) {
                            scrollView = [[local_id subviews] objectAtIndex:i];
                        }
                    }
                    
                    int count = 1;
                    NSMutableArray *array = [[NSMutableArray alloc] init];
                    for (int j = 0; j < [[scrollView subviews] count]; j++) {
                        if ([[[scrollView subviews] objectAtIndex:j] isKindOfClass:[UITextField class]])
                        {
                            CustomTextField *textField = [[scrollView subviews] objectAtIndex:j];
                            NSString *fieldValue = textField.text;
                            
                            if (fieldValue)
                                [array addObject:fieldValue];
                            else
                                [array addObject:@""];
                        }
                        else if ([[[scrollView subviews] objectAtIndex:j] isKindOfClass:[UIButton class]])
                        {
                            UIButton *buttonText = [[scrollView subviews] objectAtIndex:j];
                            NSString *buttonTextValue = buttonText.titleLabel.text;
                            NSString *keyVal = nil;
                            if ([local_currentClass isEqualToString:PARENT_TEMPLATE_4]) {
                                keyVal = [NSString stringWithFormat:@"parent_template4_drop_down_value%ld_hint",(long)buttonText.tag];
                            }
                            
                            if ([local_currentClass isEqualToString:CHILD_TEMPLATE_1]) {
                                keyVal = [NSString stringWithFormat:@"child_template1_drop_down_value%ld_hint",(long)buttonText.tag];
                            }
                            
                            if ([local_currentClass isEqualToString:CHILD_TEMPLATE_12]) {
                                keyVal = [NSString stringWithFormat:@"child_template12_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            if ([local_currentClass isEqualToString:POPUP_TEMPLATE_1]) {
                                keyVal = [NSString stringWithFormat:@"popup_template1_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            if ([local_currentClass isEqualToString:POPUP_TEMPLATE_6]) {
                                keyVal = [NSString stringWithFormat:@"popup_template6_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            if ([local_currentClass isEqualToString:CHILD_TEMPLATE_11]) {
                                keyVal = [NSString stringWithFormat:@"child_template11_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(keyVal,local_currentTemplatePropertyFileName,[NSBundle mainBundle], nil)];
                            if ([buttonTextValue isEqualToString:buttonStr])
                            {
                                [array addObject:@""];
                            }
                            else
                            {
                                [array addObject:buttonTextValue];
                            }
                            count ++;
                        }
                    }
                    for (int i = 0; i < array.count; i++) {
                        [[local_ContentArray objectAtIndex:i] setObject:[array objectAtIndex:i] forKey:@"value"];
                    }
                }
                else
                {
                    for (int i=0; i<[local_ContentArray count]; i++)
                    {
                        if ([[local_id viewWithTag:i+100] isKindOfClass:[UITextField class]])
                        {
                            CustomTextField *textField = (CustomTextField *)[local_id viewWithTag:i+100];
                            NSString *fieldValue = textField.text;
                            [[local_ContentArray objectAtIndex:i] setObject:fieldValue forKey:@"value"];
                        }
                    }
                }
            }
            
            NSString *errorMessage  = [ValidationsClass validateInputs:local_ContentArray];
            
            if (![errorMessage isEqualToString:@""])
            {
                if([local_alertType compare:TICKER_TEXT] == NSOrderedSame)
                {
                    NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
                    
                    NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    [[[local_id superview] superview] makeToast:errorMessage duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
                }
                else if ([local_alertType compare:POPUP_TEXT] == NSOrderedSame)
                {
                    PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]]  andMessage:errorMessage withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                    
                    [[[local_id superview] superview] addSubview:popup];
                }
                else
                {
                }
            }
            else
            {
                
                webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
                
                if (local_dataDictionary)
                {
                    webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                }
                
                for (int i=0; i<[local_ContentArray count]; i++)
                {
                    //MPIN Validation
                    if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                    {
                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                    }
                    else
                        if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                        {
                            [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                        }
                        else
                            if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                
                            }
                            else
                            {
                                [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                }
                
                
                
                if ([local_processorCode isEqualToString:PROCESSOR_CODE_SELF_REG_DIGITAL_KYC]) {
                    if ([local_dataDictionary objectForKey:PARAMETER19]) {
                        [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                    }
                    else
                    {
                        [self showAlertForErrorWithMessage:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10015", nil)]] andErrorCode:nil];
                    }
                }
                if ([local_processorCode isEqualToString:PROCESSOR_CODE_PAY_MERCHANT]) {
                    [webServiceRequestInputDetails setObject:[webServiceRequestInputDetails objectForKey:@"paymentMode"] forKey:PARAMETER35];
                    [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
                }
                else
                {
                    //Default Processor Code Implementation
                }
            }
        }
    }
    else if ([local_actionType isEqualToString:ACTION_TYPE_8])
    {
        if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""])
        {
            Class nextClass = NSClassFromString(local_nextTemplate);
            id object = nil;
            
            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)]){
                if (local_selectedIndex >=0)
                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:((int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                else
                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails?webServiceRequestInputDetails:local_dataDictionary withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                
                [self.view addSubview:(UIView *)object];
            }
            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
            {
                if ([local_nextTemplate isEqualToString:PARENT_TEMPLATE_5]) {
                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:10 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                    UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:object];
                    [navi setNavigationBarHidden:YES];
                    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                    [delegate.window setRootViewController:navi];
                }
                else if ([nextClass isSubclassOfClass:[UIViewController class]])
                {
                    BaseViewController *object = [[NSClassFromString(local_nextTemplate) alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                    if (self.topController == nil){
                        object.topController = self;
                    }
                    else
                        object.topController = self.topController;
                    
                    [self.navigationController pushViewController:object animated:NO];
                }
                else
                {
                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode?local_processorCode:nil withPropertyFile:local_nextTemplatePropertyFileName?local_nextTemplatePropertyFileName:nil withProcessorCode:nil dataArray:nil dataDictionary:nil];
                    
                    [self.navigationController pushViewController:object animated:NO];
                }
            }
            
        }
        else if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""]) {
            [self getSelectedFeaturemenuProperty];
        }
    }
    else if ([local_actionType isEqualToString:ACTION_TYPE_9])
    {
        if ([local_validationType integerValue] == 0 && (local_validationType != nil))
        {
            if ( local_ContentArray )
            {
                UIScrollView *scrollView;
                if ([local_id subviews] > 0)
                {
                    for (int i=0; i<[[local_id subviews] count]; i++)
                    {
                        if ([[[local_id subviews] objectAtIndex:i] isKindOfClass:[UIScrollView class]]) {
                            scrollView = [[local_id subviews] objectAtIndex:i];
                        }
                    }
                    
                    NSMutableArray *array = [[NSMutableArray alloc] init];
                    int count = 1;
                    for (int j = 0; j < [[scrollView subviews] count]; j++) {
                        if ([[[scrollView subviews] objectAtIndex:j] isKindOfClass:[UITextField class]])
                        {
                            CustomTextField *textField = [[scrollView subviews] objectAtIndex:j];
                            NSString *fieldValue = textField.text;
                            
                            if (fieldValue)
                                [array addObject:fieldValue];
                            
                            else
                                [array addObject:@""];
                        }
                        else if ([[[scrollView subviews] objectAtIndex:j] isKindOfClass:[UIButton class]])
                        {
                            UIButton *buttonText = [[scrollView subviews] objectAtIndex:j];
                            NSString *buttonTextValue = buttonText.titleLabel.text;
                            NSString *keyVal = nil;
                            if ([local_currentClass isEqualToString:PARENT_TEMPLATE_4]) {
                                keyVal = [NSString stringWithFormat:@"parent_template4_drop_down_value%ld_hint",(long)buttonText.tag];
                            }
                            
                            if ([local_currentClass isEqualToString:CHILD_TEMPLATE_1]) {
                                keyVal = [NSString stringWithFormat:@"child_template1_drop_down_value%ld_hint",(long)buttonText.tag];
                            }
                            
                            if ([local_currentClass isEqualToString:CHILD_TEMPLATE_12]) {
                                keyVal = [NSString stringWithFormat:@"child_template12_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            if ([local_currentClass isEqualToString:POPUP_TEMPLATE_1]) {
                                keyVal = [NSString stringWithFormat:@"popup_template1_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            if ([local_currentClass isEqualToString:POPUP_TEMPLATE_6]) {
                                keyVal = [NSString stringWithFormat:@"popup_template6_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            if ([local_currentClass isEqualToString:CHILD_TEMPLATE_11]) {
                                keyVal = [NSString stringWithFormat:@"child_template11_input_dropdown_value%ld_hint",(long)buttonText.tag];
                            }
                            NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(keyVal,local_currentTemplatePropertyFileName,[NSBundle mainBundle], nil)];
                            if ([buttonTextValue isEqualToString:buttonStr])
                            {
                                [array addObject:@""];
                            }
                            else
                            {
                                [array addObject:buttonTextValue];
                            }
                            count ++;
                        }
                    }
                    for (int i = 0; i < array.count; i++) {
                        [[local_ContentArray objectAtIndex:i] setObject:[array objectAtIndex:i] forKey:@"value"];
                    }
                }
                else
                {
                    for (int i=0; i<[local_ContentArray count]; i++)
                    {
                        if ([[local_id viewWithTag:i+100] isKindOfClass:[UITextField class]])
                        {
                            CustomTextField *textField = (CustomTextField *)[local_id viewWithTag:i+100];
                            NSString *fieldValue = textField.text;
                            [[local_ContentArray objectAtIndex:i] setObject:fieldValue forKey:@"value"];
                        }
                    }
                }
            }
            NSString *errorMessage  = [ValidationsClass validateInputs:local_ContentArray];
            
            if (![errorMessage isEqualToString:@""])
            {
                if([local_alertType compare:TICKER_TEXT] == NSOrderedSame)
                {
                    NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
                    
                    NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    [[[local_id superview] superview] makeToast:errorMessage duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
                }
                else if ([local_alertType compare:POPUP_TEXT] == NSOrderedSame)
                {
                    PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]]  andMessage:errorMessage withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                    [[[local_id superview] superview] addSubview:popup];
                }
                else
                {
                    
                }
            }
            else
            {
                if (local_dataDictionary)
                    webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                
                for (int i=0; i<[local_ContentArray count]; i++)
                {
                    //MPIN Validation
                    if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                    {
                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                    }
                    else
                        if([ValidationsClass isActivationCodeField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                        {
                            [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                        }
                        else
                            if ([ValidationsClass isMPINReEnterField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                            {
                                [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                                
                            }
                            else
                            {
                                [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                            }
                }
                
                if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""])
                {
                    Class nextClass = NSClassFromString(local_nextTemplate);
                    id object = nil;
                    
                    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                    {
                        if (local_selectedIndex >=0)
                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                        else
                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:local_processorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                        
                        [self.view addSubview:(UIView *)object];
                        
                    }
                    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                    {
                        if ([nextClass isSubclassOfClass:[UIViewController class]])
                        {
                            BaseViewController *object = [[NSClassFromString(local_nextTemplate) alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:local_ContentArray dataDictionary:webServiceRequestInputDetails];
                            if (self.topController == nil)
                            {
                                object.topController = self;
                                
                            }
                            else
                                object.topController = self.topController;
                            
                            [self.navigationController pushViewController:object animated:NO];
                        }
                    }
                    
                }
            }
        }
    }
    else if ([local_actionType isEqualToString:ACTION_TYPE_10]) {
        [[self.view viewWithTag:-5] removeFromSuperview];
    }
    else if ([local_actionType isEqualToString:ACTION_TYPE_11])
    {
        
    }
    else if ([local_actionType isEqualToString:ACTION_TYPE_12])
    {
        if ([local_validationType integerValue] == 0 && (local_validationType != nil))
        {
            if ( local_ContentArray )
            {
                for (int i=0; i<[local_ContentArray count]; i++)
                {
                    if ([[local_id viewWithTag:i+100] isKindOfClass:[UITextField class]])
                    {
                        CustomTextField *textField = (CustomTextField *)[local_id viewWithTag:i+100];
                        NSString *fieldValue = textField.text;
                        [[local_ContentArray objectAtIndex:i] setObject:fieldValue forKey:@"value"];
                    }
                }
            }
        }
        NSString *errorMessage  = [ValidationsClass validateInputs:local_ContentArray];
        if (![errorMessage isEqualToString:@""])
        {
            if([local_alertType compare:TICKER_TEXT] == NSOrderedSame)
            {
                NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
                
                NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                [[[local_id superview] superview] makeToast:errorMessage duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
            }
            else if ([local_alertType compare:POPUP_TEXT] == NSOrderedSame)
            {
                PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]]  andMessage:errorMessage withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                
                [[[local_id superview] superview] addSubview:popup];
            }
        }
        else{
            webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
            
            if (local_dataDictionary)
                webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
            
            for (int i=0; i<[local_ContentArray count]; i++)
            {
                [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
            }
            Class nextClass = NSClassFromString(local_nextTemplate);
            id object = nil;
            
            if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
            {
                object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:local_processorCode?local_processorCode:nil dataArray:nil dataDictionary:webServiceRequestInputDetails?webServiceRequestInputDetails:nil];
                [self.navigationController pushViewController:object animated:NO];
            }
        }
    }
    else if ([local_actionType isEqualToString:ACTION_TYPE_13])
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"refreshView"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
        picker.peoplePickerDelegate = self;
        
        AppDelegate *apDlgt = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        UINavigationController *navCntrl = (UINavigationController *)apDlgt.window.rootViewController;
        if (navCntrl && [navCntrl isKindOfClass:[UINavigationController class]])
        {
            [navCntrl presentViewController:picker animated:YES completion:nil];
        }
    }
    else
        if ([local_actionType isEqualToString:ACTION_TYPE_14])
        {
            [self removeViewsWithClassType:YES];
            Class nextClass = NSClassFromString(local_nextTemplate);
            id object = nil;
            
            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
            {
                if (local_selectedIndex >=0)
                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                else
                    object= [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:local_processorCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                
                [[local_id superview]  addSubview:(UIView *)object];
                
            }
            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
            {
                object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:local_processorCode dataArray:nil dataDictionary:local_dataDictionary];
                
                UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                [vc.navigationController pushViewController:object animated:NO];
            }
            
        }
        else
            if ([local_actionType isEqualToString:ACTION_TYPE_15])
            {
                [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"userLogged"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                int userLevel = (int)[NSLocalizedStringFromTableInBundle(@"application_default_user_support_mode",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
                
                if (userLevel == 0) {
                    UIViewController *object = [[NSClassFromString(application_launch_template) alloc] initWithNibName:application_launch_template bundle:nil withSelectedIndex:0 fromView:3 withFromView:nil withPropertyFile:single_user_application_launch_template_property_files_list withProcessorCode:nil dataArray:nil dataDictionary:nil];
                    UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                    [vc.navigationController pushViewController:object animated:NO];
                    
                }
                else {
                    UIViewController *object= [[NSClassFromString(application_launch_template) alloc] initWithNibName:application_launch_template bundle:nil withSelectedIndex:0 fromView:3 withFromView:nil withPropertyFile:application_launch_template_property_files_list_T1 withProcessorCode:nil dataArray:nil dataDictionary:nil];
                    UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                    [vc.navigationController pushViewController:object animated:NO];
                }
            }
            else if ([local_actionType isEqualToString:ACTION_TYPE_16])
            {
                webServiceRequestInputDetails=[[NSMutableDictionary alloc]initWithDictionary:local_dataDictionary];
                [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
            }
            else if ([local_actionType isEqualToString:ACTION_TYPE_19]){
                webServiceRequestInputDetails=[[NSMutableDictionary alloc]initWithDictionary:local_dataDictionary];
                int userLevel = (int)[NSLocalizedStringFromTableInBundle(@"application_default_user_support_mode",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
                NSLog(@"userLevel Is...%d",userLevel);
                NSString *phoneNumberStr=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
                NSLog(@"Phone number string is..%@",phoneNumberStr);
                
                if ((userLevel ==1) && ([[webServiceRequestInputDetails objectForKey:PARAMETER15] isEqualToString:PROCESSOR_CODE_LOGIN_T3]) && ([phoneNumberStr length]==0))
                {
                    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"userLogged"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[webServiceRequestInputDetails objectForKey:PARAMETER6] forKey:SAVEDMOBILENUMBER];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                }
                
                [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
            }
            else if ([local_actionType isEqualToString:ACTION_TYPE_20])
            {
                webServiceRequestInputDetails=[[NSMutableDictionary alloc]initWithDictionary:local_dataDictionary];
                [self commonWebServiceCall:webServiceRequestInputDetails withProcessorCode:local_processorCode];
            }
}



#pragma mark - Get Address book Contect details by using Delegate Methods.
/**
 * This method is used to get Contact details.
 * Fetch data from  Address book contact details.
 */
//Contact AddressBook

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    UINavigationController *navCntrl = (UINavigationController *)delegate.window.rootViewController;
    
    if (navCntrl && [navCntrl isKindOfClass:[UINavigationController class]])
        [navCntrl dismissViewControllerAnimated:YES completion:nil];
}


- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    
    [basetarget performSelectorOnMainThread:baseSelector withObject:(__bridge id)(person) waitUntilDone:YES];
    
    // AppDelegate method.
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    UINavigationController *navCntrl = (UINavigationController *)delegate.window.rootViewController;
    
    if (navCntrl && [navCntrl isKindOfClass:[UINavigationController class]])
        [navCntrl dismissViewControllerAnimated:YES completion:nil];
    
    return NO;
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person
{
    [basetarget performSelectorOnMainThread:baseSelector withObject:(__bridge id)(person) waitUntilDone:YES];
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    UINavigationController *navCntrl = (UINavigationController *)delegate.window.rootViewController;
    
    if (navCntrl && [navCntrl isKindOfClass:[UINavigationController class]])
        [navCntrl dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Remove Class from view and clear Textfield Data.
/**
 * This method is used to Remove Class from view and clear Textfield Data.
 * Clear Previous Stack Data and Load Current view Data.
 */
-(void) removeViewsWithClassType:(BOOL) clear
{
    if ([local_id isKindOfClass:[PopUpTemplate2 class]])
    {
        NSArray *inputFields;
        
        NSArray *viewsArray = [[NSArray alloc] initWithArray:[[local_id superview] subviews]];
        for (int i = 0 ; i < viewsArray.count; i++) {
            NSArray *array = [[viewsArray objectAtIndex:i] subviews];
            for (int j = 0; j < array.count; j++) {
                UIView *view = [array objectAtIndex:j];
                NSArray *subviewArray = [view subviews];
                for (int k=0; k<[subviewArray count]; k++) {
                    if ([[subviewArray objectAtIndex:k] isKindOfClass:[UIScrollView class]])
                    {
                        UIScrollView *scrollView = [subviewArray objectAtIndex:k];
                        inputFields = [scrollView subviews];
                    }
                }
            }
        }
        
        //Clearing textfield on successful send money
        for (int i = 0; i < [inputFields count]; i++)
        {
            if ([[inputFields objectAtIndex:i] isKindOfClass:[UITextField class]])
            {
                [[inputFields objectAtIndex:i] setText:nil];
            }
            else if ([[inputFields objectAtIndex:i] isKindOfClass:[UIButton class]])
            {
                [[inputFields objectAtIndex:i] setTitle:nil];
            }
        }
        
        for (int i = 0; i<[viewsArray count]; i++)
        {
            UIView *lView = [viewsArray objectAtIndex:i];
            if (lView.tag < 0)
            {
                [lView removeFromSuperview];
            }
            else if (lView.tag == 1)
            {
                [lView removeFromSuperview];
            }
        }
    }
    else
        if ([local_id isKindOfClass:[PopUpTemplate1 class]])
        {
            NSArray *inputFields;
            
            NSArray *viewsArray = [[NSArray alloc] initWithArray:[[local_id superview] subviews]];
            for (int i = 0 ; i < viewsArray.count; i++) {
                NSArray *array = [[viewsArray objectAtIndex:i] subviews];
                for (int j = 0; j < array.count; j++) {
                    UIView *view = [array objectAtIndex:j];
                    NSArray *subviewArray = [view subviews];
                    for (int k=0; k<[subviewArray count]; k++) {
                        if ([[subviewArray objectAtIndex:k] isKindOfClass:[UIScrollView class]])
                        {
                            UIScrollView *scrollView = [subviewArray objectAtIndex:k];
                            inputFields = [scrollView subviews];
                        }
                        
                    }
                }
            }
            
            for (int i = 0; i<[viewsArray count]; i++)
            {
                UIView *lView = [viewsArray objectAtIndex:i];
                if (lView.tag < 0)
                {
                    [lView removeFromSuperview];
                }
                else if (lView.tag == 1)
                {
                    [lView removeFromSuperview];
                }
            }
        }
        else
        {
            
            if ([local_id isKindOfClass:[ParentTemplate5 class]])
            {
                ParentTemplate5 *obj=local_id;
                [obj.view removeFromSuperview];
            } else {
                [local_id removeFromSuperview];
            }
        }
}

#pragma mark - Clear TextField Data.
/**
 * This method is used to Remove Text from Textfield Data.
 */
- (void)removeTextOfTextField : (CustomTextField *)textField
{
    [textField setText:nil];
}
#pragma mark - Remove TextData from Button.
/**
 * This method is used to Remove Text from button.
 */
- (void)removeTextOfButton : (UIButton *)btnStr
{
    [btnStr setTitle:@"" forState:UIControlStateNormal];
}

-(void)viewDidAppear:(BOOL)animated
{
    activityIndicator = [[ActivityIndicator alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    
    [super viewDidAppear:YES];
    
    NSMutableArray *ObserverArray = [[NSMutableArray alloc] initWithArray:[OBSERVERS componentsSeparatedByString:@"|"]];
    for (int i = 0; i < ObserverArray.count; i++) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationAction:) name:[ObserverArray objectAtIndex:i] object:nil];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setContentValue:) name:POST_CONTENT object:nil];
}

-(void) viewDidDisappear:(BOOL)animated
{
    [activityIndicator removeFromSuperview];
    [super viewDidDisappear:YES];
    
    NSMutableArray *ObserverArray = [[NSMutableArray alloc] initWithArray:[OBSERVERS componentsSeparatedByString:@"|"]];
    for (int i = 0; i < ObserverArray.count; i++) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:[ObserverArray objectAtIndex:i] object:nil];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:POST_CONTENT object:nil];
}

#pragma mark - Set notification data method.
/**
 * This method is used to Set notification data.
 */
- (void) notificationAction:(NSNotification *)notification
{
    [self setNextTempletewithTemplate:notification.object];
}

#pragma mark - Set notification content data.
/**
 * This method is used to Set notification conntent data.
 */
- (void) setContentValue:(NSNotification *)notification
{
    postDictionary = notification.object;
}

#pragma mark - Set next Template data for Given Action Type.
/**
 * This method is used to Set next Template data for Given Action Type.
 *@pram type - Feature Realted Data(local_dataDictionary,local_actionType,local_nextTemplate
 ,local_nextTemplatePropertyFileName,local_processorCode,local_validationType,local_alertType,local_ContentArray,local_apiType,local_currentTemplatePropertyFileName,local_id,local_labelValidationArray,local_selected_processorCode,local_selectedIndex,[local_dataDictionary objectForKey:PARAMETER13] and local_currentClass)
 */
-(void) setNextTempletewithTemplate :(Template *)templateObject
{
    local_dataDictionary = templateObject.dictionary;
    local_actionType = templateObject.actionType;
    local_nextTemplate = templateObject.nextTemplateName;
    local_nextTemplatePropertyFileName = templateObject.nextTemplatepropertyFile;
    local_processorCode = templateObject.processorCode;
    local_validationType = templateObject.validationType;
    local_alertType = templateObject.alertType;
    local_ContentArray = templateObject.contentArray;
    local_apiType = templateObject.apiParameterType;
    local_currentTemplatePropertyFileName = templateObject.currentTemplatepropertyFile;
    local_id = templateObject.classType;
    local_labelValidationArray = templateObject.labelValidationArray;
    local_selected_processorCode = templateObject.selectedLocalProcessorCode;
    local_selectedIndex = templateObject.selectedIndex;
    local_transactionType = templateObject.transactionType;
    local_currentClass = templateObject.currentClass;
    
    if (templateObject.target != [NSNull null]) {
        basetarget = templateObject.target;
    }
    if(templateObject.selector) {
        baseSelector = templateObject.selector;
    }
    
    [self performAction];
}

#pragma mark - Get TableView list data .
/**
 * This method is used to Get TableView list data.
 *@pram type - Table list Data From DataBase Manager.
 */
+ (BOOL)getDataFromProcessoreCode : (NSString *)processorCode andPropertyFileName : (NSString *)propertyFileName forArray : (NSMutableArray *__strong *)dataArray
{
    DatabaseManager *dataManager = nil;
    BOOL isProcessorCodeProcessed = NO;
    NSString *data = NSLocalizedStringFromTableInBundle(@"child_template2_list_data_to_load_webservice_api",propertyFileName,[NSBundle mainBundle], nil);
    NSString *webserviceDropDownTypeName;
    NSString *processor_code;
    NSString *transactionType;
    
    if ([[data componentsSeparatedByString:@","] count] == 1) {
        webserviceDropDownTypeName = data;
    }
    else
    {
        processor_code = [Template getProcessorCodeWithData:data];
        transactionType = [Template getTransactionCodeWithData:data];
    }
    
    NSString *apiName = NSLocalizedStringFromTableInBundle(@"child_template2_list_data_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    
    if ([processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG])
    {
        dataManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
        *dataArray = [[NSMutableArray alloc] initWithArray:[dataManager getAllDropDownDetailsForKey:apiName]];
        isProcessorCodeProcessed = YES;
    }
    else if ([processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG_T3])
    {
        dataManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
        *dataArray = [[NSMutableArray alloc] initWithArray:[dataManager getAllDropDownDetailsForKey:apiName]];
        isProcessorCodeProcessed = YES;
    }
    else if (webserviceDropDownTypeName != nil && webserviceDropDownTypeName.length != 0) {
        dataManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
        isProcessorCodeProcessed = YES;
        
        if ([webserviceDropDownTypeName isEqualToString:@"SUPPORTED_BANK_LIST"] || [webserviceDropDownTypeName isEqualToString:@"AGG_LINKED_BANK_LIST"]) {
            NSMutableArray *parseArray = [[NSMutableArray alloc] initWithArray:[dataManager getAllDropDownDetailsForKey:webserviceDropDownTypeName]];
            *dataArray = [[NSMutableArray alloc] init];
            for (int i = 0; i < parseArray.count; i++) {
                NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] initWithDictionary:[parseArray objectAtIndex:i]];
                [dataDict setObject:[[parseArray objectAtIndex:i] objectForKey:DROP_DOWN_TYPE_DESC] forKey:TRANSACTION_DATA_TRANSACTION_CASHOUT_BANK_DESC];
                [dataDict setObject:[[parseArray objectAtIndex:i] objectForKey:DROP_DOWN_TYPE_NAME] forKey:TRANSACTION_DATA_TRANSACTION_CASHOUT_BANK_NAME];
                [*dataArray addObject:dataDict];
            }
        }
        else
            *dataArray =[[NSMutableArray alloc] initWithArray:[dataManager getAllDropDownDetailsForKey:webserviceDropDownTypeName]];
        
    }
    else if ([processor_code isEqualToString:PROCESSOR_CODE_P2P_REGISTERED_BENEFICIARY_MOBILE_NUMBER])
    {
        dataManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
        NSArray *tempArray = [[NSArray alloc] initWithArray:[dataManager getAllTransactionHistory]];
        if ([tempArray count]>0)
        {
            *dataArray = [[NSMutableArray alloc] init];
            for (int i=0; i<[tempArray count]; i++)
            {
                if ([[[tempArray objectAtIndex:i] objectForKey: TRANSACTION_DATA_PROCCESSOR_CODE] isEqualToString:PROCESSOR_CODE_P2P_REGISTERED_BENEFICIARY_MOBILE_NUMBER])
                {
                    [*dataArray addObject:[tempArray objectAtIndex:i]];
                }
            }
        }
        
        NSSortDescriptor * idDescriptor = [[NSSortDescriptor alloc] initWithKey:@"_id" ascending:NO];
        NSArray *sortedArray = [*dataArray sortedArrayUsingDescriptors:@[idDescriptor]];
        [*dataArray removeAllObjects];
        [*dataArray addObjectsFromArray:sortedArray];
        isProcessorCodeProcessed = YES;
    }
    else if([processor_code isEqualToString:PROCESSOR_CODE_BANK])
    {
        dataManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
        NSArray *tempArray = [[NSArray alloc] initWithArray:[dataManager getAllTransactionHistory]];
        
        if ([tempArray count]>0)
        {
            *dataArray = [[NSMutableArray alloc] init];
            for (int i=0; i<[tempArray count]; i++)
            {
                if ([[[tempArray objectAtIndex:i] objectForKey:TRANSACTION_DATA_PROCCESSOR_CODE] isEqualToString:processorCode] && [[[tempArray objectAtIndex:i] objectForKey:TRANSACTION_DATA_TRANSACTION_TYPE] isEqualToString:transactionType]) {
                    [*dataArray addObject:[tempArray objectAtIndex:i]];
                }
            }
        }
        
        NSSortDescriptor * idDescriptor = [[NSSortDescriptor alloc] initWithKey:@"_id" ascending:NO];
        NSArray *sortedArray = [*dataArray sortedArrayUsingDescriptors:@[idDescriptor]];
        [*dataArray removeAllObjects];
        [*dataArray addObjectsFromArray:sortedArray];
        isProcessorCodeProcessed = YES;
    }
    else
        if([processor_code isEqualToString:PROCESSOR_CODE_TOP_UP_PREPAID_MOBILE_OPERATOR])
        {
            dataManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
            *dataArray = [[NSMutableArray alloc] initWithArray:[dataManager getAllTransactionHistoryWithTransactionCode:transactionType]];
            
            NSSortDescriptor * idDescriptor = [[NSSortDescriptor alloc] initWithKey:@"_id" ascending:NO];
            NSArray *sortedArray = [*dataArray sortedArrayUsingDescriptors:@[idDescriptor]];
            [*dataArray removeAllObjects];
            [*dataArray addObjectsFromArray:sortedArray];
            
            isProcessorCodeProcessed = YES;
        }
        else if([processorCode isEqualToString:PROCESSOR_CODE_FETCH_BILLER_ALL])
        {
            isProcessorCodeProcessed = YES;
        }
    return isProcessorCodeProcessed;
}

#pragma mark - Move To Previous Screen method.
/**
 * This method is used to Move To Previous Screen.
 */
-(void)moveToPreviousScreen
{
    UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
    [vc.navigationController popViewControllerAnimated:NO];
}

#pragma mark - Webservice methods.
/**
 * This method is used to Webservice common call method.
 */
-(void)commonWebServiceCall:(NSMutableDictionary *)dataDict withProcessorCode:(NSString *)proccessorCode
{
    [self setActivityIndicator];
    reachability = [Reachability reachabilityForInternetConnection];
    netStatus = [reachability  currentReachabilityStatus];
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    mode = [[standardUserDefaults objectForKey:@"userDetails"] objectForKey:PARAMETER36];
    DatabaseManager *databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
    NSArray *loginData = [databaseManager getAllLoginInformation];
    
    if (netStatus == NotReachable)
    {
        [activityIndicator stopActivityIndicator];
        activityIndicator.hidden = YES;
        if ([local_processorCode isEqualToString:PROCESSOR_CODE_LOGIN_T3]) {
            
            if (loginData.count > 0) {
                [databaseManager deleteAllDetailsFromTransactionHistory];
                [databaseManager deleteAllDetailsFromLoginDetails];
                [databaseManager deleteAllDetailsFromMiniStatement];
                [databaseManager deleteAllDetailsPayBills];
                [databaseManager deleteAllPayBillsTransactionHistory];
            }
            
            if ([mode isEqualToString:@"SMS"]) {
                [self SMSFlow];
            }
            else
            {
                NSMutableDictionary *userDetails = [[NSMutableDictionary alloc] init];
                [userDetails addEntriesFromDictionary:[standardUserDefaults objectForKey:@"userDetails"]];
                [userDetails setObject:@"SMS"forKey:PARAMETER36];
                [standardUserDefaults setObject:userDetails forKey:@"userDetails"];
                [standardUserDefaults synchronize];
                //                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_sms_alert_title", nil)]] message:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_sms_alert_message", nil)]] delegate:self cancelButtonTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_ok_button", nil)]] otherButtonTitles:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancelbutton", nil)]],nil];
                //
                //                alert.tag = 101;
                //                [alert show];
                
                
                UIAlertController *alert= [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_sms_alert_title", nil)]] message:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_sms_alert_message", nil)]] preferredStyle:UIAlertControllerStyleAlert];
                
                [alert addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_ok_button", nil)]] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self SMSFlow];
                }]];
                
                [alert addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancelbutton", nil)]] style:UIAlertActionStyleCancel handler:nil]];
                
                UIViewController *topController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
                while (topController.presentedViewController)
                {
                    topController = topController.presentedViewController;
                }
                [topController presentViewController:alert animated:NO completion:nil];
                
            }
        }
        else
        {
            if ([self isSMSFeature:dataDict]) {
                WebSericeUtils *webUtils = [[WebSericeUtils alloc] init];
                NSMutableDictionary *webUtilsValues = [[NSMutableDictionary alloc] initWithDictionary:[webUtils getWebServiceBundleObjectWithProccessorCode:proccessorCode withInputDataModel:dataDict]];
                [self processDataForSMS:webUtilsValues withProcessCode:local_processorCode];
            }
            else
            {
                [self removeActivityIndicator];
                
                NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
                NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
                NSString *appendingString = [[[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_label_prefix", nil)] stringByAppendingString:[NSString stringWithFormat:@"_1005"]] stringByAppendingString:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_error_msg_seperator", nil)]];
                if ([local_id isKindOfClass:[ParentTemplate5 class]])
                {
                    ParentTemplate5 *obj=local_id;
                    [obj.view makeToast:[NSString stringWithFormat:@"%@%@",appendingString,NSLocalizedStringFromTableInBundle(@"_1005",[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil)] duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
                }
                else
                {
                    [[[local_id superview] superview] makeToast:[NSString stringWithFormat:@"%@%@",appendingString,NSLocalizedStringFromTableInBundle(@"_1005",[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil)] duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
                }
                
            }
        }
    }
    else
    {
        if ([mode isEqualToString:@"SMS"]) {
            if ([self isSMSFeature:dataDict]) {
                if ([local_processorCode isEqualToString:PROCESSOR_CODE_LOGIN_T3]) {
                    if ([loginData count]>0)
                    {
                        [databaseManager deleteAllDetailsFromTransactionHistory];
                        [databaseManager deleteAllDetailsFromLoginDetails];
                        [databaseManager deleteAllDetailsFromMiniStatement];
                        [databaseManager deleteAllDetailsPayBills];
                        [databaseManager deleteAllPayBillsTransactionHistory];
                    }
                    [self SMSFlow];
                }
                else{
                    WebSericeUtils *webUtils = [[WebSericeUtils alloc] init];
                    NSMutableDictionary *webUtilsValues = [[NSMutableDictionary alloc] initWithDictionary:[webUtils getWebServiceBundleObjectWithProccessorCode:proccessorCode withInputDataModel:dataDict]];
                    [self processDataForSMS:webUtilsValues withProcessCode:local_processorCode];
                }
            }
            else
            {
                [self removeActivityIndicator];
                
                NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
                NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
                NSString *appendingString = [[[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_label_prefix", nil)] stringByAppendingString:[NSString stringWithFormat:@"_1005"]] stringByAppendingString:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_error_msg_seperator", nil)]];
                
                if ([local_id isKindOfClass:[ParentTemplate5 class]])
                {
                    ParentTemplate5 *obj=local_id;
                    [obj.view makeToast:[NSString stringWithFormat:@"%@%@",appendingString,NSLocalizedStringFromTableInBundle(@"_1005",[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil)] duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
                }
                else
                {
                    [[[local_id superview] superview] makeToast:[NSString stringWithFormat:@"%@%@",appendingString,NSLocalizedStringFromTableInBundle(@"_1005",[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil)] duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
                }
            }
        }
        else
        {
            WebSericeUtils *webUtils = [[WebSericeUtils alloc] init];
            NSMutableDictionary *webUtilsValues = [[NSMutableDictionary alloc] initWithDictionary:[webUtils getWebServiceBundleObjectWithProccessorCode:proccessorCode withInputDataModel:dataDict]];
            WebServiceRequestFormation *webServiceRequest = [[WebServiceRequestFormation alloc] init];
            NSString *webRequest  = [webServiceRequest sendRequest:webUtilsValues];
            WebServiceRunning *webServiceRun = [[WebServiceRunning alloc] init];
            [webServiceRun startWebServiceWithRequest:webRequest withReqDictionary:webUtilsValues withDelegate:self];
        }
    }
}

#pragma mark - Get Feature Mode (SMS or GPRS).
/**
 * This method is used to Get Feature Mode (SMS or GPRS).
 */
-(BOOL)isSMSFeature:(NSDictionary *)dict
{
    NSMutableArray *smsFeatures = [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(@"application_deafault_sms_supported_features", @"GeneralSettings",[NSBundle mainBundle], nil) componentsSeparatedByString:@","] ];
    NSString *smsFeature = [NSString stringWithFormat:@"%@_%@",local_processorCode,[dict objectForKey:PARAMETER13]];
    
    // Needed to check PLS un comment
    NSLog(@"SMS Feature : %@",smsFeature);
    
    if ([smsFeatures containsObject:smsFeature]) {
        return YES;
    }
    else
        return NO;
}

#pragma mark - UIAlertView delegate Method.
/**
 * This method is used to when user click alert view button remove view from given class.
 */
//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (alertView.tag == 101) {
//        if (buttonIndex==0) {
//            [self SMSFlow];
//        }
//    }
//}

#pragma mark - Application Is in SMS Flow.
/**
 * This method is used to Application Is in SMS Flow.
 */
-(void) SMSFlow {
    
    if ([local_processorCode isEqualToString:PROCESSOR_CODE_LOGIN_T3]) {
        NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
        [sta setValue:@"1" forKey:@"userLogged"];
        [sta synchronize];
        
    }
    propertyFilesArray = [NSLocalizedStringFromTableInBundle(@"sms_default_profile_properties", @"GeneralSettings",[NSBundle mainBundle], nil) componentsSeparatedByString:@","];
    int selectedIndex = (int)[propertyFilesArray indexOfObject:local_nextTemplatePropertyFileName];
    Class nextClass = NSClassFromString(local_nextTemplate);
    id object = nil;
    
    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
    {
        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:local_selectedIndex withDataArray:local_ContentArray withSubIndex:0];
        [self.view addSubview:(UIView *)object];
    }
    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
    {
        object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:selectedIndex+1 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
        UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:object];
        [navi setNavigationBarHidden:YES];
        [[(AppDelegate*)[[UIApplication sharedApplication] delegate] window] setRootViewController:navi];
    }
}

#pragma mark - MFMessageCompose Delegate method.
/**
 * This method is used to Delegate Method of Message (Send Message to user).
 */
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
        {
            NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
            NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
            NSString *appendingString = [[[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_label_prefix", nil)] stringByAppendingString:[NSString stringWithFormat:@"_1008"]] stringByAppendingString:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_error_msg_seperator", nil)]];
            
            if([local_id isKindOfClass:[ParentTemplate5 class]])
            {
                ParentTemplate5 *obj=local_id;
                [obj.view makeToast:[NSString stringWithFormat:@"%@%@",appendingString,NSLocalizedStringFromTableInBundle(@"_1008",[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil)] duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
            }
            else
            {
                [[[local_id superview] superview] makeToast:[NSString stringWithFormat:@"%@%@",appendingString,NSLocalizedStringFromTableInBundle(@"_1008",[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil)] duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
            }
            
            break;
            
        }
            
        case MessageComposeResultFailed:
        {
            NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
            NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
            NSString *appendingString = [[[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_label_prefix", nil)] stringByAppendingString:[NSString stringWithFormat:@"_1008"]] stringByAppendingString:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_error_msg_seperator", nil)]];
            
            
            if([local_id isKindOfClass:[ParentTemplate5 class]])
            {
                ParentTemplate5 *obj=local_id;
                [obj.view makeToast:[NSString stringWithFormat:@"%@%@",appendingString,NSLocalizedStringFromTableInBundle(@"_1008",[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil)] duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
            }
            else
            {
                [[[local_id superview] superview] makeToast:[NSString stringWithFormat:@"%@%@",appendingString,NSLocalizedStringFromTableInBundle(@"_1008",[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil)] duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
            }
            
            break;
        }
        case MessageComposeResultSent:
        {
            NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
            NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
            NSString *appendingString = [[[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_label_prefix", nil)] stringByAppendingString:[NSString stringWithFormat:@"_1006"]] stringByAppendingString:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_error_msg_seperator", nil)]];
            
            if([local_id isKindOfClass:[ParentTemplate5 class]])
            {
                ParentTemplate5 *obj=local_id;
                [obj.view makeToast:[NSString stringWithFormat:@"%@%@",appendingString,NSLocalizedStringFromTableInBundle(@"_1006",[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil)] duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
            }
            else
            {
                [[[local_id superview] superview] makeToast:[NSString stringWithFormat:@"%@%@",appendingString,NSLocalizedStringFromTableInBundle(@"_1006",[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil)] duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
            }
            
            break;
        }
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        activityIndicator.hidden = YES;
        [activityIndicator stopActivityIndicator];
        if (result==MessageComposeResultSent)
        {
            [self processData:nil withProcessCode:local_processorCode];
        }
    }];
    
    NSLog(@"Message Sent Result is...%u",result);
}

-(void)processDataForSMS:(NSMutableDictionary *)webServiceDataObject withProcessCode:(NSString *)processorCode
{
    [self removeActivityIndicator];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    customerPhoneNumber = [[userDefaults objectForKey:@"userDetails"] objectForKey:PARAMETER9];
    local_processorCode = processorCode;
    smsFormatterDictionary = webServiceDataObject;
    smsText = [Template getSMSFormatTextWithDictionary:webServiceDataObject];
    if (([local_processorCode compare:PROCESSOR_CODE_FETCH_FEE] == NSOrderedSame) || ([local_processorCode compare: PROCESSOR_CODE_AGENT_DETAILS]== NSOrderedSame) || [local_processorCode isEqualToString:PROCESSOR_CODE_FETCH_BILLER_ALL] || ([local_processorCode compare:PROCESSOR_CODE_FETCH_FEE_T3] == NSOrderedSame)) {
        [self processData:nil withProcessCode:local_processorCode];
    }
    else
        [self showSMS];
}





#pragma mark - Processing Data WebService Call Method.
/**
 * This method is used to Delegate Method Used WebService Call method..
 */
-(void)processData:(WebServiceDataObject *)webServiceDataObject withProcessCode:(NSString *)processCode
{

    [self removeActivityIndicator];
    currentPCode = webServiceDataObject.ProcessorCode;
    currentTType = webServiceDataObject.TransactionType;
    if([currentPCode isEqualToString:@"0088"] && [currentTType isEqualToString:@"118"]){
        [[NSUserDefaults standardUserDefaults] setObject:webServiceDataObject.Tier3AgentId forKey:@"CustomerPhoneNumber"];
    }
    if (webServiceDataObject.faultCode && webServiceDataObject.faultString) {
        [self showAlertForErrorWithMessage: NSLocalizedStringFromTableInBundle(@"999",[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil) andErrorCode:nil];
    }
    else
    {
        if([currentPCode isEqualToString:@"0062"] && [currentTType isEqualToString:@"346"]){
            if(webServiceDataObject.PaymentDetails2){
                [[NSUserDefaults standardUserDefaults] setObject:webServiceDataObject.PaymentDetails2 forKey:@"LatestMiniStatement"];
            }else{
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"LatestMiniStatement"];
            }
        }
        if ([local_actionType isEqualToString:ACTION_TYPE_1])
        {}
        else if ([local_actionType isEqualToString:ACTION_TYPE_2])
        {
            if([currentPCode isEqualToString:PROCESSOR_CODE_VERIFY_REG] && [currentTType isEqualToString:@"233"]){
                NSString *nextTemplateProperty = @"VerifyRegistrationPPT6_T1";
                NSString *nextTemplate = @"PopUpTemplate6";
                if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
                {
                    Class myclass = NSClassFromString(nextTemplate);
                    //                    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
                    NSMutableDictionary *dict = [self getDictValues:webServiceDataObject];
                    id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:dict withProcessorCode:currentPCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                    //                                 [[[local_id superview] superview] addSubview:(UIView *)obj];
                    [[UIApplication sharedApplication].keyWindow addSubview:(UIView *)obj];
                }
            }else if([currentPCode isEqualToString:PROCESSOR_CODE_VERIFY_REG]
                     && [currentTType isEqualToString:@"234"]){
                //send notification to alert success/error msg and remove subview
                if(webServiceDataObject.ErrorCode){
                    PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]]  andMessage:webServiceDataObject.Remark withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                    [[UIApplication sharedApplication].keyWindow addSubview:popup];
                }else{
                    if([webServiceDataObject.PaymentDetails2 isEqualToString:@"1"]){
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"VerifyRegApproved" object:self userInfo:nil];
                    }
                    if([webServiceDataObject.PaymentDetails2 isEqualToString:@"2"]){
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"VerifyRegRejected" object:self userInfo:nil];
                    }
                    [self.navigationController popToRootViewControllerAnimated:NO];
                }
            }else
                if ([local_processorCode isEqualToString:PROCESSOR_CODE_FETCH_IFSC])
                {
                    if ([webServiceDataObject.Reference5 isEqualToString:@"5"]) {
                        [self.navigationController popToViewController:self.topController animated:NO];
                        [[[TemplateData sharedManager] target] performSelectorOnMainThread:[[TemplateData sharedManager] selector] withObject:nil waitUntilDone:YES];
                    }
                    else{
                        NSArray *array;
                        if (webServiceDataObject.PaymentDetails2) {
                            NSData *data = [webServiceDataObject.PaymentDetails2 dataUsingEncoding:NSUTF8StringEncoding];
                            array = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                        }
                        
                        Class nextClass = NSClassFromString(local_nextTemplate);
                        id object = nil;
                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                        {
                            if (local_selectedIndex >=0)
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                            else
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                            [[[local_id superview] superview] addSubview:(UIView *)object];
                            
                        }
                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                        {
                            BaseViewController *object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:array dataDictionary:webServiceRequestInputDetails];
                            
                            if (self.topController == nil) {
                                object.topController = self;
                            }
                            else
                                object.topController = self.topController;
                            
                            [self.navigationController pushViewController:object animated:NO];
                        }
                    }
                }
                else if([local_processorCode isEqualToString:PROCESSOR_CODE_PAYU_LOAD_AUTHENTICATE])
                {
                    if(![[webServiceRequestInputDetails objectForKey:webServiceDataObject.FeeAmount] isEqualToString:@"(null)"])
                    {
                        [webServiceRequestInputDetails setValue:webServiceDataObject.FeeAmount forKey:PARAMETER24];
                        [[NSUserDefaults standardUserDefaults] setValue:webServiceDataObject.FeeAmount forKey:@"TranasctionFeeAmount"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    if(![[webServiceRequestInputDetails objectForKey:webServiceDataObject.TxnAmount] isEqualToString:@"(null)"])
                    {
                        [webServiceRequestInputDetails setValue:webServiceDataObject.TxnAmount forKey:PARAMETER21];
                        [[NSUserDefaults standardUserDefaults] setValue:webServiceDataObject.TxnAmount forKey:@"TranasctionAmount"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    if(![[webServiceRequestInputDetails objectForKey:webServiceDataObject.Reference1] isEqualToString:@"(null)"])
                    {
                        [[NSUserDefaults standardUserDefaults] setValue:webServiceDataObject.Reference1 forKey:@"PayUAuthDetails"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [webServiceRequestInputDetails setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"PayUAuthDetails"] forKey:@"PayUAuthDetails"];
                    }
                    if(![[webServiceRequestInputDetails objectForKey:webServiceDataObject.OpsTransactionId] isEqualToString:@"(null)"])
                    {
                        [[NSUserDefaults standardUserDefaults] setValue:webServiceDataObject.OpsTransactionId forKey:@"PayuOpsTransactionId"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    
                    Class nextClass = NSClassFromString(local_nextTemplate);
                    id object = nil;
                    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                    {
                        if (local_selectedIndex >=0)
                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails?webServiceRequestInputDetails:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                        else
                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                        [[[local_id superview] superview] addSubview:(UIView *)object];
                    }
                    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                    {
                        object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:webServiceRequestInputDetails];
                        [self.navigationController pushViewController:object animated:NO];
                    }
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_FETCH_FEE])
                {
                    if ([local_apiType isEqualToString:PARAMETER_P2P] || [local_apiType isEqualToString:PARAMETER_TOPUP] || [local_apiType isEqualToString:PARAMETER_DTH] || [local_apiType isEqualToString:PARAMETER_BCASHOUT] || [local_apiType isEqualToString:PARAMETER_WALLET_TO_BANK])
                    {
                        if ([mode isEqualToString:@"SMS"]){
                            NSString *feeAmount = [Template getFeeForAmount:[smsFormatterDictionary objectForKey:PARAMETER21] andTransactionType:[smsFormatterDictionary objectForKey:PARAMETER19]];
                            [webServiceRequestInputDetails setValue:feeAmount forKey:PARAMETER24];
                            NSLog(@"Fee amount is..%@",feeAmount);
                        }
                        else {
                            [webServiceRequestInputDetails setValue:webServiceDataObject.FeeAmount forKey:@"FeeAmount"];
                            [webServiceRequestInputDetails setValue:webServiceDataObject.FeeAmount forKey:TRANSACTION_DATA_TRANSACTION_FEE];
                        }
                        
                        [webServiceRequestInputDetails setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"topupType"] forKey:PARAMETER20];
                        
                        if([webServiceRequestInputDetails objectForKey:@"MyNumber"])
                        {
                            [webServiceRequestInputDetails setValue:[webServiceRequestInputDetails objectForKey:@"MyNumber"] forKey:PARAMETER19];
                        }
                        
                        if([webServiceRequestInputDetails objectForKey:@"account_number"])
                        {
                            [webServiceRequestInputDetails setValue:[webServiceRequestInputDetails objectForKey:@"account_number"] forKey:PARAMETER19];
                        }
                        
                        if(![[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER] isEqualToString:@"(null)"])
                        {
                            if ([[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER] length] != 0) {
                                [webServiceRequestInputDetails setValue:[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER] forKey:PARAMETER19];
                            }
                        }
                        
                        if (webServiceDataObject.TxnAmount) {
                            [webServiceRequestInputDetails setValue:webServiceDataObject.TxnAmount forKey:PARAMETER21];
                        }
                        
                        if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""])
                        {
                            Class nextClass = NSClassFromString(local_nextTemplate);
                            id object = nil;
                            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                            {
                                if (local_selectedIndex >=0)
                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                else
                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                [[[local_id superview] superview] addSubview:(UIView *)object];
                            }
                            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                            {
                                object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:webServiceRequestInputDetails];
                                [self.navigationController pushViewController:object animated:NO];
                            }
                        }
                    }
                    else
                        if ([local_apiType isEqualToString:PARAMETER_OBOANY]  || [local_apiType isEqualToString:PARAMETER_NP2P] || [local_apiType isEqualToString:PARAMETER_PAY_TO_MERCHANT])
                        {
                            if(![[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FULL_NAME] isEqualToString:@"(null)"])
                            {
                                if ([[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FULL_NAME] length] != 0) {
                                    [webServiceRequestInputDetails setValue:[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FULL_NAME] forKey:PARAMETER18];
                                }
                            }
                            
                            if(![[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_BANK_IFSC_CODE] isEqualToString:@"(null)"])
                            {
                                if ([[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_BANK_IFSC_CODE] length] != 0) {
                                    [webServiceRequestInputDetails setValue:[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_BANK_IFSC_CODE] forKey:PARAMETER20];
                                }
                            }
                            
                            if(![[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_TRANSACTION_ACCOUNT_NUMBER] isEqualToString:@"(null)"])
                            {
                                if ([[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_TRANSACTION_ACCOUNT_NUMBER] length] != 0) {
                                    [webServiceRequestInputDetails setValue:[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_TRANSACTION_ACCOUNT_NUMBER] forKey:PARAMETER34];
                                }
                            }
                            if(![[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_BANK_MMID] isEqualToString:@"(null)"])
                            {
                                if ([[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_BANK_MMID] length] != 0) {
                                    [webServiceRequestInputDetails setValue:[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_BANK_MMID] forKey:PARAMETER20];
                                }
                            }
                            if(![[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_BANK] isEqualToString:@"(null)"])
                            {
                                if ([[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_BANK] length] != 0) {
                                    [webServiceRequestInputDetails setValue:[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_BANK] forKey:PARAMETER19];
                                }
                            }
                            
                            if(![[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_BANK_NAME] isEqualToString:@"(null)"])
                            {
                                if ([[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_BANK_NAME] length] != 0) {
                                    [webServiceRequestInputDetails setValue:[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_BANK_NAME] forKey:PARAMETER19];
                                }
                            }
                            
                            if(![[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FIRST_NAME] isEqualToString:@"(null)"])
                            {
                                if ([[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FIRST_NAME] length] != 0) {
                                    [webServiceRequestInputDetails setValue:[webServiceRequestInputDetails objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FIRST_NAME] forKey:PARAMETER18];
                                }
                            }
                            
                            //For Customer Phone Number
                            NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
                            [webServiceRequestInputDetails setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
                            
                            
                            //SMS Support
                            if ([mode isEqualToString:@"SMS"]) {
                                NSString *feeAmount = [Template getFeeForAmount:[smsFormatterDictionary objectForKey:PARAMETER21] andTransactionType:[smsFormatterDictionary objectForKey:PARAMETER19]];
                                [webServiceRequestInputDetails setValue:feeAmount forKey:PARAMETER24];
                            }
                            else {
                                [webServiceRequestInputDetails setValue:webServiceDataObject.FeeAmount forKey:PARAMETER24];
                                [webServiceRequestInputDetails setValue:webServiceDataObject.FeeAmount forKey:TRANSACTION_DATA_TRANSACTION_FEE];
                                if (webServiceDataObject.TxnAmount) {
                                    [webServiceRequestInputDetails setValue:webServiceDataObject.TxnAmount forKey:PARAMETER21];
                                }
                            }
                            
                            
                            Class nextClass = NSClassFromString(local_nextTemplate);
                            id object = nil;
                            
                            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                            {
                                if (local_selectedIndex >=0)
                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                else
                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                
                                [[[local_id superview] superview] addSubview:(UIView *)object];
                                
                            }
                            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                            {
                                object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:nil dataDictionary:webServiceRequestInputDetails];
                                
                                UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                                [vc.navigationController pushViewController:object animated:NO];
                            }
                        }
                        else if ([local_apiType isEqualToString:PARAMETER_BILL_PAY])
                        {
                            
                            if ([mode isEqualToString:@"SMS"]) {
                                NSString *feeAmount = [Template getFeeForAmount:[smsFormatterDictionary objectForKey:PARAMETER21] andTransactionType:[smsFormatterDictionary objectForKey:PARAMETER19]];
                                [webServiceRequestInputDetails setValue:feeAmount forKey:PARAMETER24];
                            }
                            else
                                [webServiceRequestInputDetails setValue:webServiceDataObject.FeeAmount forKey:TRANSACTION_DATA_TRANSACTION_FEE];
                            
                            Class nextClass = NSClassFromString(local_nextTemplate);
                            id object = nil;
                            
                            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                            {
                                if (local_selectedIndex >=0)
                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                else
                                    object= [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                [[local_id superview] addSubview:(UIView *)object];
                                
                            }
                            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                            {
                                object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:nil dataDictionary:webServiceRequestInputDetails];
                                
                                UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                                [vc.navigationController pushViewController:object animated:NO];
                            }
                        }
                        else
                        {
                            [webServiceRequestInputDetails setObject:webServiceDataObject.FeeAmount forKey:PARAMETER24];
                        }
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_FETCH_FEE_T3])
                {
                    if ([mode isEqualToString:@"SMS"] || (netStatus == NotReachable)) {
                        NSString *feeAmount = [Template getFeeForAmount:[smsFormatterDictionary objectForKey:PARAMETER21] andTransactionType:[smsFormatterDictionary objectForKey:PARAMETER19]];
                        [webServiceRequestInputDetails setValue:feeAmount forKey:PARAMETER24];
                    }
                    
                    if ([[webServiceRequestInputDetails objectForKey:PARAMETER18] isEqualToString:PARAMETER_BANK_UNLOAD_OTHERS_T3]) {
                        [[NSUserDefaults standardUserDefaults] setObject:[webServiceRequestInputDetails objectForKey:@"account_number"] forKey:@"caccNum"];
                        [[NSUserDefaults standardUserDefaults] setObject:[webServiceRequestInputDetails objectForKey:@"cashoutBankName"] forKey:@"cbankName"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        
                    }
                    if ([[webServiceRequestInputDetails objectForKey:PARAMETER18] isEqualToString:PARAMETER_VIRAL_CASH_OUT_T3]) {
                        [[NSUserDefaults standardUserDefaults] setObject:[webServiceRequestInputDetails  objectForKey:PARAMETER9] forKey:@"benifNumber"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        [webServiceRequestInputDetails setValue:webServiceDataObject.FeeAmount forKey:@"FeeAmount"];
                        [webServiceRequestInputDetails setValue:webServiceDataObject.FeeAmount forKey:TRANSACTION_DATA_TRANSACTION_FEE];
                        
                    }
                    else {
                        if ([[webServiceRequestInputDetails  objectForKey:@"PaymentDetails3"]length]>0) {
                            [webServiceRequestInputDetails setValue:[webServiceRequestInputDetails  objectForKey:PARAMETER19] forKey:PARAMETER9];
                        }
                        [webServiceRequestInputDetails setValue:webServiceDataObject.PaymentDetails1 forKey:PARAMETER17];
                        [webServiceRequestInputDetails setValue:webServiceDataObject.FeeAmount forKey:@"FeeAmount"];
                        [webServiceRequestInputDetails setValue:webServiceDataObject.FeeAmount forKey:TRANSACTION_DATA_TRANSACTION_FEE];
                    }
                    for (int i=0; i<[local_ContentArray count]>0; i++) {
                        if ([[[local_ContentArray objectAtIndex:i]objectForKey:@"value_inputtype"] isEqualToString:PARAMETER18]) {
                            [webServiceRequestInputDetails setValue:[[local_ContentArray objectAtIndex:i]objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i]objectForKey:@"value_inputtype"]];
                        }
                    }
                    //                if ([[[NSUserDefaults ≥]objectForKey:@"benifNumber"]length]>0) {
                    //                    [webServiceRequestInputDetails setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"benifNumber"] forKey:PARAMETER9];
                    //                }
                    if (([[[NSUserDefaults standardUserDefaults]objectForKey:@"caccNum"]length]>0) || ([[[NSUserDefaults standardUserDefaults]objectForKey:@"cbankName"]length]>0)) {
                        [webServiceRequestInputDetails setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"caccNum"] forKey:PARAMETER19];
                        [webServiceRequestInputDetails setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"cbankName"] forKey:PARAMETER18];
                    }
                    if ([mode isEqualToString:@"SMS"] || (netStatus == NotReachable)) {
                        [webServiceRequestInputDetails setValue:@"0.00" forKey:PARAMETER24];
                    }
                    NSLog(@"Unload Others Dictionary Values are..%@",webServiceRequestInputDetails);
                    
                    
                    Class nextClass = NSClassFromString(local_nextTemplate);
                    id object = nil;
                    
                    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                    {
                        if (local_selectedIndex >=0)
                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                        else
                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                        
                        [[[local_id superview] superview] addSubview:(UIView *)object];
                        
                    }
                    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                    {
                        object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:nil dataDictionary:webServiceRequestInputDetails];
                        
                        UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                        [vc.navigationController pushViewController:object animated:NO];
                    }
                }
                else
                    if([local_processorCode isEqualToString:PROCESSOR_CODE_AGENT_DETAILS])
                    {
                        if ([mode isEqualToString:@"SMS"]) {
                            [smsFormatterDictionary removeObjectForKey:PARAMETER35];
                            NSString *jsonDataStr =  NSLocalizedStringFromTableInBundle([Template getFormatedKey:smsFormatterDictionary],@"DropDownTemplate",[NSBundle mainBundle], nil);
                            ;
                            NSError *jsonError;
                            NSMutableDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[jsonDataStr dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&jsonError];
                            NSString *detailedString = [NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@\n%@\n%@\n",[dataDict objectForKey:@"bn"],[dataDict objectForKey:@"adl1"],[dataDict objectForKey:@"adl2"],[dataDict objectForKey:@"adl3"],[dataDict objectForKey:@"city"],[dataDict objectForKey:@"pincd"],@"India"];
                            
                            [webServiceRequestInputDetails setObject:detailedString forKey:@"detailedStr"];
                            [webServiceRequestInputDetails addEntriesFromDictionary:dataDict];
                            [webServiceRequestInputDetails setObject:customerPhoneNumber forKey:PARAMETER9];
                        }
                        else
                        {
                            [[NSUserDefaults standardUserDefaults] setValue:webServiceDataObject.Country  forKey:@"Country"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            NSData *jsonData = [webServiceDataObject.PaymentDetails1 dataUsingEncoding:NSUTF8StringEncoding];
                            NSError *jsonError;
                            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError];
                            
                            NSString *detailedString = [NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@\n%@\n%@\n",[dataDict objectForKey:@"bn"],[dataDict objectForKey:@"adl1"],[dataDict objectForKey:@"adl2"],[dataDict objectForKey:@"adl3"],[dataDict objectForKey:@"city"],[dataDict objectForKey:@"pincd"],[[NSUserDefaults standardUserDefaults]objectForKey:@"Country"]];
                            
                            [webServiceRequestInputDetails setObject:detailedString forKey:@"detailedStr"];
                            [webServiceRequestInputDetails addEntriesFromDictionary:dataDict];
                            [webServiceRequestInputDetails setObject:webServiceDataObject.CustomerPhoneNumber forKey:PARAMETER9];
                        }
                        
                        Class nextClass = NSClassFromString(local_nextTemplate);
                        id object = nil;
                        
                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                        {
                            if (local_selectedIndex >=0)
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                            else
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:processCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                            
                            [[[local_id superview] superview] addSubview:(UIView *)object];
                            
                        }
                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                        {
                            object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:nil dataDictionary:webServiceRequestInputDetails];
                            
                            UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                            [vc.navigationController pushViewController:object animated:NO];
                        }
                        
                    }
                    else
                        if ([local_processorCode isEqualToString:PROCESSOR_CODE_P2P_REGISTERED_BENEFICIARY_MOBILE_NUMBER])
                        {
                            if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""])
                            {
                                Class nextClass = NSClassFromString(local_nextTemplate);
                                id object = nil;
                                
                                if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                {
                                    if (local_selectedIndex >=0)
                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                    else
                                        object= [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                    
                                    [[local_id superview]  addSubview:(UIView *)object];
                                    
                                }
                                else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                {
                                    object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:nil dataDictionary:local_dataDictionary];
                                    
                                    UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                                    [vc.navigationController pushViewController:object animated:NO];
                                }
                                [[NSNotificationCenter defaultCenter] postNotificationName:POPUPTEMPLATE1 object:PROCESSOR_CODE_BANK];
                                
                                [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                            }
                            else
                            {
                                [self removeViewsWithClassType:YES];
                                [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                            }
                        }
                        else
                            if ([local_processorCode isEqualToString:PROCESSOR_CODE_GROUPSAVING_CASH_IN_T3]&&[[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_GROUPSAVING_CASH_IN_T3])
                            {
                                if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""])
                                {
                                    Class nextClass = NSClassFromString(local_nextTemplate);
                                    id object = nil;
                                    
                                    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                    {
                                        if (local_selectedIndex >=0)
                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                        else
                                            object= [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                        
                                        [[local_id superview]  addSubview:(UIView *)object];
                                        
                                    }
                                    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                    {
                                        object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:nil dataDictionary:local_dataDictionary];
                                        
                                        UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                                        [vc.navigationController pushViewController:object animated:NO];
                                    }
                                }
                            }
                            else
                                if ([local_processorCode isEqualToString:PROCESSOR_CODE_CUSTOMER_CASH_IN_T3]&&[[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_CUSTOMER_CASH_IN_T3])
                                {
                                    if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""])
                                    {
                                        Class nextClass = NSClassFromString(local_nextTemplate);
                                        id object = nil;
                                        
                                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                        {
                                            if (local_selectedIndex >=0)
                                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                            else
                                                object= [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                            
                                            [[local_id superview]  addSubview:(UIView *)object];
                                            
                                        }
                                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                        {
                                            object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:nil dataDictionary:local_dataDictionary];
                                            
                                            UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                                            [vc.navigationController pushViewController:object animated:NO];
                                        }
                                    }
                                }
                                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CASHOUT_REGISTERED_T3])
                                {
                                    if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""])
                                    {
                                        Class nextClass = NSClassFromString(local_nextTemplate);
                                        id object = nil;
                                        
                                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                        {
                                            if (local_selectedIndex >=0)
                                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                            else
                                                object= [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                            
                                            [[local_id superview]  addSubview:(UIView *)object];
                                            
                                        }
                                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                        {
                                            object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:nil dataDictionary:local_dataDictionary];
                                            
                                            UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                                            [vc.navigationController pushViewController:object animated:NO];
                                        }
                                    }
                                }
                                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CASHOUT_VIRAL_T3])
                                {
                                    if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""])
                                    {
                                        Class nextClass = NSClassFromString(local_nextTemplate);
                                        id object = nil;
                                        
                                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                        {
                                            if (local_selectedIndex >=0)
                                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                            else
                                                object= [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                            
                                            [[local_id superview]  addSubview:(UIView *)object];
                                            
                                        }
                                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                        {
                                            object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:nil dataDictionary:local_dataDictionary];
                                            
                                            UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                                            [vc.navigationController pushViewController:object animated:NO];
                                        }
                                    }
                                }
                                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_LOAD_BANK_T3])
                                {
                                    if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""])
                                    {
                                        Class nextClass = NSClassFromString(local_nextTemplate);
                                        id object = nil;
                                        
                                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                        {
                                            if (local_selectedIndex >=0)
                                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                            else
                                                object= [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                            
                                            [[local_id superview]  addSubview:(UIView *)object];
                                            
                                        }
                                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                        {
                                            object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:nil dataDictionary:local_dataDictionary];
                                            
                                            UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                                            [vc.navigationController pushViewController:object animated:NO];
                                        }
                                    }
                                }
                                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_UNLOAD_LINKED_BANK_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_UNLOAD_LINKED_BANK_T3])
                                {
                                    if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""])
                                    {
                                        Class nextClass = NSClassFromString(local_nextTemplate);
                                        id object = nil;
                                        
                                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                        {
                                            if (local_selectedIndex >=0)
                                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                            else
                                                object= [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                            
                                            [[local_id superview]  addSubview:(UIView *)object];
                                            
                                        }
                                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                        {
                                            object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:nil dataDictionary:local_dataDictionary];
                                            
                                            UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                                            [vc.navigationController pushViewController:object animated:NO];
                                        }
                                    }
                                }
                                else
                                    if ([local_processorCode isEqualToString:PROCESSOR_CODE_PULL_DECLINE_REQUEST_T3]&&[[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_PULL_DECLINE_REQUEST_T3])
                                    {
                                        if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""])
                                        {
                                            Class nextClass = NSClassFromString(local_nextTemplate);
                                            id object = nil;
                                            
                                            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                            {
                                                if (local_selectedIndex >=0)
                                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                else
                                                    object= [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                
                                                [[local_id superview]  addSubview:(UIView *)object];
                                                
                                            }
                                            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                            {
                                                object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:nil dataDictionary:local_dataDictionary];
                                                
                                                UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                                                [vc.navigationController pushViewController:object animated:NO];
                                            }
                                        }
                                    }
                                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_PULL_ACCEPT_REQUEST_T3]&&[[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_PULL_ACCEPT_REQUEST_T3])
                                    {
                                        if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""])
                                        {
                                            Class nextClass = NSClassFromString(local_nextTemplate);
                                            id object = nil;
                                            
                                            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                            {
                                                if (local_selectedIndex >=0)
                                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                else
                                                    object= [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                
                                                [[local_id superview]  addSubview:(UIView *)object];
                                                
                                            }
                                            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                            {
                                                object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:nil dataDictionary:local_dataDictionary];
                                                
                                                UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                                                [vc.navigationController pushViewController:object animated:NO];
                                            }
                                        }
                                    }
            
                                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_UNLOAD_OTHER_BANK_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_UNLOAD_OTHER_BANK_T3])
                                    {
                                        if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""])
                                        {
                                            Class nextClass = NSClassFromString(local_nextTemplate);
                                            id object = nil;
                                            
                                            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                            {
                                                if (local_selectedIndex >=0)
                                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                else
                                                    object= [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                
                                                [[local_id superview]  addSubview:(UIView *)object];
                                                
                                            }
                                            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                            {
                                                object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:nil dataDictionary:local_dataDictionary];
                                                
                                                UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                                                [vc.navigationController pushViewController:object animated:NO];
                                            }
                                        }
                                    }
                                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_BANK]){
                                        if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""]){
                                            Class nextClass = NSClassFromString(local_nextTemplate);
                                            id object = nil;
                                            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                            {
                                                if (local_selectedIndex >=0)
                                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                else
                                                    object= [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                
                                                [[local_id superview]  addSubview:(UIView *)object];
                                                
                                            }
                                            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                            {
                                                object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:nil dataDictionary:webServiceRequestInputDetails];
                                                
                                                UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                                                [vc.navigationController pushViewController:object animated:NO];
                                            }
                                        }
                                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                                        [self removeViewsWithClassType:YES];
                                        [[NSNotificationCenter defaultCenter] postNotificationName:POPUPTEMPLATE1 object:PROCESSOR_CODE_BANK];
                                        [[NSNotificationCenter defaultCenter] postNotificationName:CHILD_TEMPLATE2 object:PROCESSOR_CODE_BANK];
                                    }
                                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CAHNGE_PIN])
                                    {
                                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                                        [self performSelector:@selector(moveToPreviousScreen) withObject:nil afterDelay:3];
                                    }
                                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CHANGE_MPIN_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_CHANGE_MPIN_T3]){
                                        //  [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                                        [self performSelector:@selector(moveToPreviousScreen) withObject:nil afterDelay:3];
                                    }
                                    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_FETCH_BILLER_ALL])
                                    {
                                        
                                        NSMutableArray *finalArray;
                                        if ([mode isEqualToString:@"SMS"]) {
                                            NSString *bn = @"bn";
                                            NSString *type = @"type";
                                            NSString *sn = @"sn";
                                            NSString *bmi = @"bmi";
                                            NSString *a1 = @"A1";
                                            NSString *a2 = @"A2";
                                            NSString *a3 = @"A3";
                                            NSString *a4 = @"A4";
                                            NSString *a5 = @"A5";
                                            NSString *loc = @"loc";
                                            NSString *cat = @"cat";
                                            
                                            WebSericeUtils *webUtils = [[WebSericeUtils alloc] init];
                                            NSMutableDictionary *webUtilsValues = [[NSMutableDictionary alloc] initWithDictionary:[webUtils getWebServiceBundleObjectWithProccessorCode:local_processorCode withInputDataModel:webServiceRequestInputDetails]];
                                            [webUtilsValues removeObjectForKey:PARAMETER35];
                                            
                                            NSLog(@"Format : %@",[Template getFormatedKey:webUtilsValues]);
                                            
                                            NSString *jsonDataStr =  NSLocalizedStringFromTableInBundle([Template getFormatedKey:webUtilsValues],@"DropDownTemplate",[NSBundle mainBundle], nil);
                                            
                                            if (![jsonDataStr isEqualToString:[Template getFormatedKey:webServiceRequestInputDetails]] && !(jsonDataStr.length == 0)) {
                                                NSData *jsonData = [jsonDataStr dataUsingEncoding:NSUTF8StringEncoding];
                                                NSError *jsonError;
                                                NSArray *local_dataArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError];
                                                
                                                finalArray = [[NSMutableArray alloc] init];
                                                for (int i=0; i<[local_dataArray count]; i++)
                                                {
                                                    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                                                    
                                                    if ([[local_dataArray objectAtIndex:i] objectForKey:bn])
                                                    {
                                                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:bn] forKey:BILLERNAME];
                                                    }
                                                    if ([[local_dataArray objectAtIndex:i] objectForKey:type])
                                                    {
                                                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:type] forKey:BILLERTYPE];
                                                    }
                                                    if ([[local_dataArray objectAtIndex:i] objectForKey:sn])
                                                    {
                                                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:sn] forKey:BILLERSHORTNAME];
                                                    }
                                                    if ([[local_dataArray objectAtIndex:i] objectForKey:bmi])
                                                    {
                                                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:bmi] forKey:BILLERMASTERID];
                                                    }
                                                    if ([[local_dataArray objectAtIndex:i] objectForKey:a1])
                                                    {
                                                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a1] forKey:BILLERA1];
                                                    }
                                                    if ([[local_dataArray objectAtIndex:i] objectForKey:a2])
                                                    {
                                                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a2] forKey:BILLERA2];
                                                    }
                                                    if ([[local_dataArray objectAtIndex:i] objectForKey:a3])
                                                    {
                                                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a3] forKey:BILLERA3];
                                                    }
                                                    if ([[local_dataArray objectAtIndex:i] objectForKey:a4])
                                                    {
                                                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a4] forKey:BILLERA4];
                                                    }
                                                    if ([[local_dataArray objectAtIndex:i] objectForKey:a5])
                                                    {
                                                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a5] forKey:BILLERA5];
                                                    }
                                                    if ([[local_dataArray objectAtIndex:i] objectForKey:loc])
                                                    {
                                                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:loc] forKey:BILLERLOCATION];
                                                    }
                                                    if ([[local_dataArray objectAtIndex:i] objectForKey:cat])
                                                    {
                                                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:cat] forKey:BILLERCATEGORY];
                                                    }
                                                    
                                                    [finalArray addObject:tempDict];
                                                }
                                            }
                                        }
                                        else
                                        {
                                            NSString *bn = @"bn";
                                            NSString *type = @"type";
                                            NSString *sn = @"sn";
                                            NSString *bmi = @"bmi";
                                            NSString *a1 = @"A1";
                                            NSString *a2 = @"A2";
                                            NSString *a3 = @"A3";
                                            NSString *a4 = @"A4";
                                            NSString *a5 = @"A5";
                                            NSString *loc = @"loc";
                                            NSString *cat = @"cat";
                                            NSString *billerId = @"billerId";
                                            
                                            NSData *jsonData = [webServiceDataObject.PaymentDetails2 dataUsingEncoding:NSUTF8StringEncoding];
                                            NSError *jsonError;
                                            NSArray *local_dataArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError];
                                            
                                            finalArray = [[NSMutableArray alloc] init];
                                            for (int i=0; i<[local_dataArray count]; i++)
                                            {
                                                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                                                
                                                if ([[local_dataArray objectAtIndex:i] objectForKey:bn])
                                                {
                                                    [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:bn] forKey:BILLERNAME];
                                                }
                                                if ([[local_dataArray objectAtIndex:i] objectForKey:billerId])
                                                {
                                                    [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:billerId] forKey:BILLERID];
                                                }
                                                if ([[local_dataArray objectAtIndex:i] objectForKey:type])
                                                {
                                                    [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:type] forKey:BILLERTYPE];
                                                }
                                                if ([[local_dataArray objectAtIndex:i] objectForKey:sn])
                                                {
                                                    [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:sn] forKey:BILLERSHORTNAME];
                                                }
                                                if ([[local_dataArray objectAtIndex:i] objectForKey:bmi])
                                                {
                                                    [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:bmi] forKey:BILLERMASTERID];
                                                }
                                                if ([[local_dataArray objectAtIndex:i] objectForKey:a1])
                                                {
                                                    [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a1] forKey:BILLERA1];
                                                }
                                                if ([[local_dataArray objectAtIndex:i] objectForKey:a2])
                                                {
                                                    [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a2] forKey:BILLERA2];
                                                }
                                                if ([[local_dataArray objectAtIndex:i] objectForKey:a3])
                                                {
                                                    [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a3] forKey:BILLERA3];
                                                }
                                                if ([[local_dataArray objectAtIndex:i] objectForKey:a4])
                                                {
                                                    [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a4] forKey:BILLERA4];
                                                }
                                                if ([[local_dataArray objectAtIndex:i] objectForKey:a5])
                                                {
                                                    [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a5] forKey:BILLERA5];
                                                }
                                                if ([[local_dataArray objectAtIndex:i] objectForKey:loc])
                                                {
                                                    [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:loc] forKey:BILLERLOCATION];
                                                }
                                                if ([[local_dataArray objectAtIndex:i] objectForKey:cat])
                                                {
                                                    [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:cat] forKey:BILLERCATEGORY];
                                                }
                                                
                                                [finalArray addObject:tempDict];
                                            }
                                        }
                                        
                                        if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""])
                                        {
                                            Class nextClass = NSClassFromString(local_nextTemplate);
                                            id object = nil;
                                            
                                            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                            {
                                                if (local_selectedIndex >=0)
                                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                else
                                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:webServiceDataObject.ProcessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                
                                                [[[local_id superview] superview] addSubview:(UIView *)object];
                                            }
                                            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                            {
                                                object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:finalArray dataDictionary:nil];
                                                
                                                UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                                                [vc.navigationController pushViewController:object animated:NO];
                                            }
                                        }
                                    }
                                    else
                                        if ([local_processorCode isEqualToString:PROCESSOR_CODE_FETCH_BILLS])
                                        {
                                            if (webServiceDataObject.PaymentDetails2)
                                            {
                                                NSData *jsonData = [webServiceDataObject.PaymentDetails2 dataUsingEncoding:NSUTF8StringEncoding];
                                                NSError *jsonError;
                                                NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError];
                                                
                                                if (![dataDictionary count])
                                                {
                                                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_noresults_found_message", nil)] andErrorCode:nil];
                                                    
                                                }
                                                else
                                                {
                                                    NSString *nn = @"nn";
                                                    NSString *dd = @"dd";
                                                    NSString *am = @"am";
                                                    NSString *bpc = @"bpc";
                                                    NSString *bt = @"bt";
                                                    NSString *bn = @"bn";
                                                    NSString *bi = @"bi";
                                                    NSString *loc = @"loc";
                                                    NSString *a1 = @"A1";
                                                    NSString *a2 = @"A2";
                                                    NSString *a3 = @"A3";
                                                    NSString *a4 = @"A4";
                                                    NSString *a5 = @"A5";
                                                    NSString *ref1 = @"Ref1";
                                                    NSString *ref2 = @"Ref2";
                                                    NSString *ref3 = @"Ref3";
                                                    NSString *ref4 = @"Ref4";
                                                    NSString *ref5 = @"Ref5";
                                                    
                                                    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                                                    
                                                    if ([dataDictionary objectForKey:nn])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:nn] forKey:BILLERNICKNAME];
                                                    }
                                                    if ([dataDictionary objectForKey:bn])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:bn] forKey:BILLERNAME];
                                                    }
                                                    if ([dataDictionary objectForKey:dd])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:dd] forKey:BILLERDUEDATE];
                                                    }
                                                    
                                                    if ([dataDictionary objectForKey:am])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:am] forKey:PARAMETER21];
                                                    }
                                                    
                                                    if ([dataDictionary objectForKey:bpc])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:bpc] forKey:BILLPAYCODE];
                                                    }
                                                    
                                                    if ([dataDictionary objectForKey:bt])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:bt] forKey:BILLERTYPE];
                                                    }
                                                    if ([dataDictionary objectForKey:bi])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:bi] forKey:BILLERID];
                                                    }
                                                    
                                                    if ([dataDictionary objectForKey:loc])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:loc] forKey:BILLERLOCATION];
                                                    }
                                                    
                                                    if ([dataDictionary objectForKey:a1])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:a1] forKey:BILLERA1];
                                                    }
                                                    if ([dataDictionary objectForKey:a2])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:a2] forKey:BILLERA2];
                                                    }
                                                    if ([dataDictionary objectForKey:a3])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:a3] forKey:BILLERA3];
                                                    }
                                                    if ([dataDictionary objectForKey:a4])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:a4] forKey:BILLERA4];
                                                    }
                                                    if ([dataDictionary objectForKey:a5])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:a5] forKey:BILLERA5];
                                                    }
                                                    
                                                    if ([dataDictionary objectForKey:ref1])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:ref1] forKey:BILLERREFERENCE1];
                                                    }
                                                    if ([dataDictionary objectForKey:ref2])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:ref2] forKey:BILLERREFERENCE2];
                                                    }
                                                    if ([dataDictionary objectForKey:ref3])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:ref3] forKey:BILLERREFERENCE3];
                                                    }
                                                    if ([dataDictionary objectForKey:ref4])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:ref4] forKey:BILLERREFERENCE4];
                                                    }
                                                    if ([dataDictionary objectForKey:ref5])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:ref5] forKey:BILLERREFERENCE5];
                                                    }
                                                    
                                                    if ([dataDictionary objectForKey:ref5])
                                                    {
                                                        [tempDict setObject:[dataDictionary objectForKey:ref5] forKey:BILLERREFERENCE5];
                                                    }
                                                    
                                                    if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""])
                                                    {
                                                        Class nextClass = NSClassFromString(local_nextTemplate);
                                                        id object = nil;
                                                        
                                                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                                        {
                                                            if (local_selectedIndex >=0)
                                                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                            else
                                                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:tempDict withProcessorCode:webServiceDataObject.ProcessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                            
                                                            [[[local_id superview] superview] addSubview:(UIView *)object];
                                                        }
                                                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                                        {
                                                            object= [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:processCode dataArray:nil dataDictionary:nil];
                                                            
                                                            UIViewController *vc = [(UINavigationController *)self.navigationController visibleViewController];
                                                            [vc.navigationController pushViewController:object animated:NO];
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_LOGIN_T3])
                                        {
                                            if ([webServiceDataObject.Reference5 isEqualToString:WEBSERVICE_REFERENCE_DATA]) {
                                                [self addUpdateDeviceIDView];
                                            }
                                            else{
                                                activityIndicator.hidden =YES;
                                                [activityIndicator startActivityIndicator];
                                                
                                                Class nextClass = NSClassFromString(local_nextTemplate);
                                                id object = nil;
                                                if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                                {
                                                    if (local_selectedIndex >=0)
                                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                    else
                                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                    
                                                    [self.view addSubview:(UIView *)object];
                                                }
                                                else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                                {
                                                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_nextTemplatePropertyFileName withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                    UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:object];
                                                    [navi setNavigationBarHidden:YES];
                                                    [[(AppDelegate*)[[UIApplication sharedApplication] delegate] window] setRootViewController:navi];
                                                }
                                            }
                                        }
                                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_ACTIVATION_T3])
                                        {
                                            activityIndicator.hidden =YES;
                                            [activityIndicator startActivityIndicator];
                                            
                                            Class nextClass = NSClassFromString(local_nextTemplate);
                                            id object = nil;
                                            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                            {
                                                if (local_selectedIndex >=0)
                                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                else
                                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                
                                                [self.view addSubview:(UIView *)object];
                                            }
                                            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                            {
                                                object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_nextTemplatePropertyFileName withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:object];
                                                [navi setNavigationBarHidden:YES];
                                                [[(AppDelegate*)[[UIApplication sharedApplication] delegate] window] setRootViewController:navi];
                                            }
                                        }
            
                                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_FORGOT_MPIN_T3] && [[local_dataDictionary objectForKey:PARAMETER13]isEqualToString:TRANSACTION_CODE_FORGOT_MPIN_T3])
                                        {
                                            activityIndicator.hidden = YES;
                                            [activityIndicator stopActivityIndicator];
                                            
                                            
                                            [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                                            
                                            double delayInSeconds = 2.0;
                                            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                                            
                                            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                                
                                                Class nextClass = NSClassFromString(local_nextTemplate);
                                                id object = nil;
                                                
                                                if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                                {
                                                    if (local_selectedIndex >=0)
                                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                    else
                                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                    
                                                    [self.view addSubview:(UIView *)object];
                                                }
                                                else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                                {
                                                    object = [[NSClassFromString(local_nextTemplate) alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:PROCESSOR_CODE_LOGIN withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                    [self.navigationController pushViewController:object animated:NO];
                                                }
                                            });
                                            
                                        }
                                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_RESET_PIN_FORGOT_MPIN])
                                        {
                                            activityIndicator.hidden = YES;
                                            [activityIndicator stopActivityIndicator];
                                            
                                            Class nextClass = NSClassFromString(local_nextTemplate);
                                            id object = nil;
                                            
                                            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                            {
                                                if (local_selectedIndex >=0)
                                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                else
                                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                
                                                [self.view addSubview:(UIView *)object];
                                            }
                                            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                            {
                                                object = [[NSClassFromString(local_nextTemplate) alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:PROCESSOR_CODE_LOGIN withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                [self.navigationController pushViewController:object animated:NO];
                                            }
                                            
                                        }
                                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_SELF_REG]){
                                            Class nextClass = NSClassFromString(local_nextTemplate);
                                            id object = nil;
                                            
                                            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                            {
                                                if (local_selectedIndex >=0)
                                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                else
                                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                
                                                [self.view addSubview:(UIView *)object];
                                            }
                                            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                            {
                                                object = [[NSClassFromString(local_nextTemplate) alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                [self.navigationController pushViewController:object animated:NO];
                                            }
                                        }
                                        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_SELF_REG_DIGITAL_KYC]){
                                            
                                            if (webServiceDataObject.ErrorCode) {
                                                PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:@"Erorr" andMessage:webServiceDataObject.Remark withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                [[UIApplication sharedApplication].keyWindow addSubview:popup];
                                            }else{
                                                PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:@"Success" andMessage:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_upload_docs_success", nil)]  withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                [[UIApplication sharedApplication].keyWindow addSubview:popup];
                                            }
                                            //                                        NSString *uploadedDocuments = webServiceDataObject.PaymentDetails4;
                                            //                                        int uploadedCount = (int)[[uploadedDocuments componentsSeparatedByString:@","] count];
                                            //                                        int totalDocs = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"totalDocuments"];
                                            //                                        NSLog(@"Total Docs count is...%d",totalDocs);
                                            //                                        NSString *localizedString;
                                            //
                                            //                                        if (webServiceDataObject.ErrorCode) {
                                            //                                            if (totalDocs != uploadedCount)
                                            //                                            {
                                            //                                                NSString *errorCode = webServiceDataObject.ErrorCode;
                                            //                                                NSString *appendingString;
                                            //                                                NSString *message;
                                            //                                                NSString *technicalErrorMessage = NSLocalizedStringFromTableInBundle(@"999",@"ExternalErrorMessages",[NSBundle mainBundle], nil);
                                            //
                                            //                                                if (errorCode.length != 0) {
                                            //                                                    if([NSLocalizedStringFromTableInBundle(@"application_is_error_code_required_for_error_message",@"GeneralSettings",[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                                            //                                                        appendingString = [[[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_label_prefix", nil)] stringByAppendingString:errorCode] stringByAppendingString:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_error_msg_seperator", nil)]];
                                            //                                                    else
                                            //                                                        appendingString=@"";
                                            //                                                }
                                            //                                                else
                                            //                                                {
                                            //                                                    if([NSLocalizedStringFromTableInBundle(@"application_is_error_code_required_for_error_message",@"GeneralSettings",[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                                            //                                                        appendingString = [[[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_label_prefix", nil)] stringByAppendingString:@"999"] stringByAppendingString:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_error_msg_seperator", nil)]];
                                            //                                                    else
                                            //                                                        appendingString=@"";
                                            //                                                }
                                            //
                                            //                                                if (webServiceDataObject.Reference5)
                                            //                                                    message = [Template formatMessageWithMessage:NSLocalizedStringFromTableInBundle(webServiceDataObject.ErrorCode,@"ExternalErrorMessages",[NSBundle mainBundle], nil) withReferenceValues:webServiceDataObject.Reference5];
                                            //                                                else
                                            //                                                    message =[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(webServiceDataObject.ErrorCode,@"ExternalErrorMessages",[NSBundle mainBundle], nil)];
                                            //
                                            //                                                NSString *propertyFileString = NSLocalizedStringFromTableInBundle(errorCode,@"ExternalErrorMessages",[NSBundle mainBundle], nil);
                                            //
                                            //                                                if ([propertyFileString isEqualToString:webServiceDataObject.ErrorCode] || propertyFileString.length == 0) {
                                            //                                                    NSString *displayString = [appendingString stringByAppendingString:technicalErrorMessage];
                                            //                                                    [self errorCodeHandlingInXML:displayString andMessgae:nil withProcessorCode:webServiceDataObject.ProcessorCode];
                                            //                                                }
                                            //                                                else
                                            //                                                {
                                            //                                                    [self errorCodeHandlingInXML:nil andMessgae:[NSString stringWithFormat:@"%@%@",appendingString,message] withProcessorCode:webServiceDataObject.ProcessorCode];
                                            //                                                }
                                            //                                            }
                                            //                                        }
                                            //
                                            //                                        if (totalDocs == uploadedCount) {
                                            //                                            [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                                            //
                                            //                                            double delayInSeconds = 2.0;
                                            //                                            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                                            //
                                            //                                            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                            //                                                Class nextClass = NSClassFromString(local_nextTemplate);
                                            //                                                id object = nil;
                                            //
                                            //                                                if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                            //                                                {
                                            //                                                    if (local_selectedIndex >=0)
                                            //                                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                            //                                                    else
                                            //                                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                            //
                                            //                                                    [self.view addSubview:(UIView *)object];
                                            //                                                }
                                            //                                                else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                            //                                                {
                                            //                                                    object = [[NSClassFromString(local_nextTemplate) alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                            //                                                    [self.navigationController pushViewController:object animated:NO];
                                            //                                                }
                                            //
                                            //                                            });
                                            //                                        }
                                            //
                                            //                                        if ([local_currentClass isEqualToString:CHILD_TEMPLATE_1]) {
                                            //                                            localizedString = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_header_label_text",local_currentTemplatePropertyFileName,[NSBundle mainBundle], nil)];
                                            //                                            NSLog(@"Localised String : %@",localizedString);
                                            //                                        }
                                            //                                        else if([local_currentClass isEqualToString:CHILD_TEMPLATE_12])
                                            //                                        {
                                            //                                            localizedString = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_header_label_text",local_currentTemplatePropertyFileName,[NSBundle mainBundle], nil)];
                                            //                                        }
                                            //                                        else if([local_currentClass isEqualToString:PARENT_TEMPLATE_4])
                                            //                                        {
                                            //                                            localizedString = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_title_text",local_currentTemplatePropertyFileName,[NSBundle mainBundle], nil)];
                                            //                                        }
                                            //
                                            //                                        DigitalKYCUploadString = [Template formatMessageWithMessage:localizedString withReferenceValues:[NSString stringWithFormat:@"%d|%d",uploadedCount,totalDocs]];
                                            //
                                            //                                        if (webServiceDataObject.ErrorCode) {
                                            //
                                            //                                            if ([[local_id viewWithTag:999] isKindOfClass:[UIButton class]]) {
                                            //                                                [(UIButton *)[local_id viewWithTag:999] setEnabled:YES];
                                            //                                                [(UIButton *)[local_id viewWithTag:999] setTitle:DigitalKYCUploadString forState:UIControlStateNormal];
                                            //                                            }
                                            //                                            else
                                            //                                            {
                                            //                                                [(UILabel *)[local_id viewWithTag:999] setText:DigitalKYCUploadString];
                                            //                                            }
                                            //                                        }
                                            //                                        else
                                            //                                        {
                                            //                                            [self removeViewsWithClassType:YES];
                                            //                                            local_dataDictionary = nil;
                                            //                                        }
                                        }
                                        else
                                            if ([local_processorCode isEqualToString:PROCESSOR_CODE_ACTIVATION])
                                            {
                                                Class nextClass = NSClassFromString(local_nextTemplate);
                                                id object = nil;
                                                
                                                if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                                {
                                                    if (local_selectedIndex >=0)
                                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                    else
                                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:webServiceDataObject.ProcessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                    [[[local_id superview] superview] addSubview:(UIView *)object];
                                                }
                                                else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                                {
                                                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:10 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                    [self.navigationController pushViewController:object animated:NO];
                                                }
                                            }
                                            else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CHECK_VELOCITY_LIMITS_T3])
                                            {
                                                NSString *velocityData = webServiceDataObject.Reference1;
                                                NSArray *separatedArray = [velocityData componentsSeparatedByString:@"~"];
                                                NSMutableDictionary *velocityDict = [[NSMutableDictionary alloc] init];
                                                NSArray *keyArray = [NSArray arrayWithObjects:PARAMETER19,PARAMETER20,AVAILABLELIMIT,AVAILABLEVOLUME,MINIMUMAMOUNT,MAXIMUMAMOUNT,nil];
                                                
                                                DatabaseManager *dataManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
                                                NSMutableArray *dropDownDataArray = [[NSMutableArray alloc] initWithArray:[dataManager getAllDropDownDetailsForKey:@"AGENTAPP_VELOCITY_TXN_TYPE"]];
                                                if([dropDownDataArray count] > 0)
                                                {
                                                    NSString *dropDownDisplayValue = nil;
                                                    for (NSDictionary *localDict in dropDownDataArray)
                                                    {
                                                        if ([[localDict objectForKey:DROP_DOWN_TYPE_NAME] isEqualToString:webServiceDataObject.PaymentDetails3])
                                                        {
                                                            dropDownDisplayValue = [localDict objectForKey:DROP_DOWN_TYPE_DESC];
                                                            break;
                                                        }
                                                    }
                                                    if (dropDownDisplayValue)
                                                    {
                                                        [velocityDict setObject:dropDownDisplayValue forKey:[keyArray objectAtIndex:0]];
                                                    }
                                                }
                                                [dropDownDataArray removeAllObjects];
                                                dropDownDataArray = [[dataManager getAllDropDownDetailsForKey:@"VELOCITY_FREQUENCY"] mutableCopy];
                                                if ([dropDownDataArray count] > 0)
                                                {
                                                    NSString *dropDownDisplayValue = nil;
                                                    for (NSDictionary *localDict in dropDownDataArray)
                                                    {
                                                        if ([[localDict objectForKey:DROP_DOWN_TYPE_NAME] isEqualToString:webServiceDataObject.PaymentDetails4])
                                                        {
                                                            dropDownDisplayValue = [localDict objectForKey:DROP_DOWN_TYPE_DESC];
                                                            break;
                                                        }
                                                    }
                                                    if (dropDownDisplayValue)
                                                    {
                                                        [velocityDict setObject:dropDownDisplayValue forKey:[keyArray objectAtIndex:1]];
                                                    }
                                                }
                                                for (int i = 0; i<[separatedArray count]; i++)
                                                {
                                                    [velocityDict setObject:[separatedArray objectAtIndex:i] forKey:[keyArray objectAtIndex:i+2]];
                                                }
                                                if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""])
                                                {
                                                    Class nextClass = NSClassFromString(local_nextTemplate);
                                                    id object = nil;
                                                    
                                                    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                                    {
                                                        if (local_selectedIndex >=0)
                                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                        else
                                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:velocityDict withProcessorCode:webServiceDataObject.ProcessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                        [[[local_id superview] superview] addSubview:(UIView *)object];
                                                    }
                                                    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                                    {
                                                        object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                        [self.navigationController pushViewController:object animated:NO];
                                                    }
                                                }
                                            }
                                            else if ([local_processorCode isEqualToString:PROCESSOR_CODE_TRANSACTION_STATUS_T3])
                                            {
                                                NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                                                [dict setObject:webServiceDataObject.PaymentDetails2 forKey:TRANSACTION_DATA_TRANSACTION_ID];
                                                [dict setObject:webServiceDataObject.TxnAmount forKey:TRANSACTION_DATA_AMOUNT_T3];
                                                [dict setObject:webServiceDataObject.FeeAmount forKey:TRANSACTION_DATA_TRANSACTION_FEE];
                                                [dict setObject:webServiceDataObject.PaymentDetails3 forKey:TRANSACTION_DATA_TRANSACTION_DATE];
                                                [dict setObject:webServiceDataObject.PaymentDetails4 forKey:TRANSACTION_DATA_TRANSACTION_STATUS];
                                                NSLog(@"Dict Values Are..%@",dict);
                                                activityIndicator.hidden = YES;
                                                [activityIndicator stopActivityIndicator];
                                                
                                                Class nextClass = NSClassFromString(local_nextTemplate);
                                                id object = nil;
                                                
                                                if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                                {
                                                    if (local_selectedIndex >=0)
                                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:dict withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                    else
                                                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:dict withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                    [[[local_id superview] superview] addSubview:(UIView *)object];
                                                }
                                                else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                                {
                                                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:10 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                    [self.navigationController pushViewController:object animated:NO];
                                                }
                                            }
                                            else
                                                if ([local_processorCode isEqualToString:PROCESSOR_CODE_CHECK_MY_VELOCITY])
                                                {
                                                    NSString *velocityData = webServiceDataObject.Reference1;
                                                    NSArray *separatedArray = [velocityData componentsSeparatedByString:@"~"];
                                                    NSMutableDictionary *velocityDict = [[NSMutableDictionary alloc] init];
                                                    NSArray *keyArray = [NSArray arrayWithObjects:PARAMETER19,PARAMETER20,AVAILABLELIMIT,AVAILABLEVOLUME,MINIMUMAMOUNT,MAXIMUMAMOUNT,nil];
                                                    
                                                    DatabaseManager *dataManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
                                                    NSMutableArray *dropDownDataArray = [[NSMutableArray alloc] initWithArray:[dataManager getAllDropDownDetailsForKey:@"VELOCITY_TXN_TYPE"]];
                                                    if([dropDownDataArray count] > 0)
                                                    {
                                                        NSString *dropDownDisplayValue = nil;
                                                        for (NSDictionary *localDict in dropDownDataArray)
                                                        {
                                                            if ([[localDict objectForKey:DROP_DOWN_TYPE_NAME] isEqualToString:webServiceDataObject.PaymentDetails3])
                                                            {
                                                                dropDownDisplayValue = [localDict objectForKey:DROP_DOWN_TYPE_DESC];
                                                                break;
                                                            }
                                                        }
                                                        if (dropDownDisplayValue)
                                                        {
                                                            [velocityDict setObject:dropDownDisplayValue forKey:[keyArray objectAtIndex:0]];
                                                        }
                                                    }
                                                    [dropDownDataArray removeAllObjects];
                                                    dropDownDataArray = [[dataManager getAllDropDownDetailsForKey:@"VELOCITY_FREQUENCY"] mutableCopy];
                                                    if ([dropDownDataArray count] > 0)
                                                    {
                                                        NSString *dropDownDisplayValue = nil;
                                                        for (NSDictionary *localDict in dropDownDataArray)
                                                        {
                                                            if ([[localDict objectForKey:DROP_DOWN_TYPE_NAME] isEqualToString:webServiceDataObject.PaymentDetails4])
                                                            {
                                                                dropDownDisplayValue = [localDict objectForKey:DROP_DOWN_TYPE_DESC];
                                                                break;
                                                            }
                                                        }
                                                        if (dropDownDisplayValue)
                                                        {
                                                            [velocityDict setObject:dropDownDisplayValue forKey:[keyArray objectAtIndex:1]];
                                                        }
                                                    }
                                                    for (int i = 0; i<[separatedArray count]; i++)
                                                    {
                                                        [velocityDict setObject:[separatedArray objectAtIndex:i] forKey:[keyArray objectAtIndex:i+2]];
                                                    }
                                                    if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""])
                                                    {
                                                        Class nextClass = NSClassFromString(local_nextTemplate);
                                                        id object = nil;
                                                        
                                                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                                        {
                                                            if (local_selectedIndex >=0)
                                                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                            else
                                                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:velocityDict withProcessorCode:webServiceDataObject.ProcessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                            [[[local_id superview] superview] addSubview:(UIView *)object];
                                                        }
                                                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                                        {
                                                            object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                            [self.navigationController pushViewController:object animated:NO];
                                                        }
                                                    }
                                                }
                                                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_VIEW_PAYEE])
                                                {
                                                    activityIndicator.hidden = YES;
                                                    [activityIndicator stopActivityIndicator];
                                                    
                                                    Class nextClass = NSClassFromString(local_nextTemplate);
                                                    id object = nil;
                                                    
                                                    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                                    {
                                                        if (local_selectedIndex >=0)
                                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                        else
                                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:webServiceDataObject.ProcessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                        [[[local_id superview] superview] addSubview:(UIView *)object];
                                                    }
                                                    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                                    {
                                                        object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:10 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                        [self.navigationController pushViewController:object animated:NO];
                                                    }
                                                }
                                                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_PAY_MERCHANT_BARCODE])
                                                {
                                                    [self removeActivityIndicator];
                                                    Class nextClass = NSClassFromString(local_nextTemplate);
                                                    id object = nil;
                                                    
                                                    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                                    {
                                                        if (local_selectedIndex >=0)
                                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                        else
                                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:webServiceDataObject.ProcessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                        [[[local_id superview] superview] addSubview:(UIView *)object];
                                                    }
                                                    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                                    {
                                                        object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:10 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                        [self.navigationController pushViewController:object animated:NO];
                                                    }
                                                }
                                                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_PAY_MERCHANT_QRCODE])
                                                {
                                                    [self removeActivityIndicator];
                                                    Class nextClass = NSClassFromString(local_nextTemplate);
                                                    id object = nil;
                                                    
                                                    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                                    {
                                                        if (local_selectedIndex >=0)
                                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                        else
                                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:webServiceDataObject.ProcessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                        [[[local_id superview] superview] addSubview:(UIView *)object];
                                                    }
                                                    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                                    {
                                                        object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:10 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                        [self.navigationController pushViewController:object animated:NO];
                                                    }
                                                }
                                                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_PRODUCT_DETAILS])
                                                {
                                                    webServiceRequestInputDetails=[[NSMutableDictionary alloc]init];
                                                    [webServiceRequestInputDetails setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"topupType"] forKey:@"message4"];
                                                    [webServiceRequestInputDetails setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selectedPPT7_TemplateId"] forKey:PARAMETER35];
                                                    NSData *objectData = [webServiceDataObject.PaymentDetails2 dataUsingEncoding:NSUTF8StringEncoding];
                                                    NSDictionary *json=[[NSDictionary alloc]init];
                                                    json = [NSJSONSerialization JSONObjectWithData:objectData options:kNilOptions error:nil];
                                                    [webServiceRequestInputDetails addEntriesFromDictionary:json];
                                                    
                                                    [self removeActivityIndicator];
                                                    
                                                    Class nextClass = NSClassFromString(local_nextTemplate);
                                                    id object = nil;
                                                    
                                                    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                                                    {
                                                        if (local_selectedIndex >=0)
                                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary?local_dataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                                        else
                                                            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:webServiceDataObject.ProcessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                                        [[[local_id superview] superview] addSubview:(UIView *)object];
                                                    }
                                                    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                                                    {
                                                        object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:10 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                                        [self.navigationController pushViewController:object animated:NO];
                                                    }
                                                }
            
                                                else if([local_processorCode isEqualToString:PROCESSOR_CODE_SELF_REG_T3])
                                                {
                                                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                                                    [self removeViewsWithClassType:YES];
                                                    [self performSelector:@selector(gotoHomeScreen) withObject:nil afterDelay:2.0];
                                                }
                                                else
                                                {
                                                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                                                }
        }
        else if ([local_actionType isEqualToString:ACTION_TYPE_3])
        {
            if ([processCode isEqualToString:PROCESSOR_CODE_ADD_BILLER])
            {
                [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                
                [self performSelector:@selector(showTicker:) withObject:processCode afterDelay:1.0];
            }
            else if([local_processorCode isEqualToString:PROCESSOR_CODE_CASH_OUT_WITHOUT_OTP])
            {
                [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                [self removeViewsWithClassType:YES];
            }
            else
                if ([local_processorCode isEqualToString:PROCESSOR_CODE_TOP_UP_PREPAID_MOBILE_OPERATOR])
                {
                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:POPUPTEMPLATE1 object:PROCESSOR_CODE_TOP_UP_PREPAID_MOBILE_OPERATOR];
                    [self removeViewsWithClassType:YES];
                    [self performSelector:@selector(gotoHomeScreen) withObject:nil afterDelay:2.0];
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CASHOUT_BANK_AGGREGATOR]) {
                    if ([webServiceDataObject.TransactionType isEqualToString:TRANSACTION_CODE_CASHOUT_AGGREGATOR]) {
                        [self popToPreviousView];
                    }
                    NSLog(@"Response Object : %@",webServiceDataObject);
                    //Response handling
                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil withProceesorCode:local_processorCode];
                    [[NSNotificationCenter defaultCenter] postNotificationName:POPUPTEMPLATE1 object:nil];
                }
                else if([local_processorCode isEqualToString:PROCESSOR_CODE_PAY_MERCHANT])
                {
                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                    [self removeViewsWithClassType:YES];
                }
                else if([local_processorCode isEqualToString:PROCESSOR_CODE_ADD_PAYEE])
                {
                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                    [self performSelector:@selector(popToPreviousView) withObject:self afterDelay:3.0];
                }
                else  if ([processCode isEqualToString:PROCESSOR_CODE_PAY_BILLER_NICK_NAME])
                {
                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:POPUPTEMPLATE6 object:PROCESSOR_CODE_PAY_BILLER_NICK_NAME];
                    [self performSelector:@selector(gotoHomeScreen) withObject:nil afterDelay:2.0];
//                    [self performSelector:@selector(updateRecentTxns) withObject:nil afterDelay:2.0];
                }
                else  if ([processCode isEqualToString:PROCESSOR_CODE_PULL_DECLINE_REQUEST_T3])
                {
                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                    [self performSelector:@selector(popToPreviousView) withObject:self afterDelay:1.0];
                }
            
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG])
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:CHILD_TEMPLATE2 object:PROCESSOR_CODE_CHANGE_LANG];
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_P2P_REGISTERED_BENEFICIARY_MOBILE_NUMBER])
                {
                    if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""])
                    {
                        if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""]){
                            
                            Class nextClass = NSClassFromString(local_nextTemplate);
                            id object = nil;
                            
                            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                            {
                                if (local_selectedIndex >=0)
                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                                else
                                    object = [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                                
                                [[local_id superview]  addSubview:(UIView *)object];
                            }
                            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                            {
                                object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                                [self.navigationController pushViewController:object animated:NO];
                            }
                        }
                        
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                    }
                    else
                    {
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                        [self removeViewsWithClassType:NO];
                        [[NSNotificationCenter defaultCenter] postNotificationName:POPUPTEMPLATE1 object:nil];
                    }
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG_T3])
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:CHILD_TEMPLATE2 object:PROCESSOR_CODE_CHANGE_LANG_T3];
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateRecentTransactions" object:self userInfo:nil];
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_MINI_STATEMENT])
                {
                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:PARENT_TEMPLATE2 object:[[local_ContentArray objectAtIndex:0] objectForKey:@"value"]];
                    [[NSNotificationCenter defaultCenter] postNotificationName:PARENT_TEMPLATE5 object:nil];
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_SELF_REG_DIGITAL_KYC])
                {
                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                    [self performSelector:@selector(showTicker:) withObject:processCode afterDelay:1];
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_BALANCE_ENQUIRY]){
                    [[NSNotificationCenter defaultCenter] postNotificationName:PARENT_TEMPLATE5 object:webServiceDataObject.PaymentDetails1];
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CHECK_BALANCE_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_CHECK_BALANCE_T3]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:PARENT_TEMPLATE5 object:webServiceDataObject.PaymentDetails2];
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CHECK_BANK_BALANCE_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_CHECK_BANK_BALANCE_T3]) {
                    NSLog(@"webserVice details..%@",webServiceDataObject.PaymentDetails2);
                    [local_dataDictionary setObject:webServiceDataObject.PaymentDetails2 forKey:PARAMETER18];
                    
                    if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""]){
                        Class nextClass = NSClassFromString(local_nextTemplate);
                        id object = nil;
                        
                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                        {
                            if (local_selectedIndex >=0)
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                            else
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                            
                            [self.view  addSubview:(UIView *)object];
                        }
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:POPUPTEMPLATE1 object:nil];
                        
                    }
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_TXN_HISTORY_T3]&& [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_TXN_HISTORY_T3])
                {
                    
                    if (![local_id isKindOfClass:[ParentTemplate5 class]])
                    {
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:PARENT_TEMPLATE2 object:[[local_ContentArray objectAtIndex:0] objectForKey:@"value"]];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:PARENT_TEMPLATE5 object:nil];
                }
            //                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_REPORTS_SUMMARY_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_REPORTS_SUMMARY_T3])
            //                {
            //                    NSLog(@"Next Template is...%@,%@",local_nextTemplate,local_nextTemplatePropertyFileName);
            //                    if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""]){
            //                        Class nextClass = NSClassFromString(local_nextTemplate);
            //                        id object = nil;
            //
            //                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
            //                        {
            //                            if (local_selectedIndex >=0)
            //                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
            //                            else
            //                                object = [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
            //
            //                            [self.view  addSubview:(UIView *)object];
            //                        }
            //                    }
            //                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_BANK_TXN_HISTORY_T3]&& [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_BANK_TXN_HISTORY_T3])
                {
                    //                    [self removeViewsWithClassType:YES];
                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:PARENT_TEMPLATE7 object:[[local_ContentArray objectAtIndex:0] objectForKey:@"value"]];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:PARENT_TEMPLATE5 object:nil];
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CUSTOMER_CASH_IN_T3]&&[[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_CUSTOMER_CASH_IN_T3])
                {
                    if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""]){
                        Class nextClass = NSClassFromString(local_nextTemplate);
                        id object = nil;
                        
                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                        {
                            if (local_selectedIndex >=0)
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                            else
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                            
                            [[local_id superview]  addSubview:(UIView *)object];
                        }
                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                        {
                            object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                            [self.navigationController pushViewController:object animated:NO];
                        }
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                        [self removeViewsWithClassType:YES];
                    }
                    else{
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                        [self removeViewsWithClassType:NO];
                        [[NSNotificationCenter defaultCenter] postNotificationName:POPUPTEMPLATE1 object:nil];
                        [self performSelector:@selector(gotoHomeScreen) withObject:nil afterDelay:2.0];
                    }
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_GROUPSAVING_CASH_IN_T3] &&[[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_GROUPSAVING_CASH_IN_T3])
                {
                    if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""]){
                        
                        Class nextClass = NSClassFromString(local_nextTemplate);
                        id object = nil;
                        
                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                        {
                            if (local_selectedIndex >=0)
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                            else
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                            
                            [[local_id superview]  addSubview:(UIView *)object];
                        }
                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                        {
                            object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                            [self.navigationController pushViewController:object animated:NO];
                        }
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                    }
                    else
                    {
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                        [self removeViewsWithClassType:NO];
                        [[NSNotificationCenter defaultCenter] postNotificationName:POPUPTEMPLATE1 object:nil];
                    }
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CASHOUT_REGISTERED_T3])
                {
                    if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""]){
                        
                        Class nextClass = NSClassFromString(local_nextTemplate);
                        id object = nil;
                        
                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                        {
                            if (local_selectedIndex >=0)
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                            else
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                            
                            [[local_id superview]  addSubview:(UIView *)object];
                        }
                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                        {
                            object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                            [self.navigationController pushViewController:object animated:NO];
                        }
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                    }
                    else
                    {
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                        [self removeViewsWithClassType:NO];
                        [[NSNotificationCenter defaultCenter] postNotificationName:POPUPTEMPLATE1 object:nil];
                        [self performSelector:@selector(gotoHomeScreen) withObject:nil afterDelay:2.0];
                    }
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_CASHOUT_VIRAL_T3]){
                    if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""]){
                        Class nextClass = NSClassFromString(local_nextTemplate);
                        id object = nil;
                        
                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                        {
                            if (local_selectedIndex >=0)
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                            else
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                            
                            [[local_id superview]  addSubview:(UIView *)object];
                        }
                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                        {
                            object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                            [self.navigationController pushViewController:object animated:NO];
                        }
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                    }
                    else{
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                        [self removeViewsWithClassType:NO];
                        [[NSNotificationCenter defaultCenter] postNotificationName:POPUPTEMPLATE1 object:nil];
                        [self performSelector:@selector(gotoHomeScreen) withObject:nil afterDelay:2.0];
                    }
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_LOAD_BANK_T3])
                {
                    if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""]){
                        Class nextClass = NSClassFromString(local_nextTemplate);
                        id object = nil;
                        
                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                        {
                            if (local_selectedIndex >=0)
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                            else
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                            
                            [[local_id superview]  addSubview:(UIView *)object];
                        }
                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                        {
                            object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                            [self.navigationController pushViewController:object animated:NO];
                        }
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                    }
                    else{
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                        [self removeViewsWithClassType:NO];
                        [[NSNotificationCenter defaultCenter] postNotificationName:POPUPTEMPLATE1 object:nil];
                    }
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_UNLOAD_LINKED_BANK_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_UNLOAD_LINKED_BANK_T3])
                {
                    if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""]){
                        Class nextClass = NSClassFromString(local_nextTemplate);
                        id object = nil;
                        
                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                        {
                            if (local_selectedIndex >=0)
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                            else
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                            
                            [[local_id superview]  addSubview:(UIView *)object];
                        }
                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                        {
                            object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                            [self.navigationController pushViewController:object animated:NO];
                        }
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                    }
                    else{
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                        [self removeViewsWithClassType:NO];
                        [[NSNotificationCenter defaultCenter] postNotificationName:POPUPTEMPLATE1 object:nil];
                    }
                }
                else if ([local_processorCode isEqualToString:PROCESSOR_CODE_UNLOAD_OTHER_BANK_T3] && [[local_dataDictionary objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_UNLOAD_OTHER_BANK_T3])
                {
                    if (![local_nextTemplate isEqualToString:@""] && ![local_nextTemplatePropertyFileName isEqualToString:@""]){
                        Class nextClass = NSClassFromString(local_nextTemplate);
                        id object = nil;
                        
                        if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                        {
                            if (local_selectedIndex >=0)
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                            else
                                object = [[nextClass alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:processCode withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                            
                            [[local_id superview]  addSubview:(UIView *)object];
                        }
                        else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                        {
                            object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                            [self.navigationController pushViewController:object animated:NO];
                        }
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                    }
                    else{
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                        [self removeViewsWithClassType:NO];
                        [[NSNotificationCenter defaultCenter] postNotificationName:POPUPTEMPLATE1 object:nil];
                    }
                }
            
                else
                {
                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                }
        }
        else if ([local_actionType isEqualToString:ACTION_TYPE_4])
        {
            webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
            
            if (local_dataDictionary)
            {
                webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:local_dataDictionary];
            }
            for (int i=0; i<[local_ContentArray count]; i++)
            {
                //susmit
                //                    if([ValidationsClass isMPINField:[[local_ContentArray objectAtIndex:i] objectForKey:@"validation_type"]])
                //                    {
                //                        [webServiceRequestInputDetails setObject:[ValidationsClass encryptString:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"]] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                //                    }
                //                    else
                //                    {
                [webServiceRequestInputDetails setObject:[[local_ContentArray objectAtIndex:i] objectForKey:@"value"] forKey:[[local_ContentArray objectAtIndex:i] objectForKey:@"value_inputtype"]];
                //                    }
                //susmit
            }
            
        }
        else if ([local_actionType isEqualToString:ACTION_TYPE_5])
        {}
        else if ([local_actionType isEqualToString:ACTION_TYPE_6])
        {
            if ([processCode isEqualToString:PROCESSOR_CODE_PAY_BILLER_NICK_NAME] ||[processCode isEqualToString:PROCESSOR_CODE_PULL_DECLINE_REQUEST_T3])
            {
                [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                [self performSelector:@selector(popToPreviousView) withObject:self afterDelay:1.0];
                [self performSelector:@selector(poptoRootView) withObject:nil afterDelay:1.0];
            }
            else if (local_processorCode) {
                [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
            }
            else
            {
                NSArray *viewsArray = [[NSArray alloc] initWithArray:[[local_id superview] subviews]];
                for (int i = 0; i<[viewsArray count]; i++)
                {
                    UIView *lView = [viewsArray objectAtIndex:i];
                    if (lView.tag < 0)
                    {
                        [lView removeFromSuperview];
                    }
                }
            }
        }
        else if ([local_actionType isEqualToString:ACTION_TYPE_7])
        {
            if ([local_processorCode isEqualToString:PROCESSOR_CODE_SELF_REG_DIGITAL_KYC]){
                
                if (webServiceDataObject.ErrorCode) {
                    PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:@"Erorr" andMessage:webServiceDataObject.Remark withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                    [[UIApplication sharedApplication].keyWindow addSubview:popup];
                }else{
                    PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:@"Success" andMessage:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_upload_docs_success", nil)] withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                    [[UIApplication sharedApplication].keyWindow addSubview:popup];
        //                    [[NSNotificationCenter defaultCenter] postNotificationName:@"DocsUploadSuccess" object:self userInfo:nil];
                    [self.navigationController popToRootViewControllerAnimated:NO];
                }
                
                
                //                NSString *uploadedDocuments = webServiceDataObject.PaymentDetails4;
                //                int uploadedCount = (int)[[uploadedDocuments componentsSeparatedByString:@","] count];
                //                int totalDocs = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"totalDocuments"];
                //                NSLog(@"Total Docs count is...%d",totalDocs);
                //                NSString *localizedString;
                //
                //                if (webServiceDataObject.ErrorCode) {
                //                    if (totalDocs != uploadedCount)
                //                    {
                //                        NSString *errorCode = webServiceDataObject.ErrorCode;
                //                        NSString *appendingString;
                //                        NSString *message;
                //                        NSString *technicalErrorMessage = NSLocalizedStringFromTableInBundle(@"999",@"ExternalErrorMessages",[NSBundle mainBundle], nil);
                //
                //                        if (errorCode.length != 0) {
                //                            if([NSLocalizedStringFromTableInBundle(@"application_is_error_code_required_for_error_message",@"GeneralSettings",[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                //                                appendingString = [[[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_label_prefix", nil)] stringByAppendingString:errorCode] stringByAppendingString:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_error_msg_seperator", nil)]];
                //                            else
                //                                appendingString=@"";
                //                        }
                //                        else
                //                        {
                //                            if([NSLocalizedStringFromTableInBundle(@"application_is_error_code_required_for_error_message",@"GeneralSettings",[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                //                                appendingString = [[[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_label_prefix", nil)] stringByAppendingString:@"999"] stringByAppendingString:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_error_msg_seperator", nil)]];
                //                            else
                //                                appendingString=@"";
                //                        }
                //
                //                        if (webServiceDataObject.Reference5)
                //                            message = [Template formatMessageWithMessage:NSLocalizedStringFromTableInBundle(webServiceDataObject.ErrorCode,@"ExternalErrorMessages",[NSBundle mainBundle], nil) withReferenceValues:webServiceDataObject.Reference5];
                //                        else
                //                            message =[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(webServiceDataObject.ErrorCode,@"ExternalErrorMessages",[NSBundle mainBundle], nil)];
                //
                //                        NSString *propertyFileString = NSLocalizedStringFromTableInBundle(errorCode,@"ExternalErrorMessages",[NSBundle mainBundle], nil);
                //
                //                        if ([propertyFileString isEqualToString:webServiceDataObject.ErrorCode] || propertyFileString.length == 0) {
                //                            NSString *displayString = [appendingString stringByAppendingString:technicalErrorMessage];
                //                            [self errorCodeHandlingInXML:displayString andMessgae:nil withProcessorCode:webServiceDataObject.ProcessorCode];
                //                        }
                //                        else
                //                        {
                //                            [self errorCodeHandlingInXML:nil andMessgae:[NSString stringWithFormat:@"%@%@",appendingString,message] withProcessorCode:webServiceDataObject.ProcessorCode];
                //                        }
                //                    }
                //                }
                //                if (totalDocs == uploadedCount) {
                //                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString([self showAlertFormat], nil)] andErrorCode:nil];
                //                    double delayInSeconds = 2.0;
                //                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                //                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                //                        int loginStatus = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"userLoginStatus"];
                //
                //                        if (loginStatus == 0) {
                //                            UIViewController *object = [[NSClassFromString(application_launch_template) alloc] initWithNibName:application_launch_template bundle:nil withSelectedIndex:0 fromView:3 withFromView:nil withPropertyFile:application_launch_template_property_files_list withProcessorCode:nil dataArray:nil dataDictionary:nil];
                //                            UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:object];
                //                            [navi setNavigationBarHidden:YES];
                //                            AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                //                            [delegate.window setRootViewController:navi];
                //
                //                        }
                //                        else
                //                        {
                //                            NSArray *myControllers = self.navigationController.viewControllers;
                //                            int previous = (int)myControllers.count - 2;
                //                            UIViewController *previousController = [myControllers objectAtIndex:previous];
                //                            [self.navigationController popToViewController:previousController animated:NO];
                //                        }
                //                    });
                //                }
                //
                //                if ([local_currentClass isEqualToString:CHILD_TEMPLATE_1]) {
                //                    localizedString = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_header_label_text",local_currentTemplatePropertyFileName,[NSBundle mainBundle], nil)];
                //                    NSLog(@"Localised String : %@",localizedString);
                //                }
                //                else if([local_currentClass isEqualToString:CHILD_TEMPLATE_12])
                //                {
                //                    localizedString = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_header_label_text",local_currentTemplatePropertyFileName,[NSBundle mainBundle], nil)];
                //                }
                //                else if([local_currentClass isEqualToString:PARENT_TEMPLATE_4])
                //                {
                //                    localizedString = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_title_text",local_currentTemplatePropertyFileName,[NSBundle mainBundle], nil)];
                //                }
                //
                //                DigitalKYCUploadString = [Template formatMessageWithMessage:localizedString withReferenceValues:[NSString stringWithFormat:@"%d|%d",uploadedCount,totalDocs]];
                //
                //                if (webServiceDataObject.ErrorCode) {
                //                    if ([[local_id viewWithTag:999] isKindOfClass:[UIButton class]]) {
                //                        [(UIButton *)[local_id viewWithTag:999] setEnabled:YES];
                //                        [(UIButton *)[local_id viewWithTag:999] setTitle:DigitalKYCUploadString forState:UIControlStateNormal];
                //                    }
                //                    else
                //                    {
                //                        [(UILabel *)[local_id viewWithTag:999] setText:DigitalKYCUploadString];
                //                    }
                //                }
                //                else
                //                {
                //                    [self removeViewsWithClassType:YES];
                //                    [self showAlertForErrorWithMessage:localizedString andErrorCode:nil];
                //                    local_dataDictionary = nil;
                //                }
            }
            if([local_processorCode isEqualToString:PROCESSOR_CODE_PAY_MERCHANT])
            {
                if ([AppDelegate getNotificationStatus]) {
                    UIViewController *object = [[NSClassFromString(application_launch_template) alloc] initWithNibName:application_launch_template bundle:nil withSelectedIndex:0 fromView:3 withFromView:nil withPropertyFile:application_launch_template_property_files_list withProcessorCode:nil dataArray:nil dataDictionary:nil];
                    
                    UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:object];
                    [navi setNavigationBarHidden:YES];
                    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                    [delegate.window setRootViewController:navi];
                    
                }
                else {
                    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                    [[((UINavigationController*)delegate.window.rootViewController).visibleViewController navigationController] popViewControllerAnimated:NO];
                }
            }
        }
        else if ([local_actionType isEqualToString:ACTION_TYPE_9])
        {}
        else if ([local_actionType isEqualToString:ACTION_TYPE_10])
        {}
        else if ([local_actionType isEqualToString:ACTION_TYPE_11])
        {}
        else if ([local_actionType isEqualToString:ACTION_TYPE_12])
        {}
        else if ([local_actionType isEqualToString:ACTION_TYPE_13])
        {}
        else if ([local_actionType isEqualToString:ACTION_TYPE_14])
        {}
        else if ([local_actionType isEqualToString:ACTION_TYPE_15])
        {}
        else if ([local_actionType isEqualToString:ACTION_TYPE_16])
        {
            Class nextClass = NSClassFromString(local_nextTemplate);
            id object = nil;
            if ([local_nextTemplate isEqualToString:PARENT_TEMPLATE_5]){
                object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:10 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:object];
                [navi setNavigationBarHidden:YES];
                AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                [delegate.window setRootViewController:navi];
            }
            else
                if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                {
                    if (local_selectedIndex >=0)
                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                    else
                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:webServiceDataObject.ProcessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                    [[[local_id superview] superview] addSubview:(UIView *)object];
                }
                else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
                {
                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:10 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                    [self.navigationController pushViewController:object animated:NO];
                    
                }
        }
        else if ([local_actionType isEqualToString:ACTION_TYPE_19]){
            
            if ([webServiceDataObject.Reference5 isEqualToString:WEBSERVICE_REFERENCE_DATA]) {
                [self addUpdateDeviceIDView];
            }
            else{
                
                Class nextClass = NSClassFromString(local_nextTemplate);
                id object = nil;
                
                if ([local_nextTemplate isEqualToString:PARENT_TEMPLATE_5]) {
                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:10 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                    UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:object];
                    [navi setNavigationBarHidden:YES];
                    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                    [delegate.window setRootViewController:navi];
                }
                
                else if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                {
                    if (local_selectedIndex >=0)
                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                    else
                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:webServiceDataObject.ProcessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                    [[[local_id superview] superview] addSubview:(UIView *)object];
                }
                else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)]){
                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:10 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                    [self.navigationController pushViewController:object animated:NO];
                }
            }
        }
        else if ([local_actionType isEqualToString:ACTION_TYPE_20]){
            if ([webServiceDataObject.Reference5 isEqualToString:WEBSERVICE_REFERENCE_DATA]) {
                [self addUpdateDeviceIDView];
            }
            else{
                
                Class nextClass = NSClassFromString(local_nextTemplate);
                id object = nil;
                
                if ([local_nextTemplate isEqualToString:PARENT_TEMPLATE_5]) {
                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:10 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                    UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:object];
                    [navi setNavigationBarHidden:YES];
                    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                    [delegate.window setRootViewController:navi];
                }
                
                else if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
                {
                    if (local_selectedIndex >=0)
                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                    else
                        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:webServiceDataObject.ProcessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                    [[[local_id superview] superview] addSubview:(UIView *)object];
                }
                else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)]){
                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:10 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                    [self.navigationController pushViewController:object animated:NO];
                    
                }
            }
        }
    }
}

#pragma mark - Error handling Methods.
/**
 * This method is used to Error handling Methods.
 *@Param type - Based on Error Code Error get from ExternalErrorMessage file.
 */
-(void)errorCodeHandlingInDBWithCode:(NSString *)errorCode withProcessorCode:(NSString *)processorCode
{
    [self removeActivityIndicator];
}

-(void)errorCodeHandlingInXML:(NSString *)errorCode andMessgae:(NSString *)message withProcessorCode:(NSString *)processorCode
{
    [self removeActivityIndicator];
    [self showAlertForErrorWithMessage:message andErrorCode:errorCode];
    if([processorCode isEqualToString:@"0062"]){
        AppDelegate *apDlgt = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        UINavigationController *navCntrl = (UINavigationController *)apDlgt.window.rootViewController;
        [navCntrl popViewControllerAnimated:NO];
    }
}

-(void)errorCodeHandlingInWebServiceRunningWithErrorCode:(NSString *)errorCode
{
    [self removeActivityIndicator];
    [self showAlertForErrorWithMessage:nil andErrorCode:errorCode];
}

-(void)showAlertForErrorWithMessage:(NSString *)message andErrorCode:(NSString *)errorCode
{
    [self removeActivityIndicator];
    
    NSString *errorMessage = nil;
    
    NSLog(@"AlertType is local...%@",local_alertType);
    
    if (errorCode)
        errorMessage = NSLocalizedStringFromTableInBundle(errorCode,[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil);
    else
        errorMessage = message;
    
    if([local_alertType compare:TICKER_TEXT] == NSOrderedSame)
    {
        NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
        
        NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
        
        [[[local_id superview] superview] makeToast:errorMessage duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
    }
    else if ([local_alertType compare:POPUP_TEXT] == NSOrderedSame){
        
        PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]]  andMessage:errorMessage withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
        
        
        if ([local_id isKindOfClass:[ParentTemplate5 class]])
        {
            ParentTemplate5 *obj=local_id;
            [obj.view addSubview:popup];
        }
        else
        {
//            [[[local_id superview] superview] addSubview:popup];
            [[UIApplication sharedApplication].keyWindow addSubview:popup];
            
            if([local_processorCode isEqualToString:PROCESSOR_CODE_LOGIN_T3]||[local_processorCode isEqualToString:PROCESSOR_CODE_REPORTS_SUMMARY_T3] ||[local_processorCode isEqualToString:PROCESSOR_CODE_GROUPSAVING_CASH_IN_T3]||[local_processorCode isEqualToString:PROCESSOR_CODE_CASHOUT_REGISTERED_T3] )
                [self removeViewsWithClassType:YES];
        }
    }
}

-(void)showAlertForErrorWithMessage:(NSString *)message andErrorCode:(NSString *)errorCode withProceesorCode:(NSString *)pCode
{
    NSString *errorMessage = nil;
    
    if (errorCode)
        errorMessage = NSLocalizedStringFromTableInBundle(errorCode,[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil);
    else
        errorMessage = message;
    
    NSLog(@"AlertType is...%@",local_alertType);
    
    if ([local_alertType compare:TICKER_TEXT] == NSOrderedSame)
    {
        NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
        
        NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
        
        if ([pCode isEqualToString:PROCESSOR_CODE_CASH_OUT_WITHOUT_OTP])
        {
            [[local_id superview] makeToast:errorMessage duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f]textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
        }
        else
        {
            [[[local_id superview] superview] makeToast:errorMessage duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f]textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
        }
    }
    else if ([local_alertType compare:POPUP_TEXT] == NSOrderedSame){
        PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]]  andMessage:errorMessage withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
        [[[local_id superview] superview] addSubview:popup];
        
        if([local_processorCode isEqualToString:PROCESSOR_CODE_LOGIN_T3] || [local_processorCode isEqualToString:PROCESSOR_CODE_SELF_REG_T3]||[local_processorCode isEqualToString:PROCESSOR_CODE_REPORTS_SUMMARY_T3] ||[local_processorCode isEqualToString:PROCESSOR_CODE_GROUPSAVING_CASH_IN_T3]||[local_processorCode isEqualToString:PROCESSOR_CODE_CASHOUT_REGISTERED_T3] )
            [self removeViewsWithClassType:YES];
    }
}

#pragma mark - Show Alert message(TIcker Or PopUp).
/**
 * This method is used to Show Alert message.
 *@Param type -Ticker Or PopUp.
 */
-(void)showTicker:(NSString *) pCode
{
    [self removeActivityIndicator];
    if ([local_actionType isEqualToString:@"1"])
    {
    }
    else if ([local_actionType isEqualToString:@"2"])
    {
    }
    else if ([local_actionType isEqualToString:@"3"])
    {
        if ([pCode isEqualToString:PROCESSOR_CODE_PAY_BILLER_NICK_NAME] || [pCode isEqualToString:PROCESSOR_CODE_ADD_BILLER] || [pCode isEqualToString:PROCESSOR_CODE_ADD_PAYEE] || [pCode isEqualToString:PROCESSOR_CODE_PULL_ACCEPT_REQUEST_T3])
            [self popToPreviousView];
        
        else
            [self popToPreviousView];
    }
    else if ([local_actionType isEqualToString:@"4"])
    {
    }
    else if ([local_actionType isEqualToString:@"5"])
    {
    }
    else if ([local_actionType isEqualToString:@"6"])
    {
        NSArray *viewsArray = [[NSArray alloc] initWithArray:[[local_id superview] subviews]];
        for (int i = 0; i<[viewsArray count]; i++)
        {
            UIView *lView = [viewsArray objectAtIndex:i];
            if (lView.tag < 0)
            {
                [lView removeFromSuperview];
            }
        }
    }
}

#pragma mark - Set Activity Indicator.
/**
 * This method is used to Set Activity Indicator(ProgressBar) for Particular View.
 */
-(void) setActivityIndicator
{
    [[[[UIApplication sharedApplication] delegate] window] addSubview:activityIndicator];
    activityIndicator.hidden = NO;
    [activityIndicator startActivityIndicator];
}

#pragma mark - Remove Activity Indicator.
/**
 * This method is used to remove Activity Indicator(ProgressBar) for Particular View.
 */
-(void) removeActivityIndicator
{
    [activityIndicator removeFromSuperview];
    [activityIndicator stopActivityIndicator];
}

#pragma mark - Parent template 7.
/**
 * This method is used to add  Parent template7 ViewController ParentSubview with pageheader.
 * @param Type - Feature name (NextTemplate name and Next template property file Name).
 */
-(void) setParentTemplatewithHeader:(PageHeaderView *)pageHeader
{
    if ([local_processorCode isEqualToString:PROCESSOR_CODE_FETCH_BILLER_ALL]){
        if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""]){
            CGRect frame;
            if ([local_ContentArray count]>0)
            {
                frame = CGRectMake(0,pageHeader.frame.size.height, SCREEN_WIDTH, SCREEN_HEIGHT-(pageHeader.frame.size.height+64));
            }
            else if ([local_dataDictionary count]>0)
            {
                frame = CGRectMake(0,pageHeader.frame.size.height, SCREEN_WIDTH, SCREEN_HEIGHT-(pageHeader.frame.size.height));
            }
            else
            {
                frame = CGRectMake(0,pageHeader.frame.size.height, SCREEN_WIDTH, SCREEN_HEIGHT-(pageHeader.frame.size.height));
            }
            
            Class nextClass = NSClassFromString(local_nextTemplate);
            id object = nil;
            
            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
            {
                if (local_selectedIndex >=0)
                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                else
                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                
                [self.view addSubview:(UIView *)object];
            }
            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
            {
                object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:local_dataDictionary];
                [self.navigationController pushViewController:object animated:NO];
            }
            else if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)])
            {
                object = [[nextClass alloc] initWithFrame:frame withPropertyName:local_nextTemplatePropertyFileName hasDidSelectFunction:YES withDataDictionary:local_dataDictionary withDataArray:local_ContentArray withPIN:nil withProcessorCode:local_processorCode withType:nil fromView:0 insideView:nil];
                
                [self.view addSubview:(UIView *)object];
            }
            
        }
    }
    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_ACTIVATION]){
        if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""]){
            Class nextClass = NSClassFromString(local_nextTemplate);
            id object = nil;
            
            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)]){
                if (local_selectedIndex >=0)
                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                else
                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                
                [self.view addSubview:(UIView *)object];
            }
            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)]){
                object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:local_dataDictionary];
                [self.navigationController pushViewController:object animated:NO];
            }
            else if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)]){
                object = [[nextClass alloc] initWithFrame:CGRectMake(0,pageHeader.frame.size.height, SCREEN_WIDTH, SCREEN_HEIGHT-(pageHeader.frame.size.height)) withPropertyName:local_nextTemplatePropertyFileName hasDidSelectFunction:NO withDataDictionary:local_dataDictionary withDataArray:nil withPIN:nil withProcessorCode:local_processorCode withType:nil fromView:0 insideView:nil];
                [self.view addSubview:(UIView *)object];
            }
        }
    }
    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_ACTIVATION_T3]){
        if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""]){
            Class nextClass = NSClassFromString(local_nextTemplate);
            id object = nil;
            
            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)]){
                if (local_selectedIndex >=0)
                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                else
                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                
                [self.view addSubview:(UIView *)object];
            }
            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)]){
                object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:local_dataDictionary];
                [self.navigationController pushViewController:object animated:NO];
            }
            else if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)]){
                object = [[nextClass alloc] initWithFrame:CGRectMake(0,pageHeader.frame.size.height, SCREEN_WIDTH, SCREEN_HEIGHT-(pageHeader.frame.size.height)) withPropertyName:local_nextTemplatePropertyFileName hasDidSelectFunction:NO withDataDictionary:local_dataDictionary withDataArray:nil withPIN:nil withProcessorCode:local_processorCode withType:nil fromView:0 insideView:nil];
                [self.view addSubview:(UIView *)object];
            }
        }
    }
    else{
        if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""]){
            Class nextClass = NSClassFromString(local_nextTemplate);
            id object = nil;
            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)]){
                if (local_selectedIndex >=0)
                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                else
                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:local_dataDictionary withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                
                [self.view addSubview:(UIView *)object];
            }
            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)]){
                object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:local_dataDictionary];
                [self.navigationController pushViewController:object animated:NO];
            }
            else if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)]){
                object = [[nextClass alloc] initWithFrame:CGRectMake(0,pageHeader.frame.size.height,SCREEN_WIDTH, SCREEN_HEIGHT-(pageHeader.frame.size.height)) withPropertyName:local_nextTemplatePropertyFileName hasDidSelectFunction:NO withDataDictionary:local_dataDictionary withDataArray:local_ContentArray withPIN:nil withProcessorCode:nil withType:nil fromView:0 insideView:nil];
                [self.view addSubview:(UIView *)object];
            }
        }
    }
}

#pragma mark - PopUp template 1.
/**
 * This method is used to add  PopUptemplate 1 ViewController Subview.
 * @param Type - Feature name (Based on Processor code comparing adding  Frame).
 */
+ (BOOL)getControlFrameForProcessorCode : (NSString *)processorCode andPropertyFile : (NSString *)propertyFileName forControl : (UILabel *__strong *)controlView toCenter : (PopUpTemplate1 *)PPT1 withYCoordinate: (CGFloat)y_Position andXCoordinate : (CGFloat)x_Position andHeight : (CGFloat)height andChangeableyCoordinate : (float *)y_Position_change withAdditonalControl : (UIScrollView *__strong *)controlView1 andButton1 : (UIButton *__strong *)button1 andButton2 : (UIButton *__strong *)button2 withDictionary : (NSDictionary *)localDataDictionary forSequence : (int)sequence
{
    BOOL processorCodeProcessed = NO;
    
    if (([processorCode isEqualToString:PROCESSOR_CODE_BALANCE_ENQUIRY] || [processorCode isEqualToString:PROCESSOR_CODE_MINI_STATEMENT] || [processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG] ||[processorCode isEqualToString:PROCESSOR_CODE_GENERATE_OTP] ||[processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG_T3]||[processorCode isEqualToString:PROCESSOR_CODE_CHECK_BALANCE_T3]||[processorCode isEqualToString:PROCESSOR_CODE_TXN_HISTORY_T3])&& controlView != nil && sequence == 0)
    {
        [*controlView setFrame:CGRectMake(40, (SCREEN_HEIGHT*40)/100, SCREEN_WIDTH-80 , SCREEN_HEIGHT-(SCREEN_HEIGHT*60/100))];
        [*controlView setCenter:PPT1.center];
        processorCodeProcessed = YES;
    }
    else if ([propertyFileName caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame && sequence == 1)
    {
        if ([processorCode isEqualToString:PROCESSOR_CODE_BALANCE_ENQUIRY] || [processorCode isEqualToString:PROCESSOR_CODE_MINI_STATEMENT] || [processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG]||[processorCode isEqualToString:PROCESSOR_CODE_GENERATE_OTP]|| [processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG_T3] ||[processorCode isEqualToString:PROCESSOR_CODE_CHECK_BALANCE_T3] || [processorCode isEqualToString:PROCESSOR_CODE_TXN_HISTORY_T3])
        {
            [*controlView setFrame:CGRectMake(50, ((SCREEN_HEIGHT*40)/100)-40, SCREEN_WIDTH-100 , 40)];
            processorCodeProcessed = YES;
        }
    }
    else if([propertyFileName caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame && sequence == 2)
    {
        if ([processorCode isEqualToString:PROCESSOR_CODE_BALANCE_ENQUIRY] || [processorCode isEqualToString:PROCESSOR_CODE_MINI_STATEMENT] || [processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG]|| [processorCode isEqualToString:PROCESSOR_CODE_GENERATE_OTP] || [processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG_T3]||[processorCode isEqualToString:PROCESSOR_CODE_CHECK_BALANCE_T3]||[processorCode isEqualToString:PROCESSOR_CODE_TXN_HISTORY_T3])
        {
            if(controlView)
            {
                [*controlView setFrame:(SCREEN_HEIGHT>=667)? (CGRectMake(50, y_Position+10, SCREEN_WIDTH-100 , 1)):(CGRectMake(50, y_Position, SCREEN_WIDTH-100 , 1))];
                processorCodeProcessed = YES;
            }
            else if(controlView1)
            {
                [*controlView1 setFrame:CGRectMake(50, y_Position+2,SCREEN_WIDTH-100 , height-90)];
                processorCodeProcessed = YES;
            }
        }
    }
    else if([propertyFileName caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame && sequence == 3)
    {
        if ([processorCode isEqualToString:PROCESSOR_CODE_AGENT_DETAILS]){
            NSString *detailedStr;
            NSString *newLineString = @"\n";
            for (int i = 0; i < 4; i++)
            {
                if ((i == 0) && !([[localDataDictionary objectForKey:@""] length]>0))
                    detailedStr = [localDataDictionary objectForKey:@"bn"];
                
                if((i == 1) && ([[localDataDictionary objectForKey:@"adl1"] length]>0))
                    detailedStr = [detailedStr stringByAppendingString:[NSString stringWithFormat:@"%@%@",newLineString,[localDataDictionary objectForKey:@"adl1"]]];
                
                if ((i == 2) && ([[localDataDictionary objectForKey:@"adl2"] length]>0))
                    detailedStr = [detailedStr stringByAppendingString:[NSString stringWithFormat:@"%@%@",newLineString,[localDataDictionary objectForKey:@"adl2"]]];
                
                if ((i == 3) && ([[localDataDictionary objectForKey:@"adl3"] length]>0))
                    detailedStr = [detailedStr stringByAppendingString:[NSString stringWithFormat:@"%@%@",newLineString,[localDataDictionary objectForKey:@"adl3"]]];
            }
            
            NSString *concatenateStr=[NSString stringWithFormat:@"%@\t---%@\n",[localDataDictionary objectForKey:@"city"],[localDataDictionary objectForKey:@"pincd"]];
            NSArray* words = [concatenateStr componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            NSString* nospacestring = [words componentsJoinedByString:@""];
            NSString *mainStr=[[NSString stringWithFormat:@"%@\n%@\n%@\n",detailedStr,nospacestring,[[NSUserDefaults standardUserDefaults]objectForKey:@"Country"]] stringByReplacingOccurrencesOfString:@"  " withString:@""];
            [*controlView setText:mainStr];
            [*controlView setNumberOfLines:0];
            [*controlView setLineBreakMode:NSLineBreakByWordWrapping];
            [*controlView sizeToFit];
            processorCodeProcessed = YES;
        }
    }
    else if([propertyFileName caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame && sequence == 4)
    {
        if ([processorCode isEqualToString:PROCESSOR_CODE_BALANCE_ENQUIRY] || [processorCode isEqualToString:PROCESSOR_CODE_MINI_STATEMENT]||[processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG]||[processorCode isEqualToString:PROCESSOR_CODE_GENERATE_OTP]||[processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG_T3]||[processorCode isEqualToString:PROCESSOR_CODE_CHECK_BALANCE_T3] || [processorCode isEqualToString:PROCESSOR_CODE_TXN_HISTORY_T3])
        {
            if(controlView && controlView1)
                [*controlView setFrame:(SCREEN_HEIGHT>=667)? CGRectMake(x_Position,y_Position+20, CGRectGetWidth([*controlView1 frame])-10,40):CGRectMake(x_Position,y_Position, CGRectGetWidth([*controlView1 frame])-10,40)];
            else
                *y_Position_change =(SCREEN_HEIGHT>=667)? (CGRectGetMinY([*controlView frame])+CGRectGetHeight([*controlView frame])+30):(CGRectGetMinY([*controlView frame])+CGRectGetHeight([*controlView frame])-40);
            
            processorCodeProcessed = YES;
        }
    }
    return processorCodeProcessed;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    NSArray *viewsToRemove = [self.view subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
}

#pragma mark - Method For Show SMS To User.
/**
 * This method is used to Show SMS to User.
 * @param Type - Feature name (SMS Related content(Processor Code,Transaction type and API related Details)).
 */
- (void)showSMS {
    BOOL isSimCardAvailable = YES;
    CTTelephonyNetworkInfo* info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier* carrier = info.subscriberCellularProvider;
    if(carrier.mobileNetworkCode == nil || [carrier.mobileNetworkCode isEqualToString:@""])
    {
        isSimCardAvailable = NO;
        NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
        NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
        NSString *appendingString = [[[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_label_prefix", nil)] stringByAppendingString:[NSString stringWithFormat:@"_1007"]] stringByAppendingString:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_error_msg_seperator", nil)]];
        [[[local_id superview] superview] makeToast:[NSString stringWithFormat:@"%@%@",appendingString,NSLocalizedStringFromTableInBundle(@"_1007",[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil)] duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
    }
    else
    {
        NSString *phoneNumStr=NSLocalizedStringFromTableInBundle(@"application_default_reciepient_number", @"GeneralSettings",[NSBundle mainBundle], nil);
        NSLog(@"Phone number....%@",phoneNumStr);
        NSLog(@"SMS TEXT....%@",smsText);
        if(![MFMessageComposeViewController canSendText]) {
            //            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_ok_button", nil)]] otherButtonTitles:nil];
            //            [warningAlert show];
            
            
            UIAlertController *alert= [UIAlertController alertControllerWithTitle:@"Error" message:@"Your device doesn't support SMS!" preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_ok_button", nil)]] style:UIAlertActionStyleCancel handler:nil]];
            
            UIViewController *topController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
            while (topController.presentedViewController)
            {
                topController = topController.presentedViewController;
            }
            [topController presentViewController:alert animated:NO completion:nil];
            
            
            return;
        }
        
        NSArray *recipents = @[phoneNumStr];
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        messageController.messageComposeDelegate = self;
        messageController.view.backgroundColor=[UIColor whiteColor];
        [messageController setRecipients:recipents];
        [messageController setBody:smsText];
        // Present message view controller on screen
        [self presentViewController:messageController animated:YES completion:nil];
    }
}

#pragma mark - getFeatureMenu index.
/**
 * This method is used to add Feature related Properties to Other View.
 */
-(void)getSelectedFeaturemenuProperty{
    NSString *selectedProfile;
    NSString *profileID = [[NSUserDefaults standardUserDefaults] objectForKey:PARAMETER36];
    NSMutableArray *profilesList = [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(@"profiles_list", @"Features_List",[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
    NSMutableArray *profileKeys =  [[NSMutableArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(@"profiles_keys", @"Features_List",[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
    
    for (int i=0; i < profileKeys.count; i++) {
        if ([profileID isEqualToString:[profileKeys objectAtIndex:i]]) {
            selectedProfile = [profilesList objectAtIndex:i];
            break;
        }
    }
    
    if (selectedProfile)
        propertyFilesArray= [NSLocalizedStringFromTableInBundle([selectedProfile stringByAppendingString:@"_template_properties"],@"GeneralSettings",[NSBundle mainBundle], nil) componentsSeparatedByString:@","];
    
    NSString *propertyFileName;
    propertyFileName=@"FeatureMenu_ListT1";
    
    
    int selectedIndex = (int)[propertyFilesArray indexOfObject:local_nextTemplatePropertyFileName];
    if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""])
    {
        NSString *profileID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"] objectForKey:PARAMETER36];
        
        if ([profileID isEqualToString:@"SILVER"])
            propertyFilesArray= [NSLocalizedStringFromTableInBundle(@"profile_1_template_properties", @"GeneralSettings",[NSBundle mainBundle], nil) componentsSeparatedByString:@","];
        
        else if ([profileID isEqualToString:@"MBILL"])
            propertyFilesArray= [NSLocalizedStringFromTableInBundle(@"profile_2_template_properties", @"GeneralSettings",[NSBundle mainBundle], nil) componentsSeparatedByString:@","];
        
        else if ([profileID isEqualToString:@"WBIL"])
            propertyFilesArray= [NSLocalizedStringFromTableInBundle(@"profile_3_template_properties", @"GeneralSettings",[NSBundle mainBundle], nil) componentsSeparatedByString:@","];
        
        else if ([profileID isEqualToString:@"MOSASILVER"])
            propertyFilesArray= [NSLocalizedStringFromTableInBundle(@"profile_4_template_properties", @"GeneralSettings",[NSBundle mainBundle], nil) componentsSeparatedByString:@","];
        
        else if ([profileID isEqualToString:@"WMBILL"])
            propertyFilesArray= [NSLocalizedStringFromTableInBundle(@"profile_5_template_properties", @"GeneralSettings",[NSBundle mainBundle], nil) componentsSeparatedByString:@","];
        
        NSLog(@"PropertyFile Array...%@",propertyFilesArray);
        
        if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""]){
            Class nextClass = NSClassFromString(local_nextTemplate);
            id object = nil;
            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)]){
                if (local_selectedIndex >=0)
                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                else
                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                
                [self.view addSubview:(UIView *)object];
            }
            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)]){
                if ([local_nextTemplate isEqualToString:PARENT_TEMPLATE_5]) {
                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:10 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                    UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:object];
                    [navi setNavigationBarHidden:YES];
                    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                    [delegate.window setRootViewController:navi];
                }
                else{
                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:selectedIndex+1 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                    [self.navigationController pushViewController:object animated:NO];
                }
            }
        }
    }
    else if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"1"])
    {
        if (![local_nextTemplatePropertyFileName isEqualToString:@""] && ![local_nextTemplate isEqualToString:@""]){
            Class nextClass = NSClassFromString(local_nextTemplate);
            id object = nil;
            
            if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)]){
                if (local_selectedIndex >=0)
                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
                else
                    object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                
                [self.view addSubview:(UIView *)object];
            }
            else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
            {
                if ([local_nextTemplate isEqualToString:PARENT_TEMPLATE_5]) {
                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:10 fromView:2 withFromView:nil withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                    UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:object];
                    [navi setNavigationBarHidden:YES];
                    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                    [delegate.window setRootViewController:navi];
                }
                else{
                    object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:local_processorCode withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
                    [self.navigationController pushViewController:object animated:NO];
                }
            }
        }
    }
}

#pragma mark - PopTo Parent viewController.
/**
 * This method is used to PopTo Parent viewController.
 * Clear All stacks and Move to ParenT viewController.
 */
-(void)popToPreviousView
{
    UIView *tempView = [[UIView alloc] init];
    tempView = [local_id superview];
    UIViewController *vc = [(UINavigationController *)tempView.window.rootViewController visibleViewController];
    int index = 0;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray:vc.navigationController.viewControllers];
    for (int i=0; i<navigationArray.count; i++) {
        if ([[navigationArray objectAtIndex:i] isKindOfClass:[NSClassFromString([[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_IMPLEMENTATION_TEMPLATE]) class]]) {
            index = i;
        }
    }
    [vc.navigationController popToViewController:[navigationArray objectAtIndex:index] animated:NO];
    NSLog(@"Object Value is..%d,%@",index,[[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_IMPLEMENTATION_TEMPLATE]);
}
#pragma mark - Alert Message With processor code And Transaction type.
/**
 * This method is used to Alert Message With processor code And Transaction type.
 */
-(NSString *)showAlertFormat
{
    NSString *formatString = [NSString stringWithFormat:@"%@_%@",[local_dataDictionary objectForKey:PARAMETER15],[local_dataDictionary objectForKey:PARAMETER13]];
    NSLog(@"Formatted String is....%@",formatString);
    return  formatString;
}

-(void)addUpdateDeviceIDView{
    activityIndicator.hidden =YES;
    [activityIndicator startActivityIndicator];
    
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setObject:PROCESSOR_CODE_UPDATEDEVICE_ID forKey:PARAMETER15];
    [dict setObject:TRANSACTION_CODE_UPDATEDEVICE_ID forKey:PARAMETER13];
    [self commonWebServiceCall:dict withProcessorCode:PROCESSOR_CODE_UPDATEDEVICE_ID];
    
    //    Class nextClass=NSClassFromString(POPUP_TEMPLATE_2);
    //    id object = nil;
    //     local_nextTemplatePropertyFileName=UPDATE_DEVICEID_TEMPLATE_T3;
    //
    //    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
    //    {
    //        if (local_selectedIndex >=0)
    //            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:(local_selectedIndex >=0)?local_selectedIndex:0 withDataArray:( (int)[local_ContentArray count]>=0)?local_ContentArray:nil withSubIndex:0];
    //        else
    //            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
    //
    //        [self.view addSubview:(UIView *)object];
    //    }
    //    else if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
    //    {
    //        object = [[nextClass alloc] initWithNibName:local_nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:local_nextTemplatePropertyFileName withPropertyFile:local_nextTemplatePropertyFileName withProcessorCode:nil dataArray:nil dataDictionary:nil];
    //        [self.navigationController pushViewController:object animated:NO];
    //    }
}


#pragma mark - RECHABILITY ALERT
-(void)netConnectionStatusMessage
{
    reachability = [Reachability reachabilityForInternetConnection];
    netStatus = [reachability  currentReachabilityStatus];
    NSString *smsMode = NSLocalizedStringFromTableInBundle(@"application_deafault_communication_mode",@"GeneralSettings",[NSBundle mainBundle], nil);
    if ((netStatus == NotReachable) || [smsMode isEqualToString:@"SMS"]){
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_sms_alert_title", nil)]] message:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_sms_alert_message", nil)]] delegate:self cancelButtonTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_ok_button", nil)]] otherButtonTitles:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancelbutton", nil)]],nil];
        //        [alert show];
        
        
        
        UIAlertController *alert= [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_sms_alert_title", nil)]] message:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_sms_alert_message", nil)]] preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_ok_button", nil)]] style:UIAlertActionStyleCancel handler:nil]];
        
        [alert addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancelbutton", nil)]] style:UIAlertActionStyleCancel handler:nil]];
        
        UIViewController *topController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
        while (topController.presentedViewController)
        {
            topController = topController.presentedViewController;
        }
        [topController presentViewController:alert animated:NO completion:nil];
        
    }
}


-(void)poptoRootView
{
    AppDelegate *apDlgt = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    UINavigationController *navCntrl = (UINavigationController *)apDlgt.window.rootViewController;
    if (navCntrl && [navCntrl isKindOfClass:[UINavigationController class]])
    {
        [navCntrl popViewControllerAnimated:NO];
    }
}

-(void)gotoHomeScreen
{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

//below method is to get dict for verify registration popuptemplate 6
-(NSMutableDictionary*)getDictValues:(WebServiceDataObject *)webServiceDataObject{
    NSMutableDictionary *localDataDictionary = [[NSMutableDictionary alloc] init];
    NSString *mobNo = @"";
    NSString *valueString;
    NSArray *arr;
    if(webServiceDataObject.PaymentDetails1)
        mobNo = webServiceDataObject.PaymentDetails1;
    if(webServiceDataObject.PaymentDetails2)
        valueString = webServiceDataObject.PaymentDetails2;
    if(valueString)
        arr = [valueString componentsSeparatedByString:@"|"];
    
    [localDataDictionary setObject:@"Mobile Number" forKey:@"mobile_number"];
    [localDataDictionary setObject:mobNo forKey:@"mobile_number_value"];
    [localDataDictionary setObject:mobNo forKey:PARAMETER17];
    
    if([arr count] == 14){
        [localDataDictionary setObject:@"First Name" forKey:@"first_name"];
        [localDataDictionary setObject:[arr objectAtIndex:0] forKey:@"first_name_value"];
        
        [localDataDictionary setObject:@"Father Name" forKey:@"father_name"];
        [localDataDictionary setObject:[arr objectAtIndex:1] forKey:@"father_name_value"];
        
        [localDataDictionary setObject:@"Grand Father Name" forKey:@"gfather_name"];
        [localDataDictionary setObject:[arr objectAtIndex:2] forKey:@"gfather_name_value"];
        
        [localDataDictionary setObject:@"Family Name" forKey:@"family_name"];
        [localDataDictionary setObject:[arr objectAtIndex:3] forKey:@"family_name_value"];
        
        //for the rest of label titles below, you can edit them in the strings file VerifyRegistrationPPT6_T1
        [localDataDictionary setObject:[arr objectAtIndex:4] forKey:@"date_of_birth_value"];
        
        [localDataDictionary setObject:[arr objectAtIndex:5] forKey:@"place_of_birth_value"];
        
        [localDataDictionary setObject:[arr objectAtIndex:6] forKey:@"gender_value"];
        
        [localDataDictionary setObject:[arr objectAtIndex:7] forKey:@"id_type_value"];
        
        [localDataDictionary setObject:[arr objectAtIndex:8] forKey:@"id_number_value"];
        
        [localDataDictionary setObject:[arr objectAtIndex:9] forKey:@"id_expiry_date_value"];
        
        [localDataDictionary setObject:[arr objectAtIndex:10] forKey:@"governorate_value"];
        
        [localDataDictionary setObject:[arr objectAtIndex:11] forKey:@"district_value"];
        
        [localDataDictionary setObject:[arr objectAtIndex:12] forKey:@"street_value"];
        
        [localDataDictionary setObject:[arr objectAtIndex:13] forKey:@"zone_value"];
    }
    if(webServiceDataObject.Reference1)
        [localDataDictionary setObject:webServiceDataObject.Reference1 forKey:@"Reference1"];
    if(webServiceDataObject.Reference2)
        [localDataDictionary setObject:webServiceDataObject.Reference2 forKey:@"Reference2"];
    if(webServiceDataObject.Reference3)
        [localDataDictionary setObject:webServiceDataObject.Reference3 forKey:@"Reference3"];
    if(webServiceDataObject.Reference4)
        [localDataDictionary setObject:webServiceDataObject.Reference4 forKey:@"Reference4"];
    return localDataDictionary;
}

-(void)updateRecentTxns{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateRecentTransactions" object:self userInfo:nil];
}

@end
