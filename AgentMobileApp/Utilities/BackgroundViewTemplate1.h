//
//  BackgroundViewTemplate1.h
//  AgentMobileApp
//
//  Created by Gokul Krishnan on 29/01/18.
//  Copyright © 2018 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface BackgroundViewTemplate1 : UIView

@property IBInspectable CGFloat contentViewBorderWidth;
@property IBInspectable UIColor* contentViewBorderColor;


@end
