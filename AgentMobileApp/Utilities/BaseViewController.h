//
//  BaseViewController.h
//  Consumer Client
//
//  Created by android on 7/23/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageHeaderView.h"
#import "ActivityIndicator.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import "PopUpTemplate2.h"

@class PopUpTemplate1;

/**
 * This class used to handle functionality and UIViewController of BaseViewController.
 *
 * @author Integra
 *
 */
@interface BaseViewController : UIViewController
{
    NSString *local_actionType;
    id local_id;
    NSString *local_nextTemplate;
    NSString *local_nextTemplatePropertyFileName;
    NSString *local_currentTemplatePropertyFileName;
    NSString *local_alertType;
    NSString *local_validationType;
    NSString *local_apiType;
    NSString *local_processorCode;
    NSString *local_selected_processorCode;
    NSMutableArray *local_ContentArray;
    NSMutableDictionary *local_dataDictionary;
    NSArray *local_labelValidationArray;
    NSString *local_currentClass;
    NSMutableDictionary *webServiceRequestInputDetails;
    NSDictionary *postDictionary;
    int local_selectedIndex;
    ActivityIndicator *activityIndicator;
    NSString *local_transactionType;
    NSString *currentImplementationClass;
    SEL baseSelector;
    id basetarget;
    NSString *DigitalKYCUploadString;
    NSString *currentPCode;
    NSString *currentTType;
    
}
@property(retain,nonatomic) UIViewController *topController;
/**
 * This method is used to handle functionality of BaseViewController by using Actiontype((0 to 18)Action Types defined on SRS Document).
 */
-(void)performAction;
/**
 * This method is used to Clear the Previous View Stack related in BaseViewController.
 */
-(void) removeViewsWithClassType:(BOOL) clear;
/**
 * This method is used to ParentTemplate7 Views Loading By using ParentSubView with PageHeader.
 */
-(void) setParentTemplatewithHeader:(PageHeaderView *)pageHeader;

/**
 * This method is used to populating data into list view based on processor code.
 */
+ (BOOL)getDataFromProcessoreCode : (NSString *)processorCode andPropertyFileName : (NSString *)propertyFileName forArray : (NSMutableArray *__strong *)dataArray;

/**
 * This method is used to PopupTemplate1 view varying With Frame size.
 */
+ (BOOL)getControlFrameForProcessorCode : (NSString *)processorCode andPropertyFile : (NSString *)propertyFileName forControl : (UILabel *__strong *)controlView toCenter : (PopUpTemplate1 *)PPT1 withYCoordinate: (CGFloat)y_Position andXCoordinate : (CGFloat)x_Position andHeight : (CGFloat)height andChangeableyCoordinate : (float *)y_Position_change withAdditonalControl : (UIScrollView *__strong *)controlView1 andButton1 : (UIButton *__strong *)button1 andButton2 : (UIButton *__strong *)button2 withDictionary : (NSDictionary *)localDataDictionary forSequence : (int)sequence;
/**
 * This method is used to add activity indicator(Progress bar) to view.
 */
-(void) setActivityIndicator;
/**
 * This method is used to remove activity indicator(Progress bar) to view.
 */
-(void) removeActivityIndicator;

@end
