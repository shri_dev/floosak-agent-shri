//
//  SplashViewController.m
//  Consumer Client
//
//  Created by test on 07/10/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "SplashViewController.h"
#import "Constants.h"
#import "BaseViewController.h"
#import "AppDelegate.h"
#import "ParentTemplate3.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

#pragma mark OS Default methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"Default" inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    [self performSelector:@selector(changeRoot) withObject:nil afterDelay:(int)[NSLocalizedStringFromTableInBundle(@"splash_screen_visible_time",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue]];
}


#pragma mark Helper methods
- (void)changeRoot
{
    int userLevel = (int)[NSLocalizedStringFromTableInBundle(@"application_default_user_support_mode",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
    
    if (userLevel == 0) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSDictionary *dictionary = [userDefaults objectForKey:@"userDetails"];
        NSString *phoneNumber = [dictionary objectForKey:PARAMETER9];
//        NSLog(@"Mobile number is...%@",phoneNumber);
        if (([phoneNumber compare:@"(null)"] != NSOrderedSame) && ([phoneNumber length] != 0)) {
            UIViewController *object = [[NSClassFromString(application_launch_template) alloc] initWithNibName:application_launch_template bundle:nil withSelectedIndex:0 fromView:3 withFromView:nil withPropertyFile:single_user_application_launch_template_property_files_list withProcessorCode:nil dataArray:nil dataDictionary:nil];
            UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:object];
            [navi setNavigationBarHidden:YES];
            AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
            [delegate.window setRootViewController:navi];
        }
        else
        {
            UIViewController *object = [[NSClassFromString(application_launch_template) alloc] initWithNibName:application_launch_template bundle:nil withSelectedIndex:0 fromView:3 withFromView:nil withPropertyFile:application_launch_template_property_files_list withProcessorCode:nil dataArray:nil dataDictionary:nil];
            UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:object];
            [navi setNavigationBarHidden:YES];
            AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
            [delegate.window setRootViewController:navi];
        }
    }
    else
    {
        UIViewController *object= [[NSClassFromString(application_launch_template) alloc] initWithNibName:application_launch_template bundle:nil withSelectedIndex:0 fromView:3 withFromView:nil withPropertyFile:application_launch_template_property_files_list_T1 withProcessorCode:nil dataArray:nil dataDictionary:nil];
        UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:object];
        [navi setNavigationBarHidden:YES];
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        [delegate.window setRootViewController:navi];
    }
}

#pragma mark - Default memory warning method.
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
