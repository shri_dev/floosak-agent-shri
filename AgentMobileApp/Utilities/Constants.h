//
//  Constants.h
//  Consumer Client
//
//  Created by Integra Micro on 1/27/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//


// Payu Seamless Integration

#define     VIEW_CONTROLLER_IDENTIFIER_PAYMENT_OPTION           @"PaymentOptionVC"
#define     VIEW_CONTROLLER_IDENTIFIER_PAYMENT_UIWEBVIEW        @"PaymentUIWebViewVC"
#define     VIEW_CONTROLLER_IDENTIFIER_PAYMENT_CCDC             @"PaymentCCDCVC"
#define     VIEW_CONTROLLER_IDENTIFIER_PAYMENT_NET_BANKING      @"PaymentNB"
#define     VIEW_CONTROLLER_IDENTIFIER_PAYMENT_STORED_CARD      @"PaymentSC"

#define     CELL_IDENTIFIER_NETBANKING                          @"NetBankingCell"
#define     CELL_IDENTIFIER_STOREDCARD                          @"StoredCardCell"


//#define     PAYUALERT(T,M)                  {[[[UIAlertView alloc] initWithTitle:T message:M delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];}

#define PAYUALERT(T,M)  {UIAlertController *alert= [UIAlertController alertControllerWithTitle:T message:M preferredStyle:UIAlertControllerStyleAlert];[alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];UIViewController *topController =[[[UIApplication sharedApplication] delegate] window].rootViewController;while (topController.presentedViewController){topController = topController.presentedViewController;}[topController presentViewController:alert animated:NO completion:nil];}


#define ENGLSIH_LANGUAGE    @"English"
#define ARABIC_LANGUAGE     @"Arabic"
#define HINDI_LANGUAGE      @"HINDI"
#define RUSSIAN_LANGUAGE    @"RUSSIAN"
#define TELUGU_LANGUAGE     @"TELUGU"
#define KANNADA_LANGUAGE    @"KANNADA"


#define userLanguage @"userLanguage"
#define en  ENGLSIH_LANGUAGE
#define ar  ARABIC_LANGUAGE

#define SCREEN_WIDTH  [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height

#define FONT_NAME @"HelveticaNeue-Light"
#define BOLD_FONT_NAME @"HelveticaNeue-Bold"
#define FONT_SIZE 14
#define FONTSIZE_18 18
#define FONTSIZE_26 26
#define SMALL_FONT_SIZE 12
#define SIDE_POPUP_VIEW_YPOS 64



// Image Names
#define MENU_IMAGE_NAME @"menuicon.png"
#define BACK_BUTTON_IMAGE @"back_normal.png"
#define HEADER_IMAGE_NAME @"Obopay.png"
#define DELETE_IMG @"Drop down_Close.png"
#define DROPDOWN_IMAGE_NAME @"dropDown.png"
#define CLOSE_IMAGE_NAME @"close.png"
#define SELECTED_CHECK_MARK_IMAGE @"CAB_Check_Selected.png"


#define APP_BGCOLOR [UIColor colorWithRed:(230.0 / 255.0) green:(230.0 / 255.0) blue:(230.0 / 255.0) alpha:1.0]
#define TITLE_TEXT_COLOR [UIColor colorWithRed:(51.0 / 255.0) green:(51.0 / 255.0) blue:(51.0 / 255.0) alpha:1.0]
#define TEXTFIELD_PLACEHOLDER_TEXTCOLOR [UIColor colorWithRed:(51.0 / 255.0) green:(51.0 / 255.0) blue:(51.0 / 255.0) alpha:1.0]
#define BACKGROUND_COLOR [UIColor colorWithRed:(236.0 / 255.0) green:(236.0 / 255.0) blue:(236.0 / 255.0) alpha:1.0]
#define SIDEMENU_BACKGROUND_COLOR [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0]


#define TRUE_TEXT @"true"
#define FALSE_TEXT @"false"

#define YES_TEXT @"yes"
#define NO_TEXT @"no"

#define SMS_ENCRYPTION_TYPE_1 1
#define SMS_ENCRYPTION_TYPE_2 2

#define SMS_MODE @"SMS"
#define GPRS_MODE @"GPRS"


#define application_launch_template @"ParentTemplate3"
#define application_launch_template_property_files_list @"PreLoginPT3_T3"

//Single User
#define single_user_application_launch_template_property_files_list @"SingleUser_PreLoginPT3"

// UpdateDevice ID
#define UPDATE_DEVICEID_TEMPLATE @"UpdateDeviceIDPPT2"
#define WEBSERVICE_REFERENCE_DATA @"6"

#define UPDATE_USER_MOBILE_NUMBER @"MultiUserMobileNumPPT2"
#define UPDATE_USER_MOBILE_NUMBER_T3 @"MultiUserPhoneNumberPPT2_T3"

#define UPDATE_DEVICEID_TEMPLATE_T3 @"UpdateDeviceIDPPT2_T3"

#define LAUNCH_PAGE_TEMPLATE    @"LaunchPagePT5_T1"

// SIGNUP
#define SELF_REG_PAYMENTDETAILS2 @"30"
#define SELF_REG_REFERENCE1    @"111"

#define ACTION_TYPE_1 @"1"
#define ACTION_TYPE_2 @"2"
#define ACTION_TYPE_3 @"3"
#define ACTION_TYPE_4 @"4"
#define ACTION_TYPE_5 @"5"
#define ACTION_TYPE_6 @"6"
#define ACTION_TYPE_7 @"7"
#define ACTION_TYPE_8 @"8"
#define ACTION_TYPE_9 @"9"
#define ACTION_TYPE_10 @"10"
#define ACTION_TYPE_11 @"11"
#define ACTION_TYPE_12 @"12"
#define ACTION_TYPE_13 @"13"
#define ACTION_TYPE_14 @"14"
#define ACTION_TYPE_15 @"15"
#define ACTION_TYPE_16 @"16"
#define ACTION_TYPE_17 @"17"
#define ACTION_TYPE_18 @"18"
#define ACTION_TYPE_19 @"19"
#define ACTION_TYPE_20 @"20"

//Text Normal,Bold and Italic Format
//0-Normal
//1-Bold
//2-Italic

#define TEXT_STYLE_0 @"0"
#define TEXT_STYLE_1 @"1"
#define TEXT_STYLE_2 @"2"

// KeyBoard Types
#define DEFAULTKEYBOARDTYPE_0 @"0"
#define DECIMALKEYBOARDTYPE_1 @"1"
#define PHONENUMBERKEYBOARDTYPE_2 @"2"
#define NUMBERWITHSECUREKEYBOARDTYPE_3 @"3"
#define NUMBERKEYBOARDTYPE_4 @"4"
#define DATEOFBIRTHKEYBOARDTYPE @"4"

#define SETDATEFORMATDATEMONTHYEAR @"dd/MM/yyyy"
#define SETDATEFORMATDATEMONTHYEARWITHTIME @"ddMMyyHHmm"
#define SETDATEFORMATYEARMONTHDATE @"yyyy-MM-dd HH:mm:ss.SSS"
#define SERVER_DATE_FORMAT @"yyyy-MM-dd'T'HH:mm:ss.SSS"
#define DATE_TIME_FORMAT_WITH_TIMEZONE @"yyyy-MM-dd'T'HH:mm:ss.SSSZ"
#define SERVER_LAST_TRANSACTION_TIME   @"ServerLastTransactionTime"
#define DEVICE_LAST_TRANSACTION_TIME   @"DeviceLastTransactionTime"

#define LOCALIZATION @"Localization"


#define application_branding_color_theme @"#F29400"

//  To Be changed
// Default values of the text label in the application
// Please provide what would be the default if no value is given.
#define application_default_text_size @"14"
#define application_default_text_color @"#000000"
#define application_default_text_style @"1"

// Default values of the Input text (typing by user) in the application
// Please provide what would be the default if no value is given.
#define application_default_value_text_size @"14"
#define application_default_value_text_color @"#333333"
#define application_default_value_text_style @"1"
#define application_default_value_keyboard_type @"0"

// Default values of the Dropdown in the application
// Please provide what would be the default if no value is given.
#define application_default_drop_down_value_text_size @"14"
#define application_default_drop_down_value_text_color @ "#333333"
#define application_default_drop_down_value_text_style @"1"

// Default values of the Button in the application
// Please provide what would be the default if no value is given.
#define application_default_button_text_size @"14"
#define application_default_button_text_color @"#ffffff"
#define application_default_button_text_style @"1"
//#define  application_default_button_background_color @"#F29400"
#define  application_default_button_background_color @"#792744"

#define TICKER_TEXT @"Ticker"
#define POPUP_TEXT @"PopUp"

#define GRID @"GRIDVIEW"
#define LIST @"LISTVIEW"

#define application_default_no_value_available @"NA"

#define default_dth_operator_name @"AIRTELTV"
#define default_dth_operator_id @"AIRTEL"

#define default_operator_name @"AIRCEL"
#define default_operator_id   @"AIRC"

#define parent_template5_login_user_name_back_ground_color @"ff6633"

#define HOMESCREEN_APPROACH_TYPE1 @"1"
#define HOMESCREEN_APPROACH_TYPE2 @"2"

//Additional
#define FIRST_LOGIN @"firstLogin"
#define FEATURELIST_BORDER_COLOR @"#000000"

// Additional For T1
#define application_launch_template_property_files_list_T1 @"PreLoginPT3_T3"

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


#define IS_DEBUGGING NO
