 //
//  PageHeaderView.m
//  Consumer Client
//
//  Created by android on 6/18/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "PageHeaderView.h"
#import "Constants.h"

@implementation PageHeaderView

@synthesize delegate,rightButtondelegate,logoIcon;
@synthesize leftMenu_Icon,header_titleLabel,leftmenuButton1,leftmenuButton2;
@synthesize rightmenu_Button1,rightmenu_Button2,rightmenu_Button3;

#pragma mark - PageHeader UIView.
/*
 * This method is used to set The frame for Page Header.
 */
-(id)initWithFrame:(CGRect)frame withHeaderTitle:(NSString *)title withLeftbarBtn1Image_IconName:(NSString *)leftBtn1_Icon withLeftbarBtn2Image_IconName:(NSString *)leftBtn2_Icon  withHeaderButtonImage:(NSString *)headerBtn_Icon withRightbarBtn1Image_IconName:(NSString *)rightBtn1_Icon withRightbarBtn2Image_IconName:(NSString *)rightBtn2_Icon withRightbarBtn3Image_IconName:(NSString *)rightBtn3_Icon withTag:(int)viewTag
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setBackgroundColor:APP_BGCOLOR];
        
        header_titleLabel=[[UILabel alloc]init];
        leftMenu_Icon=[[UIImageView alloc]init];
        
        leftmenuButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
        leftmenuButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
        
        rightmenu_Button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        rightmenu_Button2 = [UIButton buttonWithType:UIButtonTypeCustom];
        rightmenu_Button3 = [UIButton buttonWithType:UIButtonTypeCustom];
        
        if (viewTag == 0)
        {
            viewTags = viewTag;
            
            // Left menu Button
            [leftmenuButton1 setFrame:CGRectMake(0, 20,(SCREEN_WIDTH/4),44)];
            [leftmenuButton1 addTarget:self action:@selector(menu_Action:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:leftmenuButton1];
            
            leftMenu_Icon.frame=CGRectMake(6, 30, 30, 25);
            leftMenu_Icon.image=[UIImage imageNamed:leftBtn1_Icon inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil];
            leftMenu_Icon.contentMode=UIViewContentModeScaleAspectFit;
            [self addSubview:leftMenu_Icon];
            
            header_titleLabel.frame=CGRectMake(32, 27, (SCREEN_WIDTH/2), 32);
            
            logoIcon = [[UIImageView alloc] init];
            float x = (SCREEN_WIDTH * 240)/320;
            logoIcon.frame=CGRectMake(leftMenu_Icon.frame.origin.x+x, 30, (SCREEN_WIDTH/4), 25);
            [logoIcon setImage:[UIImage imageNamed:headerBtn_Icon inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil]];
            logoIcon.contentMode=UIViewContentModeScaleAspectFit;
            [self addSubview:logoIcon];
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                leftMenu_Icon.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
                logoIcon.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
            }
            else
            {
                leftMenu_Icon.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
                logoIcon.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
            }

        }
        else if (viewTag == 1)
        {
            viewTags = viewTag;
            
            // Left menu Button
            [leftmenuButton2 setFrame:CGRectMake(0, 20, (SCREEN_WIDTH)/2, 44)];
            [leftmenuButton2 addTarget:self action:@selector(checkMarkBtn_Action:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:leftmenuButton2];
            
            leftMenu_Icon.frame=CGRectMake(7, 30, 25, 25);
            leftMenu_Icon.image=[UIImage imageNamed:leftBtn2_Icon inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil];
            [self addSubview:leftMenu_Icon];
            
            //header title
            header_titleLabel.frame=CGRectMake(38, 26, (SCREEN_WIDTH/2), 32);
            
            //right button1
            rightmenu_Button1.frame=CGRectMake(header_titleLabel.frame.size.width+50, 22, 30, 30);
            [rightmenu_Button1 addTarget:self action:@selector(infoBtn_Action:) forControlEvents:UIControlEventTouchUpInside];
            [rightmenu_Button1 setImage:[UIImage imageNamed:rightBtn1_Icon inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
            [self addSubview:rightmenu_Button1];
            
            //right button2
            rightmenu_Button2.frame=CGRectMake(rightmenu_Button1.frame.origin.x+rightmenu_Button1.frame.size.width+10, 29, 25, 25);
            [rightmenu_Button2 addTarget:self action:@selector(editBtn_Action:) forControlEvents:UIControlEventTouchUpInside];
            [rightmenu_Button2 setImage:[UIImage imageNamed:rightBtn2_Icon inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
            [self addSubview:rightmenu_Button2];
            
            //right button3
            rightmenu_Button3.frame=CGRectMake(rightmenu_Button2.frame.origin.x+rightmenu_Button2.frame.size.width+10, 29, 25, 25);
            [rightmenu_Button3 addTarget:self action:@selector(deleteBtn_Action:) forControlEvents:UIControlEventTouchUpInside];
            [rightmenu_Button3 setImage:[UIImage imageNamed:rightBtn3_Icon inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
            [self addSubview:rightmenu_Button3];
        }
        else if (viewTag == 2)
        {
            viewTags = viewTag;
            
            // Left menu Button
            [leftmenuButton2 setFrame:CGRectMake(0, 30, (SCREEN_WIDTH/2), 44)];
            [leftmenuButton2 addTarget:self action:@selector(checkMarkBtn_Action:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:leftmenuButton2];
            
            leftMenu_Icon.frame=CGRectMake(7,26,25,25);
            leftMenu_Icon.image=[UIImage imageNamed:leftBtn2_Icon inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil];
            [self addSubview:leftMenu_Icon];
            
            //header title
            header_titleLabel.frame=CGRectMake(leftMenu_Icon.frame.origin.x+leftMenu_Icon.frame.size.width+22, 26, (SCREEN_WIDTH/2), 26);
            
            //right button3
            if (SCREEN_HEIGHT>667)
                 rightmenu_Button3.frame=CGRectMake(leftmenuButton2.frame.size.width+160, 30, 25, 25);
            else
                 rightmenu_Button3.frame=CGRectMake(leftmenuButton2.frame.size.width+120, 30, 25, 25);

            [rightmenu_Button3 setImage:[UIImage imageNamed:rightBtn3_Icon inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
            [rightmenu_Button3 addTarget:self action:@selector(deleteBtn_Action:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:rightmenu_Button3];
        }
    }
    
    //Header label
    header_titleLabel.text = title;
    header_titleLabel.textColor = TITLE_TEXT_COLOR;
    header_titleLabel.font = [UIFont fontWithName:BOLD_FONT_NAME size:FONT_SIZE];
    header_titleLabel.frame = CGRectMake(140, 30, SCREEN_WIDTH/2, 30);
    [self addSubview:header_titleLabel];
    
    return self;
}

#pragma mark -  PAGE HEADER LEFT MENU BUTTON ACTION
/**
 * This method is used to set Delegate Method of PageHeaderView Button Action.
 */
-(void)menu_Action:(id)sender
{
    if (self.delegate != nil)
    {
        [self.delegate menuBtn_Action];
    }
    
}

#pragma mark - PAGE HEADER RIGHT BUTTON METHODS

-(void)checkMarkBtn_Action:(id)sender
{
    if (self.rightButtondelegate != nil)
    {
        [self.rightButtondelegate checkMarkBtn_Action];
    }
}

-(void)editBtn_Action:(id)sender
{
    if (self.rightButtondelegate != nil)
    {
        [self.rightButtondelegate editBtn_Action];
    }
}

-(void)infoBtn_Action:(id)sender
{
    if (self.rightButtondelegate != nil)
    {
        [self.rightButtondelegate infoBtn_Action];
    }
}

-(void)deleteBtn_Action:(id)sender
{
    if (self.rightButtondelegate != nil)
    {
        [self.rightButtondelegate deleteBtn_Action];
    }
}

@end
