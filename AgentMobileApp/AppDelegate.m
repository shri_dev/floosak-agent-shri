
//
//  AppDelegate.m
//  Consumer Client
//
//  Created by Integra Micro on 1/27/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//

#import "AppDelegate.h"
#import "Constants.h"
#import "ParentTemplate3.h"
#import "ParentTemplate4.h"
#import "WebSericeUtils.h"
#import "WebServiceConstants.h"
#import "WebServiceRequestFormation.h"
#import "WebServiceRunning.h"
#import "WebServiceDataObject.h"
#import "BaseViewController.h"
#import "DatabaseConstatants.h"
#import "SplashViewController.h"
#import <CommonCrypto/CommonHMAC.h>
#import <objc/runtime.h>
#import "GenerateOTP.h"
#import "Localization.h"
#import "Template.h"
#import "PushNotifiactionViewController.h"
#import "PopUpTemplate2.h"
#import "GenerateOTP.h"
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>
#import "AGPushNoteView.h"
//#import "Log.h"

@interface AppDelegate ()<UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate
{
    BOOL localNotificationStatus;
    NSMutableDictionary *pushNotificationData;
    NSDictionary * userInfoDictionary;
}
@synthesize window = _window;

/*
 * This method Denotes application Start point.
 */

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    [self parseStatic];
    
    /*
     * This method is used stored data locally.
     */
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    /*
     * This method is to set Login status.
     */
    if ([userDefaults integerForKey:FIRST_LOGIN] == 0) {
        [userDefaults setInteger:1 forKey:FIRST_LOGIN];
        [userDefaults synchronize];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(transformScreen:) name:@"TransformScreen" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(transformScreenAr:) name:@"TransformScreenAr" object:nil];
    
    NSString *loggedIn = [userDefaults objectForKey:@"userLogged"];
    /*
     * This method is to set Application communication mode.
     */
    NSString *smsMode = NSLocalizedStringFromTableInBundle(@"application_deafault_communication_mode",@"GeneralSettings",[NSBundle mainBundle], nil);
    NSMutableDictionary *userDetails =[[NSMutableDictionary alloc] initWithDictionary:[userDefaults objectForKey:@"userDetails"]];
    
    if ([userDefaults objectForKey:@"userDetails"]) {
        [userDetails setObject:smsMode forKey:PARAMETER36];
        [userDefaults setObject:userDetails forKey:@"userDetails"];
    }
    
    if ([loggedIn integerValue] == 1)
    {
        [userDefaults setValue:@"0" forKey:@"userLogged"];
        [userDefaults synchronize];
    }
    
    /*
     * This method is to set Application default language.
     */
    
    if (![userDefaults valueForKey:userLanguage])
    {
        [userDefaults setValue:NSLocalizedStringFromTableInBundle(@"application_default_language",@"GeneralSettings",[NSBundle mainBundle], nil) forKey:userLanguage];
        [userDefaults synchronize];
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    if ([[userDefaults valueForKey:userLanguage] isEqualToString:ar])
    {
        self.window.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
    }
    else
    {
        self.window.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
    }
    
    /*
     * This method is to set load Application Splash screen.
     */
    if (![application_launch_template_property_files_list isEqualToString:@""] && ![application_launch_template isEqualToString:@""])
    {
        SplashViewController *splashVC = [[SplashViewController alloc]init];
        [self.window setRootViewController:splashVC];
        if ([smsMode isEqualToString:@"GPRS"]) {
            [self getLaunchApiResponseData];
        }
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"HomePageType"];
    
    NSString *phoneNumberStr=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
    NSLog(@"Phone number string is..%@",phoneNumberStr);
    
    /*
     * This method is to set handle application push notification.
     */
    // Push Notification
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
         [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [application registerForRemoteNotifications];
    }
    else
    {
        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    
    
    if (launchOptions!=nil)
    {
        if([launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey])
        {
            [application setApplicationIconBadgeNumber:0];
            NSString *alertMsg=[[[[launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey] objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"body"];
            [[[UIAlertView alloc] initWithTitle:[Localization languageSelectedStringForKey:@"label_id_transaction_init"] message:alertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else if ([launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey])
        {
            [application setApplicationIconBadgeNumber:0];
            NSString *alertMsg=[[launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey] alertBody];
            [[[UIAlertView alloc] initWithTitle:[Localization languageSelectedStringForKey:@"label_id_transaction_init"] message:alertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }
    
    [self.window makeKeyAndVisible];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"LatestMiniStatement"];

//    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
//        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//        center.delegate = self;
//        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
//            if( !error ){
//                [[UIApplication sharedApplication] registerForRemoteNotifications];
//            }
//        }];
//    }
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma pushNotification handler
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *devToken = [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<"withString:@""]stringByReplacingOccurrencesOfString:@">" withString:@""] stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    deviceIdentifier = devToken;
    NSLog(@"Device Token:%@",deviceIdentifier);
    [[NSUserDefaults standardUserDefaults] setObject:deviceIdentifier forKey:@"DeviceToken"];
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSLog(@"Error:%@",err.localizedDescription);
}
/*
 * This method is to set handle application push notification using application delegate methods.
 */
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"User info :%@",userInfo);
    
    [application setApplicationIconBadgeNumber:0];
    /*
     * This method is to set handle application push notification (Type1).
     */
    
    NSString *alertMsg=[[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"body"];
    
    if (application.applicationState == UIApplicationStateActive)
    {
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.alertBody = alertMsg;
        localNotification.applicationIconBadgeNumber=application.applicationIconBadgeNumber+1;
        localNotification.timeZone=[NSTimeZone defaultTimeZone];
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:0];
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        
    }
    /*
     * This method is to set handle application push notification (Type2).
     */
    if (application.applicationState == UIApplicationStateBackground || application.applicationState == UIApplicationStateInactive)
    {
        [self handlePushNotification:alertMsg];
    }
}


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [application setApplicationIconBadgeNumber:0];
    
    if (application.applicationState == UIApplicationStateActive)
    {
        [AGPushNoteView showWithNotificationMessage:[NSString stringWithFormat:@"%@",notification.alertBody]];
        [AGPushNoteView setMessageAction:^(NSString *message) {
            [self handlePushNotification:message];
        }];
    }
}

-(void) handlePushNotification:(NSString *)message
{
    Class nextClass=NSClassFromString(@"PopUpTemplate2");
    id object = nil;
    NSString *local_nextTemplatePropertyFileName=@"PushNotificationPPT2";
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:message forKey:@"pushNotification"];
    
    if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
    {
        object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:local_nextTemplatePropertyFileName andDelegate:self withDataDictionary:dictionary withProcessorCode:nil withTag:-5 withTitle:nil andMessage:nil  withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        UIViewController *controller = ((UINavigationController*)delegate.window.rootViewController).visibleViewController;
        [[controller view] addSubview:(UIView *)object];
    }
}


#pragma mark - Error methods
-(void)errorCodeHandlingInXML:(NSString *)errorCode andMessgae:(NSString *)message withProcessorCode:(NSString *)processorCode
{

}

- (void)errorCodeHandlingInWebServiceRunningWithErrorCode:(NSString *)errorCode
{
    
}


#pragma mark - DropDown List Data.
-(void)getDropdownList
{
    databaseManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
    
    Reachability *reachability=[Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus=[reachability  currentReachabilityStatus];
    
    if (netStatus == NotReachable)
    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]] message:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_sms_alert_message", nil)]] delegate:self cancelButtonTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_ok_button", nil)]] otherButtonTitles:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancelbutton", nil)]],nil];
//         [alert show];
        
//        UIAlertController *alert= [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]] message:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_sms_alert_message", nil)]] preferredStyle:UIAlertControllerStyleAlert];
//        
//        [alert addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_ok_button", nil)]] style:UIAlertActionStyleCancel handler:nil]];
//               
//        UIViewController *topController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
//        while (topController.presentedViewController)
//        {
//            topController = topController.presentedViewController;
//        }
//        [topController presentViewController:alert animated:NO completion:nil];

        activityIndicator.hidden = YES;
    }
    else
    {
        WebSericeUtils *webUtils = [[WebSericeUtils alloc] init];
        
        NSString *proccessorCode = PROCESSOR_CODE_SELECT_LIST_DROPDOWN_LIST;
        
        NSMutableDictionary *webUtilsValues = [[NSMutableDictionary alloc] initWithDictionary:[webUtils getWebServiceBundleObjectWithProccessorCode:proccessorCode withInputDataModel:nil]];
        WebServiceRequestFormation *webServiceRequest = [[WebServiceRequestFormation alloc] init];
        NSString *webRequest  = [webServiceRequest sendRequest:webUtilsValues];
        
        WebServiceRunning *webServiceRun = [[WebServiceRunning alloc] init];
        [webServiceRun startWebServiceWithRequest:webRequest withReqDictionary:webUtilsValues  withDelegate:self];
    }
}

#pragma mark - LAUNCH API DATA
-(void)getLaunchApiResponseData
{
    WebSericeUtils *webUtils = [[WebSericeUtils alloc] init];
    NSMutableDictionary *webUtilsValues = [[NSMutableDictionary alloc] initWithDictionary:[webUtils getWebServiceBundleObjectWithProccessorCode:PROCESSOR_CODE_LAUNCH withInputDataModel:nil]];
    
    WebServiceRequestFormation *webServiceRequest = [[WebServiceRequestFormation alloc] init];
    NSString *webRequest  = [webServiceRequest sendRequest:webUtilsValues];
    
    WebServiceRunning *webServiceRun = [[WebServiceRunning alloc] init];
    [webServiceRun startWebServiceWithRequest:webRequest withReqDictionary:webUtilsValues withDelegate:self];
}

-(void)processData:(WebServiceDataObject *)webServiceDataObject withProcessCode:(NSString *)processCode
{
    if ([processCode isEqualToString:PROCESSOR_CODE_LAUNCH]) {
            [self performSelector:@selector(getDropdownList) withObject:nil afterDelay:1.0];
    }
}

-(void) parseStatic
{
    DatabaseManager *dataManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
    NSArray *dropDownContent = [dataManager getAllDropDownDetails];
    
    if (dropDownContent.count == 0) {
        NSMutableArray *dataArray = [[NSMutableArray alloc] init];
        NSString *typeName = @"typeName";
        NSString *typeDesc = @"typeDesc";
        NSString *jsonDataStr =  NSLocalizedStringFromTableInBundle(@"static_drop_down_data",@"DropDownTemplate",[NSBundle mainBundle], nil);
        NSError *jsonError;
        NSMutableDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[jsonDataStr dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&jsonError];
        
        NSArray *drodownNamesArray = [dataDict allKeys];

        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"totalDocuments"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        for (NSString *key in drodownNamesArray)
        {
            NSArray *internalArray = [[NSArray alloc] initWithArray:[dataDict objectForKey:key]];
            for (int i=0; i<[internalArray count]; i++)
            {
                NSMutableDictionary *dropdown_detailsDict = [[NSMutableDictionary alloc] init];
                [dropdown_detailsDict setObject:key forKey:DROP_DOWN_TYPE];
                
                if ([key isEqualToString:@"DKYC_DOC_TYPE"]) {
                    [[NSUserDefaults standardUserDefaults] setInteger:([[NSUserDefaults standardUserDefaults] integerForKey:@"totalDocuments"] + 1) forKey:@"totalDocuments"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                [dropdown_detailsDict setObject:[[internalArray objectAtIndex:i] valueForKey:typeName] forKey:DROP_DOWN_TYPE_NAME];
                if([[internalArray objectAtIndex:i] valueForKey:typeDesc])
                    [dropdown_detailsDict setObject:[[internalArray objectAtIndex:i] valueForKey:typeDesc] forKey:DROP_DOWN_TYPE_DESC];
                else
                    [dropdown_detailsDict setObject:@"" forKey:DROP_DOWN_TYPE_DESC];
                
                [dataArray addObject:dropdown_detailsDict];
            }
        }
        [dataManager recordInsertionIntoDropDownDetailsWithData:dataArray];
    }
}

+(NSString *)getDeviceIdentifier
{
    UIDevice *currentDevice = [UIDevice currentDevice];
    NSLog(@"%@", currentDevice);
    
    
#if TARGET_OS_SIMULATOR
     deviceIdentifier = @"780cd7cc8a5b79e72508f477e1bb67b88b739059674ee732e897050ace296123";
#else
    if (!deviceIdentifier)
    {
        deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    }
#endif
    
//    if ([currentDevice.model rangeOfString:@"Simulator"].location == NSNotFound) {
//        //running on device
//        deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
//    } else {
//        // running in Simulator
//        deviceIdentifier = @"780cd7cc8a5b79e72508f477e1bb67b88b739059674ee732e897050ace296123";
//    }
    
    NSLog(@"Device_ID:%@",deviceIdentifier);
    
    return deviceIdentifier;

}

+(NSString *)getExternalErrorMessagePropertyFile
{
    NSString *fileString = [[NSUserDefaults standardUserDefaults] objectForKey:userLanguage];
    NSString *staticFileString = @"ExternalErrorMessages";
    NSString *externalPropertyFileName = [NSString stringWithFormat:@"%@_%@",staticFileString,fileString.uppercaseString];
    return externalPropertyFileName;
}

+(BOOL)getNotificationStatus
{
    return notificationStatus;
}

//-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
//
//    //Called when a notification is delivered to a foreground app.
//
//    NSLog(@"Userinfo %@", notification.request.content.userInfo);
//
//    completionHandler(UNNotificationPresentationOptionAlert);
//}
//
//-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler{
//    //Called to let your app know which action was selected by the user for a given notification.
//    NSLog(@"Userinfo %@", response.notification.request.content.userInfo);
//}

- (void)transformScreen:(NSNotification *)notification{
    self.window.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isScreenTransformed"];
}

- (void)transformScreenAr:(NSNotification *)notification{
    self.window.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isScreenTransformed"];
}

@end
