//
//  main.m
//  Consumer Client
//
//  Created by Integra Micro on 1/27/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ScreenTimeOut.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, NSStringFromClass([ScreenTimeOut class]), NSStringFromClass([AppDelegate class]));
    }
}
