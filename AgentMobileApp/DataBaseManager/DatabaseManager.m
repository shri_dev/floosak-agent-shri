//
//  DatabaseManager.m
//  GenericDatabase
//
//  Created by PRABAKAR MP on 27/07/12.
//  Copyright 2012 CORPUS MOBILE LABS. All rights reserved.
//

#import "DatabaseManager.h"
#import "DatabaseConstatants.h"
#import "Constants.h"

@implementation DatabaseManager

@synthesize dbName;

- (id)initWithDatabaseName:(NSString *)databaseName{
    self = [super init];
    if (self)
    {
        dbName = databaseName;
        
    }
    return self;
}

#pragma mark Common Methods Starts
// Definition: This function is used to retrieve the documentDirectory to path.
-(NSString *) documentDirectoryPath
{
    NSString *documentsDirectory = NULL;
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentsDirectory = [documentPaths objectAtIndex:0];
    return documentsDirectory;
}

// Definition: This function is used to know the database is there or not.
- (BOOL)findDBPresent
{
    BOOL success;
    NSString *writableDBPath = NULL;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsDirectory = [self documentDirectoryPath];
    // NSLog(@"Document Directiory path : %@",documentsDirectory);
    if ( documentsDirectory!=NULL && dbName!=NULL )
        writableDBPath = [documentsDirectory stringByAppendingPathComponent:dbName];
    
    if ( writableDBPath!=NULL )
        success = [fileManager fileExistsAtPath:writableDBPath];
    
    return success;
}

// Definition: This function is used to copy the database to project bundle when there is no database
- (NSError *)createDatabaseIfNoDBPresent
{
    NSError *error = nil;
    // Check if DB present, otherwise do a copy of DB into documents directory for the app.
    if ( ![self findDBPresent] )
    {
        NSString *defaultDBPath = NULL;
        NSString *writableDBPath = NULL;
        BOOL success = FALSE;
        
        // The writable database does not exist, so copy the default to the appropriate location.
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *documentsDirectory = [self documentDirectoryPath];
        if ( documentsDirectory!=NULL && dbName!=NULL )
            writableDBPath = [documentsDirectory stringByAppendingPathComponent:dbName];
        defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:dbName];
        success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
        if ( !success )
            NSLog(@"Failed to create writable database file with message %@.", [error localizedDescription]);
    }
    return error;
}

// Definition: This function is used to retrieve the database to path.
- (NSString *)getDBPath
{
//    NSLog(@"getDBPath..");
    NSString* databasePath = NULL;
    NSString *documentsDirectory = [self documentDirectoryPath];
    if ( documentsDirectory!=NULL && [self findDBPresent] )
        databasePath = [documentsDirectory stringByAppendingPathComponent:dbName];
//    NSLog(@"return db path..%@", databasePath);
    return databasePath;
}

-(BOOL)isInserts
{
    BOOL isInserts = NO;
    
    if (![self findDBPresent])
    {
        NSLog(@"NO DB");
        if([self createDatabaseIfNoDBPresent])
            isInserts = NO;
        else
            isInserts = YES;
    }
    else
        isInserts = YES;
    
    return isInserts;
}

- (NSError *)recordInsertionErrorWithResult:(int)result
{
//    NSLog(@"recordInsertionErrorWithResult..");
    NSError *resultError;
    NSString *errStr;
    if (result == 0)
    {
        errStr = @"Insertion operation can not be completed. ";
        resultError = [[NSError alloc] initWithDomain:@"database" code:result
             userInfo:[NSDictionary dictionaryWithObject:errStr forKey:@"failureMsg"]];
    }
    else if (result == 1)
    {
        errStr = @"Insertion operation completed successfully. ";
        resultError = [[NSError alloc] initWithDomain:@"database" code:result userInfo:[NSDictionary dictionaryWithObject:errStr forKey:@"successMsg"]];
    }
//    if(IS_DEBUGGING){
//        UITextView *txtView = [[UITextView alloc] initWithFrame:CGRectMake(0, 260, SCREEN_WIDTH, 40)];
//        [txtView setBackgroundColor:[UIColor whiteColor]];
//        [txtView setText:errStr];
//        [txtView setUserInteractionEnabled:NO];
//        [txtView accessibilityScroll:UIAccessibilityScrollDirectionUp];
//        [[UIApplication sharedApplication].keyWindow addSubview:txtView];
//    }
    return resultError;
}
#pragma mark Common Methods Ends


#pragma mark Marketing Images Starts

/*
 Method used to get transaction columns
 */
-(NSArray *)getLaunchAPIColumns
{
    NSLog(@"getLaunchAPIColumns..");
    NSMutableArray *transHistories = [[NSMutableArray alloc] init];
    [transHistories addObject:MARKET_IMAGE_COLOUMN_ID];
    [transHistories addObject:MARKET_IMAGE_SERVER_URLS];
    [transHistories addObject:MARKET_IMAGE_LOCAL_URLS];
    
    return transHistories;
}

/*
 Method used to get all transaction details
 */
-(NSArray *)getAllLaunchAPIInformation
{
    NSLog(@"Getting all launch api info..");
    NSMutableArray *transHistories;
    NSString *readStmt = [NSString stringWithFormat:@"SELECT * FROM %@",TABLE_MARKET_IMAGE_URLS];
     NSLog(@"Read Statement....%@",readStmt);
    
    NSArray *fieldsArray = [self getLaunchAPIColumns];
        NSLog(@"Field Array....%@",fieldsArray);
    
    if([self isInserts])
    {
        sqlite3 *database;
        NSString *dbPath = [self getDBPath];
        if ( dbPath!=NULL )
        {
            if ( sqlite3_open([dbPath UTF8String], &database)==SQLITE_OK )
            {
                sqlite3_stmt *statement;
                if ( sqlite3_prepare_v2(database, [readStmt UTF8String], -1, &statement, NULL)==SQLITE_OK )
                {
                    transHistories = [[NSMutableArray alloc] init];
                    while ( sqlite3_step(statement)==SQLITE_ROW )
                    {
                        NSMutableDictionary *recordDictionary = [[NSMutableDictionary alloc] init];
                        // Read the data from the result row
                        for ( int i=0; i<[fieldsArray count]; i++ )
                        {
                            [recordDictionary setValue:[NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, i)] forKey:[fieldsArray objectAtIndex:i]];
                        }
                        [transHistories addObject:recordDictionary];

                    }
                    sqlite3_finalize(statement);
                }
                else
                {
                    sqlite3_reset(statement);
                    NSLog(@"Error = %s",sqlite3_errmsg(database));
                    NSLog(@"Database: readTableRecord: Statement Not Prepared");
                }
                sqlite3_close(database);
            }
            else
                NSLog(@"Database: readTableRecord :Database Not Opened");
        }
        else
            NSLog(@"Database: readTableRecord :Database Not availbale");
    }
    NSLog(@"Returning transaction history..%@", transHistories);
    return transHistories;
}

- (NSError *)recordInsertionIntoLaunchAPIUserDetailsWithData:(NSArray *)dataArray
{
    NSLog(@"recordInsertionIntoLaunchAPIUserDetailsWithData..");
    NSError *returnError = nil;
    
    if ([self isInserts])
    {
        NSLog(@"inserting..");
        NSString *dbPath = [self getDBPath];
        sqlite3 *database;
        // Setup the SQL Statement and compile it for faster access
        sqlite3_stmt *statement;
        // Open the database
        if( sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK )
        {
            for (int i=0; i<[dataArray count]; i++)
            {
                NSMutableString *insertStatement = [NSMutableString stringWithFormat:@"INSERT INTO %@ (%@,%@) VALUES ('%@','%@')",TABLE_MARKET_IMAGE_URLS,MARKET_IMAGE_SERVER_URLS,MARKET_IMAGE_LOCAL_URLS,[[dataArray objectAtIndex:i] valueForKey:MARKET_IMAGE_SERVER_URLS],[[dataArray objectAtIndex:i] valueForKey:MARKET_IMAGE_LOCAL_URLS]];
                
                if ( sqlite3_prepare_v2(database, [insertStatement UTF8String], -1, &statement, NULL)!=SQLITE_OK )
                {
                    NSLog(@"%s: error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_prepare_v2(database, [insertStatement UTF8String], -1, &statement, NULL));
                    returnError = [self recordInsertionErrorWithResult:0];
                    sqlite3_close(database);
                }
                else
                {
                    if ( sqlite3_step(statement) == SQLITE_DONE )
                    {
                        sqlite3_finalize(statement);
                        returnError = [self recordInsertionErrorWithResult:1];
                    }
                    else
                    {
                        NSLog(@"%s: error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_step(statement));
                        returnError = [self recordInsertionErrorWithResult:0];
                    }
                }
            }
            sqlite3_close(database);
        }
        else
        {
            NSLog(@"%s: step error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_open([dbPath UTF8String], &database));
            returnError = [self recordInsertionErrorWithResult:0];
        }
    }
    else
        [self recordInsertionErrorWithResult:0];
    
    NSLog(@"returning error.. %@", returnError);
    return returnError;
}


- (void)updateTableDataInLaunchAPIDetailsWithServerURL:(NSString *)localImageUrl forServerURL: (NSString *)marketImageURL
{
    NSLog(@"updateTableDataInLaunchAPIDetailsWithServerURL..");
    sqlite3 *database;
    
    NSString *updateStmt = [NSString stringWithFormat:@"UPDATE %@ SET %@ = '%@' where market_image_local_url = '%@'",TABLE_MARKET_IMAGE_URLS,MARKET_IMAGE_SERVER_URLS,marketImageURL,localImageUrl];
    
    NSLog(@"Update Statement : %@",updateStmt);
    
    NSString *dbPath = [self getDBPath];
    if ( dbPath != NULL )
    {
        if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK )
        {
            sqlite3_stmt *statement;
            if ( sqlite3_prepare_v2(database, [updateStmt UTF8String], -1, &statement, NULL) != SQLITE_OK )
            {
//                NSLog(@"Database: updateTableData: sqlite3_prepare_v2. '%s'", sqlite3_errmsg(database));
                sqlite3_close(database);
            }
            else
            {
                if ( sqlite3_step(statement) == SQLITE_DONE)
                {
//                    NSLog(@"Database: updateTableData: Success");
                    sqlite3_finalize(statement);
                }
                else
                {
                    NSLog(@"Database: updateTableData: sqlite3_step. '%s'", sqlite3_errmsg(database));
                    sqlite3_finalize(statement);
                    sqlite3_reset(statement);
                }
            }
            sqlite3_close(database);
        }
        else
        {
            NSLog(@"Database: updateTableData: sqlite3_open. '%s'", sqlite3_errmsg(database));
        }
    }
    else
    {
        NSLog(@"Database: updateTableData: dbPath. '%s'", sqlite3_errmsg(database));
    }
}







- (void)updateTableDataInLaunchAPIDetails:(NSString *)localImageUrl forServerURL : (NSString *)marketImageURL
{
    sqlite3 *database;
    
    NSString *updateStmt = [NSString stringWithFormat:@"UPDATE %@ SET %@ = '%@' where market_image_server_url = '%@'",TABLE_MARKET_IMAGE_URLS,MARKET_IMAGE_LOCAL_URLS,localImageUrl,marketImageURL];
    
    NSString *dbPath = [self getDBPath];
    if ( dbPath != NULL )
    {
        if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK )
        {
            sqlite3_stmt *statement;
            if ( sqlite3_prepare_v2(database, [updateStmt UTF8String], -1, &statement, NULL) != SQLITE_OK )
            {
                NSLog(@"Database: updateTableData: sqlite3_prepare_v2. '%s'", sqlite3_errmsg(database));
                sqlite3_close(database);
            }
            else
            {
                if ( sqlite3_step(statement) == SQLITE_DONE)
                {
//                    NSLog(@"Database: updateTableData: Success");
                    sqlite3_finalize(statement);
                }
                else
                {
                    NSLog(@"Database: updateTableData: sqlite3_step. '%s'", sqlite3_errmsg(database));
                    sqlite3_finalize(statement);
                    sqlite3_reset(statement);
                }
            }
            sqlite3_close(database);
        }
        else
        {
            NSLog(@"Database: updateTableData: sqlite3_open. '%s'", sqlite3_errmsg(database));
        }
    }
    else
    {
        NSLog(@"Database: updateTableData: dbPath. '%s'", sqlite3_errmsg(database));
    }
}

//Update Status
- (void)updateTableDataInDropDownTimeStamp:(NSString *)dropDownType withStatus: (NSString *)status
{
    sqlite3 *database;

    NSString *updateStmt = [NSString stringWithFormat:@"UPDATE %@ SET %@ = '%@' where %@ = '%@'",TABLE_DROPDOWN_TIMESTAMP,DROPDOWN_TIMESTAMP_ISUPDATED,status,DROPDOWN_TIMESTAMP_DROPDOWN_NAME,dropDownType];
    
    NSString *dbPath = [self getDBPath];
    if ( dbPath != NULL )
    {
        if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK )
        {
            sqlite3_stmt *statement;
            if ( sqlite3_prepare_v2(database, [updateStmt UTF8String], -1, &statement, NULL) != SQLITE_OK )
            {
                NSLog(@"Database: updateTableData: sqlite3_prepare_v2. '%s'", sqlite3_errmsg(database));
                sqlite3_close(database);
            }
            else
            {
                if ( sqlite3_step(statement) == SQLITE_DONE)
                {
//                    NSLog(@"Database: updateTableData: Success");
                    sqlite3_finalize(statement);
                }
                else
                {
                    NSLog(@"Database: updateTableData: sqlite3_step. '%s'", sqlite3_errmsg(database));
                    sqlite3_finalize(statement);
                    sqlite3_reset(statement);
                }
            }
            sqlite3_close(database);
        }
        else
        {
            NSLog(@"Database: updateTableData: sqlite3_open. '%s'", sqlite3_errmsg(database));
        }
    }
    else
    {
        NSLog(@"Database: updateTableData: dbPath. '%s'", sqlite3_errmsg(database));
    }
}

#pragma mark Login Details Starts
/*
 Method used to get transaction columns
 */
-(NSArray *)getAllLoginColumns
{
    NSMutableArray *transHistories = [[NSMutableArray alloc] init];
    [transHistories addObject:LOGIN_USER_INFORMATION_COLOUMN_ID];
    [transHistories addObject:LOGIN_USER_FIRST_NAME];
    [transHistories addObject:LOGIN_USER_LAST_NAME];
    [transHistories addObject:LOGIN_USER_PHONE_NUMBER];
    [transHistories addObject:LOGIN_USER_UPDATED_TIME];
    [transHistories addObject:LOGIN_USER_PROFILE_ID];
    [transHistories addObject:LOGIN_USER_IS_ACTIVATED];
    [transHistories addObject:LOGIN_USER_IMAGE_PATH];
    
    return transHistories;
}

/*
 Method used to get all transaction details
 */
-(NSArray *)getAllLoginInformation
{
    NSMutableArray *transHistories;
    NSString *readStmt = [NSString stringWithFormat:@"SELECT * FROM %@",TABLE_LOGIN_USER_INFORMATION];
    
    NSArray *fieldsArray = [self getAllLoginColumns];
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    if ( dbPath!=NULL )
    {
        if ( sqlite3_open([dbPath UTF8String], &database)==SQLITE_OK )
        {
            sqlite3_stmt *statement;
            if ( sqlite3_prepare_v2(database, [readStmt UTF8String], -1, &statement, NULL)==SQLITE_OK )
            {
                transHistories = [[NSMutableArray alloc] init];
                while ( sqlite3_step(statement)==SQLITE_ROW )
                {
                    NSMutableDictionary *recordDictionary = [[NSMutableDictionary alloc] init];
                    // Read the data from the result row
                    for ( int i=0; i<[fieldsArray count]; i++ )
                    {
                        [recordDictionary setValue:[NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, i)] forKey:[fieldsArray objectAtIndex:i]];
                    }
                    [transHistories addObject:recordDictionary];
                }
                sqlite3_finalize(statement);
            }
            else
            {
                sqlite3_reset(statement);
                NSLog(@"Database: readTableRecord: Statement Not Prepared");
            }
            sqlite3_close(database);
        }
        else
            NSLog(@"Database: readTableRecord :Database Not Opened");
    }
    else
        NSLog(@"Database: readTableRecord :Database Not availbale");
    
    return transHistories;
}

- (NSError *)recordInsertionIntoLoginUserDetailsWithData:(NSArray *)dataArray
{
    NSError *returnError = nil;
    
    if ([self isInserts])
    {
        NSString *dbPath = [self getDBPath];
        sqlite3 *database;
        
        // Setup the SQL Statement and compile it for faster access
        sqlite3_stmt *statement;
        // Open the database
        if( sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK )
        {
            for (int i=0; i<[dataArray count]; i++)
            {
                NSMutableString *insertStatement = [NSMutableString stringWithFormat:@"INSERT INTO %@ (%@,%@,%@,%@,%@,%@,%@,%@) VALUES ('%@','%@','%@','%@','%@','%@','%@','%@')",TABLE_LOGIN_USER_INFORMATION,LOGIN_USER_INFORMATION_COLOUMN_ID,LOGIN_USER_FIRST_NAME,LOGIN_USER_LAST_NAME,LOGIN_USER_PHONE_NUMBER,LOGIN_USER_UPDATED_TIME,LOGIN_USER_PROFILE_ID,LOGIN_USER_IS_ACTIVATED,LOGIN_USER_IMAGE_PATH,[[dataArray objectAtIndex:i] valueForKey:LOGIN_USER_INFORMATION_COLOUMN_ID],[[dataArray objectAtIndex:i] valueForKey:LOGIN_USER_FIRST_NAME],[[dataArray objectAtIndex:i] valueForKey:LOGIN_USER_LAST_NAME],[[dataArray objectAtIndex:i] valueForKey:LOGIN_USER_PHONE_NUMBER],[[dataArray objectAtIndex:i] valueForKey:LOGIN_USER_UPDATED_TIME],[[dataArray objectAtIndex:i] valueForKey:LOGIN_USER_PROFILE_ID],[[dataArray objectAtIndex:i] valueForKey:LOGIN_USER_IS_ACTIVATED],[[dataArray objectAtIndex:i] valueForKey:LOGIN_USER_IMAGE_PATH]];
                
                if ( sqlite3_prepare_v2(database, [insertStatement UTF8String], -1, &statement, NULL)!=SQLITE_OK )
                {
                    NSLog(@"%s: error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_prepare_v2(database, [insertStatement UTF8String], -1, &statement, NULL));
                    returnError = [self recordInsertionErrorWithResult:0];
                }
                else
                {
                    if ( sqlite3_step(statement) == SQLITE_DONE )
                    {
                        sqlite3_finalize(statement);
                        NSLog(@"%d insertStatement: %@ ------------- SUCCESS",i+1,insertStatement);
                        returnError = [self recordInsertionErrorWithResult:1];
                    }
                    else
                    {
                        NSLog(@"%s: error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_step(statement));
                        returnError = [self recordInsertionErrorWithResult:0];
                    }
                }
            }
            sqlite3_close(database);
        }
        else
        {
            NSLog(@"%s: step error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_open([dbPath UTF8String], &database));
            returnError = [self recordInsertionErrorWithResult:0];
        }
    }
    else
        [self recordInsertionErrorWithResult:0];
    
    
    return returnError;
}

- (void)updateTableDataInLoginDetails :(NSString *) imagePath
{
    NSString *updateStmt = [NSString stringWithFormat:@"UPDATE %@ SET %@ = '%@'",TABLE_LOGIN_USER_INFORMATION,LOGIN_USER_IMAGE_PATH,imagePath];
    NSLog(@"imagePath: %@",imagePath);
    NSLog(@"updateStmt: %@",updateStmt);
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    if ( dbPath != NULL )
    {
        if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK )
        {
            sqlite3_stmt *statement;
            if ( sqlite3_prepare_v2(database, [updateStmt UTF8String], -1, &statement, NULL) != SQLITE_OK )
            {
                NSLog(@"Database: updateTableData: sqlite3_prepare_v2. '%s'", sqlite3_errmsg(database));
                sqlite3_close(database);
            }
            else
            {
                if ( sqlite3_step(statement) == SQLITE_DONE)
                {
//                    NSLog(@"Database: updateTableData: Success");
                    sqlite3_finalize(statement);
                }
                else
                {
                    NSLog(@"Database: updateTableData: sqlite3_step. '%s'", sqlite3_errmsg(database));
                    sqlite3_finalize(statement);
                    sqlite3_reset(statement);
                }
            }
            sqlite3_close(database);
        }
        else
        {
            NSLog(@"Database: updateTableData: sqlite3_open. '%s'", sqlite3_errmsg(database));
        }
    }
    else
    {
        NSLog(@"Database: updateTableData: dbPath. '%s'", sqlite3_errmsg(database));
    }
}

#pragma mark Login Details Ends

#pragma mark Drop Down Starts

/*
 Method used to get transaction columns
 */
-(NSArray *)getDropDownTimeStapColumns
{
    NSLog(@"getDropDownTimeStapColumns..");
    NSMutableArray *transHistories = [[NSMutableArray alloc] init];
    [transHistories addObject:DROPDOWN_TIMESTAMP_COLOUMN_ID];
    [transHistories addObject:DROPDOWN_TIMESTAMP_DROPDOWN_NAME];
    [transHistories addObject:DROPDOWN_TIMESTAMP_TIMESTAMP];
    [transHistories addObject:DROPDOWN_TIMESTAMP_ISUPDATED];
    return transHistories;
}

/*
 Method used to get all drop down details
 */
-(NSArray *)getAllDropDownTimeStapDetails
{
    NSLog(@"getAllDropDownTimeStapDetails..");
    NSMutableArray *transHistories = NULL;
    NSString *readStmt = [NSString stringWithFormat:@"SELECT * FROM %@",TABLE_DROPDOWN_TIMESTAMP];
    NSArray *fieldsArray = [self getDropDownTimeStapColumns];
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    NSLog(@"DB Path: %@", dbPath);
    if ( dbPath!=NULL )
    {
        if ( sqlite3_open([dbPath UTF8String], &database)==SQLITE_OK )
        {
            sqlite3_stmt *statement;
            if ( sqlite3_prepare_v2(database, [readStmt UTF8String], -1, &statement, NULL)==SQLITE_OK )
            {
                transHistories = [[NSMutableArray alloc] init];
                while ( sqlite3_step(statement)==SQLITE_ROW )
                {
                    NSMutableDictionary *recordDictionary = [[NSMutableDictionary alloc] init];
                    // Read the data from the result row
                    for ( int i=0; i<[fieldsArray count]; i++ )
                    {
                        [recordDictionary setValue:[NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, i)] forKey:[fieldsArray objectAtIndex:i]];
                    }
                    [transHistories addObject:recordDictionary];
                }
                sqlite3_finalize(statement);
            }
            else
            {
                sqlite3_reset(statement);
                NSLog(@"Database: readTableRecord: Statement Not Prepared");
            }
            sqlite3_close(database);
        }
        else
            NSLog(@"Database: readTableRecord :Database Not Opened");
    }
    else
        [self showAlert:@"Error" description:@"Database Not availbale"];

    NSLog(@"Transaction : %@",transHistories);

    return transHistories;
}

/*
 Method used to insert into drop down details
 */
- (NSError *)recordInsertionIntoDropDownTimeStampWithData:(NSArray *)dataArray
{
    NSError *returnError = nil;
    if ([self isInserts])
    {
        NSString *dbPath = [self getDBPath];
        sqlite3 *database;
        
        // Setup the SQL Statement and compile it for faster access
        sqlite3_stmt *statement;
        // Open the database
        if( sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK )
        {
            for (int i=0; i<[dataArray count]; i++)
            {
                NSMutableString *insertStatement = [NSMutableString stringWithFormat:@"INSERT INTO %@ (%@,%@,%@) VALUES ('%@','%@','%@')",TABLE_DROPDOWN_TIMESTAMP,DROPDOWN_TIMESTAMP_DROPDOWN_NAME,DROPDOWN_TIMESTAMP_TIMESTAMP,DROPDOWN_TIMESTAMP_ISUPDATED,[[dataArray objectAtIndex:i] valueForKey:DROPDOWN_TIMESTAMP_DROPDOWN_NAME],[[dataArray objectAtIndex:i] valueForKey:DROPDOWN_TIMESTAMP_TIMESTAMP],[[dataArray objectAtIndex:i] valueForKey:DROPDOWN_TIMESTAMP_ISUPDATED]];
                
                if ( sqlite3_prepare_v2(database, [insertStatement UTF8String], -1, &statement, NULL) != SQLITE_OK )
                {
                    returnError = [self recordInsertionErrorWithResult:0];
                }
                else
                {
                    if ( sqlite3_step(statement) == SQLITE_DONE )
                    {
                        sqlite3_finalize(statement);
                        returnError = [self recordInsertionErrorWithResult:1];
                    }
                    else
                    {
                        returnError = [self recordInsertionErrorWithResult:0];
                    }
                }
            }
            sqlite3_close(database);
        }
        else
            returnError = [self recordInsertionErrorWithResult:0];
    }
    else
    {
        returnError = [self recordInsertionErrorWithResult:0];
    }
    
    return returnError;
}

#pragma mark Drop Down Ends


#pragma mark Transaction History Starts
/*
 Method used to get transaction columns
 */
-(NSArray *)getAllTransactionColumns
{
    NSMutableArray *transHistories = [[NSMutableArray alloc] init];
    [transHistories addObject:TRANSACTION_DATA_COLOUMN_ID];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_TYPE];
    [transHistories addObject:TRANSACTION_DATA_TRANSACITON_AMOUNT];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_DATE];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_TIME];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_ID];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_STATUS];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_SENDER_PHONE_NUMBER];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_SENDER_FULL_NAME];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_SENDER_NICK_NAME];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_SENDER_FIRST_NAME];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_SENDER_LAST_NAME];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FULL_NAME];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FIRST_NAME];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_BENIFICARY_LAST_NAME];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_FEE];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_MESSAGE];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_SENDER_BUSINESS_NAME];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_BENIFICARY_BUSINESS_NAME];
    [transHistories addObject:TRANSACTION_DATA_PROCCESSOR_CODE];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_ADDRESS1];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_ADDRESS2];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_ADDRESS3];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_CITY];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_COUNTRY];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_PINCODE];
    [transHistories addObject:TRANSACTION_DATA_TRANSACTION_AGENT_BUSINESS_NAME];
    
    //Newly Added
    [transHistories addObject: TRANSACTION_DATA_TRANSACTION_OPERATOR_ID];
    [transHistories addObject: TRANSACTION_DATA_TRANSACTION_OPERATOR_NAME];
    [transHistories addObject: TRANSACTION_DATA_TRANSACTION_TRANSACTION_TYPE_ID];
    [transHistories addObject: TRANSACTION_DATA_TRANSACTION_TRANSACTION_TYPE_NAME];
    [transHistories addObject: TRANSACTION_DATA_TRANSACTION_DTH_TYPE_ID];
    [transHistories addObject: TRANSACTION_DATA_TRANSACTION_DTH_TYPE_NAME];
    [transHistories addObject: TRANSACTION_DATA_TRANSACTION_ACCOUNT_NUMBER];
    [transHistories addObject: TRANSACTION_DATA_BANK_IFSC_CODE];
    [transHistories addObject: TRANSACTION_DATA_BANK_NAME];
    [transHistories addObject: TRANSACTION_DATA_BANK_MMID];

    
    [transHistories addObject: BILLERNICKNAME];
    [transHistories addObject: BILLERNAME];
    [transHistories addObject: BILLERTYPE];
    [transHistories addObject: BILLERCATEGORY];
    [transHistories addObject: BILLERID];
    [transHistories addObject: BILLERDUEDATE];
    [transHistories addObject: BILLERSHORTNAME];
    [transHistories addObject: BILLERMASTERID];
    [transHistories addObject: BILLERMEMBERBILLERID];
    [transHistories addObject: BILLERLOCATION];
    [transHistories addObject: BILLERA1];
    [transHistories addObject: BILLERA2];
    [transHistories addObject: BILLERA3];
    [transHistories addObject: BILLERA4];
    [transHistories addObject: BILLERA5];
    [transHistories addObject: BILLERREFERENCE1];
    [transHistories addObject: BILLERREFERENCE2];
    [transHistories addObject: BILLERREFERENCE3];
    [transHistories addObject: BILLERREFERENCE4];
    [transHistories addObject: BILLERREFERENCE5];
    [transHistories addObject: BILLPAYCODE];
    [transHistories addObject: OPERATORID];
    [transHistories addObject: TRANSACTION_DATA_TRANSACTION_TOPUP_TYPE];
    [transHistories addObject: TRANSACTION_DATA_TRANSACTION_TOPUP_ID];
    
    return transHistories;
}

/*
 Method used to get all transaction details
 */
-(NSArray *)getAllTransactionHistory
{
    NSMutableArray *transHistories;
    NSString *readStmt = [NSString stringWithFormat:@"SELECT * FROM %@",TABLE_TRANSACTION_DATA];
    
    NSArray *fieldsArray = [self getAllTransactionColumns];
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    if ( dbPath!=NULL )
    {
        if ( sqlite3_open([dbPath UTF8String], &database)==SQLITE_OK )
        {
            sqlite3_stmt *statement;
            if ( sqlite3_prepare_v2(database, [readStmt UTF8String], -1, &statement, NULL)==SQLITE_OK )
            {
                transHistories = [[NSMutableArray alloc] init];
                while (sqlite3_step(statement)==SQLITE_ROW)
                {
                    NSMutableDictionary *recordDictionary = [[NSMutableDictionary alloc] init];
                    // Read the data from the result row
                    for ( int i=0; i<[fieldsArray count]; i++ )
                    {
                        [recordDictionary setValue:[NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, i)] forKey:[fieldsArray objectAtIndex:i]];
                    }
                    [transHistories addObject:recordDictionary];
                }
                sqlite3_finalize(statement);
            }
            else
            {
                sqlite3_reset(statement);
                NSLog(@"Database: readTableRecord: Statement Not Prepared");
            }
            sqlite3_close(database);
        }
        else
            NSLog(@"Database: readTableRecord :Database Not Opened");
    }
    else
        NSLog(@"Database: readTableRecord :Database Not availbale");
    
    return transHistories;
}

-(NSArray *)getAllTransactionHistoryWithTransactionCode:(NSString*)transactionType
{
    NSMutableArray *transHistories;
    NSString *readStmt = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ = '%@'",TABLE_TRANSACTION_DATA,TRANSACTION_DATA_TRANSACTION_TYPE,transactionType];
    
    NSArray *fieldsArray = [self getAllTransactionColumns];
    
    sqlite3 *database;
    
    NSString *dbPath = [self getDBPath];
    
    NSLog(@"DB Path : %@",dbPath);
    
    if ( dbPath!=NULL )
    {
        if ( sqlite3_open([dbPath UTF8String], &database)==SQLITE_OK )
        {
            sqlite3_stmt *statement;
            if ( sqlite3_prepare_v2(database, [readStmt UTF8String], -1, &statement, NULL)==SQLITE_OK )
            {
                transHistories = [[NSMutableArray alloc] init];
                while (sqlite3_step(statement)==SQLITE_ROW)
                {
                    NSMutableDictionary *recordDictionary = [[NSMutableDictionary alloc] init];
                    // Read the data from the result row
                    for ( int i=0; i<[fieldsArray count]; i++ )
                    {
                        [recordDictionary setValue:[NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, i)] forKey:[fieldsArray objectAtIndex:i]];
                    }
                    [transHistories addObject:recordDictionary];
                }
                sqlite3_finalize(statement);
            }
            else
            {
                sqlite3_reset(statement);
                NSLog(@"Database: readTableRecord: Statement Not Prepared");
            }
            sqlite3_close(database);
        }
        else
            NSLog(@"Database: readTableRecord :Database Not Opened");
    }
    else
        NSLog(@"Database: readTableRecord :Database Not availbale");
    
    return transHistories;
}


/*
 Method used to insert transaction details
 */
- (NSError *)recordInsertionIntoTransactionDetailsWithData:(NSArray *)dataArray
{
    NSError *returnError = nil;
    
    if ([self isInserts])
    {
        NSString *dbPath = [self getDBPath];
        sqlite3 *database;
        
        // Setup the SQL Statement and compile it for faster access
        sqlite3_stmt *statement;
        // Open the database
        if( sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK )
        {
            for (int i=0; i<[dataArray count]; i++)
            {
                
// define TRANSACTION_DATA_TRANSACTION_TOPUP_TYPE @"topupType"
// define TRANSACTION_DATA_TRANSACTION_TOPUP_ID @"topupId"
                
                NSMutableString *insertStatement = [NSMutableString stringWithFormat:@"INSERT INTO %@ (%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@) VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",TABLE_TRANSACTION_DATA,TRANSACTION_DATA_COLOUMN_ID,TRANSACTION_DATA_TRANSACTION_TYPE,TRANSACTION_DATA_TRANSACITON_AMOUNT,TRANSACTION_DATA_TRANSACTION_DATE,TRANSACTION_DATA_TRANSACTION_TIME,TRANSACTION_DATA_TRANSACTION_ID,TRANSACTION_DATA_TRANSACTION_STATUS,TRANSACTION_DATA_TRANSACTION_SENDER_PHONE_NUMBER,TRANSACTION_DATA_TRANSACTION_SENDER_FULL_NAME,TRANSACTION_DATA_TRANSACTION_SENDER_NICK_NAME,TRANSACTION_DATA_TRANSACTION_SENDER_FIRST_NAME,TRANSACTION_DATA_TRANSACTION_SENDER_LAST_NAME,TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER,TRANSACTION_DATA_TRANSACTION_BENIFICARY_FULL_NAME,TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME,TRANSACTION_DATA_TRANSACTION_BENIFICARY_FIRST_NAME,TRANSACTION_DATA_TRANSACTION_BENIFICARY_LAST_NAME,TRANSACTION_DATA_TRANSACTION_FEE,TRANSACTION_DATA_TRANSACTION_MESSAGE,TRANSACTION_DATA_TRANSACTION_SENDER_BUSINESS_NAME,TRANSACTION_DATA_TRANSACTION_BENIFICARY_BUSINESS_NAME,TRANSACTION_DATA_PROCCESSOR_CODE,TRANSACTION_DATA_TRANSACTION_ADDRESS1,TRANSACTION_DATA_TRANSACTION_ADDRESS2,TRANSACTION_DATA_TRANSACTION_ADDRESS3,TRANSACTION_DATA_TRANSACTION_CITY,TRANSACTION_DATA_TRANSACTION_COUNTRY,TRANSACTION_DATA_TRANSACTION_PINCODE,TRANSACTION_DATA_TRANSACTION_AGENT_BUSINESS_NAME,TRANSACTION_DATA_TRANSACTION_OPERATOR_ID,TRANSACTION_DATA_TRANSACTION_OPERATOR_NAME,TRANSACTION_DATA_TRANSACTION_TRANSACTION_TYPE_ID ,TRANSACTION_DATA_TRANSACTION_TRANSACTION_TYPE_NAME,TRANSACTION_DATA_TRANSACTION_DTH_TYPE_ID , TRANSACTION_DATA_TRANSACTION_DTH_TYPE_NAME,TRANSACTION_DATA_TRANSACTION_ACCOUNT_NUMBER ,TRANSACTION_DATA_BANK_IFSC_CODE,TRANSACTION_DATA_BANK_NAME,TRANSACTION_DATA_BANK_MMID,BILLERNICKNAME,BILLERNAME,BILLERTYPE,BILLERCATEGORY,BILLERID,BILLERDUEDATE,BILLERSHORTNAME,BILLERMASTERID,BILLERMEMBERBILLERID,BILLERLOCATION,BILLERA1,BILLERA2,BILLERA3,BILLERA4,BILLERA5,BILLERREFERENCE1,BILLERREFERENCE2,BILLERREFERENCE3,BILLERREFERENCE4,BILLERREFERENCE5 ,BILLPAYCODE,OPERATORID,TRANSACTION_DATA_TRANSACTION_TOPUP_TYPE,TRANSACTION_DATA_TRANSACTION_TOPUP_ID,[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_COLOUMN_ID],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_TYPE],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACITON_AMOUNT],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_DATE],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_TIME],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_ID],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_STATUS],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_SENDER_PHONE_NUMBER],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_SENDER_FULL_NAME],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_SENDER_NICK_NAME],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_SENDER_FIRST_NAME],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_SENDER_LAST_NAME],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FULL_NAME],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FIRST_NAME],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_LAST_NAME],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_FEE],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_MESSAGE],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_SENDER_BUSINESS_NAME],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_BUSINESS_NAME],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_PROCCESSOR_CODE],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_ADDRESS1],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_ADDRESS2],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_ADDRESS3],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_CITY],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_COUNTRY],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_PINCODE],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_AGENT_BUSINESS_NAME], [[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_OPERATOR_ID],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_OPERATOR_NAME],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_TRANSACTION_TYPE_ID],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_TRANSACTION_TYPE_NAME],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_DTH_TYPE_ID],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_DTH_TYPE_NAME],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_ACCOUNT_NUMBER],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_BANK_IFSC_CODE],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_BANK_NAME],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_BANK_MMID],[[dataArray objectAtIndex:i] valueForKey:BILLERNICKNAME],[[dataArray objectAtIndex:i] valueForKey:BILLERNAME],[[dataArray objectAtIndex:i] valueForKey:BILLERTYPE],[[dataArray objectAtIndex:i] valueForKey:BILLERCATEGORY],[[dataArray objectAtIndex:i] valueForKey:BILLERID],[[dataArray objectAtIndex:i] valueForKey:BILLERDUEDATE],[[dataArray objectAtIndex:i] valueForKey:BILLERSHORTNAME],[[dataArray objectAtIndex:i] valueForKey:BILLERMASTERID],[[dataArray objectAtIndex:i] valueForKey:BILLERMEMBERBILLERID],[[dataArray objectAtIndex:i] valueForKey:BILLERLOCATION],[[dataArray objectAtIndex:i] valueForKey:BILLERA1],[[dataArray objectAtIndex:i] valueForKey:BILLERA2],[[dataArray objectAtIndex:i] valueForKey:BILLERA3],[[dataArray objectAtIndex:i] valueForKey:BILLERA4],[[dataArray objectAtIndex:i] valueForKey:BILLERA5],[[dataArray objectAtIndex:i] valueForKey:BILLERREFERENCE1],[[dataArray objectAtIndex:i] valueForKey:BILLERREFERENCE2],[[dataArray objectAtIndex:i] valueForKey:BILLERREFERENCE3],[[dataArray objectAtIndex:i] valueForKey:BILLERREFERENCE4],[[dataArray objectAtIndex:i] valueForKey:BILLERREFERENCE5],[[dataArray objectAtIndex:i] valueForKey:BILLPAYCODE],[[dataArray objectAtIndex:i] valueForKey:OPERATORID],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_TOPUP_TYPE],[[dataArray objectAtIndex:i] valueForKey:TRANSACTION_DATA_TRANSACTION_TOPUP_ID]];

                NSLog(@"Insert State Ment : %@",insertStatement);
                
                if ( sqlite3_prepare_v2(database, [insertStatement UTF8String], -1, &statement, NULL)!=SQLITE_OK )
                {
                    NSLog(@"%s: error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_prepare_v2(database, [insertStatement UTF8String], -1, &statement, NULL));
                    returnError = [self recordInsertionErrorWithResult:0];
                }
                else
                {
                    if ( sqlite3_step(statement) == SQLITE_DONE )
                    {
                        sqlite3_finalize(statement);
                        returnError = [self recordInsertionErrorWithResult:1];
                    }
                    else
                    {
                        NSLog(@"%s: error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_step(statement));
                        returnError = [self recordInsertionErrorWithResult:0];
                    }
                }
            }
            sqlite3_close(database);
        }
        else
        {
            NSLog(@"%s: step error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_open([dbPath UTF8String], &database));
            returnError = [self recordInsertionErrorWithResult:0];
        }
    }
    else
        [self recordInsertionErrorWithResult:0];
    
    
    return returnError;
}
#pragma mark Transaction History Ends

#pragma mark Drop Down Details Starts
/*
 Method used to insert into drop down details
 */

/*
 Method used to get transaction columns
 */
-(NSArray *)getAllDropDownColumns
{
    NSMutableArray *transHistories = [[NSMutableArray alloc] init];
    [transHistories addObject:DROP_DOWN_COLOUMN_ID];
    [transHistories addObject:DROP_DOWN_TYPE];
    [transHistories addObject:DROP_DOWN_TYPE_NAME];
    [transHistories addObject:DROP_DOWN_TYPE_DESC];
    
    return transHistories;
}

/*
 Method used to get all transaction details
 */
-(NSArray *)getAllDropDownDetails
{
    NSMutableArray *transHistories;
    NSString *readStmt = [NSString stringWithFormat:@"SELECT * FROM %@",TABLE_DROP_DOWN];
    
    NSArray *fieldsArray = [self getAllDropDownColumns];
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    if ( dbPath!=NULL )
    {
        if ( sqlite3_open([dbPath UTF8String], &database)==SQLITE_OK )
        {
            sqlite3_stmt *statement;
            if ( sqlite3_prepare_v2(database, [readStmt UTF8String], -1, &statement, NULL)==SQLITE_OK )
            {
                transHistories = [[NSMutableArray alloc] init];
                while ( sqlite3_step(statement)==SQLITE_ROW )
                {
                    NSMutableDictionary *recordDictionary = [[NSMutableDictionary alloc] init];
                    // Read the data from the result row
                    for ( int i=0; i<[fieldsArray count]; i++ )
                    {
                        [recordDictionary setValue:[NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, i)] forKey:[fieldsArray objectAtIndex:i]];
                    }
                    [transHistories addObject:recordDictionary];
                }
                sqlite3_finalize(statement);
            }
            else
            {
                sqlite3_reset(statement);
                NSLog(@"Database: readTableRecord: Statement Not Prepared");
            }
            sqlite3_close(database);
        }
        else
            NSLog(@"Database: readTableRecord :Database Not Opened");
    }
    else
        NSLog(@"Database: readTableRecord :Database Not availbale");
    
    return transHistories;
}

- (NSArray *)getAllDropDownDetailsForKey:(NSString *)keyValue
{
    NSMutableArray *transHistories;
    NSString *readStmt = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ = '%@'",TABLE_DROP_DOWN,DROP_DOWN_TYPE,keyValue];
    
    NSLog(@"readStmt : %@",readStmt);
    
    NSLog(@"readStmt: %@",readStmt);
    
    NSArray *fieldsArray = [self getAllDropDownColumns];
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    if ( dbPath!=NULL )
    {
        if ( sqlite3_open([dbPath UTF8String], &database)==SQLITE_OK )
        {
            sqlite3_stmt *statement;
            if ( sqlite3_prepare_v2(database, [readStmt UTF8String], -1, &statement, NULL)==SQLITE_OK )
            {
                transHistories = [[NSMutableArray alloc] init];
                while ( sqlite3_step(statement)==SQLITE_ROW )
                {
                    NSMutableDictionary *recordDictionary = [[NSMutableDictionary alloc] init];
                    // Read the data from the result row
                    for ( int i=0; i<[fieldsArray count]; i++ )
                    {
                        [recordDictionary setValue:[NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, i)] forKey:[fieldsArray objectAtIndex:i]];
                    }
                    [transHistories addObject:recordDictionary];
                }
                sqlite3_finalize(statement);
            }
            else
            {
                NSLog(@"%s: error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_step(statement));
                sqlite3_reset(statement);
                NSLog(@"Database: readTableRecord: Statement Not Prepared");
            }
            sqlite3_close(database);
        }
        else
            NSLog(@"Database: readTableRecord :Database Not Opened");
    }
    else
        NSLog(@"Database: readTableRecord :Database Not availbale");
    
    return transHistories;
}

- (NSError *)recordInsertionIntoDropDownDetailsWithData:(NSArray *)dataArray
{
    sqlite3 *database;
    
    
    NSError *returnError = nil;
    if ([self isInserts])
    {
        NSString *dbPath = [self getDBPath];
        
        // Setup the SQL Statement and compile it for faster access
        sqlite3_stmt *statement;
        // Open the database
        if( sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK )
        {
            for (int i=0; i<[dataArray count]; i++)
            {
                NSMutableString *insertStatement = [NSMutableString stringWithFormat:@"INSERT INTO %@ (%@,%@,%@) VALUES ('%@','%@','%@')",TABLE_DROP_DOWN,DROP_DOWN_TYPE,DROP_DOWN_TYPE_NAME,DROP_DOWN_TYPE_DESC,[[dataArray objectAtIndex:i] valueForKey:DROP_DOWN_TYPE],[[dataArray objectAtIndex:i] valueForKey:DROP_DOWN_TYPE_NAME],[[dataArray objectAtIndex:i] valueForKey:DROP_DOWN_TYPE_DESC]];
                
                if ( sqlite3_prepare_v2(database, [insertStatement UTF8String], -1, &statement, NULL) != SQLITE_OK )
                {
                    returnError = [self recordInsertionErrorWithResult:0];
                }
                else
                {
                    if ( sqlite3_step(statement) == SQLITE_DONE )
                    {
                        sqlite3_finalize(statement);
                        returnError = [self recordInsertionErrorWithResult:1];
                    }
                    else
                    {
                        NSLog(@"%s: step error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_step(statement));
                        returnError = [self recordInsertionErrorWithResult:0];
                    }
                }
            }
            sqlite3_close(database);
        }
        else
            returnError = [self recordInsertionErrorWithResult:0];
    }
    else
    {
        returnError = [self recordInsertionErrorWithResult:0];
    }
    
    return returnError;
}
#pragma mark Drop Down Details Ends

#pragma mark Delete DB Starts

- (NSError *)deleteFirstRecordInsertionFromTransactionDetails
{
    NSError *returnError = nil;
    
    return returnError;
}

- (void)deleteAllDetailsFromLaunchAPIDetails
{
    [self deleteAllDetailsWithDeleteStatement:[NSString stringWithFormat:@"DELETE FROM %@",TABLE_MARKET_IMAGE_URLS]];
}

- (void)deleteAllDetailsFromTransactionHistory
{
    [self deleteAllDetailsWithDeleteStatement:[NSString stringWithFormat:@"DELETE FROM %@",TABLE_TRANSACTION_DATA]];
}

- (void)deleteAllDetailsFromMiniStatement
{
    [self deleteAllDetailsWithDeleteStatement:[NSString stringWithFormat:@"DELETE FROM %@",TABLE_MINI_STATEMENT]];
}

- (void)deleteAllPayBillsTransactionHistory
{
    [self deleteAllDetailsWithDeleteStatement:[NSString stringWithFormat:@"DELETE FROM %@",TABLE_PAYBILL_TRANSACTION]];
}

- (void)deleteAllDetailsPayBills
{
    [self deleteAllDetailsWithDeleteStatement:[NSString stringWithFormat:@"DELETE FROM %@",TABLE_PAY_BILL]];
}

- (void)deleteAllDetailsFromDropDownDetails
{
    [self deleteAllDetailsWithDeleteStatement:[NSString stringWithFormat:@"DELETE FROM %@",TABLE_DROP_DOWN]];
}

- (void)deleteAllDetailsFromDropDownTimeStampDetails
{
    [self deleteAllDetailsWithDeleteStatement:[NSString stringWithFormat:@"DELETE FROM %@",TABLE_DROPDOWN_TIMESTAMP]];
}

- (void)deleteAllDetailsFromLoginDetails
{
    [self deleteAllDetailsWithDeleteStatement:[NSString stringWithFormat:@"DELETE FROM %@",TABLE_LOGIN_USER_INFORMATION]];
}

-(void)deleteFromDropDownValueWithType :(NSString *)type
{
    NSString *deleteStmt = [NSString
                            stringWithFormat: @"delete from %@ where %@='%@'",TABLE_DROP_DOWN,DROP_DOWN_TYPE,type];
//    NSLog(@"Delete Statement : %@",deleteStmt);
    
    [self deleteAllDetailsWithDeleteStatement:deleteStmt];
}

- (void)deleteAllDetailsWithDeleteStatement:(NSString *)deleteStmt
{
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    if ( dbPath!=NULL )
    {
        if ( sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK )
        {
            sqlite3_stmt *statement;
            if ( sqlite3_prepare_v2(database, [deleteStmt UTF8String], -1, &statement, NULL) != SQLITE_OK )
            {
                NSLog(@"Database: deleteTableRecord: Error while creating delete statement. '%s'", sqlite3_errmsg(database));
            }
            else
            {
                if ( sqlite3_step(statement) == SQLITE_DONE )
                {
                    sqlite3_finalize(statement);
//                    NSLog(@"Delete Statement: %@ ------------- SUCCESS",deleteStmt);
                }
                else
                {
                    NSLog(@"%s: step error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_step(statement));
                }
            }
            sqlite3_close(database);
        }
        else
        {
            NSLog(@"Database: deleteTableRecord: Error while deleting. '%s'", sqlite3_errmsg(database));
        }
    }
    else
    {
        NSLog(@"Database: deleteTableRecord: Error while deleting. '%s'", sqlite3_errmsg(database));
    }
}

#pragma mark Delete DB Ends
// Definition: This function is used to delete the record from table of the database
- (void) deleteRecordFromTable :(NSString *) deleteStmt
{
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    if ( dbPath!=NULL )
    {
        if ( sqlite3_open([dbPath UTF8String], &database)==SQLITE_OK )
        {
            sqlite3_stmt *statement;
            if ( sqlite3_prepare_v2(database, [deleteStmt UTF8String], -1, &statement, NULL)!=SQLITE_OK )
            {
                NSLog(@"Database: deleteTableRecord: Error while creating delete statement. '%s'", sqlite3_errmsg(database));
                sqlite3_close(database);
                return ;
            }
            if ( SQLITE_DONE!=sqlite3_step(statement) )
            {
                NSLog(@"Database: deleteTableRecord: Error while deleting. '%s'", sqlite3_errmsg(database));
                sqlite3_finalize(statement);
            }
            else
            {
                sqlite3_finalize(statement);
            }
            sqlite3_close(database);
        }
        else
        {
            NSLog(@"Database: deleteTableRecord :Database Not Opened");
        }
    }
    else
    {
        [self showAlert:@"Error" description:@"Database Not availbale"];
    }
}

// Definition: This function is used to modify or update the record in the table of the database
- (void)updateTableData :(NSString *) updateStmt
{
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    if ( dbPath!=NULL )
    {
        if (sqlite3_open([dbPath UTF8String], &database)==SQLITE_OK )
        {
            sqlite3_stmt *statement;
            if ( sqlite3_prepare_v2(database, [updateStmt UTF8String], -1, &statement, NULL)!=SQLITE_OK )
            {
                NSLog(@"Database: updateTableData: Error while creating delete statement. '%s'", sqlite3_errmsg(database));
                sqlite3_close(database);
                return;
            }
            if ( SQLITE_DONE!=sqlite3_step(statement) )
            {
                [self showAlert:@"Error" description:@"Failed to update in DB"];
                NSLog(@"Database: updateTableData: Error while updating. '%s'", sqlite3_errmsg(database));
                sqlite3_finalize(statement);
                sqlite3_reset(statement);
            }
            else
            {
                [self showAlert:@"Success" description:@"Updated successfully"];
                sqlite3_finalize(statement);
            }
            sqlite3_close(database);
        }
        else
        {
            NSLog(@"Database: updateTableData :Database Not Opened");
        }
    }
    else
    {
        [self showAlert:@"Error" description:@"Database Not availbale"];
    }
}

// Definition: This function is used to show the alert when any error occurs.
- (void)showAlert:(NSString *)aTitle description:(NSString *)aDescription{
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:aTitle message:aDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [alert show];
    
    
    UIAlertController *alert= [UIAlertController alertControllerWithTitle:aTitle message:aDescription preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        
    UIViewController *topController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
    while (topController.presentedViewController)
    {
        topController = topController.presentedViewController;
    }
    [topController presentViewController:alert animated:NO completion:nil];

    
}




// Definition: This function is used to delete the record from table of the database
- (void) deleteRecordsFromTable :(NSString *) tableName
{
    NSString *deleteStmt = [NSString stringWithFormat:@"DELETE from %@",tableName];
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    if ( dbPath!=NULL )
    {
        if ( sqlite3_open([dbPath UTF8String], &database)==SQLITE_OK )
        {
            if(sqlite3_exec(database, [deleteStmt UTF8String],NULL, NULL, NULL) == SQLITE_OK)
            {
                NSLog(@"Delete Query Success");
            }
            else
            {
                NSLog(@"Delete Query Failed");
            }
            sqlite3_close(database);
            
        }
        else
        {
            NSLog(@"Database: deleteTableRecord :Database Not Opened");
        }
        sqlite3_close(database);
    }
    else
    {
        [self showAlert:@"Error" description:@"Database Not availbale"];
    }
}



#pragma mark Mini Statement Starts
/*
 Method used to get transaction columns
 */
-(NSArray *)getAllMiniStatementColumns
{
    NSMutableArray *transHistories = [[NSMutableArray alloc] init];
    [transHistories addObject:MINI_STATEMENT_COLOUMN_ID];
    [transHistories addObject:MINI_STATEMENT_TRANSACTION_TYPE];
    [transHistories addObject:MINI_STATEMENT_PROCESSOR_CODE];
    [transHistories addObject:MINI_STATEMENT_TRANSACTION_AMOUNT];
    [transHistories addObject:MINI_STATEMENT_TRANSACTION_MESSAGE];
    [transHistories addObject:MINI_STATEMENT_TRANSACTION_ID];
    [transHistories addObject:MINI_STATEMENT_TRANSACTION_DATE];
    [transHistories addObject:MINI_STATEMENT_TRANSACTION_TIME];
    [transHistories addObject:MINI_STATEMENT_TRANSACTION_STATUS];
    [transHistories addObject:MINI_STATEMENT_TRANSACTION_FEE];
    [transHistories addObject:MINI_STATEMENT_DYNAMIC_LABEL1];
    [transHistories addObject:MINI_STATEMENT_DYNAMIC_VALUE1];
    [transHistories addObject:MINI_STATEMENT_DYNAMIC_LABEL2];
    [transHistories addObject:MINI_STATEMENT_DYNAMIC_VALUE2];
    [transHistories addObject:MINI_STATEMENT_DYNAMIC_LABEL3];
    [transHistories addObject:MINI_STATEMENT_DYNAMIC_VALUE3];
    [transHistories addObject:MINI_STATEMENT_DYNAMIC_LABEL4];
    [transHistories addObject:MINI_STATEMENT_DYNAMIC_VALUE4];
    
    return transHistories;
}

/*
 Method used to get all MiniStatement details
 */
-(NSArray *)getAllMiniStatementDetails
{
    NSMutableArray *transHistories;
    NSString *readStmt = [NSString stringWithFormat:@"SELECT * FROM %@",TABLE_MINI_STATEMENT];
    
    NSArray *fieldsArray = [self getAllMiniStatementColumns];
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    if ( dbPath!=NULL )
    {
        if ( sqlite3_open([dbPath UTF8String], &database)==SQLITE_OK )
        {
            sqlite3_stmt *statement;
            if ( sqlite3_prepare_v2(database, [readStmt UTF8String], -1, &statement, NULL)==SQLITE_OK )
            {
                transHistories = [[NSMutableArray alloc] init];
                while ( sqlite3_step(statement)==SQLITE_ROW )
                {
                    NSMutableDictionary *recordDictionary = [[NSMutableDictionary alloc] init];
                    // Read the data from the result row
                    for ( int i=0; i<[fieldsArray count]; i++ )
                    {
                        [recordDictionary setValue:[NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, i)] forKey:[fieldsArray objectAtIndex:i]];
                    }
                    [transHistories addObject:recordDictionary];
                }
                sqlite3_finalize(statement);
            }
            else
            {
                sqlite3_reset(statement);
                NSLog(@"Database: readTableRecord: Statement Not Prepared");
            }
            sqlite3_close(database);
        }
        else
            NSLog(@"Database: readTableRecord :Database Not Opened");
    }
    else
        NSLog(@"Database: readTableRecord :Database Not availbale");
    
    return transHistories;
}

/*
 Method used to insert transaction details
 */
- (NSError *)recordInsertionIntoMiniStatementDetailsWithData:(NSArray *)dataArray
{
    NSError *returnError = nil;
    
    if ([self isInserts])
    {
        NSString *dbPath = [self getDBPath];
        sqlite3 *database;
        
        // Setup the SQL Statement and compile it for faster access
        sqlite3_stmt *statement;
        // Open the database
        if( sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK )
        {
            for (int i=0; i<[dataArray count]; i++)
            {
                NSLog(@"MINI_STATEMENT_COLOUMN_ID: %@",[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_COLOUMN_ID]);
                
                NSMutableString *insertStatement = [NSMutableString stringWithFormat:@"INSERT INTO %@ (%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@) VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",TABLE_MINI_STATEMENT, MINI_STATEMENT_TRANSACTION_TYPE,MINI_STATEMENT_PROCESSOR_CODE,MINI_STATEMENT_TRANSACTION_AMOUNT,MINI_STATEMENT_TRANSACTION_MESSAGE,MINI_STATEMENT_TRANSACTION_ID,MINI_STATEMENT_TRANSACTION_DATE,MINI_STATEMENT_TRANSACTION_TIME,MINI_STATEMENT_TRANSACTION_STATUS,MINI_STATEMENT_TRANSACTION_FEE,MINI_STATEMENT_DYNAMIC_LABEL1,MINI_STATEMENT_DYNAMIC_VALUE1,MINI_STATEMENT_DYNAMIC_LABEL2,MINI_STATEMENT_DYNAMIC_VALUE2,MINI_STATEMENT_DYNAMIC_LABEL3,MINI_STATEMENT_DYNAMIC_VALUE3,MINI_STATEMENT_DYNAMIC_LABEL4,MINI_STATEMENT_DYNAMIC_VALUE4,[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_TRANSACTION_TYPE],[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_PROCESSOR_CODE],[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_TRANSACTION_AMOUNT],[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_TRANSACTION_MESSAGE],[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_TRANSACTION_ID],[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_TRANSACTION_DATE],[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_TRANSACTION_TIME],[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_TRANSACTION_STATUS],[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_TRANSACTION_FEE],[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_DYNAMIC_LABEL1],[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_DYNAMIC_VALUE1],[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_DYNAMIC_LABEL2],[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_DYNAMIC_VALUE2],[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_DYNAMIC_LABEL3],[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_DYNAMIC_VALUE3],[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_DYNAMIC_LABEL4],[[dataArray objectAtIndex:i] valueForKey:MINI_STATEMENT_DYNAMIC_VALUE4]];
                
                if ( sqlite3_prepare_v2(database, [insertStatement UTF8String], -1, &statement, NULL)!=SQLITE_OK )
                {
                    NSLog(@"%s: error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_prepare_v2(database, [insertStatement UTF8String], -1, &statement, NULL));
                    returnError = [self recordInsertionErrorWithResult:0];
                }
                else
                {
                    if ( sqlite3_step(statement) == SQLITE_DONE )
                    {
                        sqlite3_finalize(statement);
                        returnError = [self recordInsertionErrorWithResult:1];
                    }
                    else
                    {
                        NSLog(@"%s: error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_step(statement));
                        returnError = [self recordInsertionErrorWithResult:0];
                    }
                }
            }
            sqlite3_close(database);
        }
        else
        {
            NSLog(@"%s: step error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_open([dbPath UTF8String], &database));
            returnError = [self recordInsertionErrorWithResult:0];
        }
    }
    else
        [self recordInsertionErrorWithResult:0];
    
    
    return returnError;
}
#pragma mark Mini Statement Ends



#pragma mark PayBillTransaction Starts
/*
 Method used to PayBillTransaction columns
 */

-(NSArray *)getAllPayBillTransactionColumns
{
    NSMutableArray *transHistories = [[NSMutableArray alloc] init];
    [transHistories addObject:PAYBILL_TRANSACTION_COLOUMN_ID];
    [transHistories addObject:PAYBILL_TRANSACTION_TYPE];
    [transHistories addObject:PAYBILL_TRANSACTION_PROCESSOR_CODE];
    [transHistories addObject:PAYBILL_TRANSACTION_AMOUNT];
    [transHistories addObject:PAYBILL_TRANSACTION_ID];
    [transHistories addObject:PAYBILL_TRANSACTION_DATE];
    [transHistories addObject:PAYBILL_TRANSACTION_TIME];
    [transHistories addObject:PAYBILL_TRANSACTION_STATUS];
    [transHistories addObject:PAYBILL_TRANSACTION_FEE];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERNICKNAME];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERNAME];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERTYPE];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERCATEGORY];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERID];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERDUEDATE];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERSHORTNAME];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERMASTERID];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERMEMBERBILLERID];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERLOCATION];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERA1];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERA2];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERA3];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERA4];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERA5];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERREFERENCE1];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERREFERENCE2];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERREFERENCE3];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERREFERENCE4];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLERREFERENCE5];
    [transHistories addObject:PAYBILL_TRANSACTION_BILLPAYCODE];
    
    return transHistories;
}

/*
 Method used to getAllPayBillTransactionDetails
 */
-(NSArray *)getAllPayBillTransactionDetails
{
    NSMutableArray *transHistories;
    NSString *readStmt = [NSString stringWithFormat:@"SELECT * FROM %@",TABLE_PAYBILL_TRANSACTION];
    
    NSArray *fieldsArray = [self getAllPayBillTransactionColumns];
    
    sqlite3 *database;
    NSString *dbPath = [self getDBPath];
    if ( dbPath!=NULL )
    {
        if ( sqlite3_open([dbPath UTF8String], &database)==SQLITE_OK )
        {
            sqlite3_stmt *statement;
            if ( sqlite3_prepare_v2(database, [readStmt UTF8String], -1, &statement, NULL)==SQLITE_OK )
            {
                transHistories = [[NSMutableArray alloc] init];
                while ( sqlite3_step(statement)==SQLITE_ROW )
                {
                    NSMutableDictionary *recordDictionary = [[NSMutableDictionary alloc] init];
                    // Read the data from the result row
                    for ( int i=0; i<[fieldsArray count]; i++ )
                    {
                        [recordDictionary setValue:[NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, i)] forKey:[fieldsArray objectAtIndex:i]];
                    }
                    [transHistories addObject:recordDictionary];
                }
                sqlite3_finalize(statement);
            }
            else
            {
                sqlite3_reset(statement);
                NSLog(@"Database: readTableRecord: Statement Not Prepared");
            }
            sqlite3_close(database);
        }
        else
            NSLog(@"Database: readTableRecord :Database Not Opened");
    }
    else
        NSLog(@"Database: readTableRecord :Database Not availbale");
    
    return transHistories;
}

/*
 Method used to PayBillTransaction details
 */
- (NSError *)recordInsertionIntoPayBillTransactionDetailsWithData:(NSArray *)dataArray
{
    NSError *returnError = nil;
    
    if ([self isInserts])
    {
        NSString *dbPath = [self getDBPath];
        sqlite3 *database;
        
        // Setup the SQL Statement and compile it for faster access
        sqlite3_stmt *statement;
        // Open the database
        if( sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK )
        {
            for (int i=0; i<[dataArray count]; i++)
            {
                NSMutableString *insertStatement = [NSMutableString stringWithFormat:@"INSERT INTO %@ (%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@) VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",TABLE_PAYBILL_TRANSACTION,PAYBILL_TRANSACTION_COLOUMN_ID,PAYBILL_TRANSACTION_TYPE,PAYBILL_TRANSACTION_PROCESSOR_CODE,PAYBILL_TRANSACTION_AMOUNT,PAYBILL_TRANSACTION_ID,PAYBILL_TRANSACTION_DATE,PAYBILL_TRANSACTION_TIME,PAYBILL_TRANSACTION_STATUS,PAYBILL_TRANSACTION_FEE,PAYBILL_TRANSACTION_BILLERNICKNAME,PAYBILL_TRANSACTION_BILLERNAME,PAYBILL_TRANSACTION_BILLERTYPE,PAYBILL_TRANSACTION_BILLERCATEGORY,PAYBILL_TRANSACTION_BILLERID,PAYBILL_TRANSACTION_BILLERDUEDATE,PAYBILL_TRANSACTION_BILLERSHORTNAME,PAYBILL_TRANSACTION_BILLERMASTERID,PAYBILL_TRANSACTION_BILLERMEMBERBILLERID,PAYBILL_TRANSACTION_BILLERLOCATION,PAYBILL_TRANSACTION_BILLERA1,PAYBILL_TRANSACTION_BILLERA2,PAYBILL_TRANSACTION_BILLERA3,PAYBILL_TRANSACTION_BILLERA4,PAYBILL_TRANSACTION_BILLERA5,PAYBILL_TRANSACTION_BILLERREFERENCE1,PAYBILL_TRANSACTION_BILLERREFERENCE2,PAYBILL_TRANSACTION_BILLERREFERENCE3,PAYBILL_TRANSACTION_BILLERREFERENCE4,PAYBILL_TRANSACTION_BILLERREFERENCE5,PAYBILL_TRANSACTION_BILLPAYCODE,[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_COLOUMN_ID],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_TYPE],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_PROCESSOR_CODE],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_AMOUNT],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_ID],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_DATE],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_TIME],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_STATUS],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_FEE],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERNICKNAME],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERNAME],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERTYPE],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERCATEGORY],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERID],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERDUEDATE],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERSHORTNAME],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERMASTERID],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERMEMBERBILLERID],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERLOCATION],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERA1],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERA2],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERA3],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERA4],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERA5],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERREFERENCE1],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERREFERENCE2],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERREFERENCE3],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERREFERENCE4],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLERREFERENCE5],[[dataArray objectAtIndex:i] valueForKey:PAYBILL_TRANSACTION_BILLPAYCODE]];
                
                if ( sqlite3_prepare_v2(database, [insertStatement UTF8String], -1, &statement, NULL)!=SQLITE_OK )
                {
                    NSLog(@"%s: error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_prepare_v2(database, [insertStatement UTF8String], -1, &statement, NULL));
                    returnError = [self recordInsertionErrorWithResult:0];
                }
                else
                {
                    if ( sqlite3_step(statement) == SQLITE_DONE )
                    {
                        sqlite3_finalize(statement);
                        returnError = [self recordInsertionErrorWithResult:1];
                    }
                    else
                    {
                        NSLog(@"%s: error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_step(statement));
                        returnError = [self recordInsertionErrorWithResult:0];
                    }
                }
            }
            sqlite3_close(database);
        }
        else
        {
            NSLog(@"%s: step error: %s (%d)", __FUNCTION__, sqlite3_errmsg(database), sqlite3_open([dbPath UTF8String], &database));
            returnError = [self recordInsertionErrorWithResult:0];
        }
    }
    else
        [self recordInsertionErrorWithResult:0];
    
    
    return returnError;
}
#pragma mark PayBillTransaction Ends

@end
