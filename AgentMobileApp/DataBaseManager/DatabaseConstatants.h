//
//  DatabaseConstatants.h
//  Consumer Client
//
//  Created by android on 6/9/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DatabaseConstatants : NSObject

#define DATABASE_NAME @"obopay.sqlite"
#define  DATABASE_VERSION 12

// START -- Transaction History and Mini Statement

// Columns used in Mini Statement and Transaction History table
#define TABLE_TRANSACTION_DATA @"trans_history"
#define TRANSACTION_DATA_COLOUMN_ID @"_id"
#define TRANSACTION_DATA_TRANSACTION_TYPE @"transaction_type"
#define TRANSACTION_DATA_TRANSACITON_AMOUNT @"transaction_amount"
#define TRANSACTION_DATA_TRANSACTION_DATE @"transaction_date"
#define TRANSACTION_DATA_TRANSACTION_TIME @"transacion_time"
#define TRANSACTION_DATA_TRANSACTION_ID @"transaction_id"
#define TRANSACTION_DATA_TRANSACTION_STATUS @"transaction_status"
#define TRANSACTION_DATA_TRANSACTION_SENDER_PHONE_NUMBER @"sender_phone_number"
#define TRANSACTION_DATA_TRANSACTION_SENDER_FULL_NAME @"sender_full_name"
#define TRANSACTION_DATA_TRANSACTION_SENDER_NICK_NAME @"sender_nick_name"
#define TRANSACTION_DATA_TRANSACTION_SENDER_FIRST_NAME @"sender_first_name"
#define TRANSACTION_DATA_TRANSACTION_SENDER_LAST_NAME @"sender_last_name"
#define TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER @"benificary_phone_number"
#define TRANSACTION_DATA_TRANSACTION_BENIFICARY_FULL_NAME @"benificary_full_name"
#define TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME @"benificary_nick_name"
#define TRANSACTION_DATA_TRANSACTION_BENIFICARY_FIRST_NAME @"benificary_first_name"
#define TRANSACTION_DATA_TRANSACTION_BENIFICARY_LAST_NAME @"benificary_last_name"
#define TRANSACTION_DATA_TRANSACTION_MESSAGE @"message"
#define TRANSACTION_DATA_TRANSACTION_FEE @"fee"
#define TRANSACTION_DATA_PROCCESSOR_CODE @"proccessor_code"
#define TRANSACTION_DATA_TRANSACTION_CODE @"transaction_code"
#define TRANSACTION_DATA_TRANSACTION_SENDER_BUSINESS_NAME @"sender_business_name"
#define TRANSACTION_DATA_TRANSACTION_AGENT_BUSINESS_NAME @"agent_business_name"
#define TRANSACTION_DATA_TRANSACTION_BENIFICARY_BUSINESS_NAME @"benficiary_business_name"
#define TRANSACTION_DATA_TRANSACTION_ADDRESS1 @"address_1"
#define TRANSACTION_DATA_TRANSACTION_ADDRESS2 @"address_2"
#define TRANSACTION_DATA_TRANSACTION_ADDRESS3 @"address_3"
#define TRANSACTION_DATA_TRANSACTION_CITY @"city"
#define TRANSACTION_DATA_TRANSACTION_PINCODE @"pinCode"
#define TRANSACTION_DATA_TRANSACTION_COUNTRY @"country"

//T3
#define TRANSACTION_DATA_AGENT_ID @"transaction_agentId"
#define TRANSACTION_DATA_AGENT_NAME @"transaction_agentName"
#define TRANSACTION_DATA_REQUEST_ID @"transaction_reqId"
#define TRANSACTION_DATA_AMOUNT_T3 @"amount"

#define TRANSACTION_DATA_BANK_CODE @"bank_code"
#define TRANSACTION_DATA_BANK_NICK_NAME @"bank_nickname"
#define TRANSACTION_DATA_BANK_ACCOUNT_NUMBER @"bank_accno"
#define TRANSACTION_DATA_BANK_NAME @"bank_name"


#define MINISTATEMENT_TRANSACTION_COUNT @"transaction_count"
// Newly Added

#define TRANSACTION_DATA_TRANSACTION_ACCOUNT_NUMBER @"account_number"
#define TRANSACTION_DATA_BANK_IFSC_CODE @"ifsc_code"
#define TRANSACTION_DATA_BANK_NAME @"bank_name"
#define TRANSACTION_DATA_BANK_MMID @"bank_mmid"
#define TRANSACTION_DATA_BANK @"Bank"

#define TRANSACTION_DATA_TRANSACTION_OPERATOR_ID @"operator_id"
#define TRANSACTION_DATA_TRANSACTION_OPERATOR_NAME @"operator_name"
#define TRANSACTION_DATA_TRANSACTION_TRANSACTION_TYPE_ID @"transaction_type_id"
#define TRANSACTION_DATA_TRANSACTION_TRANSACTION_TYPE_NAME @"transaction_type_name"
#define TRANSACTION_DATA_TRANSACTION_DTH_TYPE_ID @"dth_type_id"
#define TRANSACTION_DATA_TRANSACTION_DTH_TYPE_NAME @"dth_type_name"
#define TRANSACTION_DATA_TRANSACTION_CASHOUT_BANK_NAME @"cashoutBankName"
#define TRANSACTION_DATA_TRANSACTION_CASHOUT_BANK_DESC @"cashoutBankDesc"

#define TRANSACTION_DATA_TRANSACTION_TOPUP_TYPE @"topupType"
#define TRANSACTION_DATA_TRANSACTION_TOPUP_ID @"topupId"

#define MERCHANT_ID @"merchantId"

#define BILLERNICKNAME @"billerNickName"
#define BILLERNAME @"billerName"
#define BILLERTYPE @"billerType"
#define BILLERCATEGORY @"billerCategory"
#define BILLERID @"billerId"
#define BILLERDUEDATE @"billerDueDate"
#define BILLERSHORTNAME @"billerShortName"
#define BILLERMASTERID @"billerMasterId"
#define BILLERMEMBERBILLERID @"billerMemberBillerId"
#define BILLERLOCATION @"billerLocation"
#define BILLERA1 @"billerA1"
#define BILLERA2 @"billerA2"
#define BILLERA3 @"billerA3"
#define BILLERA4 @"billerA4"
#define BILLERA5 @"billerA5"
#define BILLERREFERENCE1 @"billerReference1"
#define BILLERREFERENCE2 @"billerReference2"
#define BILLERREFERENCE3 @"billerReference3"
#define BILLERREFERENCE4 @"billerReference4"
#define BILLERREFERENCE5 @"billerReference5"
#define BILLPAYCODE @"billPayCode"
#define OPERATORID @"OperatorId"


// END -- Transaction History and Mini Statement

// START -- Pay Bill
// Columns used in Pay Bill table
#define TABLE_PAY_BILL @"pay_bill"
#define PAY_BILL_COLOUMN_ID @"_id"
#define PAY_BILL_BILLER_NAME @"biller_name"
#define PAY_BILL_CATEGORY @"category"
#define PAY_BILL_REGION @"region"
#define PAY_BILL_INFO1 @"info1"
#define PAY_BILL_INFO2 @"info2"
#define PAY_BILL_INFO3 @"info3"
#define PAY_BILL_INFO4 @"info4"
#define PAY_BILL_INFO5 @"info5"
#define PAY_BILL_NICK_NAME @"nick_name"
// END -- Pay Bill

// START -- payee details
#define TABLE_PAYEE_DETAILS @"payee_details"
#define PAYEE_DETAILS_COLOUMN_ID @"_id"
#define PAYEE_DETAILS_FIRST_NAME @"biller_name"
#define PAYEE_DETAILS_LAST_NAME @"category"
#define PAYEE_DETAILS_MOBILE_NUMBER @"region"
#define PAYEE_DETAILS_NICK_NAME @"info1"
// END -- payee details

// Columns used in drop down table
#define TABLE_DROP_DOWN @"drop_down"
#define DROP_DOWN_COLOUMN_ID @"_id"
#define DROP_DOWN_TYPE @"drop_down_type"
#define DROP_DOWN_TYPE_NAME @"typeName"
#define DROP_DOWN_TYPE_DESC @"typeDesc"
// end drop down

// START - MARKET IMAGE URL TABLE
#define TABLE_MARKET_IMAGE_URLS @"market_images_url"
#define MARKET_IMAGE_COLOUMN_ID @"_id"
#define MARKET_IMAGE_SERVER_URLS @"market_image_server_url"
#define MARKET_IMAGE_LOCAL_URLS @"market_image_local_url"
// END - MARKET IMAGE URL TABLE

// START - DROP DOWN TIME STAMP TABLE
#define TABLE_DROPDOWN_TIMESTAMP @"dropdown_timestamp"
#define DROPDOWN_TIMESTAMP_COLOUMN_ID @"_id"
#define DROPDOWN_TIMESTAMP_DROPDOWN_NAME @"dropdown_name"
#define DROPDOWN_TIMESTAMP_TIMESTAMP @"time_stamp"
#define DROPDOWN_TIMESTAMP_ISUPDATED @"is_updated"
// END - DROP DOWN TIME STAMP TABLE

// Columns used in LOGIN USER INFORMATION TABLE
#define TABLE_LOGIN_USER_INFORMATION @"login_user_information"
#define LOGIN_USER_INFORMATION_COLOUMN_ID @"_id"
#define LOGIN_USER_FIRST_NAME @"first_name"
#define LOGIN_USER_LAST_NAME @"last_name"
#define LOGIN_USER_PHONE_NUMBER @"phone_number"
#define LOGIN_USER_UPDATED_TIME @"updated_time"
#define LOGIN_USER_PROFILE_ID @"profile_id"
#define LOGIN_USER_IS_ACTIVATED @"is_activated"
#define LOGIN_USER_IMAGE_PATH @"image_path"
// END -- LOGIN USER INFORMATION TABLE

// Columns used in MINI STATEMENT INFORMATION TABLE
#define TABLE_MINI_STATEMENT @"mini_statement"
#define MINI_STATEMENT_COLOUMN_ID @"_id"
#define MINI_STATEMENT_TRANSACTION_TYPE @"transaction_type"
#define MINI_STATEMENT_PROCESSOR_CODE @"processor_code"
#define MINI_STATEMENT_TRANSACTION_AMOUNT @"transaction_amount"
#define MINI_STATEMENT_TRANSACTION_MESSAGE @"message"
#define MINI_STATEMENT_TRANSACTION_ID @"trans_id"
#define MINI_STATEMENT_TRANSACTION_DATE @"transaction_date"
#define MINI_STATEMENT_TRANSACTION_TIME @"transacion_time"
#define MINI_STATEMENT_TRANSACTION_STATUS @"transaction_status"
#define MINI_STATEMENT_TRANSACTION_FEE @"transaction_fee"
#define MINI_STATEMENT_STATIC_TEXT  @"static"
#define MINI_STATEMENT_DYNAMIC_LABEL1 @"dynamicLabel1"
#define MINI_STATEMENT_DYNAMIC_VALUE1 @"dynamicValue1"
#define MINI_STATEMENT_DYNAMIC_LABEL2 @"dynamicLabel2"
#define MINI_STATEMENT_DYNAMIC_VALUE2 @"dynamicValue2"
#define MINI_STATEMENT_DYNAMIC_LABEL3 @"dynamicLabel3"
#define MINI_STATEMENT_DYNAMIC_VALUE3 @"dynamicValue3"
#define MINI_STATEMENT_DYNAMIC_LABEL4 @"dynamicLabel4"
#define MINI_STATEMENT_DYNAMIC_VALUE4 @"dynamicValue4"
// END -- LOGIN USER INFORMATION TABLE


// Columns used in PAYBILL TRANSACTION INFORMATION TABLE
#define TABLE_PAYBILL_TRANSACTION @"payBill_transHistory"
#define PAYBILL_TRANSACTION_COLOUMN_ID @"_id"
#define PAYBILL_TRANSACTION_TYPE @"transaction_type"
#define PAYBILL_TRANSACTION_PROCESSOR_CODE @"processor_code"
#define PAYBILL_TRANSACTION_AMOUNT @"transaction_amount"
#define PAYBILL_TRANSACTION_ID @"trans_id"
#define PAYBILL_TRANSACTION_DATE @"transaction_date"
#define PAYBILL_TRANSACTION_TIME @"transacion_time"
#define PAYBILL_TRANSACTION_STATUS @"transaction_status"
#define PAYBILL_TRANSACTION_FEE @"transaction_fee"
#define PAYBILL_TRANSACTION_BILLERNICKNAME @"billerNickName"
#define PAYBILL_TRANSACTION_BILLERNAME @"billerName"
#define PAYBILL_TRANSACTION_BILLERTYPE @"billerType"
#define PAYBILL_TRANSACTION_BILLERCATEGORY @"billerCategory"
#define PAYBILL_TRANSACTION_BILLERID @"billerId"
#define PAYBILL_TRANSACTION_BILLERDUEDATE @"billerDueDate"
#define PAYBILL_TRANSACTION_BILLERSHORTNAME @"billerShortName"
#define PAYBILL_TRANSACTION_BILLERMASTERID @"billerMasterId"
#define PAYBILL_TRANSACTION_BILLERMEMBERBILLERID @"billerMemberBillerId"
#define PAYBILL_TRANSACTION_BILLERLOCATION @"billerLocation"
#define PAYBILL_TRANSACTION_BILLERA1 @"billerA1"
#define PAYBILL_TRANSACTION_BILLERA2 @"billerA2"
#define PAYBILL_TRANSACTION_BILLERA3 @"billerA3"
#define PAYBILL_TRANSACTION_BILLERA4 @"billerA4"
#define PAYBILL_TRANSACTION_BILLERA5 @"billerA5"
#define PAYBILL_TRANSACTION_BILLERREFERENCE1 @"billerReference1"
#define PAYBILL_TRANSACTION_BILLERREFERENCE2 @"billerReference2"
#define PAYBILL_TRANSACTION_BILLERREFERENCE3 @"billerReference3"
#define PAYBILL_TRANSACTION_BILLERREFERENCE4 @"billerReference4"
#define PAYBILL_TRANSACTION_BILLERREFERENCE5  @"billerReference5"
#define PAYBILL_TRANSACTION_BILLPAYCODE @"billPayCode"

// END -- LOGIN USER INFORMATION TABLE
#define BILLERNICKNAME @"billerNickName"
#define BILLERNAME @"billerName"
#define BILLERTYPE @"billerType"
#define BILLERCATEGORY @"billerCategory"
#define BILLERID @"billerId"
#define BILLERDUEDATE @"billerDueDate"
#define BILLERSHORTNAME @"billerShortName"
#define BILLERMASTERID @"billerMasterId"
#define BILLERMEMBERBILLERID @"billerMemberBillerId"
#define BILLERLOCATION @"billerLocation"
#define BILLERA1 @"billerA1"
#define BILLERA2 @"billerA2"
#define BILLERA3 @"billerA3"
#define BILLERA4 @"billerA4"
#define BILLERA5 @"billerA5"
#define BILLERREFERENCE1 @"billerReference1"
#define BILLERREFERENCE2 @"billerReference2"
#define BILLERREFERENCE3 @"billerReference3"
#define BILLERREFERENCE4 @"billerReference4"
#define BILLERREFERENCE5  @"billerReference5"
#define BILLPAYCODE @"billPayCode"

#define BILLER_TYPE_HYBRID @"HYBRID"
#define BILLER_TYPE_FIXED  @"FIXED"
#define BILLER_TYPE_VARIABLE @"VARIABLE"

#define SEND_MONEY @"Send Money"
#define PAY_BILL  @"Pay Bill"
#define TRANSACTION_STATUS  @"COMPLETED"


#define PAYEE_DEVICE_NUMBER @"pdn"
#define PAYEE_NAME @"pn"
#define PAYEE_NICK_NAME @"pnn"

#define PAYEE_FIRST_NAME @"pfn"
#define PAYEE_LAST_NAME @"pln"
#define PAYEE_BANK_NAME @"bn"
#define PAYEE_MMID @"mmid"
#define PAYEE_IFSC @"ifsc"
#define PAYEE_ACCOUNT_NUMBER @"acctNo"


// Bundle Parameters
#define BUNDLE_TRANSACTION_TYPE @"transaction_type"
#define BUNDLE_TRANSACTION_AMOUNT @"transaction_amount"
#define BUNDLE_TRANSACTION_VALIDITY @"transaction_date"
#define BUNDLE_FREE_TRANSACTIONS @"free_transactions"
#define BUNDLE_TRANSACTION_SUBSCRIBE_DATE @"subscribe_date"


@end
