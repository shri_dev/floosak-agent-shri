//
//  WebServiceResponseProcessing.h
//  Consumer Client
//
//  Created by android on 6/4/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceDataObject.h"
#import "DatabaseManager.h"


@protocol WebServiceResponseProcessingDelegate <NSObject>

@required

-(void)processData:(WebServiceDataObject *)webServiceDataObject withProcessCode:(NSString *)processorCode;
-(void)errorCodeHandlingInDBWithCode:(NSString *)errorCode withProcessorCode:(NSString *)processorCode;
-(void)processDataForSMS:(WebServiceDataObject *)webServiceDataObject withProcessCode:(NSString *)processorCode;

@end

@interface WebServiceResponseProcessing : NSObject
{
    BOOL isDBAvailble;
    DatabaseManager *databaseManager;
}


@property (nonatomic, assign) NSObject <WebServiceResponseProcessingDelegate> *delegate;

-(void)processResponseData:(WebServiceDataObject *)pWebServiceData withRequestDictionary :(NSMutableDictionary *)reqDictionary;

@end
