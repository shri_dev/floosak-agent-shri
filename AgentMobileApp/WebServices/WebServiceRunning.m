//
//  WebServiceRunning.m
//  Consumer Client
//
//  Created by android on 6/5/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "WebServiceRunning.h"
#import "XmlParserHandler.h"
#import "Constants.h"
#import "Localization.h"
#import "AppDelegate.h"
//#import "Log.h"

@implementation WebServiceRunning
{
    NSMutableDictionary *requestDictionary;
}
@synthesize webServiceRunningDelegate;

-(void)startWebServiceWithRequest:(NSString *)request withReqDictionary:(NSMutableDictionary *)reqDict withDelegate:(id)sendDelegate
{
    
    [self setActivityIndicator];
    requestDictionary = reqDict;
    senderDelegate = sendDelegate;
    // Application BaseUrl
    NSString *urlString;
    urlString = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(@"application_base_url",@"GeneralSettings",[NSBundle mainBundle], nil)];
    NSLog(@"startWebServiceWithRequest.. %@", urlString);
    NSString *timeoutStr = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(@"application_http_connection_time_out",@"GeneralSettings",[NSBundle mainBundle], nil)];
    NSLog(@"timeoutStr = %@", timeoutStr);
    NSTimeInterval timeInterval = [timeoutStr doubleValue];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%d", (int)[request length]];
    [theRequest addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue:msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setTimeoutInterval:timeInterval];
    [theRequest setHTTPBody: [request dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if(connection){
        NSLog(@"Obtained connection..");
        responseData = [[NSMutableData alloc] init];
    }
}

#pragma mark Connection Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
//    NSLog(@"Connection did receive response..%@", response);
    [responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
//    NSLog(@"connectionDidFinishLoading..%@", connection);
    [self removeActivityIndicator];
    
    NSString *xmlResponseString = [[NSString alloc] initWithBytes: [responseData mutableBytes] length:[responseData length] encoding:NSUTF8StringEncoding];
    xmlResponseString = [self htmlEntityDecode:xmlResponseString];
    
    NSLog(@"WEBSERVICE_RESPONSE: %@", xmlResponseString);
    
    NSData *local_responseData = [xmlResponseString dataUsingEncoding:NSUTF8StringEncoding];
    XmlParserHandler *parser = [[XmlParserHandler alloc] init];
    parser.delegate = senderDelegate;
    [parser parseResponseData:local_responseData withReqDictionary:requestDictionary  withDelegate:senderDelegate];
}


- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    
//    NSLog(@"canAuthenticateAgainstProtectionSpace %@", [protectionSpace authenticationMethod]);
    
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    
//    NSLog(@"didReceiveAuthenticationChallenge %@ %zd", [[challenge protectionSpace] authenticationMethod], (ssize_t) [challenge previousFailureCount]);
    
    [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
}

-(NSString *)htmlEntityDecode:(NSString *)string
{
    string = [string stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    string = [string stringByReplacingOccurrencesOfString:@"&apos;" withString:@"'"];
    string = [string stringByReplacingOccurrencesOfString:@"&amp;" withString:@"and"];
    string = [string stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    string = [string stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    
    return string;
}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"connection didFailWithError.. %@", error);
    [self removeActivityIndicator];
    // The request has failed for some reason!
    // Check the error var
    NSString *appendStringval=nil;
    if([NSLocalizedStringFromTableInBundle(@"application_is_error_code_required_for_error_message",@"GeneralSettings",[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        appendStringval = [[[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_label_prefix", nil)] stringByAppendingString:[NSString stringWithFormat:@"_1003"]] stringByAppendingString:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_error_msg_seperator", nil)]];
    else
        appendStringval=@"";
    
    [senderDelegate errorCodeHandlingInWebServiceRunningWithErrorCode:[NSString stringWithFormat:@"%@%@",appendStringval,NSLocalizedStringFromTableInBundle(@"_1003",[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil)]];
}

#pragma mark - Add ctivity indicator.
/**
 * This method is used to set Add Activity indicator for View.
 */
-(void) setActivityIndicator
{
    activityIndicator=[[ActivityIndicator alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:activityIndicator];
    activityIndicator.hidden = NO;
    [activityIndicator startActivityIndicator];
}
#pragma mark - remove ctivity indicator.
/**
 * This method is used to set Activity indicator Remove from View.
 */
-(void) removeActivityIndicator
{
    activityIndicator.hidden = YES;
    [activityIndicator stopActivityIndicator];
    [activityIndicator removeFromSuperview];
    
}


@end
