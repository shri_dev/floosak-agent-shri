//
//  WebSericeUtils.h
//  Consumer Client
//
//  Created by android on 6/4/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebSericeUtils : NSObject
/**
 * This method is used to Form The WebService Request.
 @param Type- Input Details dictionary and Processor code of Feature.
 */
-(NSMutableDictionary *)getWebServiceBundleObjectWithProccessorCode:(NSString *)proccessorCode withInputDataModel:(NSDictionary *)dataDictionary;

- (NSMutableDictionary *)getGovernoratesBundle:(NSDictionary *)localWebServiceValues;


@end
