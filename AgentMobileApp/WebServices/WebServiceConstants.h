//
//  WebServiceConstants.h
//  Consumer Client
//
//  Created by android on 6/4/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebServiceConstants : NSObject

//#define requestEnvelopeStartTag @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v1=\"http://www.obopay.com/xml/oews/v1\" xmlns:java=\"java:com.obopay.core.transferobject\" xmlns:java1=\"java:com.ewp.webservices.data\">"

// For T1 ,T2 and T3
#define requestEnvelopeStartTag @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v1=\"http://www.obopay.com/xml/oews/v1\" xmlns:java=\"java:com.obopay.core.transferobject\" xmlns:java1=\"java:com.obopay.ws.data\">"

//<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://www.obopay.com/xml/oews/v1" xmlns:java="java:com.obopay.core.transferobject" xmlns:java1="java:com.obopay.ws.data">
//<soapenv:Header>
//<obons:header name="UserName" xmlns:obons="http://www.obopay.com/xml/oews/v1">mobileApp01</obons:header>
//<obons:header name="Password" xmlns:obons="http://www.obopay.com/xml/oews/v1">$mobileApp123$</obons:header>
//<obons:header name="PartnerId" xmlns:obons="http://www.obopay.com/xml/oews/v1">MobileApp|0x01060965</obons:header>
//<obons:header name="iv" xmlns:obons="http://www.obopay.com/xml/oews/v1">@X9M7Fdh</obons:header>
//</soapenv:Header>


#define requestEnvelopeEndTag @"</soapenv:Envelope>"

//#define requestHeaderTag @"<soapenv:Header/>"

#define requestHeaderTag @"<soapenv:Header><obons:header name=\"UserName\" xmlns:obons=\"http://www.obopay.com/xml/oews/v1\">mobileApp01</obons:header><obons:header name=\"Password\" xmlns:obons=\"http://www.obopay.com/xml/oews/v1\">$mobileApp123$</obons:header><obons:header name=\"PartnerId\" xmlns:obons=\"http://www.obopay.com/xml/oews/v1\">AgentApp|0x01060966</obons:header><obons:header name=\"iv\" xmlns:obons=\"http://www.obopay.com/xml/oews/v1\">@X9M7Fdh</obons:header></soapenv:Header>"

//"<soapenv:Header><obons:header name=\"UserName\" xmlns:obons=\"http://www.obopay.com/xml/oews/v1\">mobileApp01</obons:header><obons:header name=\"Password\" xmlns:obons=\"http://www.obopay.com/xml/oews/v1\">$mobileApp123$</obons:header><obons:header name=\"PartnerId\" xmlns:obons=\"http://www.obopay.com/xml/oews/v1\">MobileApp|0x01060965</obons:header><obons:header name=\"iv\" xmlns:obons=\"http://www.obopay.com/xml/oews/v1\">@X9M7Fdh</obons:header></soapenv:Header\>"

#define requestBodyStartTag @"<soapenv:Body>"
#define requestBodyEndTag @"</soapenv:Body>"
#define requestMethodStartTag @"<v1:process>"
#define requestMethodEndTag @"</v1:process>"
#define requestStartTag @"<v1:request>"
#define requestEndTag @"</v1:request>"

#define keyPrefix1 @"<java:"
#define keyPrefix3 @"</java:"
#define keyPrefix @"<java1:"
#define keyPrefix2 @"</java1:"
#define angBracketClose @">"
#define newLine @"\n"
#define ResponseKeyPrefix1 @"java:"

#define APIID_TYPE @"REQ"
#define TIER1_AGENT_ID NSLocalizedStringFromTableInBundle(@"application_tier_1_agent_id",@"GeneralSettings",[NSBundle mainBundle], nil)
#define SMS_TIER1_AGENT_ID NSLocalizedStringFromTableInBundle(@"application_sms_tier1_agent_id",@"GeneralSettings",[NSBundle mainBundle], nil)

#define INSTRUMENT_TYPE_ZERO @"0"
#define INSTRUMENT_TYPE_ONE @"1"
#define VALUE_ZERO @"0"
#define VALUE_TEN_T1 @"10"
#define VALUE_ONE @"1"
#define VALUE_TWO @"2"
//#define CURRENCY_CODE @"INR"
#define CURRENCY_CODE @"YER"
#define CURRENCY_CODE_T1 @"KES"

// START - Transaction codes

#define TRANSACTION_CODE_FETCH_FEE @"100"
#define TRANSACTION_CODE_BALANCE_ENQUIRY @"102"
#define TRANSACTION_CODE_CASH_OUT_WITHOUT_OTP @"103"
#define TRANSACTION_CODE_MINI_STATEMENT @"104"
#define TRANSACTION_CODE_CAHNGE_PIN @"105"
#define TRANSACTION_CODE_ADD_BILLER @"106"
#define TRANSACTION_CODE_PAY_BILLER_NICK_NAME @"107"
#define TRANSACTION_CODE_PAY_BILLER_ADHOC @"108"
#define TRANSACTION_CODE_PAY_MERCHANT @"158"

#define TRANSACTION_CODE_TOP_UP_PREPAID @"109"
#define TRANSACTION_CODE_TOP_UP_MOBILE @"110"
#define TRANSACTION_CODE_P2P_REGISTERED_BENEFICIARY_MOBILE_NUMBER @"112"
#define TRANSACTION_CODE_P2P_REGISTERED_BENEFICIARY_NICK_NAME @"113"
#define TRANSACTION_CODE_P2P_UNREGISTERED_VIRAL @"114"
#define TRANSACTION_CODE_FETCH_BILLER_ALL @"000"
#define TRANSACTION_CODE_FETCH_MY_BILLERS @"139"
#define TRANSACTION_CODE_FETCH_BILLS @"121"
#define TRANSACTION_CODE_IMPS_IFSC @"122"
#define TRANSACTION_CODE_IMPS_MMID @"122"
#define TRANSACTION_CODE_BANK @"122"
#define TRANSACTION_CODE_SELF_REG @"123"
#define TRANSACTION_CODE_CHANGE_LANG @"124"
#define TRANSACTION_CODE_ACTIVATION @"123"
#define TRANSACTION_CODE_PAYU_LOAD_AUTHENTICATE @"124"
#define TRANSACTION_CODE_PAYU_PAYMENT @"124"
#define TRANSACTION_CODE_RESET_PIN_FORGOT_MPIN @"105"
#define TRANSACTION_CODE_LOGIN @"124"
#define TRANSACTION_CODE_LOGOUT @"105"
#define TRANSACTION_CODE_ADD_PAYEE @"112"
#define TRANSACTION_CODE_MY_PAYEE @"112"
#define TRANSACTION_CODE_VIEW_PAYEE @"133"
#define TRANSACTION_CODE_CASH_WITHDRAW_FROM_AGENT @"105"
#define TRANSACTION_CODE_CHECK_MY_VELOCITY @"134"
#define TRANSACTION_CODE_TOP_UP_PREPAID_MOBILE_OPERATOR @"109"
#define TRANSACTION_CODE_SELF_REG_WITH_KYC @"123"
#define TRANSACTION_CODE_SELECT_LIST_DROPDOWN_LIST @"135"
#define TRANSACTION_CODE_AGENT_DETAILS @"136"
#define TRANSACTION_CODE_PENDING_BILLS @"137"
#define TRANSACTION_CODE_LAUNCH @"138"

#define TRANSACTION_CODE_FETCH_BENEFICIARY_IFSC @"148"
#define TRANSACTION_CODE_FETCH_BENEFICIARY_MMID @"150"
#define TRANSACTION_CODE_FETCH_BENEFICIARY_NEFT @"149"

//Sprint2NewAddition
#define TRANSACTION_CODE_CASHOUT_BANK @"150"
#define TRANSACTION_CODE_CASHOUT_AGGREGATOR @"151"
#define TRANSACTION_CODE_CASHOUT_BANK_LIST @"156"

#define TRANSACTION_CODE_BANK_IFSC @"149"
#define TRANSACTION_CODE_BANK_NEFT @"148"
#define TRANSACTION_CODE_BANK_MMID @"150"

#define TRANSACTION_CODE_PAYU_SEAMLESS @"127"

// SignUp DKYC
#define TRANSACTION_CODE_SIGNUP_DKYC @"153"

// PRODUCT DETAILS
#define TRANSACTION_CODE_PRODUCT_DETAILS @"149"

// UPDATEDEVICE ID
#define TRANSACTION_CODE_UPDATEDEVICE_ID @"155"

// END - Transaction codes

// START - Processor codes
#define PROCESSOR_CODE_BANK @"0066"

#define PROCESSOR_CODE_FETCH_FEE @"0017"
#define PROCESSOR_CODE_BALANCE_ENQUIRY @"0038"
#define PROCESSOR_CODE_CASH_OUT_WITHOUT_OTP @"0039"
#define PROCESSOR_CODE_MINI_STATEMENT @"0040"
#define PROCESSOR_CODE_CAHNGE_PIN @"0041"
#define PROCESSOR_CODE_ADD_BILLER @"0042"
#define PROCESSOR_CODE_PAY_BILLER_NICK_NAME @"0153~2.0"
//#define PROCESSOR_CODE_PAY_BILLER_NICK_NAME @"0043"
//#define PROCESSOR_CODE_PAY_BILLER_ADHOC @"0043"
#define PROCESSOR_CODE_PAY_MERCHANT @"0163"
#define PROCESSOR_CODE_TOP_UP_PREPAID @"0154~2.0"
#define PROCESSOR_CODE_P2P_REGISTERED_BENEFICIARY_MOBILE_NUMBER @"0048"
#define PROCESSOR_CODE_P2P_REGISTERED_BENEFICIARY_NICK_NAME @"0049"

#define PROCESSOR_CODE_FETCH_MY_BILLERS @"0051"
#define PROCESSOR_CODE_FETCH_MY_PAYEES @"0051"
#define PROCESSOR_CODE_FETCH_BILLER_ALL @"0053"
#define PROCESSOR_CODE_FETCH_BILLS @"0065"
#define PROCESSOR_CODE_FETCH_BENEFICIARY_BANK @"0051"
#define PROCESSOR_CODE_SELF_REG @"0081"
#define PROCESSOR_CODE_VERIFY_REG @"0209"
#define PROCESSOR_CODE_CHANGE_LANG @"0070"
#define PROCESSOR_CODE_ACTIVATION @"0071"
#define PROCESSOR_CODE_PAYU_LOAD_AUTHENTICATE @"0072"
#define PROCESSOR_CODE_PAYU_PAYMENT @"0074"
#define PROCESSOR_CODE_RESET_PIN_FORGOT_MPIN @"0076"
#define PROCESSOR_CODE_LOGIN @"0106"
#define PROCESSOR_CODE_LOGOUT @"0106"
#define PROCESSOR_CODE_ADD_PAYEE @"0108"
#define PROCESSOR_CODE_VIEW_PAYEE @"0109"
#define PROCESSOR_CODE_CASH_WITHDRAW_FROM_AGENT @"0039"
#define PROCESSOR_CODE_CHECK_MY_VELOCITY @"0111"
#define PROCESSOR_CODE_TOP_UP_PREPAID_MOBILE_OPERATOR @"0154~3.0"
#define PROCESSOR_CODE_TOP_UP_PREPAID_DTH_OPERATOR @"0073"
#define PROCESSOR_CODE_SELF_REG_WITH_KYC @"0081"
#define PROCESSOR_CODE_SELECT_LIST_DROPDOWN_LIST @"0125"
#define PROCESSOR_CODE_AGENT_DETAILS @"0110"
#define PROCESSOR_CODE_PENDING_BILLS @"0107"
#define PROCESSOR_CODE_LAUNCH @"0044"
#define PROCESSOR_CODE_FETCH_IFSC @"0150"
#define PROCESSOR_CODE_UPGRADE @"0197"
#define PROCESSOR_CODE_GENERATE_OTP @"1234"
#define PROCESSOR_CODE_CASHOUT_BANK_AGGREGATOR @"0047"
#define PROCESSOR_CODE_CASHOUT_BANK_LIST @"0161"
//#define PROCESSOR_CODE_SELF_REG_DIGITAL_KYC @"0160"
#define PROCESSOR_CODE_SELF_REG_DIGITAL_KYC @"0160~2.0"


//by Shri
#define PROCESSOR_CODE_FETCH_AREA_CODES @"0216"



#define PROCESSOR_CODE_CASH_IN_SEAMLESS @"900"
#define PROCESSOR_CODE_CASH_IN_NONSEAMLESS @"901"
#define PROCESSOR_CODE_CASH_IN_NETBANKING @"902"


#define PROCESSOR_CODE_PRODUCT_DETAILS @"0159"

#define PROCESSOR_CODE_UPDATEDEVICE_ID @"0151"

// Processor Code for SignUp KYC
#define PROCESSOR_CODE_IMAGE_CROP @"7001"

// Processor Code Merchant Barcode/QrCode.
#define PROCESSOR_CODE_PAY_MERCHANT_QRCODE @"6000"
#define PROCESSOR_CODE_PAY_MERCHANT_BARCODE @"6001"

//TemporaryProcessorCode
#define PROCESSOR_CODE_AGGREGATOR_BANK_LIST @"0047"

//Aggregator Linked BankList
#define PROCESSOR_CODE_AGGREGATOR_LINKED_BANK_LIST @"0162"



// Processor Codes and Transaction Type For T1,T2 and T3.
// LOGIN.
#define PROCESSOR_CODE_LOGIN_T3 @"0088"
#define TRANSACTION_CODE_LOGIN_T3 @"118"

// ACTIVATION CODE.
#define PROCESSOR_CODE_ACTIVATION_T3 @"0115"
#define TRANSACTION_CODE_ACTIVATION_T3 @"123"

//CHECK BALANCE
#define PROCESSOR_CODE_CHECK_BALANCE_T3 @"0032"
#define TRANSACTION_CODE_CHECK_BALANCE_T3 @"304"

// CHANGE LANGUAGE.
#define  PROCESSOR_CODE_CHANGE_LANG_T3 @"0174"
#define  TRANSACTION_CODE_CHANGE_LANGUAGE_T3 @"014"

// CHECK VELOCITY LIMITS.
#define PROCESSOR_CODE_CHECK_VELOCITY_LIMITS_T3 @"0171"
#define TRANSACTION_CODE_CHECK_VELOCITY_LIMITS_T3 @"343"
//#define TRANSACTION_CODE_CHECK_VELOCITY_LIMITS_T3 @"012"

// CHANGE MPIN
#define PROCESSOR_CODE_CHANGE_MPIN_T3 @"0116"
#define TRANSACTION_CODE_CHANGE_MPIN_T3 @"105"

// FORGOT MPIN
#define PROCESSOR_CODE_FORGOT_MPIN_T3 @"0116"
#define TRANSACTION_CODE_FORGOT_MPIN_T3 @"106"



// FETCH FEE
#define PROCESSOR_CODE_FETCH_FEE_T3 @"0140"
#define TRANSACTION_CODE_FETCH_FEE_T3 @"100"

// DEPOSITE
#define PROCESSOR_CODE_GROUPSAVING_CASH_IN_T3 @"0146~2.0"
#define TRANSACTION_CODE_GROUPSAVING_CASH_IN_T3 @"019"

#define PROCESSOR_CODE_CUSTOMER_CASH_IN_T3 @"0146~2.0"
#define TRANSACTION_CODE_CUSTOMER_CASH_IN_T3 @"136"

//WITH DRAWAL
#define PROCESSOR_CODE_CASHOUT_VIRAL_T3 @"0147~2.0"
#define TRANSACTION_CODE_CASHOUT_VIRAL_T3 @"141"

#define PROCESSOR_CODE_CASHOUT_REGISTERED_T3 @"0147~2.0"
#define TRANSACTION_CODE_CASHOUT_REGISTERED_T3 @"140"


// PULL
#define PROCESSOR_CODE_PULL_PENDING_LIST_T3 @"0173"
#define TRANSACTION_CODE_PULL_PENDING_LIST_T3 @"351"

#define PROCESSOR_CODE_PULL_DECLINE_REQUEST_T3 @"0084"
#define TRANSACTION_CODE_PULL_DECLINE_REQUEST_T3 @"336"

#define PROCESSOR_CODE_PULL_ACCEPT_REQUEST_T3 @"0084"
#define TRANSACTION_CODE_PULL_ACCEPT_REQUEST_T3 @"335"


// MY ACCOUNT : REPORTS
#define PROCESSOR_CODE_REPORTS_SUMMARY_T3 @"0062"
#define TRANSACTION_CODE_REPORTS_SUMMARY_T3 @"344"

#define PROCESSOR_CODE_TXN_SUMMARY_T3 @"0062"
#define TRANSACTION_CODE_TXN_SUMMARY_T3 @"345"

#define PROCESSOR_CODE_TXN_HISTORY_T3 @"0062"
#define TRANSACTION_CODE_TXN_HISTORY_T3 @"346"

#define PROCESSOR_CODE_TRANSACTION_STATUS_T3 @"0080"
#define TRANSACTION_CODE_TRANSACTION_STATUS_T3 @"130"

// SELF REGISTRATION
#define PROCESSOR_CODE_SELF_REG_T3 @"0081"
#define TRANSACTION_CODE_SELF_REG_T3 @"131"

// BANK
#define PROCESSOR_CODE_GET_BANK_DETAILS_T3 @"0172"
#define TRANSACTION_CODE_GET_BANK_DETAILS_T3 @"020"

#define PROCESSOR_CODE_UNLOAD_LINKED_BANK_T3 @"0021"
#define TRANSACTION_CODE_UNLOAD_LINKED_BANK_T3 @"339"

#define PROCESSOR_CODE_UNLOAD_OTHER_BANK_T3 @"0021"
#define TRANSACTION_CODE_UNLOAD_OTHER_BANK_T3 @"340"

#define PROCESSOR_CODE_LOAD_BANK_T3 @"0022"
#define TRANSACTION_CODE_LOAD_BANK_T3 @"341"

#define PROCESSOR_CODE_GET_BANK_LIST_DETAILS_T3 @"0094"
#define TRANSACTION_CODE_GET_BANK_LIST_DETAILS_T3 @"015"

#define PROCESSOR_CODE_CHECK_BANK_BALANCE_T3 @"0032"
#define TRANSACTION_CODE_CHECK_BANK_BALANCE_T3 @"023"

#define PROCESSOR_CODE_BANK_TXN_HISTORY_T3 @"0062"
#define TRANSACTION_CODE_BANK_TXN_HISTORY_T3 @"024"

#define PROCESSOR_CODE_GET_GOVERNORATES @"0216"
#define TRANSACTION_CODE_GET_GOVERNORATES @"242"


// END - PROCESSOR codes
#define PARAMETER1 @"GwalTraceId"
#define PARAMETER2 @"ApiIdType"
#define PARAMETER3 @"Tier1AgentId"
#define PARAMETER4 @"Tier1AgentName"
#define PARAMETER5 @"Tier2AgentId"
#define PARAMETER6 @"Tier3AgentId"
#define PARAMETER7 @"Tier1AgentPassword"
#define PARAMETER8 @"Tier3AgentPassword"
#define PARAMETER9 @"CustomerPhoneNumber"
#define PARAMETER10 @"NationalID"
#define PARAMETER11 @"SenderFirstName"
#define PARAMETER12 @"SenderLastName"
#define PARAMETER13 @"TransactionType"
#define PARAMETER14 @"InstrumentType"
#define PARAMETER15 @"ProcessorCode"
#define PARAMETER16 @"CashInAmount"
#define PARAMETER17 @"PaymentDetails1"
#define PARAMETER18 @"PaymentDetails2"
#define PARAMETER19 @"PaymentDetails3"
#define PARAMETER20 @"PaymentDetails4"
#define PARAMETER21 @"TxnAmount"
#define PARAMETER22 @"CurrencyType"
#define PARAMETER23 @"NetTxnAmount" 
#define PARAMETER24 @"FeeAmount"
#define PARAMETER25 @"TaxAmount"
#define PARAMETER26 @"Country"
#define PARAMETER27 @"RequestId"
#define PARAMETER28 @"TerminalID"
#define PARAMETER29 @"TxnStatus"
#define PARAMETER30 @"ErrorCode"
#define PARAMETER31 @"OpsTransactionId"
#define PARAMETER32 @"TransactionDate"
#define PARAMETER33 @"Remark"
#define PARAMETER34 @"Reference1"
#define PARAMETER35 @"Reference2"
#define PARAMETER36 @"Reference3"
#define PARAMETER37 @"Reference4"
#define PARAMETER38 @"Reference5"

#define RES_FAULTCODE @"faultcode"
#define RES_FAULTSTRING @"faultstring"
#define RES_FAULT @"soapenv:Fault"
#define RES_PARAMETER1 @"java:GwalTraceId"
#define RES_PARAMETER2 @"java:ApiIdType"
#define RES_PARAMETER3 @"java:Tier1AgentId"
#define RES_PARAMETER4 @"java:Tier1AgentName"
#define RES_PARAMETER5 @"java:Tier2AgentId"
#define RES_PARAMETER6 @"java:Tier3AgentId"
#define RES_PARAMETER7 @"java:Tier1AgentPassword"
#define RES_PARAMETER8 @"java:Tier3AgentPassword"
#define RES_PARAMETER9 @"java:CustomerPhoneNumber"
#define RES_PARAMETER10 @"java:NationalID"
#define RES_PARAMETER11 @"java:SenderFirstName"
#define RES_PARAMETER12 @"java:SenderLastName"
#define RES_PARAMETER13 @"java:TransactionType"
#define RES_PARAMETER14 @"java:InstrumentType"
#define RES_PARAMETER15 @"java:ProcessorCode"
#define RES_PARAMETER16 @"java:CashInAmount"
#define RES_PARAMETER17 @"java:PaymentDetails1"
#define RES_PARAMETER18 @"java:PaymentDetails2"
#define RES_PARAMETER19 @"java:PaymentDetails3"
#define RES_PARAMETER20 @"java:PaymentDetails4"
#define RES_PARAMETER21 @"java:TxnAmount"
#define RES_PARAMETER22 @"java:CurrencyType"
#define RES_PARAMETER23 @"java:NetTxnAmount"
#define RES_PARAMETER24 @"java:FeeAmount"
#define RES_PARAMETER25 @"java:TaxAmount"
#define RES_PARAMETER26 @"java:Country"
#define RES_PARAMETER27 @"java:RequestId"
#define RES_PARAMETER28 @"java:TerminalID"
#define RES_PARAMETER29 @"java:TxnStatus"
#define RES_PARAMETER30 @"java:ErrorCode"
#define RES_PARAMETER31 @"java:OpsTransactionId"
#define RES_PARAMETER32 @"java:TransactionDate"
#define RES_PARAMETER33 @"java:Remark"
#define RES_PARAMETER34 @"java:Reference1"
#define RES_PARAMETER35 @"java:Reference2"
#define RES_PARAMETER36 @"java:Reference3"
#define RES_PARAMETER37 @"java:Reference4"
#define RES_PARAMETER38 @"java:Reference5"

#define LOCAL_DATA @"0"
#define REMOTE_DATA @"1"

#define SAVEDMOBILENUMBER @"savedMobileNumber"
#define ACTIVATIONMOBILENUMBER @"activationMobileNumber"

#define OPS_TRANSACTION_ID @""
#define TIER1_AGENT_NAME_T3 @"agent"
#define APP_VERSION2 @"2.0"
#define APP_VERSION1 @"1.0"

#define DTH_VERSION @"4.3"

#define PARAMETER_TYPE @"paramType"

#define PARAMETER_BILL_PAY @"BILLPAY"
#define PARAMETER_P2P @"P2P"
#define PARAMETER_OBOANY @"OBOANY"
#define PARAMETER_NP2P @"NP2P"
#define PARAMETER_WALLET_TO_BANK @"W2B"
#define PARAMETER_PAY_TO_MERCHANT @"P2M"
#define PARAMETER_TOPUP @"TOPUP"
#define PARAMETER_DTH @"DTH"
#define PARAMETER_BCASHOUT @"BCASHOUT"

//  For T3
#define PARAMETER_TOPUP_T3 @"TOPUP"
#define PARAMETER_DTH_T3 @"DTH"
#define PARAMETER_CASH_IN @"CASHIN"
#define PARAMETER_CASH_OUT_T3 @"CASHOUT"
#define PARAMETER_VIRAL_CASH_OUT_T3 @"VIRAL_PICK"

#define PARAMETER_PAYBILL_T3 @"BILLPAY"
#define PARAMETER_BANK_UNLOAD_T3 @"EQUITYUNLOAD"
#define PARAMETER_BANK_LOAD_T3 @"EQUITYUNLOAD"
#define PARAMETER_BANK_UNLOAD_OTHERS_T3 @"UNLOAD"




#define PARAMETER_RETINA @"RETINA"
#define AVAILABLELIMIT  @"AvailableLimit"
#define AVAILABLEVOLUME @"AvailableVolume"
#define MINIMUMAMOUNT   @"MinAmount"
#define MAXIMUMAMOUNT   @"MaxAmount"

#define PROCESSOR_CODE_VIEW_ALL_ACTIVE_BUNDLES @"0175"
#define TRANSACTION_CODE_VIEW_ALL_ACTIVE_BUNDLES @"160"

@end
