//
//  WebServiceResponseProcessing.m
//  Consumer Client
//
//  Created by android on 6/4/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "WebServiceResponseProcessing.h"
#import "WebServiceConstants.h"
#import "DatabaseConstatants.h"
#import "Template.h"
#import "ValidationsClass.h"
#import "Constants.h"
#import "AppDelegate.h"

@implementation WebServiceResponseProcessing
{
    NSMutableDictionary *requestDictinary;
}

@synthesize delegate;


#pragma mark - Response processing For ConsumerClient Project.

-(void)processResponseData:(WebServiceDataObject *)pWebServiceData withRequestDictionary :(NSMutableDictionary *)reqDictionary;
{
    NSLog(@"processResponseData:(WebServiceDataObject..");
    requestDictinary = reqDictionary;
    
    if (pWebServiceData.faultString)
    {
        NSLog(@"pWebServiceData.faultString: %@", pWebServiceData.faultString);
        if ([Template checkMode]) {
            [delegate processDataForSMS:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
        }
        else
        [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
    }
    else
    {
        if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_TOP_UP_PREPAID] && [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_TOP_UP_MOBILE])
        {
            NSLog(@"Mobile Recharge");
            [self processTopUpMobile:pWebServiceData];
        }
        else
        if (([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_GET_GOVERNORATES] && [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_GET_GOVERNORATES]) && !(pWebServiceData.PaymentDetails1 && [pWebServiceData.PaymentDetails1 rangeOfString:@"FLAT|BillerID"].location != NSNotFound))
        {
            [[NSUserDefaults standardUserDefaults] setObject:pWebServiceData.PaymentDetails2 forKey:@"GovernoratesDict"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateGovernorates" object:self userInfo:nil];
        }else
        if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_TOP_UP_PREPAID_MOBILE_OPERATOR] && [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_TOP_UP_PREPAID_MOBILE_OPERATOR])
        {
            NSLog(@"DTH Recharge");
            [self processTopUpDTH:pWebServiceData];
        }
        else
        if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_TOP_UP_PREPAID] && [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_TOP_UP_PREPAID])
        {
            NSLog(@"DTH Recharge");
            [self processTopUpDTH:pWebServiceData];
        }
        
        else
        if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_BANK] && [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_BANK_NEFT])
        {
            [self processBankNEFTResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_BANK] && [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_BANK_IFSC])
        {
            [self processBankIFSCIMPSResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_BANK] && [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_BANK_MMID])
        {
            [self processBankMMIDResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_LOGIN])
        {
            [self processLoginResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_MINI_STATEMENT])
        {
            [self processMiniStatementResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_BALANCE_ENQUIRY])
        {
            [self processBalanceEnquiryResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_FETCH_FEE])
        {
            [self processFetchFeeResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_P2P_REGISTERED_BENEFICIARY_MOBILE_NUMBER])
        {
            [self processP2PResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_SELECT_LIST_DROPDOWN_LIST])
        {
            [self processDropDownResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_ACTIVATION])
        {
            [self processActivationResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_SELF_REG])
        {
            [self processSignUpResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_RESET_PIN_FORGOT_MPIN])
        {
            [self processforgotResetMPINResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_CAHNGE_PIN])
        {
            [self processChangeMPINResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_AGENT_DETAILS])
        {
            [self processAgentDetailsResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_CASH_OUT_WITHOUT_OTP])
        {
            [self processCashOutAgetWithoutOTPResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_PENDING_BILLS])
        {
            [self processPendingBillsResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG])
        {
            [self processChangeLanguageResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_FETCH_BILLS])
        {
            [self processFetchPayBillsResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_PAY_BILLER_NICK_NAME])
        {
            if([pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_PAY_BILLER_NICK_NAME] && [pWebServiceData.Reference5 isEqualToString:VALUE_ONE])
                [self processPayBillByNickNameResponse:pWebServiceData];
            else if ([pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_PAY_MERCHANT] && [pWebServiceData.Reference5 isEqualToString:VALUE_TWO])
                [self processPayMerchantBundle:pWebServiceData];
            else if([pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_PAY_BILLER_ADHOC] && [pWebServiceData.Reference5 isEqualToString:VALUE_ONE])
                [self processPayBillByNickNameResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_FETCH_MY_BILLERS])
        {
            if([pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_MY_PAYEE])
                [self processMyPayeeResponse:pWebServiceData];
            else
                [self processPayBillFetchMyBillersResponse:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_FETCH_BILLER_ALL])
        {
            [self processFetchAllBillersBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_ADD_BILLER])
        {
            [self processAddBillersBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_FETCH_IFSC])
        {
            [self processFetchIFSC:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_CHECK_MY_VELOCITY])
        {
            [self processCheckVelocityBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_ADD_PAYEE])
        {
            [self processAddPayeeBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_VIEW_PAYEE])
        {
            [self processViewPayeeBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_LAUNCH])
        {
            NSLog(@"WSRP processLaunchAPIBundle..");
            [self processLaunchAPIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_CASHOUT_BANK_LIST])
        {
            [self processBankAggregatorList:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_PAYU_LOAD_AUTHENTICATE])
        {
            [self processPayUApiBumdle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_SELF_REG_DIGITAL_KYC])
        {
            [self processSignUPDKYCAPIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_PRODUCT_DETAILS])
        {
            [self processProductDetailsAPIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_UPDATEDEVICE_ID])
        {
            [self processUpdateDeviceIDBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_PAY_MERCHANT])
        {
            [self processPayMerchant:pWebServiceData];
        }
        // Newly Added for T1,T2 and T3
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_LOGIN_T3])
        {
            [self processLoginDetails_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_ACTIVATION_T3])
        {
            [self processActivation_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_CHECK_BALANCE_T3]&& [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_CHECK_BALANCE_T3])
        {
            [self processCheckBalance_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_TXN_HISTORY_T3] &&[pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_TXN_HISTORY_T3] )
        {
            [self processTransactionHistory_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG_T3])
        {
            [self processChangeLanguage_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_CHECK_VELOCITY_LIMITS_T3] &&[pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_CHECK_VELOCITY_LIMITS_T3])
        {
            [self processCheckVelocityLimits_T3APIBundle:pWebServiceData];
        }
      else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_CHANGE_MPIN_T3] && [pWebServiceData.TransactionType isEqualToString:PROCESSOR_CODE_CHANGE_MPIN_T3])
        {
            [self processChangeMPIN_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_CUSTOMER_CASH_IN_T3]&& [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_CUSTOMER_CASH_IN_T3])
        {
            [self processCustomerCashin_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_GROUPSAVING_CASH_IN_T3] && [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_GROUPSAVING_CASH_IN_T3])
        {
            [self processGroupCashin_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_CASHOUT_VIRAL_T3])
        {
            [self processCashOutViral_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_CASHOUT_REGISTERED_T3])
        {
            [self processCashoutRegistered_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_TRANSACTION_STATUS_T3])
        {
            [self processTransactionStatusDetails_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_REPORTS_SUMMARY_T3] && [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_REPORTS_SUMMARY_T3] )
        {
            [self processReportsSummary_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_TXN_SUMMARY_T3] &&[pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_TXN_SUMMARY_T3])
        {
            [self processTransactionSummary_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_BANK_TXN_HISTORY_T3] &&   [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_BANK_TXN_HISTORY_T3])
        {
            [self processBankTransactionHistory_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_CHECK_BANK_BALANCE_T3] && [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_CHECK_BANK_BALANCE_T3])
        {
            [self processBankBalanceDetails_T3APIBundle:pWebServiceData];
        }
      else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_PULL_PENDING_LIST_T3])
        {
            [self processPullPending_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_PULL_ACCEPT_REQUEST_T3]&& [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_PULL_ACCEPT_REQUEST_T3])
        {            
           [self processPullAcceptList_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_PULL_DECLINE_REQUEST_T3]&& [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_PULL_DECLINE_REQUEST_T3])
        {
         [self processPullDecline_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_SELF_REG_T3])
        {
            [self processSignUpDetails_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_GET_BANK_DETAILS_T3])
        {
            [self processBanksDetails_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_UNLOAD_OTHER_BANK_T3])
        {
            [self processUnloadOtherBankDetails_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_UNLOAD_LINKED_BANK_T3] && [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_UNLOAD_LINKED_BANK_T3])
        {
           [self processUnLoadLinkedBankDetails_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_LOAD_BANK_T3])
        {
             [self processLoadBankDetails_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_GET_BANK_LIST_DETAILS_T3])
        {
            [self processBankListDetails_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_FORGOT_MPIN_T3]&& [pWebServiceData.TransactionType isEqualToString:TRANSACTION_CODE_FORGOT_MPIN_T3])
        {
            [self processForgotMPINDetails_T3APIBundle:pWebServiceData];
        }
        else if ([pWebServiceData.ProcessorCode isEqualToString:PROCESSOR_CODE_FETCH_FEE_T3])
        {
            [self processFetchFeeResponse_T3:pWebServiceData];
        }
        else
        {
            [self processDefaultAPIBundle:pWebServiceData];
        }
    }
}
-(void)processDefaultAPIBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

-(void)processPayMerchant:(WebServiceDataObject *)pWebServiceData
{
     pWebServiceData.TransactionType = @"Pay Merchant";
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date]forKey:@"miniStatementDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self addMiniStatement:pWebServiceData];
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

-(void)processTopUpMobile:(WebServiceDataObject *)pWebServiceData
{
    NSLog(@"Request Data : %@",requestDictinary);
    //Storing to data base to be implemented

    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
    NSMutableArray *localDataArray = [[NSMutableArray alloc] initWithArray:[databaseManager getAllTransactionHistory]];
    
    BOOL isInseratable = NO;
    
    if ([localDataArray count] > 0)
    {
        for (int index = 0; index < [localDataArray count]; index++)
        {
            if (![pWebServiceData.OpsTransactionId isEqualToString:[[localDataArray objectAtIndex:index] objectForKey:TRANSACTION_DATA_TRANSACTION_ID]])
            {
                isInseratable = YES;
            }
        }
    }
    else
        isInseratable = YES;

    if (isInseratable)
    {
        NSMutableDictionary *otherDetails = [[NSMutableDictionary alloc] init];
        
        [otherDetails setObject:pWebServiceData.CustomerPhoneNumber forKey:TRANSACTION_DATA_TRANSACTION_SENDER_PHONE_NUMBER];
        [otherDetails setObject:pWebServiceData.FeeAmount forKey:TRANSACTION_DATA_TRANSACTION_FEE];
        [otherDetails setObject:pWebServiceData.ProcessorCode forKey:TRANSACTION_DATA_PROCCESSOR_CODE];
        [otherDetails setObject:pWebServiceData.TransactionType forKey:TRANSACTION_DATA_TRANSACTION_CODE];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_TIME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_FULL_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_NICK_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_FIRST_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_LAST_NAME];
        [otherDetails setObject:pWebServiceData.PaymentDetails2 forKey:TRANSACTION_DATA_TRANSACTION_OPERATOR_ID];
        [otherDetails setObject:pWebServiceData.PaymentDetails3 forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FULL_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FIRST_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_LAST_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_MESSAGE];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_BUSINESS_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_AGENT_BUSINESS_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_BUSINESS_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS1];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS2];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS3];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_CITY];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_PINCODE];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_COUNTRY];
        [otherDetails setObject:[NSString stringWithFormat:@"%d",(int)[localDataArray count]+1] forKey:TRANSACTION_DATA_COLOUMN_ID];
        [otherDetails setObject:pWebServiceData.TransactionType forKey:TRANSACTION_DATA_TRANSACTION_TYPE];
        [otherDetails setObject:pWebServiceData.TxnAmount forKey:TRANSACTION_DATA_TRANSACITON_AMOUNT];
        [otherDetails setObject:pWebServiceData.TransactionDate forKey:TRANSACTION_DATA_TRANSACTION_DATE];
        [otherDetails setObject:pWebServiceData.OpsTransactionId forKey:TRANSACTION_DATA_TRANSACTION_ID];
        [otherDetails setObject:pWebServiceData.TxnStatus forKey:TRANSACTION_DATA_TRANSACTION_STATUS];
//        [otherDetails setObject:pWebServiceData.Reference1 forKey:TRANSACTION_DATA_TRANSACTION_OPERATOR_NAME];
         [otherDetails setObject:[requestDictinary objectForKey:@"TOPUP_OPERATOR_typeDesc"] forKey:TRANSACTION_DATA_TRANSACTION_OPERATOR_ID];
        [otherDetails setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"staticListId"] forKey:TRANSACTION_DATA_TRANSACTION_TOPUP_ID];
        [otherDetails setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"staticListValue"] forKey:TRANSACTION_DATA_TRANSACTION_TOPUP_TYPE];
        
        NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
        if (pWebServiceData.PaymentDetails1)
            [sta setObject:pWebServiceData.PaymentDetails1 forKey:@"updatedBalance"];
        
        [sta setObject:[NSDate date]forKey:@"balanceDate"];
        
        [sta setObject:[NSDate date]forKey:@"miniStatementDate"];
        
        [sta synchronize];

        NSLog(@"OtherDetails : %@",otherDetails);

        if ([requestDictinary objectForKey:@"PaymentDetails2"]) {
            [otherDetails setObject:[requestDictinary objectForKey:@"PaymentDetails2"] forKey:TRANSACTION_DATA_TRANSACTION_OPERATOR_ID];
        }
        
        NSMutableArray *transActionHistoryArrayList = [[NSMutableArray alloc] init];
        [transActionHistoryArrayList addObject:otherDetails];

        NSError *dbError = [databaseManager recordInsertionIntoTransactionDetailsWithData:transActionHistoryArrayList];
        pWebServiceData.TransactionType = @"TopUp";
        [self addMiniStatement:pWebServiceData];
        
        if (dbError.code == 1)
        {
            NSLog(@"success %@",dbError.userInfo);
            [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
        }
        else
        {
            NSLog(@"Error %@",dbError.userInfo);
            [delegate errorCodeHandlingInDBWithCode:@"-1" withProcessorCode:pWebServiceData.ProcessorCode];
        }
    }
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

-(void)processTopUpDTH:(WebServiceDataObject *)pWebServiceData
{
    //Storing to data base to be implemented
    NSLog(@"Webservice Response processing : %@",pWebServiceData);
    
    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
    NSMutableArray *localDataArray = [[NSMutableArray alloc] initWithArray:[databaseManager getAllTransactionHistory]];
    
    BOOL isInseratable = NO;
    
    if ([localDataArray count] > 0)
    {
        for (int index = 0; index < [localDataArray count]; index++)
        {
            if (![pWebServiceData.OpsTransactionId isEqualToString:[[localDataArray objectAtIndex:index] objectForKey:TRANSACTION_DATA_TRANSACTION_ID]])
            {
                isInseratable = YES;
            }
        }
    }
    else
        isInseratable = YES;
    
    if (isInseratable)
    {
        NSMutableDictionary *otherDetails = [[NSMutableDictionary alloc] init];
        [otherDetails setObject:pWebServiceData.CustomerPhoneNumber forKey:TRANSACTION_DATA_TRANSACTION_SENDER_PHONE_NUMBER];
        [otherDetails setObject:pWebServiceData.FeeAmount forKey:TRANSACTION_DATA_TRANSACTION_FEE];
        [otherDetails setObject:pWebServiceData.ProcessorCode forKey:TRANSACTION_DATA_PROCCESSOR_CODE];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_TIME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_FULL_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_NICK_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_FIRST_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_LAST_NAME];
        [otherDetails setObject:pWebServiceData.PaymentDetails2 forKey:TRANSACTION_DATA_TRANSACTION_DTH_TYPE_NAME];
        [otherDetails setObject:pWebServiceData.PaymentDetails3 forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FULL_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FIRST_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_LAST_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_MESSAGE];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_BUSINESS_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_AGENT_BUSINESS_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_BUSINESS_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS1];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS2];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS3];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_CITY];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_PINCODE];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_COUNTRY];
        [otherDetails setObject:[NSString stringWithFormat:@"%d",(int)[localDataArray count]+1] forKey:TRANSACTION_DATA_COLOUMN_ID];
        [otherDetails setObject:pWebServiceData.TransactionType forKey:TRANSACTION_DATA_TRANSACTION_TYPE];
        [otherDetails setObject:pWebServiceData.TxnAmount forKey:TRANSACTION_DATA_TRANSACITON_AMOUNT];
        [otherDetails setObject:pWebServiceData.TransactionDate forKey:TRANSACTION_DATA_TRANSACTION_DATE];
        [otherDetails setObject:pWebServiceData.OpsTransactionId forKey:TRANSACTION_DATA_TRANSACTION_ID];
        [otherDetails setObject:pWebServiceData.TxnStatus forKey:TRANSACTION_DATA_TRANSACTION_STATUS];
        
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"staticListId"])
        {
         [otherDetails setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"staticListId"] forKey:TRANSACTION_DATA_TRANSACTION_TOPUP_ID];
        }
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"staticListValue"])
        {
              [otherDetails setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"staticListValue"] forKey:TRANSACTION_DATA_TRANSACTION_TOPUP_TYPE];
        }
       
      
        [otherDetails setObject:[requestDictinary objectForKey:PARAMETER18] forKey:TRANSACTION_DATA_TRANSACTION_OPERATOR_NAME];
        if ([requestDictinary objectForKey:PARAMETER18]) {
            [otherDetails setObject:[requestDictinary objectForKey:PARAMETER18] forKey:TRANSACTION_DATA_TRANSACTION_OPERATOR_ID];
        }
        if([[requestDictinary objectForKey:@"DTH_TYPE_typeDesc"]length]==0)
        {
            [otherDetails setObject:[requestDictinary objectForKey:@"DTH_TYPE_typeName"] forKey:TRANSACTION_DATA_TRANSACTION_DTH_TYPE_NAME];
        }
        else
        {
            [otherDetails setObject:[requestDictinary objectForKey:@"DTH_TYPE_typeDesc"] forKey:TRANSACTION_DATA_TRANSACTION_DTH_TYPE_NAME];
        }
        
        
        if ([requestDictinary objectForKey:PARAMETER35]) {
            [otherDetails setObject:[requestDictinary objectForKey:PARAMETER35] forKey:TRANSACTION_DATA_TRANSACTION_DTH_TYPE_ID];
        }
        NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
        if (pWebServiceData.PaymentDetails1)
            [sta setObject:pWebServiceData.PaymentDetails1 forKey:@"updatedBalance"];
        
        [sta setObject:[NSDate date] forKey:@"balanceDate"];
        [sta setObject:[NSDate date]forKey:@"miniStatementDate"];

        [sta synchronize];
        
        
        NSLog(@"PaymentDetails1: %@",pWebServiceData.PaymentDetails1); // Balance
        NSLog(@"otherDetails: %@",otherDetails); // Balance
        
        NSMutableArray *transActionHistoryArrayList = [[NSMutableArray alloc] init];
        [transActionHistoryArrayList addObject:otherDetails];
        
        NSError *dbError = [databaseManager recordInsertionIntoTransactionDetailsWithData:transActionHistoryArrayList];
        
        pWebServiceData.TransactionType = @"TopUp";
        [self addMiniStatement:pWebServiceData];
        
        if (dbError.code == 1)
        {
            NSLog(@"success %@",dbError.userInfo);
            [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
        }
        else
        {
            NSLog(@"Error %@",dbError.userInfo);
            [delegate errorCodeHandlingInDBWithCode:@"-1" withProcessorCode:pWebServiceData.ProcessorCode];
        }
    }
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

-(void)processBankMMIDResponse:(WebServiceDataObject *)pWebServiceData
{
    NSLog(@"Transaction ID : %@",pWebServiceData);
    
    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
    
    NSMutableArray *localDataArray = [[NSMutableArray alloc] initWithArray:[databaseManager getAllTransactionHistory]];
    
    
    BOOL isInseratable = NO;
    
    if ([localDataArray count] > 0)
    {
        for (int index = 0; index < [localDataArray count]; index++)
        {
            if (![pWebServiceData.OpsTransactionId isEqualToString:[[localDataArray objectAtIndex:index] objectForKey:TRANSACTION_DATA_TRANSACTION_ID]])
            {
                isInseratable = YES;
            }
        }
    }
    else
        isInseratable = YES;
    
    if (isInseratable)
    {
        NSMutableDictionary *otherDetails = [[NSMutableDictionary alloc] init];
        
        [otherDetails setObject:pWebServiceData.CustomerPhoneNumber forKey:TRANSACTION_DATA_TRANSACTION_SENDER_PHONE_NUMBER];
        [otherDetails setObject:pWebServiceData.ProcessorCode forKey:TRANSACTION_DATA_PROCCESSOR_CODE];

        [otherDetails setObject:pWebServiceData.SenderFirstName forKey:TRANSACTION_DATA_TRANSACTION_SENDER_FIRST_NAME];
        [otherDetails setObject:pWebServiceData.SenderLastName forKey:TRANSACTION_DATA_TRANSACTION_SENDER_LAST_NAME];
        [otherDetails setObject:pWebServiceData.Reference1 forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER];
        [otherDetails setObject:pWebServiceData.PaymentDetails2 forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FULL_NAME];

        [otherDetails setObject:[NSString stringWithFormat:@"%d",(int)[localDataArray count]+1] forKey:TRANSACTION_DATA_COLOUMN_ID];
        [otherDetails setObject:pWebServiceData.TransactionType forKey:TRANSACTION_DATA_TRANSACTION_TYPE];
        [otherDetails setObject:pWebServiceData.TxnAmount forKey:TRANSACTION_DATA_TRANSACITON_AMOUNT];
        [otherDetails setObject:pWebServiceData.TransactionDate forKey:TRANSACTION_DATA_TRANSACTION_DATE];
        [otherDetails setObject:pWebServiceData.OpsTransactionId forKey:TRANSACTION_DATA_TRANSACTION_ID];
        [otherDetails setObject:pWebServiceData.TxnStatus forKey:TRANSACTION_DATA_TRANSACTION_STATUS];
    
        [otherDetails setObject:pWebServiceData.PaymentDetails4 forKey:TRANSACTION_DATA_BANK_MMID];
        [otherDetails setObject:pWebServiceData.PaymentDetails3 forKey:TRANSACTION_DATA_BANK_NAME];
        
        if ([requestDictinary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME]) {
            [otherDetails setObject:[requestDictinary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME] forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME];
        }
        
        NSLog(@"PaymentDetails1: %@",pWebServiceData.PaymentDetails1);
        
        NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
        if (pWebServiceData.PaymentDetails1)
            [sta setObject:pWebServiceData.PaymentDetails1 forKey:@"updatedBalance"];
        
        [sta setObject:[NSDate date] forKey:@"balanceDate"];
        [sta setObject:[NSDate date]forKey:@"miniStatementDate"];
        
        [sta synchronize];
        
        NSLog(@"otherDetails: %@",otherDetails);
        
        NSMutableArray *transActionHistoryArrayList = [[NSMutableArray alloc] init];
        [transActionHistoryArrayList addObject:otherDetails];
        
        NSError *dbError = [databaseManager recordInsertionIntoTransactionDetailsWithData:transActionHistoryArrayList];
        pWebServiceData.TransactionType = @"Bank";
        [self addMiniStatement:pWebServiceData];
        if (dbError.code == 1)
        {
            NSLog(@"success %@",dbError.userInfo);
            [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
        }
        else
        {
            NSLog(@"Error %@",dbError.userInfo);
            [delegate errorCodeHandlingInDBWithCode:@"-1" withProcessorCode:pWebServiceData.ProcessorCode];
        }
    }
    
    
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

-(void)processBankIFSCIMPSResponse:(WebServiceDataObject *)pWebServiceData
{
    NSLog(@"Transaction ID : %@",pWebServiceData);
    
    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
    
    NSMutableArray *localDataArray = [[NSMutableArray alloc] initWithArray:[databaseManager getAllTransactionHistory]];
    NSLog(@"localDataArray %@ \n localDataArray count %d",localDataArray,(int)[localDataArray count]);
    
    BOOL isInseratable = NO;
    
    if ([localDataArray count] > 0)
    {
        for (int index = 0; index < [localDataArray count]; index++)
        {
            if (![pWebServiceData.OpsTransactionId isEqualToString:[[localDataArray objectAtIndex:index] objectForKey:TRANSACTION_DATA_TRANSACTION_ID]])
            {
                isInseratable = YES;
            }
        }
    }
    else
        isInseratable = YES;
    
    if (isInseratable)
    {
        NSMutableDictionary *otherDetails = [[NSMutableDictionary alloc] init];
        
        [otherDetails setObject:pWebServiceData.CustomerPhoneNumber forKey:TRANSACTION_DATA_TRANSACTION_SENDER_PHONE_NUMBER];
        [otherDetails setObject:pWebServiceData.ProcessorCode forKey:TRANSACTION_DATA_PROCCESSOR_CODE];
        [otherDetails setObject:pWebServiceData.SenderFirstName forKey:TRANSACTION_DATA_TRANSACTION_SENDER_FIRST_NAME];
        [otherDetails setObject:pWebServiceData.SenderLastName forKey:TRANSACTION_DATA_TRANSACTION_SENDER_LAST_NAME];
        [otherDetails setObject:pWebServiceData.PaymentDetails2 forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FULL_NAME];
        [otherDetails setObject:[NSString stringWithFormat:@"%d",(int)[localDataArray count]+1] forKey:TRANSACTION_DATA_COLOUMN_ID];
        [otherDetails setObject:pWebServiceData.TransactionType forKey:TRANSACTION_DATA_TRANSACTION_TYPE];
        [otherDetails setObject:pWebServiceData.TxnAmount forKey:TRANSACTION_DATA_TRANSACITON_AMOUNT];
        [otherDetails setObject:pWebServiceData.TransactionDate forKey:TRANSACTION_DATA_TRANSACTION_DATE];
        [otherDetails setObject:pWebServiceData.OpsTransactionId forKey:TRANSACTION_DATA_TRANSACTION_ID];
        [otherDetails setObject:pWebServiceData.TxnStatus forKey:TRANSACTION_DATA_TRANSACTION_STATUS];
        [otherDetails setObject:pWebServiceData.Reference1 forKey:TRANSACTION_DATA_TRANSACTION_ACCOUNT_NUMBER];
        [otherDetails setObject:pWebServiceData.PaymentDetails4 forKey:TRANSACTION_DATA_BANK_IFSC_CODE];
        [otherDetails setObject:pWebServiceData.PaymentDetails3 forKey:TRANSACTION_DATA_BANK_NAME];

        if ([requestDictinary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME]) {
            [otherDetails setObject:[requestDictinary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME] forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME];
        }
        
        NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
        if (pWebServiceData.PaymentDetails1)
            [sta setObject:pWebServiceData.PaymentDetails1 forKey:@"updatedBalance"];
        
            [sta setObject:[NSDate date] forKey:@"balanceDate"];
            [sta setObject:[NSDate date]forKey:@"miniStatementDate"];
        
        [sta synchronize];
        
        NSMutableArray *transActionHistoryArrayList = [[NSMutableArray alloc] init];
        [transActionHistoryArrayList addObject:otherDetails];
        
        NSError *dbError = [databaseManager recordInsertionIntoTransactionDetailsWithData:transActionHistoryArrayList];
        pWebServiceData.TransactionType = @"Bank";
        [self addMiniStatement:pWebServiceData];
        
        if (dbError.code == 1)
        {
            NSLog(@"success %@",dbError.userInfo);
            [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
        }
        else
        {
            NSLog(@"Error %@",dbError.userInfo);
            [delegate errorCodeHandlingInDBWithCode:@"-1" withProcessorCode:pWebServiceData.ProcessorCode];
        }
    }

    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

-(void)processBankNEFTResponse:(WebServiceDataObject *)pWebServiceData
{
    NSLog(@"Transaction ID : %@",pWebServiceData);

    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
    
    NSMutableArray *localDataArray = [[NSMutableArray alloc] initWithArray:[databaseManager getAllTransactionHistory]];
    NSLog(@"localDataArray %@ \n localDataArray count %d",localDataArray,(int)[localDataArray count]);
    
    BOOL isInseratable = NO;
    
    if ([localDataArray count] > 0)
    {
        for (int index = 0; index < [localDataArray count]; index++)
        {
            if (![pWebServiceData.OpsTransactionId isEqualToString:[[localDataArray objectAtIndex:index] objectForKey:TRANSACTION_DATA_TRANSACTION_ID]])
            {
                isInseratable = YES;
            }
        }
    }
    else
        isInseratable = YES;
    
    if (isInseratable)
    {
        NSMutableDictionary *otherDetails = [[NSMutableDictionary alloc] init];
        
        [otherDetails setObject:pWebServiceData.CustomerPhoneNumber forKey:TRANSACTION_DATA_TRANSACTION_SENDER_PHONE_NUMBER];
        [otherDetails setObject:pWebServiceData.SenderFirstName forKey:TRANSACTION_DATA_TRANSACTION_SENDER_FIRST_NAME];
        [otherDetails setObject:pWebServiceData.SenderLastName forKey:TRANSACTION_DATA_TRANSACTION_SENDER_LAST_NAME];
        [otherDetails setObject:pWebServiceData.ProcessorCode forKey:TRANSACTION_DATA_PROCCESSOR_CODE];
        [otherDetails setObject:pWebServiceData.PaymentDetails2 forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FULL_NAME];
        [otherDetails setObject:[NSString stringWithFormat:@"%d",(int)[localDataArray count]+1] forKey:TRANSACTION_DATA_COLOUMN_ID];
        [otherDetails setObject:pWebServiceData.TransactionType forKey:TRANSACTION_DATA_TRANSACTION_TYPE];
        [otherDetails setObject:pWebServiceData.TxnAmount forKey:TRANSACTION_DATA_TRANSACITON_AMOUNT];
        [otherDetails setObject:pWebServiceData.TransactionDate forKey:TRANSACTION_DATA_TRANSACTION_DATE];
        [otherDetails setObject:pWebServiceData.OpsTransactionId forKey:TRANSACTION_DATA_TRANSACTION_ID];
        [otherDetails setObject:pWebServiceData.TxnStatus forKey:TRANSACTION_DATA_TRANSACTION_STATUS];
        [otherDetails setObject:pWebServiceData.Reference1 forKey:TRANSACTION_DATA_TRANSACTION_ACCOUNT_NUMBER];
        [otherDetails setObject:pWebServiceData.PaymentDetails4 forKey:TRANSACTION_DATA_BANK_IFSC_CODE];
        [otherDetails setObject:pWebServiceData.PaymentDetails3 forKey:TRANSACTION_DATA_BANK_NAME];
        if ([requestDictinary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME]) {
            [otherDetails setObject:[requestDictinary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME] forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME];
        }
        
        NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
        if (pWebServiceData.PaymentDetails1)
            [sta setObject:pWebServiceData.PaymentDetails1 forKey:@"updatedBalance"];
        
            [sta setObject:[NSDate date] forKey:@"balanceDate"];
        [sta setObject:[NSDate date]forKey:@"miniStatementDate"];
    
        
        
        [sta synchronize];

        NSMutableArray *transActionHistoryArrayList = [[NSMutableArray alloc] init];
        [transActionHistoryArrayList addObject:otherDetails];
        
        NSError *dbError = [databaseManager recordInsertionIntoTransactionDetailsWithData:transActionHistoryArrayList];
        pWebServiceData.TransactionType = @"Bank";
        [self addMiniStatement:pWebServiceData];
        
        if (dbError.code == 1)
        {
            NSLog(@"success %@",dbError.userInfo);
            [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
        }
        else
        {
            NSLog(@"Error %@",dbError.userInfo);
            [delegate errorCodeHandlingInDBWithCode:@"-1" withProcessorCode:pWebServiceData.ProcessorCode];
        }
    }

    
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to store the login user details in the application
 * object.
 *
 * @param pWebServiceData
 *            represents the response object.
 */

-(void)processLoginResponse:(WebServiceDataObject *)pWebServiceData
{
    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];

    NSArray *loginData = [databaseManager getAllLoginInformation];
    NSLog(@"loginData: %@",loginData);
    
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"userLoginStatus"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"User Login Status : %ld",(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"userLoginStatus"]);
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [sta setValue:@"1" forKey:@"userLogged"];
    [sta synchronize];
    
    if ([loginData count]>0 && [[[loginData objectAtIndex:0] objectForKey:LOGIN_USER_PHONE_NUMBER] isEqualToString:pWebServiceData.CustomerPhoneNumber])
    {
        NSMutableDictionary *userDetails = [[NSMutableDictionary alloc] init];
        [userDetails setObject:pWebServiceData.SenderFirstName forKey:PARAMETER11];
        
        if(pWebServiceData.SenderLastName.length>0)
            [userDetails setObject:pWebServiceData.SenderLastName forKey:PARAMETER12];
        else
            [userDetails setObject:@"" forKey:PARAMETER12];
        
        [userDetails setObject:pWebServiceData.SenderLastName forKey:PARAMETER12];
        [userDetails setObject:pWebServiceData.CustomerPhoneNumber forKey:PARAMETER9];
        [userDetails setObject:pWebServiceData.TransactionDate forKey:PARAMETER32];
        [userDetails setObject:pWebServiceData.Reference3 forKey:PARAMETER36];
        [userDetails setObject:pWebServiceData.Reference2 forKey:PARAMETER35];
        [userDetails setObject:pWebServiceData.CurrencyType forKey:PARAMETER22];

        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

        [userDefaults setInteger:1 forKey:@"userLoginStatus"];
        [userDefaults setObject:userDetails forKey:@"userDetails"];
        [userDefaults setObject:[NSDate date] forKey:@"balanceDate"];
        [userDefaults setInteger:1 forKey:@"miniStatementStatus"];
        
        if(pWebServiceData.PaymentDetails4)
        {
            [userDefaults setObject:pWebServiceData.PaymentDetails4 forKey:userLanguage];
        }
        if (pWebServiceData.PaymentDetails1) {
            [userDefaults setObject:pWebServiceData.PaymentDetails1 forKey:@"updatedBalance"];
        }
        [userDefaults synchronize];
        
        [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
    }
    else
    {
        if ([loginData count]>0)
        {
            NSArray *finalTranArray = [[NSArray alloc] initWithArray:[databaseManager getAllLoginInformation]];
            NSDictionary *myDictionary = [finalTranArray objectAtIndex:0];
            NSString *strImagePath = [myDictionary valueForKey:LOGIN_USER_IMAGE_PATH];
            
            if(strImagePath.length > 0)
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:strImagePath];
                if ([[NSFileManager defaultManager]fileExistsAtPath:imagePath])
                {
                    [[NSFileManager defaultManager] removeItemAtPath:imagePath error:nil];
                }
            }
            
            [databaseManager deleteAllDetailsFromTransactionHistory];
            [databaseManager deleteAllDetailsFromLoginDetails];
            [databaseManager deleteAllDetailsFromMiniStatement];
            [databaseManager deleteAllDetailsPayBills];
//            [databaseManager deleteAllDetailsFromDropDownTimeStampDetails];
            [databaseManager deleteAllPayBillsTransactionHistory];
        }
        
        NSMutableDictionary *userDetailsForDB = [[NSMutableDictionary alloc] init];
        [userDetailsForDB setObject:[NSString stringWithFormat:@"%d",(int)[loginData count]+1] forKey:LOGIN_USER_INFORMATION_COLOUMN_ID];
        [userDetailsForDB setObject:pWebServiceData.SenderFirstName forKey:LOGIN_USER_FIRST_NAME];
        [userDetailsForDB setObject:pWebServiceData.SenderLastName forKey:LOGIN_USER_LAST_NAME];
        [userDetailsForDB setObject:pWebServiceData.CustomerPhoneNumber forKey:LOGIN_USER_PHONE_NUMBER];
        [userDetailsForDB setObject:pWebServiceData.TransactionDate forKey:LOGIN_USER_UPDATED_TIME];
        [userDetailsForDB setObject:pWebServiceData.Reference2 forKey:LOGIN_USER_IS_ACTIVATED];
        [userDetailsForDB setObject:pWebServiceData.Reference3 forKey:LOGIN_USER_PROFILE_ID];
        [userDetailsForDB setObject:@"" forKey:LOGIN_USER_IMAGE_PATH];
        
        NSLog(@"Userdeatils data...%@",[userDetailsForDB description]);
        
        NSArray *login_responseArray = [[NSArray alloc] initWithObjects:userDetailsForDB, nil];
        NSError *dbError1 = [databaseManager recordInsertionIntoLoginUserDetailsWithData:login_responseArray];
        NSLog(@"dbError1: %@",dbError1.description);
        
        NSMutableDictionary *userDetails = [[NSMutableDictionary alloc] init];
        [userDetails setObject:pWebServiceData.SenderFirstName forKey:PARAMETER11];
        [userDetails setObject:pWebServiceData.SenderLastName forKey:PARAMETER12];
        [userDetails setObject:pWebServiceData.CustomerPhoneNumber forKey:PARAMETER9];
        [userDetails setObject:pWebServiceData.TransactionDate forKey:PARAMETER32];
        [userDetails setObject:pWebServiceData.Reference3 forKey:PARAMETER36];
        NSLog(@"Profile From Webservice Response : %@",[userDetails objectForKey:PARAMETER36]);
        [userDetails setObject:pWebServiceData.Reference2 forKey:PARAMETER35];
        [userDetails setObject:pWebServiceData.PaymentDetails1 forKey:PARAMETER17];
        [userDetails setObject:pWebServiceData.CurrencyType forKey:PARAMETER22];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:userDetails forKey:@"userDetails"];
        [userDefaults setObject:[NSDate date] forKey:@"balanceDate"];
        [userDefaults removeObjectForKey:@"miniStatementDate"];
        [userDefaults setInteger:1 forKey:@"miniStatementStatus"];
      
        if (pWebServiceData.PaymentDetails1) {
            [userDefaults setObject:pWebServiceData.PaymentDetails1 forKey:@"updatedBalance"];
        }
        
        NSLog(@"Webservice Response Language : %@",pWebServiceData.PaymentDetails4);
        
        if(pWebServiceData.PaymentDetails4)
        {
            [userDefaults setObject:pWebServiceData.PaymentDetails4 forKey:userLanguage];
        }
        
        [userDefaults synchronize];
        
        [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
    }
}

/**
 * Method that is used to store the updated balance in the application
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processBalanceEnquiryResponse:(WebServiceDataObject *)pWebServiceData
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if (pWebServiceData.PaymentDetails1) {
        [userDefaults setObject:pWebServiceData.PaymentDetails1 forKey:@"updatedBalance"];
    }
    [userDefaults setObject:[NSDate date] forKey:@"balanceDate"];
    [userDefaults synchronize];
    
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to store the login user details in the application
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processMiniStatementResponse:(WebServiceDataObject *)pWebServiceData
{
    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
    
    NSMutableArray *localDataArray = [[NSMutableArray alloc] initWithArray:[databaseManager getAllMiniStatementDetails]];
    NSLog(@"localDataArray %@",localDataArray);

    if ([localDataArray count] > 0)
    {
        [databaseManager deleteAllDetailsFromMiniStatement];
    }
    NSMutableDictionary *otherDetails = [[NSMutableDictionary alloc] init];
    
    NSString *miniStatementResponse = pWebServiceData.Reference1;
    
    NSArray *transactionHistoryRecords = [miniStatementResponse componentsSeparatedByString:@";"];
    int noOfRecords = (int)[transactionHistoryRecords count];
    NSMutableArray *transActionHistoryArrayList = [[NSMutableArray alloc] init];
    NSMutableDictionary *tempDictionary;
    
    for (int index = 0; index < noOfRecords; index++)
    {
        tempDictionary = [[NSMutableDictionary alloc] initWithDictionary:otherDetails];
        
        NSMutableArray *transactionFileds = (NSMutableArray *)[[transactionHistoryRecords objectAtIndex:index] componentsSeparatedByString:@"|"];
        NSLog(@"Transaction Fields Are..%@",transactionFileds);
        
        if ([transactionFileds count] > 1)
        {
            [tempDictionary setObject:[NSString stringWithFormat:@"%d",index+1] forKey:MINI_STATEMENT_COLOUMN_ID];
            
            if ([transactionFileds objectAtIndex:0])
                [tempDictionary setObject:[transactionFileds objectAtIndex:0] forKey:MINI_STATEMENT_TRANSACTION_TYPE];
            else
                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_TYPE];
            
            if ([transactionFileds objectAtIndex:1])
                [tempDictionary setObject:[transactionFileds objectAtIndex:1] forKey:MINI_STATEMENT_TRANSACTION_ID];
            else
                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_ID];

            if ([transactionFileds objectAtIndex:2])
                [tempDictionary setObject:[transactionFileds objectAtIndex:2] forKey:MINI_STATEMENT_TRANSACTION_AMOUNT];
            else
                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_AMOUNT];
            
            if ([transactionFileds objectAtIndex:3])
                [tempDictionary setObject:[transactionFileds objectAtIndex:3] forKey:MINI_STATEMENT_TRANSACTION_FEE];
            else
                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_FEE];

            if ([transactionFileds objectAtIndex:4])
                [tempDictionary setObject:[transactionFileds objectAtIndex:4] forKey:MINI_STATEMENT_TRANSACTION_DATE];
            else
                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_DATE];

            if ([transactionFileds objectAtIndex:5])
                [tempDictionary setObject:[transactionFileds objectAtIndex:5] forKey:MINI_STATEMENT_TRANSACTION_STATUS];
            else
                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_STATUS];

            if ([transactionFileds count]>6)
            {
                if ([transactionFileds objectAtIndex:6])
                {
                    NSArray *tempArray = [[NSArray alloc] initWithArray:[[transactionFileds objectAtIndex:6] componentsSeparatedByString:@":"]];
                    if ([[tempArray objectAtIndex:0] isEqualToString:@""])
                        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL1];
                    else
                        [tempDictionary setObject:[tempArray objectAtIndex:0] forKey:MINI_STATEMENT_DYNAMIC_LABEL1];
                    
                    if ([[tempArray objectAtIndex:1] isEqualToString:@""])
                        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE1];
                    else
                        [tempDictionary setObject:[tempArray objectAtIndex:1] forKey:MINI_STATEMENT_DYNAMIC_VALUE1];
                }
                else
                {
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL1];
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE1];
                }
            }

            if ([transactionFileds count]>7)
            {
                if ([transactionFileds objectAtIndex:7])
                {
                    NSArray *tempArray = [[NSArray alloc] initWithArray:[[transactionFileds objectAtIndex:7] componentsSeparatedByString:@":"]];
                    if ([[tempArray objectAtIndex:0] isEqualToString:@""])
                        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL2];
                    else
                        [tempDictionary setObject:[tempArray objectAtIndex:0] forKey:MINI_STATEMENT_DYNAMIC_LABEL2];
                    
                    if ([[tempArray objectAtIndex:1] isEqualToString:@""])
                        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE2];
                    else
                        [tempDictionary setObject:[tempArray objectAtIndex:1] forKey:MINI_STATEMENT_DYNAMIC_VALUE2];
                }
                else
                {
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL2];
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE2];
                }
                if ([transactionFileds objectAtIndex:8])
                {
                    NSArray *tempArray = [[NSArray alloc] initWithArray:[[transactionFileds objectAtIndex:8] componentsSeparatedByString:@":"]];
                    
                    if ([[tempArray objectAtIndex:0] isEqualToString:@""])
                        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL3];
                    else
                        [tempDictionary setObject:[tempArray objectAtIndex:0] forKey:MINI_STATEMENT_DYNAMIC_LABEL3];

                    if ([[tempArray objectAtIndex:1] isEqualToString:@""])
                        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE3];
                    else
                        [tempDictionary setObject:[tempArray objectAtIndex:1] forKey:MINI_STATEMENT_DYNAMIC_VALUE3];
                }
                else
                {
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL3];
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE3];
                }

                if ([transactionFileds objectAtIndex:9])
                {
                    NSArray *tempArray = [[NSArray alloc] initWithArray:[[transactionFileds objectAtIndex:9] componentsSeparatedByString:@":"]];
                    if ([[tempArray objectAtIndex:0] isEqualToString:@""])
                        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL4];
                    else
                        [tempDictionary setObject:[tempArray objectAtIndex:0] forKey:MINI_STATEMENT_DYNAMIC_LABEL4];
                    
                    if ([[tempArray objectAtIndex:1] isEqualToString:@""])
                        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE4];
                    else
                        [tempDictionary setObject:[tempArray objectAtIndex:1] forKey:MINI_STATEMENT_DYNAMIC_VALUE4];
                }
                else
                {
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL4];
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE4];
                }
            }

            [transActionHistoryArrayList addObject:tempDictionary];
        }
    }

    NSLog(@"transActionHistoryArrayList: %@",transActionHistoryArrayList);
    
    if ([transActionHistoryArrayList count]>0)
    {
        NSError *dbError = [databaseManager recordInsertionIntoMiniStatementDetailsWithData:transActionHistoryArrayList];
        if (dbError.code == 1)
        {
            NSLog(@"success %@",dbError.userInfo);
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:[NSDate date] forKey:@"miniStatementDate"];
            //[userDefaults setObject:[NSDate date] forKey:@"balanceDate"];
            [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
        }
        else
        {
            NSLog(@"Error %@",dbError.userInfo);
            [delegate errorCodeHandlingInDBWithCode:@"-1" withProcessorCode:pWebServiceData.ProcessorCode];
        }
    }
    else
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:[NSDate date] forKey:@"miniStatementDate"];
        //[userDefaults setObject:[NSDate date] forKey:@"balanceDate"];
        //balanceDate
        [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
    }
}

//add MiniStatement
-(void) addMiniStatement :(WebServiceDataObject *)pWebServiceData
{
    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
    NSMutableArray *transActionHistoryArrayList = [[NSMutableArray alloc] init];
    NSMutableDictionary *tempDictionary;
    tempDictionary = [[NSMutableDictionary alloc] init];

    if (pWebServiceData.TransactionType)
        [tempDictionary setObject:pWebServiceData.TransactionType forKey:MINI_STATEMENT_TRANSACTION_TYPE];
    else
        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_TYPE];

    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_ID];
    
    if (pWebServiceData.TxnAmount)
        [tempDictionary setObject:pWebServiceData.TxnAmount forKey:MINI_STATEMENT_TRANSACTION_AMOUNT];
    else
        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_AMOUNT];
    
    if (pWebServiceData.FeeAmount)
        [tempDictionary setObject:pWebServiceData.FeeAmount forKey:MINI_STATEMENT_TRANSACTION_FEE];
    else
        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_FEE];
    
    if (pWebServiceData.TransactionDate)
    {
        NSString *date = [NSString stringWithFormat:@"%@",pWebServiceData.TransactionDate];
        NSString *finalTimeStr = [ValidationsClass convertTorequiredFormatte:date withFormate:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"application_date_and_time_format",@"GeneralSettings",[NSBundle mainBundle], nil)]];
        finalTimeStr = [finalTimeStr substringToIndex:[finalTimeStr length] - 3];
        [tempDictionary setObject:finalTimeStr forKey:MINI_STATEMENT_TRANSACTION_DATE];
    }
    else
        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_DATE];

    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_STATUS];


    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL1];
    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE1];


    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL2];
    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE2];

    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL3];
    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE3];


    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL4];
    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE4];

    [transActionHistoryArrayList addObject:tempDictionary];

    NSLog(@"Transaction History Array List : %@",transActionHistoryArrayList);
    
    NSError *dbError = [databaseManager recordInsertionIntoMiniStatementDetailsWithData:transActionHistoryArrayList];
    
    if (dbError.code == 1)
    {
        NSLog(@"success %@",dbError.userInfo);
    }
    else
    {
        NSLog(@"Error %@",dbError.userInfo);
    }
}


/**
 * Method that is used to store the updated balance in the application
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processFetchFeeResponse:(WebServiceDataObject *)pWebServiceData
{
    NSString *feeAmount = pWebServiceData.FeeAmount;
    NSLog(@"feeAmount: %@",feeAmount);
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}


-(void)processFetchFeeResponse_T3:(WebServiceDataObject *)pWebServiceData
{
    NSString *feeAmount = pWebServiceData.FeeAmount;
    NSLog(@"feeAmount: %@",feeAmount);
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform Send Mony New Operation
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processP2PResponse:(WebServiceDataObject *)pWebServiceData
{
    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
    
    NSMutableArray *localDataArray = [[NSMutableArray alloc] initWithArray:[databaseManager getAllTransactionHistory]];
    
    BOOL isInseratable = NO;
    
    if ([localDataArray count] > 0)
    {
        for (int index = 0; index < [localDataArray count]; index++)
        {
            if (![pWebServiceData.OpsTransactionId isEqualToString:[[localDataArray objectAtIndex:index] objectForKey:TRANSACTION_DATA_TRANSACTION_ID]])
            {
                isInseratable = YES;
            }
        }
    }
    else
        isInseratable = YES;
    
    if (isInseratable)
    {
        NSMutableDictionary *otherDetails = [[NSMutableDictionary alloc] init];
        
        [otherDetails setObject:pWebServiceData.CustomerPhoneNumber forKey:TRANSACTION_DATA_TRANSACTION_SENDER_PHONE_NUMBER];
        [otherDetails setObject:pWebServiceData.FeeAmount forKey:TRANSACTION_DATA_TRANSACTION_FEE];
        [otherDetails setObject:pWebServiceData.ProcessorCode forKey:TRANSACTION_DATA_PROCCESSOR_CODE];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_TIME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_FULL_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_NICK_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_FIRST_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_LAST_NAME];
        [otherDetails setObject:pWebServiceData.PaymentDetails2 forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FULL_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FIRST_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_LAST_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_MESSAGE];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_BUSINESS_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_AGENT_BUSINESS_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_BUSINESS_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS1];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS2];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS3];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_CITY];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_PINCODE];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_COUNTRY];
        [otherDetails setObject:[NSString stringWithFormat:@"%d",(int)[localDataArray count]+1] forKey:TRANSACTION_DATA_COLOUMN_ID];
        [otherDetails setObject:pWebServiceData.TransactionType forKey:TRANSACTION_DATA_TRANSACTION_TYPE];
        [otherDetails setObject:pWebServiceData.TxnAmount forKey:TRANSACTION_DATA_TRANSACITON_AMOUNT];
        [otherDetails setObject:pWebServiceData.TransactionDate forKey:TRANSACTION_DATA_TRANSACTION_DATE];
        [otherDetails setObject:pWebServiceData.OpsTransactionId forKey:TRANSACTION_DATA_TRANSACTION_ID];
        [otherDetails setObject:pWebServiceData.TxnStatus forKey:TRANSACTION_DATA_TRANSACTION_STATUS];
        
        if ([requestDictinary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME]) {
            NSString *responseStr=[requestDictinary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME];
            if ([responseStr  compare:application_default_no_value_available] != NSOrderedSame) {
                [otherDetails setObject:[requestDictinary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME] forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME];
            }
        }
        
        NSLog(@"PaymentDetails1: %@",pWebServiceData.PaymentDetails1); // Balance
        
        NSUserDefaults *sta=[NSUserDefaults standardUserDefaults];
        if (pWebServiceData.PaymentDetails1) {
            [sta setObject:pWebServiceData.PaymentDetails1 forKey:@"updatedBalance"];
        }
        [sta setObject:[NSDate date]  forKey:@"balanceDate"];
        [sta setObject:[NSDate date]forKey:@"miniStatementDate"];
        [sta synchronize];
        
        NSLog(@"otherDetails: %@",otherDetails);
        
        NSMutableArray *transActionHistoryArrayList = [[NSMutableArray alloc] init];
        [transActionHistoryArrayList addObject:otherDetails];
    
        NSError *dbError = [databaseManager recordInsertionIntoTransactionDetailsWithData:transActionHistoryArrayList];
        pWebServiceData.TransactionType = @"Send Money";
        [self addMiniStatement:pWebServiceData];
        
        if (dbError.code == 1)
        {
            NSLog(@"success %@",dbError.userInfo);
            [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
        }
        else
        {
            NSLog(@"Error %@",dbError.userInfo);
            [delegate errorCodeHandlingInDBWithCode:@"-1" withProcessorCode:pWebServiceData.ProcessorCode];
        }
    }
}

/**
 * Method that is used to perform DropDownResponse
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processDropDownResponse:(WebServiceDataObject *)pWebServiceData
{
    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];

    NSString *appendState = pWebServiceData.PaymentDetails1;
    NSData *jsonData = [appendState dataUsingEncoding:NSUTF8StringEncoding];
    NSError *jsonError;
    NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError];
    
    NSString *typeName = @"typeName";
    NSString *typeDesc = @"typeDesc";
    
    NSUserDefaults *dropDownTypes = [NSUserDefaults standardUserDefaults];
    NSMutableArray *drodownNamesArray =  [[NSMutableArray alloc] initWithArray:[dropDownTypes objectForKey:@"dropDownTypes"]];
    NSMutableArray *dropdown_detailsArray = [[NSMutableArray alloc] init];
    int k = 0;

    for (NSString *key in drodownNamesArray)
    {
        [databaseManager deleteFromDropDownValueWithType:key];
    }
    
    int totalDocs = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"totalDocuments"];
    
    for (NSString *key in drodownNamesArray)
    {
        if ([dataDict objectForKey:key])
        {
            NSArray *internalArray = [[NSArray alloc] initWithArray:[dataDict objectForKey:key]];
            for (int i=0; i<[internalArray count]; i++)
            {
                if (totalDocs == 0) {
                    if ([key isEqualToString:@"DKYC_DOC_TYPE"]) {
                        [[NSUserDefaults standardUserDefaults] setInteger:([[NSUserDefaults standardUserDefaults] integerForKey:@"totalDocuments"] + 1) forKey:@"totalDocuments"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                }
            
                NSMutableDictionary *dropdown_detailsDict = [[NSMutableDictionary alloc] init];
                [dropdown_detailsDict setObject:key forKey:DROP_DOWN_TYPE];
                [dropdown_detailsDict setObject:[[internalArray objectAtIndex:i] valueForKey:typeName] forKey:DROP_DOWN_TYPE_NAME];
                if([[internalArray objectAtIndex:i] valueForKey:typeDesc])
                    [dropdown_detailsDict setObject:[[internalArray objectAtIndex:i] valueForKey:typeDesc] forKey:DROP_DOWN_TYPE_DESC];
                else
                [dropdown_detailsDict setObject:@"" forKey:DROP_DOWN_TYPE_DESC];
                [dropdown_detailsArray addObject:dropdown_detailsDict];
                [databaseManager updateTableDataInDropDownTimeStamp:key withStatus:@"1"];
                k++;
            }
        }
    }
    
    NSError *dbError = [databaseManager recordInsertionIntoDropDownDetailsWithData:dropdown_detailsArray];
    if (dbError.code == 1)
    {
        NSLog(@"Success %@",dbError.userInfo);
    }
    else
    {
        NSLog(@"Failed %@",dbError.userInfo);
    }
    
    if ([requestDictinary objectForKey:@"dropDownString"]) {
        [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
    }
}

/**
 * Method that is used to perform Activation Response
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processActivationResponse:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform SignUp Response
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processSignUpResponse:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform forgot Reset MPIN Response
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processforgotResetMPINResponse:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform Change MPIN Response
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processChangeMPINResponse:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform AgentDetails Response
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processAgentDetailsResponse:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform PendingBills Response
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processPendingBillsResponse:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform FetchPayBills Response
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processFetchPayBillsResponse:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform CashOutAgetWithoutOTP Operation
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processCashOutAgetWithoutOTPResponse:(WebServiceDataObject *)pWebServiceData
{
    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];

    NSMutableArray *localDataArray = [[NSMutableArray alloc] initWithArray:[databaseManager getAllTransactionHistory]];
    
    BOOL isInseratable = NO;
    
    
    if ([localDataArray count] > 0)
    {
        for (int index = 0; index < [localDataArray count]; index++)
        {
            if (![pWebServiceData.OpsTransactionId isEqualToString:[[localDataArray objectAtIndex:index] objectForKey:TRANSACTION_DATA_TRANSACTION_ID]])
            {
                isInseratable = YES;
            }
        }
    }
    else
        isInseratable = YES;
    
    if (isInseratable)
    {
        NSMutableDictionary *otherDetails = [[NSMutableDictionary alloc] init];
        
        [otherDetails setObject:pWebServiceData.CustomerPhoneNumber forKey:TRANSACTION_DATA_TRANSACTION_SENDER_PHONE_NUMBER];
        [otherDetails setObject:pWebServiceData.FeeAmount forKey:TRANSACTION_DATA_TRANSACTION_FEE];
        [otherDetails setObject:pWebServiceData.ProcessorCode forKey:TRANSACTION_DATA_PROCCESSOR_CODE];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_TIME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_FULL_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_NICK_NAME];
        [otherDetails setObject:pWebServiceData.SenderFirstName forKey:TRANSACTION_DATA_TRANSACTION_SENDER_FIRST_NAME];
        [otherDetails setObject:pWebServiceData.SenderLastName forKey:TRANSACTION_DATA_TRANSACTION_SENDER_LAST_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FULL_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FIRST_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_LAST_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_MESSAGE];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_BUSINESS_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_AGENT_BUSINESS_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_BUSINESS_NAME];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS1];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS2];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS3];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_CITY];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_PINCODE];
        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_COUNTRY];
        [otherDetails setObject:[NSString stringWithFormat:@"%d",(int)[localDataArray count]+1] forKey:TRANSACTION_DATA_COLOUMN_ID];
        [otherDetails setObject:pWebServiceData.TransactionType forKey:TRANSACTION_DATA_TRANSACTION_TYPE];
        [otherDetails setObject:pWebServiceData.TxnAmount forKey:TRANSACTION_DATA_TRANSACITON_AMOUNT];
        [otherDetails setObject:pWebServiceData.TransactionDate forKey:TRANSACTION_DATA_TRANSACTION_DATE];
        [otherDetails setObject:pWebServiceData.OpsTransactionId forKey:TRANSACTION_DATA_TRANSACTION_ID];
        [otherDetails setObject:pWebServiceData.TxnStatus forKey:TRANSACTION_DATA_TRANSACTION_STATUS];
        
        NSLog(@"PaymentDetails4: %@",pWebServiceData.PaymentDetails4); // Balance
        
        NSUserDefaults *sta=[NSUserDefaults standardUserDefaults];
        
        if (pWebServiceData.PaymentDetails4)
            [sta setObject:pWebServiceData.PaymentDetails4 forKey:@"updatedBalance"];
        
        [sta setObject:[NSDate date]  forKey:@"balanceDate"];
        [sta setObject:[NSDate date]forKey:@"miniStatementDate"];
        [sta synchronize];
        
        NSLog(@"otherDetails: %@",otherDetails); // Balance
        
        NSMutableArray *transActionHistoryArrayList = [[NSMutableArray alloc] init];
        [transActionHistoryArrayList addObject:otherDetails];
        
        NSError *dbError = [databaseManager recordInsertionIntoTransactionDetailsWithData:transActionHistoryArrayList];
        pWebServiceData.TransactionType = @"Cashout";
        [self addMiniStatement:pWebServiceData];
        if (dbError.code == 1)
        {
            // Needed to check PLS un comment
            // NSLog(@"success %@",dbError.userInfo);
            [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
        }
        else
        {
            // Needed to check PLS un comment
            // NSLog(@"Error %@",dbError.userInfo);
            [delegate errorCodeHandlingInDBWithCode:@"-1" withProcessorCode:pWebServiceData.ProcessorCode];
        }
    }
}

/**
 * Method that is used to perform ChangeLanguage Response
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processChangeLanguageResponse:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform PayBillByNickName Operation
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processPayBillByNickNameResponse:(WebServiceDataObject *)pWebServiceData
{
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    NSDictionary *dataDict = [[NSDictionary alloc] initWithDictionary:[sta objectForKey:@"PayBillsByNickName"]];
    
    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
    
    NSArray *tempMiniArray = [databaseManager getAllPayBillTransactionDetails];
    NSMutableDictionary *tempMiniDict = [[NSMutableDictionary alloc] init];
    [tempMiniDict setObject:[NSString stringWithFormat:@"%d",(int)[tempMiniArray count]+1] forKey:PAYBILL_TRANSACTION_COLOUMN_ID];
    [tempMiniDict setObject:PAY_BILL forKey:PAYBILL_TRANSACTION_TYPE];
    [tempMiniDict setObject:pWebServiceData.ProcessorCode forKey:PAYBILL_TRANSACTION_PROCESSOR_CODE];
    [tempMiniDict setObject:pWebServiceData.TxnAmount forKey:PAYBILL_TRANSACTION_AMOUNT];
    [tempMiniDict setObject:pWebServiceData.OpsTransactionId forKey:PAYBILL_TRANSACTION_ID];
    [tempMiniDict setObject:pWebServiceData.TransactionDate forKey:PAYBILL_TRANSACTION_DATE];
    [tempMiniDict setObject:TRANSACTION_STATUS forKey:PAYBILL_TRANSACTION_STATUS];
    
    if ([dataDict objectForKey:TRANSACTION_DATA_TRANSACTION_FEE])
        [tempMiniDict setObject:[dataDict objectForKey:TRANSACTION_DATA_TRANSACTION_FEE] forKey:PAYBILL_TRANSACTION_FEE];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_FEE];
    
    if ([dataDict objectForKey:BILLERNICKNAME])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERNICKNAME] forKey:PAYBILL_TRANSACTION_BILLERNICKNAME];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERNICKNAME];
    
    if ([dataDict objectForKey:BILLERNAME])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERNAME] forKey:PAYBILL_TRANSACTION_BILLERNAME];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERNAME];
    
    if ([dataDict objectForKey:PAYBILL_TRANSACTION_BILLERTYPE])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERTYPE] forKey:PAYBILL_TRANSACTION_BILLERTYPE];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERTYPE];

    if ([dataDict objectForKey:BILLERCATEGORY])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERCATEGORY] forKey:PAYBILL_TRANSACTION_BILLERCATEGORY];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERCATEGORY];
    
    if ([dataDict objectForKey:BILLERID])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERID] forKey:PAYBILL_TRANSACTION_BILLERID];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERID];
    
    if ([dataDict objectForKey:BILLERDUEDATE])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERDUEDATE] forKey:PAYBILL_TRANSACTION_BILLERDUEDATE];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERDUEDATE];
    
    if ([dataDict objectForKey:BILLERSHORTNAME])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERSHORTNAME] forKey:PAYBILL_TRANSACTION_BILLERSHORTNAME];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERSHORTNAME];
    
    if ([dataDict objectForKey:BILLERMASTERID])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERMASTERID] forKey:PAYBILL_TRANSACTION_BILLERMASTERID];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERMASTERID];
 
    if ([dataDict objectForKey:BILLERMEMBERBILLERID])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERMEMBERBILLERID] forKey:PAYBILL_TRANSACTION_BILLERMEMBERBILLERID];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERMEMBERBILLERID];

    if ([dataDict objectForKey:BILLERLOCATION])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERLOCATION] forKey:PAYBILL_TRANSACTION_BILLERLOCATION];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERLOCATION];

    if ([dataDict objectForKey:BILLERA1])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERA1] forKey:PAYBILL_TRANSACTION_BILLERA1];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERA1];

    if ([dataDict objectForKey:BILLERA2])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERA2] forKey:PAYBILL_TRANSACTION_BILLERA2];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERA2];

    if ([dataDict objectForKey:BILLERA3])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERA3] forKey:PAYBILL_TRANSACTION_BILLERA3];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERA3];

    if ([dataDict objectForKey:BILLERA4])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERA4] forKey:PAYBILL_TRANSACTION_BILLERA4];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERA4];

    if ([dataDict objectForKey:BILLERA5])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERA5] forKey:PAYBILL_TRANSACTION_BILLERA5];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERA5];

    if ([dataDict objectForKey:BILLERREFERENCE1])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERREFERENCE1] forKey:PAYBILL_TRANSACTION_BILLERREFERENCE1];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERREFERENCE1];

    if ([dataDict objectForKey:BILLERREFERENCE2])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERREFERENCE2] forKey:PAYBILL_TRANSACTION_BILLERREFERENCE2];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERREFERENCE2];

    if ([dataDict objectForKey:BILLERREFERENCE3])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERREFERENCE3] forKey:PAYBILL_TRANSACTION_BILLERREFERENCE3];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERREFERENCE3];

    if ([dataDict objectForKey:BILLERREFERENCE4])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERREFERENCE4] forKey:PAYBILL_TRANSACTION_BILLERREFERENCE4];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERREFERENCE4];

    if ([dataDict objectForKey:BILLERREFERENCE5])
        [tempMiniDict setObject:[dataDict objectForKey:BILLERREFERENCE4] forKey:PAYBILL_TRANSACTION_BILLERREFERENCE5];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLERREFERENCE5];

    if ([dataDict objectForKey:BILLPAYCODE])
        [tempMiniDict setObject:[dataDict objectForKey:BILLPAYCODE] forKey:PAYBILL_TRANSACTION_BILLPAYCODE];
    else
        [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_BILLPAYCODE];

    [tempMiniDict setObject:@"" forKey:PAYBILL_TRANSACTION_TIME];
    // Needed to check PLS un comment
    // NSLog(@"tempMiniDict: %@",tempMiniDict);
    NSArray *miniArray = [[NSArray alloc] initWithObjects:tempMiniDict,nil];
    NSError *dbError = [databaseManager recordInsertionIntoPayBillTransactionDetailsWithData:miniArray];

    if (dbError.code == 1)
    {
        // Needed to check PLS un comment
        // NSLog(@"success %@",dbError.userInfo);
        [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
    }
    else
    {
        // Needed to check PLS un comment
        // NSLog(@"Error %@",dbError.userInfo);
        [delegate errorCodeHandlingInDBWithCode:@"-1" withProcessorCode:pWebServiceData.ProcessorCode];
    }
}

/**
 * Method that is used to perform Fetch My Billers Operation
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processPayBillFetchMyBillersResponse:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform Fetch My Payee Operation
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processMyPayeeResponse:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform Fetch My Billers Operation
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processFetchAllBillersBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform Add Billers Operation
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processAddBillersBundle:(WebServiceDataObject *)pWebServiceData
{
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"PayBillNewBillerAdded" object:self userInfo:nil];
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform Check Velocity Limit Operation
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processCheckVelocityBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform Pay Merchant Operation
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processPayMerchantBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
    
}

/**
 * Method that is used to perform Add Payee Operation
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processAddPayeeBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform View Payee Operation
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processViewPayeeBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
-(void)processFetchIFSC:(WebServiceDataObject *)pWebServiceData
{
    NSUserDefaults *bankDefaults = [NSUserDefaults standardUserDefaults];
    if (pWebServiceData.PaymentDetails2) {
        [bankDefaults setValue:pWebServiceData.PaymentDetails2 forKey:@"IFSC"];
    }
    [bankDefaults synchronize];

    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform Launch API Operation
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processLaunchAPIBundle:(WebServiceDataObject *)pWebServiceData
{
    NSLog(@"Processing..");
    NSMutableArray *launchAPI_responseArray;
    NSMutableArray * downloadArray = [[NSMutableArray alloc] init];
    NSMutableArray * dbUpdateArray = [[NSMutableArray alloc] init];

    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
    NSLog(@"databaseManager initialized..");
    NSArray *launchAPIDataArr = [databaseManager getAllLaunchAPIInformation];
    NSLog(@"Obtained allLaunchAPIInformation..%@", launchAPIDataArr);

    // JSON  Image DATA
    NSData *jsonData = [((pWebServiceData.Reference2)?pWebServiceData.Reference2:@"") dataUsingEncoding:NSUTF8StringEncoding];
    NSError *jsonError;
    NSArray *dataDict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError];
    launchAPI_responseArray=[[NSMutableArray alloc]init];
    NSLog(@"Serialized json..");
    for (int i=0 ; i < dataDict.count; i++)
    {
        NSMutableDictionary *userDetailsForDB = [[NSMutableDictionary alloc] init];
        [userDetailsForDB setObject:[NSNumber numberWithInt:i+1] forKey:@"_id"];
        [userDetailsForDB setObject:[dataDict objectAtIndex:i] forKey:MARKET_IMAGE_SERVER_URLS];
        [userDetailsForDB setObject:[NSString stringWithFormat:@"Retina%d.png",i+1] forKey:MARKET_IMAGE_LOCAL_URLS];
        [launchAPI_responseArray addObject:userDetailsForDB];
    }    
    if (launchAPIDataArr.count > 0)
    {
        for (int i=0; i < launchAPI_responseArray.count; i++) {
            [downloadArray addObject:[[launchAPI_responseArray objectAtIndex:i] objectForKey:MARKET_IMAGE_SERVER_URLS]];
            [dbUpdateArray addObject:[launchAPI_responseArray objectAtIndex:i]];
        }
        for (int j=0; j < downloadArray.count; j++) {
            for (int i=0; i<launchAPIDataArr.count; i++) {
                if ([[[launchAPIDataArr objectAtIndex:i] objectForKey:MARKET_IMAGE_SERVER_URLS] isEqualToString:[downloadArray objectAtIndex:j]]) {
                    [downloadArray removeObjectAtIndex:j];
                    [dbUpdateArray removeObjectAtIndex:j];
                }
            }
        }
        for (int i=0; i<dbUpdateArray.count; i++)
        {
            [databaseManager updateTableDataInLaunchAPIDetailsWithServerURL:[[dbUpdateArray objectAtIndex:i] objectForKey:MARKET_IMAGE_LOCAL_URLS] forServerURL:[[dbUpdateArray objectAtIndex:i] objectForKey:MARKET_IMAGE_SERVER_URLS]];
        }
    }
    else
    {
        [databaseManager recordInsertionIntoLaunchAPIUserDetailsWithData:launchAPI_responseArray];
        for(int i = 0 ; i < launchAPI_responseArray.count ; i++) {
            [downloadArray addObject:[[launchAPI_responseArray objectAtIndex:i] objectForKey:MARKET_IMAGE_SERVER_URLS]];
        }
    }
    // Image  download Data.
    dispatch_queue_t serialQueue = dispatch_queue_create("Obopay Serial Queue", DISPATCH_QUEUE_SERIAL);
    
    // Image Down Load Block
    
    dispatch_sync(serialQueue,
    ^{
          NSData *imageData;
        
          // Image From ImagePath.
          NSString *imagePathName;
          for (int i=0; i<[downloadArray count]; i++)
          {
              imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[downloadArray objectAtIndex:i]]];
              NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
              NSString *documentsDirectory = [paths objectAtIndex:0];
              NSString *imageDirectory = [documentsDirectory stringByAppendingString:@"/SplashImages"];
              if(![[NSFileManager defaultManager]fileExistsAtPath:imageDirectory])
              {
                  [[NSFileManager defaultManager]createDirectoryAtPath:imageDirectory withIntermediateDirectories:NO attributes:nil error:nil];
              }
              imagePathName = [[downloadArray objectAtIndex:i] lastPathComponent]; /*Uncomment this when API works*/
              imagePathName= [imageDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Retina%d.png",i+1]];
              [imageData writeToFile:imagePathName atomically:YES];
          }
    });

    NSArray *dataDict1 = [NSJSONSerialization JSONObjectWithData:[pWebServiceData.PaymentDetails2 dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&jsonError];
    NSString *NAME = @"nm";
    NSString *TIMESTAMP = @"ts";
    NSLog(@"Serialized PD2..%@", dataDict1);
    NSArray *local_dropdown_timestampArray = [[NSArray alloc] initWithArray:[databaseManager getAllDropDownTimeStapDetails]];
    
    NSMutableArray *dropDownDetailsArray = [[NSMutableArray alloc] init];
    
    //Webservice Data parsing
    NSMutableArray *dropdown_timestampArray = [[NSMutableArray alloc] init];
    for (int i=0; i<[dataDict1 count]; i++)
    {
        NSMutableDictionary *dropdown_timestampDict = [[NSMutableDictionary alloc] init];
        [dropdown_timestampDict setObject:[[dataDict1 objectAtIndex:i] valueForKey:NAME] forKey:DROPDOWN_TIMESTAMP_DROPDOWN_NAME];
        [dropdown_timestampDict setObject:[[dataDict1 objectAtIndex:i] valueForKey:TIMESTAMP] forKey:DROPDOWN_TIMESTAMP_TIMESTAMP];
        [dropdown_timestampDict setObject:@"0" forKey:DROPDOWN_TIMESTAMP_ISUPDATED];
        [dropdown_timestampArray addObject:dropdown_timestampDict];
    }
    
    //For State list Hardcoding
    NSMutableDictionary *dropdown_timestampDict = [[NSMutableDictionary alloc] init];
    [dropdown_timestampDict setObject:@"STATE_LIST" forKey:DROPDOWN_TIMESTAMP_DROPDOWN_NAME];
    [dropdown_timestampDict setObject:@""  forKey:DROPDOWN_TIMESTAMP_TIMESTAMP];
    [dropdown_timestampDict setObject:@"0" forKey:DROPDOWN_TIMESTAMP_ISUPDATED];
    [dropdown_timestampArray addObject:dropdown_timestampDict];

    //do only if dropdown timestamp table is empty
    if (local_dropdown_timestampArray.count == 0)
    {
    
        //Adding Dropdown types for the first time if the table is empty
        [databaseManager recordInsertionIntoDropDownTimeStampWithData:dropdown_timestampArray];
        local_dropdown_timestampArray = [[NSArray alloc] initWithArray:[databaseManager getAllDropDownTimeStapDetails]];
        
        //Finding for dropdown types which has status of 0
        for (int j = 0; j < local_dropdown_timestampArray.count; j++)
        {
//            if ([[[local_dropdown_timestampArray objectAtIndex:j] objectForKey:DROPDOWN_TIMESTAMP_ISUPDATED] isEqualToString:@"0"])
//            {
//                [dropDownDetailsArray addObject:[[local_dropdown_timestampArray objectAtIndex:j] objectForKey:DROPDOWN_TIMESTAMP_DROPDOWN_NAME]];
//            }
            
            [dropDownDetailsArray addObject:[[local_dropdown_timestampArray objectAtIndex:j] objectForKey:DROPDOWN_TIMESTAMP_DROPDOWN_NAME]];
        }
    }
    else
    {
        //Count - 1(Excluding State from dropdown list as its hardcoded)
        for (int j = 0; j < local_dropdown_timestampArray.count-1; j++)
        {
//            if ([[[local_dropdown_timestampArray objectAtIndex:j] objectForKey:DROPDOWN_TIMESTAMP_ISUPDATED] isEqualToString:@"0"] || !([[[local_dropdown_timestampArray objectAtIndex:j] objectForKey:DROPDOWN_TIMESTAMP_TIMESTAMP] isEqualToString:[[dropdown_timestampArray objectAtIndex:j] objectForKey:DROPDOWN_TIMESTAMP_TIMESTAMP]]))
//            {
//                [dropDownDetailsArray addObject:[[local_dropdown_timestampArray objectAtIndex:j] objectForKey:DROPDOWN_TIMESTAMP_DROPDOWN_NAME]];
//            }
            
            [dropDownDetailsArray addObject:[[local_dropdown_timestampArray objectAtIndex:j] objectForKey:DROPDOWN_TIMESTAMP_DROPDOWN_NAME]];
        }
    }
 
    NSUserDefaults *dropDownTypes = [NSUserDefaults standardUserDefaults];
    [dropDownTypes setObject:dropDownDetailsArray forKey:@"dropDownTypes"];
    [dropDownTypes synchronize];
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform BankAggregatorList
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processBankAggregatorList:(WebServiceDataObject *)pWebServiceData{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
    
    NSUserDefaults *sta=[NSUserDefaults standardUserDefaults];
    if (pWebServiceData.PaymentDetails4)
        [sta setObject:pWebServiceData.PaymentDetails4 forKey:@"updatedBalance"];
    
    [sta setObject:[NSDate date]  forKey:@"balanceDate"];
    [sta setObject:[NSDate date]forKey:@"miniStatementDate"];
    [sta synchronize];
    
}
/**
 * Method that is used to perform PayUApiBumdle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processPayUApiBumdle:(WebServiceDataObject *)pWebServiceData{
        [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform SignUPDKYCAPIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processSignUPDKYCAPIBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform UpdateDeviceIDAPI
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processUpdateDeviceIDBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform ProductDetailsAPI
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */

-(void)processProductDetailsAPIBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

#pragma mark - Response Processing for T1,T2 and T3.
/**
 * Method that is used to perform LoginDetails_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processLoginDetails_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
    NSArray *loginData = [databaseManager getAllLoginInformation];
    NSLog(@"loginData: %@",loginData);
    
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"userLoginStatus"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"User Login Status : %ld",(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"userLoginStatus"]);
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [sta setValue:@"1" forKey:@"userLogged"];
    [sta synchronize];
    
    if ([loginData count]>0 && [[[loginData objectAtIndex:0] objectForKey:LOGIN_USER_PHONE_NUMBER] isEqualToString:pWebServiceData.Tier3AgentId])
    {
        NSMutableDictionary *userDetails = [[NSMutableDictionary alloc] init];
        [userDetails setObject:pWebServiceData.Tier3AgentId forKey:PARAMETER6];
        [userDetails setObject:pWebServiceData.Tier3AgentId forKey:PARAMETER9];
        [userDetails setObject:pWebServiceData.SenderFirstName forKey:PARAMETER11];
        if(pWebServiceData.SenderLastName.length>0)
            [userDetails setObject:pWebServiceData.SenderLastName forKey:PARAMETER12];
        else
            [userDetails setObject:@"" forKey:PARAMETER12];
//        [userDetails setObject:pWebServiceData.SenderLastName forKey:PARAMETER12];
        [userDetails setObject:pWebServiceData.TransactionDate forKey:PARAMETER32];
        [userDetails setObject:pWebServiceData.Reference3 forKey:PARAMETER36];
        [userDetails setObject:pWebServiceData.Reference2 forKey:PARAMETER35];
       // [userDetails setObject:pWebServiceData.CurrencyType forKey:PARAMETER22];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        [userDefaults setInteger:1 forKey:@"userLoginStatus"];
        
        [userDefaults setObject:userDetails forKey:@"userDetails"];
        [userDefaults setObject:[NSDate date] forKey:@"balanceDate"];
        [userDefaults setInteger:1 forKey:@"miniStatementStatus"];
        
        if(pWebServiceData.PaymentDetails4)
        {
            [userDefaults setObject:pWebServiceData.PaymentDetails4 forKey:userLanguage];
        }
        if (pWebServiceData.PaymentDetails1) {
            [userDefaults setObject:pWebServiceData.PaymentDetails1 forKey:@"updatedBalance_T3"];
        }
        [userDefaults synchronize];
       
    }
    else
    {
        if ([loginData count]>0)
        {
            NSArray *finalTranArray = [[NSArray alloc] initWithArray:[databaseManager getAllLoginInformation]];
            NSDictionary *myDictionary = [finalTranArray objectAtIndex:0];
            NSString *strImagePath = [myDictionary valueForKey:LOGIN_USER_IMAGE_PATH];
            
            if(strImagePath.length > 0)
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:strImagePath];
                if ([[NSFileManager defaultManager]fileExistsAtPath:imagePath])
                {
                    [[NSFileManager defaultManager] removeItemAtPath:imagePath error:nil];
                }
            }
            
            [databaseManager deleteAllDetailsFromTransactionHistory];
            [databaseManager deleteAllDetailsFromLoginDetails];
            [databaseManager deleteAllDetailsFromMiniStatement];
            [databaseManager deleteAllDetailsPayBills];
            //            [databaseManager deleteAllDetailsFromDropDownTimeStampDetails];
            [databaseManager deleteAllPayBillsTransactionHistory];
        }
        
        NSMutableDictionary *userDetailsForDB = [[NSMutableDictionary alloc] init];
        [userDetailsForDB setObject:[NSString stringWithFormat:@"%d",(int)[loginData count]+1] forKey:LOGIN_USER_INFORMATION_COLOUMN_ID];
        [userDetailsForDB setObject:pWebServiceData.SenderFirstName forKey:LOGIN_USER_FIRST_NAME];
        
        if(pWebServiceData.SenderLastName.length>0)
          [userDetailsForDB setObject:pWebServiceData.SenderLastName forKey:LOGIN_USER_LAST_NAME];
        else
            [userDetailsForDB setObject:@"" forKey:LOGIN_USER_LAST_NAME];
        
        [userDetailsForDB setObject:pWebServiceData.Tier3AgentId forKey:LOGIN_USER_PHONE_NUMBER];
        [userDetailsForDB setObject:pWebServiceData.TransactionDate forKey:LOGIN_USER_UPDATED_TIME];
        [userDetailsForDB setObject:pWebServiceData.Reference2 forKey:LOGIN_USER_IS_ACTIVATED];
        [userDetailsForDB setObject:pWebServiceData.Reference3 forKey:LOGIN_USER_PROFILE_ID];
        [userDetailsForDB setObject:@"" forKey:LOGIN_USER_IMAGE_PATH];
        
        NSArray *login_responseArray = [[NSArray alloc] initWithObjects:userDetailsForDB, nil];
        NSError *dbError1 = [databaseManager recordInsertionIntoLoginUserDetailsWithData:login_responseArray];
       NSLog(@"dbError1: %@",dbError1.description);
        
        NSMutableDictionary *userDetails = [[NSMutableDictionary alloc] init];
        [userDetails setObject:pWebServiceData.SenderFirstName forKey:PARAMETER11];
        
        if(pWebServiceData.SenderLastName.length>0)
            [userDetailsForDB setObject:pWebServiceData.SenderLastName forKey:PARAMETER12];
        else
            [userDetailsForDB setObject:@"" forKey:PARAMETER12];
        
//        [userDetails setObject:pWebServiceData.SenderLastName forKey:PARAMETER12];
        [userDetails setObject:pWebServiceData.Tier3AgentId forKey:PARAMETER6];
        [userDetails setObject:pWebServiceData.Tier3AgentId forKey:PARAMETER9];
        [userDetails setObject:pWebServiceData.TransactionDate forKey:PARAMETER32];
        [userDetails setObject:pWebServiceData.Reference3 forKey:PARAMETER36];
        [userDetails setObject:pWebServiceData.Reference2 forKey:PARAMETER35];
        [userDetails setObject:pWebServiceData.PaymentDetails1 forKey:PARAMETER17];
//        [userDetails setObject:pWebServiceData.CurrencyType forKey:PARAMETER22];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:userDetails forKey:@"userDetails"];
        [userDefaults setObject:[NSDate date] forKey:@"balanceDate"];
        [userDefaults removeObjectForKey:@"miniStatementDate"];
        [userDefaults setInteger:1 forKey:@"miniStatementStatus"];
        
        if (pWebServiceData.PaymentDetails1) {
            [userDefaults setObject:pWebServiceData.PaymentDetails1 forKey:@"updatedBalance_T3"];
        }
        if(pWebServiceData.PaymentDetails4)
        {
            [userDefaults setObject:pWebServiceData.PaymentDetails4 forKey:userLanguage];
        }
        [userDefaults synchronize];
    }
    
    UIWindow *window=[[[UIApplication sharedApplication] delegate] window];
    if ([[sta valueForKey:userLanguage] isEqualToString:ar])
    {
        window.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
    }
    else
    {
        window.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
    }
    
    if (!([pWebServiceData.PaymentDetails4 caseInsensitiveCompare:[sta valueForKey:userLanguage]]==NSOrderedSame))
    {
        AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate] ;
        [appDelegate performSelector:@selector(getDropdownList) withObject:nil afterDelay:1.0];
    }
    
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];

}
/**
 * Method that is used to perform Activation_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processActivation_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform CheckBalnce_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processCheckBalance_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    if (pWebServiceData.PaymentDetails2) {
        [[NSUserDefaults standardUserDefaults] setObject:pWebServiceData.PaymentDetails2 forKey:@"updatedBalance_T3"];
    }
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"balanceDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"Updated Balance is...%@",pWebServiceData.PaymentDetails2);
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];

}
/**
 * Method that is used to perform ChangeLanguage_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processChangeLanguage_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform CheckVelocityLimits_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processCheckVelocityLimits_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform ChangeMPIN_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processChangeMPIN_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform CustomerCashin_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processCustomerCashin_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform GroupCashin_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processGroupCashin_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform CashOutViral_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processCashOutViral_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
  * Method that is used to perform CashoutRegistered_T3APIBundle
  * object.
  *
  * @param pWebServiceResponse
  *            represents the response object.
  */
-(void)processCashoutRegistered_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform CashoutRegistered_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processTransactionStatusDetails_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform ReportsSummary_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processReportsSummary_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform TransactionSummary_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processTransactionSummary_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    NSLog(@"Web service request Data is.....%@",pWebServiceData.PaymentDetails2);

    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform TransactionHistory_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processTransactionHistory_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
    
    NSMutableArray *localDataArray = [[NSMutableArray alloc] initWithArray:[databaseManager getAllMiniStatementDetails]];
    
    if ([localDataArray count] > 0)
    {
        [databaseManager deleteAllDetailsFromMiniStatement];
    }
    NSMutableDictionary *otherDetails = [[NSMutableDictionary alloc] init];
    
    NSString *miniStatementResponse = pWebServiceData.PaymentDetails2;
    
    NSMutableArray *transactionHistoryRecords = [[miniStatementResponse componentsSeparatedByString:@";"] mutableCopy];
   
    if ([transactionHistoryRecords containsObject:@""])
    {
        [transactionHistoryRecords removeObject:@""];
    }
   
    NSLog(@"Record Data %@",transactionHistoryRecords);
    int noOfRecords = (int)[transactionHistoryRecords count];
    
    NSMutableArray *transActionHistoryArrayList = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *tempDictionary;
    
       for (int index = 0; index < noOfRecords; index++)
        {
            tempDictionary = [[NSMutableDictionary alloc] initWithDictionary:otherDetails];
            
            NSMutableArray *transactionFileds =[[[transactionHistoryRecords objectAtIndex:index] componentsSeparatedByString:@"|"] mutableCopy];
        
            NSLog(@"Temp array values are...%@",transactionFileds);
            
            if ([transactionFileds count] > 1)
            {
                [tempDictionary setObject:[NSString stringWithFormat:@"%d",index+1] forKey:MINI_STATEMENT_COLOUMN_ID];
                
                for (int i=0; i<transactionFileds.count; i++)
                {
                    if (i==0)
                    {
                        if ([transactionFileds objectAtIndex:0])
                            [tempDictionary setObject:[transactionFileds objectAtIndex:0] forKey:MINI_STATEMENT_TRANSACTION_TYPE];
                        else
                            [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_TYPE];
                    }
                    
                    if (i==1)
                    {
                        if ([transactionFileds objectAtIndex:1])
                            [tempDictionary setObject:[transactionFileds objectAtIndex:1] forKey:MINI_STATEMENT_TRANSACTION_ID];
                        else
                            [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_ID];
                    }
                    
                    if (i==2)
                    {
                        if ([transactionFileds objectAtIndex:2])
                            [tempDictionary setObject:[transactionFileds objectAtIndex:2] forKey:MINI_STATEMENT_TRANSACTION_AMOUNT];
                        else
                            [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_AMOUNT];
                    }
                    
                    if (i==3)
                    {
                        if ([transactionFileds objectAtIndex:3])
                            [tempDictionary setObject:[transactionFileds objectAtIndex:3] forKey:MINI_STATEMENT_TRANSACTION_FEE];
                        else
                            [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_FEE];
                    }
                    
                    if (i==4)
                    {
                        if ([transactionFileds objectAtIndex:4])
                            [tempDictionary setObject:[transactionFileds objectAtIndex:4] forKey:MINI_STATEMENT_TRANSACTION_DATE];
                        else
                            [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_DATE];
                    }
                    if (i==5)
                    {
                        if ([transactionFileds objectAtIndex:5])
                            [tempDictionary setObject:[transactionFileds objectAtIndex:5] forKey:MINI_STATEMENT_TRANSACTION_STATUS];
                        else
                            [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_STATUS];
                    }
                    
                    if ([transactionFileds count]>6)
                    {
                        if ([transactionFileds objectAtIndex:6])
                        {
                            NSArray *tempArray = [[NSArray alloc] initWithArray:[[transactionFileds objectAtIndex:6] componentsSeparatedByString:@":"]];
                            if ([[tempArray objectAtIndex:0] isEqualToString:@""])
                                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL1];
                            else
                                [tempDictionary setObject:[tempArray objectAtIndex:0] forKey:MINI_STATEMENT_DYNAMIC_LABEL1];
                            
                            if ([[tempArray objectAtIndex:1] isEqualToString:@""])
                                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE1];
                            else
                                [tempDictionary setObject:[tempArray objectAtIndex:1] forKey:MINI_STATEMENT_DYNAMIC_VALUE1];
                        }
                        else
                        {
                            [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL1];
                            [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE1];
                        }
                    }
                    
                    if ([transactionFileds count]>7)
                    {
                        if ([transactionFileds objectAtIndex:7])
                        {
                            NSArray *tempArray = [[NSArray alloc] initWithArray:[[transactionFileds objectAtIndex:7] componentsSeparatedByString:@":"]];
                            if ([[tempArray objectAtIndex:0] isEqualToString:@""])
                                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL2];
                            else
                                [tempDictionary setObject:[tempArray objectAtIndex:0] forKey:MINI_STATEMENT_DYNAMIC_LABEL2];
                            if([tempArray count] >= 2){
                                if ([[tempArray objectAtIndex:1] isEqualToString:@""])
                                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE2];
                                else
                                    [tempDictionary setObject:[tempArray objectAtIndex:1] forKey:MINI_STATEMENT_DYNAMIC_VALUE2];
                            }
                        }
                        else
                        {
                            [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL2];
                            [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE2];
                        }
//                        if ([transactionFileds objectAtIndex:8])
                        if (([transactionFileds count]==9) && [transactionFileds objectAtIndex:8])                        {
                            NSArray *tempArray = [[NSArray alloc] initWithArray:[[transactionFileds objectAtIndex:8] componentsSeparatedByString:@":"]];
                            
                            if ([[tempArray objectAtIndex:0] isEqualToString:@""])
                                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL3];
                            else
                                [tempDictionary setObject:[tempArray objectAtIndex:0] forKey:MINI_STATEMENT_DYNAMIC_LABEL3];
                            if([tempArray count] >=2){
                                if ([[tempArray objectAtIndex:1] isEqualToString:@""])
                                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE3];
                                else
                                    [tempDictionary setObject:[tempArray objectAtIndex:1] forKey:MINI_STATEMENT_DYNAMIC_VALUE3];
                            }
                        }
                        else
                        {
                            [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL3];
                            [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE3];
                        }
                        
                        if (([transactionFileds count]==10) && [transactionFileds objectAtIndex:9])
                        {
                            NSArray *tempArray = [[NSArray alloc] initWithArray:[[transactionFileds objectAtIndex:9] componentsSeparatedByString:@":"]];
                            if ([[tempArray objectAtIndex:0] isEqualToString:@""])
                                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL4];
                            else
                                [tempDictionary setObject:[tempArray objectAtIndex:0] forKey:MINI_STATEMENT_DYNAMIC_LABEL4];
                            if([tempArray count] >=2){
                                if ([[tempArray objectAtIndex:1] isEqualToString:@""])
                                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE4];
                                else
                                    [tempDictionary setObject:[tempArray objectAtIndex:1] forKey:MINI_STATEMENT_DYNAMIC_VALUE4];
                            }
                        }
                        else
                        {
                            [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL4];
                            [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE4];
                        }
                    }
                    
                    
                    
                }
                
                [transActionHistoryArrayList addObject:tempDictionary];
            }
        }
    
        NSLog(@"transActionHistoryArrayList: %@",transActionHistoryArrayList);
    
        if ([transActionHistoryArrayList count]>0)
        {
            NSError *dbError = [databaseManager recordInsertionIntoMiniStatementDetailsWithData:transActionHistoryArrayList];
            if (dbError.code == 1)
            {
                NSLog(@"success %@",dbError.userInfo);
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:[NSDate date] forKey:@"miniStatementDate"];
                [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
            }
            else
            {
                NSLog(@"Error %@",dbError.userInfo);
                [delegate errorCodeHandlingInDBWithCode:@"-1" withProcessorCode:pWebServiceData.ProcessorCode];
            }
        }
    else
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:[NSDate date] forKey:@"miniStatementDate"];
        [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
    }
}

//

/**
 * Method that is used to perform BankTransactionHistory_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
//-(void)processMobileTopUp_T3APIBundle:(WebServiceDataObject *)pWebServiceData
//{
//    NSLog(@"Mobile TopUP Request Data : %@",requestDictinary);
//    //Storing to data base to be implemented
//    
//    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
//    NSMutableArray *localDataArray = [[NSMutableArray alloc] initWithArray:[databaseManager getAllTransactionHistory]];
//    
//    BOOL isInseratable = NO;
//    
//    if ([localDataArray count] > 0)
//    {
//        for (int index = 0; index < [localDataArray count]; index++)
//        {
//            if (![pWebServiceData.OpsTransactionId isEqualToString:[[localDataArray objectAtIndex:index] objectForKey:TRANSACTION_DATA_TRANSACTION_ID]])
//            {
//                isInseratable = YES;
//            }
//        }
//    }
//    else
//        isInseratable = YES;
//    
//    if (isInseratable)
//    {
//        NSMutableDictionary *otherDetails = [[NSMutableDictionary alloc] init];
//        
//        [otherDetails setObject:pWebServiceData.CustomerPhoneNumber forKey:TRANSACTION_DATA_TRANSACTION_SENDER_PHONE_NUMBER];
//        [otherDetails setObject:pWebServiceData.FeeAmount forKey:TRANSACTION_DATA_TRANSACTION_FEE];
//        [otherDetails setObject:pWebServiceData.ProcessorCode forKey:TRANSACTION_DATA_PROCCESSOR_CODE];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_TIME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_FULL_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_NICK_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_FIRST_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_LAST_NAME];
//        [otherDetails setObject:pWebServiceData.PaymentDetails2 forKey:TRANSACTION_DATA_TRANSACTION_OPERATOR_ID];
//        [otherDetails setObject:pWebServiceData.PaymentDetails3 forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FULL_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FIRST_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_LAST_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_MESSAGE];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_BUSINESS_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_AGENT_BUSINESS_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_BUSINESS_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS1];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS2];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS3];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_CITY];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_PINCODE];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_COUNTRY];
//        [otherDetails setObject:[NSString stringWithFormat:@"%d",(int)[localDataArray count]+1] forKey:TRANSACTION_DATA_COLOUMN_ID];
//        [otherDetails setObject:pWebServiceData.TransactionType forKey:TRANSACTION_DATA_TRANSACTION_TYPE];
//        [otherDetails setObject:pWebServiceData.TxnAmount forKey:TRANSACTION_DATA_TRANSACITON_AMOUNT];
//        [otherDetails setObject:pWebServiceData.TransactionDate forKey:TRANSACTION_DATA_TRANSACTION_DATE];
//        [otherDetails setObject:pWebServiceData.OpsTransactionId forKey:TRANSACTION_DATA_TRANSACTION_ID];
//        [otherDetails setObject:pWebServiceData.TxnStatus forKey:TRANSACTION_DATA_TRANSACTION_STATUS];
//        [otherDetails setObject:pWebServiceData.Reference1 forKey:TRANSACTION_DATA_TRANSACTION_OPERATOR_NAME];
//        
//        [otherDetails setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"staticListId"] forKey:TRANSACTION_DATA_TRANSACTION_TOPUP_ID];
//        [otherDetails setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"staticListValue"] forKey:TRANSACTION_DATA_TRANSACTION_TOPUP_TYPE];
//        
//        NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
//        if (pWebServiceData.PaymentDetails1)
//            [sta setObject:pWebServiceData.PaymentDetails1 forKey:@"updatedBalance"];
//        
//        [sta setObject:[NSDate date]forKey:@"balanceDate"];
//        
//        [sta setObject:[NSDate date]forKey:@"miniStatementDate"];
//        
//        [sta synchronize];
//        
//        NSLog(@"OtherDetails : %@",otherDetails);
//        
//        if ([requestDictinary objectForKey:@"PaymentDetails2"]) {
//            [otherDetails setObject:[requestDictinary objectForKey:@"PaymentDetails2"] forKey:TRANSACTION_DATA_TRANSACTION_OPERATOR_ID];
//        }
//        
//        NSMutableArray *transActionHistoryArrayList = [[NSMutableArray alloc] init];
//        [transActionHistoryArrayList addObject:otherDetails];
//        
//        NSError *dbError = [databaseManager recordInsertionIntoTransactionDetailsWithData:transActionHistoryArrayList];
//        pWebServiceData.TransactionType = @"TopUp";
//        [self addMiniStatement:pWebServiceData];
//        
//        if (dbError.code == 1)
//        {
//            NSLog(@"success %@",dbError.userInfo);
//            [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
//        }
//        else
//        {
//            NSLog(@"Error %@",dbError.userInfo);
//            [delegate errorCodeHandlingInDBWithCode:@"-1" withProcessorCode:pWebServiceData.ProcessorCode];
//        }
//    }
//    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
//}

/**
 * Method that is used to perform BankTransactionHistory_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
//-(void)processMobileDTH_T3APIBundle:(WebServiceDataObject *)pWebServiceData
//{
//    //Storing to data base to be implemented
//    NSLog(@"Mobile DTH Response processing : %@",pWebServiceData);
//    
//    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
//    NSMutableArray *localDataArray = [[NSMutableArray alloc] initWithArray:[databaseManager getAllTransactionHistory]];
//    
//    BOOL isInseratable = NO;
//    
//    if ([localDataArray count] > 0)
//    {
//        for (int index = 0; index < [localDataArray count]; index++)
//        {
//            if (![pWebServiceData.OpsTransactionId isEqualToString:[[localDataArray objectAtIndex:index] objectForKey:TRANSACTION_DATA_TRANSACTION_ID]])
//            {
//                isInseratable = YES;
//            }
//        }
//    }
//    else
//        isInseratable = YES;
//    
//    if (isInseratable)
//    {
//        NSMutableDictionary *otherDetails = [[NSMutableDictionary alloc] init];
//        
//        [otherDetails setObject:pWebServiceData.CustomerPhoneNumber forKey:TRANSACTION_DATA_TRANSACTION_SENDER_PHONE_NUMBER];
//        [otherDetails setObject:pWebServiceData.FeeAmount forKey:TRANSACTION_DATA_TRANSACTION_FEE];
//        [otherDetails setObject:pWebServiceData.ProcessorCode forKey:TRANSACTION_DATA_PROCCESSOR_CODE];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_TIME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_FULL_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_NICK_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_FIRST_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_LAST_NAME];
//        [otherDetails setObject:pWebServiceData.PaymentDetails2 forKey:TRANSACTION_DATA_TRANSACTION_DTH_TYPE_NAME];
//        [otherDetails setObject:pWebServiceData.PaymentDetails3 forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FULL_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FIRST_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_LAST_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_MESSAGE];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_SENDER_BUSINESS_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_AGENT_BUSINESS_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_BUSINESS_NAME];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS1];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS2];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_ADDRESS3];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_CITY];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_PINCODE];
//        [otherDetails setObject:@"" forKey:TRANSACTION_DATA_TRANSACTION_COUNTRY];
//        [otherDetails setObject:[NSString stringWithFormat:@"%d",(int)[localDataArray count]+1] forKey:TRANSACTION_DATA_COLOUMN_ID];
//        [otherDetails setObject:pWebServiceData.TransactionType forKey:TRANSACTION_DATA_TRANSACTION_TYPE];
//        [otherDetails setObject:pWebServiceData.TxnAmount forKey:TRANSACTION_DATA_TRANSACITON_AMOUNT];
//        [otherDetails setObject:pWebServiceData.TransactionDate forKey:TRANSACTION_DATA_TRANSACTION_DATE];
//        [otherDetails setObject:pWebServiceData.OpsTransactionId forKey:TRANSACTION_DATA_TRANSACTION_ID];
//        [otherDetails setObject:pWebServiceData.TxnStatus forKey:TRANSACTION_DATA_TRANSACTION_STATUS];
//        
//        [otherDetails setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"staticListId"] forKey:TRANSACTION_DATA_TRANSACTION_TOPUP_ID];
//        [otherDetails setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"staticListValue"] forKey:TRANSACTION_DATA_TRANSACTION_TOPUP_TYPE];
//        
//        [otherDetails setObject:pWebServiceData.Reference1 forKey:TRANSACTION_DATA_TRANSACTION_OPERATOR_NAME];
//        if ([requestDictinary objectForKey:PARAMETER18]) {
//            [otherDetails setObject:[requestDictinary objectForKey:PARAMETER18] forKey:TRANSACTION_DATA_TRANSACTION_OPERATOR_ID];
//        }
//        
//        [otherDetails setObject:pWebServiceData.Reference2 forKey:TRANSACTION_DATA_TRANSACTION_DTH_TYPE_NAME];
//        if ([requestDictinary objectForKey:PARAMETER35]) {
//            [otherDetails setObject:[requestDictinary objectForKey:PARAMETER35] forKey:TRANSACTION_DATA_TRANSACTION_DTH_TYPE_ID];
//        }
//        
//        NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
//        if (pWebServiceData.PaymentDetails1)
//            [sta setObject:pWebServiceData.PaymentDetails1 forKey:@"updatedBalance"];
//        
//        [sta setObject:[NSDate date] forKey:@"balanceDate"];
//        [sta setObject:[NSDate date]forKey:@"miniStatementDate"];
//        
//        [sta synchronize];
//        
//        
//        NSLog(@"PaymentDetails1: %@",pWebServiceData.PaymentDetails1); // Balance
//        NSLog(@"otherDetails: %@",otherDetails); // Balance
//        
//        NSMutableArray *transActionHistoryArrayList = [[NSMutableArray alloc] init];
//        [transActionHistoryArrayList addObject:otherDetails];
//        
//        NSError *dbError = [databaseManager recordInsertionIntoTransactionDetailsWithData:transActionHistoryArrayList];
//        
//        pWebServiceData.TransactionType = @"TopUp";
//        [self addMiniStatement:pWebServiceData];
//        
//        if (dbError.code == 1)
//        {
//            NSLog(@"success %@",dbError.userInfo);
//            [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
//        }
//        else
//        {
//            NSLog(@"Error %@",dbError.userInfo);
//            [delegate errorCodeHandlingInDBWithCode:@"-1" withProcessorCode:pWebServiceData.ProcessorCode];
//        }
//    }
//    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
//}


/**
 * Method that is used to perform BankTransactionHistory_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processBankTransactionHistory_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
    
    NSMutableArray *localDataArray = [[NSMutableArray alloc] initWithArray:[databaseManager getAllMiniStatementDetails]];
    
    if ([localDataArray count] > 0)
    {
        [databaseManager deleteAllDetailsFromMiniStatement];
    }
    NSMutableDictionary *otherDetails = [[NSMutableDictionary alloc] init];
    
    NSString *miniStatementResponse = pWebServiceData.PaymentDetails2;
    
    NSArray *transactionHistoryRecords = [miniStatementResponse componentsSeparatedByString:@";"];
    
    int noOfRecords = (int)[transactionHistoryRecords count];
    
    NSMutableArray *transActionHistoryArrayList = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *tempDictionary;
    
    for (int index = 0; index < noOfRecords; index++)
    {
        tempDictionary = [[NSMutableDictionary alloc] initWithDictionary:otherDetails];
        
        NSMutableArray *transactionFileds = (NSMutableArray *)[[transactionHistoryRecords objectAtIndex:index] componentsSeparatedByString:@"|"];
        
        if ([transactionFileds count] > 1)
        {
            [tempDictionary setObject:[NSString stringWithFormat:@"%d",index+1] forKey:MINI_STATEMENT_COLOUMN_ID];
            
            if ([transactionFileds objectAtIndex:0])
                [tempDictionary setObject:[transactionFileds objectAtIndex:0] forKey:MINI_STATEMENT_TRANSACTION_TYPE];
            else
                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_TYPE];
            
            if ([transactionFileds objectAtIndex:1])
                [tempDictionary setObject:[transactionFileds objectAtIndex:1] forKey:MINI_STATEMENT_TRANSACTION_ID];
            else
                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_ID];
            
            if ([transactionFileds objectAtIndex:2])
                [tempDictionary setObject:[transactionFileds objectAtIndex:2] forKey:MINI_STATEMENT_TRANSACTION_AMOUNT];
            else
                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_AMOUNT];
            
            if ([transactionFileds objectAtIndex:3])
                [tempDictionary setObject:[transactionFileds objectAtIndex:3] forKey:MINI_STATEMENT_TRANSACTION_FEE];
            else
                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_FEE];
            
            if ([transactionFileds objectAtIndex:4])
                [tempDictionary setObject:[transactionFileds objectAtIndex:4] forKey:MINI_STATEMENT_TRANSACTION_DATE];
            else
                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_DATE];
            
            if ([transactionFileds objectAtIndex:5])
                [tempDictionary setObject:[transactionFileds objectAtIndex:5] forKey:MINI_STATEMENT_TRANSACTION_STATUS];
            else
                [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_STATUS];
            
            if ([transactionFileds count]>6)
            {
                if ([transactionFileds objectAtIndex:6])
                {
                    NSArray *tempArray = [[NSArray alloc] initWithArray:[[transactionFileds objectAtIndex:6] componentsSeparatedByString:@":"]];
                    if ([[tempArray objectAtIndex:0] isEqualToString:@""])
                        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL1];
                    else
                        [tempDictionary setObject:[tempArray objectAtIndex:0] forKey:MINI_STATEMENT_DYNAMIC_LABEL1];
                    
                    if ([[tempArray objectAtIndex:1] isEqualToString:@""])
                        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE1];
                    else
                        [tempDictionary setObject:[tempArray objectAtIndex:1] forKey:MINI_STATEMENT_DYNAMIC_VALUE1];
                }
                else
                {
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL1];
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE1];
                }
            }
            
            if ([transactionFileds count]>7)
            {
                if ([transactionFileds objectAtIndex:7])
                {
                    NSArray *tempArray = [[NSArray alloc] initWithArray:[[transactionFileds objectAtIndex:7] componentsSeparatedByString:@":"]];
                    if ([[tempArray objectAtIndex:0] isEqualToString:@""])
                        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL2];
                    else
                        [tempDictionary setObject:[tempArray objectAtIndex:0] forKey:MINI_STATEMENT_DYNAMIC_LABEL2];
                    
                    if ([[tempArray objectAtIndex:1] isEqualToString:@""])
                        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE2];
                    else
                        [tempDictionary setObject:[tempArray objectAtIndex:1] forKey:MINI_STATEMENT_DYNAMIC_VALUE2];
                }
                else
                {
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL2];
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE2];
                }
                if ([transactionFileds objectAtIndex:8])
                {
                    NSArray *tempArray = [[NSArray alloc] initWithArray:[[transactionFileds objectAtIndex:8] componentsSeparatedByString:@":"]];
                    
                    if ([[tempArray objectAtIndex:0] isEqualToString:@""])
                        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL3];
                    else
                        [tempDictionary setObject:[tempArray objectAtIndex:0] forKey:MINI_STATEMENT_DYNAMIC_LABEL3];
                    
                    if ([[tempArray objectAtIndex:1] isEqualToString:@""])
                        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE3];
                    else
                        [tempDictionary setObject:[tempArray objectAtIndex:1] forKey:MINI_STATEMENT_DYNAMIC_VALUE3];
                }
                else
                {
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL3];
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE3];
                }
                
                if ([transactionFileds objectAtIndex:9])
                {
                    NSArray *tempArray = [[NSArray alloc] initWithArray:[[transactionFileds objectAtIndex:9] componentsSeparatedByString:@":"]];
                    if ([[tempArray objectAtIndex:0] isEqualToString:@""])
                        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL4];
                    else
                        [tempDictionary setObject:[tempArray objectAtIndex:0] forKey:MINI_STATEMENT_DYNAMIC_LABEL4];
                    
                    if ([[tempArray objectAtIndex:1] isEqualToString:@""])
                        [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE4];
                    else
                        [tempDictionary setObject:[tempArray objectAtIndex:1] forKey:MINI_STATEMENT_DYNAMIC_VALUE4];
                }
                else
                {
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_LABEL4];
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_DYNAMIC_VALUE4];
                }
            }
            
            [transActionHistoryArrayList addObject:tempDictionary];
        }
    }
    if ([transActionHistoryArrayList count]>0)
    {
        NSError *dbError = [databaseManager recordInsertionIntoMiniStatementDetailsWithData:transActionHistoryArrayList];
        if (dbError.code == 1)
        {
            NSLog(@"success %@",dbError.userInfo);
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:[NSDate date] forKey:@"miniStatementDate"];
            [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
        }
        else
        {
            NSLog(@"Error %@",dbError.userInfo);
            [delegate errorCodeHandlingInDBWithCode:@"-1" withProcessorCode:pWebServiceData.ProcessorCode];
        }
    }
    else
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:[NSDate date] forKey:@"miniStatementDate"];
        [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
    }
}

/**
 * Method that is used to perform BankTransactionHistory_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processBankBalanceDetails_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    NSLog(@"Web service request Data is.....%@",pWebServiceData.PaymentDetails2);

    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform BankTransactionHistory_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processPullPending_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    //NSLog(@"Web service request Data is.....%@",pWebServiceData.PaymentDetails2);
    
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform BankTransactionHistory_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processPullAcceptList_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform BankTransactionHistory_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processPullDecline_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    NSLog(@"Web service request Data is.....%@",pWebServiceData.PaymentDetails2);
    
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform BankTransactionHistory_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processSignUpDetails_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    NSLog(@"Web service request Data is.....%@",pWebServiceData.PaymentDetails2);
    
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform BankTransactionHistory_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processBanksDetails_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    NSLog(@"Web service request Data is.....%@",pWebServiceData.PaymentDetails2);
    
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform BankTransactionHistory_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processUnloadOtherBankDetails_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
//    NSLog(@"Web service request Data is.....%@",pWebServiceData.PaymentDetails2);
    
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
  * Method that is used to perform BankTransactionHistory_T3APIBundle
  * object.
  *
  * @param pWebServiceResponse
  *            represents the response object.
  */
-(void)processUnLoadLinkedBankDetails_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
//    NSLog(@"Web service request Data is.....%@",pWebServiceData.PaymentDetails2);
    
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform BankTransactionHistory_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processLoadBankDetails_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
//    NSLog(@"Web service request Data is.....%@",pWebServiceData.PaymentDetails2);
    
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}

/**
 * Method that is used to perform BankTransactionHistory_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processBankListDetails_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    NSLog(@"BANK Web service request Data is.....%@",pWebServiceData.PaymentDetails2);
    
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
/**
 * Method that is used to perform BankTransactionHistory_T3APIBundle
 * object.
 *
 * @param pWebServiceResponse
 *            represents the response object.
 */
-(void)processForgotMPINDetails_T3APIBundle:(WebServiceDataObject *)pWebServiceData
{
    NSLog(@"Forgot service request Data is.....%@",pWebServiceData.PaymentDetails2);
    
    [delegate processData:pWebServiceData withProcessCode:pWebServiceData.ProcessorCode];
}
@end
