//
//  WebServiceRunning.h
//  Consumer Client
//
//  Created by android on 6/5/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ActivityIndicator.h"
@protocol WebServiceRunningDelegate <NSObject>

@required

-(void)errorCodeHandlingInWebServiceRunningWithErrorCode:(NSString *)errorCode;

@end

@interface WebServiceRunning : NSObject<NSURLConnectionDelegate>
{
    NSMutableData *responseData;
    id senderDelegate;
    ActivityIndicator *activityIndicator;
}

@property (nonatomic, assign) NSObject <WebServiceRunningDelegate> *webServiceRunningDelegate;

-(void)startWebServiceWithRequest:(NSString *)request withReqDictionary:(NSMutableDictionary *)reqDict withDelegate:(id)sendDelegate;

@end
