//
//  WebServiceDataObject.h
//  Consumer Client
//
//  Created by android on 6/4/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebServiceDataObject : NSObject
{
    
}
@property(nonatomic,retain) NSString *GwalTraceId;
@property(nonatomic,retain) NSString *ApiIdType;
@property(nonatomic,retain) NSString *Tier1AgentId;
@property(nonatomic,retain) NSString *Tier1AgentName;
@property(nonatomic,retain) NSString *Tier2AgentId;
@property(nonatomic,retain) NSString *Tier3AgentId;
@property(nonatomic,retain) NSString *Tier1AgentPassword;
@property(nonatomic,retain) NSString *Tier3AgentPassword;
@property(nonatomic,retain) NSString *CustomerPhoneNumber;
@property(nonatomic,retain) NSString *NationalID;
@property(nonatomic,retain) NSString *SenderFirstName;
@property(nonatomic,retain) NSString *SenderLastName;
@property(nonatomic,retain) NSString *TransactionType;
@property(nonatomic,retain) NSString *InstrumentType;
@property(nonatomic,retain) NSString *ProcessorCode;
@property(nonatomic,retain) NSString *CashInAmount;
@property(nonatomic,retain) NSString *PaymentDetails1;
@property(nonatomic,retain) NSString *PaymentDetails2;
@property(nonatomic,retain) NSString *PaymentDetails3;
@property(nonatomic,retain) NSString *PaymentDetails4;
@property(nonatomic,retain) NSString *TxnAmount;
@property(nonatomic,retain) NSString *CurrencyType;
@property(nonatomic,retain) NSString *NetTxnAmount;
@property(nonatomic,retain) NSString *FeeAmount;
@property(nonatomic,retain) NSString *TaxAmount;
@property(nonatomic,retain) NSString *Country;
@property(nonatomic,retain) NSString *RequestId;
@property(nonatomic,retain) NSString *TerminalID;
@property(nonatomic,retain) NSString *TxnStatus;
@property(nonatomic,retain) NSString *ErrorCode;
@property(nonatomic,retain) NSString *OpsTransactionId;
@property(nonatomic,retain) NSString *TransactionDate;
@property(nonatomic,retain) NSString *Remark;
@property(nonatomic,retain) NSString *Reference1;
@property(nonatomic,retain) NSString *Reference2;
@property(nonatomic,retain) NSString *Reference3;
@property(nonatomic,retain) NSString *Reference4;
@property(nonatomic,retain) NSString *Reference5;
@property(nonatomic,retain) NSString *faultCode;
@property(nonatomic,retain) NSString *faultString;

@end
