//
//  WebServiceRequestFormation.h
//  Consumer Client
//
//  Created by android on 6/4/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WebServiceRequestFormation : NSObject {
    float temp;
}

-(NSString *) wrapWebRequestKeyswithValue:(NSMutableDictionary *)pBundle;
-(NSString *)sendRequest:(NSMutableDictionary *)pBundle;

@end
