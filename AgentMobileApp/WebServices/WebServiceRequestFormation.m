//
//  WebServiceRequestFormation.m
//  Consumer Client
//
//  Created by android on 6/4/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "WebServiceRequestFormation.h"
#import "WebServiceConstants.h"
#import "Constants.h"
//#import "Log.h"

@implementation WebServiceRequestFormation

/**
 * Method used to encapsulate web service request
 *
 * @param arrayList
 * @param hashMap
 * @return
 */
-(NSString *)wrapWebRequestKeyswithValue:(NSMutableDictionary *)pBundle
{
    
    NSMutableString *stringBuilder = [[NSMutableString alloc] init];
    NSMutableArray *arrayList = [self populateList];
    
    for (int i = 0; i < [arrayList count]; i++)
    {
        if (i == 0)
        {
            [stringBuilder appendString:keyPrefix1];
            [stringBuilder appendString:[arrayList objectAtIndex:i]];
            [stringBuilder appendString:angBracketClose];
            [stringBuilder appendString:[self nullCheck:[pBundle objectForKey:[arrayList objectAtIndex:i]]]];
            [stringBuilder appendString:keyPrefix3];
            [stringBuilder appendString:[arrayList objectAtIndex:i]];
            [stringBuilder appendString:angBracketClose];
            [stringBuilder appendString:newLine];
        }
        else
        {
            [stringBuilder appendString:keyPrefix];
            [stringBuilder appendString:[arrayList objectAtIndex:i]];
            [stringBuilder appendString:angBracketClose];
            [stringBuilder appendString:[self nullCheck:[pBundle objectForKey:[arrayList objectAtIndex:i]]]];
            [stringBuilder appendString:keyPrefix2];
            [stringBuilder appendString:[arrayList objectAtIndex:i]];
            [stringBuilder appendString:angBracketClose];
            [stringBuilder appendString:newLine];
        }
    }
    
    return stringBuilder;
}

/**
 * Method used to send request to the server
 *
 * @param hashMap
 */
-(NSString *)sendRequest:(NSMutableDictionary *)pBundle
{
    NSMutableString *string = [[NSMutableString alloc] initWithString:requestEnvelopeStartTag];
    [string appendString:newLine];
    [string appendString:requestHeaderTag];
    [string appendString:newLine];
    [string appendString:requestBodyStartTag];
    [string appendString:newLine];
    [string appendString:requestMethodStartTag];
    [string appendString:newLine];
    [string appendString:requestStartTag];
    [string appendString:newLine];
    [string appendString:[self wrapWebRequestKeyswithValue:pBundle]];
    [string appendString:requestEndTag];
    [string appendString:newLine];
    [string appendString:requestMethodEndTag];
    [string appendString:newLine];
    [string appendString:requestBodyEndTag];
    [string appendString:newLine];
    [string appendString:requestEnvelopeEndTag];
    NSLog(@"WEBSERVICE_REQUEST: %@",string);
    if(IS_DEBUGGING){
        if([[pBundle objectForKey:PARAMETER15] isEqualToString:@"0044"]){
            UITextView *txtView = [[UITextView alloc] initWithFrame:CGRectMake(0, 180, SCREEN_WIDTH, 70)];
            NSString *str = [NSString stringWithFormat:@"Webservice request.. ProcessorCode=%@, Request ID:%@", [pBundle objectForKey:PARAMETER15], [pBundle objectForKey:PARAMETER27]];
            [txtView setBackgroundColor:[UIColor whiteColor]];
            [txtView setUserInteractionEnabled:NO];
            [txtView setText:str];
            [txtView accessibilityScroll:UIAccessibilityScrollDirectionUp];
            [[UIApplication sharedApplication].keyWindow addSubview:txtView];
        }
    }
    return string;
}

/**
 * Method used to populate keys of Web Service
 *
 * @return
 */

-(NSMutableArray *)populateList
{
    NSMutableArray *listWithWebSerParam = [[NSMutableArray alloc] init];
    
    [listWithWebSerParam addObject:PARAMETER1];
    [listWithWebSerParam addObject:PARAMETER2];
    [listWithWebSerParam addObject:PARAMETER3];
    [listWithWebSerParam addObject:PARAMETER4];
    [listWithWebSerParam addObject:PARAMETER5];
    [listWithWebSerParam addObject:PARAMETER6];
    [listWithWebSerParam addObject:PARAMETER7];
    [listWithWebSerParam addObject:PARAMETER8];
    [listWithWebSerParam addObject:PARAMETER9];
    [listWithWebSerParam addObject:PARAMETER10];
    [listWithWebSerParam addObject:PARAMETER11];
    [listWithWebSerParam addObject:PARAMETER12];
    [listWithWebSerParam addObject:PARAMETER13];
    [listWithWebSerParam addObject:PARAMETER14];
    [listWithWebSerParam addObject:PARAMETER15];
    [listWithWebSerParam addObject:PARAMETER16];
    [listWithWebSerParam addObject:PARAMETER17];
    [listWithWebSerParam addObject:PARAMETER18];
    [listWithWebSerParam addObject:PARAMETER19];
    [listWithWebSerParam addObject:PARAMETER20];
    [listWithWebSerParam addObject:PARAMETER21];
    [listWithWebSerParam addObject:PARAMETER22];
    [listWithWebSerParam addObject:PARAMETER23];
    [listWithWebSerParam addObject:PARAMETER24];
    [listWithWebSerParam addObject:PARAMETER25];
    [listWithWebSerParam addObject:PARAMETER26];
    [listWithWebSerParam addObject:PARAMETER27];
    [listWithWebSerParam addObject:PARAMETER28];
    [listWithWebSerParam addObject:PARAMETER29];
    [listWithWebSerParam addObject:PARAMETER30];
    [listWithWebSerParam addObject:PARAMETER31];
    [listWithWebSerParam addObject:PARAMETER32];
    [listWithWebSerParam addObject:PARAMETER33];
    [listWithWebSerParam addObject:PARAMETER34];
    [listWithWebSerParam addObject:PARAMETER35];
    [listWithWebSerParam addObject:PARAMETER36];
    [listWithWebSerParam addObject:PARAMETER37];
    [listWithWebSerParam addObject:PARAMETER38];
     
    return listWithWebSerParam;
}

-(NSString *)nullCheck:(NSString *)value
{
    //    if (value == nil || [value isEqual:[NSNull null]])

    if (value == (id)[NSNull null] || value == nil )
        return @"";
    else
        return value;
}

@end
