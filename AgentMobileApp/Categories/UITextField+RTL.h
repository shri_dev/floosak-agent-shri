//
//  UITextField+RTL.h
//  ConsumerMobileApp_9.2
//
//  Created by test on 8/17/17.
//  Copyright © 2017 Soumya. All rights reserved.
//

#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"
#import <UIKit/UIKit.h>
#import "Constants.h"

@interface UITextField (RTL)<UITextFieldDelegate>

-(instancetype)init;
- (void)setCustomLeftContentView:(NSString*)imageName;
- (void)setBottomBorderColor:(UIColor *)color;

@end
