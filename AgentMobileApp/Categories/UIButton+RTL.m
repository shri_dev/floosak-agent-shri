//
//  UIButton+RTL.m
//  ConsumerMobileApp_9.2
//
//  Created by test on 8/17/17.
//  Copyright © 2017 Soumya. All rights reserved.
//

#import "UIButton+RTL.h"

#define kBottomBorderViewTag 1010

@implementation UIButton (RTL)

@dynamic customButtonType;

-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if (self)
    {
        NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
        
        if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
        {
            self.titleLabel.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
        }
        else
        {
            self.titleLabel.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
        }
    }
    return self;
}

- (void)setCustomButtonType:(ButtonType)customButtonType {
    
    customButtonType = customButtonType;
    
    if (customButtonType == ButtonTypeSubmit) {
        [self setBackgroundImage:[UIImage imageNamed:@"submit-button-rounded-rect" inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
    }
}

- (void)setBottomBorderColor:(UIColor *)color {
    
    UIView* borderView = [self viewWithTag:kBottomBorderViewTag];
    
    if (borderView == nil) {
        
        borderView = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height-1, self.bounds.size.width, 1)];
        [borderView setTag:kBottomBorderViewTag];
        [self addSubview:borderView];
    }
    
    [borderView setBackgroundColor:color];
}

@end
