//
//  UILabel+RTL.m
//  ConsumerMobileApp_9.2
//
//  Created by test on 8/17/17.
//  Copyright © 2017 Soumya. All rights reserved.
//

#import "UILabel+RTL.h"


@implementation UILabel (RTL)

-(id)init
{
    self=[super init];
    if (self)
    {
        NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
        
        if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
        {
            self.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
        }
        else
        {
            self.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
        }
    }
    return self;
}

-(void)layoutSubviews
{
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
    {
        if (self.textAlignment==NSTextAlignmentCenter)
            self.textAlignment=NSTextAlignmentCenter;
        else if (self.textAlignment==NSTextAlignmentLeft)
            self.textAlignment=NSTextAlignmentRight;
        else
            self.textAlignment=NSTextAlignmentRight;
    }
    else
    {
        if (self.textAlignment==NSTextAlignmentCenter)
            self.textAlignment=NSTextAlignmentCenter;
        else if (self.textAlignment==NSTextAlignmentRight)
            self.textAlignment=NSTextAlignmentRight;
        else
            self.textAlignment=NSTextAlignmentLeft;
    }
}

@end
