//
//  UIAlertController+RTL.m
//  ConsumerMobileApp_9.2
//
//  Created by test on 8/23/17.
//  Copyright © 2017 Soumya. All rights reserved.
//

#import "UIAlertController+RTL.h"

@implementation UIAlertController (RTL)


-(void)viewWillAppear:(BOOL)animated
{
    if (self.preferredStyle==UIAlertControllerStyleActionSheet)
    {
        NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
        
        if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
        {
            self.view.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
        }
        else
        {
            self.view.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
        }
    }
    else
    {
        NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
        
        if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
        {
            self.view.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
        }
        else
        {
            self.view.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
        }
    }
    
}

@end
