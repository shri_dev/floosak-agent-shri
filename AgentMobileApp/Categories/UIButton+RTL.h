//
//  UIButton+RTL.h
//  ConsumerMobileApp_9.2
//
//  Created by test on 8/17/17.
//  Copyright © 2017 Soumya. All rights reserved.
//

#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"
#import <UIKit/UIKit.h>
#import "Constants.h"

typedef enum : NSUInteger {
    ButtonTypeNormal = 0,
    ButtonTypeSubmit
} ButtonType;

@interface UIButton (RTL)

@property ButtonType customButtonType;

-(id)initWithFrame:(CGRect)frame;
- (void)setBottomBorderColor:(UIColor *)color;

@end

