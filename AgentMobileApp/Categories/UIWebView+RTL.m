//
//  UIWebView+RTL.m
//  ConsumerMobileApp_9.2
//
//  Created by test on 8/23/17.
//  Copyright © 2017 Soumya. All rights reserved.
//

#import "UIWebView+RTL.h"

@implementation UIWebView (RTL)
-(id)init
{
    self=[super init];
    if (self)
    {
        NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
        
        if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
        {
            self.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
        }
        else
        {
            self.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
        }
    }
    return self;
}

@end
