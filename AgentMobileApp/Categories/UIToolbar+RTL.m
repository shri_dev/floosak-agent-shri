//
//  UIToolbar+RTL.m
//  ConsumerMobileApp_9.2
//
//  Created by test on 8/17/17.
//  Copyright © 2017 Soumya. All rights reserved.
//

#import "UIToolbar+RTL.h"

@implementation UIToolbar (RTL)
-(id)init
{
    self=[super init];
    if (self)
    {
        NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
        
        if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
        {
            self.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
        }
        else
        {
            self.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
        }
    }
    return self;
}
@end
