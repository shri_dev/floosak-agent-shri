//
//  UIImage+RTL.m
//  ConsumerMobileApp_9.2
//
//  Created by test on 8/30/17.
//  Copyright © 2017 Soumya. All rights reserved.
//

#import "UIImage+RTL.h"

@implementation UIImage (RTL)
+(UIImage *)imageNamed:(NSString *)name
{
    NSString *imgPath=[NSString stringWithFormat:@"%@",[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:name]];
    UIImage *sourceImage=[UIImage imageWithContentsOfFile:imgPath];
    UIImage *newImage=sourceImage;
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
    {
        newImage = [UIImage imageWithCGImage:sourceImage.CGImage
                                                    scale:sourceImage.scale
                                              orientation:UIImageOrientationUpMirrored];
    }
    return newImage;
}


+(UIImage *)imageFlipWithSourceImage:(UIImage *)sourceImage
{
    UIImage *newImage=sourceImage;
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
    {
        newImage = [UIImage imageWithCGImage:sourceImage.CGImage
                                       scale:sourceImage.scale
                                 orientation:UIImageOrientationUpMirrored];
    }
    return newImage;
    
}


@end
