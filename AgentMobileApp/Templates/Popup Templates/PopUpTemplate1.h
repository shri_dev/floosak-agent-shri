//
//  PopUpTemplate1.h
//  Consumer Client
//
//  Created by android on 6/17/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XmlParserHandler.h"
#import "ActivityIndicator.h"
#import "Reachability.h"
#import "CustomTextField.h"
/**
 * This class used to handle functionality and view of Popup Template1
 *
 * @author Integra
 *
 */
@interface PopUpTemplate1 : UIView<UITextFieldDelegate>
{
    NSMutableDictionary *localDataDictionary;
    NSDictionary *localDataDictionary1;
    /**
     Declare a toolbar for pickerview
     */
    UIToolbar *numberToolbar;
    
    CustomTextField *activeField;
    
    NSMutableArray *validationsArray;
    NSMutableArray *labelValidationsArray;
    
    ActivityIndicator *activityIndicator;
    NSString *validation_Type;
    NSString *alertview_Type;

    NSString *local_processorCode;
    
    NSString *nextTemplateProperty;
    NSString *nextTemplate;
    
    NSMutableDictionary *local_webServiceRequestInputDetails;
    
    NSString* sTemp;
    NSString *generatedKey;
    
    
    NSString *dropdownString;
    UIButton *btn;
    NSArray *dropDownDataArray;
    
    NSMutableDictionary *datavalueDictionary;
    
    
}
/**
 * declarations are used to set the UIConstraints Of Popup template1.
 *Label,Value label,Border label and Buttons.
 *keylabel,Textfield and Dropdownbutton.
 */
@property(nonatomic,strong) NSString *propertyFileName;
@property(nonatomic,strong) NSString *processor_Code;
@property(nonatomic,strong) UILabel *popupTemplateLabel;
@property(nonatomic,strong) UILabel *valueLabel;
@property(nonatomic,strong) UIScrollView *popupTemplateScrollView;
@property(nonatomic,strong) NSNumber *numStr;

@property(nonatomic,strong) UILabel *headingTitle_label;
@property(nonatomic,strong) UILabel *headingTitle_border_label;

@property(nonatomic,strong) UILabel *key_label1;
@property(nonatomic,strong) UILabel *value_label1;
@property(nonatomic,strong) UILabel *value_border_label1;

@property(nonatomic,strong) UILabel *key_label2;
@property(nonatomic,strong) UILabel *value_label2;
@property(nonatomic,strong) UILabel *value_border_label2;

@property(nonatomic,strong) UILabel *key_label3;
@property(nonatomic,strong) UILabel *value_label3;
@property(nonatomic,strong) UILabel *value_border_label3;

@property(nonatomic,strong) UILabel *key_label4;
@property(nonatomic,strong) UILabel *value_label4;
@property(nonatomic,strong) UILabel *value_border_label4;

@property(nonatomic,strong) UILabel *key_label5;
@property(nonatomic,strong) UILabel *value_label5;
@property(nonatomic,strong) UILabel *value_border_label5;

@property(nonatomic,strong) UILabel *key_label6;
@property(nonatomic,strong) UILabel *value_label6;
@property(nonatomic,strong) UILabel *value_border_label6;

@property(nonatomic,strong) UILabel *key_label7;
@property(nonatomic,strong) UILabel *value_label7;
@property(nonatomic,strong) UILabel *value_border_label7;

@property(nonatomic,strong) UILabel *key_label8;
@property(nonatomic,strong) UILabel *value_label8;
@property(nonatomic,strong) UILabel *value_border_label8;

@property(nonatomic,strong) UILabel *key_label9;
@property(nonatomic,strong) UILabel *value_label9;
@property(nonatomic,strong) UILabel *value_border_label9;

@property(nonatomic,strong) UILabel *key_label10;
@property(nonatomic,strong) UILabel *value_label10;
@property(nonatomic,strong) UILabel *value_border_label10;

@property(nonatomic,strong) UILabel *input_key_label1;
@property(nonatomic,strong) CustomTextField *input_value_field1;
@property(nonatomic,strong) UILabel *input_value_border_label1;
@property(nonatomic,strong) UIButton *dropDownButton1;

@property(nonatomic,strong) UILabel *input_key_label2;
@property(nonatomic,strong) CustomTextField *input_value_field2;
@property(nonatomic,strong) UILabel *input_value_border_label2;
@property(nonatomic,strong) UIButton *dropDownButton2;

@property(nonatomic,strong) UILabel *input_key_label3;
@property(nonatomic,strong) CustomTextField *input_value_field3;
@property(nonatomic,strong) UILabel *input_value_border_label3;
@property(nonatomic,strong) UIButton *dropDownButton3;

@property(nonatomic,strong) UILabel *input_key_label4;
@property(nonatomic,strong) CustomTextField *input_value_field4;
@property(nonatomic,strong) UILabel *input_value_border_label4;
@property(nonatomic,strong) UIButton *dropDownButton4;

@property(nonatomic,strong) UILabel *input_key_label5;
@property(nonatomic,strong) CustomTextField *input_value_field5;
@property(nonatomic,strong) UILabel *input_value_border_label5;
@property(nonatomic,strong) UIButton *dropDownButton5;

@property(nonatomic,strong) UIButton *button1;
@property(nonatomic,strong) UIButton *button2;

/**
 * This method is  used for Method Initialization of Popup template1.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile andDelegate:(id)delegate withDataDictionary:(NSDictionary *)dataDictionary withProcessorCode:(NSString *)processorCode  withTag:(int)tagValue withTitle:(NSString *)title  andMessage:(NSString *)message withSelectedIndexPath:(int)selectedIndexPath withDataArray:(NSArray *)dataArray withSubIndex:(int)subIndex;

@end




