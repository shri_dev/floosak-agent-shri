//
//  PopUpTemplate2.m
//  Consumer Client
//
//  Created by Integra Micro on 27/04/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//

#import "PopUpTemplate2.h"
#import "Localization.h"
#import "ValidationsClass.h"
#import "Constants.h"
#import "OptionsMenu.h"
#import "Template.h"
#import "DatabaseManager.h"
#import "DatabaseConstatants.h"
#import "NotificationConstants.h"
#import "Reachability.h"
#import "WebServiceConstants.h"
#import "CustomTextField.h"

@implementation PopUpTemplate2

@synthesize propertyFileName,processor_Code;
@synthesize popupTemplateLabel;
@synthesize headingTitle_label,headingTitle_border_label;
@synthesize message_label,message_border_label;

@synthesize button1,button2;

#pragma mark - PopupTemplate2 UIView.
/**
 * This method is used to set implemention of Popup template2.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile andDelegate:(id)delegate1 withDataDictionary:(NSDictionary *)dataDictionary withProcessorCode:(NSString *)processorCode  withTag:(int)tagValue withTitle:(NSString *)title  andMessage:(NSString *)message withSelectedIndexPath:(int)selectedIndexPath withDataArray:(NSArray *)dataArray withSubIndex:(int)subIndex{
    
    NSLog(@"PropertyFileName:%@",propertyFile);
    self = [super initWithFrame:frame];
    [self setBackgroundColor:[UIColor colorWithRed:0.1f green:0.1f blue:0.1f alpha:0.4f]];
    if (self)
    {
        NSString *str = NSLocalizedStringFromTableInBundle(@"0174_014",@"Localization_ENGLISH",[NSBundle mainBundle], nil);
        if ([message isEqualToString:str]){
            self.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
        }
        self.tag = tagValue;
        self.titleStr=title;
        propertyFileName = propertyFile;
        
        if ([[dataDictionary objectForKey:PARAMETER9]length]>0) {
            logindetailsDict = [[NSMutableDictionary alloc] initWithDictionary:dataDictionary];
        }
        localDataDictionary = [[NSMutableDictionary alloc] initWithDictionary:dataDictionary];
        NSLog(@"Message is .... ...%@",localDataDictionary);
        
        if ([localDataDictionary objectForKey:PARAMETER15]) {
            if ([[localDataDictionary objectForKey:PARAMETER15] isEqualToString:PROCESSOR_CODE_P2P_REGISTERED_BENEFICIARY_MOBILE_NUMBER]) {
                if ([localDataDictionary objectForKey:PARAMETER9]) {
                    [localDataDictionary setObject:[localDataDictionary objectForKey:PARAMETER9] forKey:PARAMETER37];
                    [localDataDictionary removeObjectForKey:PARAMETER9];
                }
            }
            else
            {
                if ([localDataDictionary objectForKey:PARAMETER34]) {
                    [localDataDictionary setObject:[localDataDictionary objectForKey:PARAMETER34] forKey:PARAMETER19];
                    [localDataDictionary setObject:[localDataDictionary objectForKey:PARAMETER34] forKey:PARAMETER37];
                    [localDataDictionary removeObjectForKey:PARAMETER34];
                }
                
                if ([localDataDictionary objectForKey:PARAMETER20]) {
                    [localDataDictionary setObject:[localDataDictionary objectForKey:PARAMETER20] forKey:PARAMETER34];
                }
            }
        }
        
        NSArray *keysArr = [localDataDictionary allKeys];
        if (keysArr.count != 0) {
            [localDataDictionary setValue:[NSNumber numberWithInt:1] forKey:@"addPayeeStatus"];
            for (int i = 0; i < keysArr.count; i++) {
                NSString *key = [keysArr objectAtIndex:i];
                if (!([key isEqualToString:PARAMETER19] || [key isEqualToString:PARAMETER37] || [key isEqualToString:@"addPayeeStatus"] || [key isEqualToString:PARAMETER20] || [key isEqualToString:PARAMETER34] || [key isEqualToString:PARAMETER37] || [key isEqualToString:@"pushNotification"])) {
                    [localDataDictionary removeObjectForKey:key];
                }
            }
        }
        
        if (![message isEqualToString:@""]) {
            messageString=message;
        }
        processorCode=processor_Code;
        [self addControlesToView:message];
    }
    
    
    
    return self;
}


#pragma mark  - PopUpTemplate2 UI constraints Creation.
/**
 * This method is used to set add UIconstraints Frame of Popup template2.
 */
-(void)addControlesToView:(NSString *)message
{
    popupTemplateLabel = [[UILabel alloc] init];
    if ([localDataDictionary objectForKey:@"pushNotification"])
    {
        CGRect r = [[localDataDictionary objectForKey:@"pushNotification"] boundingRectWithSize:CGSizeMake(SCREEN_WIDTH-100, 0)  options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]} context:nil];
        popupTemplateLabel.frame = CGRectMake(30, 0, SCREEN_WIDTH-60 ,r.size.height+180);
    }
    else if ((![messageString isEqualToString:@""]) || ([propertyFileName compare:@"(null)"]==NSOrderedSame)) {
        CGRect r = [message boundingRectWithSize:CGSizeMake(SCREEN_WIDTH-100, 0)  options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]} context:nil];
        popupTemplateLabel.frame = CGRectMake(30, 0, SCREEN_WIDTH-60 ,r.size.height+180);
    }
    else
        popupTemplateLabel.frame = CGRectMake(30, 0, SCREEN_WIDTH-60 ,180);
    
    popupTemplateLabel.center = self.center;
    popupTemplateLabel.backgroundColor = [UIColor whiteColor];
    
    [self addSubview:popupTemplateLabel];
    
    // header Title label.
    headingTitle_label = [[UILabel alloc] init];
    headingTitle_label.frame = CGRectMake(30, popupTemplateLabel.frame.origin.y, popupTemplateLabel.frame.size.width, 45);
    headingTitle_label.backgroundColor = [UIColor clearColor];
    headingTitle_label.adjustsFontSizeToFitWidth = YES;
    
    NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template2_title_label",propertyFileName,[NSBundle mainBundle], nil)];
    
    if (![headerStr isEqualToString:@"popup_template2_title_label"] && NSLocalizedStringFromTableInBundle(@"popup_template2_title_label",propertyFileName,[NSBundle mainBundle], nil).length !=0) {
        headingTitle_label.text = headerStr;
    }
    else
        headingTitle_label.text = self.titleStr;
    
    
    // Properties for label textcolor
    NSArray *headingLabel_Color ;
    if (NSLocalizedStringFromTableInBundle(@"popup_template2_title_label_color",propertyFileName,[NSBundle mainBundle], nil))
        headingLabel_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template2_title_label_color",propertyFileName,[NSBundle mainBundle], nil)];
    else
        headingLabel_Color=[ValidationsClass colorWithHexString:application_default_text_color];
    
    if (headingLabel_Color)
        headingTitle_label.textColor =[UIColor colorWithRed:[[headingLabel_Color objectAtIndex:0] floatValue] green:[[headingLabel_Color objectAtIndex:1] floatValue] blue:[[headingLabel_Color objectAtIndex:2] floatValue] alpha:1.0f];
    
    // Properties for label textStyle
    
    NSString *textStyle;
    if(![NSLocalizedStringFromTableInBundle(@"popup_template2_title_label_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"popup_template2_title_label_style"] && NSLocalizedStringFromTableInBundle(@"popup_template2_title_label_style",propertyFileName,[NSBundle mainBundle], nil).length !=0)
        textStyle =NSLocalizedStringFromTableInBundle(@"popup_template2_title_label_style",propertyFileName,[NSBundle mainBundle], nil);
    else
        textStyle =application_default_text_style;
    
    // Properties for label fontSize
    
    NSString *fontSize;
    if(![NSLocalizedStringFromTableInBundle(@"popup_template2_title_label_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"popup_template2_title_label_size"] && NSLocalizedStringFromTableInBundle(@"popup_template2_title_label_size",propertyFileName,[NSBundle mainBundle], nil).length !=0)
        fontSize =NSLocalizedStringFromTableInBundle(@"popup_template2_title_label_size",propertyFileName,[NSBundle mainBundle], nil);
    else
        fontSize =application_default_text_size;
    
    if ([textStyle isEqualToString:TEXT_STYLE_0])
        headingTitle_label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_1])
        headingTitle_label.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_2])
        headingTitle_label.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
    
    else
        headingTitle_label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    headingTitle_label.textAlignment = NSTextAlignmentCenter;
    nextY_position=popupTemplateLabel.frame.origin.y+headingTitle_label.frame.size.height;
    [self addSubview:headingTitle_label];
    
    // header border title label
    headingTitle_border_label = [[UILabel alloc] init];
    headingTitle_border_label.frame = CGRectMake(50,nextY_position,popupTemplateLabel.frame.size.width-40,1);
    headingTitle_border_label.backgroundColor =[UIColor blackColor];
    nextY_position=nextY_position+headingTitle_border_label.frame.size.height;
    [self addSubview:headingTitle_border_label];
    
    // Message label
    message_label = [[UILabel alloc] init];
    message_label.frame = CGRectMake(50,nextY_position,popupTemplateLabel.frame.size.width-25, 50);
    message_label.backgroundColor = [UIColor clearColor];
    
    NSString *messageStr;
    if ([localDataDictionary objectForKey:@"pushNotification"]) {
        CGRect rect=message_label.frame;
        rect.origin.y=(([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:ar])?rect.origin.y+22:rect.origin.y);
        message_label.frame=rect;
        messageStr=[NSString stringWithFormat:@"%@",[localDataDictionary objectForKey:@"pushNotification"]];
    }
    else if(![NSLocalizedStringFromTableInBundle(@"popup_template2_title_label",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"popup_template2_title_label"] && NSLocalizedStringFromTableInBundle(@"popup_template2_title_label",propertyFileName,[NSBundle mainBundle], nil).length !=0)
        messageStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template2_message_label",propertyFileName,[NSBundle mainBundle], nil)];
    if (messageStr)
        message_label.text=messageStr;
    
    else
        message_label.text=messageString;
    
    // Properties for label textcolor
    NSArray *messageLabel_Color ;
    if(![NSLocalizedStringFromTableInBundle(@"popup_template2_message_label_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"popup_template2_message_label_color"] && NSLocalizedStringFromTableInBundle(@"popup_template2_message_label_color",propertyFileName,[NSBundle mainBundle], nil).length !=0)
        messageLabel_Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template2_message_label_color",propertyFileName,[NSBundle mainBundle], nil)];
    else
        messageLabel_Color=[ValidationsClass colorWithHexString:application_default_text_color];
    
    if (messageLabel_Color)
        message_label.textColor = [UIColor colorWithRed:[[messageLabel_Color objectAtIndex:0] floatValue] green:[[messageLabel_Color objectAtIndex:1] floatValue] blue:[[messageLabel_Color objectAtIndex:2] floatValue] alpha:1.0f];
    
    // Properties for label textStyle
    NSString *textStyle1;
    
    if(![NSLocalizedStringFromTableInBundle(@"popup_template2_message_label_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"popup_template2_message_label_style"] && NSLocalizedStringFromTableInBundle(@"popup_template2_message_label_style",propertyFileName,[NSBundle mainBundle], nil).length !=0)
        textStyle1 =NSLocalizedStringFromTableInBundle(@"popup_template2_message_label_style",propertyFileName,[NSBundle mainBundle], nil);
    else
        textStyle1 =application_default_text_style;
    
    // Properties for label fontSize
    NSString *fontSize1;
    if(![NSLocalizedStringFromTableInBundle(@"popup_template2_message_label_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"popup_template2_message_label_size"] && NSLocalizedStringFromTableInBundle(@"popup_template2_message_label_size",propertyFileName,[NSBundle mainBundle], nil).length !=0)
        fontSize1 =NSLocalizedStringFromTableInBundle(@"popup_template2_message_label_size",propertyFileName,[NSBundle mainBundle], nil);
    else
        fontSize1 =application_default_text_size;
    
    if ([textStyle1 isEqualToString:TEXT_STYLE_0])
        message_label.font = [UIFont systemFontOfSize:[fontSize1 floatValue]];
    
    else if ([textStyle1 isEqualToString:TEXT_STYLE_1])
        message_label.font = [UIFont boldSystemFontOfSize:[fontSize1 floatValue]];
    
    else if ([textStyle1 isEqualToString:TEXT_STYLE_2])
        message_label.font = [UIFont italicSystemFontOfSize:[fontSize1 floatValue]];
    
    else
        message_label.font = [UIFont systemFontOfSize:[fontSize1 floatValue]];
    
    message_label.textAlignment=NSTextAlignmentCenter;
    message_label.numberOfLines=0;
    [message_label sizeToFit];
    nextY_position=(nextY_position+message_label.frame.size.height+((SCREEN_HEIGHT>568)?(SCREEN_HEIGHT/8.5):(SCREEN_HEIGHT/7)));
    [self addSubview:message_label];
    
    // Buttons
    if([propertyFileName compare:@"(null)"]==NSOrderedSame)
    {
        button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setFrame:CGRectMake(50,nextY_position-10,headingTitle_label.frame.size.width-50,35)];
        [button1 setTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_ok_button", nil)] forState:UIControlStateNormal];
        
        // properties For Button backgroundColor
        NSArray *button1Color;
        button1Color=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button1Color)
            button1.backgroundColor=[UIColor colorWithRed:[[button1Color objectAtIndex:0] floatValue] green:[[button1Color objectAtIndex:1] floatValue] blue:[[button1Color objectAtIndex:2] floatValue] alpha:1.0f];
        
        //Default Properties for Button textcolor
        NSArray *button1_TextColor ;
        button1_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
        
        if (button1_TextColor)
            [button1 setTitleColor:[UIColor colorWithRed:[[button1_TextColor objectAtIndex:0] floatValue] green:[[button1_TextColor objectAtIndex:1] floatValue] blue:[[button1_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
        
        // Properties for Button textStyle
        NSString *textStyle;
        textStyle =application_default_button_text_style;
        
        //Default Properties for Button fontSize
        NSString *fontSize;
        fontSize=application_default_button_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
            button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
            button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
            button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
            button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        [button1 setTag:0];
        [button1 setExclusiveTouch:YES];
        [button1 addTarget:self action:@selector(okButton1Action:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button1];
    }
    else
    {
        //Button1 And Button2
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template2_button1_text_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame && [NSLocalizedStringFromTableInBundle(@"popup_template2_button2_text_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            button1 = [UIButton buttonWithType:UIButtonTypeCustom];
            [button1 setFrame:CGRectMake(50,headingTitle_label.frame.origin.y+headingTitle_label.frame.size.height+headingTitle_border_label.frame.size.height+message_label.frame.size.height+message_border_label.frame.size.height+40,(headingTitle_label.frame.size.width/2)-25,35)];
            
            NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template2_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
            if (buttonStr)
                [button1 setTitle:buttonStr forState:UIControlStateNormal];
            
            // properties For Button backgroundColor
            NSArray *button1Color;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template2_button1_color",propertyFileName,[NSBundle mainBundle], nil))
                button1Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template2_button1_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button1Color=[ValidationsClass colorWithHexString:application_default_button_background_color];
            
            if (button1Color)
                button1.backgroundColor=[UIColor colorWithRed:[[button1Color objectAtIndex:0] floatValue] green:[[button1Color objectAtIndex:1] floatValue] blue:[[button1Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for Button textcolor
            NSArray *button1_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template2_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template2_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button1_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button1_TextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[button1_TextColor objectAtIndex:0] floatValue] green:[[button1_TextColor objectAtIndex:1] floatValue] blue:[[button1_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template2_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"popup_template2_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template2_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize =NSLocalizedStringFromTableInBundle(@"popup_template2_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize=application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            
            [button1 setTag:0];
            [button1 setExclusiveTouch:YES];
            [button1 addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:button1];
            
            //Button2
            
            button2 = [UIButton buttonWithType:UIButtonTypeCustom];
            [button2 setFrame:CGRectMake((button1.frame.origin.x+button1.frame.size.width+10),headingTitle_label.frame.origin.y+headingTitle_label.frame.size.height+headingTitle_border_label.frame.size.height+message_label.frame.size.height+message_border_label.frame.size.height+40,(headingTitle_label.frame.size.width/2)-25,35)];
            
            NSString *buttonStr1=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template2_button2_text",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (buttonStr1)
                [button2 setTitle:buttonStr1 forState:UIControlStateNormal];
            
            // properties For Button backgroundColor
            NSArray *button2Color;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template2_button2_color",propertyFileName,[NSBundle mainBundle], nil))
                button2Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template2_button2_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button2Color=[ValidationsClass colorWithHexString:application_default_button_background_color];
            
            if (button2Color)
                button2.backgroundColor=[UIColor colorWithRed:[[button2Color objectAtIndex:0] floatValue] green:[[button2Color objectAtIndex:1] floatValue] blue:[[button2Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            // Default Properties for Button textcolor
            NSArray *button2_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template2_button2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template2_button2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button2_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button2_TextColor)
                [button2 setTitleColor:[UIColor colorWithRed:[[button2_TextColor objectAtIndex:0] floatValue] green:[[button2_TextColor objectAtIndex:1] floatValue] blue:[[button2_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            if(NSLocalizedStringFromTableInBundle(@"popup_template2_button2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"popup_template2_button2_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            if(NSLocalizedStringFromTableInBundle(@"popup_template2_button2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize =NSLocalizedStringFromTableInBundle(@"popup_template2_button2_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize=application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            
            [button2 setTag:1];
            [button2 setExclusiveTouch:YES];
            [button2 addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:button2];
            
        }
        
        else if ([NSLocalizedStringFromTableInBundle(@"popup_template2_button1_text_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            button1 = [UIButton buttonWithType:UIButtonTypeCustom];
            [button1 setFrame:CGRectMake(50,headingTitle_label.frame.origin.y+headingTitle_label.frame.size.height+headingTitle_border_label.frame.size.height+message_label.frame.size.height+message_border_label.frame.size.height+40,headingTitle_label.frame.size.width-50,35)];
            
            NSString *butttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template2_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (butttonStr)
                [button1 setTitle:butttonStr forState:UIControlStateNormal];
            
            // properties For Button backgroundColor
            NSArray *button1Color;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template2_button1_color",propertyFileName,[NSBundle mainBundle], nil))
                button1Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template2_button1_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                button1Color=[ValidationsClass colorWithHexString:application_default_button_background_color];
            
            
            if (button1Color)
                button1.backgroundColor=[UIColor colorWithRed:[[button1Color objectAtIndex:0] floatValue] green:[[button1Color objectAtIndex:1] floatValue] blue:[[button1Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for Button textcolor
            NSArray *button1_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template2_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template2_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button1_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button1_TextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[button1_TextColor objectAtIndex:0] floatValue] green:[[button1_TextColor objectAtIndex:1] floatValue] blue:[[button1_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template2_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"popup_template2_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template2_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize =NSLocalizedStringFromTableInBundle(@"popup_template2_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize=application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            [button1 setTag:0];
            [button1 setExclusiveTouch:YES];
            [button1 addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:button1];
        }
        
        else if ([NSLocalizedStringFromTableInBundle(@"popup_template2_button2_text_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            button2 = [UIButton buttonWithType:UIButtonTypeCustom];
            [button2 setFrame:CGRectMake(50,headingTitle_label.frame.origin.y+headingTitle_label.frame.size.height+headingTitle_border_label.frame.size.height+message_label.frame.size.height+message_border_label.frame.size.height+40,headingTitle_label.frame.size.width-50,35)];
            
            NSString *buttonStr = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template2_button2_text",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (buttonStr)
                [button2 setTitle:buttonStr forState:UIControlStateNormal];
            
            // properties For Button backgroundColor
            NSArray *button2Color;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template2_button2_color",propertyFileName,[NSBundle mainBundle], nil))
                button2Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template2_button2_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button2Color=[ValidationsClass colorWithHexString:application_default_button_background_color];
            
            if (button2Color)
                button2.backgroundColor=[UIColor colorWithRed:[[button2Color objectAtIndex:0] floatValue] green:[[button2Color objectAtIndex:1] floatValue] blue:[[button2Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for Button textcolor
            NSArray *button2_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template2_button2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template2_button2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button2_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button2_TextColor)
                [button2 setTitleColor:[UIColor colorWithRed:[[button2_TextColor objectAtIndex:0] floatValue] green:[[button2_TextColor objectAtIndex:1] floatValue] blue:[[button2_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template2_button2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"popup_template2_button2_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template2_button2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize =NSLocalizedStringFromTableInBundle(@"popup_template2_button2_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize=application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            [button2 setTag:1];
            [button2 setExclusiveTouch:YES];
            [button2 addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:button2];
        }
    }
}

#pragma mark - Button methods

/**
 * This method is used to set button1 action of popup template2.
 @prameters are defined in button action those are
 * Application display type(ticker,PopUp),validation type,next template,next template PropertyFile,Action type,
 Feature-(processorCode,transaction type).
 */
-(void)button1Action:(id)sender
{
    NSString *alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    NSString *validation_Type = NSLocalizedStringFromTableInBundle(@"application_validation_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"popup_template2_button1_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"popup_template2_button1_next_template",propertyFileName,[NSBundle mainBundle], nil);
    NSString *actionType =  NSLocalizedStringFromTableInBundle(@"popup_template2_button1_action_type",propertyFileName,[NSBundle mainBundle], nil);
    NSString *apiParamType = NSLocalizedStringFromTableInBundle(@"popup_template2_button1_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil);
    if ( validationsArray)
        {
            for (int i=0; i<[validationsArray count]; i++)
            {
                CustomTextField *textField = (CustomTextField *)[self viewWithTag:i+1];
                NSString *fieldValue = textField.text;
    
                if (fieldValue)
                    [[validationsArray objectAtIndex:i] setObject:fieldValue forKey:@"value"];
                else
                    [[validationsArray objectAtIndex:i] setObject:@"" forKey:@"value"];
            }
        }
        if ( labelValidationsArray )
        {
            for (int i=0; i<[labelValidationsArray count]; i++)
            {
                UILabel *textLabel = (UILabel *)[self viewWithTag:i+101];
                NSString *fieldValue = textLabel.text;
    
                if (fieldValue)
                    [[labelValidationsArray objectAtIndex:i] setObject:fieldValue forKey:@"value"];
                else
                    [[labelValidationsArray objectAtIndex:i] setObject:@"" forKey:@"value"];
    
                [validationsArray addObject:[labelValidationsArray objectAtIndex:i]];
            }
        }
    
        NSString *data = NSLocalizedStringFromTableInBundle(@"popup_template2_button1_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
        processor_Code = [Template getProcessorCodeWithData:data];
        NSString *transactionType = [Template getTransactionCodeWithData:data];
    
        if (transactionType) {
            [localDataDictionary setObject:transactionType forKey:PARAMETER13];
        }
    
        if (processor_Code) {
            [localDataDictionary setObject:processor_Code forKey:PARAMETER15];
        }
    
    Template *obj = [Template initWithactionType:actionType nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:propertyFileName ProcessorCode:processor_Code Dictionary:[processor_Code isEqualToString:PROCESSOR_CODE_LOGIN]?logindetailsDict:localDataDictionary contentArray:nil alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:apiParamType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:transactionType withCurrentClass:POPUP_TEMPLATE_2];;
    obj.selector = @selector(processData);
    obj.target = self;
    [[NSNotificationCenter defaultCenter] postNotificationName:PPT2_BUTTON1_ACTION object:obj];
}
/**
 * This method is used to set button1 action of popup template2.
 */
-(void)button2Action:(id)sender
{
     NSString *alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    NSString *validation_Type = NSLocalizedStringFromTableInBundle(@"application_validation_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"popup_template2_button2_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"popup_template2_button2_next_template",propertyFileName,[NSBundle mainBundle], nil);
    NSString *actionType =  NSLocalizedStringFromTableInBundle(@"popup_template2_button2_action_type",propertyFileName,[NSBundle mainBundle], nil);
    NSString *apiParamType = NSLocalizedStringFromTableInBundle(@"popup_template2_button2_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil);
    
    if ( validationsArray)
    {
        for (int i=0; i<[validationsArray count]; i++)
        {
            CustomTextField *textField = (CustomTextField *)[self viewWithTag:i+1];
            NSString *fieldValue = textField.text;
            
            if (fieldValue)
                [[validationsArray objectAtIndex:i] setObject:fieldValue forKey:@"value"];
            else
                [[validationsArray objectAtIndex:i] setObject:@"" forKey:@"value"];
        }
    }
    
    if (labelValidationsArray)
    {
        for (int i=0; i<[labelValidationsArray count]; i++)
        {
            UILabel *textLabel = (UILabel *)[self viewWithTag:i+101];
            NSString *fieldValue = textLabel.text;
            
            if (fieldValue)
                [[labelValidationsArray objectAtIndex:i] setObject:fieldValue forKey:@"value"];
            else
                [[labelValidationsArray objectAtIndex:i] setObject:@"" forKey:@"value"];
            
            [validationsArray addObject:[labelValidationsArray objectAtIndex:i]];
        }
    }
    NSString *data = NSLocalizedStringFromTableInBundle(@"popup_template2_button2_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    processor_Code = [Template getProcessorCodeWithData:data];
    NSString *transactionType = [Template getTransactionCodeWithData:data];
    
    if (transactionType) {
        [localDataDictionary setObject:transactionType forKey:PARAMETER13];
    }
    
    if (processor_Code) {
        [localDataDictionary setObject:processor_Code forKey:PARAMETER15];
    }
    
    NSLog(@"Button Action Type is....%@,%@,%@",actionType,nextTemplate,nextTemplateProperty);
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"HomePageType"];
    Template *obj = [Template initWithactionType:actionType nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:propertyFileName ProcessorCode:processor_Code Dictionary:[processor_Code isEqualToString:PROCESSOR_CODE_LOGIN]?logindetailsDict:localDataDictionary contentArray:nil alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:apiParamType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:transactionType withCurrentClass:POPUP_TEMPLATE_2];
    obj.selector = @selector(processData);
    obj.target = self;
    [[NSNotificationCenter defaultCenter] postNotificationName:PPT2_BUTTON2_ACTION object:obj];
    
}
/**
 * This method is used to set alert button action of popup template2.
 */
-(void)okButton1Action:(id)sender{
    
//    for (UIView *v in self.subviews) {
//        if (![v isKindOfClass:[POPUP_TEMPLATE_2 class]]) {
//            [v removeFromSuperview];
//        }
//    }
//    NSString *className=NSStringFromClass([PopUpTemplate2 class]);
//                                          
//    NSLog(@"Class of controller: %@",className);
    
//    NSArray *subviews = [[self subviews] copy];
//    for (UIView *subview in subviews) {
//        [subview removeFromSuperview];
//    }
//    for (id viewToRemove in [self subviews]){
//        
//        if ([viewToRemove isMemberOfClass:[UIView class]])
//            [viewToRemove removeFromSuperview];
//    }
    
//    for (UIView *subUIView in self.subviews) {
//        if ([subUIView isKindOfClass:[self.subviews class]]) {
//               [subUIView removeFromSuperview];
//        }
//     
//    }
    [self removeFromSuperview];

}
#pragma mark - BaseView Process Data methods.
/**
 * This method is used to set baseviewcontroller Process data notification.
 */
-(void)processData
{
    //ProcessData
}

@end
