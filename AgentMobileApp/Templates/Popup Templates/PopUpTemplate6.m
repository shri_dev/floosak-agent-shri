//
//  PopUpTemplate6.m
//  Consumer Client
//
//  Created by android on 6/24/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "PopUpTemplate6.h"

#import "Constants.h"
#import "Localization.h"

#import "ValidationsClass.h"
#import "UIView+Toast.h"

#import "PopUpTemplate2.h"
#import "ChildTemplate3.h"

#import "WebSericeUtils.h"
#import "WebServiceConstants.h"
#import "WebServiceRequestFormation.h"
#import "WebServiceRunning.h"
#import "WebServiceDataObject.h"

#import "DatabaseManager.h"
#import "DatabaseConstatants.h"
#import "ConvertDetailsToSrverRequestFormate.h"

#import "ParentTemplate1.h"
#import "Template.h"
#import "NotificationConstants.h"
#import "AppDelegate.h"

#import "UIImage+RTL.h"
#import "CustomTextField.h"

@implementation PopUpTemplate6

@synthesize propertyFileName,processor_Code;
@synthesize popupTemplateScrollView;
@synthesize popupTemplateLabel;
@synthesize headingTitle_label,headingTitle_border_label;
@synthesize key_label1,value_label1,value_border_label1;
@synthesize key_label2,value_label2,value_border_label2;
@synthesize key_label3,value_label3,value_border_label3;
@synthesize key_label4,value_label4,value_border_label4;
@synthesize key_label5,value_label5,value_border_label5;
@synthesize key_label6,value_label6,value_border_label6;
@synthesize key_label7,value_label7,value_border_label7;
@synthesize key_label8,value_label8,value_border_label8;
@synthesize key_label9,value_label9,value_border_label9;
@synthesize key_label10,value_label10,value_border_label10;
@synthesize key_label11,value_label11,value_border_label11;
@synthesize key_label12,value_label12,value_border_label12;
@synthesize key_label13,value_label13,value_border_label13;
@synthesize key_label14,value_label14,value_border_label14;
@synthesize key_label15,value_label15,value_border_label15;
@synthesize key_label16,value_label16,value_border_label16;
@synthesize key_label17,value_label17,value_border_label17;
@synthesize key_label18,value_label18,value_border_label18;

@synthesize input_key_label1,input_value_field1,dropDownButton1,input_value_border_label1;
@synthesize input_key_label2,input_value_field2,dropDownButton2,input_value_border_label2;
@synthesize input_key_label3,input_value_field3,dropDownButton3,input_value_border_label3;
@synthesize input_key_label4,input_value_field4,dropDownButton4,input_value_border_label4;
@synthesize input_key_label5,input_value_field5,dropDownButton5,input_value_border_label5;

@synthesize button1,button2;


#pragma mark - PopupTemplate6 UIView.
/**
 * This method is used to set implemention of Popup template1.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile andDelegate:(id)delegate withDataDictionary:(NSDictionary *)dataDictionary withProcessorCode:(NSString *)processorCode  withTag:(int)viewTag withTitle:(NSString *)title  andMessage:(NSString *)message withSelectedIndexPath:(int)selectedIndexPath withDataArray:(NSArray *)dataArray withSubIndex:(int)subIndex
{
    NSLog(@"PropertyFileName:%@",propertyFile);
    self = [super initWithFrame:frame];
    if (self)
    {
        isArabic = [[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"];
        self.tag = viewTag;
        [self setBackgroundColor:[UIColor colorWithRed:0.1f green:0.1f blue:0.1f alpha:0.4f]];
        
        
        propertyFileName = propertyFile;
        localDataDictionary = [[NSMutableDictionary alloc] initWithDictionary:dataDictionary];
        if([propertyFileName isEqualToString:@"VerifyRegistrationPPT6_T1"]){
            [self addObservers];
        }
        if([propertyFileName isEqualToString:@"VerifyRegistrationPPT6_T2"]){
            [self addObservers];
            verifyImagesCount = 0;
            currentImage = 1;
            
            if([localDataDictionary objectForKey:@"Reference1"]){
                NSURL *URL = [NSURL URLWithString:
                              [NSString stringWithFormat:@"data:application/octet-stream;base64,%@", [localDataDictionary objectForKey:@"Reference1"]]];
                NSData *tempData = [NSData dataWithContentsOfURL:URL];
                if(tempData){
                    refImage1 = [UIImage imageWithData:tempData];
                    verifyImagesCount++;
                }
            }
            if([localDataDictionary objectForKey:@"Reference2"]){
                NSURL *URL = [NSURL URLWithString:
                              [NSString stringWithFormat:@"data:application/octet-stream;base64,%@", [localDataDictionary objectForKey:@"Reference2"]]];
                NSData *tempData = [NSData dataWithContentsOfURL:URL];
                if(tempData){
                    refImage2 = [UIImage imageWithData:tempData];
                    verifyImagesCount++;
                }
            }
            if([localDataDictionary objectForKey:@"Reference3"]){
                NSURL *URL = [NSURL URLWithString:
                              [NSString stringWithFormat:@"data:application/octet-stream;base64,%@", [localDataDictionary objectForKey:@"Reference3"]]];
                NSData *tempData = [NSData dataWithContentsOfURL:URL];
                if(tempData){
                    refImage3 = [UIImage imageWithData:tempData];
                    verifyImagesCount++;
                }
            }
            if([localDataDictionary objectForKey:@"Reference4"]){
                NSURL *URL = [NSURL URLWithString:
                              [NSString stringWithFormat:@"data:application/octet-stream;base64,%@", [localDataDictionary objectForKey:@"Reference4"]]];
                NSData *tempData = [NSData dataWithContentsOfURL:URL];
                if(tempData){
                    refImage4 = [UIImage imageWithData:tempData];
                    verifyImagesCount++;
                }
            }

        }
        local_processorCode = processorCode;
        [self addControlesToView];
//        [self addVerifyRegView];
        if([propertyFileName isEqualToString:@"VerifyRegistrationPPT6_T2"]){
            [input_key_label1 setHidden:YES];
            [input_value_field1 setHidden:YES];
            [input_value_border_label1 setHidden:YES];
        }
    }
    
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popUpTemplateButton6:) name:POPUPTEMPLATE6 object:nil];
    return self;
}

-(void)addObservers{
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(verifyRegApproved:) name:@"VerifyRegApproved" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(verifyRegRejected:) name:@"VerifyRegRejected" object:nil];
}

#pragma mark  - PopUpTemplate6 UI constraints Creation.
/**
 * This method is used to set add UIconstraints Frame of Popup template1.
 */

-(void)popUpTemplateButton6:(NSNotification *)notification
{
    NSArray *viewsArray = [[NSArray alloc] initWithArray:[[self superview] subviews]];
    NSLog(@"Views Array...%@",viewsArray);
    for (int i = 0; i<[viewsArray count]; i++)
    {
        UIView *lView = [viewsArray objectAtIndex:i];
        if (lView.tag < 0)
        {
            [lView removeFromSuperview];
        }
    }
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)addVerifyRegView{
    if([propertyFileName isEqualToString:@"VerifyRegistrationPPT6_T1"]){
        CGRect rect = popupTemplateScrollView.frame;
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(rect.origin.x+10, rect.origin.y+5, SCREEN_WIDTH-20, SCREEN_HEIGHT/2)];
        [label setNumberOfLines:5];
        //        if(localDataDictionary)
        //            [label setText:[localDataDictionary objectForKey:@"PaymentDetails2"]];
        //        else
        [label setText:@"no values yet"];
        [self addSubview:label];
    }
}


-(void)addControlesToView
{
    int validationsCount = 100;
    int inputValidationsCount = 100;
    
    validationsArray = [[NSMutableArray alloc] init];
    labelValidationsArray = [[NSMutableArray alloc] init];
    /*
     * This method is used to add popUptemplate6 ToolBar (Cancel,Ok buttons).
     */
    numberToolbar = [[UIToolbar alloc] init];
    numberToolbar.frame=CGRectMake(0, 0, SCREEN_WIDTH, 40);
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:
  [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancel", nil)] style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_ok_button", nil)]] style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    NSString *selectedIconBtncolors =application_branding_color_theme;
    NSArray *selectedIconBtncolorsArray = [ValidationsClass colorWithHexString:selectedIconBtncolors];
    numberToolbar.tintColor = [UIColor colorWithRed:[[selectedIconBtncolorsArray objectAtIndex:0] floatValue] green:[[selectedIconBtncolorsArray objectAtIndex:1] floatValue] blue:[[selectedIconBtncolorsArray objectAtIndex:2] floatValue] alpha:1.0f];

    popupTemplateLabel = [[UILabel alloc] init];
    popupTemplateLabel.frame = CGRectMake(20, 30, SCREEN_WIDTH-40 , SCREEN_HEIGHT-60);
    
    NSArray *backGroundColors = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_background_color",propertyFileName,[NSBundle mainBundle], nil)];
    
    if (backGroundColors)
        popupTemplateLabel.backgroundColor = [UIColor colorWithRed:[[backGroundColors objectAtIndex:0] floatValue] green:[[backGroundColors objectAtIndex:1] floatValue] blue:[[backGroundColors objectAtIndex:2] floatValue] alpha:1.0f];
    
    [self addSubview:popupTemplateLabel];
    
    // ------------------- Header Label ------------------- //
    
    float heading_Y = 30;
    
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_title_label_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        headingTitle_label = [[UILabel alloc] init];
        headingTitle_label.frame = CGRectMake(30, 30, SCREEN_WIDTH-60 , 50);
        
        headingTitle_label.backgroundColor = [UIColor clearColor];
        NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_title_label",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (headerStr)
            headingTitle_label.text=headerStr;
        
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_title_label_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template6_title_label_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_title_label_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                headingTitle_label.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_title_label_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_title_label_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_title_label_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_title_label_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headingTitle_label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headingTitle_label.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headingTitle_label.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headingTitle_label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                headingTitle_label.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headingTitle_label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headingTitle_label.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headingTitle_label.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headingTitle_label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        headingTitle_label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:headingTitle_label];
        heading_Y =(SCREEN_HEIGHT>=667) ?(headingTitle_label.frame.origin.y+headingTitle_label.frame.size.height+1):(headingTitle_label.frame.origin.y+headingTitle_label.frame.size.height+10);
    }
    //---------------------Header Borderlabel----------------------//
        headingTitle_border_label = [[UILabel alloc] init];
        headingTitle_border_label.frame = CGRectMake(50, heading_Y, SCREEN_WIDTH-100 , 1);
        headingTitle_border_label.backgroundColor = [UIColor blackColor];
        [self addSubview:headingTitle_border_label];
    
    popupTemplateScrollView = [[UIScrollView alloc] init];
    popupTemplateScrollView.backgroundColor = [UIColor clearColor];
    popupTemplateScrollView.frame = CGRectMake(40, heading_Y+2,popupTemplateLabel.frame.size.width-40 , popupTemplateLabel.frame.size.height-120);
    [self addSubview:popupTemplateScrollView];
    
    float X_Position = 5.0;
    float next_Y_Position = 0;
    float borderLabel_Y = 0.0;
    int numberOfFields = 0;
    
    // ------------------- Non Editable Starts ------------------- //
    
    // ------------------- 1st Value(label,Value label and border label) ------------------- //
    if([propertyFileName isEqualToString:@"VerifyRegistrationPPT6_T2"]){
        refImageView = [[UIImageView alloc] initWithFrame:CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,popupTemplateScrollView.frame.size.height-50)];
        [refImageView setImage:refImage1];
        [refImageView setUserInteractionEnabled:YES];
        [self.popupTemplateScrollView addSubview:refImageView];
        if (isArabic)
        {
            if(!isImageReversed && refImageView.image){
                isImageReversed = YES;
                NSLog(@"Image reversed in ppt6..initial..");
                refImageView.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
            }else{
                NSLog(@"refimage not found..");
            }
        }

        next_Y_Position = refImageView.frame.origin.y+refImageView.frame.size.height-4;
    }

    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if (![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value1_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value1_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"] && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value1_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
            NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
            [tempDictionary setObject:@"" forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value1_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
                key_label1 = [[UILabel alloc] init];
                key_label1.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
                key_label1.backgroundColor = [UIColor clearColor];
                
                NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label1",propertyFileName,[NSBundle mainBundle], nil)];
                
                if (labelStr)
                    key_label1.text=labelStr;
                
                    key_label1.textAlignment = NSTextAlignmentLeft;
                
                if ([NSLocalizedStringFromTableInBundle(@"popup_template6_label1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    NSArray *label_TextColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_label1_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label1_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        key_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label1_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label1_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label1_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label1_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        key_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        key_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                else
                {
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        key_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        key_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        key_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                
                [popupTemplateScrollView addSubview:key_label1];
                next_Y_Position = key_label1.frame.origin.y+key_label1.frame.size.height-4;
                numberOfFields++;

                value_label1 = [[UILabel alloc] init];
                value_label1.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
                  validationsCount++;
                 value_label1.tag = validationsCount;
                value_label1.backgroundColor = [UIColor clearColor];
                
                NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value1_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
                
                if (valueStr)
                    value_label1.text = valueStr;
                else
                    value_label1.text = application_default_no_value_available;
                
                value_label1.textAlignment = NSTextAlignmentLeft;
                
                if ([NSLocalizedStringFromTableInBundle(@"popup_template6_title_label_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    // Properties for label TextColor.
                    
                    NSArray *label_TextColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_value1_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value1_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        value_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value1_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value1_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value1_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value1_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        value_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        value_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        value_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        value_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                else
                {
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        value_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        value_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        value_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        value_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        value_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value1_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                    [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value1_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
                [popupTemplateScrollView addSubview:value_label1];
                next_Y_Position = next_Y_Position+value_label1.frame.size.height;
                borderLabel_Y = value_label1.frame.origin.y+value_label1.frame.size.height;
                numberOfFields++;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field1_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                value_border_label1 = [[UILabel alloc] init];
                value_border_label1.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
                value_border_label1.backgroundColor = [UIColor blackColor];
                [popupTemplateScrollView addSubview:value_border_label1];
                next_Y_Position = next_Y_Position+value_border_label1.frame.size.height;
            }
            [labelValidationsArray addObject:tempDictionary];
        }
    }
    // ------------------- 2nd Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if (![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value2_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[NSString stringWithFormat:@"%@",[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value2_param_type",propertyFileName,[NSBundle mainBundle], nil)]] isEqualToString:@"(null)"] && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value2_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
            NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
            [tempDictionary setObject:@"" forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value2_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        
                key_label2 = [[UILabel alloc] init];
                key_label2.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
                key_label2.backgroundColor = [UIColor clearColor];
                
                NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label2",propertyFileName,[NSBundle mainBundle], nil)];
                
                if (labelStr)
                    key_label2.text=labelStr;
                
                if ([NSLocalizedStringFromTableInBundle(@"popup_template6_label2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    // Properties for label TextColor.
                    
                    NSArray *label_TextColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_label2_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label2_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        key_label2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label2_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label2_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label2_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label2_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        key_label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        key_label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                else
                {
                    //Default Properties for label textcolor
                    
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        key_label2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        key_label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        key_label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                key_label2.textAlignment = NSTextAlignmentLeft;
                [popupTemplateScrollView addSubview:key_label2];
                next_Y_Position = next_Y_Position+key_label2.frame.size.height-4;
                numberOfFields++;

                value_label2 = [[UILabel alloc] init];
                value_label2.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
                 validationsCount++;
                value_label2.tag = validationsCount;
                value_label2.backgroundColor = [UIColor clearColor];
                
                NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value2_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
                
                if (valueStr)
                    value_label2.text = valueStr;
                else
                    value_label2.text = application_default_no_value_available;
                
                value_label2.textAlignment = NSTextAlignmentLeft;
                if ([NSLocalizedStringFromTableInBundle(@"popup_template6_value2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    
                    // Properties for label TextColor.
                    
                    NSArray *label_TextColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_value2_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value2_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        value_label2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value2_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value2_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value2_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value2_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        value_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        value_label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        value_label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        value_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                else
                {
                    //Default Properties for label textcolor
                    
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        value_label2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        value_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        value_label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        value_label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        value_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                [popupTemplateScrollView addSubview:value_label2];
                next_Y_Position = next_Y_Position+value_label2.frame.size.height;
                borderLabel_Y = value_label2.frame.origin.y+value_label2.frame.size.height;
                if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value2_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                    [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value2_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
                numberOfFields++;
                
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field2_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                value_border_label2 = [[UILabel alloc] init];
                value_border_label2.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
                value_border_label2.backgroundColor = [UIColor blackColor];
                [popupTemplateScrollView addSubview:value_border_label2];
                next_Y_Position = next_Y_Position+value_border_label2.frame.size.height;
            }
            [labelValidationsArray addObject:tempDictionary];
        }
    }
    // ------------------- 3rd Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field3_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if (![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value3_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[NSString stringWithFormat:@"%@",[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value3_param_type",propertyFileName,[NSBundle mainBundle], nil)]] isEqualToString:@"(null)"])
        {
            NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
            [tempDictionary setObject:@"" forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value3_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:@"" forKey:@"validation_message"];
            
    
                key_label3 = [[UILabel alloc] init];
                key_label3.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
                key_label3.backgroundColor = [UIColor clearColor];
           
                
                NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label3",propertyFileName,[NSBundle mainBundle], nil)];
                
                if (labelStr)
                    key_label3.text =labelStr;
                
                key_label3.textAlignment = NSTextAlignmentLeft;
                if ([NSLocalizedStringFromTableInBundle(@"popup_template6_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    // Properties for label TextColor.
                    
                    NSArray *label_TextColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_label3_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label3_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        key_label3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label3_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label3_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label3_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label3_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        key_label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        key_label3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                else
                {
                    //Default Properties for label textcolor
                    
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        key_label3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        key_label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        key_label3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                [popupTemplateScrollView addSubview:key_label3];
                next_Y_Position = next_Y_Position+key_label3.frame.size.height-4;
                numberOfFields++;

                value_label3 = [[UILabel alloc] init];
                value_label3.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
              validationsCount++;
               value_label3.tag = validationsCount;
                value_label3.backgroundColor = [UIColor clearColor];
                
               
                
                NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value3_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
                if (valueStr)
                    value_label3.text = valueStr;
                else
                    value_label3.text = application_default_no_value_available;
                
                value_label3.textAlignment = NSTextAlignmentLeft;
                
                if ([NSLocalizedStringFromTableInBundle(@"popup_template6_value3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    // Properties for label TextColor.
                    
                    NSArray *label_TextColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_value3_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value3_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        value_label3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value3_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value3_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value3_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value3_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        value_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        value_label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        value_label3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        value_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                else
                {
                    //Default Properties for label textcolor
                    
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        value_label3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        value_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        value_label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        value_label3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        value_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                }
                [popupTemplateScrollView addSubview:value_label3];
                next_Y_Position = next_Y_Position+value_label3.frame.size.height;
                borderLabel_Y = value_label3.frame.origin.y+value_label3.frame.size.height;
                if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value3_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                    [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value3_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
                numberOfFields++;
            
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field3_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                value_border_label3 = [[UILabel alloc] init];
                value_border_label3.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
                value_border_label3.backgroundColor = [UIColor blackColor];
                [popupTemplateScrollView addSubview:value_border_label3];
                next_Y_Position = next_Y_Position+value_border_label3.frame.size.height;
            }
            [labelValidationsArray addObject:tempDictionary];
        }
    }
    // ------------------- 4th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field4_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label4_param_type",propertyFileName,[NSBundle mainBundle], nil)] && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value4_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
            if (![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label4_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label4_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"])
            {
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                [tempDictionary setObject:@"" forKey:@"validation_type"];
                [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_label4_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
                
                if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label4_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                    [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label4_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
                [labelValidationsArray addObject:tempDictionary];
                
                key_label4 = [[UILabel alloc] init];
                key_label4.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
                key_label4.backgroundColor = [UIColor clearColor];
                
                NSString *labelStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label4_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
                
                if (labelStr)
                    key_label4.text =labelStr;
                
                key_label4.textAlignment = NSTextAlignmentLeft;
                
                
                if ([NSLocalizedStringFromTableInBundle(@"popup_template6_label4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    // Properties for label TextColor.
                    
                    NSArray *label_TextColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_label4_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label4_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        key_label4.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label4_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label4_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label4_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label4_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        key_label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        key_label4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                else
                {
                    //Default Properties for label textcolor
                    
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        key_label4.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        key_label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        key_label4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                [popupTemplateScrollView addSubview:key_label4];
                next_Y_Position = next_Y_Position+key_label4.frame.size.height-4;
                numberOfFields++;
            }
    
            if (![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value4_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value4_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"])
            {
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                [tempDictionary setObject:@"" forKey:@"validation_type"];
                [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value4_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
                if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value4_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                    [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value4_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
                
                [labelValidationsArray addObject:tempDictionary];
                
                value_label4 = [[UILabel alloc] init];
                value_label4.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
                validationsCount++;
                value_label4.tag = validationsCount;
                value_label4.backgroundColor = [UIColor clearColor];
                
               
                
                NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value4_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
                
                if (valueStr)
                    value_label4.text = valueStr;
                else
                    value_label4.text = application_default_no_value_available;

                
                value_label4.textAlignment = NSTextAlignmentLeft;
                if ([NSLocalizedStringFromTableInBundle(@"popup_template6_value4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    // Properties for label TextColor.
                    
                    NSArray *label_TextColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_value4_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value4_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        value_label4.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value4_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value4_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value4_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value4_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        value_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        value_label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        value_label4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        value_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                else
                {
                    //Default Properties for label textcolor
                    
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        value_label4.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        value_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        value_label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        value_label4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        value_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                next_Y_Position = next_Y_Position+value_label4.frame.size.height;
                borderLabel_Y = value_label4.frame.origin.y+value_label4.frame.size.height;
                [popupTemplateScrollView addSubview:value_label4];
                numberOfFields++;
            }
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field4_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label4 = [[UILabel alloc] init];
            value_border_label4.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label4.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label4];
            next_Y_Position = next_Y_Position+value_border_label4.frame.size.height;
        }
        }
    }
    // ------------------- 5th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field5_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label5_param_type",propertyFileName,[NSBundle mainBundle], nil)] && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value5_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
            if (![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label5_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label5_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"] && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label5_param_type",propertyFileName,[NSBundle mainBundle], nil)])
            {
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                [tempDictionary setObject:@"" forKey:@"validation_type"];
                [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_label5_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
                if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label5_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                    [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label5_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
                [labelValidationsArray addObject:tempDictionary];
                
                key_label5 = [[UILabel alloc] init];
                key_label5.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
                key_label5.backgroundColor = [UIColor clearColor];
                
                NSString *labelStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label5_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
                
                if (labelStr)
                    key_label5.text =labelStr;
                
                key_label5.textAlignment = NSTextAlignmentLeft;
                
                if ([NSLocalizedStringFromTableInBundle(@"popup_template6_label5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    // Properties for label TextColor.
                    
                    NSArray *label_TextColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_label5_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label5_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        key_label5.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label5_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label5_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label5_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label5_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        key_label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        key_label5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                else
                {
                    //Default Properties for label textcolor
                    
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        key_label5.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        key_label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        key_label5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                }
                [popupTemplateScrollView addSubview:key_label5];
                next_Y_Position = next_Y_Position+key_label5.frame.size.height-4;
                numberOfFields++;
            }
      
            if (![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value5_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value5_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"] && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value5_param_type",propertyFileName,[NSBundle mainBundle], nil)])
            {
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                [tempDictionary setObject:@"" forKey:@"validation_type"];
                [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value5_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
                if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value5_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                    [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value5_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
                [labelValidationsArray addObject:tempDictionary];

                value_label5 = [[UILabel alloc] init];
                value_label5.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
                validationsCount++;
                value_label5.tag = validationsCount;
                value_label5.backgroundColor = [UIColor clearColor];
                
               
                
                NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value5_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
                
                if (valueStr)
                    value_label5.text = valueStr;
                else
                    value_label5.text = application_default_no_value_available;

                value_label5.textAlignment = NSTextAlignmentLeft;
                if ([NSLocalizedStringFromTableInBundle(@"popup_template6_value5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    // Properties for label TextColor.
                    
                    NSArray *label_TextColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_value5_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value5_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        value_label5.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value5_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value5_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value5_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value5_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        value_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        value_label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        value_label5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        value_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                else
                {
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        value_label5.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        value_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        value_label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        value_label5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        value_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                [popupTemplateScrollView addSubview:value_label5];
                next_Y_Position = next_Y_Position+value_label5.frame.size.height;
                borderLabel_Y = value_label5.frame.origin.y+value_label5.frame.size.height;
                numberOfFields++;
            }
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field5_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label5 = [[UILabel alloc] init];
            value_border_label5.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label5.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label5];
            next_Y_Position = next_Y_Position+value_border_label5.frame.size.height;
        }
        }
    }
    // ------------------- 6th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field6_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label6_param_type",propertyFileName,[NSBundle mainBundle], nil)] && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value6_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
            if (![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label6_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label6_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"] && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label6_param_type",propertyFileName,[NSBundle mainBundle], nil)])
            {
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                [tempDictionary setObject:@"" forKey:@"validation_type"];
                [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_label6_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
                if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label6_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                    [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label6_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
                [labelValidationsArray addObject:tempDictionary];

                key_label6 = [[UILabel alloc] init];
                key_label6.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
                key_label6.backgroundColor = [UIColor clearColor];
                
                
//                if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label6_param_type",propertyFileName,[NSBundle mainBundle], nil)])
//                    key_label6.text = [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label6_param_type",propertyFileName,[NSBundle mainBundle], nil)];
                
                NSString *labelStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label6_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
                
                if (labelStr)
                    key_label6.text =labelStr;
                
                key_label6.textAlignment = NSTextAlignmentLeft;
                if([NSLocalizedStringFromTableInBundle(@"popup_template6_label6_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    // Properties for label TextColor.
                    
                    NSArray *label_TextColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_label6_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label6_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        key_label6.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label6_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label6_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label6_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label6_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        key_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        key_label6.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        key_label6.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        key_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                else
                {
                    //Default Properties for label textcolor
                    
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        key_label6.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        key_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        key_label6.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        key_label6.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        key_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                [popupTemplateScrollView addSubview:key_label6];
                next_Y_Position = next_Y_Position+key_label6.frame.size.height-4;
                numberOfFields++;
            }
    
            if (![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value6_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value6_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"] && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value6_param_type",propertyFileName,[NSBundle mainBundle], nil)])
            {
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                [tempDictionary setObject:@"" forKey:@"validation_type"];
                [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value6_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
                if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value6_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                    [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value6_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
                [labelValidationsArray addObject:tempDictionary];

                value_label6 = [[UILabel alloc] init];
                value_label6.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
                validationsCount++;
                value_label6.tag = validationsCount;
                value_label6.backgroundColor = [UIColor clearColor];
                
               
                
                NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value6_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
                
                if (valueStr)
                    value_label6.text = valueStr;
                else
                    value_label6.text = application_default_no_value_available;

                
                value_label6.textAlignment = NSTextAlignmentLeft;
                if([NSLocalizedStringFromTableInBundle(@"popup_template6_value6_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    // Properties for label TextColor.
                    
                    NSArray *label_TextColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_value6_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value6_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        value_label6.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value6_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value6_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value6_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value6_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        value_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        value_label6.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        value_label6.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        value_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                else
                {
                    //Default Properties for label textcolor
                    
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        value_label6.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        value_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        value_label6.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        value_label6.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        value_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                [popupTemplateScrollView addSubview:value_label6];
                next_Y_Position = next_Y_Position+value_label6.frame.size.height;
                borderLabel_Y = value_label6.frame.origin.y+value_label6.frame.size.height;
                numberOfFields++;
            }
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field6_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label6 = [[UILabel alloc] init];
            value_border_label6.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label6.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label6];
            next_Y_Position = next_Y_Position+value_border_label6.frame.size.height;
        }
        }
    }
    // ------------------- 7th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field7_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label7_param_type",propertyFileName,[NSBundle mainBundle], nil)] && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value7_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
            if (![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label7_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label7_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"] && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label7_param_type",propertyFileName,[NSBundle mainBundle], nil)])
            {
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                [tempDictionary setObject:@"" forKey:@"validation_type"];
                [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_label7_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
                if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label7_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                    [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label7_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
                [labelValidationsArray addObject:tempDictionary];

                key_label7 = [[UILabel alloc] init];
                key_label7.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
                key_label7.backgroundColor = [UIColor clearColor];
                
                NSString *labelStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label7_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
                
                if (labelStr)
                    key_label7.text =labelStr;
                
                
                if([NSLocalizedStringFromTableInBundle(@"popup_template6_label7_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    // Properties for label TextColor.
                    
                    NSArray *label_TextColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_label7_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label7_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        key_label7.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label7_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label7_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label7_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label7_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        key_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        key_label7.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        key_label7.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        key_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                else
                {
                    
                    //Default Properties for label textcolor
                    
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        key_label7.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        key_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        key_label7.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        key_label7.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        key_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                key_label7.textAlignment = NSTextAlignmentLeft;
                [popupTemplateScrollView addSubview:key_label7];
                next_Y_Position = next_Y_Position+key_label7.frame.size.height-4;
                numberOfFields++;
            }

            if (![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value7_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value7_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"] && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value7_param_type",propertyFileName,[NSBundle mainBundle], nil)])
            {
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                [tempDictionary setObject:@"" forKey:@"validation_type"];
                [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value7_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
                if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value7_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                    [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value7_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
                [labelValidationsArray addObject:tempDictionary];

                value_label7 = [[UILabel alloc] init];
                value_label7.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
                validationsCount++;
                value_label7.tag = validationsCount;
                value_label7.backgroundColor = [UIColor clearColor];
                
               
                
                NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value7_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
                
                if (valueStr)
                    value_label7.text = [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value7_param_type",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    value_label7.text = application_default_no_value_available;
                
                value_label7.textAlignment = NSTextAlignmentLeft;
                if([NSLocalizedStringFromTableInBundle(@"popup_template6_value7_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    // Properties for label TextColor.
                    NSArray *label_TextColor;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_value7_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value7_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        value_label7.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value7_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value7_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value7_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value7_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        value_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        value_label7.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        value_label7.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        value_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                else
                {
                    
                    //Default Properties for label textcolor
                    
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        value_label7.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        value_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        value_label7.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        value_label7.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        value_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                [popupTemplateScrollView addSubview:value_label7];
                next_Y_Position = next_Y_Position+value_label7.frame.size.height;
                borderLabel_Y = value_label7.frame.origin.y+value_label7.frame.size.height;
                numberOfFields++;
            }
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field7_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label7 = [[UILabel alloc] init];
            value_border_label7.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label7.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label7];
            next_Y_Position = next_Y_Position+value_border_label7.frame.size.height;
        }
        }
    }
    // ------------------- 8th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field8_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label8_param_type",propertyFileName,[NSBundle mainBundle], nil)] && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value8_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
            if (![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label8_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label8_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"] && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label8_param_type",propertyFileName,[NSBundle mainBundle], nil)])
            {
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                [tempDictionary setObject:@"" forKey:@"validation_type"];
                [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_label8_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
                if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label8_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                    [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label8_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
                [labelValidationsArray addObject:tempDictionary];

                key_label8 = [[UILabel alloc] init];
                key_label8.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
                key_label8.backgroundColor = [UIColor clearColor];
                
                NSString *labelStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label8_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
                
                if (labelStr)
                    key_label8.text =labelStr;
                
                key_label8.textAlignment = NSTextAlignmentLeft;
                
                
                if([NSLocalizedStringFromTableInBundle(@"popup_template6_label8_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    // Properties for label TextColor.
                    
                    NSArray *label_TextColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_label8_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label8_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        key_label8.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label8_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label8_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label8_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label8_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        key_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        key_label8.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        key_label8.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        key_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                }
                else
                {
                    //Default Properties for label textcolor
                    
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        key_label8.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        key_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        key_label8.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        key_label8.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        key_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                }
                [popupTemplateScrollView addSubview:key_label8];
                next_Y_Position = next_Y_Position+key_label8.frame.size.height-4;
                numberOfFields++;
            }
       
            if (![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value8_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value8_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"] && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value8_param_type",propertyFileName,[NSBundle mainBundle], nil)])
            {
                NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
                [tempDictionary setObject:@"" forKey:@"validation_type"];
                [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value8_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
                if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value8_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                    [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value8_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
                [labelValidationsArray addObject:tempDictionary];

                value_label8 = [[UILabel alloc] init];
                value_label8.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
                validationsCount++;
                value_label8.tag = validationsCount;
                value_label8.backgroundColor = [UIColor clearColor];
                
               
                
                NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value8_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
                
                if (valueStr)
                    value_label8.text = valueStr;
                else
                    value_label8.text = application_default_no_value_available;

                value_label8.textAlignment = NSTextAlignmentLeft;
                if([NSLocalizedStringFromTableInBundle(@"popup_template6_value8_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    // Properties for label TextColor.
                    NSArray *label_TextColor;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_value8_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value8_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        value_label8.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value8_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value8_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value8_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value8_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        value_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        value_label8.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        value_label8.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        value_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                }
                else
                {
                    //Default Properties for label textcolor
                    
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        value_label8.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        value_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        value_label8.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        value_label8.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        value_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                    
                }
                [popupTemplateScrollView addSubview:value_label8];
                next_Y_Position = next_Y_Position+value_label8.frame.size.height;
                borderLabel_Y = value_label8.frame.origin.y+value_label8.frame.size.height;
                numberOfFields++;
                [labelValidationsArray addObject:tempDictionary];
            }
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field8_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label8 = [[UILabel alloc] init];
            value_border_label8.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label8.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label8];
            next_Y_Position = next_Y_Position+value_border_label8.frame.size.height;
        }
        }
    }
    // ------------------- 9th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field9_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if (![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value9_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value9_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"] && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value9_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
         NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value9_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
    
            key_label9 = [[UILabel alloc] init];
            key_label9.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            key_label9.backgroundColor = [UIColor clearColor];
            NSString *labelStr;
            
           if (![NSLocalizedStringFromTableInBundle(@"popup_template6_label9",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@""])
                 labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label9",propertyFileName,[NSBundle mainBundle], nil)];
           
            if (labelStr)
                key_label9.text=labelStr;
            
            key_label9.textAlignment = NSTextAlignmentLeft;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_label9_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label9_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label9_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label9.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label9_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label9_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label9_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label9_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label9.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label9.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label9.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label9.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label9.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:key_label9];
            next_Y_Position = next_Y_Position+key_label9.frame.size.height-4;
            numberOfFields++;
        
            value_label9 = [[UILabel alloc] init];
            value_label9.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
            validationsCount++;
            value_label9.tag = validationsCount;
            value_label9.backgroundColor = [UIColor clearColor];
            
           
            
            NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value9_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
            
            if (valueStr)
                value_label9.text = valueStr;
            else
                value_label9.text = application_default_no_value_available;
            
            value_label9.textAlignment = NSTextAlignmentLeft;
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_value9_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                NSArray *label_TextColor;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value9_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value9_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label9.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value9_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value9_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value9_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value9_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label9.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label9.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label9.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label9.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label9.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:value_label9];
            next_Y_Position = next_Y_Position+value_label9.frame.size.height;
            borderLabel_Y = value_label9.frame.origin.y+value_label9.frame.size.height;
            if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value9_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value9_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
            numberOfFields++;
    
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field9_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label9 = [[UILabel alloc] init];
            value_border_label9.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label9.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label9];
            next_Y_Position = next_Y_Position+value_border_label9.frame.size.height;
        }
        [labelValidationsArray addObject:tempDictionary];
        }
    }
    // ------------------- 10th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field10_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ((![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value10_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value10_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"]) && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value10_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value10_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
    
            key_label10 = [[UILabel alloc] init];
            key_label10.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            key_label10.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label10",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                key_label10.text=labelStr;

            key_label10.textAlignment = NSTextAlignmentLeft;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_label10_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label10_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label10_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label10.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label10_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label10_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label10_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label10_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label10.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label10.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label10.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label10.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label10.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:key_label10];
            next_Y_Position = next_Y_Position+key_label10.frame.size.height-4;
            numberOfFields++;

            value_label10 = [[UILabel alloc] init];
            value_label10.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
            validationsCount++;
            value_label10.tag = validationsCount;
            value_label10.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value10_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
            
            if (valueStr)
                value_label10.text = valueStr;
            else
                value_label10.text = application_default_no_value_available;

            value_label10.textAlignment = NSTextAlignmentLeft;
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_value10_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                NSArray *label_TextColor;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value10_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value10_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label10.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value10_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value10_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value10_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value10_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label10.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label10.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label10.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label10.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label10.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:value_label10];
            next_Y_Position = next_Y_Position+value_label10.frame.size.height;
            borderLabel_Y = value_label10.frame.origin.y+value_label10.frame.size.height;
            if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value10_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value10_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
            numberOfFields++;
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field10_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label10 = [[UILabel alloc] init];
            value_border_label10.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label10.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label10];
            next_Y_Position = next_Y_Position+value_border_label10.frame.size.height;
        }
        [labelValidationsArray addObject:tempDictionary];
        }
    }
    // ------------------- 11th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field11_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ((![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value11_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value11_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"]) && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value11_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
            NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
            [tempDictionary setObject:@"" forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value11_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            key_label11 = [[UILabel alloc] init];
            key_label11.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            key_label11.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label11",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                key_label11.text=labelStr;
            
            key_label11.textAlignment = NSTextAlignmentLeft;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_label11_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label11_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label11_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label11.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label11_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label11_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label11_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label11_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label11.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label11.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label11.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label11.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label11.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label11.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label11.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label11.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label11.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:key_label11];
            next_Y_Position = next_Y_Position+key_label11.frame.size.height-4;
            numberOfFields++;
            
            value_label11 = [[UILabel alloc] init];
            value_label11.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
            validationsCount++;
            value_label11.tag = validationsCount;
            value_label11.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value11_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
            
            if (valueStr)
                value_label11.text = valueStr;
            else
                value_label11.text = application_default_no_value_available;
            
            value_label11.textAlignment = NSTextAlignmentLeft;
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_value11_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                NSArray *label_TextColor;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value11_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value11_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label11.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value11_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value11_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value11_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value11_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label11.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label11.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label11.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label11.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label11.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label11.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label11.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label11.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label11.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:value_label11];
            next_Y_Position = next_Y_Position+value_label11.frame.size.height;
            borderLabel_Y = value_label11.frame.origin.y+value_label11.frame.size.height;
            if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value11_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value11_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
            numberOfFields++;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field11_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                value_border_label11 = [[UILabel alloc] init];
                value_border_label11.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
                value_border_label11.backgroundColor = [UIColor blackColor];
                [popupTemplateScrollView addSubview:value_border_label11];
                next_Y_Position = next_Y_Position+value_border_label11.frame.size.height;
            }
            [labelValidationsArray addObject:tempDictionary];
        }
    }

    // ------------------- 12th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field12_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ((![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value12_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value12_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"]) && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value12_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
            NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
            [tempDictionary setObject:@"" forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value12_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            key_label12 = [[UILabel alloc] init];
            key_label12.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            key_label12.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label12",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                key_label12.text=labelStr;
            
            key_label12.textAlignment = NSTextAlignmentLeft;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_label12_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label12_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label12_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label12.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label12_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label12_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label12_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label12_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label12.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label12.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label12.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label12.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label12.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label12.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label12.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label12.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label12.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:key_label12];
            next_Y_Position = next_Y_Position+key_label12.frame.size.height-4;
            numberOfFields++;
            
            value_label12 = [[UILabel alloc] init];
            value_label12.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
            validationsCount++;
            value_label12.tag = validationsCount;
            value_label12.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value12_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
            
            if (valueStr)
                value_label12.text = valueStr;
            else
                value_label12.text = application_default_no_value_available;
            
            value_label12.textAlignment = NSTextAlignmentLeft;
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_value12_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                NSArray *label_TextColor;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value12_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value12_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label12.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value12_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value12_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value12_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value12_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label12.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label12.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label12.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label12.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label12.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label12.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label12.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label12.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label12.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:value_label12];
            next_Y_Position = next_Y_Position+value_label12.frame.size.height;
            borderLabel_Y = value_label12.frame.origin.y+value_label12.frame.size.height;
            if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value12_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value12_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
            numberOfFields++;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field12_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                value_border_label12 = [[UILabel alloc] init];
                value_border_label12.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
                value_border_label12.backgroundColor = [UIColor blackColor];
                [popupTemplateScrollView addSubview:value_border_label12];
                next_Y_Position = next_Y_Position+value_border_label12.frame.size.height;
            }
            [labelValidationsArray addObject:tempDictionary];
        }
    }
    // ------------------- 13th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field13_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ((![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value13_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value13_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"]) && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value13_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
            NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
            [tempDictionary setObject:@"" forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value13_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            key_label13 = [[UILabel alloc] init];
            key_label13.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            key_label13.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label13",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                key_label13.text=labelStr;
            
            key_label13.textAlignment = NSTextAlignmentLeft;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_label13_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label13_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label13_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label13.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label13_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label13_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label13_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label13_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label13.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label13.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label13.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label13.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label13.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label13.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label13.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label13.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label13.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:key_label13];
            next_Y_Position = next_Y_Position+key_label13.frame.size.height-4;
            numberOfFields++;
            
            value_label13 = [[UILabel alloc] init];
            value_label13.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
            validationsCount++;
            value_label13.tag = validationsCount;
            value_label13.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value13_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
            
            if (valueStr)
                value_label13.text = valueStr;
            else
                value_label13.text = application_default_no_value_available;
            
            value_label13.textAlignment = NSTextAlignmentLeft;
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_value13_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                NSArray *label_TextColor;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value13_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value13_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label13.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value13_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value13_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value13_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value13_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label13.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label13.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label13.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label13.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label13.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label13.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label13.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label13.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label13.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:value_label13];
            next_Y_Position = next_Y_Position+value_label13.frame.size.height;
            borderLabel_Y = value_label13.frame.origin.y+value_label13.frame.size.height;
            if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value13_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value13_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
            numberOfFields++;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field13_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                value_border_label13 = [[UILabel alloc] init];
                value_border_label13.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
                value_border_label13.backgroundColor = [UIColor blackColor];
                [popupTemplateScrollView addSubview:value_border_label13];
                next_Y_Position = next_Y_Position+value_border_label13.frame.size.height;
            }
            [labelValidationsArray addObject:tempDictionary];
        }
    }
    // ------------------- 14th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field14_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ((![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value14_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value14_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"]) && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value14_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
            NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
            [tempDictionary setObject:@"" forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value14_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            key_label14 = [[UILabel alloc] init];
            key_label14.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            key_label14.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label14",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                key_label14.text=labelStr;
            
            key_label14.textAlignment = NSTextAlignmentLeft;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_label14_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label14_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label14_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label14.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label14_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label14_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label14_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label14_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label14.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label14.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label14.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label14.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label14.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label14.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label14.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label14.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label14.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:key_label14];
            next_Y_Position = next_Y_Position+key_label14.frame.size.height-4;
            numberOfFields++;
            
            value_label14 = [[UILabel alloc] init];
            value_label14.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
            validationsCount++;
            value_label14.tag = validationsCount;
            value_label14.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value14_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
            
            if (valueStr)
                value_label14.text = valueStr;
            else
                value_label14.text = application_default_no_value_available;
            
            value_label14.textAlignment = NSTextAlignmentLeft;
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_value14_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                NSArray *label_TextColor;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value14_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value14_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label14.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value14_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value14_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value14_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value14_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label14.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label14.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label14.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label14.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label14.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label14.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label14.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label14.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label14.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:value_label14];
            next_Y_Position = next_Y_Position+value_label14.frame.size.height;
            borderLabel_Y = value_label14.frame.origin.y+value_label14.frame.size.height;
            if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value14_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value14_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
            numberOfFields++;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field13_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                value_border_label14 = [[UILabel alloc] init];
                value_border_label14.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
                value_border_label14.backgroundColor = [UIColor blackColor];
                [popupTemplateScrollView addSubview:value_border_label14];
                next_Y_Position = next_Y_Position+value_border_label14.frame.size.height;
            }
            [labelValidationsArray addObject:tempDictionary];
        }
    }
    // ------------------- 15th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field15_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ((![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value15_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value15_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"]) && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value15_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
            NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
            [tempDictionary setObject:@"" forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value15_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            key_label15 = [[UILabel alloc] init];
            key_label15.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            key_label15.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label15",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                key_label15.text=labelStr;
            
            key_label15.textAlignment = NSTextAlignmentLeft;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_label15_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label15_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label15_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label15.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label15_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label15_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label15_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label15_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label15.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label15.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label15.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label15.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label15.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label15.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label15.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label15.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label15.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:key_label15];
            next_Y_Position = next_Y_Position+key_label15.frame.size.height-4;
            numberOfFields++;
            
            value_label15 = [[UILabel alloc] init];
            value_label15.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
            validationsCount++;
            value_label15.tag = validationsCount;
            value_label15.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value15_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
            
            if (valueStr)
                value_label15.text = valueStr;
            else
                value_label15.text = application_default_no_value_available;
            
            value_label15.textAlignment = NSTextAlignmentLeft;
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_value15_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                NSArray *label_TextColor;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value15_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value15_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label15.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value15_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value15_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value15_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value15_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label15.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label15.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label15.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label15.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label15.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label15.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label15.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label15.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label15.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:value_label15];
            next_Y_Position = next_Y_Position+value_label15.frame.size.height;
            borderLabel_Y = value_label15.frame.origin.y+value_label15.frame.size.height;
            if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value15_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value15_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
            numberOfFields++;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field13_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                value_border_label15 = [[UILabel alloc] init];
                value_border_label15.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
                value_border_label15.backgroundColor = [UIColor blackColor];
                [popupTemplateScrollView addSubview:value_border_label15];
                next_Y_Position = next_Y_Position+value_border_label15.frame.size.height;
            }
            [labelValidationsArray addObject:tempDictionary];
        }
    }
    
    // ------------------- 16th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field16_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ((![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value16_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value16_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"]) && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value16_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
            NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
            [tempDictionary setObject:@"" forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value16_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            key_label16 = [[UILabel alloc] init];
            key_label16.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            key_label16.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label16",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                key_label16.text=labelStr;
            
            key_label16.textAlignment = NSTextAlignmentLeft;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_label16_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label16_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label16_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label16.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label16_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label16_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label16_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label16_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label16.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label16.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label16.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label16.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label16.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label16.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label16.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label16.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label16.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:key_label16];
            next_Y_Position = next_Y_Position+key_label16.frame.size.height-4;
            numberOfFields++;
            
            value_label16 = [[UILabel alloc] init];
            value_label16.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
            validationsCount++;
            value_label16.tag = validationsCount;
            value_label16.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value16_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
            
            if (valueStr)
                value_label16.text = valueStr;
            else
                value_label16.text = application_default_no_value_available;
            
            value_label16.textAlignment = NSTextAlignmentLeft;
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_value16_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                NSArray *label_TextColor;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value16_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value16_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label16.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value16_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value16_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value16_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value16_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label16.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label16.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label16.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label16.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label16.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label16.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label16.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label16.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label16.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:value_label16];
            next_Y_Position = next_Y_Position+value_label16.frame.size.height;
            borderLabel_Y = value_label16.frame.origin.y+value_label16.frame.size.height;
            if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value16_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value16_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
            numberOfFields++;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field13_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                value_border_label16 = [[UILabel alloc] init];
                value_border_label16.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
                value_border_label16.backgroundColor = [UIColor blackColor];
                [popupTemplateScrollView addSubview:value_border_label16];
                next_Y_Position = next_Y_Position+value_border_label16.frame.size.height;
            }
            [labelValidationsArray addObject:tempDictionary];
        }
    }
    // ------------------- 17th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field17_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ((![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value17_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value17_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"]) && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value17_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
            NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
            [tempDictionary setObject:@"" forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value17_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            key_label17 = [[UILabel alloc] init];
            key_label17.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            key_label17.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label17",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                key_label17.text=labelStr;
            
            key_label17.textAlignment = NSTextAlignmentLeft;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_label17_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label17_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label17_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label17.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label17_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label17_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label17_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label17_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label17.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label17.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label17.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label17.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label17.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label17.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label17.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label17.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label17.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:key_label17];
            next_Y_Position = next_Y_Position+key_label17.frame.size.height-4;
            numberOfFields++;
            
            value_label17 = [[UILabel alloc] init];
            value_label17.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
            validationsCount++;
            value_label17.tag = validationsCount;
            value_label17.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value17_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
            
            if (valueStr)
                value_label17.text = valueStr;
            else
                value_label17.text = application_default_no_value_available;
            
            value_label17.textAlignment = NSTextAlignmentLeft;
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_value17_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                NSArray *label_TextColor;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value17_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value17_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label17.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value17_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value17_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value17_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value17_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label17.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label17.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label17.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label17.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label17.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label17.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label17.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label17.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label17.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:value_label17];
            next_Y_Position = next_Y_Position+value_label17.frame.size.height;
            borderLabel_Y = value_label17.frame.origin.y+value_label17.frame.size.height;
            if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value17_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value17_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
            numberOfFields++;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field13_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                value_border_label17 = [[UILabel alloc] init];
                value_border_label17.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
                value_border_label17.backgroundColor = [UIColor blackColor];
                [popupTemplateScrollView addSubview:value_border_label17];
                next_Y_Position = next_Y_Position+value_border_label17.frame.size.height;
            }
            [labelValidationsArray addObject:tempDictionary];
        }
    }
    // ------------------- 18th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field18_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ((![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value18_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""] && ![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value18_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@"(null)"]) && [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value18_param_type",propertyFileName,[NSBundle mainBundle], nil)])
        {
            NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
            [tempDictionary setObject:@"" forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_value18_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            key_label18 = [[UILabel alloc] init];
            key_label18.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            key_label18.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_label18",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                key_label18.text=labelStr;
            
            key_label18.textAlignment = NSTextAlignmentLeft;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_label18_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label18_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label18_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label18.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label18_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label18_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label18_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label18_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label18.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label18.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label18.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label18.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label18.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label18.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label18.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label18.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label18.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:key_label18];
            next_Y_Position = next_Y_Position+key_label18.frame.size.height-4;
            numberOfFields++;
            
            value_label18 = [[UILabel alloc] init];
            value_label18.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
            validationsCount++;
            value_label18.tag = validationsCount;
            value_label18.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value18_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
            
            if (valueStr)
                value_label18.text = valueStr;
            else
                value_label18.text = application_default_no_value_available;
            
            value_label18.textAlignment = NSTextAlignmentLeft;
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_value18_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                NSArray *label_TextColor;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value18_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value18_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label18.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value18_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value18_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value18_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value18_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label18.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label18.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label18.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label18.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label18.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label18.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label18.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label18.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label18.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:value_label18];
            next_Y_Position = next_Y_Position+value_label18.frame.size.height;
            borderLabel_Y = value_label18.frame.origin.y+value_label18.frame.size.height;
            if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value18_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                [tempDictionary setObject:[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_value18_param_type",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"value"];
            numberOfFields++;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_labels_field18_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                value_border_label18 = [[UILabel alloc] init];
                value_border_label18.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
                value_border_label18.backgroundColor = [UIColor blackColor];
                [popupTemplateScrollView addSubview:value_border_label18];
                next_Y_Position = next_Y_Position+value_border_label18.frame.size.height;
            }
            [labelValidationsArray addObject:tempDictionary];
        }
    }
    
    
    // ------------------- Non Editable Ends ------------------- //
    
    // ------------------- Editable Starts ------------------- //
    
    // ------------------- 1st Value(key label,text field , Dropdown value and Border label) ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_field1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        input_key_label1 = [[UILabel alloc] init];
        input_key_label1.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
        
        input_key_label1.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_input_label1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            input_key_label1.text=labelStr;
        
            
        input_key_label1.textAlignment = NSTextAlignmentLeft;
        if([NSLocalizedStringFromTableInBundle(@"popup_template6_input_label1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_label1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_label1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                input_key_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_label1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_label1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_label1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_label1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                input_key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                input_key_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                input_key_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                input_key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                input_key_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                input_key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                input_key_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                input_key_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                input_key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [popupTemplateScrollView addSubview:input_key_label1];
        next_Y_Position = input_key_label1.frame.origin.y+input_key_label1.frame.size.height-4;
        numberOfFields++;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_field1 = [[CustomTextField alloc] init];
            input_value_field1.delegate = self;
            input_value_field1.tag = inputValidationsCount;
            inputValidationsCount++;
            input_value_field1.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
            if([propertyFileName isEqualToString:@"VerifyRegistrationPPT6_T2"]){
                input_key_label1.frame = CGRectMake(input_key_label1.frame.origin.x, input_key_label1.frame.origin.y, input_key_label1.frame.size.width/3, input_key_label1.frame.size.height);
                input_value_field1.frame = CGRectMake(X_Position+input_key_label1.frame.size.width+20, input_key_label1.frame.origin.y+5, popupTemplateScrollView.frame.size.width/2, 30);
            }
            input_value_field1.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                input_value_field1.placeholder =valueStr;
            
            input_value_field1.textAlignment = NSTextAlignmentLeft;
            
            if ([localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_param_type",propertyFileName,[NSBundle mainBundle], nil)])
                input_value_field1.text = [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_param_type",propertyFileName,[NSBundle mainBundle], nil)];
            
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:PARAMETER21])
            {
                if ([[localDataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_FIXED] )
                {
                    input_value_field1.userInteractionEnabled = NO;
                }
                else if ([[localDataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID])
                {
                    input_value_field1.userInteractionEnabled = YES;
                }
                else if ([[localDataDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_VARIABLE])
                {
                    input_value_field1.userInteractionEnabled = YES;
                }
            }
            
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([input_value_field1 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        input_value_field1.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                    
//
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField_TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField_TextColor)
                    input_value_field1.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField_TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField_TextColor)
                    input_value_field1.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            // property for KeyBoardType
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                input_value_field1.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                input_value_field1.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                input_value_field1.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                input_value_field1.secureTextEntry=YES;
                input_value_field1.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:NUMBERKEYBOARDTYPE_4])
                input_value_field1.keyboardType = UIKeyboardTypeNumberPad;
            else
                input_value_field1.keyboardType = UIKeyboardTypeDefault;
            
            input_value_field1.inputAccessoryView = numberToolbar;
            
            [popupTemplateScrollView addSubview:input_value_field1];
            if([propertyFileName isEqualToString:@"VerifyRegistrationPPT6_T2"]){
                input_value_field1.secureTextEntry = YES;
            }
            next_Y_Position = input_value_field1.frame.origin.y+input_value_field1.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value1_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label1.text forKey:@"labelName"];
            
            if ([input_value_field1 text])
                [tempDictionary setObject:[input_value_field1 text] forKey:@"value"];
        }
        else if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value1_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton1 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton1 setFrame:CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35)];
            
            NSString *dropdownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value1_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropdownStr) {
                [dropDownButton1 setTitle:dropdownStr forState:UIControlStateNormal];
                [dropDownButton1 setTitle:dropdownStr forState:UIControlStateHighlighted];
            }
            [dropDownButton1 setTag:inputValidationsCount];
            inputValidationsCount++;
            dropDownButton1.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
           
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton1 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value1_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton1.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [dropDownButton1 setExclusiveTouch:YES];
            dropDownButton1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton1.frame.size.width-dropDownButton1.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton1.frame.size.width-dropDownButton1.intrinsicContentSize.width-8.0));
                [dropDownButton1 setTitleEdgeInsets:titleInsets];
                dropDownButton1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton1 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [popupTemplateScrollView addSubview:dropDownButton1];
            
            [[dropDownButton1 layer] setBorderWidth:0.5f];
            [[dropDownButton1 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton1.frame.size.width-20,next_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [popupTemplateScrollView addSubview:imageView];
            
            next_Y_Position = dropDownButton1.frame.origin.y+dropDownButton1.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value1_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label1.text forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton1.titleLabel.text forKey:@"value"];
        }
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_field1_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_border_label1 = [[UILabel alloc] init];
            input_value_border_label1.frame = CGRectMake(X_Position, next_Y_Position+3, popupTemplateScrollView.frame.size.width-10,1);
            input_value_border_label1.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:input_value_border_label1];
            if([propertyFileName isEqualToString:@"VerifyRegistrationPPT6_T2"]){
                [input_value_border_label1 removeFromSuperview];
            }
            next_Y_Position = next_Y_Position+input_value_border_label1.frame.size.height;
        }
        numberOfFields++;
        [validationsArray addObject:tempDictionary];
    }
    // ------------------- 2nd Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_field2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        {
            input_key_label2 = [[UILabel alloc] init];
            input_key_label2.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            input_key_label2.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_input_label2_text",propertyFileName,[NSBundle mainBundle], nil)];
            
            if(labelStr)
                input_key_label2.text = labelStr;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_input_label2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_label2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_label2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    input_key_label2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_label2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_label2_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_label2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_label2_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_key_label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_key_label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    input_key_label2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_key_label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_key_label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:input_key_label2];
            next_Y_Position = input_key_label2.frame.origin.y+input_key_label2.frame.size.height;
        }
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_value2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_field2 = [[CustomTextField alloc] init];
            input_value_field2.delegate = self;
            input_value_field2.tag = inputValidationsCount;
            inputValidationsCount++;
            input_value_field2.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
            input_value_field2.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value2_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                input_value_field2.placeholder =valueStr;
            
            input_value_field2.textAlignment = NSTextAlignmentLeft;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_value2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([input_value_field2 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value2_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value2_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        input_value_field2.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
//
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField_TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField_TextColor)
                    input_value_field2.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value2_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value2_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField_TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField_TextColor)
                    input_value_field2.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            // property for KeyBoardType
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value2_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value2_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                input_value_field2.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                input_value_field2.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                input_value_field2.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                input_value_field2.secureTextEntry=YES;
                input_value_field2.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:NUMBERKEYBOARDTYPE_4])
                input_value_field2.keyboardType = UIKeyboardTypeNumberPad;
            else
                input_value_field2.keyboardType = UIKeyboardTypeDefault;
            
            input_value_field2.inputAccessoryView = numberToolbar;
            [popupTemplateScrollView addSubview:input_value_field2];
            
            next_Y_Position = input_value_field2.frame.origin.y+input_value_field2.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value2_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value2_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label2.text forKey:@"labelName"];
            if ([input_value_field2 text])
                [tempDictionary setObject:[input_value_field2 text] forKey:@"value"];
            
        }
        else if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton2 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton2 setFrame:CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35)];
            
            NSString *dropdownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value2_hint",propertyFileName,[NSBundle mainBundle], nil)];
            if (dropdownStr) {
                [dropDownButton2 setTitle:dropdownStr forState:UIControlStateNormal];
                [dropDownButton2 setTitle:dropdownStr forState:UIControlStateHighlighted];
            }
            [dropDownButton2 setTag:inputValidationsCount];
            inputValidationsCount++;
            dropDownButton2.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton2 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value2_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value2_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                   dropDownButton2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton2.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [dropDownButton2 setExclusiveTouch:YES];
            dropDownButton2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton2.frame.size.width-dropDownButton2.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton2.frame.size.width-dropDownButton2.intrinsicContentSize.width-8.0));
                [dropDownButton2 setTitleEdgeInsets:titleInsets];
                dropDownButton2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton2 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [popupTemplateScrollView addSubview:dropDownButton2];
            
            [[dropDownButton2 layer] setBorderWidth:0.5f];
            [[dropDownButton2 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton2.frame.size.width-20,next_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [popupTemplateScrollView addSubview:imageView];
            
            next_Y_Position = dropDownButton2.frame.origin.y+dropDownButton2.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value2_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label2.text forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton2.titleLabel.text forKey:@"value"];
        }
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_field2_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_border_label2 = [[UILabel alloc] init];
            input_value_border_label2.frame = CGRectMake(X_Position, next_Y_Position+3, popupTemplateScrollView.frame.size.width-10,1);
            input_value_border_label2.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:input_value_border_label2];
            next_Y_Position = next_Y_Position+input_value_border_label2.frame.size.height;
        }
        numberOfFields++;
        [validationsArray addObject:tempDictionary];
    }
    
    // ------------------- 3rd Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_field3_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        
        {
            input_key_label3 = [[UILabel alloc] init];
            input_key_label3.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            input_key_label3.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_input_label3_text",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                input_key_label3.text=labelStr;

            if([NSLocalizedStringFromTableInBundle(@"popup_template6_input_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_label3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_label3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    input_key_label3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_label3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_label3_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_label3_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_label3_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_key_label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_key_label3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            else
            {
                //Default Properties for label textcolor
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    input_key_label3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_key_label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_key_label3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [popupTemplateScrollView addSubview:input_key_label3];
            next_Y_Position = input_key_label3.frame.origin.y+input_key_label3.frame.size.height;
        }
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_value3_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_field3 = [[CustomTextField alloc] init];
            input_value_field3.delegate = self;
            input_value_field3.tag = inputValidationsCount;
            inputValidationsCount++;
            input_value_field3.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
            input_value_field3.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value3_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                input_value_field3.placeholder =valueStr ;
            
            input_value_field3.textAlignment = NSTextAlignmentLeft;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_value3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([input_value_field3 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value3_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value3_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        input_value_field3.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }

                
                // Properties for textField TextColor.
                
                NSArray *textField_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField_TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField_TextColor)
                    input_value_field3.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value3_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value3_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField_TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField_TextColor)
                    input_value_field3.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value3_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value3_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                input_value_field3.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                input_value_field3.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                input_value_field3.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                input_value_field3.secureTextEntry=YES;
                input_value_field3.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:NUMBERKEYBOARDTYPE_4])
                input_value_field3.keyboardType = UIKeyboardTypeNumberPad;
            else
                input_value_field3.keyboardType = UIKeyboardTypeDefault;
            
            
            input_value_field3.inputAccessoryView = numberToolbar;
            [popupTemplateScrollView addSubview:input_value_field3];
            next_Y_Position = input_value_field3.frame.origin.y+input_value_field3.frame.size.height;
            
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value3_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value3_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label3.text forKey:@"labelName"];
            if ([input_value_field3 text])
                [tempDictionary setObject:[input_value_field3 text] forKey:@"value"];
            
        }
        else if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value3_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton3 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton3 setFrame:CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35)];
            
            NSString *dropdownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value3_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropdownStr) {
                [dropDownButton3 setTitle:dropdownStr forState:UIControlStateNormal];
                [dropDownButton3 setTitle:dropdownStr forState:UIControlStateHighlighted];
            }
            [dropDownButton3 setTag:inputValidationsCount];
            inputValidationsCount++;
            dropDownButton3.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;

            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton3 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value3_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value3_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value3_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton3.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton3.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton3.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton3.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton3.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton3.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton3.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton3.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton3.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            [dropDownButton3 setExclusiveTouch:YES];
            dropDownButton3.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton3.frame.size.width-dropDownButton3.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton3.frame.size.width-dropDownButton3.intrinsicContentSize.width-8.0));
                [dropDownButton3 setTitleEdgeInsets:titleInsets];
                dropDownButton3.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton3 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [popupTemplateScrollView addSubview:dropDownButton3];
            
            [[dropDownButton3 layer] setBorderWidth:0.5f];
            [[dropDownButton3 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton3.frame.size.width-20,next_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [popupTemplateScrollView addSubview:imageView];
            
            next_Y_Position = dropDownButton3.frame.origin.y+dropDownButton3.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value3_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label3.text forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton3.titleLabel.text forKey:@"value"];
        }
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_field3_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_border_label3 = [[UILabel alloc] init];
            input_value_border_label3.frame = CGRectMake(X_Position, next_Y_Position+3, popupTemplateScrollView.frame.size.width-10,1);
            input_value_border_label3.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:input_value_border_label3];
            next_Y_Position = next_Y_Position+input_value_border_label3.frame.size.height;
        }
        numberOfFields++;
        [validationsArray addObject:tempDictionary];
    }
    
    // ------------------- 4th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_field4_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        {
            input_key_label4 = [[UILabel alloc] init];
            input_key_label4.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            input_key_label4.backgroundColor = [UIColor clearColor];

            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_input_label4_text",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                input_key_label4.text=labelStr;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_input_label4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_label4_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_label4_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    input_key_label4.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_label4_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_label4_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_label4_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_label4_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_key_label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_key_label4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    input_key_label4.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_key_label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_key_label4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [popupTemplateScrollView addSubview:input_key_label4];
            next_Y_Position = input_key_label4.frame.origin.y+input_key_label4.frame.size.height;
        }
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_value4_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_field4 = [[CustomTextField alloc] init];
            input_value_field4.delegate = self;
            input_value_field4.tag = inputValidationsCount;
            inputValidationsCount++;
            input_value_field4.frame = CGRectMake(X_Position, input_key_label4.frame.origin.y+input_key_label4.frame.size.height-4, popupTemplateScrollView.frame.size.width-10,35);
            input_value_field4.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey: NSLocalizedStringFromTableInBundle(@"popup_template6_input_value4_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                input_value_field4.placeholder =valueStr;
            
            input_value_field4.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_value4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([input_value_field4 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value4_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value4_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        input_value_field4.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value4_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value4_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField_TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField_TextColor)
                    input_value_field4.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value4_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value4_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value4_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value4_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else{
                
                //Default Properties for textfiled textcolor
                
                NSArray *textField_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField_TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField_TextColor)
                    input_value_field4.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            // property for KeyBoardType
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value4_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value4_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                input_value_field4.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                input_value_field4.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                input_value_field4.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                input_value_field4.secureTextEntry=YES;
                input_value_field4.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:NUMBERKEYBOARDTYPE_4])
                input_value_field4.keyboardType = UIKeyboardTypeNumberPad;
            else
                input_value_field4.keyboardType = UIKeyboardTypeDefault;
            
            input_value_field4.inputAccessoryView = numberToolbar;
            [popupTemplateScrollView addSubview:input_value_field4];
            
            next_Y_Position = input_value_field4.frame.origin.y+input_value_field4.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value4_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value4_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label4.text forKey:@"labelName"];
            if ([input_value_field4 text])
                [tempDictionary setObject:[input_value_field4 text] forKey:@"value"];
            
        }
        else if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value4_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton4 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton4 setFrame:CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35)];
            
            NSString *dropdownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value4_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropdownStr) {
                [dropDownButton4 setTitle:dropdownStr forState:UIControlStateNormal];
                [dropDownButton4 setTitle:dropdownStr forState:UIControlStateHighlighted];
            }
            [dropDownButton4 setTag:inputValidationsCount];
            inputValidationsCount++;

            dropDownButton4.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value4_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value4_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton4 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value4_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value4_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value4_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value4_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton4.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton4.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton4.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                   dropDownButton4.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton4.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton4.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                else
                    dropDownButton4.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [dropDownButton4 setExclusiveTouch:YES];
            dropDownButton4.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton4.frame.size.width-dropDownButton4.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton4.frame.size.width-dropDownButton4.intrinsicContentSize.width-8.0));
                [dropDownButton4 setTitleEdgeInsets:titleInsets];
                dropDownButton4.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton4 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [popupTemplateScrollView addSubview:dropDownButton4];
            
            [[dropDownButton4 layer] setBorderWidth:0.5f];
            [[dropDownButton4 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton4.frame.size.width-20,next_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [popupTemplateScrollView addSubview:imageView];
            
            next_Y_Position = dropDownButton4.frame.origin.y+dropDownButton4.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value4_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label4.text forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton4.titleLabel.text forKey:@"value"];
        }
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_field4_border_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_border_label4 = [[UILabel alloc] init];
            input_value_border_label4.frame = CGRectMake(X_Position, next_Y_Position+3, popupTemplateScrollView.frame.size.width-10,1);
            input_value_border_label4.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:input_value_border_label4];
            next_Y_Position = next_Y_Position+input_value_border_label4.frame.size.height;
        }
        numberOfFields++;
        [validationsArray addObject:tempDictionary];
    }
    
    // ------------------- 5th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_field5_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        {
            input_key_label5 = [[UILabel alloc] init];
            input_key_label5.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            input_key_label5.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_input_label5_text",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                input_key_label5.text=labelStr;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template6_input_label5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_label5_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_label5_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    input_key_label5.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_label5_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_label5_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_label5_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_label5_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_key_label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_key_label5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    input_key_label5.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_key_label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_key_label5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:input_key_label5];
            next_Y_Position = input_key_label5.frame.origin.y+input_key_label5.frame.size.height;
        }
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_value5_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_field5 = [[CustomTextField alloc] init];
            input_value_field5.delegate = self;
            input_value_field5.tag = inputValidationsCount;
            inputValidationsCount++;
            input_value_field5.frame = CGRectMake(X_Position, input_key_label5.frame.origin.y+input_key_label5.frame.size.height-4, popupTemplateScrollView.frame.size.width-10,35);
            input_value_field5.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value5_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                input_value_field5.placeholder =valueStr ;
            
            input_value_field5.textAlignment = NSTextAlignmentLeft;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_value5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([input_value_field5 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value5_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value5_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        input_value_field5.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }

                // Properties for textField TextColor.
                
                NSArray *textField_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value5_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value5_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField_TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField_TextColor)
                    input_value_field5.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value5_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value5_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value5_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value5_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
        {
            //Default Properties for textfiled textcolor
            
            NSArray *textField_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                textField_TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
            if (textField_TextColor)
                input_value_field5.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for textfiled textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for textfiled fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_value_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                input_value_field5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                input_value_field5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                input_value_field5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                input_value_field5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
            // property for KeyBoardType
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_value5_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"popup_template6_input_value5_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                input_value_field5.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                input_value_field5.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                input_value_field5.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                input_value_field5.secureTextEntry=YES;
                input_value_field5.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:NUMBERKEYBOARDTYPE_4])
                input_value_field5.keyboardType = UIKeyboardTypeNumberPad;
            else
                input_value_field5.keyboardType = UIKeyboardTypeDefault;
            
            input_value_field5.inputAccessoryView = numberToolbar;
            [popupTemplateScrollView addSubview:input_value_field5];
            next_Y_Position = input_value_field5.frame.origin.y+input_value_field5.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value5_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_input_value5_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label5.text forKey:@"labelName"];
            if ([input_value_field5 text])
                [tempDictionary setObject:[input_value_field5 text] forKey:@"value"];
            
        }
        else if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value5_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton5 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton5 setFrame:CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35)];
            
            NSString *dropdownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value5_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropdownStr) {
                [dropDownButton5 setTitle:dropdownStr forState:UIControlStateNormal];
                [dropDownButton5 setTitle:dropdownStr forState:UIControlStateHighlighted];
            }
            [dropDownButton5 setTag:inputValidationsCount];
            inputValidationsCount++;
    
            dropDownButton5.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
          
            if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value5_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value5_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton5 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value5_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value5_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value5_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value5_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton5.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton5.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                  dropDownButton5.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton5.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton5.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton5.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton5.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton5.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton5.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [dropDownButton5 setExclusiveTouch:YES];
            dropDownButton5.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton5.frame.size.width-dropDownButton5.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton5.frame.size.width-dropDownButton5.intrinsicContentSize.width-8.0));
                [dropDownButton5 setTitleEdgeInsets:titleInsets];
                dropDownButton5.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton5 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [popupTemplateScrollView addSubview:dropDownButton5];
            
            [[dropDownButton5 layer] setBorderWidth:0.5f];
            [[dropDownButton5 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton5.frame.size.width-20,next_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [popupTemplateScrollView addSubview:imageView];
            
            next_Y_Position = dropDownButton5.frame.origin.y+dropDownButton5.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template6_input_dropdown_value5_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label5.text forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton5.titleLabel.text forKey:@"value"];
        }
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_input_field5_border_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_border_label5 = [[UILabel alloc] init];
            input_value_border_label5.frame = CGRectMake(X_Position, next_Y_Position+3, popupTemplateScrollView.frame.size.width-10,1);
            input_value_border_label5.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:input_value_border_label5];
            next_Y_Position = next_Y_Position+input_value_border_label5.frame.size.height;
        }
        numberOfFields++;
        [validationsArray addObject:tempDictionary];
    }
    
    // ------------------- Editable Ends ------------------- //
    
    // ------------------- Buttons ------------------- //
    
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_button1_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setFrame:CGRectMake(popupTemplateScrollView.frame.origin.x,popupTemplateScrollView.frame.origin.y+popupTemplateScrollView.frame.size.height+5,(popupTemplateScrollView.frame.size.width/2)-5,40)];
        // properties For Button backgroundColor
        
        NSArray *button1_BackgroundColor;
        if (NSLocalizedStringFromTableInBundle(@"popup_template6_button1_color",propertyFileName,[NSBundle mainBundle], nil))
            button1_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_button1_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            button1_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button1_BackgroundColor)
            button1.backgroundColor=[UIColor colorWithRed:[[button1_BackgroundColor objectAtIndex:0] floatValue] green:[[button1_BackgroundColor objectAtIndex:1] floatValue] blue:[[button1_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        NSString *buttonTitle = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (buttonTitle)
            [button1 setTitle:buttonTitle forState:UIControlStateNormal];
        
        if ([NSLocalizedStringFromTableInBundle(@" popup_template6_button1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *button1_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template6_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                button1_TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
           if (button1_TextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[button1_TextColor objectAtIndex:0] floatValue] green:[[button1_TextColor objectAtIndex:1] floatValue] blue:[[button1_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            // Properties for Button Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for Button textcolor
            
            NSArray *button1_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button1_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button1_TextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[button1_TextColor objectAtIndex:0] floatValue] green:[[button1_TextColor objectAtIndex:1] floatValue] blue:[[button1_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [button1 setExclusiveTouch:YES];
        [button1 addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:button1];
    }
    // Button2
    if ([NSLocalizedStringFromTableInBundle(@"popup_template6_button2_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        button2 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button2 setFrame:CGRectMake((button1.frame.origin.x+button1.frame.size.width+10),popupTemplateScrollView.frame.origin.y+popupTemplateScrollView.frame.size.height+5,(popupTemplateScrollView.frame.size.width/2)-5,40)];
        
        // properties For Button backgroundColor
        NSArray *button2_BackgroundColor;
        if (NSLocalizedStringFromTableInBundle(@"popup_template6_button2_color",propertyFileName,[NSBundle mainBundle], nil))
            button2_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_button2_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            button2_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button2_BackgroundColor)
            button2.backgroundColor=[UIColor colorWithRed:[[button2_BackgroundColor objectAtIndex:0] floatValue] green:[[button2_BackgroundColor objectAtIndex:1] floatValue] blue:[[button2_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        NSString *buttonTitle = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template6_button2_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (buttonTitle)
            [button2 setTitle:buttonTitle forState:UIControlStateNormal];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template6_button2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *button2_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template6_button2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_button2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                button2_TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button2_TextColor)
                [button2 setTitleColor:[UIColor colorWithRed:[[button2_TextColor objectAtIndex:0] floatValue] green:[[button2_TextColor objectAtIndex:1] floatValue] blue:[[button2_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_button2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_button2_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            
            // Properties for Button Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_button2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_button2_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
               button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for Button textcolor
            
            NSArray *button2_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button2_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button2_TextColor)
                [button2 setTitleColor:[UIColor colorWithRed:[[button2_TextColor objectAtIndex:0] floatValue] green:[[button2_TextColor objectAtIndex:1] floatValue] blue:[[button2_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template6_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [button2 setExclusiveTouch:YES];
        
        [button2 addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:button2];
    }
    
    [popupTemplateScrollView setContentSize:CGSizeMake(SCREEN_WIDTH-96, numberOfFields*43)];
    
    activityIndicator = [[ActivityIndicator alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self addSubview:activityIndicator];
    activityIndicator.hidden = YES;
}

#pragma mark - DropDown Selection.
/*
 * This method is used to add PopupTemplate6 dropdown button action.
 */
-(void)selectDropDown:(id)sender
{
    btn = (UIButton *)sender;
    dropdownString = [NSString stringWithFormat:@"popup_template6_input_dropdown_value%ld",((long)btn.tag+1)-100];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData:) name:@"UpdateData" object:nil];
    DatabaseManager *dataManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
    dropDownDataArray = [[NSArray alloc] initWithArray:[dataManager getAllDropDownDetailsForKey:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_data_webservice_name"],propertyFileName,[NSBundle mainBundle], nil)]];
    nextTemplateProperty =  NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template_properties_file"],propertyFileName,[NSBundle mainBundle], nil);
    if ([NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template"],propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"1"]){
        nextTemplate=CHILD_TEMPLATE_3;
    }
    else if ([NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template"],propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"2"]){
        nextTemplate=POPUP_TEMPLATE_8;
        
    }
    
    if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
    {
        Class nextClass = NSClassFromString(nextTemplate);
        id object = nil;
        
        if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)])
        {
            object = [[nextClass alloc] initWithNibName:nextTemplate bundle:nil withPropertyName:nextTemplateProperty hasDidSelectFunction:NO withDataDictionary:nil withDataArray:dropDownDataArray withPIN:nil withProcessorCode:nil withType:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_data_webservice_name"],propertyFileName,[NSBundle mainBundle], nil) fromView:0 insideView:nil];
            [(ChildTemplate3 *)object setBaseSelector:@selector(updateData:)];
            [(ChildTemplate3 *)object setBasetarget:self];
            AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
            UINavigationController *navCntrl = (UINavigationController *)delegate.window.rootViewController;
            if (navCntrl && [navCntrl isKindOfClass:[UINavigationController class]])
            {
                [navCntrl pushViewController:object animated:NO];
            }
        }
        else if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray:withSubIndex:)])
        {
            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:0 withDataArray:dropDownDataArray?dropDownDataArray:nil withSubIndex:0];
            [object setBasetarget:self];
            [object setBaseSelector:@selector(updateData:)];
            [[[self superview] superview] addSubview:(UIView *)object];
        }
    }
}

#pragma mark - Get dropdownData.
/*
 * This method is used to add PopupTemplate6 dropdown updated data to particular field.
 */
- (void)updateData:(id)object
{
    datavalueDictionary=[[NSMutableDictionary alloc]init];
    
    if ([[object objectForKey:DROP_DOWN_TYPE] isEqualToString:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_data_webservice_name"],propertyFileName,[NSBundle mainBundle], nil)])
    {
        NSLog(@"[object objectForKey:DROP_DOWN_TYPE_DESC]   :   %@",[object objectForKey:DROP_DOWN_TYPE_DESC]);
        NSString *dropDownValue = [object objectForKey:DROP_DOWN_TYPE_DESC];
        
        if (dropDownValue.length == 0) {
            dropDownValue = [object objectForKey:DROP_DOWN_TYPE_NAME];
        }
        
        [btn setTitle:dropDownValue forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setTitle:dropDownValue forState:UIControlStateHighlighted];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        
        NSString *typeName = [NSString stringWithFormat:@"%@_typeName",[object objectForKey:DROP_DOWN_TYPE]];
        NSString *typeDesc = [NSString stringWithFormat:@"%@_typeDesc",[object objectForKey:DROP_DOWN_TYPE]];
        
        if ([object objectForKey:@"typeName"]) {
            [datavalueDictionary setObject:[object objectForKey:@"typeName"] forKey:typeName];
        }
        
        if ([object objectForKey:@"typeDesc"]) {
            [datavalueDictionary setObject:[object objectForKey:@"typeDesc"] forKey:typeDesc];
        }
        
        [datavalueDictionary setObject:[object objectForKey:DROP_DOWN_TYPE_NAME] forKey:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_param_type"],propertyFileName,[NSBundle mainBundle], nil)];
    }
    
}

#pragma mark - Button Actions.
/**
 * This method is used to set button1 action of popup template6.
 @prameters are defined in button action those are
 * Application display type(ticker,PopUp),validation type,next template,next template PropertyFile,Action type,
 Feature-(processorCode,transaction type).
 */
-(void)button1Action:(id)sender
{
    if ([activeField isFirstResponder])
        [activeField resignFirstResponder];
    
    if([propertyFileName isEqualToString:@"VerifyRegistrationPPT6_T2"]){
        if(currentImage == 1){
            //do nothing
        }else{
            currentImage--;
            if(currentImage < 1)
                currentImage = 1;
            if(currentImage > 4)
                currentImage = verifyImagesCount;
            if(currentImage==4 && refImage4)
                [refImageView setImage:refImage4];
            if(currentImage==3 && refImage3)
                [refImageView setImage:refImage3];
            if(currentImage==2 && refImage2)
                [refImageView setImage:refImage2];
            if(currentImage==1 && refImage1)
                [refImageView setImage:refImage1];


            if(isArabic && !isImageReversed && refImageView.image){
                isImageReversed = YES;
                NSLog(@"Image reversed in ppt6.. button1..");
                refImageView.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
            }
           
            return;
        }
    }
    
    NSString *actionType = NSLocalizedStringFromTableInBundle(@"popup_template6_button1_action_type",propertyFileName,[NSBundle mainBundle], nil);
    validation_Type = NSLocalizedStringFromTableInBundle(@"application_validation_type",@"GeneralSettings",[NSBundle mainBundle], nil);
    alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type",@"GeneralSettings",[NSBundle mainBundle], nil);
    nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"popup_template6_button1_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplate = NSLocalizedStringFromTableInBundle(@"popup_template6_button1_next_template",propertyFileName,[NSBundle mainBundle], nil);
    NSString *paramType = nil;
    if (NSLocalizedStringFromTableInBundle(@"popup_template6_button1_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil))
    {
        paramType = NSLocalizedStringFromTableInBundle(@"popup_template6_button1_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil);
    }
    
    NSString *data = NSLocalizedStringFromTableInBundle(@"popup_template6_button1_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    NSString *processorCode = [Template getProcessorCodeWithData:data];
    NSString *type = [Template getTransactionCodeWithData:data];
    if (processorCode) {
        [localDataDictionary setObject:processorCode forKey:PARAMETER15];
    }
    if (type) {
        [localDataDictionary setObject:type forKey:PARAMETER13];
    }
    Template *obj = [Template initWithactionType:actionType nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:propertyFileName ProcessorCode:processorCode Dictionary:(NSMutableDictionary *)localDataDictionary contentArray:validationsArray alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:paramType withLableValidation:labelValidationsArray withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:nil withCurrentClass:POPUP_TEMPLATE_6];
    obj.selector = @selector(processData);
    obj.target = self;
    [[NSNotificationCenter defaultCenter] postNotificationName:PPT6_BUTTON1_ACTION object:obj];
}

-(void)agentConfirmedVerification{
    [input_key_label1 setHidden:NO];
    [input_value_field1 setHidden:NO];
    [input_value_border_label1 setHidden:NO];

    [button1 setTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_decline", nil)] forState:UIControlStateNormal];
    [button1 removeTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
    [button1 addTarget:self action:@selector(agentRejectedRegistration) forControlEvents:UIControlEventTouchUpInside];

    [button2 setTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_approve", nil)] forState:UIControlStateNormal];
    [button2 removeTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
    [button2 addTarget:self action:@selector(agentApprovedRegistration) forControlEvents:UIControlEventTouchUpInside];
}

-(void)agentRejectedRegistration{
    bool isValid = [self validateMpin];
    if(isValid){
        NSLog(@"Rejected");
        isRejected = YES;
        [localDataDictionary setObject:@"2" forKey:PARAMETER18];
        [self button2Action:nil];
    }else{
        NSLog(@"%@", mpinValidationString);
    }
}

-(void)agentApprovedRegistration{
    bool isValid = [self validateMpin];
    if(isValid){
        NSLog(@"Approved");
        isApproved = YES;
        [localDataDictionary setObject:@"1" forKey:PARAMETER18];
        [self button2Action:nil];
    }else{
        NSLog(@"%@", mpinValidationString);
    }
}

-(BOOL)validateMpin{
    mpinValidationString = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_enter_mpin", nil)];
    if(input_value_field1.text.length != 4){
        return NO;
    }else{
        if (![[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
        {
            if([input_value_field1.text rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location == NSNotFound) {
                return YES;
            }else {
                return NO;
            }
        }
        return YES;
    }
    return YES;
}

/**
 * This method is used to set button2 action of popup template6.
 */
-(void)button2Action:(id)sender
{
    if([propertyFileName isEqualToString:@"VerifyRegistrationPPT6_T1"]){
        NSString *nextTemplateProperty = @"VerifyRegistrationPPT6_T2";
        NSString *nextTemplate = @"PopUpTemplate6";
        if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
        {
            Class myclass = NSClassFromString(nextTemplate);
            id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:localDataDictionary withProcessorCode:@"0209" withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
            
            //[[[local_id superview] superview] addSubview:(UIView *)obj];
            [[UIApplication sharedApplication].keyWindow addSubview:(UIView *)obj];
        }
        return;
    }
    if([propertyFileName isEqualToString:@"VerifyRegistrationPPT6_T2"]){
        if(!isApproved && !isRejected){
            [input_key_label1 setHidden:YES];
            [input_value_field1 setHidden:YES];
            [input_value_border_label1 setHidden:YES];
            if(currentImage < verifyImagesCount){
                currentImage++;
                if(currentImage > verifyImagesCount)
                    currentImage = verifyImagesCount;
                if(currentImage==4 && refImage4)
                    [refImageView setImage:refImage4];
                if(currentImage==3 && refImage3)
                    [refImageView setImage:refImage3];
                if(currentImage==2 && refImage2)
                    [refImageView setImage:refImage2];
            }
            
            if(isArabic && !isImageReversed && refImageView.image){
                isImageReversed = YES;
                NSLog(@"Image reversed in ppt6..button2");
                refImageView.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
            }
            if(currentImage == verifyImagesCount){
                [button2 setTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_confirmbutton", nil)] forState:UIControlStateNormal];
                [button2 removeTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
                [button2 addTarget:self action:@selector(agentConfirmedVerification) forControlEvents:UIControlEventTouchUpInside];
                return;
            }else{
                [button2 addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
                return;
            }
        }

    }
    if ([activeField isFirstResponder])
        [activeField resignFirstResponder];
    
    NSString *actionType = NSLocalizedStringFromTableInBundle(@"popup_template6_button2_action_type",propertyFileName,[NSBundle mainBundle], nil);
    validation_Type = NSLocalizedStringFromTableInBundle(@"application_validation_type",@"GeneralSettings",[NSBundle mainBundle], nil);
    alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type",@"GeneralSettings",[NSBundle mainBundle], nil);
    nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"popup_template6_button2_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplate = NSLocalizedStringFromTableInBundle(@"popup_template6_button2_next_template",propertyFileName,[NSBundle mainBundle], nil);
    NSString *paramType = nil;
    if (NSLocalizedStringFromTableInBundle(@"popup_template6_button2_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil))
    {
        paramType = NSLocalizedStringFromTableInBundle(@"popup_template6_button2_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil);
    }
    
    NSString *data = NSLocalizedStringFromTableInBundle(@"popup_template6_button2_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    NSString *processorCode = [Template getProcessorCodeWithData:data];
    NSString *type = [Template getTransactionCodeWithData:data];
    
    //prepare dict for verify reg approve or deny action..
    if([processorCode isEqualToString:@"0209"] && [type isEqualToString:@"234"]){
        NSString *mobNo = [localDataDictionary objectForKey:@"PaymentDetails1"];
        NSString *pd2 = [localDataDictionary objectForKey:@"PaymentDetails2"];
        localDataDictionary = [[NSMutableDictionary alloc] init];
        [localDataDictionary setObject:mobNo forKey:@"PaymentDetails1"];
        if(pd2)
            [localDataDictionary setObject:pd2 forKey:@"PaymentDetails2"];
        
//        [localDataDictionary setObject:[ValidationsClass encryptString:input_value_field1.text] forKey:PARAMETER8];
        [localDataDictionary setObject:input_value_field1.text forKey:PARAMETER7];

    }
    if (processorCode) {
        [localDataDictionary setObject:processorCode forKey:PARAMETER15];
    }
    if (type) {
        [localDataDictionary setObject:type forKey:PARAMETER13];
    }
    if([propertyFileName isEqualToString:@"PayBillMyBillersPPT6_2"]){
        [localDataDictionary setObject:[localDataDictionary objectForKey:BILLERID] forKey:PARAMETER18];
    }
    if([propertyFileName isEqualToString:@"PayBillMyBillersPayBillersCategoryPayBillerPPT6_1"]){
        [localDataDictionary setObject:[localDataDictionary objectForKey:BILLERMASTERID] forKey:PARAMETER18];
    }
    Template *obj = [Template initWithactionType:actionType nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:propertyFileName ProcessorCode:processorCode Dictionary:(NSMutableDictionary *)localDataDictionary contentArray:validationsArray alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:paramType withLableValidation:labelValidationsArray withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:nil withCurrentClass:POPUP_TEMPLATE_6];
    obj.selector = @selector(processData);
    obj.target = self;
    [[NSNotificationCenter defaultCenter] postNotificationName:PPT6_BUTTON2_ACTION object:obj];
}


#pragma mark - toolbar methods
/**
 * This method is used to cancel button action of tool bar.
 */
-(void)cancelNumberPad
{
    if ([activeField isFirstResponder])
    {
        [activeField resignFirstResponder];
        activeField.text = @"";
    }
}

/**
 * This method is used to ok button action of tool bar.
 */
-(void)doneWithNumberPad
{
    if ([activeField isFirstResponder])
        [activeField resignFirstResponder];
}

#pragma mark - textField delegate methods
/**
 * This method is used to keybord show and hide method(resign,become responder).
 */
- (void)textFieldDidBeginEditing:(CustomTextField *)textField
{
    activeField = textField;
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-35);
    [popupTemplateScrollView setContentOffset:scrollPoint animated:NO];
}

- (void)textFieldDidEndEditing:(CustomTextField *)textField
{
    [textField resignFirstResponder];
    [popupTemplateScrollView setContentOffset:CGPointZero animated:NO];
    
}

- (BOOL)textFieldShouldBeginEditing:(CustomTextField *)textField
{
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    
    if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
    {
        textField.textAlignment=NSTextAlignmentRight;
    }
    else
    {
        textField.textAlignment=NSTextAlignmentLeft;
    }
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(CustomTextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(CustomTextField *)textField
{
    return YES;
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance =(SCREEN_HEIGHT>480)? -130:-100;
    const float movementDuration = 0.3f;
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.frame = CGRectOffset(self.frame, 0, movement);
    [UIView commitAnimations];
}

#pragma mark - BaseView process data.
/**
 * This method is used to set baseviewcontroller Process data notification.
 */


-(void)processData
{
    //ProcessData
}

#pragma mark - Error Handling methods.
/**
 * This method is used to set Error handling methods.
 */
-(void)errorCodeHandlingInXML:(NSString *)errorCode andMessgae:(NSString *)message withProcessorCode:(NSString *)processorCode
{
    [activityIndicator stopActivityIndicator];
    activityIndicator.hidden = YES;
    [self showAlertForErrorWithMessage:message andErrorCode:errorCode];
}

-(void)errorCodeHandlingInWebServiceRunningWithErrorCode:(NSString *)errorCode
{
    activityIndicator.hidden = YES;
    [activityIndicator stopActivityIndicator];
    [self showAlertForErrorWithMessage:nil andErrorCode:errorCode];
}

-(void)showAlertForErrorWithMessage:(NSString *)message andErrorCode:(NSString *)errorCode
{
    NSString *errorMessage = nil;
    
    if (errorCode)
        errorMessage = NSLocalizedStringFromTableInBundle(errorCode,[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil);
    else
        errorMessage = message;
    
    if([alertview_Type compare:TICKER_TEXT] == NSOrderedSame)
    {
        NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
        
        NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
        
        [[[self superview] superview] makeToast:errorMessage
                                       duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue]
                                       position:CSToastPositionBottom
                                backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f]
                                      textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
    }
    else if ([alertview_Type compare:POPUP_TEXT] == NSOrderedSame)
    {
        PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]]  andMessage:errorMessage withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
        [[[self superview] superview] addSubview:popup];
    }
}

- (void)verifyRegApproved:(NSNotification *)notification{
    if([propertyFileName isEqualToString:@"VerifyRegistrationPPT6_T2"]){
        PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]]  andMessage:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_verify_reg_approved", nil)] withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
        [[UIApplication sharedApplication].keyWindow addSubview:popup];
    }
    [self removeFromSuperview];
}

- (void)verifyRegRejected:(NSNotification *)notification{
    if([propertyFileName isEqualToString:@"VerifyRegistrationPPT6_T2"]){
        PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]]  andMessage:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_verify_reg_rejected", nil)] withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
        [[UIApplication sharedApplication].keyWindow addSubview:popup];
    }
    [self removeFromSuperview];
}



@end
