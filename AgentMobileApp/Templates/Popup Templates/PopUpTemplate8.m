//
//  PopUpTemplate8.m
//  Consumer Client
//
//  Created by android on 11/30/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "PopUpTemplate8.h"
#import "Constants.h"
#import "ValidationsClass.h"
#import "Localization.h"
#import "DatabaseConstatants.h"

@implementation PopUpTemplate8
@synthesize headerLabel,cancelBtn,popupTempletTblview,valueLabel,headderlabelBorder;
@synthesize propertyFileName,processor_Code,popupTemplateLabel,valuesArr;

#pragma mark - PopupTemplate8 UIView.
/**
 * This method is used to set implemention of Popup template8.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile andDelegate:(id)delegate withDataDictionary:(NSDictionary *)dataDictionary withProcessorCode:(NSString *)processorCode  withTag:(int)tagValue withTitle:(NSString *)title  andMessage:(NSString *)message withSelectedIndexPath:(int)selectedIndexPath withDataArray:(NSArray *)dataArray withSubIndex:(int)subIndex
{
    
    NSLog(@"PropertyFileName:%@",propertyFile);
    self = [super initWithFrame:frame];
    if (self)
    {
        propertyFileName=propertyFile;
        [self setBackgroundColor:[UIColor colorWithRed:0.1f green:0.1f blue:0.1f alpha:0.4f]];
        if (dataArray.count>0) {
            valuesArr=[[NSArray alloc]initWithArray:dataArray];
        }
        [self addControlsToView];
    }
    return self;
}

#pragma mark  - PopUpTemplate8 UI constraints Creation.
/**
 * This method is used to set add UIconstraints Frame of Popup template8.
 */
-(void)addControlsToView{
    // PopUp Template View
    popupTemplateLabel=[[UILabel alloc]init];
    popupTemplateLabel.frame=CGRectMake(0,0,self.frame.size.width-60,(self.frame.size.height/3));
    popupTemplateLabel.center = self.center;
    popupTemplateLabel.backgroundColor=[UIColor whiteColor];
    [self addSubview:popupTemplateLabel];
    
    // Header Label1.
    if ([NSLocalizedStringFromTableInBundle(@"popup_template8_header_label_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        headerLabel=[[UILabel alloc]init];
        headerLabel.frame=CGRectMake(popupTemplateLabel.frame.origin.x+10, popupTemplateLabel.frame.origin.y+10, popupTemplateLabel.frame.size.width-20, 40);
        headerLabel.backgroundColor=[UIColor clearColor];
        
        NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template8_header_label",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (headerStr)
            headerLabel.text=headerStr;
        
        headerLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template8_header_label_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *headerLabelTextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template8_header_label_color",propertyFileName,[NSBundle mainBundle], nil))
                headerLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template8_header_label_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                headerLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                headerLabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (headerLabelTextColor)
                headerLabel.textColor = [UIColor colorWithRed:[[headerLabelTextColor objectAtIndex:0] floatValue] green:[[headerLabelTextColor objectAtIndex:1] floatValue] blue:[[headerLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template8_header_label_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"popup_template8_header_label_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template8_header_label_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize =NSLocalizedStringFromTableInBundle(@"popup_template8_header_label_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize =NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize =application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else{
            //Default Properties for label textcolor
            
            NSArray *headerLabelTextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                headerLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                headerLabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (headerLabelTextColor)
                headerLabel.textColor = [UIColor colorWithRed:[[headerLabelTextColor objectAtIndex:0] floatValue] green:[[headerLabelTextColor objectAtIndex:1] floatValue] blue:[[headerLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle=NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize =NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize =application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        headerLabel.textAlignment=NSTextAlignmentCenter;
        headerLabel.numberOfLines=0;
        labelY_position=popupTemplateLabel.frame.origin.y+(distanceY*2)+headerLabel.frame.size.height;
        [self addSubview:headerLabel];
    }
    
    // close button
    cancelBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame=CGRectMake(popupTemplateLabel.frame.size.width-10, popupTemplateLabel.frame.origin.y+7, 30, 30);
    cancelBtn.exclusiveTouch=YES;
    [cancelBtn setImage:[UIImage imageNamed:CLOSE_IMAGE_NAME] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(deleteButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelBtn];
    
        // Header label border
        if ([NSLocalizedStringFromTableInBundle(@"popup_template8_header_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            headderlabelBorder=[[UILabel alloc]init];
            headderlabelBorder.frame=CGRectMake(popupTemplateLabel.frame.origin.x+10,labelY_position, popupTemplateLabel.frame.size.width-20, 1);
            headderlabelBorder.backgroundColor=[UIColor blackColor];
            labelY_position=labelY_position+headderlabelBorder.frame.size.height;
            [self addSubview:headderlabelBorder];
        }
        
    // TableView
    popupTempletTblview=[[UITableView alloc]init];
    popupTempletTblview.frame=CGRectMake(popupTemplateLabel.frame.origin.x, labelY_position, popupTemplateLabel.frame.size.width, (popupTemplateLabel.frame.size.height/1.4));
    popupTempletTblview.dataSource=self;
    popupTempletTblview.delegate=self;
    labelY_position=labelY_position+popupTempletTblview.frame.size.height;
    [self addSubview:popupTempletTblview];
}

-(void)deleteButtonAction:(id)sender{
    
    [self removeFromSuperview];
    
}

#pragma mark  - UITABLEVIEW DELEGATE AND DATASOURCE METHODS.
/**
 * This method is used to set add Tableview (list and delegate methods) of Popup template8.
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [valuesArr count];
}
/**
 * This method is used to set add Tableview for PopuPtemplate8.
 *@param type- Declare Listview related UI(Labels,Images etc..base on Req).
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cashin";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    valueLabel=[[UILabel alloc]init];
    labelX_position=10;
    
    valueLabel.frame=CGRectMake(labelX_position, 14, SCREEN_WIDTH-(labelX_position*2)-96, 22);
    
       NSString *value = [[valuesArr objectAtIndex:indexPath.row] objectForKey:DROP_DOWN_TYPE_DESC];
    if (value.length == 0 ) {
        value = [[valuesArr objectAtIndex:indexPath.row] objectForKey:DROP_DOWN_TYPE_NAME];
        if (value.length == 0 ) {
            value =  [[valuesArr objectAtIndex:indexPath.row] objectForKey:@"value"];
            if (value.length == 0 ) {
                value = [[valuesArr objectAtIndex:indexPath.row] objectForKey:@"id"];
            }
        }
    }
            valueLabel.text = value;
            valueLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            
            if ([NSLocalizedStringFromTableInBundle(@"popup_template8_listitem_label_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *valueLabelTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template8_listitem_label_color",propertyFileName,[NSBundle mainBundle], nil))
                    valueLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template8_listitem_label_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    valueLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    valueLabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (valueLabelTextColor)
                    valueLabel.textColor = [UIColor colorWithRed:[[valueLabelTextColor objectAtIndex:0] floatValue] green:[[valueLabelTextColor objectAtIndex:1] floatValue] blue:[[valueLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template8_listitem_label_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle =NSLocalizedStringFromTableInBundle(@"popup_template8_listitem_label_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle =NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle =application_default_text_style;
                
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template8_listitem_label_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"popup_template8_listitem_label_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize =application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    valueLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    valueLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else{
                //Default Properties for label textcolor
                
                NSArray *valueLabelTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    valueLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    valueLabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (valueLabelTextColor)
                    valueLabel.textColor = [UIColor colorWithRed:[[valueLabelTextColor objectAtIndex:0] floatValue] green:[[valueLabelTextColor objectAtIndex:1] floatValue] blue:[[valueLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle =NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle =application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"popup_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize =application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    valueLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    valueLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell.contentView addSubview:valueLabel];
            
    return cell;
}
/**
 * This method is used to set add Tableview Delegate Method User selects List .
 *@param type- Declare Listview - (NextTemplate and Next template proprty file).
 *To show List Item Detailed Data etc..
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedValueInteger = (int)indexPath.row;
    selectedCount =(int) [[tableView indexPathsForSelectedRows] count];
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] initWithDictionary:[valuesArr objectAtIndex:selectedValueInteger]];
    [dataDict setObject:[NSString stringWithFormat:@"%d",selectedValueInteger] forKey:@"selectedPosition"];
    [self.basetarget performSelectorOnMainThread:self.baseSelector withObject:dataDict waitUntilDone:YES];
    [self removeFromSuperview];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - View controller Life Cycle Method.
/**
 * This method is used to set for removing notification data.
 */
-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:@"UpdateData"];
}
@end
