//
//  PopUpTemplate1.m
//  Consumer Client
//
//  Created by android on 6/17/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "PopUpTemplate1.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "Localization.h"
#import "ValidationsClass.h"
#import "UIView+Toast.h"
#import "PopUpTemplate2.h"
#import "WebSericeUtils.h"
#import "WebServiceConstants.h"
#import "WebServiceRequestFormation.h"
#import "WebServiceRunning.h"
#import "WebServiceDataObject.h"
#import "Template.h"
#import "NotificationConstants.h"
#import "DatabaseConstatants.h"
#import "ConvertDetailsToSrverRequestFormate.h"
#import "NotificationConstants.h"
#import "BaseViewController.h"
#import "GenerateOTP.h"
#import "DatabaseManager.h"
#import "ChildTemplate3.h"
#import "CustomTextField.h"
#import "AppDelegate.h"

@implementation PopUpTemplate1

@synthesize propertyFileName,processor_Code;
@synthesize popupTemplateScrollView;
@synthesize popupTemplateLabel;
@synthesize headingTitle_label,headingTitle_border_label;
@synthesize key_label1,value_label1,value_border_label1;
@synthesize key_label2,value_label2,value_border_label2;
@synthesize key_label3,value_label3,value_border_label3;
@synthesize key_label4,value_label4,value_border_label4;
@synthesize key_label5,value_label5,value_border_label5;
@synthesize key_label6,value_label6,value_border_label6;
@synthesize key_label7,value_label7,value_border_label7;
@synthesize key_label8,value_label8,value_border_label8;
@synthesize key_label9,value_label9,value_border_label9;
@synthesize key_label10,value_label10,value_border_label10;

@synthesize input_key_label1,input_value_field1,dropDownButton1,input_value_border_label1;
@synthesize input_key_label2,input_value_field2,dropDownButton2,input_value_border_label2;
@synthesize input_key_label3,input_value_field3,dropDownButton3,input_value_border_label3;
@synthesize input_key_label4,input_value_field4,dropDownButton4,input_value_border_label4;
@synthesize input_key_label5,input_value_field5,dropDownButton5,input_value_border_label5;

@synthesize button1,button2;
@synthesize numStr;

#pragma mark - PopupTemplate1 UIView.
/**
 * This method is used to set implemention of Popup template1.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile andDelegate:(id)delegate withDataDictionary:(NSDictionary *)dataDictionary withProcessorCode:(NSString *)processorCode  withTag:(int)tagValue withTitle:(NSString *)title  andMessage:(NSString *)message withSelectedIndexPath:(int)selectedIndexPath withDataArray:(NSArray *)dataArray withSubIndex:(int)subIndex
{
   
    NSLog(@"PropertyFileName:%@",propertyFile);
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self setBackgroundColor:[UIColor colorWithRed:0.1f green:0.1f blue:0.1f alpha:0.5f]];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popUpTemplateButton2:) name:POPUPTEMPLATE1 object:nil];
        self.tag = tagValue;
        propertyFileName = propertyFile;
        localDataDictionary = [[NSMutableDictionary alloc] initWithDictionary:dataDictionary];
        local_processorCode = processorCode;
        NSLog(@"POPUP1 Local Data Dictionary...%@",localDataDictionary);
        
        [self addControlesToView];
    }
    
    return self;
}

#pragma mark  - PopUpTemplate1 UI constraints Creation.
/**
 * This method is used to set add UIconstraints Frame of Popup template1.
 */
-(void)addControlesToView
{
        if (local_processorCode.length == 0) {
            if ([[Template getProcessorCodeWithData:NSLocalizedStringFromTableInBundle(@"popup_template1_button2_web_service_api_name", propertyFileName,[NSBundle mainBundle], nil)] length] != 0) {
                local_processorCode = [Template getProcessorCodeWithData:NSLocalizedStringFromTableInBundle(@"popup_template1_button2_web_service_api_name", propertyFileName,[NSBundle mainBundle], nil)];
            }
            else if ([[Template getProcessorCodeWithData:NSLocalizedStringFromTableInBundle(@"popup_template1_button1_web_service_api_name", propertyFileName,[NSBundle mainBundle], nil)] length] != 0) {
                local_processorCode = [Template getProcessorCodeWithData:NSLocalizedStringFromTableInBundle(@"popup_template1_button1_web_service_api_name", propertyFileName,[NSBundle mainBundle], nil)];
            }
        }
    /**
     * This method is used to set add Generate OTP.
     */
    if ([local_processorCode isEqualToString:PROCESSOR_CODE_GENERATE_OTP]) {
        
        NSString *lengthStr=[NSString stringWithFormat:@"%@", NSLocalizedStringFromTableInBundle(@"application_default_key_length_generate_otp", @"GeneralSettings",[NSBundle mainBundle], nil)];
        
        generatedKey = [GenerateOTP getValue:[localDataDictionary objectForKey:@"phone_number"] withNSString:[localDataDictionary objectForKey:@"Paymentdetails2"] withNSString:[localDataDictionary objectForKey:@"TxnAmount"] withNSString:[localDataDictionary objectForKey:@"PaymentDetails1"] withLong:(long)[GenerateOTP getTime] withInt:[lengthStr intValue]];
    }
    
    if ([localDataDictionary objectForKey:@"fee"] ) {
        [localDataDictionary setObject:[localDataDictionary objectForKey:@"fee"] forKey:PARAMETER24];
    }
    if ([localDataDictionary objectForKey:@"amount"] ) {
        [localDataDictionary setObject:[localDataDictionary objectForKey:@"amount"] forKey:PARAMETER21];
    }
    
    int label_localTag = 101;
    int field_localTag = 1;
    
    validationsArray = [[NSMutableArray alloc] init];
    labelValidationsArray = [[NSMutableArray alloc] init];
    
    /*
     * This method is used to add popUptemplate1 ToolBar (Cancel,Ok buttons).
     */
    numberToolbar = [[UIToolbar alloc] init];
    numberToolbar.frame=CGRectMake(0, 0, SCREEN_WIDTH, 40);
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:
                            [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancel", nil)] style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_ok_button", nil)]] style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    NSString *selectedIconBtncolors = application_branding_color_theme;
    NSArray *selectedIconBtncolorsArray = [ValidationsClass colorWithHexString:selectedIconBtncolors];
    numberToolbar.tintColor = [UIColor colorWithRed:[[selectedIconBtncolorsArray objectAtIndex:0] floatValue] green:[[selectedIconBtncolorsArray objectAtIndex:1] floatValue] blue:[[selectedIconBtncolorsArray objectAtIndex:2] floatValue] alpha:1.0f];
    
    popupTemplateLabel = [[UILabel alloc] init];
    popupTemplateLabel.frame = CGRectMake(20, 30, SCREEN_WIDTH-40 , SCREEN_HEIGHT-60);
    
    [BaseViewController getControlFrameForProcessorCode:local_processorCode andPropertyFile:nil forControl:&popupTemplateLabel toCenter:self withYCoordinate:0.0 andXCoordinate:0.0 andHeight:0.0 andChangeableyCoordinate:nil withAdditonalControl:nil andButton1:nil andButton2:nil withDictionary:nil forSequence:0];
    
    
    NSArray *backGroundColors = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_background_color",propertyFileName,[NSBundle mainBundle], nil)];
    
    if (backGroundColors)
        popupTemplateLabel.backgroundColor = [UIColor colorWithRed:[[backGroundColors objectAtIndex:0] floatValue] green:[[backGroundColors objectAtIndex:1] floatValue] blue:[[backGroundColors objectAtIndex:2] floatValue] alpha:1.0f];
    
    [self addSubview:popupTemplateLabel];
    
    // ------------------- Header Label ------------------- //
    /**
     * This method is used to set add UIconstraints Frame of Popup template1.
     */
    float heading_Y = 30;
    
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_title_label_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        headingTitle_label = [[UILabel alloc] init];
        headingTitle_label.frame = CGRectMake(40, 30, SCREEN_WIDTH-80 , 50);
        
        [BaseViewController getControlFrameForProcessorCode:local_processorCode andPropertyFile: NSLocalizedStringFromTableInBundle(@"popup_template1_title_label_visibility",propertyFileName,[NSBundle mainBundle], nil) forControl:&headingTitle_label toCenter:nil withYCoordinate:0.0 andXCoordinate:0.0 andHeight:0.0 andChangeableyCoordinate:nil withAdditonalControl:nil andButton1:nil andButton2:nil withDictionary:nil forSequence:1];
        
        
        headingTitle_label.backgroundColor = [UIColor clearColor];
        
        NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_title_label",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (headerStr)
            headingTitle_label.text = headerStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_title_label_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_title_label_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_title_label_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                headingTitle_label.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_title_label_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_title_label_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_title_label_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_title_label_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
               headingTitle_label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headingTitle_label.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headingTitle_label.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
               headingTitle_label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                headingTitle_label.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headingTitle_label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headingTitle_label.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headingTitle_label.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headingTitle_label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        headingTitle_label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:headingTitle_label];
        heading_Y =(headingTitle_label.frame.origin.y+headingTitle_label.frame.size.height);
    }
    
    // header Border color.
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_title_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        headingTitle_border_label = [[UILabel alloc] init];
        headingTitle_border_label.frame = CGRectMake(50, heading_Y, SCREEN_WIDTH-100 , 1);
        
        [BaseViewController getControlFrameForProcessorCode:local_processorCode andPropertyFile: NSLocalizedStringFromTableInBundle(@"popup_template1_title_border_visibility",propertyFileName,[NSBundle mainBundle], nil) forControl:&headingTitle_border_label toCenter:nil withYCoordinate:heading_Y andXCoordinate:0.0 andHeight:0.0 andChangeableyCoordinate:nil withAdditonalControl:nil andButton1:nil andButton2:nil withDictionary:nil forSequence:2];
        
        headingTitle_border_label.backgroundColor = [UIColor blackColor];
        [self addSubview:headingTitle_border_label];
    }
    // ------------------- Header Label ------------------- //
    
    popupTemplateScrollView = [[UIScrollView alloc] init];
    popupTemplateScrollView.backgroundColor = [UIColor clearColor];
    popupTemplateScrollView.frame = CGRectMake(40, heading_Y+2,popupTemplateLabel.frame.size.width-40 , popupTemplateLabel.frame.size.height-120);
    
    [BaseViewController getControlFrameForProcessorCode:local_processorCode andPropertyFile:NSLocalizedStringFromTableInBundle(@"popup_template1_title_border_visibility",propertyFileName,[NSBundle mainBundle], nil) forControl:nil toCenter:nil withYCoordinate:heading_Y andXCoordinate:0.0 andHeight:CGRectGetHeight(popupTemplateLabel.frame) andChangeableyCoordinate:nil withAdditonalControl:&popupTemplateScrollView andButton1:nil andButton2:nil withDictionary:nil forSequence:2];
    
    [self addSubview:popupTemplateScrollView];
    
    float X_Position = 5.0;
    float next_Y_Position = 0;
    float borderLabel_Y = 0.0;
    int numberOfFields = 0;
    
    // ------------------- Non Editable Starts ------------------- //
    
    // ------------------- 1st Value(Label,Value label and Border label) ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_value1_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        
        
        key_label1 = [[UILabel alloc] init];
        key_label1.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
        key_label1.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_label1",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label1.text = labelStr;
        
        key_label1.textAlignment = NSTextAlignmentLeft;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_label1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label1_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label1_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                key_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label1_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label1_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label1_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label1_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                key_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        key_label1.numberOfLines=2;
        [key_label1 sizeToFit];
        [popupTemplateScrollView addSubview:key_label1];
        next_Y_Position = key_label1.frame.origin.y+key_label1.frame.size.height-4;
        
        value_label1 = [[UILabel alloc] init];
        value_label1.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
        value_label1.tag = label_localTag;
        label_localTag++;
        value_label1.backgroundColor = [UIColor clearColor];
        
        if (![BaseViewController getControlFrameForProcessorCode:local_processorCode andPropertyFile:nil forControl:&value_label1 toCenter:nil withYCoordinate:0.0 andXCoordinate:0.0 andHeight:0.0 andChangeableyCoordinate:nil withAdditonalControl:nil andButton1:nil andButton2:nil withDictionary:localDataDictionary forSequence:3])
        {
            NSString *str=NSLocalizedStringFromTableInBundle(@"popup_template1_value1_param_type",propertyFileName,[NSBundle mainBundle], nil);
            NSString *localStr=[localDataDictionary objectForKey:str];
            
            if (!(localStr == (id)[NSNull null]) && !(localStr ==nil))
                value_label1.text = [NSString stringWithFormat:@"%@",localStr];
            
            else if ((localStr == (id)[NSNull null] || localStr.length == 0 || [localStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0 || [localStr isEqualToString:@""] || [localDataDictionary objectForKey:@""])||([localStr compare:@"(null)"]==NSOrderedSame))
                value_label1.text =  application_default_no_value_available;
            
            if ([local_processorCode isEqualToString:PROCESSOR_CODE_GENERATE_OTP]) {
                if (!(generatedKey == (id)[NSNull null] || generatedKey.length == 0 || [generatedKey stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0 || [generatedKey isEqualToString:@""]))
                    value_label1.text=generatedKey;
            }
        }
        value_label1.textAlignment = NSTextAlignmentLeft;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_value1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value1_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value1_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                value_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value1_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value1_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value1_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value1_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                value_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        if (value_label1.text)
            [tempDictionary setObject:value_label1.text forKey:@"value"];
        [popupTemplateScrollView addSubview:value_label1];
        next_Y_Position = next_Y_Position+value_label1.frame.size.height;
        borderLabel_Y=(value_label1.frame.origin.y+value_label1.frame.size.height)-4;
        
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field1_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label1 = [[UILabel alloc] init];
            value_border_label1.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label1.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label1];
            next_Y_Position = next_Y_Position+value_border_label1.frame.size.height;
        }
        [labelValidationsArray addObject:tempDictionary];
        numberOfFields++;
    }
    // ------------------- 2nd Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_value2_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        
        
        key_label2 = [[UILabel alloc] init];
        key_label2.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
        key_label2.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_label2",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label2.text = labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_label2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label2_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label2_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                key_label2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label2_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label2_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label2_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label2_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                key_label2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        key_label2.textAlignment = NSTextAlignmentLeft;
        [popupTemplateScrollView addSubview:key_label2];
        next_Y_Position = next_Y_Position+key_label2.frame.size.height-4;
        
        
        value_label2 = [[UILabel alloc] init];
        value_label2.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
        value_label2.backgroundColor = [UIColor clearColor];
        value_label2.tag = label_localTag;
        label_localTag++;
        
        NSString *str=NSLocalizedStringFromTableInBundle(@"popup_template1_value2_param_type",propertyFileName,[NSBundle mainBundle], nil);
        NSString *localStr=[localDataDictionary objectForKey:str];
        
        if (!(localStr == (id)[NSNull null]) && !(localStr ==nil))
            value_label2.text = [NSString stringWithFormat:@"%@",localStr];
        
        else if ((localStr == (id)[NSNull null] || localStr.length == 0 || [localStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0 || [localStr isEqualToString:@""] || [localDataDictionary objectForKey:@""])||([localStr compare:@"(null)"]==NSOrderedSame))
            value_label2.text =  application_default_no_value_available;
        
        value_label2.textAlignment = NSTextAlignmentLeft;
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_value2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value2_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value2_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                value_label2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value2_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value2_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value2_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value2_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                value_label2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [popupTemplateScrollView addSubview:value_label2];
        next_Y_Position = next_Y_Position+value_label2.frame.size.height;
        borderLabel_Y = (value_label2.frame.origin.y+value_label2.frame.size.height)-4;
        if (value_label2.text)
            [tempDictionary setObject:value_label2.text forKey:@"value"];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field2_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label2 = [[UILabel alloc] init];
            value_border_label2.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label2.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label2];
            next_Y_Position = next_Y_Position+value_border_label2.frame.size.height;
        }
        [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
    // ------------------- 3rd Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field3_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_value3_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        
        key_label3 = [[UILabel alloc] init];
        key_label3.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
        key_label3.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_label3",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label3.text = labelStr;
        
        key_label3.textAlignment = NSTextAlignmentLeft;
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label3_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label3_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                key_label3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label3_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label3_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label3_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label3_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                key_label3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [popupTemplateScrollView addSubview:key_label3];
        next_Y_Position = next_Y_Position+key_label3.frame.size.height-4;
        
        value_label3 = [[UILabel alloc] init];
        value_label3.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
        value_label3.backgroundColor = [UIColor clearColor];
        value_label3.tag = label_localTag;
        label_localTag++;
        
        NSString *str=NSLocalizedStringFromTableInBundle(@"popup_template1_value3_param_type",propertyFileName,[NSBundle mainBundle], nil);
        NSString *localStr=[localDataDictionary objectForKey:str];
        
        if (!(localStr == (id)[NSNull null]) && !(localStr ==nil))
            value_label3.text = [NSString stringWithFormat:@"%@",localStr];
        
        else if ((localStr == (id)[NSNull null] || localStr.length == 0 || [localStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0 || [localStr isEqualToString:@""] || [localDataDictionary objectForKey:@""])||([localStr compare:@"(null)"]==NSOrderedSame))
            value_label3.text =  application_default_no_value_available;
        

        value_label3.textAlignment = NSTextAlignmentLeft;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_value3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value3_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value3_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                value_label3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value3_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value3_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value3_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value3_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                value_label3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [popupTemplateScrollView addSubview:value_label3];
        next_Y_Position = next_Y_Position+value_label3.frame.size.height;
        borderLabel_Y = (value_label3.frame.origin.y+value_label3.frame.size.height)-4;
        if (value_label3.text)
            [tempDictionary setObject:value_label3.text forKey:@"value"];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field3_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label3 = [[UILabel alloc] init];
            value_border_label3.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label3.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label3];
            next_Y_Position = next_Y_Position+value_border_label3.frame.size.height;
        }
        [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
    // ------------------- 4th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field4_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_value4_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        
        key_label4 = [[UILabel alloc] init];
        key_label4.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
        key_label4.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_label4",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label4.text = labelStr;
        
        key_label4.textAlignment = NSTextAlignmentLeft;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_label4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label4_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label4_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                key_label4.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label4_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label4_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label4_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label4_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                key_label4.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [popupTemplateScrollView addSubview:key_label4];
        next_Y_Position = next_Y_Position+key_label4.frame.size.height-4;
        
        
        value_label4 = [[UILabel alloc] init];
        value_label4.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
        value_label4.backgroundColor = [UIColor clearColor];
        value_label4.tag = label_localTag;
        label_localTag++;
        
        NSString *str=NSLocalizedStringFromTableInBundle(@"popup_template1_value4_param_type",propertyFileName,[NSBundle mainBundle], nil);
        NSString *localStr=[localDataDictionary objectForKey:str];
        
        if (!(localStr == (id)[NSNull null]) && !(localStr ==nil))
            value_label4.text = [NSString stringWithFormat:@"%@",localStr];
        
        else if ((localStr == (id)[NSNull null] || localStr.length == 0 || [localStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0 || [localStr isEqualToString:@""] || [localDataDictionary objectForKey:@""])||([localStr compare:@"(null)"]==NSOrderedSame))
            value_label4.text =  application_default_no_value_available;
        
        value_label4.textAlignment = NSTextAlignmentLeft;
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_value4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value4_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value4_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                value_label4.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value4_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value4_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value4_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value4_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
               value_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                value_label4.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        next_Y_Position = next_Y_Position+value_label4.frame.size.height;
        borderLabel_Y = (value_label4.frame.origin.y+value_label4.frame.size.height)-4;
        [popupTemplateScrollView addSubview:value_label4];
        if (value_label4.text)
            [tempDictionary setObject:value_label4.text forKey:@"value"];
        
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field4_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label4 = [[UILabel alloc] init];
            value_border_label4.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label4.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label4];
            next_Y_Position = next_Y_Position+value_border_label4.frame.size.height;
        }
        [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
    // ------------------- 5th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field5_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_value5_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        
        
        key_label5 = [[UILabel alloc] init];
        key_label5.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
        key_label5.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_label5",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label5.text = labelStr;
        
        key_label5.textAlignment = NSTextAlignmentLeft;
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_label5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label5_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label5_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                key_label5.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label5_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label5_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label5_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label5_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                key_label5.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [popupTemplateScrollView addSubview:key_label5];
        next_Y_Position = next_Y_Position+key_label5.frame.size.height-4;
        
        value_label5 = [[UILabel alloc] init];
        value_label5.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
        value_label5.backgroundColor = [UIColor clearColor];
        value_label5.tag = label_localTag;
        label_localTag++;
        
        NSString *str=NSLocalizedStringFromTableInBundle(@"popup_template1_value5_param_type",propertyFileName,[NSBundle mainBundle], nil);
        NSString *localStr=[localDataDictionary objectForKey:str];
        
        if (!(localStr == (id)[NSNull null]) && !(localStr ==nil))
            value_label5.text = [NSString stringWithFormat:@"%@",localStr];
        
        else if ((localStr == (id)[NSNull null] || localStr.length == 0 || [localStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0 || [localStr isEqualToString:@""] || [localDataDictionary objectForKey:@""])||([localStr compare:@"(null)"]==NSOrderedSame))
            value_label5.text =  application_default_no_value_available;
        
        
        value_label5.textAlignment = NSTextAlignmentLeft;
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_value5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value5_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value5_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                value_label5.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value5_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value5_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value5_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value5_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                value_label5.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [popupTemplateScrollView addSubview:value_label5];
        if (value_label5.text)
            [tempDictionary setObject:value_label5.text forKey:@"value"];
        next_Y_Position = next_Y_Position+value_label5.frame.size.height;
        borderLabel_Y = (value_label5.frame.origin.y+value_label5.frame.size.height)-4;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field5_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label5 = [[UILabel alloc] init];
            value_border_label5.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label5.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label5];
            next_Y_Position = next_Y_Position+value_border_label5.frame.size.height;
        }
        [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
    // ------------------- 6th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field6_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_value6_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        
        key_label6 = [[UILabel alloc] init];
        key_label6.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
        key_label6.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_label6",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label6.text = labelStr;
        
        key_label6.textAlignment = NSTextAlignmentLeft;
        if([NSLocalizedStringFromTableInBundle(@"popup_template1_label6_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label6_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label6_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                key_label6.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label6_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label6_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label6_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label6_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label6.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label6.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                key_label6.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label6.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label6.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [popupTemplateScrollView addSubview:key_label6];
        next_Y_Position = next_Y_Position+key_label6.frame.size.height-4;
        
        
        value_label6 = [[UILabel alloc] init];
        value_label6.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
        value_label6.backgroundColor = [UIColor clearColor];
        value_label6.tag = label_localTag;
        label_localTag++;
        
        NSString *str=NSLocalizedStringFromTableInBundle(@"popup_template1_value6_param_type",propertyFileName,[NSBundle mainBundle], nil);
        NSString *localStr=[localDataDictionary objectForKey:str];
        
        if (!(localStr == (id)[NSNull null]) && !(localStr ==nil))
            value_label6.text = [NSString stringWithFormat:@"%@",localStr];
        
        else if ((localStr == (id)[NSNull null] || localStr.length == 0 || [localStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0 || [localStr isEqualToString:@""] || [localDataDictionary objectForKey:@""])||([localStr compare:@"(null)"]==NSOrderedSame))
            value_label6.text =  application_default_no_value_available;
        
        value_label6.textAlignment = NSTextAlignmentLeft;
        if([NSLocalizedStringFromTableInBundle(@"popup_template1_value6_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value6_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value6_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                value_label6.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value6_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value6_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value6_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value6_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label6.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label6.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                value_label6.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label6.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label6.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [popupTemplateScrollView addSubview:value_label6];
        if (value_label6.text)
            [tempDictionary setObject:value_label6.text forKey:@"value"];
        next_Y_Position = next_Y_Position+value_label6.frame.size.height;
        borderLabel_Y = (value_label6.frame.origin.y+value_label6.frame.size.height)-4;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field6_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label6 = [[UILabel alloc] init];
            value_border_label6.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label6.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label6];
            next_Y_Position = next_Y_Position+value_border_label6.frame.size.height;
        }
        [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
    // ------------------- 7th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field7_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_value7_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        
        
        key_label7 = [[UILabel alloc] init];
        key_label7.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
        key_label7.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_label7",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label7.text = labelStr;
        
        if([NSLocalizedStringFromTableInBundle(@"popup_template1_label7_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label7_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label7_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                key_label7.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label7_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label7_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label7_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label7_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label7.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label7.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                key_label7.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label7.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label7.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        key_label7.textAlignment = NSTextAlignmentLeft;
        
        [popupTemplateScrollView addSubview:key_label7];
        next_Y_Position = next_Y_Position+key_label7.frame.size.height-4;
        
        
        value_label7 = [[UILabel alloc] init];
        value_label7.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
        value_label7.backgroundColor = [UIColor clearColor];
        value_label7.tag = label_localTag;
        label_localTag++;
        
        NSString *str=NSLocalizedStringFromTableInBundle(@"popup_template1_value7_param_type",propertyFileName,[NSBundle mainBundle], nil);
        NSString *localStr=[localDataDictionary objectForKey:str];
        
        if (!(localStr == (id)[NSNull null]) && !(localStr ==nil))
            value_label7.text =[NSString stringWithFormat:@"%@",localStr];
        
        else if ((localStr == (id)[NSNull null] || localStr.length == 0 || [localStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0 || [localStr isEqualToString:@""] || [localDataDictionary objectForKey:@""])||([localStr compare:@"(null)"]==NSOrderedSame))
            value_label7.text =  application_default_no_value_available;
        
        
        value_label7.textAlignment = NSTextAlignmentLeft;
        if([NSLocalizedStringFromTableInBundle(@"popup_template1_value7_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *label_TextColor;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value7_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value7_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                value_label7.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value7_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value7_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value7_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value7_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label7.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label7.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                value_label7.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label7.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label7.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [popupTemplateScrollView addSubview:value_label7];
        if (value_label7.text)
            [tempDictionary setObject:value_label7.text forKey:@"value"];
        next_Y_Position = next_Y_Position+value_label7.frame.size.height;
        borderLabel_Y = (value_label7.frame.origin.y+value_label7.frame.size.height)-4;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field7_border_visibility",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"true"])
        {
            value_border_label7 = [[UILabel alloc] init];
            value_border_label7.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label7.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label7];
            next_Y_Position = next_Y_Position+value_border_label7.frame.size.height;
        }
        [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
    // ------------------- 8th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field8_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_value8_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        
        key_label8 = [[UILabel alloc] init];
        key_label8.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
        key_label8.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_label8",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label8.text = labelStr;
        
        key_label8.textAlignment = NSTextAlignmentLeft;
        
        if([NSLocalizedStringFromTableInBundle(@"popup_template1_label8_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label8_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label8_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                key_label8.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label8_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label8_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label8_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label8_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label8.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label8.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                key_label8.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label8.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label8.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [popupTemplateScrollView addSubview:key_label8];
        next_Y_Position = next_Y_Position+key_label8.frame.size.height-4;
        
        value_label8 = [[UILabel alloc] init];
        value_label8.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
        value_label8.backgroundColor = [UIColor clearColor];
        value_label8.tag = label_localTag;
        label_localTag++;
        
        NSString *str= NSLocalizedStringFromTableInBundle(@"popup_template1_value8_param_type",propertyFileName,[NSBundle mainBundle], nil);
        NSString *localStr=[localDataDictionary objectForKey:str];
        
        if (!(localStr == (id)[NSNull null]) && !(localStr ==nil))
            value_label8.text =[NSString stringWithFormat:@"%@",localStr];
        
        else if ((localStr == (id)[NSNull null] || localStr.length == 0 || [localStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0 || [localStr isEqualToString:@""] || [localDataDictionary objectForKey:@""])||([localStr compare:@"(null)"]==NSOrderedSame))
               value_label8.text =  application_default_no_value_available;
        
        value_label8.textAlignment = NSTextAlignmentLeft;
        if([NSLocalizedStringFromTableInBundle(@"popup_template1_value8_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *label_TextColor;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value8_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value8_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                value_label8.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value8_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value8_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value8_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value8_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label8.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label8.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                value_label8.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label8.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label8.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [popupTemplateScrollView addSubview:value_label8];
        if (value_label8.text)
            [tempDictionary setObject:value_label8.text forKey:@"value"];
        next_Y_Position = next_Y_Position+value_label8.frame.size.height;
        borderLabel_Y = (value_label8.frame.origin.y+value_label8.frame.size.height)-4;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field8_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label8 = [[UILabel alloc] init];
            value_border_label8.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label8.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label8];
            next_Y_Position = next_Y_Position+value_border_label8.frame.size.height;
        }
        [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
    // ------------------- 9th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field9_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_value9_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        
        key_label9 = [[UILabel alloc] init];
        key_label9.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
        key_label9.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_label9",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label9.text = labelStr;
        
        key_label9.textAlignment = NSTextAlignmentLeft;
        
        if([NSLocalizedStringFromTableInBundle(@"popup_template1_label9_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label9_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label9_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                key_label9.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label9_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label9_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label9_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label9_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label9.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label9.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                key_label9.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label9.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label9.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [popupTemplateScrollView addSubview:key_label9];
        next_Y_Position = next_Y_Position+key_label9.frame.size.height-4;
        
        value_label9 = [[UILabel alloc] init];
        value_label9.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
        value_label9.backgroundColor = [UIColor clearColor];
        value_label9.tag = label_localTag;
        label_localTag++;
        
        NSString *str=NSLocalizedStringFromTableInBundle(@"popup_template1_value9_param_type",propertyFileName,[NSBundle mainBundle], nil);
        NSString *localStr=[localDataDictionary objectForKey:str];
        
        if (!(localStr == (id)[NSNull null]) && !(localStr ==nil))
            value_label9.text =[NSString stringWithFormat:@"%@",localStr];
        
        else if ((localStr == (id)[NSNull null] || localStr.length == 0 || [localStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0 || [localStr isEqualToString:@""] || [localDataDictionary objectForKey:@""])||([localStr compare:@"(null)"]==NSOrderedSame))
            value_label9.text =  application_default_no_value_available;
        
        value_label9.textAlignment = NSTextAlignmentLeft;
        if([NSLocalizedStringFromTableInBundle(@"popup_template1_value9_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *label_TextColor;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value9_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value9_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                value_label9.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value9_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value9_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value9_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value9_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label9.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label9.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                value_label9.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label9.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label9.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [popupTemplateScrollView addSubview:value_label9];
        next_Y_Position = next_Y_Position+value_label9.frame.size.height;
        borderLabel_Y = (value_label9.frame.origin.y+value_label9.frame.size.height)-4;
        if (value_label9.text)
            [tempDictionary setObject:value_label9.text forKey:@"value"];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field9_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label9 = [[UILabel alloc] init];
            value_border_label9.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label9.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label9];
            next_Y_Position = next_Y_Position+value_border_label9.frame.size.height;
        }
        [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
    // ------------------- 10th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field10_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_value10_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        
        key_label10 = [[UILabel alloc] init];
        key_label10.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
        key_label10.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_label10",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label10.text = labelStr;
        
        key_label10.textAlignment = NSTextAlignmentLeft;
        
        if([NSLocalizedStringFromTableInBundle(@"popup_template1_label10_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label10_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label10_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                key_label10.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label10_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label10_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label10_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label10_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label10.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label10.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                key_label10.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label10.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label10.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [popupTemplateScrollView addSubview:key_label10];
        next_Y_Position = next_Y_Position+key_label10.frame.size.height-4;
        
        
        value_label10 = [[UILabel alloc] init];
        value_label10.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
        value_label10.backgroundColor = [UIColor clearColor];
        value_label10.tag = label_localTag;
        label_localTag++;
        
        NSString *str=NSLocalizedStringFromTableInBundle(@"popup_template1_value10_param_type",propertyFileName,[NSBundle mainBundle], nil);
        NSString *localStr=[localDataDictionary objectForKey:str];
        
        if (!(localStr == (id)[NSNull null]) && !(localStr ==nil))
            value_label10.text =[NSString stringWithFormat:@"%@",localStr];
        
        else if ((localStr == (id)[NSNull null] || localStr.length == 0 || [localStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0 || [localStr isEqualToString:@""] || [localDataDictionary objectForKey:@""])||([localStr compare:@"(null)"]==NSOrderedSame))
            value_label10.text =  application_default_no_value_available;        
        
        value_label10.textAlignment = NSTextAlignmentLeft;
        if([NSLocalizedStringFromTableInBundle(@"popup_template1_value10_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *label_TextColor;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value10_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value10_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                value_label10.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value10_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value10_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value10_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value10_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label10.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label10.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                value_label10.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label10.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label10.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [popupTemplateScrollView addSubview:value_label10];
        next_Y_Position = next_Y_Position+value_label10.frame.size.height;
        borderLabel_Y = (value_label10.frame.origin.y+value_label10.frame.size.height)-4;
        if (value_label10.text)
            [tempDictionary setObject:value_label10.text forKey:@"value"];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_labels_field10_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label10 = [[UILabel alloc] init];
            value_border_label10.frame = CGRectMake(X_Position, borderLabel_Y+1, popupTemplateScrollView.frame.size.width-10,1);
            value_border_label10.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:value_border_label10];
            next_Y_Position = next_Y_Position+value_border_label10.frame.size.height;
        }
        [labelValidationsArray addObject:tempDictionary];
        numberOfFields++;
    }
    
    // ------------------- Non Editable Ends ------------------- //
    
    // ------------------- Editable Starts ------------------- //
    
    // ------------------- 1st Value(Key label,Text field and DropDown) ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_field1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        input_key_label1 = [[UILabel alloc] init];
        input_key_label1.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
        
        [BaseViewController getControlFrameForProcessorCode:local_processorCode andPropertyFile:NSLocalizedStringFromTableInBundle(@"popup_template1_input_field1_visibility",propertyFileName,[NSBundle mainBundle], nil) forControl:&input_key_label1 toCenter:nil withYCoordinate:next_Y_Position andXCoordinate:X_Position andHeight:0.0 andChangeableyCoordinate:nil withAdditonalControl:&popupTemplateScrollView andButton1:nil andButton2:nil withDictionary:nil forSequence:4];
        
        input_key_label1.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_label1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            input_key_label1.text = labelStr;
        
        input_key_label1.textAlignment = NSTextAlignmentLeft;
        if([NSLocalizedStringFromTableInBundle(@"popup_template1_input_label1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_label1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_label1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                input_key_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_label1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_label1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_label1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_label1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                input_key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                input_key_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                input_key_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                input_key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                input_key_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                input_key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                input_key_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                input_key_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                input_key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [popupTemplateScrollView addSubview:input_key_label1];
        
        [BaseViewController getControlFrameForProcessorCode:local_processorCode andPropertyFile:NSLocalizedStringFromTableInBundle(@"popup_template1_input_field1_visibility",propertyFileName,[NSBundle mainBundle], nil) forControl:&input_key_label1 toCenter:nil withYCoordinate:next_Y_Position andXCoordinate:0.0 andHeight:0.0 andChangeableyCoordinate:&next_Y_Position  withAdditonalControl:nil andButton1:nil andButton2:nil withDictionary:nil forSequence:4];
        
        next_Y_Position = input_key_label1.frame.origin.y+input_key_label1.frame.size.height-4;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_field1 = [[CustomTextField alloc] init];
            input_value_field1.delegate = self;
            input_value_field1.tag = field_localTag;
            field_localTag++;
            input_value_field1.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
            input_value_field1.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                input_value_field1.placeholder =valueStr;
            
            NSLog(@"Value str.......%@",valueStr);
            
           // input_value_field1.textAlignment = NSTextAlignmentLeft;
            
            if (![[localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""])
                input_value_field1.text = [localDataDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_param_type",propertyFileName,[NSBundle mainBundle], nil)];
            
            if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([input_value_field1 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        input_value_field1.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField_TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField_TextColor)
                    input_value_field1.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField_TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField_TextColor)
                    input_value_field1.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            // property for KeyBoardType
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                input_value_field1.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                input_value_field1.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                input_value_field1.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                input_value_field1.secureTextEntry=YES;
                input_value_field1.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:NUMBERKEYBOARDTYPE_4])
                input_value_field1.keyboardType = UIKeyboardTypeNumberPad;
            else
                input_value_field1.keyboardType = UIKeyboardTypeDefault;
                        
            input_value_field1.inputAccessoryView = numberToolbar;
            
            [popupTemplateScrollView addSubview:input_value_field1];
            next_Y_Position = input_value_field1.frame.origin.y+input_value_field1.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value1_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label1.text forKey:@"labelName"];
            [tempDictionary setObject:[input_value_field1 text] forKey:@"value"];
        }
        else if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value1_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton1 = [UIButton buttonWithType:UIButtonTypeSystem];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton1 setTitleEdgeInsets:titleInsets];
            
            [dropDownButton1 setFrame:CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35)];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value1_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            NSLog(@"DropDown Str...%@",dropDownStr);
            
            if (dropDownStr) {
                [dropDownButton1 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton1 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            dropDownButton1.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton1 setTag:field_localTag];
            field_localTag++;
            
            if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton1 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value1_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                   dropDownButton1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton1.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [dropDownButton1 setExclusiveTouch:YES];
            dropDownButton1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton1.frame.size.width-dropDownButton1.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton1.frame.size.width-dropDownButton1.intrinsicContentSize.width-8.0));
                [dropDownButton1 setTitleEdgeInsets:titleInsets];
                dropDownButton1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton1 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [popupTemplateScrollView addSubview:dropDownButton1];
            
            [[dropDownButton1 layer] setBorderWidth:0.5f];
            [[dropDownButton1 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton1.frame.size.width-20,next_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [popupTemplateScrollView addSubview:imageView];
            
            next_Y_Position = dropDownButton1.frame.origin.y+dropDownButton1.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value1_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label1.text forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton1.titleLabel.text forKey:@"value"];
        }
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_field1_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_border_label1 = [[UILabel alloc] init];
            input_value_border_label1.frame = CGRectMake(X_Position, next_Y_Position+3, popupTemplateScrollView.frame.size.width-10,1);
            input_value_border_label1.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:input_value_border_label1];
            next_Y_Position = next_Y_Position+input_value_border_label1.frame.size.height;
        }
        numberOfFields++;
        [validationsArray addObject:tempDictionary];
    }
    // ------------------- 2nd Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_field2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        {
            input_key_label2 = [[UILabel alloc] init];
            input_key_label2.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            input_key_label2.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_label2_text",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                input_key_label2.text = labelStr;
            
            input_key_label2.textAlignment = NSTextAlignmentLeft;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template1_input_label2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_label2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_label2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    input_key_label2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_label2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_label2_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_label2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_label2_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_key_label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_key_label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    input_key_label2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_key_label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_key_label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:input_key_label2];
            next_Y_Position = input_key_label2.frame.origin.y+input_key_label2.frame.size.height;
        }
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_value2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_field2 = [[CustomTextField alloc] init];
            input_value_field2.delegate = self;
            input_value_field2.tag = field_localTag;
            field_localTag++;
            input_value_field2.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
            input_value_field2.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value2_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                input_value_field2.placeholder=valueStr;
            
            input_value_field2.textAlignment = NSTextAlignmentLeft;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_value2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([input_value_field2 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value2_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value2_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        input_value_field2.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                
                
                // Properties for textField TextColor.
                
                NSArray *textField_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField_TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField_TextColor)
                    input_value_field2.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value2_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value2_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField_TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField_TextColor)
                    input_value_field2.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            // property for KeyBoardType
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value2_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value2_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                input_value_field2.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                input_value_field2.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                input_value_field2.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                input_value_field2.secureTextEntry=YES;
                input_value_field2.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:NUMBERKEYBOARDTYPE_4])
                input_value_field2.keyboardType = UIKeyboardTypeNumberPad;
            else
                input_value_field2.keyboardType = UIKeyboardTypeDefault;
            
            input_value_field2.inputAccessoryView = numberToolbar;
            [popupTemplateScrollView addSubview:input_value_field2];
            
            next_Y_Position = input_value_field2.frame.origin.y+input_value_field2.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value2_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value2_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label2.text forKey:@"labelName"];
            [tempDictionary setObject:[input_value_field2 text] forKey:@"value"];
            
        }
        else if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton2 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton2 setFrame:CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35)];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value2_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton2 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton2 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            dropDownButton2.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton2 setTag:field_localTag];
            field_localTag++;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton2 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value2_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value2_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton2.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [dropDownButton2 setExclusiveTouch:YES];
            dropDownButton2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
               UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton2.frame.size.width-dropDownButton2.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton2.frame.size.width-dropDownButton2.intrinsicContentSize.width-8.0));
                [dropDownButton2 setTitleEdgeInsets:titleInsets];
                dropDownButton2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton2 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [popupTemplateScrollView addSubview:dropDownButton2];
            
            [[dropDownButton2 layer] setBorderWidth:0.5f];
            [[dropDownButton2 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton2.frame.size.width-20,next_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [popupTemplateScrollView addSubview:imageView];
            
            next_Y_Position = dropDownButton2.frame.origin.y+dropDownButton2.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value2_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label2.text forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton2.titleLabel.text forKey:@"value"];
        }
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_field2_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_border_label2 = [[UILabel alloc] init];
            input_value_border_label2.frame = CGRectMake(X_Position, next_Y_Position+3, popupTemplateScrollView.frame.size.width-10,1);
            input_value_border_label2.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:input_value_border_label2];
            next_Y_Position = next_Y_Position+input_value_border_label2.frame.size.height;
        }
        numberOfFields++;
        [validationsArray addObject:tempDictionary];
    }
    
    // ------------------- 3rd Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_field3_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        
        {
            input_key_label3 = [[UILabel alloc] init];
            input_key_label3.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            input_key_label3.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_label3_text",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                input_key_label3.text = labelStr;
            
            input_key_label3.textAlignment = NSTextAlignmentLeft;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template1_input_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_label3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_label3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    input_key_label3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_label3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_label3_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_label3_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_label3_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_key_label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_key_label3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            else
            {
                //Default Properties for label textcolor
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    input_key_label3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_key_label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_key_label3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [popupTemplateScrollView addSubview:input_key_label3];
            next_Y_Position = input_key_label3.frame.origin.y+input_key_label3.frame.size.height;
        }
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_value3_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_field3 = [[CustomTextField alloc] init];
            input_value_field3.delegate = self;
            input_value_field3.tag = field_localTag;
            field_localTag++;
            input_value_field3.frame = CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35);
            input_value_field3.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value3_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                input_value_field3.placeholder =valueStr ;
            
            input_value_field3.textAlignment = NSTextAlignmentLeft;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_value3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([input_value_field3 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value3_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value3_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        input_value_field3.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField_TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField_TextColor)
                    input_value_field3.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value3_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value3_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value3_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField_TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField_TextColor)
                    input_value_field3.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value3_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value3_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                input_value_field3.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                input_value_field3.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                input_value_field3.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                input_value_field3.secureTextEntry=YES;
                input_value_field3.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:NUMBERKEYBOARDTYPE_4])
                input_value_field3.keyboardType = UIKeyboardTypeNumberPad;
            else
                input_value_field3.keyboardType = UIKeyboardTypeDefault;
            
            input_value_field3.inputAccessoryView = numberToolbar;
            [popupTemplateScrollView addSubview:input_value_field3];
            next_Y_Position = input_value_field3.frame.origin.y+input_value_field3.frame.size.height;
            
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value3_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value3_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label3.text forKey:@"labelName"];
            [tempDictionary setObject:[input_value_field3 text] forKey:@"value"];
            
        }
        else if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value3_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton3 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton3 setFrame:CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35)];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value3_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton3 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton3 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            dropDownButton3.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton3 setTag:field_localTag];
            field_localTag++;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton3 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value3_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value3_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value3_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton3.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton3.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton3.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton3.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton3.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton3.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton3.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton3.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton3.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            [dropDownButton3 setExclusiveTouch:YES];
            dropDownButton3.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton3.frame.size.width-dropDownButton3.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton3.frame.size.width-dropDownButton3.intrinsicContentSize.width-8.0));
                [dropDownButton3 setTitleEdgeInsets:titleInsets];
                dropDownButton3.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton3 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [popupTemplateScrollView addSubview:dropDownButton3];
            
            [[dropDownButton3 layer] setBorderWidth:0.5f];
            [[dropDownButton3 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton3.frame.size.width-20,next_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [popupTemplateScrollView addSubview:imageView];
            
            next_Y_Position = dropDownButton3.frame.origin.y+dropDownButton3.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value3_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label3.text forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton3.titleLabel.text forKey:@"value"];
            
        }
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_field3_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_border_label3 = [[UILabel alloc] init];
            input_value_border_label3.frame = CGRectMake(X_Position, next_Y_Position+3, popupTemplateScrollView.frame.size.width-10,1);
            input_value_border_label3.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:input_value_border_label3];
            next_Y_Position = next_Y_Position+input_value_border_label3.frame.size.height;
        }
        numberOfFields++;
        [validationsArray addObject:tempDictionary];
    }
    
    // ------------------- 4th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_field4_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        {
            input_key_label4 = [[UILabel alloc] init];
            input_key_label4.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            input_key_label4.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_label4_text",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                input_key_label4.text = labelStr;
            
            input_key_label4.textAlignment = NSTextAlignmentLeft;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template1_input_label4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_label4_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_label4_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    input_key_label4.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_label4_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_label4_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_label4_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_label4_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_key_label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_key_label4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    input_key_label4.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_key_label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_key_label4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [popupTemplateScrollView addSubview:input_key_label4];
            next_Y_Position = input_key_label4.frame.origin.y+input_key_label4.frame.size.height;
        }
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_value4_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_field4 = [[CustomTextField alloc] init];
            input_value_field4.delegate = self;
            input_value_field4.tag = field_localTag;
            field_localTag++;
            input_value_field4.frame = CGRectMake(X_Position, input_key_label4.frame.origin.y+input_key_label4.frame.size.height-4, popupTemplateScrollView.frame.size.width-10,35);
            input_value_field4.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value4_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                input_value_field4.placeholder=valueStr ;
            
            input_value_field4.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_value4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([input_value_field4 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value4_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value4_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        input_value_field4.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                
                // Properties for textField TextColor.
                
                NSArray *textField_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value4_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value4_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField_TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField_TextColor)
                    input_value_field4.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value4_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value4_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value4_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value4_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else{
                
                //Default Properties for textfiled textcolor
                
                NSArray *textField_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField_TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField_TextColor)
                    input_value_field4.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            // property for KeyBoardType
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value4_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value4_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                input_value_field4.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                input_value_field4.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                input_value_field4.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                input_value_field4.secureTextEntry=YES;
                input_value_field4.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:NUMBERKEYBOARDTYPE_4])
                input_value_field4.keyboardType = UIKeyboardTypeNumberPad;
            else
                input_value_field4.keyboardType = UIKeyboardTypeDefault;
            
            input_value_field4.inputAccessoryView = numberToolbar;
            [popupTemplateScrollView addSubview:input_value_field4];
            
            next_Y_Position = input_value_field4.frame.origin.y+input_value_field4.frame.size.height;
            
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value4_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value4_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label4.text forKey:@"labelName"];
            [tempDictionary setObject:[input_value_field4 text] forKey:@"value"];
            
        }
        else if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value4_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton4 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton4 setFrame:CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35)];
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value4_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton4 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton4 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton4.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton4 setTag:field_localTag];
            field_localTag++;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value4_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value4_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton4 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value4_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value4_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value4_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value4_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton4.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton4.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton4.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton4.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton4.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton4.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton4.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton4.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton4.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [dropDownButton4 setExclusiveTouch:YES];
            dropDownButton4.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton4.frame.size.width-dropDownButton4.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton4.frame.size.width-dropDownButton4.intrinsicContentSize.width-8.0));
                [dropDownButton4 setTitleEdgeInsets:titleInsets];
                dropDownButton4.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton4 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [popupTemplateScrollView addSubview:dropDownButton4];
            
            [[dropDownButton4 layer] setBorderWidth:0.5f];
            [[dropDownButton4 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton4.frame.size.width-20,next_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [popupTemplateScrollView addSubview:imageView];
            
            next_Y_Position = dropDownButton4.frame.origin.y+dropDownButton4.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value4_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label4.text forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton4.titleLabel.text forKey:@"value"];
            
        }
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_field4_border_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_border_label4 = [[UILabel alloc] init];
            input_value_border_label4.frame = CGRectMake(X_Position, next_Y_Position+3, popupTemplateScrollView.frame.size.width-10,1);
            input_value_border_label4.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:input_value_border_label4];
            next_Y_Position = next_Y_Position+input_value_border_label4.frame.size.height;
        }
        numberOfFields++;
        [validationsArray addObject:tempDictionary];
    }
    // ------------------- 5th Value ------------------- //
    if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_field5_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        {
            input_key_label5 = [[UILabel alloc] init];
            input_key_label5.frame = CGRectMake(X_Position,next_Y_Position, popupTemplateScrollView.frame.size.width-10,40);
            input_key_label5.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_label5_text",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                input_key_label5.text = labelStr;
            
            input_key_label5.textAlignment = NSTextAlignmentLeft;
            
            if([NSLocalizedStringFromTableInBundle(@"popup_template1_input_label5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_label5_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_label5_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    input_key_label5.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_label5_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_label5_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_label5_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_label5_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_key_label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_key_label5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    input_key_label5.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_key_label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_key_label5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [popupTemplateScrollView addSubview:input_key_label5];
            next_Y_Position = input_key_label5.frame.origin.y+input_key_label5.frame.size.height;
        }
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_value5_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_field5 = [[CustomTextField alloc] init];
            input_value_field5.delegate = self;
            input_value_field5.tag = field_localTag;
            field_localTag++;
            input_value_field5.frame = CGRectMake(X_Position, input_key_label5.frame.origin.y+input_key_label5.frame.size.height-4, popupTemplateScrollView.frame.size.width-10,35);
            input_value_field5.backgroundColor = [UIColor clearColor];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value5_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                input_value_field5.placeholder =valueStr;
            
            input_value_field5.textAlignment = NSTextAlignmentLeft;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_value5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([input_value_field5 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value5_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value5_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        input_value_field5.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                
                // Properties for textField TextColor.
                
                NSArray *textField_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value5_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value5_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField_TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField_TextColor)
                    input_value_field5.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value5_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value5_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value5_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value5_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField_TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField_TextColor)
                    input_value_field5.textColor = [UIColor colorWithRed:[[textField_TextColor objectAtIndex:0] floatValue] green:[[textField_TextColor objectAtIndex:1] floatValue] blue:[[textField_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    input_value_field5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    input_value_field5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    input_value_field5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    input_value_field5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            // property for KeyBoardType
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_value5_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"popup_template1_input_value5_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                input_value_field5.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                input_value_field5.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                input_value_field5.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                input_value_field5.secureTextEntry=YES;
                input_value_field5.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:NUMBERKEYBOARDTYPE_4])
                input_value_field1.keyboardType = UIKeyboardTypeNumberPad;
            else
                input_value_field5.keyboardType = UIKeyboardTypeDefault;
            
            input_value_field5.inputAccessoryView = numberToolbar;
            [popupTemplateScrollView addSubview:input_value_field5];
            next_Y_Position = input_value_field5.frame.origin.y+input_value_field5.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value5_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_input_value5_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label5.text forKey:@"labelName"];
            [tempDictionary setObject:[input_value_field5 text] forKey:@"value"];
            
        }
        else if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value5_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton5 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton5 setFrame:CGRectMake(X_Position, next_Y_Position, popupTemplateScrollView.frame.size.width-10,35)];
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value5_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton5 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton5 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            dropDownButton5.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton5 setTag:field_localTag];
            field_localTag++;
            if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value5_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value5_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton5 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value5_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value5_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value5_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value5_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton5.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton5.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton5.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton5.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton5.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton5.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton5.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton5.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton5.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [dropDownButton5 setExclusiveTouch:YES];
            dropDownButton5.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton5.frame.size.width-dropDownButton5.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton5.frame.size.width-dropDownButton5.intrinsicContentSize.width-8.0));
                [dropDownButton5 setTitleEdgeInsets:titleInsets];
                dropDownButton5.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton5 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [popupTemplateScrollView addSubview:dropDownButton5];
            
            [[dropDownButton5 layer] setBorderWidth:0.5f];
            [[dropDownButton5 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton5.frame.size.width-20,next_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [popupTemplateScrollView addSubview:imageView];
            
            next_Y_Position = dropDownButton5.frame.origin.y+dropDownButton5.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"popup_template1_input_dropdown_value5_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:input_key_label5.text forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton5.titleLabel.text forKey:@"value"];
            
        }
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_input_field5_border_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            input_value_border_label5 = [[UILabel alloc] init];
            input_value_border_label5.frame = CGRectMake(X_Position, next_Y_Position+3, popupTemplateScrollView.frame.size.width-10,1);
            input_value_border_label5.backgroundColor = [UIColor blackColor];
            [popupTemplateScrollView addSubview:input_value_border_label5];
            next_Y_Position = next_Y_Position+input_value_border_label5.frame.size.height;
        }
        numberOfFields++;
        [validationsArray addObject:tempDictionary];
    }
    
    // ------------------- Editable Ends ------------------- //
    
    // ------------------- Buttons ------------------- //
    
    if([NSLocalizedStringFromTableInBundle(@"popup_template1_button1_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame && [NSLocalizedStringFromTableInBundle(@"popup_template1_button2_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setFrame:CGRectMake(popupTemplateLabel.frame.origin.x+10,popupTemplateLabel.frame.origin.y+popupTemplateLabel.frame.size.height-45,(popupTemplateLabel.frame.size.width/2)-10,40)];
        
        // properties For Button backgroundColor
        
        NSArray *button1_BackgroundColor;
        if (NSLocalizedStringFromTableInBundle(@"popup_template1_button1_color",propertyFileName,[NSBundle mainBundle], nil))
            button1_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_button1_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            button1_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button1_BackgroundColor)
            button1.backgroundColor=[UIColor colorWithRed:[[button1_BackgroundColor objectAtIndex:0] floatValue] green:[[button1_BackgroundColor objectAtIndex:1] floatValue] blue:[[button1_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        NSString *buttonTitle = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (buttonTitle)
            [button1 setTitle:buttonTitle forState:UIControlStateNormal];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_button1_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *button2_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                button2_TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button2_TextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[button2_TextColor objectAtIndex:0] floatValue] green:[[button2_TextColor objectAtIndex:1] floatValue] blue:[[button2_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            
            // Properties for Button Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for Button textcolor
            
            NSArray *button1_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button1_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button1_TextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[button1_TextColor objectAtIndex:0] floatValue] green:[[button1_TextColor objectAtIndex:1] floatValue] blue:[[button1_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        //[button1 setTag:100];
        [button1 setExclusiveTouch:YES];
        [button1 addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button1];
        
        button2 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button2 setFrame:CGRectMake((button1.frame.origin.x+button1.frame.size.width+10),popupTemplateLabel.frame.origin.y+popupTemplateLabel.frame.size.height-45,(popupTemplateLabel.frame.size.width/2)-16,40)];
//        [BaseViewController getControlFrameForProcessorCode:local_processorCode andPropertyFile:NSLocalizedStringFromTableInBundle(@"popup_template1_button2_visibility", propertyFileName,[NSBundle mainBundle], nil) forControl:nil toCenter:nil withYCoordinate:0.0 andXCoordinate:0.0 andHeight:0.0 andChangeableyCoordinate:nil withAdditonalControl:&popupTemplateScrollView andButton1:&button1 andButton2:&button2 withDictionary:nil forSequence:6];
        // properties For Button backgroundColor
        NSArray *button2_BackgroundColor;
        if (NSLocalizedStringFromTableInBundle(@"popup_template1_button2_color",propertyFileName,[NSBundle mainBundle], nil))
            button2_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_button2_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            button2_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button2_BackgroundColor)
            button2.backgroundColor=[UIColor colorWithRed:[[button2_BackgroundColor objectAtIndex:0] floatValue] green:[[button2_BackgroundColor objectAtIndex:1] floatValue] blue:[[button2_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        buttonTitle = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_button2_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (buttonTitle)
            [button2 setTitle:buttonTitle forState:UIControlStateNormal];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_button2_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *button2_TextColor;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_button2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_button2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                button2_TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button2_TextColor)
                [button2 setTitleColor:[UIColor colorWithRed:[[button2_TextColor objectAtIndex:0] floatValue] green:[[button2_TextColor objectAtIndex:1] floatValue] blue:[[button2_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_button2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_button2_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            
            // Properties for Button Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_button2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_button2_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for Button textcolor
            
            NSArray *button2_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button2_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button2_TextColor)
                [button2 setTitleColor:[UIColor colorWithRed:[[button2_TextColor objectAtIndex:0] floatValue] green:[[button2_TextColor objectAtIndex:1] floatValue] blue:[[button2_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [button2 setExclusiveTouch:YES];
        [button2 addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:button2];
    }
    else if ([NSLocalizedStringFromTableInBundle(@"popup_template1_button1_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 setFrame:CGRectMake(popupTemplateLabel.frame.origin.x+10,popupTemplateLabel.frame.origin.y+popupTemplateLabel.frame.size.height-45,(popupTemplateLabel.frame.size.width)-20,40)];

        // properties For Button backgroundColor
        
        NSArray *button1_BackgroundColor;
        if (NSLocalizedStringFromTableInBundle(@"popup_template1_button1_color",propertyFileName,[NSBundle mainBundle], nil))
            button1_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_button1_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            button1_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button1_BackgroundColor)
            button1.backgroundColor=[UIColor colorWithRed:[[button1_BackgroundColor objectAtIndex:0] floatValue] green:[[button1_BackgroundColor objectAtIndex:1] floatValue] blue:[[button1_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        NSString *buttonTitle = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (buttonTitle)
            [button1 setTitle:buttonTitle forState:UIControlStateNormal];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_button1_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *button2_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                button2_TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button2_TextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[button2_TextColor objectAtIndex:0] floatValue] green:[[button2_TextColor objectAtIndex:1] floatValue] blue:[[button2_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            
            // Properties for Button Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        else
        {
            //Default Properties for Button textcolor
            
            NSArray *button1_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button1_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button1_TextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[button1_TextColor objectAtIndex:0] floatValue] green:[[button1_TextColor objectAtIndex:1] floatValue] blue:[[button1_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [button1 setExclusiveTouch:YES];
        [button1 addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:button1];
    }
    
    else if ([NSLocalizedStringFromTableInBundle(@"popup_template1_button2_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        button2 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button2 setFrame:CGRectMake(popupTemplateLabel.frame.origin.x+10,popupTemplateLabel.frame.origin.y+popupTemplateLabel.frame.size.height-45,(popupTemplateLabel.frame.size.width)-20,40)];
        // properties For Button backgroundColor
        NSArray *button2_BackgroundColor;
        if (NSLocalizedStringFromTableInBundle(@"popup_template1_button2_color",propertyFileName,[NSBundle mainBundle], nil))
            button2_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_button2_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            button2_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button2_BackgroundColor)
            button2.backgroundColor=[UIColor colorWithRed:[[button2_BackgroundColor objectAtIndex:0] floatValue] green:[[button2_BackgroundColor objectAtIndex:1] floatValue] blue:[[button2_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        NSString *buttonTitle = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template1_button2_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (buttonTitle)
            [button2 setTitle:buttonTitle forState:UIControlStateNormal];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template1_button2_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *button2_TextColor;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_button2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_button2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                button2_TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button2_TextColor)
                [button2 setTitleColor:[UIColor colorWithRed:[[button2_TextColor objectAtIndex:0] floatValue] green:[[button2_TextColor objectAtIndex:1] floatValue] blue:[[button2_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_button2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_button2_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            
            // Properties for Button Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_button2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_button2_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for Button textcolor
            
            NSArray *button2_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button2_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button2_TextColor)
                [button2 setTitleColor:[UIColor colorWithRed:[[button2_TextColor objectAtIndex:0] floatValue] green:[[button2_TextColor objectAtIndex:1] floatValue] blue:[[button2_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [button2 setExclusiveTouch:YES];
        [button2 addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:button2];
    }
    
    if (self.tag != -3)
    {
        [popupTemplateScrollView setContentSize:CGSizeMake(SCREEN_WIDTH-96, numberOfFields*80)];
    }
    
    if (numberOfFields < 2) {
        popupTemplateScrollView.scrollEnabled=NO;
    }
    activityIndicator = [[ActivityIndicator alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self addSubview:activityIndicator];
    activityIndicator.hidden = YES;
}


#pragma mark - DropDown View.
/*
 * This method is used to add PopupTemplate1 dropdown button action.
 */
-(void)selectDropDown:(id)sender
{
    [self endEditing:YES];
    btn = (UIButton *)sender;
    dropdownString = [NSString stringWithFormat:@"popup_template1_input_dropdown_value%ld",(long)btn.tag];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData:) name:@"UpdateData" object:nil];
    DatabaseManager *dataManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
    dropDownDataArray = [[NSArray alloc] initWithArray:[dataManager getAllDropDownDetailsForKey:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_data_webservice_name"],propertyFileName,[NSBundle mainBundle], nil)]];
    nextTemplateProperty =  NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template_properties_file"],propertyFileName,[NSBundle mainBundle], nil);
    nextTemplate = NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template"],propertyFileName,[NSBundle mainBundle], nil);
    
    if ([NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template"],propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"1"]){
        nextTemplate=CHILD_TEMPLATE_3;
    }
    else if ([NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template"],propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"2"]){
        nextTemplate=POPUP_TEMPLATE_8;
        
    }
    
    if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
    {
        Class nextClass = NSClassFromString(nextTemplate);
        id object = nil;
        
        if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)])
        {
            object = [[nextClass alloc] initWithNibName:nextTemplate bundle:nil withPropertyName:nextTemplateProperty hasDidSelectFunction:NO withDataDictionary:nil withDataArray:dropDownDataArray withPIN:nil withProcessorCode:nil withType:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_data_webservice_name"],propertyFileName,[NSBundle mainBundle], nil) fromView:0 insideView:nil];
            [(ChildTemplate3 *)object setBaseSelector:@selector(updateData:)];
            [(ChildTemplate3 *)object setBasetarget:self];
            AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
            UINavigationController *navCntrl = (UINavigationController *)delegate.window.rootViewController;
            if (navCntrl && [navCntrl isKindOfClass:[UINavigationController class]])
            {
                [navCntrl pushViewController:object animated:NO];
            }
        }
        else if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray:withSubIndex:)])
        {
            object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:0 withDataArray:dropDownDataArray?dropDownDataArray:nil withSubIndex:0];
            [object setBasetarget:self];
            [object setBaseSelector:@selector(updateData:)];
            [[[self superview] superview] addSubview:(UIView *)object];
        }
    }
}

#pragma mark - Notification delegates.
/*
 * This method is used to add PopupTemplate1 dropdown updated data to particular field.
 */
- (void)updateData:(id)object
{
    datavalueDictionary=[[NSMutableDictionary alloc]init];
    
    if ([[object objectForKey:DROP_DOWN_TYPE] isEqualToString:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_data_webservice_name"],propertyFileName,[NSBundle mainBundle], nil)])
    {
        NSLog(@"[object objectForKey:DROP_DOWN_TYPE_DESC]   :   %@",[object objectForKey:DROP_DOWN_TYPE_DESC]);
        NSString *dropDownValue = [object objectForKey:DROP_DOWN_TYPE_DESC];
        
        if (dropDownValue.length == 0) {
            dropDownValue = [object objectForKey:DROP_DOWN_TYPE_NAME];
        }
        
        [btn setTitle:dropDownValue forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setTitle:dropDownValue forState:UIControlStateHighlighted];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        
        NSString *typeName = [NSString stringWithFormat:@"%@_typeName",[object objectForKey:DROP_DOWN_TYPE]];
        NSString *typeDesc = [NSString stringWithFormat:@"%@_typeDesc",[object objectForKey:DROP_DOWN_TYPE]];
        
        if ([object objectForKey:@"typeName"]) {
            [datavalueDictionary setObject:[object objectForKey:@"typeName"] forKey:typeName];
        }
        
        if ([object objectForKey:@"typeDesc"]) {
            [datavalueDictionary setObject:[object objectForKey:@"typeDesc"] forKey:typeDesc];
        }
        
        [datavalueDictionary setObject:[object objectForKey:DROP_DOWN_TYPE_NAME] forKey:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_param_type"],propertyFileName,[NSBundle mainBundle], nil)];
    }
    
}

#pragma mark - Toolbar methods.
/**
 * This method is used to cancel button action of tool bar.
 */
-(void)cancelNumberPad
{
    if ([activeField isFirstResponder])
    {
        [activeField resignFirstResponder];
        activeField.text = @"";
    }
}

/**
 * This method is used to ok button action of tool bar.
 */
-(void)doneWithNumberPad
{
    if ([activeField isFirstResponder])
        [activeField resignFirstResponder];
}

#pragma mark -  Button methods
/**
 * This method is used to set button1 action of popup template1.
 @prameters are defined in button action those are
             * Application display type(ticker,PopUp),validation type,next template,next template PropertyFile,Action type,
  Feature-(processorCode,transaction type).
 */
-(void)button1Action:(id)sender
{
    if ([activeField isFirstResponder])
        [activeField resignFirstResponder];
    
    if([propertyFileName isEqualToString:@"TransactionHistoryMPINPPT1"]){
        AppDelegate *apDlgt = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        UINavigationController *navCntrl = (UINavigationController *)apDlgt.window.rootViewController;
        [navCntrl popViewControllerAnimated:NO];
    }
    
    alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    validation_Type = NSLocalizedStringFromTableInBundle(@"application_validation_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"popup_template1_button1_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplate = NSLocalizedStringFromTableInBundle(@"popup_template1_button1_next_template",propertyFileName,[NSBundle mainBundle], nil);
    NSString *actionType =  NSLocalizedStringFromTableInBundle(@"popup_template1_button1_action_type",propertyFileName,[NSBundle mainBundle], nil);
    NSString *apiParamType = NSLocalizedStringFromTableInBundle(@"popup_template1_button1_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil);
    
    NSString *data = NSLocalizedStringFromTableInBundle(@"popup_template1_button1_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    processor_Code = [Template getProcessorCodeWithData:data];
    NSString *transactionType = [Template getTransactionCodeWithData:data];
    
    if (transactionType) {
        [localDataDictionary setObject:transactionType forKey:PARAMETER13];
    }
    
    if (processor_Code) {
        [localDataDictionary setObject:processor_Code forKey:PARAMETER15];
    }
    NSLog(@"Action Type is.%@",actionType);
    Template *obj = [Template initWithactionType:actionType nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:propertyFileName ProcessorCode:processor_Code Dictionary:localDataDictionary contentArray:nil alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:apiParamType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:transactionType withCurrentClass:POPUP_TEMPLATE_1];
    obj.selector = @selector(processData);
    obj.target = self;
    [[NSNotificationCenter defaultCenter] postNotificationName:PPT1_BUTTON1_ACTION object:obj];
}
/**
 * This method is used to set button2 action of popup template1.
 */
-(void)button2Action:(id)sender
{
    if ([activeField isFirstResponder])
        [activeField resignFirstResponder];
    
    alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    validation_Type = NSLocalizedStringFromTableInBundle(@"application_validation_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"popup_template1_button2_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplate = NSLocalizedStringFromTableInBundle(@"popup_template1_button2_next_template",propertyFileName,[NSBundle mainBundle], nil);
    NSString *actionType =  NSLocalizedStringFromTableInBundle(@"popup_template1_button2_action_type",propertyFileName,[NSBundle mainBundle], nil);
    NSString *apiParamType = NSLocalizedStringFromTableInBundle(@"popup_template1_button2_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil);
    
    NSString *data = NSLocalizedStringFromTableInBundle(@"popup_template1_button2_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    processor_Code = [Template getProcessorCodeWithData:data];
    NSString *transactionType = [Template getTransactionCodeWithData:data];
    
    if (transactionType) {
        [localDataDictionary setObject:transactionType forKey:PARAMETER13];
    }
    
    if (processor_Code) {
        [localDataDictionary setObject:processor_Code forKey:PARAMETER15];
    }
    
    Template *obj = [Template initWithactionType:actionType nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:propertyFileName ProcessorCode:processor_Code Dictionary:localDataDictionary contentArray:validationsArray alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:apiParamType withLableValidation:validationsArray withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:transactionType withCurrentClass:POPUP_TEMPLATE_1];
    obj.selector = @selector(processData);
    obj.target = self;
    [[NSNotificationCenter defaultCenter] postNotificationName:PPT1_BUTTON2_ACTION object:obj];
}

#pragma mark - BaseView process Data method.
/**
 * This method is used to set baseviewcontroller Process data notification.
 */
-(void)processData
{
    //Process Data
}

#pragma mark - PopUp template Button Action.
/**
 * This method is used to set Hide and clear textfield  popup template1 Data.
 */
-(void)popUpTemplateButton2:(NSNotification *)notification
{
    NSArray *viewsArray = [[NSArray alloc] initWithArray:[[self superview] subviews]];
//    NSLog(@"Views Array...%@",viewsArray);
    for (int i = 0; i<[viewsArray count]; i++)
    {
        UIView *lView = [viewsArray objectAtIndex:i];
        if (lView.tag < 0)
        {
            [lView removeFromSuperview];
        }
    }
}

/**
 * This method is used to remove notification data.
 */
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - TextField  Delegate methods
/**
 * This method is used to keybord show and hide method(resign,become responder).
 */
- (void)textFieldDidBeginEditing:(CustomTextField *)textField
{
    activeField = textField;
    NSLog(@"Self Tag Value is..%ld",(long)self.tag);
//    if (self.tag == -3)
//    {
        [self animateTextField:textField up:YES];
//    }
//
//    else
//    {
//        CGPoint scrollPoint = CGPointMake(0,textField.frame.origin.y-35);
//        [popupTemplateScrollView setContentOffset:scrollPoint animated:YES];
//    }
}

- (void)textFieldDidEndEditing:(CustomTextField *)textField
{
    [textField resignFirstResponder];
    
//    if (self.tag == -3)
//    {
        [self animateTextField:textField up:NO];
//    }
//    else
//    {
//        [popupTemplateScrollView setContentOffset:CGPointZero animated:YES];
//    }
    
}
- (BOOL)textFieldShouldBeginEditing:(CustomTextField *)textField
{
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    
    if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
    {
        textField.textAlignment=NSTextAlignmentRight;
    }
    else
    {
        textField.textAlignment=NSTextAlignmentLeft;
    }
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(CustomTextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(CustomTextField *)textField
{
    return YES;
}
-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance =(SCREEN_HEIGHT>480)? -130:-100;
    const float movementDuration = 0.3f;
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.frame = CGRectOffset(self.frame, 0, movement);
    [UIView commitAnimations];
}

@end
