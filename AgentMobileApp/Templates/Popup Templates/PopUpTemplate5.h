//
//  PopUpTemplate5.h
//  Consumer Client
//
//  Created by android on 6/3/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 * This class used to handle functionality and view of Popup Template5.
 *
 * @author Integra
 *
 */
@interface PopUpTemplate5 : UIView
{
    NSArray *localDataArray;
    
    int nextY_position;
    int xPosition;
    
    int selectedIndex;
    int arrayCount;
    
    int reselectedValue;
    
    int numberofFields;
}
/**
 * declarations are used to set the UIConstraints Of Popup template5.
 *Label,Value label,Border label and Buttons.
 */
@property(nonatomic,strong) NSString *propertyFileName;
@property(nonatomic,strong) UILabel *popupTemplateLabel;
@property(nonatomic,strong) UIScrollView *popupTemplateScrollView;
@property(nonatomic,strong) UILabel *headingTitle_label;
@property(nonatomic,strong) UILabel *headingTitle_border_label;
@property(nonatomic,strong) UILabel *featureTitle_label;

@property(nonatomic,strong) UILabel *key_label1;
@property(nonatomic,strong) UILabel *value_label1;
@property(nonatomic,strong) UILabel *value_border_label1;

@property(nonatomic,strong) UILabel *key_label2;
@property(nonatomic,strong) UILabel *value_label2;
@property(nonatomic,strong) UILabel *value_border_label2;

@property(nonatomic,strong) UILabel *key_label3;
@property(nonatomic,strong) UILabel *value_label3;
@property(nonatomic,strong) UILabel *value_border_label3;

@property(nonatomic,strong) UILabel *key_label4;
@property(nonatomic,strong) UILabel *value_label4;
@property(nonatomic,strong) UILabel *value_border_label4;

@property(nonatomic,strong) UILabel *key_label5;
@property(nonatomic,strong) UILabel *value_label5;
@property(nonatomic,strong) UILabel *value_border_label5;

@property(nonatomic,strong) UILabel *key_label6;
@property(nonatomic,strong) UILabel *value_label6;
@property(nonatomic,strong) UILabel *value_border_label6;

@property(nonatomic,strong) UILabel *key_label7;
@property(nonatomic,strong) UILabel *value_label7;
@property(nonatomic,strong) UILabel *value_border_label7;

@property(nonatomic,strong) UILabel *key_label8;
@property(nonatomic,strong) UILabel *value_label8;
@property(nonatomic,strong) UILabel *value_border_label8;

@property(nonatomic,strong) UILabel *key_label9;
@property(nonatomic,strong) UILabel *value_label9;
@property(nonatomic,strong) UILabel *value_border_label9;

@property(nonatomic,strong) UILabel *key_label10;
@property(nonatomic,strong) UILabel *value_label10;
@property(nonatomic,strong) UILabel *value_border_label10;

@property(nonatomic,strong) UIButton *button1;
@property(nonatomic,strong) UIButton *button2;
@property(nonatomic,strong) UIButton *button3;

/**
 * This method is  used for Method Initialization of Popup template5.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile andDelegate:(id)delegate withDataDictionary:(NSDictionary *)dataDictionary withProcessorCode:(NSString *)processorCode  withTag:(int)tagValue withTitle:(NSString *)title  andMessage:(NSString *)message withSelectedIndexPath:(int)selectedIndexPath withDataArray:(NSArray *)dataArray withSubIndex:(int)subIndex;

//Button Action
-(void)deleteButton_Action:(id)sender;
@end