//
//  PopUpTemplate8.h
//  Consumer Client
//
//  Created by android on 11/30/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 * This class used to handle functionality and view of Popup Template8.
 *
 * @author Integra
 *
 */
@interface PopUpTemplate8 : UIView<UITableViewDataSource,UITableViewDelegate>
{
    int labelX_position;
    int labelY_position;
    
    int distanceY;
    int rowIndex;
    
    int selectedValueInteger;
    int selectedCount;
}
/**
 * declarations are used to set the UIConstraints Of Popup template8.
 *Label,Value label,Border label and Tableview.
 */
@property(nonatomic,strong) NSString *propertyFileName;
@property(nonatomic,strong) NSString *processor_Code;

@property(nonatomic,strong) NSArray *valuesArr;
@property(nonatomic,strong) UILabel *popupTemplateLabel;
@property(strong,nonatomic) UILabel *headerLabel;
@property(strong,nonatomic) UILabel *headderlabelBorder;

@property(strong,nonatomic) UITableView *popupTempletTblview;
@property(strong,nonatomic) UILabel *valueLabel;
@property(strong,nonatomic) UIButton *cancelBtn;
@property(nonatomic) SEL baseSelector;
@property(nonatomic) id basetarget;

/**
 * This method is  used for Method Initialization of Popup template8.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile andDelegate:(id)delegate withDataDictionary:(NSDictionary *)dataDictionary withProcessorCode:(NSString *)processorCode  withTag:(int)tagValue withTitle:(NSString *)title  andMessage:(NSString *)message withSelectedIndexPath:(int)selectedIndexPath withDataArray:(NSArray *)dataArray withSubIndex:(int)subIndex;

@end
