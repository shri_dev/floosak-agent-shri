//
//  PopUpTemplate9.h
//  ConsumerClient
//
//  Created by android on 17/06/16.
//  Copyright (c) 2016 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ActivityIndicator.h"
/**
 * This class used to handle functionality and view of Popup Template3.
 *
 * @author Integra
 *
 */
@interface PopUpTemplate9 : UIView<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *datavalArray;
    NSDictionary *datavalDictionary;
    NSMutableDictionary *localDataDictionary;
    ActivityIndicator *activityIndicator;
    NSString *local_processorCode;
    NSString *transactionType;
    int y_Position;
}

/**
 * declarations are used to set the UIConstraints Of Popup template3.
 *Label,Value label,Table View and Buttons.
 */
@property(nonatomic,strong) NSString *propertyFileName;
@property(nonatomic,strong) NSString *processor_Code;
@property(nonatomic,strong) UILabel *headerLabel1;
@property(nonatomic,strong) UILabel *headerLabel2;
@property(nonatomic,strong) UILabel *borderLabel;
@property(nonatomic,strong) UITableView *myAccountsTableView;
@property(nonatomic,strong) UILabel *sectionLabel1;
@property(nonatomic,strong) UILabel *sectionLabel2;
@property(nonatomic,strong) UILabel *sectionLabel3;
@property(nonatomic,strong) UILabel *valueLabel1;
@property(nonatomic,strong) UILabel *valueLabel2;
@property(nonatomic,strong) UILabel *valueLabel3;
@property(nonatomic,strong) UIButton *button1;
@property(nonatomic,strong) UILabel *alertLabel;

@property(nonatomic,strong) UILabel *popupTemplate3Label;

/**
 * This method is  used for Method Initialization of Popup template3.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile andDelegate:(id)delegate withDataDictionary:(NSDictionary *)dataDictionary withProcessorCode:(NSString *)processorCode  withTag:(int)tagValue withTitle:(NSString *)title  andMessage:(NSString *)message withSelectedIndexPath:(int)selectedIndexPath withDataArray:(NSArray *)dataArray withSubIndex:(int)subIndex;
@end
