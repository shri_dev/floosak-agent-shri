//
//  PopUpTemplate7.h
//  Consumer Client
//
//  Created by android on 6/24/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 * This class used to handle functionality and view of Popup Template7.
 *
 * @author Integra
 *
 */
@interface PopUpTemplate7 : UIView<UITableViewDelegate,UITableViewDataSource>
{
    int labelX_position;
    int labelY_position;
    
    int distanceY;
    int rowIndex;
}
/**
 * declarations are used to set the UIConstraints Of Popup template7.
 *Label,Value label,Border label and Tableview.
 */
@property(nonatomic,strong) NSArray *valuesArr;
@property(nonatomic,strong) NSString *propertyFileName;
@property(nonatomic,strong) NSString *processor_Code;
@property(nonatomic,strong) UILabel *popupTemplateLabel;
@property(nonatomic,strong) UILabel *header_TitleLabel1;
@property(nonatomic,strong) UILabel *header_BorderLabel1;
@property(nonatomic,strong) UIButton *cancelBtn;

@property(nonatomic,strong)UILabel *header_TitleLabel2;

@property(nonatomic,strong) UITableView *popup_TableView;
@property(nonatomic,strong) UIImageView *button_Icon;
@property(nonatomic,strong) UILabel *valueLabel1;

// button action
-(void)deleteButtonAction;
/**
 * This method is  used for Method Initialization of Popup template1.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile andDelegate:(id)delegate withDataDictionary:(NSDictionary *)dataDictionary withProcessorCode:(NSString *)processorCode  withTag:(int)tagValue withTitle:(NSString *)title  andMessage:(NSString *)message withSelectedIndexPath:(int)selectedIndexPath withDataArray:(NSArray *)dataArray withSubIndex:(int)subIndex;

@end
