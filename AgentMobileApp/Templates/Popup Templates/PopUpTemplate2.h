//
//  PopUpTemplate2.h
//  Consumer Client
//
//  Created by Integra Micro on 27/04/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 * This class used to handle functionality and view of Popup Template2.
 *
 * @author Integra
 *
 */
@interface PopUpTemplate2 : UIView
{
    int nextY_position;
    NSMutableArray *validationsArray;
    NSMutableArray *labelValidationsArray;
    NSMutableDictionary *localDataDictionary;
    NSMutableDictionary *logindetailsDict;
    NSString *messageString;
}
/**
 *  Declartions are used to set the UIConstraints Of Popup template1.
 *Label,Value label,Border label and Buttons.
 */
@property(nonatomic,strong) NSString *propertyFileName;
@property(nonatomic,strong) NSString *processor_Code;
@property(nonatomic,strong) NSString *titleStr;
@property(nonatomic,strong) UILabel *popupTemplateLabel;

@property(nonatomic,strong) UILabel *headingTitle_label;
@property(nonatomic,strong) UILabel *headingTitle_border_label;

@property(nonatomic,strong) UILabel *message_label;
@property(nonatomic,strong) UILabel *message_border_label;

@property(nonatomic,strong) UIButton *button1;
@property(nonatomic,strong) UIButton *button2;
/**
 * This method is  used for Method Initialization of Popup template2.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile andDelegate:(id)delegate withDataDictionary:(NSDictionary *)dataDictionary withProcessorCode:(NSString *)processorCode  withTag:(int)tagValue withTitle:(NSString *)title  andMessage:(NSString *)message withSelectedIndexPath:(int)selectedIndexPath withDataArray:(NSArray *)dataArray withSubIndex:(int)subIndex;

@end
