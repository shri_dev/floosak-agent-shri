//  PopUpTemplate9.m
//  ConsumerClient
//
//  Created by android on 17/06/16.
//  Copyright (c) 2016 Soumya. All rights reserved.


#import "PopUpTemplate9.h"
#import "Constants.h"
#import "Localization.h"
#import "ValidationsClass.h"
#import "Template.h"
#import "WebServiceConstants.h"
#import "Constants.h"
#import "NotificationConstants.h"
#import "UIView+Toast.h"
#import "PopUpTemplate2.h"
#import "DatabaseConstatants.h"
#import "WebSericeUtils.h"
#import "WebServiceRequestFormation.h"
#import "WebServiceDataObject.h"
#import "AppDelegate.h"
#import "Localization.h"
#import "Template.h"


@implementation PopUpTemplate9
@synthesize popupTemplate3Label,propertyFileName,processor_Code;
@synthesize headerLabel1,headerLabel2,borderLabel,myAccountsTableView,sectionLabel1,sectionLabel2,sectionLabel3,valueLabel1,valueLabel2,valueLabel3,button1,alertLabel;


- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile andDelegate:(id)delegate withDataDictionary:(NSDictionary *)dataDictionary withProcessorCode:(NSString *)processorCode  withTag:(int)tagValue withTitle:(NSString *)title  andMessage:(NSString *)message withSelectedIndexPath:(int)selectedIndexPath withDataArray:(NSArray *)dataArray withSubIndex:(int)subIndex
{
   
    NSLog(@"PropertyFileName:%@",propertyFile);
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self setBackgroundColor:[UIColor colorWithRed:0.1f green:0.1f blue:0.1f alpha:0.4f]];
        propertyFileName=propertyFile;
        localDataDictionary=[[NSMutableDictionary alloc]initWithDictionary:dataDictionary];
        NSLog(@"local Data Dictioanry...%@",localDataDictionary);
        local_processorCode=[NSString stringWithFormat:@"%@",PROCESSOR_CODE_TXN_SUMMARY_T3];
        transactionType=[NSString stringWithFormat:@"%@",TRANSACTION_CODE_TXN_SUMMARY_T3];
        
        NSMutableDictionary *webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
        [webServiceRequestInputDetails setObject:local_processorCode forKey:PARAMETER15];
        [webServiceRequestInputDetails setObject:transactionType forKey:PARAMETER13];
        
//        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"staticListValue"] isEqualToString:@"Previous Day"]) {
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"staticListValue"] isEqualToString:@"label_id_previous_day"]) {
            [webServiceRequestInputDetails setObject:@"yest" forKey:PARAMETER18];
            
        }
//        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"staticListValue"] isEqualToString:@"Summary for the day"]) {
        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"staticListValue"] isEqualToString:@"label_id_summary_for_the_day"]) {
            [webServiceRequestInputDetails setObject:@"Today" forKey:PARAMETER18];
        }
//        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"staticListValue"] isEqualToString:@"Last Month Summary"]) {
        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"staticListValue"] isEqualToString:@"label_id_last_month_summary"]) {
            [webServiceRequestInputDetails setObject:@"lastMonthSummary" forKey:PARAMETER18];
        }
        [self commonWebServiceCallWithData:webServiceRequestInputDetails andProcessorCode:local_processorCode];
          [self addControlsForView];
    }
    
    NSLog(@"Selected List Value is...%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"staticListValue"]);
    
    
    return self;
}
-(void)addControlsForView
{
    int x_Position = 0;
     y_Position = 0;
    
    datavalArray=[[NSMutableArray alloc]init];
    popupTemplate3Label = [[UILabel alloc] init];
    popupTemplate3Label.frame = CGRectMake(20,50,SCREEN_WIDTH-40,SCREEN_HEIGHT-100);
    NSArray *backGroundColors = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_background_color",propertyFileName,[NSBundle mainBundle], nil)];
    if (backGroundColors)
        popupTemplate3Label.backgroundColor = [UIColor colorWithRed:[[backGroundColors objectAtIndex:0] floatValue] green:[[backGroundColors objectAtIndex:1] floatValue] blue:[[backGroundColors objectAtIndex:2] floatValue] alpha:1.0f];
    
    popupTemplate3Label.backgroundColor=[UIColor whiteColor];
    [self addSubview:popupTemplate3Label];
    
   // header Label1
    if ([NSLocalizedStringFromTableInBundle(@"popup_template9_header_label1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
    headerLabel1=[[UILabel alloc]init];
    headerLabel1.frame=CGRectMake(40, 60, SCREEN_WIDTH-80 , 30);
    headerLabel1.textAlignment=NSTextAlignmentLeft;
        
    NSString *headerStr=[Localization languageSelectedStringForKey:[[NSUserDefaults standardUserDefaults]objectForKey:@"staticListValue"]];
        
        if (headerStr)
            headerLabel1.text = headerStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template9_header_label1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *label_TextColor;
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_header_label1_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_header_label1_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                headerLabel1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_header_label1_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_header_label1_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_header_label1_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_header_label1_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                headerLabel1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
      x_Position=headerLabel1.frame.origin.x+headerLabel1.intrinsicContentSize.width+5;
      y_Position=headerLabel1.frame.origin.y;
    [self addSubview:headerLabel1];
    }
    // header Label2
    if ([NSLocalizedStringFromTableInBundle(@"popup_template9_header_label2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        headerLabel2=[[UILabel alloc]init];
        headerLabel2.frame=CGRectMake(x_Position, y_Position, SCREEN_WIDTH-80 , 30);
        headerLabel2.textAlignment=NSTextAlignmentLeft;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        
        NSString *headerStr;
        // Yesterday Date
        NSDateComponents *components =[[NSDateComponents alloc] init];
        [components setDay:-1];
         NSDate *yesterday = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
        // Last Month date
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
        [offsetComponents setMonth:-1];
        NSDate *endOfWorldWar3 = [gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0];
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"staticListValue"] isEqualToString:@"Previous Day"]) {
            headerStr=[dateFormatter stringFromDate:yesterday];
        }
        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"staticListValue"] isEqualToString:@"Summary for the day"]) {
            //Today Date
            headerStr=[dateFormatter stringFromDate:[NSDate date]];
            
        }
        else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"staticListValue"] isEqualToString:@"Last Month Summary"]) {
             headerStr=[dateFormatter stringFromDate:endOfWorldWar3];
        }
//        [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template9_header_label2",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (headerStr)
            headerLabel2.text = headerStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template9_header_label2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_header_label2_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_header_label2_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                headerLabel2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_header_label2_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_header_label2_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_header_label2_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_header_label2_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerLabel2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerLabel2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                headerLabel2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerLabel2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerLabel2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        x_Position=headerLabel2.frame.origin.x+headerLabel2.intrinsicContentSize.width+5;
        y_Position=headerLabel2.frame.origin.y+headerLabel2.frame.size.height;
        [self addSubview:headerLabel2];
    }
    // header border label
    if ([NSLocalizedStringFromTableInBundle(@"popup_template9_header_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        borderLabel=[[UILabel alloc]init];
        borderLabel.frame=CGRectMake(40, y_Position, SCREEN_WIDTH-80 , 1);
        borderLabel.backgroundColor = [UIColor blackColor];
        y_Position=borderLabel.frame.origin.y+borderLabel.frame.size.height;
        [self addSubview:borderLabel];
    }
    
    // section Label1
    if ([NSLocalizedStringFromTableInBundle(@"popup_template9_title_label1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        sectionLabel1=[[UILabel alloc]init];
        sectionLabel1.frame=CGRectMake(35, y_Position, 90, 20);
        sectionLabel1.textAlignment=NSTextAlignmentLeft;
        
        NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template9_title_label1",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (headerStr)
            sectionLabel1.text = headerStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template9_title_label1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_header_label2_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_title_label1_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                sectionLabel1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_title_label1_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_title_label1_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_title_label1_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_title_label1_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                sectionLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                sectionLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                sectionLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                sectionLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                sectionLabel1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                sectionLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                sectionLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                sectionLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                sectionLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        x_Position=sectionLabel1.frame.origin.x+sectionLabel1.frame.size.width;
        [self addSubview:sectionLabel1];
    }
    // Section Label2
    if ([NSLocalizedStringFromTableInBundle(@"popup_template9_title_label2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        sectionLabel2=[[UILabel alloc]init];
        sectionLabel2.frame=CGRectMake(x_Position, y_Position, 90, 20);
        sectionLabel2.textAlignment=NSTextAlignmentLeft;
        
        NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template9_title_label2",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (headerStr)
            sectionLabel2.text = headerStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template9_title_label2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_header_label2_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_title_label2_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                sectionLabel2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_title_label2_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_title_label2_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_title_label2_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_title_label2_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                sectionLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                sectionLabel2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                sectionLabel2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                sectionLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                sectionLabel2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                sectionLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                sectionLabel2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                sectionLabel2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                sectionLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        x_Position=sectionLabel2.frame.origin.x+sectionLabel2.frame.size.width;
        sectionLabel2.numberOfLines=0;
        [sectionLabel2 sizeToFit];
        [self addSubview:sectionLabel2];
    }
    // section Label3
    if ([NSLocalizedStringFromTableInBundle(@"popup_template9_title_label3_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        sectionLabel3=[[UILabel alloc]init];
        sectionLabel3.frame=CGRectMake(x_Position, y_Position, 90, 20);
        sectionLabel3.textAlignment=NSTextAlignmentLeft;
        
        NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template9_title_label3",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (headerStr)
            sectionLabel3.text=headerStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template9_title_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_title_label3_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_title_label3_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                sectionLabel3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_title_label3_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_title_label3_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_title_label3_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_title_label3_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                sectionLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                sectionLabel3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                sectionLabel3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                sectionLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                sectionLabel3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                sectionLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                sectionLabel3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                sectionLabel3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                sectionLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        y_Position=sectionLabel3.frame.origin.y+sectionLabel3.frame.size.height;
        [self addSubview:sectionLabel3];
    }
    if([datavalArray count] == 0)
    {
        [self removeActivityIndicator];
        alertLabel=[[UILabel alloc]init];
        alertLabel.frame=CGRectMake(popupTemplate3Label.frame.origin.x+15, y_Position, SCREEN_WIDTH-70, (popupTemplate3Label.frame.size.height-120));
        alertLabel.hidden=NO;
        alertLabel.textAlignment=NSTextAlignmentCenter;
        myAccountsTableView.hidden=YES;
        alertLabel.text=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template9_recent_transactions_not_available", propertyFileName,[NSBundle mainBundle], nil)];
        y_Position=alertLabel.frame.origin.y+alertLabel.frame.size.height;
        [self addSubview:alertLabel];
    }
    if ([NSLocalizedStringFromTableInBundle(@"popup_template9_button1_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        button1.frame=CGRectMake(popupTemplate3Label.frame.origin.x+15,y_Position,SCREEN_WIDTH-70,35);
        NSArray *button1_BackgroundColor;
        if (NSLocalizedStringFromTableInBundle(@"popup_template9_button1_color",propertyFileName,[NSBundle mainBundle], nil))
            button1_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_button1_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            button1_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button1_BackgroundColor)
            button1.backgroundColor=[UIColor colorWithRed:[[button1_BackgroundColor objectAtIndex:0] floatValue] green:[[button1_BackgroundColor objectAtIndex:1] floatValue] blue:[[button1_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        NSString *buttonTitle = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template9_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (buttonTitle)
            [button1 setTitle:buttonTitle forState:UIControlStateNormal];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template9_button1_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *button2_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                button2_TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button2_TextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[button2_TextColor objectAtIndex:0] floatValue] green:[[button2_TextColor objectAtIndex:1] floatValue] blue:[[button2_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            
            // Properties for Button Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        else
        {
            //Default Properties for Button textcolor
            
            NSArray *button1_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button1_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button1_TextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[button1_TextColor objectAtIndex:0] floatValue] green:[[button1_TextColor objectAtIndex:1] floatValue] blue:[[button1_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [button1 setExclusiveTouch:YES];
        [button1 addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button1];
    }
}


#pragma mark - TableView dataSource And Delegate method.
/**
 * This method is used to set add Tableview (list and delegate methods) of ChildTemplate14.
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [datavalArray count];
}
/**
 * This method is used to set add Tableview for ChildTemplate14.
 *@param type- Declare Listview related UI(Labels,Images etc..base on Req).
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"cashin";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    int X_Position=6;
    int Y_POSITION=10;

    // Value label1
    if ([NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        valueLabel1=[[UILabel alloc]init];
        valueLabel1.frame=CGRectMake(X_Position, Y_POSITION, (SCREEN_WIDTH-(X_Position*2))/3, 22);
        
        NSString *headerStr=NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label1_param_type",propertyFileName,[NSBundle mainBundle], nil);
        
        if (headerStr)
            valueLabel1.text=[[datavalArray objectAtIndex:indexPath.row] objectForKey:headerStr];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label1_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label1_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                valueLabel1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label1_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label1_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label1_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label1_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                valueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                valueLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                valueLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                valueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                valueLabel1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                valueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                valueLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                valueLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            else
                valueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        valueLabel1.numberOfLines=2;
        valueLabel1.textAlignment=NSTextAlignmentCenter;
        [valueLabel1 sizeToFit];
        X_Position=X_Position+valueLabel1.frame.size.width+valueLabel1.frame.origin.x+25;
        [cell.contentView addSubview:valueLabel1];
    }
    // Value label2
    if ([NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        valueLabel2=[[UILabel alloc]init];
        valueLabel2.frame=CGRectMake(110, Y_POSITION, 100, 22);
        
        NSString *headerStr=NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label2_param_type",propertyFileName,[NSBundle mainBundle], nil);
        
        if (headerStr)
            valueLabel2.text=[[datavalArray objectAtIndex:indexPath.row] objectForKey:headerStr];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label2_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label2_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                valueLabel2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label2_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label2_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label2_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label2_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                valueLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                valueLabel2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                valueLabel2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                valueLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                valueLabel2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                valueLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                valueLabel2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                valueLabel2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                valueLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        valueLabel2.numberOfLines=2;
        valueLabel2.textAlignment=NSTextAlignmentCenter;
        [valueLabel2 sizeToFit];
        X_Position=valueLabel2.frame.size.width+valueLabel2.frame.origin.x;
        [cell.contentView addSubview:valueLabel2];
    }
    // Value label3
    if ([NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label3_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        valueLabel3=[[UILabel alloc]init];
        valueLabel3.frame=CGRectMake(160, Y_POSITION, 100, 22);
        [valueLabel3 setTextAlignment:NSTextAlignmentRight];
        NSString *headerStr=NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label3_param_type",propertyFileName,[NSBundle mainBundle], nil);
        
        if (headerStr)
            valueLabel3.text=[[datavalArray objectAtIndex:indexPath.row] objectForKey:headerStr];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label3_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label3_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                valueLabel3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label3_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label3_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label3_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_listitem_label3_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                valueLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                valueLabel3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                valueLabel3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                valueLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                valueLabel3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                valueLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                valueLabel3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                valueLabel3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                valueLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        valueLabel3.numberOfLines=2;
        valueLabel3.textAlignment=NSTextAlignmentCenter;
        [valueLabel3 sizeToFit];
        [cell.contentView addSubview:valueLabel3];
    }
       return cell;

}
/**
 * This method is used to set add Tableview Delegate Method User selects List .
 *@param type- Declare Listview - (NextTemplate and Next template proprty file).
 *To show List Item Detailed Data etc..
 */
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSArray *templateArray=[(NSLocalizedStringFromTableInBundle(@"child_template14_list_next_template",propertyFileName,[NSBundle mainBundle], nil)) componentsSeparatedByString:@","];
//    NSArray *nextTemplateArray = [(NSLocalizedStringFromTableInBundle(@"child_template14_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil)) componentsSeparatedByString:@","];
//    Class myclass = NSClassFromString([templateArray objectAtIndex:indexPath.row]);
//    
//    int selected = (int)indexPath.row;
//    
//    //To be removed
//    NSUserDefaults *userDefaultsData=[NSUserDefaults standardUserDefaults];
//    [userDefaultsData setValue:[Localization languageSelectedStringForKey:[dataArray objectAtIndex:selected]] forKey:@"topupType"];
//    NSLog(@"Data Array : %@",[dataArray objectAtIndex:indexPath.row]);
//    
//    //adding Static Id's
//    [userDefaultsData setValue:[Localization languageSelectedStringForKey:[dataArray objectAtIndex:selected]] forKey:@"staticListValue"];
//    [userDefaultsData setValue:[templateIdArray objectAtIndex:indexPath.row]  forKey:@"staticListId"];
//    [userDefaultsData synchronize];
//    
//    //To be removed
//    [dictionary setObject:[userDefaultsData objectForKey:@"topupType"] forKey:@"topupType"];
//    
//    id obj = nil;
//    if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
//    {
//        obj = [[myclass alloc] initWithNibName:[templateArray objectAtIndex:indexPath.row] bundle:nil withSelectedIndex:(int)[[NSUserDefaults standardUserDefaults] integerForKey:@"selected_index"] fromView:0 withFromView:[NSString stringWithFormat:@"%d",(int)(indexPath.row+1)]  withPropertyFile:[nextTemplateArray objectAtIndex:indexPath.row] withProcessorCode:nil dataArray:nil dataDictionary:dictionary?dictionary:nil];
//        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//        UINavigationController *navController = (UINavigationController *)delegate.window.rootViewController;
//        [self removeFromSuperview];
//        [navController pushViewController:obj animated:NO];
//    }
//}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

/**
 * This method is used to set button1 action of popup template2.
 @prameters are defined in button action those are
 * Application display type(ticker,PopUp),validation type,next template,next template PropertyFile,Action type,
 Feature-(processorCode,transaction type).
 */
-(void)button1Action:(id)sender
{
    [self removeFromSuperview];
//    NSString *alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
//    NSString *validation_Type = NSLocalizedStringFromTableInBundle(@"application_validation_type", @"GeneralSettings",[NSBundle mainBundle], nil);
//    NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"popup_template9_button1_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
//    NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"popup_template9_button1_next_template",propertyFileName,[NSBundle mainBundle], nil);
//    NSString *actionType =  NSLocalizedStringFromTableInBundle(@"popup_template9_button1_action_type",propertyFileName,[NSBundle mainBundle], nil);
//    NSString *apiParamType = NSLocalizedStringFromTableInBundle(@"popup_template9_button1_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil);
//
//    
//    NSString *data = NSLocalizedStringFromTableInBundle(@"popup_template9_button1_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
//    processor_Code = [Template getProcessorCodeWithData:data];
//    NSString *transactionType = [Template getTransactionCodeWithData:data];
//    
//    if (transactionType) {
//        [localDataDictionary setObject:transactionType forKey:PARAMETER13];
//    }
//    
//    if (processor_Code) {
//        [localDataDictionary setObject:processor_Code forKey:PARAMETER15];
//    }
//    
//    NSLog(@"Action Type is...%@",actionType);
//    Template *obj = [Template initWithactionType:actionType nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:propertyFileName ProcessorCode:processor_Code Dictionary:localDataDictionary contentArray:nil alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:apiParamType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:transactionType withCurrentClass:POPUP_TEMPLATE_3];
//    obj.selector = @selector(processData);
//    obj.target = self;
//    [[NSNotificationCenter defaultCenter] postNotificationName:PPT3_BUTTON1_ACTION object:obj];
}

#pragma mark - BaseView Process Data methods.
/**
 * This method is used to set baseviewcontroller Process data notification.
 */
//-(void)processData
//{
//    //ProcessData
//}
#pragma mark - WebService Call method.
/*
 * This method is used to set childtemplate2 webservice calling common method.
 */
-(void)commonWebServiceCallWithData:(NSMutableDictionary *)inputDataDictionary andProcessorCode:(NSString *)processorCode
{
    [self setActivityIndicator];
    WebSericeUtils *webUtils = [[WebSericeUtils alloc] init];
    NSMutableDictionary *webUtilsValues = [[NSMutableDictionary alloc] initWithDictionary:[webUtils getWebServiceBundleObjectWithProccessorCode:processorCode withInputDataModel:inputDataDictionary]];
    
    WebServiceRequestFormation *webServiceRequest = [[WebServiceRequestFormation alloc] init];
    NSString *webRequest  = [webServiceRequest sendRequest:webUtilsValues];
    
    WebServiceRunning *webServiceRun = [[WebServiceRunning alloc] init];
    [webServiceRun startWebServiceWithRequest:webRequest withReqDictionary:webUtilsValues withDelegate:self];
}

-(void)processData:(WebServiceDataObject *)webServiceDataObject withProcessCode:(NSString *)processCode
{
    [self removeActivityIndicator];
    
    if (webServiceDataObject.faultCode && webServiceDataObject.faultString)
    {
        [self showAlertForErrorWithMessage:webServiceDataObject.faultString andErrorCode:nil];
    }
    else
    {
        if ([local_processorCode isEqualToString:PROCESSOR_CODE_TXN_SUMMARY_T3] && [transactionType isEqualToString:TRANSACTION_CODE_TXN_SUMMARY_T3]) {
            NSString *jsondataString=webServiceDataObject.PaymentDetails2;
            NSArray *transactionHistoryRecords = [jsondataString componentsSeparatedByString:@";"];
            int noOfRecords = (int)[transactionHistoryRecords count];
            NSMutableDictionary *tempDictionary;
            for (int index = 0; index < noOfRecords; index++)
            {
                tempDictionary = [[NSMutableDictionary alloc] init];

                NSMutableArray *transactionFileds = (NSMutableArray *)[[transactionHistoryRecords objectAtIndex:index] componentsSeparatedByString:@"|"];
                
                if ([transactionFileds count] > 1)
                {
                    NSLog(@"transactionFileds at %d: %@",index,transactionFileds);
                if ([transactionFileds objectAtIndex:0])
                    [tempDictionary setObject:[transactionFileds objectAtIndex:0] forKey:MINI_STATEMENT_TRANSACTION_TYPE];
                else
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_TYPE];
                
                if ([transactionFileds objectAtIndex:1])
                    [tempDictionary setObject:[transactionFileds objectAtIndex:1] forKey:MINISTATEMENT_TRANSACTION_COUNT];
                else
                    [tempDictionary setObject:@"" forKey:MINISTATEMENT_TRANSACTION_COUNT];
                
                if ([transactionFileds objectAtIndex:2])
                    [tempDictionary setObject:[transactionFileds objectAtIndex:2] forKey:MINI_STATEMENT_TRANSACTION_AMOUNT];
                else
                    [tempDictionary setObject:@"" forKey:MINI_STATEMENT_TRANSACTION_AMOUNT];
                
              [datavalArray addObject:tempDictionary];
            }
                if(datavalArray.count > 0)
                {
                    [myAccountsTableView reloadData];
                    alertLabel.hidden = YES;
                    myAccountsTableView.hidden=NO;
                        myAccountsTableView=[[UITableView alloc]init];
                        myAccountsTableView.frame= CGRectMake(popupTemplate3Label.frame.origin.x+15, 110, SCREEN_WIDTH-70, (popupTemplate3Label.frame.size.height-120));
                        myAccountsTableView.delegate=self;
                        myAccountsTableView.dataSource=self;
                        y_Position=myAccountsTableView.frame.origin.y+myAccountsTableView.frame.size.height;
                        [self addSubview:myAccountsTableView];
                }
                else
                {
                    alertLabel.hidden=NO;
                    myAccountsTableView.hidden=YES;
                }
                
            }
            
        }
    }
}
#pragma mark - Xml Error Handling methods.
/*
 * This method is used to set process data error handling.
 */

-(void)errorCodeHandlingInDBWithCode:(NSString *)errorCode withProcessorCode:(NSString *)processorCode
{
    [self removeActivityIndicator];
}

-(void)errorCodeHandlingInXML:(NSString *)errorCode andMessgae:(NSString *)message withProcessorCode:(NSString *)processorCode
{
    [self removeActivityIndicator];
    [self showAlertForErrorWithMessage:message andErrorCode:errorCode];
}

-(void)errorCodeHandlingInWebServiceRunningWithErrorCode:(NSString *)errorCode
{
    [self removeActivityIndicator];
    [self showAlertForErrorWithMessage:nil andErrorCode:errorCode];
    
    myAccountsTableView.hidden=YES;
}
/*
 * This method is used to set process data error handling show alert based on type(Ticker or popup).
 */
-(void)showAlertForErrorWithMessage:(NSString *)message andErrorCode:(NSString *)errorCode
{
    [self removeActivityIndicator];
    NSString *errorMessage = nil;
    
    if (errorCode)
        errorMessage = NSLocalizedStringFromTableInBundle(errorCode,[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil);
    else
        errorMessage = message;
    
    NSString *alertview_Type=NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    
    NSLog(@"AlertView Type is...%@",alertview_Type);
    
    if([alertview_Type compare:TICKER_TEXT] == NSOrderedSame)
    {
        NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
        
        NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
        
        [[[self superview] superview] makeToast:errorMessage duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
    }
    else if ([alertview_Type compare:POPUP_TEXT] == NSOrderedSame)
    {
        PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]] andMessage:errorMessage withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
        [[[self superview] superview] addSubview:popup];
    }
}

#pragma mark - Add ctivity indicator.
/**
 * This method is used to set Add Activity indicator for View.
 */
-(void) setActivityIndicator
{
    [[[[UIApplication sharedApplication] delegate] window] addSubview:activityIndicator];
//    [self addSubview:activityIndicator];
     activityIndicator.hidden = NO;
    [activityIndicator startActivityIndicator];
}
#pragma mark - remove ctivity indicator.
/**
 * This method is used to set Activity indicator Remove from View.
 */
-(void) removeActivityIndicator
{
    [activityIndicator removeFromSuperview];
    [activityIndicator stopActivityIndicator];
}
/*
 * This method is used to remove Notification.
 */
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
