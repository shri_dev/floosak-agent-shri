//
//  PopUpTemplate7.m
//  Consumer Client
//
//  Created by android on 6/24/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "PopUpTemplate7.h"

#import "Constants.h"
#import "Localization.h"

#import "ValidationsClass.h"
#import "UIView+Toast.h"

#import "WebSericeUtils.h"
#import "WebServiceConstants.h"
#import "WebServiceRequestFormation.h"
#import "WebServiceRunning.h"
#import "WebServiceDataObject.h"
#import "ParentTemplate1.h"
#import "DatabaseConstatants.h"
#import "AppDelegate.h"
#import "ConvertDetailsToSrverRequestFormate.h"
#import "Template.h"

@implementation PopUpTemplate7
{
    NSArray *templatesArray;
    NSArray *templatePropertyArray;
    NSArray *templateIdArray;
    
    NSArray *array;
    int tag;
    NSDictionary *templatesDictionary;
    int parentTag;
    int subTag;
    NSMutableDictionary *dictionary;
}

@synthesize propertyFileName,processor_Code,valuesArr;
@synthesize header_BorderLabel1,header_TitleLabel1,header_TitleLabel2,popup_TableView,popupTemplateLabel,valueLabel1,cancelBtn,button_Icon;

#pragma mark - PopupTemplate1 UIView.
/**
 * This method is used to set implemention of Popup template7.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile andDelegate:(id)delegate withDataDictionary:(NSDictionary *)dataDictionary withProcessorCode:(NSString *)processorCode  withTag:(int)tagValue withTitle:(NSString *)title  andMessage:(NSString *)message withSelectedIndexPath:(int)selectedIndexPath withDataArray:(NSArray *)dataArray withSubIndex:(int)subIndex
{
   
    NSLog(@"PropertyFileName:%@",propertyFile);
    self = [super initWithFrame:frame];

    templatesDictionary = [Template getNextFeatureTemplateWithIndex:selectedIndexPath];
    parentTag = selectedIndexPath;
    dictionary = [[NSMutableDictionary alloc] initWithDictionary:dataDictionary];    
    if (self)
    {
        [self setBackgroundColor:[UIColor colorWithRed:0.1f green:0.1f blue:0.1f alpha:0.4f]];
        [self addControllersForView];
        if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"0"])
        {
            if (propertyFile) {
                propertyFileName = propertyFile;
                [self addControllersForView];
            }
            else
            {
                propertyFileName = [[templatesDictionary objectForKey:@"templateProperties"] objectAtIndex:selectedIndexPath];
                
                [self addControllersForView];
            }
        }
        if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"1"])
        {
            if (propertyFile) {
                propertyFileName = propertyFile;
                tagValue = -1;
                subTag = subIndex;
                tag= selectedIndexPath;
                [self addControllersForView];
            }
            else
            {
                propertyFileName = [[templatesDictionary objectForKey:KEY_CATEGORY_TEMPLATE_PROPERTY_FILES] objectAtIndex:subIndex];
                tagValue = -1;
                subTag = subIndex;
                tag= selectedIndexPath;
                [self addControllersForView];
            }
        }
    }
    return self;
}


#pragma mark  - PopUpTemplate7 UI constraints Creation.
/**
 * This method is used to set add UIconstraints Frame of Popup template7.
 */
-(void)addControllersForView
{
    NSString *templateProperties = NSLocalizedStringFromTableInBundle(@"popup_template7_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
    NSString *templates = NSLocalizedStringFromTableInBundle(@"popup_template7_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    NSString *templateId = NSLocalizedStringFromTableInBundle(@"popup_template7_list_item_id",propertyFileName,[NSBundle mainBundle], nil);
    
    templateIdArray = [templateId componentsSeparatedByString:@","];
    templatesArray = [templates componentsSeparatedByString:@","];
    templatePropertyArray = [templateProperties componentsSeparatedByString:@","];

    popupTemplateLabel=[[UILabel alloc]init];
    popupTemplateLabel.frame=CGRectMake(0,0,self.frame.size.width-60,(self.frame.size.height/3)+20);
    popupTemplateLabel.center = self.center;
    
    popupTemplateLabel.backgroundColor=[UIColor whiteColor];
    [self addSubview:popupTemplateLabel];
    
    valuesArr =[[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template7_list_item_name",propertyFileName,[NSBundle mainBundle], nil)] componentsSeparatedByString:@","];
    distanceY=8;
    
    // Header Label1
    
    if ([NSLocalizedStringFromTableInBundle(@"popup_template7_header_label_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        header_TitleLabel1=[[UILabel alloc]init];
        header_TitleLabel1.frame=CGRectMake(popupTemplateLabel.frame.origin.x+10, popupTemplateLabel.frame.origin.y+10, popupTemplateLabel.frame.size.width-20, 40);
        header_TitleLabel1.backgroundColor=[UIColor clearColor];
        
        NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template7_header_label",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (headerStr)
            header_TitleLabel1.text=headerStr;
        
        header_TitleLabel1.lineBreakMode = NSLineBreakByTruncatingTail;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template7_header_label_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *headerLabelTextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template7_header_label_color",propertyFileName,[NSBundle mainBundle], nil))
                headerLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template7_header_label_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                headerLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                headerLabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (headerLabelTextColor)
                header_TitleLabel1.textColor = [UIColor colorWithRed:[[headerLabelTextColor objectAtIndex:0] floatValue] green:[[headerLabelTextColor objectAtIndex:1] floatValue] blue:[[headerLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template7_header_label_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"popup_template7_header_label_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template7_header_label_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize =NSLocalizedStringFromTableInBundle(@"popup_template7_header_label_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize =NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize =application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                header_TitleLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                header_TitleLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                header_TitleLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                header_TitleLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else{
            //Default Properties for label textcolor
            
            NSArray *headerLabelTextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                headerLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                headerLabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (headerLabelTextColor)
                header_TitleLabel1.textColor = [UIColor colorWithRed:[[headerLabelTextColor objectAtIndex:0] floatValue] green:[[headerLabelTextColor objectAtIndex:1] floatValue] blue:[[headerLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle=NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize =NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize =application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                header_TitleLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                header_TitleLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                header_TitleLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                header_TitleLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        header_TitleLabel1.textAlignment=NSTextAlignmentCenter;
        header_TitleLabel1.numberOfLines=0;
        labelY_position=popupTemplateLabel.frame.origin.y+(distanceY*2)+header_TitleLabel1.frame.size.height;
        [self addSubview:header_TitleLabel1];
   
        // close button
        cancelBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        cancelBtn.frame=CGRectMake(popupTemplateLabel.frame.size.width-10, popupTemplateLabel.frame.origin.y+7, 30, 30);
        [cancelBtn setImage:[UIImage imageNamed:CLOSE_IMAGE_NAME] forState:UIControlStateNormal];
        cancelBtn.exclusiveTouch=YES;
        [cancelBtn addTarget:self action:@selector(deleteButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:cancelBtn];
        
        // Header label border
        if ([NSLocalizedStringFromTableInBundle(@"popup_template7_header_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            header_BorderLabel1=[[UILabel alloc]init];
            header_BorderLabel1.frame=CGRectMake(popupTemplateLabel.frame.origin.x+10,labelY_position, popupTemplateLabel.frame.size.width-20, 1);
            header_BorderLabel1.backgroundColor=[UIColor blackColor];
            labelY_position=labelY_position+header_BorderLabel1.frame.size.height;
            [self addSubview:header_BorderLabel1];
        }
        
        // header TitleLabel2
        if ([NSLocalizedStringFromTableInBundle(@"popup_template7_title_label_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            header_TitleLabel2=[[UILabel alloc]init];
            header_TitleLabel2.frame=CGRectMake(popupTemplateLabel.frame.origin.x+10,labelY_position, popupTemplateLabel.frame.size.width-20, 40);
            header_TitleLabel2.backgroundColor=[UIColor clearColor];
            
            NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template7_title_label",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (headerStr)
                header_TitleLabel2.text=headerStr;
            
            header_TitleLabel2.lineBreakMode = NSLineBreakByTruncatingTail;
            
            if ([NSLocalizedStringFromTableInBundle(@"popup_template7_title_label_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *headerLabelTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template7_title_label_color",propertyFileName,[NSBundle mainBundle], nil))
                    headerLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template7_title_label_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    headerLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    headerLabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (headerLabelTextColor)
                    header_TitleLabel2.textColor = [UIColor colorWithRed:[[headerLabelTextColor objectAtIndex:0] floatValue] green:[[headerLabelTextColor objectAtIndex:1] floatValue] blue:[[headerLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template7_title_label_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle =NSLocalizedStringFromTableInBundle(@"popup_template7_title_label_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle =NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle =application_default_text_style;
                
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template7_title_label_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"popup_template7_title_label_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize =application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    header_TitleLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    header_TitleLabel2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    header_TitleLabel2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    header_TitleLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else{
                //Default Properties for label textcolor
                
                NSArray *headerLabelTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    headerLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    headerLabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (headerLabelTextColor)
                    header_TitleLabel2.textColor = [UIColor colorWithRed:[[headerLabelTextColor objectAtIndex:0] floatValue] green:[[headerLabelTextColor objectAtIndex:1] floatValue] blue:[[headerLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle=NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle =application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize =application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    header_TitleLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    header_TitleLabel2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    header_TitleLabel2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    header_TitleLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            header_TitleLabel2.textAlignment=NSTextAlignmentCenter;
            header_TitleLabel2.numberOfLines=0;
            labelY_position=labelY_position+header_TitleLabel2.frame.size.height+distanceY;
            [self addSubview:header_TitleLabel2];
        }

            // Table view.
        popup_TableView=[[UITableView alloc]init];
        popup_TableView.frame=CGRectMake(popupTemplateLabel.frame.origin.x, labelY_position, popupTemplateLabel.frame.size.width,popupTemplateLabel.frame.size.height-30);
        popup_TableView.dataSource=self;
        popup_TableView.delegate=self;
        labelY_position=labelY_position+popup_TableView.frame.size.height;
        [popup_TableView setTableFooterView:[UIView new]];
        [self addSubview:popup_TableView];
    }
 }

#pragma mark -   TABLEVIEW DATA SOURCE AND DELEGATE METHODS.
/**
 * This method is used to set add Tableview (list and delegate methods) of Popup template7.
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [valuesArr count];
}
/**
 * This method is used to set add Tableview for PopuPtemplate7.
 *@param type- Declare Listview related UI(Labels,Images etc..base on Req).
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cashin";
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
      if (cell == nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    valueLabel1=[[UILabel alloc]init];
    labelX_position=10;
    
     // Value label.
    
    valueLabel1.frame=CGRectMake(labelX_position, 19, SCREEN_WIDTH-96, 22);
        valueLabel1.text=[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:[valuesArr objectAtIndex:indexPath.row]]];
        valueLabel1.lineBreakMode = NSLineBreakByTruncatingTail;

        if ([NSLocalizedStringFromTableInBundle(@"popup_template7_listitem_label_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *valueLabelTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"popup_template7_listitem_label_color",propertyFileName,[NSBundle mainBundle], nil))
                    valueLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template7_listitem_label_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    valueLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    valueLabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (valueLabelTextColor)
                    valueLabel1.textColor = [UIColor colorWithRed:[[valueLabelTextColor objectAtIndex:0] floatValue] green:[[valueLabelTextColor objectAtIndex:1] floatValue] blue:[[valueLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template7_listitem_label_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle =NSLocalizedStringFromTableInBundle(@"popup_template7_listitem_label_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle =NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle =application_default_text_style;
                
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template7_listitem_label_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"popup_template7_listitem_label_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize =application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    valueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    valueLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    valueLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    valueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                valueLabel1.alpha=0.3;
            }
            else{
                //Default Properties for label textcolor
                
                NSArray *valueLabelTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    valueLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    valueLabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (valueLabelTextColor)
                    valueLabel1.textColor = [UIColor colorWithRed:[[valueLabelTextColor objectAtIndex:0] floatValue] green:[[valueLabelTextColor objectAtIndex:1] floatValue] blue:[[valueLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle =NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle =application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"popup_template7_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize =application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    valueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    valueLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    valueLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    valueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        valueLabel1.numberOfLines=2;
        [valueLabel1 sizeToFit];
        [cell.contentView addSubview:valueLabel1];
    
    
    return cell;
}

/**
 * This method is used to set add Tableview Delegate Method User selects List .
 *@param type- Declare Listview - (NextTemplate and Next template proprty file).
 *To show List Item Detailed Data etc..
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    int selected = (int)indexPath.row;
    NSUserDefaults *userDefaultsData=[NSUserDefaults standardUserDefaults];
    
    //To be removed
    [userDefaultsData setValue:[Localization languageSelectedStringForKey:[valuesArr objectAtIndex:selected]] forKey:@"topupType"];
    [userDefaultsData setValue:[templateIdArray objectAtIndex:indexPath.row] forKey:@"selectedPPT7_TemplateId"];

    //adding Static Id's
    [userDefaultsData setValue:[Localization languageSelectedStringForKey:[valuesArr objectAtIndex:selected]] forKey:@"staticListValue"];
    [userDefaultsData setValue:[templateIdArray objectAtIndex:indexPath.row]  forKey:@"staticListId"];
    [userDefaultsData synchronize];
    
    [dictionary setObject:[userDefaultsData objectForKey:@"topupType"] forKey:@"topupType"];
    
    Class myclass = NSClassFromString([templatePropertyArray objectAtIndex:indexPath.row]);
    id obj = nil;
    if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
    {
        if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"0"])
        {
            obj = [[myclass alloc] initWithNibName:[templatePropertyArray objectAtIndex:indexPath.row] bundle:nil withSelectedIndex:(int)[[NSUserDefaults standardUserDefaults] integerForKey:@"selected_index"] fromView:0 withFromView:[NSString stringWithFormat:@"%d",(int)(subTag)]  withPropertyFile:[templatesArray objectAtIndex:indexPath.row] withProcessorCode:nil dataArray:nil dataDictionary:dictionary?dictionary:nil];
        }
        else if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"1"])
        {
            obj = [[myclass alloc] initWithNibName:[templatePropertyArray objectAtIndex:indexPath.row] bundle:nil withSelectedIndex:tag fromView:0 withFromView:[NSString stringWithFormat:@"%d",(int)(subTag)]  withPropertyFile:[templatesArray objectAtIndex:indexPath.row] withProcessorCode:nil dataArray:nil dataDictionary:dictionary?dictionary:nil];
        }
        
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UINavigationController *navController = (UINavigationController *)delegate.window.rootViewController;
        [self removeFromSuperview];
        [navController pushViewController:obj animated:NO];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.contentView.alpha = 0.3;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Cancel Button Action
/**
 * This method is used to set close button action of Popup template7.
 */
-(void)deleteButtonAction:(id)sender{
    
    [self removeFromSuperview];
    
}



@end

