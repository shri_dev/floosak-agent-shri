//
//  PopUpTemplate6.h
//  Consumer Client
//
//  Created by android on 6/24/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "XmlParserHandler.h"
#import "ActivityIndicator.h"
#import "CustomTextField.h"
/**
 * This class used to handle functionality and view of Popup Template6
 *
 * @author Integra
 *
 */

@interface PopUpTemplate6 : UIView<UITextFieldDelegate,UIScrollViewDelegate,XMLParserHandlerDelegate>
{
    NSMutableDictionary *localDataDictionary;
    /**
     Declare a toolbar for pickerview
     */
    UIToolbar *numberToolbar;
    
    CustomTextField *activeField;
    
    NSMutableArray *validationsArray;
    NSMutableArray *labelValidationsArray;
    
    ActivityIndicator *activityIndicator;
    NSString *validation_Type;
    NSString *alertview_Type;
    
    NSString *local_processorCode;
    
    NSString *nextTemplateProperty;
    NSString *nextTemplate;
    
    NSString *dropdownString;
    UIButton *btn;
    NSArray *dropDownDataArray;
    NSMutableDictionary *datavalueDictionary;
    
    NSMutableDictionary *payBill_webServiceRequestInputDetails;
    
    //below variables are for VerifyRegistrationPPT6_T2
    int verifyImagesCount;
    int currentImage;
    UIImage *refImage1;
    UIImage *refImage2;
    UIImage *refImage3;
    UIImage *refImage4;
    UIImageView *refImageView;
    NSString *mpinValidationString;
    BOOL isApproved;
    BOOL isRejected;
    NSString *userMobileNumber;
    BOOL isImageReversed;
    BOOL isArabic;
}
/**
 * declarations are used to set the UIConstraints Of Popup template6.
 *Label,Value label,Border label and Buttons.
 *keylabel,Textfield and Dropdownbutton.
 */
@property(nonatomic,strong) NSString *propertyFileName;
@property(nonatomic,strong) NSString *processor_Code;
@property(nonatomic,strong) UILabel *popupTemplateLabel;
@property(nonatomic,strong) UILabel *valueLabel;
@property(nonatomic,strong) UIScrollView *popupTemplateScrollView;

@property(nonatomic,strong) UILabel *headingTitle_label;
@property(nonatomic,strong) UILabel *headingTitle_border_label;

@property(nonatomic,strong) UILabel *key_label1;
@property(nonatomic,strong) UILabel *value_label1;
@property(nonatomic,strong) UILabel *value_border_label1;

@property(nonatomic,strong) UILabel *key_label2;
@property(nonatomic,strong) UILabel *value_label2;
@property(nonatomic,strong) UILabel *value_border_label2;

@property(nonatomic,strong) UILabel *key_label3;
@property(nonatomic,strong) UILabel *value_label3;
@property(nonatomic,strong) UILabel *value_border_label3;

@property(nonatomic,strong) UILabel *key_label4;
@property(nonatomic,strong) UILabel *value_label4;
@property(nonatomic,strong) UILabel *value_border_label4;

@property(nonatomic,strong) UILabel *key_label5;
@property(nonatomic,strong) UILabel *value_label5;
@property(nonatomic,strong) UILabel *value_border_label5;

@property(nonatomic,strong) UILabel *key_label6;
@property(nonatomic,strong) UILabel *value_label6;
@property(nonatomic,strong) UILabel *value_border_label6;

@property(nonatomic,strong) UILabel *key_label7;
@property(nonatomic,strong) UILabel *value_label7;
@property(nonatomic,strong) UILabel *value_border_label7;

@property(nonatomic,strong) UILabel *key_label8;
@property(nonatomic,strong) UILabel *value_label8;
@property(nonatomic,strong) UILabel *value_border_label8;

@property(nonatomic,strong) UILabel *key_label9;
@property(nonatomic,strong) UILabel *value_label9;
@property(nonatomic,strong) UILabel *value_border_label9;

@property(nonatomic,strong) UILabel *key_label10;
@property(nonatomic,strong) UILabel *value_label10;
@property(nonatomic,strong) UILabel *value_border_label10;

@property(nonatomic,strong) UILabel *key_label11;
@property(nonatomic,strong) UILabel *value_label11;
@property(nonatomic,strong) UILabel *value_border_label11;

@property(nonatomic,strong) UILabel *key_label12;
@property(nonatomic,strong) UILabel *value_label12;
@property(nonatomic,strong) UILabel *value_border_label12;

@property(nonatomic,strong) UILabel *key_label13;
@property(nonatomic,strong) UILabel *value_label13;
@property(nonatomic,strong) UILabel *value_border_label13;

@property(nonatomic,strong) UILabel *key_label14;
@property(nonatomic,strong) UILabel *value_label14;
@property(nonatomic,strong) UILabel *value_border_label14;

@property(nonatomic,strong) UILabel *key_label15;
@property(nonatomic,strong) UILabel *value_label15;
@property(nonatomic,strong) UILabel *value_border_label15;

@property(nonatomic,strong) UILabel *key_label16;
@property(nonatomic,strong) UILabel *value_label16;
@property(nonatomic,strong) UILabel *value_border_label16;

@property(nonatomic,strong) UILabel *key_label17;
@property(nonatomic,strong) UILabel *value_label17;
@property(nonatomic,strong) UILabel *value_border_label17;

@property(nonatomic,strong) UILabel *key_label18;
@property(nonatomic,strong) UILabel *value_label18;
@property(nonatomic,strong) UILabel *value_border_label18;

@property(nonatomic,strong) UILabel *input_key_label1;
@property(nonatomic,strong) CustomTextField *input_value_field1;
@property(nonatomic,strong) UILabel *input_value_border_label1;
@property(nonatomic,strong) UIButton *dropDownButton1;

@property(nonatomic,strong) UILabel *input_key_label2;
@property(nonatomic,strong) CustomTextField *input_value_field2;
@property(nonatomic,strong) UILabel *input_value_border_label2;
@property(nonatomic,strong) UIButton *dropDownButton2;

@property(nonatomic,strong) UILabel *input_key_label3;
@property(nonatomic,strong) CustomTextField *input_value_field3;
@property(nonatomic,strong) UILabel *input_value_border_label3;
@property(nonatomic,strong) UIButton *dropDownButton3;

@property(nonatomic,strong) UILabel *input_key_label4;
@property(nonatomic,strong) CustomTextField *input_value_field4;
@property(nonatomic,strong) UILabel *input_value_border_label4;
@property(nonatomic,strong) UIButton *dropDownButton4;

@property(nonatomic,strong) UILabel *input_key_label5;
@property(nonatomic,strong) CustomTextField *input_value_field5;
@property(nonatomic,strong) UILabel *input_value_border_label5;
@property(nonatomic,strong) UIButton *dropDownButton5;

@property(nonatomic,strong) UIButton *button1;
@property(nonatomic,strong) UIButton *button2;

/**
 * This method is  used for Method Initialization of Popup template6.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile andDelegate:(id)delegate withDataDictionary:(NSDictionary *)dataDictionary withProcessorCode:(NSString *)processorCode  withTag:(int)tagValue withTitle:(NSString *)title  andMessage:(NSString *)message withSelectedIndexPath:(int)selectedIndexPath withDataArray:(NSArray *)dataArray withSubIndex:(int)subIndex;


@end
