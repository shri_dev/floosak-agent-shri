//
//  ScreenTimeOut.m
//  Consumer Client
//
//  Created by android on 11/20/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "ScreenTimeOut.h"
#import "Localization.h"
#import "ValidationsClass.h"
#import "Constants.h"
#import "OptionsMenu.h"
#import "Template.h"
#import "DatabaseManager.h"
#import "DatabaseConstatants.h"
#import "NotificationConstants.h"
#import "ParentTemplate3.h"
#import "BaseViewController.h"
#import "UIView+Toast.h"
#import "AGPushNoteView.h"

@interface ScreenTimeOut()
@property (strong, nonatomic) NSDate *lastInteraction;
@property (assign, nonatomic) BOOL didTimeout;
@end
@implementation ScreenTimeOut
@synthesize propertyFileName;

- (void)sendEvent:(UIEvent *)event
{
    [super sendEvent:event];
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] > 0)
    {
        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
        if (phase == UITouchPhaseBegan || phase == UITouchPhaseEnded)
            [self resetIdleTimer];
    }
}

- (void)resetIdleTimer
{
    self.lastInteraction = [NSDate date];
    
    if (self.idleTimer)
    {
        [self.idleTimer invalidate];
    }
    
    self.didTimeout = NO;
    self.idleTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                      target:self
                                                    selector:@selector(checkDidTimeout)
                                                    userInfo:nil
                                                     repeats:YES];
}

- (BOOL)checkDidTimeout
{
    NSDate *currentDate = [NSDate date];
    NSTimeInterval interactionTime = [currentDate timeIntervalSinceDate: self.lastInteraction];
    NSTimeInterval sessionTime=[NSLocalizedStringFromTableInBundle(@"application_session_timeout",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue];
    if (interactionTime > sessionTime - 1 && !self.didTimeout)
    {
        [self timedOut];
        return YES;
    }
    
    return NO;
}

- (void)timedOut
{
    [self.idleTimer invalidate];
    self.idleTimer = nil;
    self.didTimeout = YES;
    [self idleTimerExceeded];
}

- (void)idleTimerExceeded
{
    UIViewController *object =[[NSClassFromString(application_launch_template) alloc] initWithNibName:application_launch_template bundle:nil withSelectedIndex:0 fromView:0 withFromView:nil withPropertyFile:application_launch_template_property_files_list withProcessorCode:nil dataArray:nil dataDictionary:nil];
    [NSTimer scheduledTimerWithTimeInterval:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] target:self selector:@selector(alertMessage) userInfo:nil repeats:NO];
   
    [AGPushNoteView close];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"userLogged"];
    UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:object];
    [navi setNavigationBarHidden:YES];
    [[[[UIApplication sharedApplication] delegate] window] setRootViewController:navi];
    

}
-(void)alertMessage
{
    NSString *errorMessage=NSLocalizedStringFromTableInBundle(@"application_session_timeout_message",@"GeneralSettings",[NSBundle mainBundle], nil);
    NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
    NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
    UIWindow *mainWindow = [UIApplication sharedApplication].windows[0];
    [mainWindow makeToast:errorMessage duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
    // Needed to check PLS un comment
//    NSLog(@"Error message ..%@",errorMessage);
    
    
}

@end
