//
//  ScreenTimeOut.h
//  Consumer Client
//
//  Created by android on 11/20/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
// define MaxIdleTime 1000000


@interface ScreenTimeOut : UIApplication

@property (strong, nonatomic) NSTimer *idleTimer;
@property (strong, nonatomic)NSString *propertyFileName;

-(void)idleTimerExceeded;


@end
