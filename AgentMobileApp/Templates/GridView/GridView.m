//
//  GridView.m
//  ConsumerClient
//
//  Created by android on 26/05/16.
//  Copyright (c) 2016 Soumya. All rights reserved.
//

#import "GridView.h"
#import "Constants.h"
#import "Template.h"
#import "Localization.h"
#import "ValidationsClass.h"

@implementation GridView
@synthesize gridView,featureImageViewButton,featurenameLabel,selectedItemIndexPath;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma mark - optionsMenu Frame Initialization.
/**
 * This method is used to set implemention of option Menu.
 */

//- (id)initWithFrame:(CGRect)frame withPropertyFileName:(NSString *)propertyFile withIndex:(int)tag
- (id)initWithFrame:(CGRect)frame withSelectedIndex:(NSInteger)index fromView:(int)fromView withSelectedCategory:(NSInteger)categoryIndex andSetSubTemplates:(BOOL)subTemplates withParentIndex :(NSInteger)pIndex

{
   
    self = [super initWithFrame:frame];
    if (self)
    {
        [self addViewControls];
        // feature menu Array.
        if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"0"])
        {
            featureNameArray = [[Template getNextFeatureTemplateWithIndex:(int)index] objectForKey:KEY_TEMPLATES];
            featureImageArray = [[Template getNextFeatureTemplateWithIndex:(int)index] objectForKey:IMAGES];
        }
        else
            if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"1"])
            {
                if (subTemplates) {
                    int index1 = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"topLevelNavigation"];
                    featureNameArray = [[Template getNextFeatureTemplateWithIndex:index1] objectForKey:KEY_CATEGORY_FEATURES];
                    featureImageArray = [[Template getNextFeatureTemplateWithIndex:index1] objectForKey:IMAGES];
                }
                else
                {
                    featureNameArray = [[Template getNextFeatureTemplateWithIndex:(int)index] objectForKey:CATEGORIES_NAMES_STRING];
                    featureImageArray = [[Template getNextFeatureTemplateWithIndex:(int)index] objectForKey:CATEGORIES_IMAGES];
                }
            }
        NSLog(@"Feature name array....%@",featureNameArray);
    }
    return self;
}

#pragma mark - Add Uiconstraints for Options menu.
/**
 * This method is used to set add UIconstraints Frame of GridView menu.
 */
-(void)addViewControls
{
    
    // FeatureMenu BackGround Color.
    
//    NSArray *viewbackGroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"featureslist_template_back_ground_color",@"Features_List",[NSBundle mainBundle], nil)];
//    [mainScrollView setBackgroundColor:[UIColor colorWithRed:[[viewbackGroundColor objectAtIndex:0] floatValue] green:[[viewbackGroundColor objectAtIndex:1] floatValue] blue:[[viewbackGroundColor objectAtIndex:2] floatValue] alpha:1.0f]];
    
    //[self addSubview:mainScrollView];
    
    
    // CollectionView For gridView
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    gridView=[[UICollectionView alloc] initWithFrame:CGRectMake(10,10,SCREEN_WIDTH-20,SCREEN_HEIGHT-(SCREEN_HEIGHT/2.1)) collectionViewLayout:layout];
    [gridView setDataSource:self];
    [gridView setDelegate:self];
    [gridView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [gridView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:gridView];
}

#pragma  mark - TABLEVIEW DATASOURCE AND DELEGATE Methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(90, 90);
}

/**
 * This method is used to set Tabelview (list and Delegate methods) Of Option Menu.
 *@param - Features Provided (Befor Login and AfterLogin)
 */
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [featureNameArray count];
}

/**
 * This method is used to set add Tableview for OptionsMenu.
 *@param type- Declare Listview related UI(Labels,Images etc..base on Req).
 */
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    [[[cell contentView] subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    featureImageViewButton=[UIButton buttonWithType:UIButtonTypeCustom];
    featureImageViewButton.frame=CGRectMake(10, 10, 65, 65);
    featureImageViewButton.layer.cornerRadius = featureImageViewButton.frame.size.height/2;
    
    NSArray *iconBtncolorsArray = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"feature_list_view_default_color", @"GeneralSettings",[NSBundle mainBundle], nil)];
//    featureImageView.backgroundColor = [UIColor colorWithRed:[[iconBtncolorsArray objectAtIndex:0] floatValue] green:[[iconBtncolorsArray objectAtIndex:1] floatValue] blue:[[iconBtncolorsArray objectAtIndex:2] floatValue] alpha:1.0f];
    featureImageViewButton.layer.borderColor = [[UIColor colorWithRed:[[iconBtncolorsArray objectAtIndex:0] floatValue] green:[[iconBtncolorsArray objectAtIndex:1] floatValue] blue:[[iconBtncolorsArray objectAtIndex:2] floatValue] alpha:1.0f] CGColor];
    featureImageViewButton.layer.cornerRadius = featureImageViewButton.bounds.size.height/2;
    featureImageViewButton.layer.borderWidth = 1.0;
    featureImageViewButton.clipsToBounds = YES;
    [featureImageViewButton setUserInteractionEnabled:false];
    
    UIImage* iconImage = [UIImage imageNamed:[NSString stringWithFormat:@"feature-%@-icon",[featureImageArray objectAtIndex:indexPath.row]]  inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil];
    [featureImageViewButton setImageEdgeInsets:UIEdgeInsetsMake(15, 15, 15, 15)];
    [featureImageViewButton setImage:[iconImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [featureImageViewButton setTintColor: [UIColor grayColor]];
    featureImageViewButton.tag = indexPath.row;
//    [featureImageViewButton addTarget:self action:@selector(testFunc:) forControlEvents:UIControlEventTouchUpInside];
    int nextYposition=(featureImageViewButton.frame.size.height+featureImageViewButton.frame.origin.y+2);
    [cell.contentView addSubview:featureImageViewButton];
    
    // feature name Label
    featurenameLabel=[[UILabel alloc]init];
    featurenameLabel.frame=CGRectMake(0, nextYposition, cell.bounds.size.width, 18);
    featurenameLabel.text=[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:[featureNameArray objectAtIndex:indexPath.row]]];
    featurenameLabel.textAlignment=NSTextAlignmentCenter;
    
    //  Properties for label TextColor.
    NSArray *label_TextColor;
    if (NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_color",@"FeatureMenu_ListT1",[NSBundle mainBundle], nil))
        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_color",@"FeatureMenu_ListT1",[NSBundle mainBundle], nil)];
    
    else if (NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_color",@"FeatureMenu_ListT1",[NSBundle mainBundle], nil))
        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_color",@"FeatureMenu_ListT1",[NSBundle mainBundle], nil)];
    
    else
        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
    
    if (label_TextColor)
        featurenameLabel.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
    
    // Properties for label Textstyle.
    NSString *textStyle;
    if(NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_style",@"FeatureMenu_ListT1",[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_style",@"FeatureMenu_ListT1",[NSBundle mainBundle], nil);
    else if(NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_style",@"FeatureMenu_ListT1",[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_style",@"FeatureMenu_ListT1",[NSBundle mainBundle], nil);
    else
        textStyle = application_default_text_style;
    
    // Properties for label Font size.
    NSString *fontSize;
    if(NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_size",@"FeatureMenu_ListT1",[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_size",@"FeatureMenu_ListT1",[NSBundle mainBundle], nil);
    else if(NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_size",@"FeatureMenu_ListT1",[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_size",@"FeatureMenu_ListT1",[NSBundle mainBundle], nil);
    else
        fontSize = application_default_text_size;
    
    if ([textStyle isEqualToString:TEXT_STYLE_0])
        featurenameLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_1])
        featurenameLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_2])
        featurenameLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
    
    else
        featurenameLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
    
    featurenameLabel.numberOfLines=3;
    [cell.contentView addSubview:featurenameLabel];
    return cell;
}

/**
 * This method is used to set add Tableview Delegate Method User selects List .
 *@param type- Declare Listview - (NextTemplate and Next template proprty file).
 *To show List Item Detailed Data etc..
 */
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    
      NSArray *selectedIconBtncolorsArray = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"feature_list_view_selected_color",@"GeneralSettings",[NSBundle mainBundle], nil)];
    
    NSString *nameSelected = [featureImageArray objectAtIndex:indexPath.row];
    NSLog(@"nameSelected: %@ ...", nameSelected);
    
    UICollectionViewCell *datasetCell =[collectionView cellForItemAtIndexPath:indexPath];
    datasetCell.backgroundColor = [UIColor colorWithRed:[[selectedIconBtncolorsArray objectAtIndex:0] floatValue] green:[[selectedIconBtncolorsArray objectAtIndex:1] floatValue] blue:[[selectedIconBtncolorsArray objectAtIndex:2] floatValue] alpha:1.0f];
    
    selectedItemIndexPath=indexPath;
    
    NSInteger row = indexPath.row;
    NSLog(@"Selected indexpath is...%ld",(long)row);

    if (self.delegate != nil)
    {
        [self.delegate gridView_didSelectRowAtIndexPath:row];
    }
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //UICollectionViewCell *datasetCell =[collectionView cellForItemAtIndexPath:indexPath];
    
//  featureImageView.backgroundColor = [UIColor colorWithRed:[[iconBtncolorsArray objectAtIndex:0] floatValue] green:[[iconBtncolorsArray objectAtIndex:1] floatValue] blue:[[iconBtncolorsArray objectAtIndex:2] floatValue] alpha:1.0f];
    UICollectionViewCell *datasetCell =[collectionView cellForItemAtIndexPath:indexPath];
    datasetCell.backgroundColor = [UIColor clearColor]; // highlight selection
//    datasetCell.backgroundColor = [UIColor redColor]; // Default color
}

-(void)testFunc:(id)sender {
    NSLog(@"target test..");
}

@end
