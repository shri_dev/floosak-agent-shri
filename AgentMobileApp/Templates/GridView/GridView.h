//
//  GridView.h
//  ConsumerClient
//
//  Created by android on 26/05/16.
//  Copyright (c) 2016 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * This method is  used for Custom delegate Method for Option Menu.
 */
@protocol GridViewDelegate <NSObject>

// GridView Required methods Declaration.
@required
//GridView Selected List item .
-(void)gridView_didSelectRowAtIndexPath:(NSInteger)indexpath;
@end

@interface GridView : UIView <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    NSArray *featureImageArray;
    NSArray *featureNameArray;
}

/**
 * declarations are used to set the UIConstraints Of Popup template1.
 * @params are - GridView list.
 * Label and CollectionView.
 */
// GridView constarints.
@property (nonatomic, assign) NSObject <GridViewDelegate> *delegate;
@property(nonatomic,strong) NSIndexPath *selectedItemIndexPath;
@property(nonatomic,strong) UICollectionView *gridView;
@property(nonatomic,strong) UILabel *featurenameLabel;
@property(nonatomic,strong) UIButton *featureImageViewButton;

@property(nonatomic,strong) NSString *propertyFileName;

/**
 * This method is used to add Grid View Method For initialization..
 */
// Method For initialization.
//- (id)initWithFrame:(CGRect)frame  withPropertyFileName:(NSString *)propertyFile withIndex:(int)tag;
- (id)initWithFrame:(CGRect)frame withSelectedIndex:(NSInteger)index fromView:(int)fromView withSelectedCategory:(NSInteger)categoryIndex andSetSubTemplates:(BOOL)subTemplates withParentIndex :(NSInteger)pIndex;


@end
