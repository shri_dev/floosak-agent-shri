//  FeaturesMenu.m
//  Consumer Client

//  Created by jagadeeshk on 15/05/15.
//  Copyright (c) 2015 Soumya. All rights reserved.

#import "FeaturesMenu.h"
#import "Constants.h"
#import "ValidationsClass.h"
#import <QuartzCore/QuartzCore.h>
#import "WebServiceConstants.h"
#import "Template.h"
#import "Localization.h"
#import "UILabel+Extension.h"

@implementation FeaturesMenu
{
    int minNumber;
}

@synthesize featuresArray,featureImagesArray,featureTemplates,featureTemplateProperties,mainScrollView,propertyFileName;

#pragma mark - Feature Menu frame initialization.
/**
 * This method is used to set implemention of feature menu.
 */
- (id)initWithFrame:(CGRect)frame withSelectedIndex:(NSInteger)index fromView:(int)fromView withSelectedCategory:(NSInteger)categoryIndex andSetSubTemplates:(BOOL)subTemplates withParentIndex :(NSInteger)pIndex
{
    
    self = [super initWithFrame:frame];
    if (self)
    {
        mainScrollView = [[UIScrollView alloc]init];
        mainScrollView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        mainScrollView.backgroundColor = [UIColor clearColor];
        mainScrollView.contentOffset =CGPointMake(0, 0);
        [mainScrollView setShowsVerticalScrollIndicator:NO];
        
        propertyFileName=@"FeatureMenu_ListT1";
        NSLog(@"PropertyFileName:%@",propertyFileName);
        
        // FeatureMenu BackGround Color.
//        NSArray *viewbackGroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"featureslist_template_back_ground_color",propertyFileName,[NSBundle mainBundle], nil)];
//        [mainScrollView setBackgroundColor:[UIColor colorWithRed:[[viewbackGroundColor objectAtIndex:0] floatValue] green:[[viewbackGroundColor objectAtIndex:1] floatValue] blue:[[viewbackGroundColor objectAtIndex:2] floatValue] alpha:1.0f]];
        [mainScrollView setBackgroundColor: [UIColor clearColor]];
        [self addSubview:mainScrollView];
        
        // feature menu Array.
        if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"0"])
        {
            featuresArray = [[Template getNextFeatureTemplateWithIndex:(int)index] objectForKey:KEY_TEMPLATES];
            featureImagesArray = [[Template getNextFeatureTemplateWithIndex:(int)index] objectForKey:IMAGES];
        }
        else
            if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"1"])
            {
                if (subTemplates) {
                    int index1 = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"topLevelNavigation"];
                    featuresArray = [[Template getNextFeatureTemplateWithIndex:index1] objectForKey:KEY_CATEGORY_FEATURES];
                    featureImagesArray = [[Template getNextFeatureTemplateWithIndex:index1] objectForKey:IMAGES];
                }
                else
                {
                    featuresArray = [[Template getNextFeatureTemplateWithIndex:(int)index] objectForKey:CATEGORIES_NAMES_STRING];
                    featureImagesArray = [[Template getNextFeatureTemplateWithIndex:(int)index] objectForKey:CATEGORIES_IMAGES];
                }
            }
        
        // TODO: Primarly developed for vertical laying scrollview, as per new design changing it to Horizontal laying. Todo is to enable both options
        float distance;
        float y_pos = 10;
        float x_pos = 0;

        if ([[UIScreen mainScreen] bounds].size.height > 480) {
            distance = 100;
            //y_pos = 20;
        }
        else {
            distance = 80;
            //y_pos = 12;
        }
        
        NSArray *iconBtncolorsArray = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"feature_list_view_default_color", @"GeneralSettings",[NSBundle mainBundle], nil)];
        
        NSArray *selectedIconBtncolorsArray = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"feature_list_view_selected_color", @"GeneralSettings",[NSBundle mainBundle], nil)];
        
        for (int i = 0; i < featuresArray.count; i++)
        {
            UIButton *featureButton = [UIButton buttonWithType:UIButtonTypeCustom];
            if (SCREEN_HEIGHT>480){
                [featureButton setFrame:CGRectMake(x_pos + 20, y_pos, 50, 50)];
            }
            else if(SCREEN_HEIGHT==480){
                [featureButton setFrame:CGRectMake(x_pos + 25, y_pos, 50, 50)];
            }
            
            // Feature Images
            if ([NSLocalizedStringFromTableInBundle(@"featurelist_template_list_item_image_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame) {
                
                NSString* imageName = [NSString stringWithFormat:@"feature-%@-icon",[featureImagesArray objectAtIndex:i]];
                UIImage* iconImage = [UIImage imageNamed:imageName];
                [featureButton setImage:[iconImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                [featureButton setImageEdgeInsets: UIEdgeInsetsMake(10, 10, 10, 10)];
                [featureButton setTintColor:[UIColor grayColor]];
            }
            
            featureButton.tag = i;
            
            if (i == index)
            {
                selectedIconBtncolorsArray = [ValidationsClass colorWithHexString:@"#fbad01"];
                featureButton.backgroundColor = [UIColor colorWithRed:[[selectedIconBtncolorsArray objectAtIndex:0] floatValue] green:[[selectedIconBtncolorsArray objectAtIndex:1] floatValue] blue:[[selectedIconBtncolorsArray objectAtIndex:2] floatValue] alpha:1];
            }
            else
            {
                //featureButton.backgroundColor = [UIColor colorWithRed:[[iconBtncolorsArray objectAtIndex:0] floatValue] green:[[iconBtncolorsArray objectAtIndex:1] floatValue] blue:[[iconBtncolorsArray objectAtIndex:2] floatValue] alpha:1.0f];
                featureButton.backgroundColor = [UIColor clearColor];
            }
            
            [featureButton addTarget:self action:@selector(sideButn_Action:) forControlEvents:UIControlEventTouchUpInside];
            featureButton.layer.borderWidth = 1.0;
            featureButton.layer.cornerRadius = featureButton.bounds.size.height/2;
            featureButton.layer.borderColor = [[UIColor colorWithRed:[[selectedIconBtncolorsArray objectAtIndex:0] floatValue] green:[[selectedIconBtncolorsArray objectAtIndex:1] floatValue] blue:[[selectedIconBtncolorsArray objectAtIndex:2] floatValue] alpha:1.0f] CGColor];
            featureButton.clipsToBounds = YES;
            [mainScrollView addSubview:featureButton];
            
            // Title Label
            UILabel *featureLabel;
            if ([NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                featureLabel =[[UILabel alloc] init];
                
                if (SCREEN_HEIGHT>480) {
                    //featureLabel.frame=CGRectMake(0, featureButton.frame.origin.y+featureButton.frame.size.height, self.frame.size.width,40);
                    featureLabel.frame=CGRectMake(distance*i, featureButton.frame.origin.y+featureButton.frame.size.height, distance, 40);
                }
                else if(SCREEN_HEIGHT==480) {
                    //featureLabel.frame=CGRectMake(0, featureButton.frame.origin.y+featureButton.frame.size.height, self.frame.size.width,40);
                    featureLabel.frame=CGRectMake(distance*i, featureButton.frame.origin.y+featureButton.frame.size.height, distance, 40);
                }
                featureLabel.text = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:[featuresArray objectAtIndex:i]]];
                
                featureLabel.numberOfLines = 3;
                featureLabel.textAlignment = NSTextAlignmentCenter;
                [featureLabel alignToTop];
                
                if([NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_font_override", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                {
                    //  Properties for label TextColor.
                    NSArray *label_TextColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                    
                    if (label_TextColor)
                        featureLabel.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    // Properties for label Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    // Properties for label Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        featureLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        featureLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        featureLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        featureLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                }
                else{
                    //Default Properties for label textColor
                    NSArray *label_TextColor ;
                    if (NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    else
                        label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                    if (label_TextColor)
                        featureLabel.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                    
                    //Default Properties for label textStyle
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_text_style;
                    
                    //Default Properties for label fontSize
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_text_size;
                    
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        featureLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        featureLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                    
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        featureLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                    
                    else
                        featureLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                    
                }
                featureLabel.tag = i;
                [mainScrollView addSubview:featureLabel];
                if([[featuresArray objectAtIndex:i] isEqualToString:@"label_id_depositemoney"]){
                    CGRect rect = featureLabel.frame;
                    featureLabel.frame = CGRectMake(rect.origin.x, rect.origin.y-10, rect.size.width, rect.size.height);
                }
            }
            
            /*
            UILabel *featureBgLabel =[[UILabel alloc] init];
            
            if (SCREEN_HEIGHT>480) {
                featureBgLabel.frame=CGRectMake(10, featureLabel.frame.size.height+featureLabel.frame.origin.y, 60, 1);
            }
            else if(SCREEN_HEIGHT==480) {
                featureBgLabel.frame=CGRectMake(10, featureLabel.frame.size.height+featureLabel.frame.origin.y, 60, 1);
            }
            
            NSArray *featureBgcolorsArray = [ValidationsClass colorWithHexString:FEATURELIST_BORDER_COLOR];
            
            if (featureBgcolorsArray)
                featureBgLabel.backgroundColor = [UIColor colorWithRed:[[featureBgcolorsArray objectAtIndex:0] floatValue] green:[[featureBgcolorsArray objectAtIndex:1] floatValue] blue:[[featureBgcolorsArray objectAtIndex:2] floatValue] alpha:1.0f];
            
            [mainScrollView addSubview:featureBgLabel];
            */
            
            //y_pos =  featureBgLabel.frame.origin.y + featureBgLabel.frame.size.height+5;
            x_pos += distance;
            
            mainScrollView.contentSize = CGSizeMake(x_pos, 80);//CGSizeMake(self.frame.size.width, y_pos);
        }
    }
    return self;
}


#pragma mark - SET DELEGETE METHOD.

/**
 * This method is  used for Custom delegate Method for Feature Menu.
 * @param type -Based on Profile Features will Be shown.
 * Method is used for get user selected feature.
 */

-(void)sideButn_Action:(UIButton *)button
{
    NSInteger tag = button.tag;
    if (self.delegate != nil)
    {
        [self.delegate featuresMenuButtonAction:tag];
    }
}

-(void)scrollToPosition:(int)tag{
    NSArray *arr = mainScrollView.subviews;
    NSMutableArray *arr2 = [[NSMutableArray alloc] init];
    for (UIView *view in arr){
        if([view isKindOfClass:[UIButton class]]){
            [arr2 addObject:view];
        }
    }
    if(tag == 6){
        NSLog(@"Scrolling to position %d", tag);
        UIButton *button = (UIButton*)[arr2 objectAtIndex:tag-1];
        [mainScrollView scrollRectToVisible:CGRectMake(button.frame.origin.x+50, button.frame.origin.y, button.frame.size.width, button.frame.size.height) animated:NO];
    }else{
        NSLog(@"Scrolling to position %d", tag);
        UIButton *button = (UIButton*)[arr2 objectAtIndex:tag];
        [mainScrollView scrollRectToVisible:button.frame animated:NO];
    }
    return;
}


@end
