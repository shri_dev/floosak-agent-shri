//
//  FeaturesMenu.h
//  Consumer Client
//
//  Created by jagadeeshk on 15/05/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FeaturesMenuDelegate <NSObject>
/**
 * This method is  used for Custom delegate Method for Feature Menu.
 */
@required
-(void)featuresMenuButtonAction:(int)tag;
@end
/**
 * This class used to handle functionality and view of Feature menu.
 *
 * @author Integra
 *
 */
@interface FeaturesMenu : UIView

/**
 * declarations are used to set the UIConstraints Of Feature menu.
 * Label  and TableView.
 */
@property(nonatomic,strong)  UIScrollView *mainScrollView;
@property(nonatomic, strong) NSArray *featuresArray;
@property(nonatomic, strong) NSArray *featureImagesArray;
@property(nonatomic, strong) NSArray *featureTemplates;
@property(nonatomic, strong) NSArray *featureTemplateProperties;
@property(nonatomic,strong)  NSString *propertyFileName;

@property (nonatomic, assign) NSObject <FeaturesMenuDelegate> *delegate;
/**
 * This method is  used for Method Initialization of Feature Menu.
 */
- (id)initWithFrame:(CGRect)frame withSelectedIndex:(NSInteger)index fromView:(int)fromView withSelectedCategory:(NSInteger)categoryIndex andSetSubTemplates:(BOOL)subTemplates withParentIndex :(NSInteger)pIndex;

-(void)scrollToPosition:(int)tag;

@end
