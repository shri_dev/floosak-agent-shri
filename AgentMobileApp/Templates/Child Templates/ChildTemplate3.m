//
//  ChildTemplate3.m
//  Consumer Client
//
//  Created by jagadeeshk on 11/05/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "ChildTemplate3.h"
#import "Constants.h"
#import "Localization.h"
#import "ValidationsClass.h"
#import "DatabaseConstatants.h"
#import "Template.h"

@interface ChildTemplate3 ()
@end

@implementation ChildTemplate3

@synthesize dropdownList_ItemsArray;
@synthesize dropDownData_TableView;
@synthesize dropdown_NameLabel,valueLabel,content_ImageView,bglabel;
@synthesize main_searchBar;
@synthesize filteredArray,isFiltered;

#pragma mark - ChildTemplate3 UIView.
/**
 * This method is used to set implemention of childTemplate3.
 */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView
{
   
    NSLog(@"PropertyFileName:%@",propertyFile);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        typeString = type;
        propertyFileName = propertyFile;
        dropdownList_ItemsArray = dataDictsArray;
    }
    return self;
}

#pragma mark - ViewController lifecycle.
/**
 * This method is used to set add UIconstraints Frame of ChildTemplate3.
 @param Type- Tableview
 * Set label Text (Size,color and font size).
 */
- (void)viewDidLoad {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"refreshView"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor whiteColor]];
  
    nextY_postion=0;
    labelX_position=0;
    distanceY=0;
    
    /*
     * This method is used to add Childtemplate3 PageHeader view.
     */
  // PageHeader view.
    pageHeader=[[PageHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64 ) withHeaderTitle:@"" withLeftbarBtn1Image_IconName:BACK_BUTTON_IMAGE withLeftbarBtn2Image_IconName:nil withHeaderButtonImage:HEADER_IMAGE_NAME withRightbarBtn1Image_IconName:nil withRightbarBtn2Image_IconName:nil withRightbarBtn3Image_IconName:nil withTag:0];
    pageHeader.delegate = self;
    /*
     * This method is used to add Childtemplate3 PageHeader view frame.
     */
    [self.view addSubview:pageHeader];
    labelX_position=15;
    nextY_postion= pageHeader.frame.origin.y + pageHeader.frame.size.height;
    /*
     * This method is used to add Childtemplate3 DropDownTitle Visibility.
     */
    //Dropdown1
  if([NSLocalizedStringFromTableInBundle(@"child_template3_list_item_title_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
         {
        dropdown_NameLabel=[[UILabel alloc]init];
        dropdown_NameLabel.frame=CGRectMake(labelX_position,70, SCREEN_WIDTH-30, 40);

             /*
              * This method is used to add Childtemplate3 DropDown titlename.
              */
        NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template3_list_item_title_text",propertyFileName,[NSBundle mainBundle], nil)];
             
         if (dropDownStr)
             dropdown_NameLabel.text =dropDownStr;
             /*
              * This method is used to set Childtemplate3 DropDown title fontattributes override.
              */
        if ([NSLocalizedStringFromTableInBundle(@"child_template3_list_item_title_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            /*
             * This method is used to set Childtemplate3 DropDown title Properties for label TextColor.
             */
            // Properties for label TextColor.
            
            NSArray *dropdownNameLabelColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template3_list_item_title_text_color",propertyFileName,[NSBundle mainBundle], nil))
                dropdownNameLabelColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template3_list_item_title_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"child_template3_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                dropdownNameLabelColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template3_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                dropdownNameLabelColor=[ValidationsClass colorWithHexString:application_default_text_color];
            
            /*
             * This method is used to set Childtemplate3 DropDown title Properties for label Textstyle.
             */
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template3_list_item_title_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template3_list_item_title_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template3_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template3_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            /*
             * This method is used to set Childtemplate3 DropDown title Properties for label Fontsize.
             */
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template3_list_item_title_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template3_list_item_title_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template3_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template3_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_1])
                dropdown_NameLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            else
                dropdown_NameLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else{
            /*
             * This method is used to set Childtemplate3 DropDown title Default Properties for label textcolor
             */
            //Default Properties for label textcolor
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template3_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template3_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                dropdown_NameLabel.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            /*
             * This method is used to set Childtemplate3 DropDown title Default Properties for label textStyle.
             */
            //Default Properties for label textStyle
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template3_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template3_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            /*
             * This method is used to set Childtemplate3 DropDown title Default Properties for label fontSize.
             */
            //Default Properties for label fontSize
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template3_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template3_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_1])
                dropdown_NameLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            else
                dropdown_NameLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
             /*
              * This method is used to add Childtemplate3 DropDown title label Frame.
              */
             nextY_postion =  dropdown_NameLabel.frame.origin.y+dropdown_NameLabel.frame.size.height;
             [self.view addSubview:dropdown_NameLabel];
    }
    /*
     * This method is used to add Childtemplate3 value label  Visibility.
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template3_value1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        main_searchBar = [[UISearchBar alloc] init];
        main_searchBar.frame = CGRectMake(0,nextY_postion,SCREEN_WIDTH,45);
        /*
         * This method is used to add Childtemplate3 value label  fontattributes override.
         */
        if([NSLocalizedStringFromTableInBundle(@"child_template3_value1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            /*
             * This method is used to add Childtemplate3  Properties for searchBar TextColor.
             */
            // Properties for textField TextColor.
            NSArray *textField1TextColor;
            if (NSLocalizedStringFromTableInBundle(@"child_template3_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                textField1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template3_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"child_template3_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                textField1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template3_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                textField1TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
            
            if (textField1TextColor)
                main_searchBar.tintColor = [UIColor colorWithRed:[[textField1TextColor objectAtIndex:0] floatValue] green:[[textField1TextColor objectAtIndex:1] floatValue] blue:[[textField1TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            /*
             * This method is used to add Childtemplate3  Properties for searchbar TextColor.
             */
            // Properties for textField Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template3_value1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"child_template3_value1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template3_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"child_template3_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_value_text_style;
            /*
             * This method is used to add Childtemplate3  Properties for searchbar font size.
             */
            
         // Ref   http://stackoverflow.com/questions/4697689/change-the-font-size-of-uisearchbar
            
            // Properties for textField font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template3_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template3_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template3_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize =NSLocalizedStringFromTableInBundle(@"child_template3_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
          
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                [[UITextField appearanceWhenContainedIn:[main_searchBar class], nil] setDefaultTextAttributes:@{
                 NSFontAttributeName: [UIFont systemFontOfSize:[fontSize floatValue]],}];
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                [[UITextField appearanceWhenContainedIn:[main_searchBar class], nil] setDefaultTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:[fontSize floatValue]],}];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                [[UITextField appearanceWhenContainedIn:[main_searchBar class], nil] setDefaultTextAttributes:@{NSFontAttributeName: [UIFont italicSystemFontOfSize:[fontSize floatValue]],}];
            else
                [[UITextField appearanceWhenContainedIn:[main_searchBar class], nil] setDefaultTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:[fontSize floatValue]],}];
        }
        else
        {
            /*
             * This method is used to add Childtemplate3 default Properties for searchbar TextColor.
             */
            //Default Properties for textfiled textcolor
            
            NSArray *textField1TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template3_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                textField1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template3_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                textField1TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
            if (textField1TextColor)
                main_searchBar.tintColor = [UIColor colorWithRed:[[textField1TextColor objectAtIndex:0] floatValue] green:[[textField1TextColor objectAtIndex:1] floatValue] blue:[[textField1TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            /*
             * This method is used to add Childtemplate3 default Properties for searchbar Textstyle.
             */
            //Default Properties for textfiled textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template3_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"child_template3_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_text_style;
            
            /*
             * This method is used to add Childtemplate3  Deafault Properties for searchbar font size.
             */
            // Deafault Properties for textField font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template3_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template3_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                [[UITextField appearanceWhenContainedIn:[main_searchBar class], nil] setDefaultTextAttributes:@{
                  NSFontAttributeName: [UIFont systemFontOfSize:[fontSize floatValue]],}];
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                [[UITextField appearanceWhenContainedIn:[main_searchBar class], nil] setDefaultTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:[fontSize floatValue]],}];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                [[UITextField appearanceWhenContainedIn:[main_searchBar class], nil] setDefaultTextAttributes:@{NSFontAttributeName: [UIFont italicSystemFontOfSize:[fontSize floatValue]],}];
            else
                [[UITextField appearanceWhenContainedIn:[main_searchBar class], nil] setDefaultTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:[fontSize floatValue]],}];
        }
        /*
         * This method is used to add Childtemplate3  Deafault Properties for searchbar KeyBoardType.
         */
        // property for KeyBoardType
        NSString *keyboardType;
        if (NSLocalizedStringFromTableInBundle(@"child_template3_value1_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
            keyboardType = NSLocalizedStringFromTableInBundle(@"child_template3_value1_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
        else
            keyboardType = application_default_value_keyboard_type;
        /*
         * This method is used to add Childtemplate3  Properties for searchbar KeyBoardType.
         */
        if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
            main_searchBar.keyboardType = UIKeyboardTypeDefault;
        else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
            main_searchBar.keyboardType = UIKeyboardTypeDecimalPad;
        else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
            main_searchBar.keyboardType = UIKeyboardTypePhonePad;
        else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3])
            main_searchBar.keyboardType = UIKeyboardTypeNumberPad;
        else
            main_searchBar.keyboardType = UIKeyboardTypeDefault;
        
        main_searchBar.delegate = self;
        [main_searchBar setTag:1];
        [self.view addSubview:main_searchBar];
        
        nextY_postion = main_searchBar.frame.origin.y+main_searchBar.frame.size.height;
    }
    /*
     * This method is used to add Childtemplate3  tableview.
     */
    dropDownData_TableView=[[UITableView alloc]init];
    dropDownData_TableView.frame=CGRectMake(labelX_position, nextY_postion, SCREEN_WIDTH-20, SCREEN_HEIGHT-(nextY_postion+20));
    dropDownData_TableView.delegate=self;
    dropDownData_TableView.dataSource=self;
    dropDownData_TableView.backgroundColor = [UIColor clearColor];

    [self.view addSubview:dropDownData_TableView];
}


#pragma mark - Searchbar Delegate method.
/*
 * This method is used to add Childtemplate3  tableview with searchbar delegate methods.
 */
-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        isFiltered = FALSE;
        [main_searchBar resignFirstResponder];
        [main_searchBar performSelector: @selector(resignFirstResponder)
                             withObject: nil
                             afterDelay: 0.1];
    }
    else
    {
        isFiltered = TRUE;
        filteredArray = [[NSMutableArray alloc] init];
        
        for (NSDictionary* local_Dictionary in dropdownList_ItemsArray)
        {
            NSRange nameRange = [[local_Dictionary objectForKey:DROP_DOWN_TYPE_DESC] rangeOfString:text options:NSCaseInsensitiveSearch];
            if(nameRange.length >0 )
            {
                [filteredArray addObject:local_Dictionary];
            }
        }
    }
    
    dropDownData_TableView.delegate = self;
    dropDownData_TableView.dataSource = self;
    [dropDownData_TableView reloadData];
}

- (void)hideKeyboardWithSearchBar:(UISearchBar *)searchBar{
    [main_searchBar resignFirstResponder];
}

#pragma mark  - TABLEVIEW DATA SOURCE AND DELEGATE METHODS
/**
 * This method is used to set add Tableview (list and delegate methods) of ChildTemplate3.
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isFiltered) {
        return [filteredArray count];
    }
    else
    return [dropdownList_ItemsArray count];
}
/**
 * This method is used to set add Tableview for ChildTemplate3.
 *@param type- Declare Listview related UI(Labels,Images etc..base on Req).
 */
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = [NSString stringWithFormat:@"%@%d",@"Cell",(int)indexPath.row];
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
        nextY_postion=10.0;
        labelX_position=5.0;
    /*
     * This method is used to add Childtemplate3  tableview cell Image visibility.
     */
        if ([NSLocalizedStringFromTableInBundle(@"child_template3_list_item_image_visibility", propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            content_ImageView=[[UIImageView alloc]init];
            content_ImageView.frame=CGRectMake(labelX_position, nextY_postion, 40, 40);
            content_ImageView.image=[UIImage imageNamed:@"profilePic.jpg"];
            [cell.contentView addSubview:content_ImageView];
        }
        
        labelX_position=content_ImageView.frame.origin.x+content_ImageView.frame.size.width+10;
        nextY_postion=7.0;
    /*
     * This method is used to add Childtemplate3  tableview cell listitem visibility.
     */
      if (([NSLocalizedStringFromTableInBundle(@"child_template3_list_item_value_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)) {
          
          valueLabel=[[UILabel alloc]init];
          valueLabel.frame = CGRectMake(labelX_position, nextY_postion, dropDownData_TableView.frame.size.width-labelX_position-10, 30);
          valueLabel.backgroundColor = [UIColor clearColor];
          NSString *value;

          if (isFiltered) {
              value = [[filteredArray objectAtIndex:indexPath.row] objectForKey:DROP_DOWN_TYPE_DESC];
              if (value.length == 0 ) {
                  value = [[filteredArray objectAtIndex:indexPath.row] objectForKey:DROP_DOWN_TYPE_NAME];
                  if (value.length == 0 ) {
                      value =  [[filteredArray objectAtIndex:indexPath.row] objectForKey:@"value"];
                      if (value.length == 0 ) {
                          value = [[filteredArray objectAtIndex:indexPath.row] objectForKey:@"id"];
                      }
                  }
              }
          }
          else
          {
              value = [[dropdownList_ItemsArray objectAtIndex:indexPath.row] objectForKey:DROP_DOWN_TYPE_DESC];
              if (value.length == 0 ) {
                  value = [[dropdownList_ItemsArray objectAtIndex:indexPath.row] objectForKey:DROP_DOWN_TYPE_NAME];
                  if (value.length == 0 ) {
                      value =  [[dropdownList_ItemsArray objectAtIndex:indexPath.row] objectForKey:@"value"];
                      if (value.length == 0 ) {
                          value = [[dropdownList_ItemsArray objectAtIndex:indexPath.row] objectForKey:@"id"];
                      }
                  }
              }
          }
          valueLabel.text = value;
          /*
           * This method is used to add Childtemplate3  tableview cell listitem fontattributes override.
           */
            if ([NSLocalizedStringFromTableInBundle(@"child_template3_list_item_value_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                /*
                 * This method is used to add Childtemplate3  tableview cell listitem Properties for label TextColor.
                 */
                // Properties for label TextColor.
                
                NSArray *valueLabelColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template3_list_item_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    valueLabelColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template3_list_item_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"child_template3_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    valueLabelColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template3_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    valueLabelColor=[ValidationsClass colorWithHexString:application_default_text_color];
                
                /*
                 * This method is used to add Childtemplate3  tableview cell listitem Properties for label Textstyle.
                 */
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template3_list_item_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template3_list_item_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template3_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template3_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                /*
                 * This method is used to add Childtemplate3  tableview cell listitem Properties for label fontsize.
                 */
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template3_list_item_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template3_list_item_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template3_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template3_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                  valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                     valueLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                     valueLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    valueLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            else{
                /*
                 * This method is used to add Childtemplate3  tableview cell listitem Default Properties for label textcolor.
                 */
                //Default Properties for label textcolor
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template3_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template3_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    valueLabel.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                /*
                 * This method is used to add Childtemplate3  tableview cell listitem Default Properties for label textstyle.
                 */
                //Default Properties for label textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template3_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template3_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                /*
                 * This method is used to add Childtemplate3  tableview cell listitem Default Properties for label fontsize.
                 */
                //Default Properties for label fontSize
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template3_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template3_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    valueLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    valueLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    valueLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [cell.contentView addSubview:valueLabel];
        }
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor=[UIColor colorWithRed:(212 / 255.0) green:(212 / 255.0) blue:(212 / 255.0) alpha:1.0];
    [cell setSelectedBackgroundView:bgColorView];

    
    bglabel=[[UILabel alloc]init];
    bglabel.frame=CGRectMake(0, 39, dropDownData_TableView.frame.size.width, 1);
    bglabel.backgroundColor=[UIColor blackColor];
    [cell.contentView addSubview:bglabel];
    
    tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    return cell;
}
/**
 * This method is used to set add Tableview Delegate Method User selects List .
 *@param type - Declare Listview - (NextTemplate and Next template proprty file).
 *To show List Item Detailed Data etc..
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Page header view
    selectedValueInteger = (int)indexPath.row;
    selectedCount = 0;
    
    selectedCount =(int) [[tableView indexPathsForSelectedRows] count];
    
    NSMutableDictionary *dataDict;
    if (isFiltered) {
        dataDict = [[NSMutableDictionary alloc] initWithDictionary:[filteredArray objectAtIndex:selectedValueInteger]];
    }
    else
    {
        dataDict = [[NSMutableDictionary alloc] initWithDictionary:[dropdownList_ItemsArray objectAtIndex:selectedValueInteger]];
    }
    /*
     * This method is used to add Childtemplate3  tableview selected dropdown value returned.
     */
    [dataDict setObject:[NSString stringWithFormat:@"%d",selectedValueInteger] forKey:@"selectedPosition"];
    [_basetarget performSelectorOnMainThread:_baseSelector withObject:dataDict waitUntilDone:YES];
    NSLog(@"Views in hierarchy: %lu", (unsigned long)[self.navigationController.viewControllers count]);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"IDTypeSelected" object:self userInfo:dataDict];

    [self.navigationController popViewControllerAnimated:NO];
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self deleteBtn_Action];
}
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[tableView indexPathForSelectedRow] isEqual:indexPath]) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self deleteBtn_Action];
        return nil;
    }
    return indexPath;
}

-(void)viewDidLayoutSubviews
{
    if ([dropDownData_TableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [dropDownData_TableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([dropDownData_TableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [dropDownData_TableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark -  MENU BUTTON ACTION
/**
 * This method is used to set Delegate Method of PageHeaderView Button Action(Pop to Previous view).
 */
-(void)menuBtn_Action
{
    [self.navigationController popViewControllerAnimated:NO];
}


#pragma mark -  RIGHT MENU  BUTTON ACTION

-(void)checkMarkBtn_Action
{
    // Page Header button Optional Action Method
}


-(void)editBtn_Action
{
        // Page Header button Optional Action Method
}
-(void)infoBtn_Action
{
    
        // Page Header button Optional Action Method
}
/*
 * This method is used to add Childtemplate3  Pageheaderview delete button action method.
 */
-(void)deleteBtn_Action
{
    [dropDownData_TableView deselectRowAtIndexPath:
    [dropDownData_TableView indexPathForSelectedRow] animated:NO];
    [pageHeader1  removeFromSuperview];
}


#pragma mark - Default memory warning method.
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
