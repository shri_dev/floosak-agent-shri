//
//  ChildTemplate2.h
//  Consumer Client
//
//  Created by Integra Micro on 19/04/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatabaseManager.h"
#import "PopUpTemplate1.h"
#import "BaseViewController.h"
#import "ActivityIndicator.h"
#import "BaseView.h"

@protocol ChildTempalte2Delegate <NSObject>
@required

-(void)childTempalte2SelectionAction:(NSDictionary *)dataDictionary withTempalte:(NSString *)template withPropertyFile:(NSString *)propertyFile;
@end
/**
 * This class used to handle functionality and View of Childtemplate2
 *
 * @author Integra
 *
 */
@interface ChildTemplate2 : BaseView<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UIGestureRecognizerDelegate>
{
    BOOL hasSelectionOperation;
    
    int nextY_position;
    int height_Position;
    
    NSMutableArray *dataArray;
    NSString *local_processorCode;
    
    DatabaseManager *dataManager;
    NSString *user_Language;
    NSString *userSelectedLanguage;
    int rowIndex;
    NSIndexPath *selectedIndex;
    int selected;
    NSString *nextTemplatePropertyFile;
    UITableViewCell *local_cell;
    NSString *selected_language;
    
    NSIndexPath *startIndexPath;
    NSIndexPath *previousSelectedIndex;
    NSMutableDictionary *dataDict;
    NSArray *dictArray;
    
    NSInteger currentlySelectedRow;
    BOOL areaCodesRequired;

}
/**
 * declarations are used to set the UIConstraints Of ChildTemplate2.
 * Label,Value label,Border label,Dropdown and Buttons.
 */
@property(nonatomic,strong) UITableView *childTemplate2_TableView;
@property(nonatomic,strong) UILabel *alertLabel;
@property(nonatomic,strong) UILabel *usernameLbl;
@property(nonatomic,strong) UILabel *borderLabel;
@property(nonatomic,strong) NSMutableArray *array1;
@property (nonatomic, assign) NSObject <ChildTempalte2Delegate> *delegate;
@property(nonatomic,strong) NSString *propertyFileName;
@property(nonatomic,strong) UIButton *childButton;
@property(nonatomic,strong) UILabel *keyLabel;
@property(nonatomic,strong) UILabel *valueLabel;
@property(nonatomic,strong) UISearchBar *main_searchBar;
@property(nonatomic,strong) NSMutableArray *filteredArray;
@property (nonatomic, assign) BOOL isFiltered;
/**
 * This method is  used for Method Initialization of ChildTemplate2.
 */
// Method For initialization.
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView;
/*
 * This method is used to add Childtemplate2 Delegate method.
 */
-(void)setLocalDelegate:(id)fromDelegate;


@end
