//
//  ChildTemplate13.m
//  Consumer Client
//
//  Created by test on 21/10/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "ChildTemplate13.h"
#import "Constants.h"
#import "ValidationsClass.h"
#import "Template.h"
#import "ParentTemplate6.h"
#import "PopUpTemplate6.h"
#import "AppDelegate.h"
#import "Localization.h"

@implementation ChildTemplate13
{
    NSArray *tempArray;
}

@synthesize propertyFileName,categoryTableView;
@synthesize headerTitle_Label;
@synthesize headerborder_Label;

#pragma mark - ChildTemplate13 UIView.
/**
 * This method is used to set implemention of childTemplate13.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView
{
    
    NSLog(@"PropertyFileName:%@",propertyFile);
    self = [self initWithFrame:frame];
    if (self)
    {
        tagVal = view;
        superId = superView;
        templateDictionary = [Template getNextFeatureTemplateWithIndex:view];
        templateDictionary = [Template getNextFeatureTemplateWithIndex:view];
        propertyFileName=propertyFile;
        [self addControlsForView];
    }
    
    return self;
}


#pragma mark - ChildTemplate13 UIConstraints Creation.
/**
 * This method is used to set add UIconstraints Frame of ChildTemplate13.
 @param Type- Label and Tableview
 * Set label Text (Size,color and font size).
 */
-(void)addControlsForView{
    int labelX_Position = 0.0;
    int labelY_Position=0.0;
    
    // header Lavbel visibility.
    if ([NSLocalizedStringFromTableInBundle(@"child_template13_header_text_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        headerTitle_Label=[[UILabel alloc]init];
        headerTitle_Label.frame=CGRectMake(labelX_Position,labelY_Position, SCREEN_WIDTH-(labelX_Position*2)-96, 60);
        
        NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template13_header_text",propertyFileName,[NSBundle mainBundle], nil)];
        if (headerStr)
            headerTitle_Label.text=headerStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template13_header_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelTextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template13_header_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template13_header_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template13_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template13_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelTextColor)
                headerTitle_Label.textColor = [UIColor colorWithRed:[[labelTextColor objectAtIndex:0] floatValue] green:[[labelTextColor objectAtIndex:1] floatValue] blue:[[labelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template13_header_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template13_header_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template13_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template13_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template13_header_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template13_header_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template13_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template13_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerTitle_Label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerTitle_Label.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerTitle_Label.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerTitle_Label.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *valuelabelTextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template13_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                valuelabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template13_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                valuelabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (valuelabelTextColor)
                headerTitle_Label.textColor = [UIColor colorWithRed:[[valuelabelTextColor objectAtIndex:0] floatValue] green:[[valuelabelTextColor objectAtIndex:1] floatValue] blue:[[valuelabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template13_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template13_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template13_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template13_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerTitle_Label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerTitle_Label.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerTitle_Label.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerTitle_Label.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        headerTitle_Label.textAlignment=NSTextAlignmentCenter;
        [self addSubview:headerTitle_Label];
        
        //Border label
        headerborder_Label=[[UILabel alloc]init];
        headerborder_Label.frame=CGRectMake(labelX_Position, headerTitle_Label.frame.size.height-1, headerTitle_Label.frame.size.width, 1);
        headerborder_Label.backgroundColor=[UIColor blackColor];
        [self addSubview:headerborder_Label];
    }
    
    labelY_Position = labelY_Position+headerTitle_Label.frame.size.height+headerTitle_Label.frame.origin.y;
    tempArray = [templateDictionary objectForKey:KEY_CATEGORY_FEATURES];
    
    // TableView
    categoryTableView=[[UITableView alloc]init];
    categoryTableView.delegate=self;
    categoryTableView.dataSource=self;
    categoryTableView.frame=CGRectMake(0,labelY_Position, SCREEN_WIDTH-95 , SCREEN_HEIGHT-64);
    categoryTableView.backgroundColor=[UIColor clearColor];
    [categoryTableView setTableFooterView:[UIView new]];
    [self addSubview:categoryTableView];
}

#pragma mark - TableView dataSource And Delegate method.
/**
 * This method is used to set add Tableview (list and delegate methods) of ChildTemplate13.
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tempArray.count;
}

/**
 * This method is used to set add Tableview for ChildTemplate13.
 *@param type- Declare Listview related UI(Labels,Images etc..base on Req).
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = [NSString stringWithFormat:@"%@%d",@"Cell",(int)indexPath.row];
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    int nextY_postion=15.0;
    int labelX_position=5.0;
    

    if (([NSLocalizedStringFromTableInBundle(@"child_template13_list_item_text_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)) {
        UILabel *valueLabel=[[UILabel alloc]init];
        valueLabel.frame = CGRectMake(labelX_position, nextY_postion, categoryTableView.frame.size.width-labelX_position-10, 30);
        valueLabel.backgroundColor = [UIColor clearColor];
        valueLabel.text=[Localization languageSelectedStringForKey:[[templateDictionary objectForKey:KEY_CATEGORY_FEATURES] objectAtIndex:indexPath.row]];
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template13_list_item_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *valueLabelColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template13_list_item_text_color",propertyFileName,[NSBundle mainBundle], nil))
                valueLabelColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template13_list_item_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"child_template13_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                valueLabelColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template13_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                valueLabelColor=[ValidationsClass colorWithHexString:application_default_text_color];
            
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template13_list_item_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template13_list_item_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template13_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template13_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template13_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template13_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template13_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template13_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                valueLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                valueLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                valueLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else{
            
            //Default Properties for label textcolor
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template13_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template13_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                valueLabel.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template13_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template13_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template13_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template13_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                valueLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                valueLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                valueLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        valueLabel.numberOfLines=0;
        [valueLabel sizeToFit];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell.contentView addSubview:valueLabel];
    }
    
    return cell;
}
/**
 * This method is used to set add Tableview Delegate Method User selects List .
 *@param type- Declare Listview - (NextTemplate and Next template proprty file).
 *To show List Item Detailed Data etc..
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int index = (int)indexPath.row;
    
    NSString *nextTemplate = [[templateDictionary objectForKey:KEY_CATEGORY_TEMPLATES] objectAtIndex:indexPath.row];
    NSLog(@"Template Value : %@",nextTemplate);

    if (![nextTemplate isEqualToString:@""])
    {
        Class myclass = NSClassFromString(nextTemplate);
        id obj = nil;
        
        if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
        {
            //WithFromView is for which row is clicked
            obj = [[myclass alloc] initWithNibName:nextTemplate bundle:nil withSelectedIndex:(index+1) fromView:0 withFromView:[NSString stringWithFormat:@"%d",(int)(indexPath.row+1)] withPropertyFile:[[templateDictionary objectForKey:KEY_CATEGORY_TEMPLATE_PROPERTY_FILES] objectAtIndex:index] withProcessorCode:nil dataArray:nil dataDictionary:nil];
            AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            UINavigationController *navController = (UINavigationController *)delegate.window.rootViewController;
            [self removeFromSuperview];
            [navController pushViewController:obj animated:NO];
        }
        else if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
        {
            obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:[[templateDictionary objectForKey:KEY_CATEGORY_TEMPLATE_PROPERTY_FILES] objectAtIndex:index] andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:(index+1) withDataArray:nil withSubIndex:index];
            
            [(UIView *)superId addSubview:(UIView *)obj];
            ((UIView *)obj).center = ((UIView *)superId).center;
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end

