//
//  ChildTemplate1.h
//  Consumer Client
//
//  Created by Integra Micro on 17/04/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityIndicator.h"
#import <AddressBookUI/AddressBookUI.h>
#import "BaseView.h"
#import "CustomTextField.h"
/**
 * This class used to handle functionality and View of ChildTemplate1.
 *
 * @author Integra
 *
 */
@interface ChildTemplate1 : BaseView<UITextFieldDelegate,ABPeoplePickerNavigationControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate>
{
    int label_Y_Position;
    int label_X_Position;
    int distance_Y;
    int filed_Y_Position;
    int filed_X_Position;
    int next_Y_Position;
    
    UIImageView *userProfileImageView;
    NSString *dropdownString;
    UIButton *btn;
    NSArray *dropDownDataArray;
    CustomTextField *ifscCode;
    CustomTextField *bank;
    NSString *presentingViewCode;
    BOOL passData;
    /*
     *Declare a toolbar for pickerview
     */
    UIToolbar *numberToolbar;
    CustomTextField *activeField;
    int numberOfFields;

    //Added by Shri..
    bool isImage1Obtained;
    bool isImage2Obtained;
    bool isSignedRegistrationFormObtained;
    NSDictionary *selectedIDTypeDict;
    int isDebugging;

    int currentElement;
    
    NSMutableDictionary *governoratesDict;
    NSArray *governorates;
    NSArray *districts;
    
    NSInteger selectedType;
    NSString *docType;

}
/**
 * declarations are used to set the UIConstraints Of ParentTemplate4.
 * Label,Value label,Border label,Dropdown and Buttons.
 */
@property(nonatomic,strong) UIScrollView *childTemplate1ScrollView;
@property(nonatomic,strong) UIButton *button;
@property(nonatomic,strong) UILabel *borderLabel;

@property(nonatomic,strong) UILabel *key_label1;
@property(nonatomic,strong) UILabel *value_label1;
@property(nonatomic,strong) UILabel *value_border_label1;
/*
 * This method is used to set ChildTemplate1 UIConstraints labels,textfields and DropDown.
 */
@property(nonatomic,strong) UILabel *textfieldTitle_Label1;
@property(nonatomic,strong) CustomTextField *textfield1;
@property(nonatomic,strong) UIButton *dropDownButton1;

@property(nonatomic,strong) UILabel *textfieldTitle_Label2;
@property(nonatomic,strong) CustomTextField *textfield2;
@property(nonatomic,strong) UIButton *dropDownButton2;

@property(nonatomic,strong) UILabel *textfieldTitle_Label3;
@property(nonatomic,strong) CustomTextField *textfield3;
@property(nonatomic,strong) UIButton *dropDownButton3;

@property(nonatomic,strong) UILabel *textfieldTitle_Label4;
@property(nonatomic,strong) CustomTextField *textfield4;
@property(nonatomic,strong) UIButton *dropDownButton4;

@property(nonatomic,strong) UILabel *textfieldTitle_Label5;
@property(nonatomic,strong) CustomTextField *textfield5;
@property(nonatomic,strong) UIButton *dropDownButton5;

@property(nonatomic,strong) UILabel *textfieldTitle_Label6;
@property(nonatomic,strong) CustomTextField *textfield6;
@property(nonatomic,strong) UIButton *dropDownButton6;

@property(nonatomic,strong) UILabel *textfieldTitle_Label7;
@property(nonatomic,strong) CustomTextField *textfield7;
@property(nonatomic,strong) UIButton *dropDownButton7;

@property(nonatomic,strong) UILabel *textfieldTitle_Label8;
@property(nonatomic,strong) CustomTextField *textfield8;
@property(nonatomic,strong) UIButton *dropDownButton8;

@property(nonatomic,strong) UILabel *textfieldTitle_Label9;
@property(nonatomic,strong) CustomTextField *textfield9;
@property(nonatomic,strong) UIButton *dropDownButton9;

@property(nonatomic,strong) UILabel *textfieldTitle_Label10;
@property(nonatomic,strong) CustomTextField *textfield10;
@property(nonatomic,strong) UIButton *dropDownButton10;

//by shri
@property(strong,nonatomic) UILabel *textfieldTitle_Label11;
@property(strong,nonatomic) CustomTextField *textfield11;
@property(nonatomic,strong) UIButton *dropDownButton11;


@property(strong,nonatomic) UILabel *textfieldTitle_Label12;
@property(strong,nonatomic) CustomTextField *textfield12;
@property(nonatomic,strong) UIButton *dropDownButton12;


@property(strong,nonatomic) UILabel *textfieldTitle_Label13;
@property(strong,nonatomic) CustomTextField *textfield13;
@property(nonatomic,strong) UIButton *dropDownButton13;

@property(strong,nonatomic) UILabel *textfieldTitle_Label14;
@property(strong,nonatomic) CustomTextField *textfield14;
@property(nonatomic,strong) UIButton *dropDownButton14;

@property(strong,nonatomic) UILabel *textfieldTitle_Label15;
@property(strong,nonatomic) CustomTextField *textfield15;
@property(nonatomic,strong) UIButton *dropDownButton15;


@property(nonatomic,strong)UIDatePicker *datePicker1;

/*
 * This method is used to set ChildTemplate1 UIConstraints Buttons.
 */
@property(nonatomic,strong) UIButton *childButton1;
@property(nonatomic,strong) UIButton *childButton2;


/**
 * The below properties are used to add segmented views to choose capturing front and back images
 */
@property(nonatomic,strong) UISegmentedControl *mainSegment;
@property(nonatomic,strong) UILabel *infoLabel1;
@property(nonatomic,strong) UILabel *infoLabel2;
@property(nonatomic,strong) UILabel *infoLabel3;

@property(nonatomic,strong) UIView *bgView;

/**
 * This method is  used for Method Initialization of childTemplate1.
 */
// Method For initialization.
-(id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView;
@end
