//
//  ChildTemplate12.h
//  Consumer Client
//
//  Created by android on 9/1/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XmlParserHandler.h"
#import "ActivityIndicator.h"
#import <AddressBookUI/AddressBookUI.h>
#import "Reachability.h"
#import "BaseView.h"
#import "CustomTextField.h"
/**
 * This class used to handle functionality and View of Childtemplate12
 *
 * @author Integra
 *
 */
@interface ChildTemplate12 :BaseView <UITextFieldDelegate>
{
    NSMutableDictionary *localDictionary;
    NSArray * localArray;
    /**
     Declare a toolbar for pickerview
     */
    UIToolbar *numberToolbar;
    CustomTextField *activeField;
    NSMutableArray *labelValidationsArray;
    NSArray *dropDownDataArray;
    NSMutableDictionary *local_webServiceRequestInputDetails;
    NSString *dropdownString;
    UIButton *btn;
    NSArray *dropDownSetArr;
    
    int label_Y_Position;
    int label_X_Position;
    int distance_Y;
    int filed_Y_Position;
    int filed_X_Position;
    int numberOfFields;
    
}
/**
 * declarations are used to set the UIConstraints Of ChildTemplate12.
 * Label,Value label,Border label,Dropdown and Buttons.
 */
@property(nonatomic,strong) NSString *propertyFileName;
@property(nonatomic,strong) NSString *processor_Code;
@property(nonatomic,strong) UILabel *popupTemplateLabel;
@property(nonatomic,strong) UILabel *valueLabel;
@property(nonatomic,strong) UIScrollView *childTemplate1ScrollView;

@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UILabel *titleBorderLabel;

@property(nonatomic,strong) UILabel *key_label1;
@property(nonatomic,strong) UILabel *value_label1;
@property(nonatomic,strong) UILabel *value_border_label1;

@property(nonatomic,strong) UILabel *key_label2;
@property(nonatomic,strong) UILabel *value_label2;
@property(nonatomic,strong) UILabel *value_border_label2;

@property(nonatomic,strong) UILabel *key_label3;
@property(nonatomic,strong) UILabel *value_label3;
@property(nonatomic,strong) UILabel *value_border_label3;

@property(nonatomic,strong) UILabel *key_label4;
@property(nonatomic,strong) UILabel *value_label4;
@property(nonatomic,strong) UILabel *value_border_label4;

@property(nonatomic,strong) UILabel *key_label5;
@property(nonatomic,strong) UILabel *value_label5;
@property(nonatomic,strong) UILabel *value_border_label5;

@property(nonatomic,strong) UILabel *key_label6;
@property(nonatomic,strong) UILabel *value_label6;
@property(nonatomic,strong) UILabel *value_border_label6;

@property(nonatomic,strong) UILabel *key_label7;
@property(nonatomic,strong) UILabel *value_label7;
@property(nonatomic,strong) UILabel *value_border_label7;

@property(nonatomic,strong) UILabel *key_label8;
@property(nonatomic,strong) UILabel *value_label8;
@property(nonatomic,strong) UILabel *value_border_label8;

@property(nonatomic,strong) UILabel *key_label9;
@property(nonatomic,strong) UILabel *value_label9;
@property(nonatomic,strong) UILabel *value_border_label9;

@property(nonatomic,strong) UILabel *key_label10;
@property(nonatomic,strong) UILabel *value_label10;
@property(nonatomic,strong) UILabel *value_border_label10;

//For Input Fields
@property(nonatomic,strong) UILabel *textfieldTitle_Label1;
@property(nonatomic,strong) UILabel *textfieldTitle_Label2;
@property(nonatomic,strong) UILabel *textfieldTitle_Label3;
@property(nonatomic,strong) UILabel *textfieldTitle_Label4;
@property(nonatomic,strong) UILabel *textfieldTitle_Label5;

@property(nonatomic,strong) UILabel *textfieldborder_Label1;
@property(nonatomic,strong) UILabel *textfieldborder_Label2;
@property(nonatomic,strong) UILabel *textfieldborder_Label3;
@property(nonatomic,strong) UILabel *textfieldborder_Label4;
@property(nonatomic,strong) UILabel *textfieldborder_Label5;

@property(nonatomic,strong) CustomTextField *textfield1;
@property(nonatomic,strong) CustomTextField *textfield2;
@property(nonatomic,strong) CustomTextField *textfield3;
@property(nonatomic,strong) CustomTextField *textfield4;
@property(nonatomic,strong) CustomTextField *textfield5;

@property(nonatomic,strong) UIButton *dropDownButton1;
@property(nonatomic,strong) UIButton *dropDownButton2;
@property(nonatomic,strong) UIButton *dropDownButton3;
@property(nonatomic,strong) UIButton *dropDownButton4;
@property(nonatomic,strong) UIButton *dropDownButton5;

@property(nonatomic,strong) UIButton *childButton1;
@property(nonatomic,strong) UIButton *childButton2;

/**
 * This method is  used for Method Initialization of ChildTemplate12.
 */
// Method For initialization.
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView;

@end
