//  ChildTemplate1.m
//  Consumer Client
//
//  Created by Integra Micro on 17/04/15.
//  Copyright (c) 2015 Integra. All rights reserved.

#import "ChildTemplate1.h"
#import <QuartzCore/QuartzCore.h>
#import "Localization.h"
#import "Constants.h"
#import "ValidationsClass.h"
#import "DatabaseConstatants.h"
#import "NotificationConstants.h"
#import "Template.h"
#import "DatabaseManager.h"
#import "ChildTemplate3.h"
#import "AppDelegate.h"
#import "BaseViewController.h"
#import "PopUpTemplate8.h"
#import "UIView+Toast.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "TemplateData.h"
#import "WebSericeUtils.h"
#import "WebServiceRequestFormation.h"
#import "UIButton+RTL.h"
#import "UITextField+RTL.h"
#import "BackgroundViewTemplate1.h"
#import "UIButton+RTL.h"
#import "PopUpTemplate2.h"
#import "CustomTextField.h"

@interface ChildTemplate1()
@end

@implementation ChildTemplate1
{
    NSMutableDictionary *valueDictionary;
}

@synthesize childTemplate1ScrollView, bgView;
@synthesize button,borderLabel;
@synthesize key_label1,value_label1,value_border_label1;
@synthesize textfieldTitle_Label1,textfield1,dropDownButton1;
@synthesize textfieldTitle_Label2,textfield2,dropDownButton2;
@synthesize textfieldTitle_Label3,textfield3,dropDownButton3;
@synthesize textfieldTitle_Label4,textfield4,dropDownButton4;
@synthesize textfieldTitle_Label5,textfield5,dropDownButton5;
@synthesize textfieldTitle_Label6,textfield6,dropDownButton6;
@synthesize textfieldTitle_Label7,textfield7,dropDownButton7;
@synthesize textfieldTitle_Label8,textfield8,dropDownButton8;
@synthesize textfieldTitle_Label9,textfield9,dropDownButton9;
@synthesize textfieldTitle_Label10,textfield10,dropDownButton10;


@synthesize textfieldTitle_Label11,textfield11,dropDownButton11;
@synthesize textfieldTitle_Label12,textfield12,dropDownButton12;
@synthesize textfieldTitle_Label13,textfield13,dropDownButton13;
@synthesize textfieldTitle_Label14,textfield14,dropDownButton14;
@synthesize textfieldTitle_Label15,textfield15,dropDownButton15;


@synthesize propertyFileName,datePicker1;
@synthesize childButton1,childButton2;

@synthesize mainSegment, infoLabel1, infoLabel2, infoLabel3;

#pragma mark - Childtemplate1 UIView.
/**
 * This method is used to set implemention of ParentTemplate4.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView
{
    NSLog(@"PropertyFileName:%@",propertyFile);
    self = [super initWithFrame:frame];
    
    valueDictionary = [[NSMutableDictionary alloc] initWithDictionary:dataDictionary];
    
    if (self)
    {
        propertyFileName = propertyFile;
        presentingViewCode =processorCode;
        self.backgroundColor=[UIColor whiteColor];
        [self addControlesToView];
        NSArray *arr = [self subviews];
//      NSLog(@"ct1 subviews %@: %@", propertyFileName,arr);
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(idTypeSelected:) name:@"IDTypeSelected" object:nil];
        
        if([propertyFileName isEqualToString:@"RegistrationCT1_T1"]){
            [self getGovernoratesFromWebService];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateGovernorates:) name:@"UpdateGovernorates" object:nil];
        }
        
    }
    return self;
}

#pragma mark - CHILD TEMPLATE PRPERTIES
/**
 * This method is used to set add UIconstraints Frame of ParentTemplate4.
 @param Type- Label,Text field,drop down and Button
 * Set Text (Size,color and font size).
 */
-(void)addControlesToView
{
    isDebugging = NO;
    BOOL isNewUIUpdationNeeded = ([propertyFileName isEqualToString:@"CheckVelocityLimitsCT1_T3"] ||
                                  [propertyFileName isEqualToString:@"ViralWithDrawalPT2_T1"] ||
                                  [propertyFileName isEqualToString:@"ViralWithDrawalCT1_T1"] ||
                                  [propertyFileName isEqualToString:@"WithOTPPT2_T1"] ||
                                  [propertyFileName isEqualToString:@"WithOTPCT1_T1"] ||
                                  [propertyFileName isEqualToString:@"RegistrationPT2_T1"] ||
                                  [propertyFileName isEqualToString:@"RegistrationCT1_T1"] ||
                                  [propertyFileName isEqualToString:@"MyAccountSummaryPT2_T1"] ||
                                  [propertyFileName isEqualToString:@"MyAccountSummaryCT1_T1"] ||
                                  [propertyFileName isEqualToString:@"MyAccountsTransactionStatusPT2_T1"] ||
                                  [propertyFileName isEqualToString:@"MyAccountsTransactionStatusCT1_T1"] ||
                                  [propertyFileName isEqualToString:@"BuyAirTimeMobilePT1_T1"] ||
                                  [propertyFileName isEqualToString:@"MyTopUpsCT1_T1"] ||
                                  [propertyFileName isEqualToString:@"CustomerCashinPT2_T1"] ||
                                  [propertyFileName isEqualToString:@"CustomerCashinCT1_T1"]);
    int localTag=100;
    
    datavalueDictionary = [[NSMutableDictionary alloc] init];
    numberOfFields = 0;
    validationsArray = [[NSMutableArray alloc] init];
    
    label_X_Position = 5.0;
    label_Y_Position = 5.0;
    distance_Y = 5.0;
    
    filed_X_Position = 5.0;
    filed_Y_Position = 0.0;
    
    next_Y_Position = 0;
    
    
    bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor greenColor];
    childTemplate1ScrollView = [[UIScrollView alloc] init];
    
    /*
     * This method is used to add ChildTemplate1 ToolBar (Cancel,Ok buttons).
     */
    numberToolbar = [[UIToolbar alloc] init];
    numberToolbar.frame=CGRectMake(0, 0, SCREEN_WIDTH, 40);
    numberToolbar.items = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancel", nil)] style:UIBarButtonItemStyleDone target:self  action:@selector(cancelNumberPad)],[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],[[UIBarButtonItem alloc]initWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_ok_button", nil)]] style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],nil];
    
    NSString *selectedIconBtncolors =application_branding_color_theme;
    NSArray *selectedIconBtncolorsArray = [ValidationsClass colorWithHexString:selectedIconBtncolors];
    numberToolbar.tintColor = [UIColor colorWithRed:[[selectedIconBtncolorsArray objectAtIndex:0] floatValue] green:[[selectedIconBtncolorsArray objectAtIndex:1] floatValue] blue:[[selectedIconBtncolorsArray objectAtIndex:2] floatValue] alpha:1.0f];
   
    float scroll_Y_Position = 0;
    
    /*
     * This method is used to add ChildTemplate1 HeaderImage Visibility.
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_header_image_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        scroll_Y_Position = 30;
        userProfileImageView=[[UIImageView alloc]init];
        userProfileImageView.frame=CGRectMake(0, 2, 24, 24);
        userProfileImageView.image = [UIImage imageNamed:NSLocalizedStringFromTableInBundle(@"child_template1_header_image_name",propertyFileName,[NSBundle mainBundle], nil)];
        [self addSubview:userProfileImageView];
    }
    /*
     * This method is used to add ChildTemplate1 HeaderButton Visibility.
     */
    
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_header_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        scroll_Y_Position = 30;
        
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_header_label_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)

        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(0,-10,self.frame.size.width,30)];
        button.titleLabel.textAlignment= NSTextAlignmentCenter;
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0,30, 0, 0)];
        button.backgroundColor=[UIColor clearColor];
        [button setTag:999];

        /*
         * This method is used to add ChildTemplate1 HeaderButton name.
         */
        NSString *buttonStr;
        NSString *tempString = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_header_label_text",propertyFileName,[NSBundle mainBundle], nil)];
        if ([tempString rangeOfString:@"{0}"].location != NSNotFound) {
            buttonStr = [Template formatMessageWithMessage:tempString withReferenceValues:[NSString stringWithFormat:@"%d|%d",0,(int)[[NSUserDefaults standardUserDefaults] integerForKey:@"totalDocuments"]]];
        }
        else
        buttonStr = tempString;

        if (buttonStr)
            [button setTitle:buttonStr forState:UIControlStateNormal];

        /*
         * This method is used to add ChildTemplate1 HeaderButton name.
         */
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_header_label_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            /*
             * This method is used to add ChildTemplate1 HeaderButton textcolor.
             */
            
            // Properties for Button TextColor.
    
            NSArray *button1_TextColor;
    
            if (NSLocalizedStringFromTableInBundle(@"child_template1_header_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_header_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
    
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button1_TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
    
            if (button1_TextColor)
                [button setTitleColor:[UIColor colorWithRed:[[button1_TextColor objectAtIndex:0] floatValue] green:[[button1_TextColor objectAtIndex:1] floatValue] blue:[[button1_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
    
            /*
             * This method is used to add ChildTemplate1 HeaderButton textstyle.
             */
            // Properties for Button Textstyle.
    
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_header_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_header_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            /*
             * This method is used to add ChildTemplate1 HeaderButton Font size.
             */
    
            // Properties for Button Font size.
    
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_header_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_header_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button.titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
               button.titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
               button.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            /*
             * This method is used to set ChildTemplate1 Button Default Properties for Button textcolor.
             */
            
            //Default Properties for Button textcolor
    
            NSArray *button1_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button1_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button1_TextColor)
                [button setTitleColor:[UIColor colorWithRed:[[button1_TextColor objectAtIndex:0] floatValue] green:[[button1_TextColor objectAtIndex:1] floatValue] blue:[[button1_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            /*
             * This method is used to set ChildTemplate1 Button Default Properties for Button textstyle.
             */
            //Default Properties for Button textStyle
    
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
               textStyle = application_default_button_text_style;
    
            /*
             * This method is used to set ChildTemplate1 Button Default Properties for Button fontSize.
             */
            //Default Properties for Button fontSize
    
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button.titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button.titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [button setExclusiveTouch:YES];
        if ([action_type compare:ACTION_TYPE_13]==NSOrderedSame)
            [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        
//        [self addSubview:button];

        if ([NSLocalizedStringFromTableInBundle(@"child_template1_header_border_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            borderLabel=[[UILabel alloc]init];
            borderLabel.frame=CGRectMake(0, 29, self.frame.size.width, 1);
            borderLabel.backgroundColor=[UIColor blackColor];
            [self addSubview:borderLabel];
        }
    }
    
    /*
     * This method is used to set ChildTemplate1 Scrollview Declaration.
     */
//    childTemplate1ScrollView.frame = CGRectMake(0, scroll_Y_Position, self.frame.size.width , self.frame.size.height-50);
    childTemplate1ScrollView.frame = CGRectMake(15, 0, SCREEN_WIDTH-30 , self.frame.size.height-40);
    

//    childTemplate1ScrollView.backgroundColor = [UIColor blueColor];
    /*
     * This method is used to add ChildTemplate1 Scrollview Frame.
     */
    bgView.backgroundColor = [UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:0.6];
//    if([propertyFileName isEqualToString:@"RegistrationCT1_T1"] || [propertyFileName isEqualToString:@"MyTopUpsCT1_T1"] || [propertyFileName isEqualToString:@"CustomerCashinCT1_T1"]){
//        if([propertyFileName isEqualToString:@"CustomerCashinCT1_T1"]){
//            childTemplate1ScrollView.frame = CGRectMake(15, 30, SCREEN_WIDTH-30 , self.frame.size.height-40);
//        }
        if([propertyFileName isEqualToString:@"UploadDocumentCT1"]){
            [self addSubview:bgView];
            childTemplate1ScrollView.frame = CGRectMake(25, 35, SCREEN_WIDTH-30 , self.frame.size.height-40);
        }
        [self addSubview:childTemplate1ScrollView];
//    }else{
//        [bgView addSubview:childTemplate1ScrollView];
//    }
    
    if([NSLocalizedStringFromTableInBundle(@"child_template1_segmentbar_visibility", propertyFileName, [NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pickerObtainedImage:) name:@"PickerViewObtainedImage" object:nil];
        
        mainSegment = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_image_1", nil)], [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_image_2", nil)], [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_sr_form", nil)], nil]];
        mainSegment.frame = CGRectMake(childTemplate1ScrollView.frame.origin.x,
                                       bgView.frame.origin.y+230,
                                       childTemplate1ScrollView.frame.size.width, 20);
        mainSegment.selectedSegmentIndex = 0;
        mainSegment.tintColor = [UIColor grayColor];
        [mainSegment addTarget:self action:@selector(mainSegmentControl:) forControlEvents: UIControlEventValueChanged];
        [mainSegment setHidden:TRUE];
        [self addSubview:mainSegment];
        
        infoLabel1 = [[UILabel alloc] init];
        infoLabel1.textColor = [UIColor blackColor];
        infoLabel1.numberOfLines = 2;
        infoLabel1.textAlignment = NSTextAlignmentCenter;
        [infoLabel1 setFrame:CGRectMake(childTemplate1ScrollView.frame.origin.x,
                                        mainSegment.frame.origin.y+20,
                                        childTemplate1ScrollView.frame.size.width, 100)];
        infoLabel1.backgroundColor=[UIColor clearColor];
        infoLabel1.textColor=[UIColor blackColor];
        infoLabel1.userInteractionEnabled=NO;
        infoLabel1.text= [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_select_id_type_to_upload", nil)];
        [self addSubview:infoLabel1];
        
        infoLabel2 = [[UILabel alloc] init];
        infoLabel2.textColor = [UIColor blackColor];
        [infoLabel2 setFrame:CGRectMake(childTemplate1ScrollView.frame.origin.x,
                                        infoLabel1.frame.origin.y+100,
                                        childTemplate1ScrollView.frame.size.width, 25)];
        infoLabel2.backgroundColor = [UIColor clearColor];
        infoLabel2.textColor = [UIColor blackColor];
        infoLabel2.userInteractionEnabled = NO;
        infoLabel2.text = @"";
        [self addSubview:infoLabel2];
        
        infoLabel3 = [[UILabel alloc] init];
        infoLabel3.textColor = [UIColor blackColor];
        [infoLabel3 setFrame:CGRectMake(childTemplate1ScrollView.frame.origin.x,
                                        infoLabel2.frame.origin.y+50,
                                        childTemplate1ScrollView.frame.size.width, 25)];
        infoLabel3.backgroundColor = [UIColor clearColor];
        infoLabel3.textColor = [UIColor blackColor];
        infoLabel3.userInteractionEnabled = NO;
        infoLabel3.text = @"";
        [self addSubview:infoLabel3];
    }
    
    /*
     * This method is used to add ChildTemplate1 (Keylabel,Valuelabel).
     */
    // KeyLabel0
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_labels_field0_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        key_label1 = [[UILabel alloc] init];
        key_label1.frame = CGRectMake(label_X_Position, label_Y_Position, self.frame.size.width-(label_X_Position*2), 21);
        key_label1.backgroundColor = [UIColor clearColor];
        
        /*
         * This method is used to add ChildTemplate1 Keylabel name String.
         */
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label0",propertyFileName,[NSBundle mainBundle], nil)];
        if(labelStr)
            key_label1.text = labelStr;
        
        key_label1.textAlignment = NSTextAlignmentLeft;
        
        /*
         * This method is used to add ChildTemplate1 Keylabel fontattributes .
         */
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_label0_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            /*
             * This method is used to add ChildTemplate1 Keylabel textcolor.
             */
            // properties for label textcolor.
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label0_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label0_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                key_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            /*
             * This method is used to add ChildTemplate1 Keylabel textstyle.
             */
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label0_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label0_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            /*
             * This method is used to add ChildTemplate1 Keylabel Font size.
             */
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label0_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label0_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            /*
             * This method is used to add ChildTemplate1 Keylabel default label text color.
             */
            // default label text color.
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                key_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            /*
             * This method is used to add ChildTemplate1 Keylabel default label textstyle.
             */
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            /*
             * This method is used to add ChildTemplate1 Keylabel default label fontsize.
             */
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                 key_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
               key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        next_Y_Position = CGRectGetMaxY(key_label1.frame) + distance_Y;
        [childTemplate1ScrollView addSubview:key_label1];
        
        /*
         * This method is used to add ChildTemplate1 valuelabel.
         */
        value_label1 = [[UILabel alloc] init];
        value_label1.frame =  CGRectMake(filed_X_Position, next_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
        value_label1.backgroundColor = [UIColor clearColor];
        
        value_label1.textAlignment = NSTextAlignmentLeft;
        /*
         * This method is used to add ChildTemplate1 valuelabel font attributes.
         */
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value0_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            /*
             * This method is used to add ChildTemplate1 valuelabel  Properties for label TextColor.
             */
            // Properties for label TextColor.
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value0_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value0_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                value_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            /*
             * This method is used to add ChildTemplate1 valuelabel  Properties for label Textstyle.
             */
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_value0_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value0_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            /*
             * This method is used to add ChildTemplate1 valuelabel  Properties for label fontsize.
             */
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_value0_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value0_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            /*
             * This method is used to add ChildTemplate1 valuelabel  default label text color.
             */
            // default label text color.
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                value_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            /*
             * This method is used to add ChildTemplate1 valuelabel  default label textstyle.
             */
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            /*
             * This method is used to add ChildTemplate1 valuelabel  default label fontSize.
             */
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
               value_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
               value_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        /*
         * This method is used to set ChildTemplate1 valuelabel  Text based on paramtye.
         */
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value0_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"MyNumber"])
        {
            NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
            if ([[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9]length]>0) {
                value_label1.text = [[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9];
            }
            else if ([[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER6]length]>0)
            {
             value_label1.text = [[sta objectForKey:@"userDetails"] objectForKey:PARAMETER6];
            }
            
            [datavalueDictionary setObject:value_label1.text forKey:NSLocalizedStringFromTableInBundle(@"child_template1_value0_param_type",propertyFileName,[NSBundle mainBundle], nil)];
        }
        
        NSLog(@"Data VAlue dictioanary...%@",datavalueDictionary);
        /*
         * This method is used to add ChildTemplate1 valuelabel.
         */
        [childTemplate1ScrollView addSubview:value_label1];
        next_Y_Position = CGRectGetMaxY(value_label1.frame) + distance_Y;
        numberOfFields++;
    }
    
    /*
     * This method is used to add ChildTemplate1 Field1(textfieldTitle_Label1,textfield1 and dropdownbutton1).
     */
    //ChildTemplate Field1(textfieldTitle_Label1,textfield1)
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_field1_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        
        if (isNewUIUpdationNeeded) {
            label_X_Position = 20;
            label_Y_Position = 25;
            next_Y_Position = 20;
            filed_X_Position = 20;
        }
        
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label1 = [[UILabel alloc] init];
        textfieldTitle_Label1.frame = CGRectMake(label_X_Position,next_Y_Position+10,childTemplate1ScrollView.frame.size.width-(label_X_Position*2), 21);
        textfieldTitle_Label1.backgroundColor = [UIColor clearColor];
        /*
         * This method is used to add ChildTemplate1 textfieldTitle_Label1 String value.
         */
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label1.text =labelStr;
        
        /*
         * This method is used to set ChildTemplate1 textfieldTitle_Label1 fontattributes based on key true or false.
         */
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_label1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
            /*
             * This method is used to set ChildTemplate1 textfieldTitle_Label1 Properties for label TextColor.
             */
            // Properties for label TextColor.
            
            NSArray *label1_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label1_TextColor)
                textfieldTitle_Label1.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            /*
             * This method is used to set ChildTemplate1 textfieldTitle_Label1 Properties for label Textstyle.
             */
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            /*
             * This method is used to set ChildTemplate1 textfieldTitle_Label1 Properties for label Fontsize.
             */
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
               textfieldTitle_Label1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label1.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label1.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
               textfieldTitle_Label1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            /*
             * This method is used to set ChildTemplate1 textfieldTitle_Label1 Default Properties for label textcolor.
             */
            
            //Default Properties for label textcolor
            
            NSArray *label1_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label1_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label1_TextColor)
                textfieldTitle_Label1.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            /*
             * This method is used to set ChildTemplate1 textfieldTitle_Label1 Default Properties for label textstyle.
             */
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            /*
             * This method is used to set ChildTemplate1 textfieldTitle_Label1 Default Properties for label fontsize.
             */
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label1.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label1.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        /*
         * This method is used to set ChildTemplate1 textfield1 Yposition .
         */
        filed_Y_Position = CGRectGetMaxY(textfieldTitle_Label1.frame) + distance_Y;
        [childTemplate1ScrollView addSubview:textfieldTitle_Label1];
        
        /*
         * This method is used to add ChildTemplate1 textfield1 Visibility.
         */
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value1_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
            textfield1 = [[CustomTextField alloc] init];//childTemplate1ScrollView.frame.size.width-(label_X_Position*2)
            
            
            textfield1.frame = CGRectMake(filed_X_Position, filed_Y_Position, SCREEN_WIDTH-100, 30);
            
            [textfield1 setBorderStyle: UITextBorderStyleNone];

            textfield1.backgroundColor = [UIColor whiteColor];
            
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
            textfield1.leftView = paddingView;
            textfield1.leftViewMode = UITextFieldViewModeAlways;
            
            UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
            textfield1.rightView = paddingView1;
            textfield1.rightViewMode = UITextFieldViewModeAlways;
            
            bgView.frame = CGRectMake(10, scroll_Y_Position, self.frame.size.width-20 ,textfield1.frame.origin.y+textfield1.frame.size.height+50);
            bgView.layer.cornerRadius = 20;
            bgView.clipsToBounds = YES;
            bgView.layer.borderWidth = 2;
            bgView.layer.borderColor = [UIColor colorWithRed:110/255.0 green:25/255.0 blue:65/255.0 alpha:1.0].CGColor;

            
            if (isNewUIUpdationNeeded) {
                [textfield1 setBottomBorderColor:[UIColor grayColor]];
            }
            /*
             * This method is used to set ChildTemplate1 textfield1 value string .
             */
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_value1_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield1.placeholder =valueStr;
            /*
             * This method is used to set ChildTemplate1 textfield1 fontattributes override true or false based on set font,color,style.
             */
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                /*
                 * This method is used to set ChildTemplate1 textfield1 Properties for textfield Hint text color.
                 */
                //Properties for textfield Hint text color
                
                if ([textfield1 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield1.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                /*
                 * This method is used to set ChildTemplate1 textfield1 Properties for textfield text color.
                 */
                // Properties for textField TextColor.
                
                NSArray *textField1TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField1TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField1TextColor)
                    textfield1.textColor = [UIColor colorWithRed:[[textField1TextColor objectAtIndex:0] floatValue] green:[[textField1TextColor objectAtIndex:1] floatValue] blue:[[textField1TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                /*
                 * This method is used to set ChildTemplate1 textfield1 Properties for textfield Textstyle.
                 */

                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value1_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                /*
                 * This method is used to set ChildTemplate1 textfield1 Properties for textfield Font size.
                 */
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield1.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield1.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to set ChildTemplate1 textfield1 default Properties for textfield Hint text color.
                 */
                // default Properties for textfield Hint text color
                if ([textfield1 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    if (textFieldHintColor)
                        textfield1.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                /*
                 * This method is used to set ChildTemplate1 textfield1 default Properties for textfield text color.
                 */
                //Default Properties for textfiled textcolor
                
                NSArray *textField1TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField1TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField1TextColor)
                    textfield1.textColor = [UIColor colorWithRed:[[textField1TextColor objectAtIndex:0] floatValue] green:[[textField1TextColor objectAtIndex:1] floatValue] blue:[[textField1TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                /*
                 * This method is used to set ChildTemplate1 textfield1 default Properties for textfield textstyle.
                 */
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                /*
                 * This method is used to set ChildTemplate1 textfield1 default Properties for textfield fontsize.
                 */
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield1.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield1.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [textfield1 setTag:localTag];
            /*
             * This method is used to set ChildTemplate1 textfield1 default Properties for textfield KeyBoardtype.
             */
            // property for KeyBoardType
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value1_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template1_value1_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield1.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield1.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield1.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield1.secureTextEntry=YES;
                textfield1.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield1.inputView = datePicker1;
                datePicker1.tag = textfield1.tag;
            }
            else
                textfield1.keyboardType = UIKeyboardTypeDefault;

            textfield1.inputAccessoryView = numberToolbar;
            textfield1.delegate = self;
            localTag++;
            if(isDebugging){
                NSLog(@"debuging..");
                int lowerBound = 111111;
                int upperBound = 999999;
                int num = lowerBound + arc4random() % (upperBound - lowerBound);
                NSString *phoneStr = [NSString stringWithFormat:@"777%d", num];
                [textfield1 setText:phoneStr];
            }
            [self.childTemplate1ScrollView addSubview:textfield1];
            textfield1.backgroundColor = [UIColor clearColor];
            /*
             * This method is used to set ChildTemplate1 next field Yposition.
             */
            next_Y_Position = textfield1.frame.origin.y+textfield1.frame.size.height;
            /*
             * This method is used to set ChildTemplate1 textfield validationtype.
             */
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value1_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
           
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value1_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"ifscCode"]) {
                ifscCode = textfield1;
                [tempDictionary setObject:PARAMETER20 forKey:@"value_inputtype"];
            }
            else
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value1_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Bank"]) {
                bank = textfield1;
                [tempDictionary setObject:@"bank" forKey:@"value_inputtype"];
            }
            else
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value1_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            [tempDictionary setObject:[textfieldTitle_Label1 text] forKey:@"labelName"];
            [tempDictionary setObject:[textfield1 text] forKey:@"value"];
        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value1_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton1 = [UIButton buttonWithType:UIButtonTypeSystem];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton1 setTitleEdgeInsets:titleInsets];
//            [dropDownButton1 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),30)];
            [dropDownButton1 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, SCREEN_WIDTH-80,30)];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value1_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton1 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton1 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton1.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            if([propertyFileName isEqualToString:@"UploadDocumentCT1"]){
                dropDownButton1.backgroundColor = [UIColor whiteColor];
            }

            [dropDownButton1 setTag:1];
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton1 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value1_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton1.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton1.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton1.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton1.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            else
            {
                   //Default Properties for dropdown textcolor
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton1.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton1.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton1.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton1.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton1.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
            }
            [dropDownButton1 setExclusiveTouch:YES];
            dropDownButton1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton1.frame.size.width-dropDownButton1.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton1.frame.size.width-dropDownButton1.intrinsicContentSize.width-8.0));
                [dropDownButton1 setTitleEdgeInsets:titleInsets];
                dropDownButton1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton1 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton1];
            
//            [[dropDownButton1 layer] setBorderWidth:0.5f];
//            [[dropDownButton1 layer] setBorderColor:[UIColor blackColor].CGColor];
            [dropDownButton1 setBottomBorderColor:[UIColor blackColor]];

            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton1.frame.size.width-20,filed_Y_Position+6,21,21);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];
            next_Y_Position = textfieldTitle_Label1.frame.origin.y+dropDownButton1.frame.origin.y+dropDownButton1.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value1_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value1_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label1_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];

            [tempDictionary setObject:dropDownButton1.titleLabel.text forKey:@"value"];
        }
        
        [validationsArray addObject:tempDictionary];
    }
    
    /*
     * This method is used to add ChildTemplate1 Field2(textfieldTitle_Label2,textfield2 and dropdownbutton2).
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_field2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label2 = [[UILabel alloc] init];
        
        textfieldTitle_Label2.frame = CGRectMake(label_X_Position, next_Y_Position, childTemplate1ScrollView.frame.size.width-(label_X_Position*2), 21);

        textfieldTitle_Label2.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label2_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label2.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_label2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label2_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label2_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label2_TextColor)
                textfieldTitle_Label2.textColor = [UIColor colorWithRed:[[label2_TextColor objectAtIndex:0] floatValue] green:[[label2_TextColor objectAtIndex:1] floatValue] blue:[[label2_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label2_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label2_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label2_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label2_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label2_TextColor)
                textfieldTitle_Label2.textColor = [UIColor colorWithRed:[[label2_TextColor objectAtIndex:0] floatValue] green:[[label2_TextColor objectAtIndex:1] floatValue] blue:[[label2_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        
        [childTemplate1ScrollView addSubview:textfieldTitle_Label2];
        filed_Y_Position = textfieldTitle_Label2.frame.origin.y+textfieldTitle_Label2.frame.size.height+distance_Y;

        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield2 = [[CustomTextField alloc] init];
//          textfield2.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            textfield2.frame = CGRectMake(filed_X_Position, filed_Y_Position, SCREEN_WIDTH-100, 30);
            if([propertyFileName isEqualToString:@"UploadDocumentCT1"]){
                textfield2.frame = CGRectMake(filed_X_Position, filed_Y_Position, dropDownButton1.frame.size.width, 30);
            }
            [textfield2 setBorderStyle: UITextBorderStyleNone];

            textfield2.backgroundColor = [UIColor whiteColor];
            
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
            textfield2.leftView = paddingView;
            textfield2.leftViewMode = UITextFieldViewModeAlways;
            
            UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
            textfield2.rightView = paddingView1;
            textfield2.rightViewMode = UITextFieldViewModeAlways;
            
            bgView.frame = CGRectMake(10, scroll_Y_Position, self.frame.size.width-20 ,textfield2.frame.origin.y+textfield2.frame.size.height+50);
            bgView.layer.cornerRadius = 20;
            bgView.clipsToBounds = YES;
            bgView.layer.borderWidth = 2;
            bgView.layer.borderColor = [UIColor colorWithRed:110/255.0 green:25/255.0 blue:65/255.0 alpha:1.0].CGColor;

            
            if (isNewUIUpdationNeeded) {
                [textfield2 setBottomBorderColor:[UIColor grayColor]];
            }
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_value2_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield2.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([textfield2 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_value2_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value2_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield2.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField2TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_value2_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value2_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField2TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField2TextColor)
                    textfield2.textColor = [UIColor colorWithRed:[[textField2TextColor objectAtIndex:0] floatValue] green:[[textField2TextColor objectAtIndex:1] floatValue] blue:[[textField2TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value2_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value2_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                
                /*
                 * This method is used to set ChildTemplate1 textfield1 default Properties for textfield Hint text color.
                 */
                // default Properties for textfield Hint text color
                if ([textfield2 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    if (textFieldHintColor)
                        textfield2.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                //Default Properties for textfiled textcolor
                
                NSArray *textField2TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField2TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField2TextColor)
                    textfield2.textColor = [UIColor colorWithRed:[[textField2TextColor objectAtIndex:0] floatValue] green:[[textField2TextColor objectAtIndex:1] floatValue] blue:[[textField2TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [textfield2 setTag:localTag];
            // property for KeyBoardType
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value2_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template1_value2_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield2.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield2.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield2.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield2.secureTextEntry=YES;
                textfield2.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield2.inputView = datePicker1;
                datePicker1.tag = textfield2.tag;
            }
            else
                textfield2.keyboardType = UIKeyboardTypeDefault;

            textfield2.inputAccessoryView = numberToolbar;
            textfield2.delegate = self;
            localTag++;
             if(isDebugging){
                 [textfield2 setText:@"samplefirstname"];
             }
            [self.childTemplate1ScrollView addSubview:textfield2];
            if(![propertyFileName isEqualToString:@"UploadDocumentCT1"]){
                textfield2.backgroundColor = [UIColor clearColor];
            }
            next_Y_Position = distance_Y+textfield2.frame.origin.y+textfield2.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value2_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value2_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"ifscCode"]) {
                ifscCode = textfield2;
                [tempDictionary setObject:PARAMETER20 forKey:@"value_inputtype"];
            }
            else
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value2_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Bank"]) {
                bank = textfield2;
                [tempDictionary setObject:@"bank" forKey:@"value_inputtype"];
            }
            else
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value2_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:textfieldTitle_Label2.text forKey:@"labelName"];
            [tempDictionary setObject:[textfield2 text] forKey:@"value"];

        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton2 setTitleEdgeInsets:titleInsets];
            
            [dropDownButton2 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),30)];
            if([propertyFileName isEqualToString:@"CheckVelocityLimitsCT1_T3"]){
                            [dropDownButton2 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, dropDownButton1.frame.size.width,30)];
            }
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value2_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton2 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton2 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton2.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton2 setTag:2];
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton2 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value2_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value2_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton2.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton2.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton2.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton2.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton2.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton2.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton2.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton2.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton2.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];

            }
            [dropDownButton2 setExclusiveTouch:YES];
            dropDownButton2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton2.frame.size.width-dropDownButton2.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton2.frame.size.width-dropDownButton2.intrinsicContentSize.width-8.0));
                [dropDownButton2 setTitleEdgeInsets:titleInsets];
                dropDownButton2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton2 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton2];
            
//            [[dropDownButton2 layer] setBorderWidth:0.5f];
//            [[dropDownButton2 layer] setBorderColor:[UIColor blackColor].CGColor];
            [dropDownButton2 setBottomBorderColor:[UIColor blackColor]];
            
            UIImageView *imageView = [[UIImageView alloc] init];
        imageView.frame=CGRectMake(dropDownButton2.frame.size.width-20,filed_Y_Position+12,15,8);
            
            if([propertyFileName isEqualToString:@"CheckVelocityLimitsCT1_T3"]){
            imageView.frame=CGRectMake(dropDownButton2.frame.size.width-20,filed_Y_Position+5,21,21);
            }
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];

            next_Y_Position = distance_Y+dropDownButton2.frame.origin.y+dropDownButton2.frame.size.height;

            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value2_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value2_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label2_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton2.titleLabel.text forKey:@"value"];
        }
        [validationsArray addObject:tempDictionary];
    }
    /*
     * This method is used to add ChildTemplate1 Field3(textfieldTitle_Label3,textfield3 and dropdownbutton3).
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_field3_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label3 = [[UILabel alloc] init];
        textfieldTitle_Label3.frame = CGRectMake(label_X_Position, next_Y_Position, childTemplate1ScrollView.frame.size.width-(label_X_Position*2), 21);
        textfieldTitle_Label3.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label3_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label3.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label3_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label3_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label3_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label3_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label3_TextColor)
                textfieldTitle_Label3.textColor = [UIColor colorWithRed:[[label3_TextColor objectAtIndex:0] floatValue] green:[[label3_TextColor objectAtIndex:1] floatValue] blue:[[label3_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label3_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label3_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label3_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;

            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label3.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label3.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
     
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label3_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label3_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label3_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label3_TextColor)
                textfieldTitle_Label3.textColor = [UIColor colorWithRed:[[label3_TextColor objectAtIndex:0] floatValue] green:[[label3_TextColor objectAtIndex:1] floatValue] blue:[[label3_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label3.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label3.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label3];
        filed_Y_Position = textfieldTitle_Label3.frame.origin.y+textfieldTitle_Label3.frame.size.height+distance_Y;

        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value3_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield3 = [[CustomTextField alloc] init];
//            textfield3.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            textfield3.frame = CGRectMake(filed_X_Position, filed_Y_Position, SCREEN_WIDTH-100, 30);
            
            [textfield3 setBorderStyle: UITextBorderStyleNone];
            
            if (isNewUIUpdationNeeded) {
                [textfield3 setBottomBorderColor:[UIColor grayColor]];
            }
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_value3_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield3.placeholder =valueStr;
            
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([textfield3 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_value3_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value3_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield3.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField3TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_value3_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField3TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value3_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField3TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField3TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField3TextColor)
                    textfield3.textColor = [UIColor colorWithRed:[[textField3TextColor objectAtIndex:0] floatValue] green:[[textField3TextColor objectAtIndex:1] floatValue] blue:[[textField3TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value3_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value3_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value3_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield3.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield3.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to set ChildTemplate1 textfield1 default Properties for textfield Hint text color.
                 */
                // default Properties for textfield Hint text color
                if ([textfield3 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    if (textFieldHintColor)
                        textfield3.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                //Default Properties for textfiled textcolor
                
                NSArray *textField3TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField3TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField3TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField3TextColor)
                    textfield3.textColor = [UIColor colorWithRed:[[textField3TextColor objectAtIndex:0] floatValue] green:[[textField3TextColor objectAtIndex:1] floatValue] blue:[[textField3TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield3.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield3.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield3.font=[UIFont systemFontOfSize:[fontSize floatValue]];

            }
            [textfield3 setTag:localTag];
            // property for KeyBoardType
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value3_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template1_value3_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield3.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield3.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield3.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield3.secureTextEntry=YES;
                textfield3.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield3.inputView = datePicker1;
                datePicker1.tag = textfield3.tag;
            }
            else
                textfield3.keyboardType = UIKeyboardTypeDefault;
            
            textfield3.inputAccessoryView = numberToolbar;
            textfield3.delegate = self;
            localTag++;
            [self.childTemplate1ScrollView addSubview:textfield3];
            
            if(isDebugging){
                [textfield3 setText:@"samplefathername"];
            }

            next_Y_Position = distance_Y+textfield3.frame.origin.y+textfield3.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value3_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value3_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"ifscCode"]) {
                ifscCode = textfield3;
                [tempDictionary setObject:PARAMETER20 forKey:@"value_inputtype"];
            }
            else
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value3_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Bank"]) {
                bank = textfield3;
                [tempDictionary setObject:PARAMETER19 forKey:@"value_inputtype"];
            }
            else
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value3_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            [tempDictionary setObject:textfieldTitle_Label3.text forKey:@"labelName"];
            [tempDictionary setObject:[textfield3 text] forKey:@"value"];

        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value3_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton3 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton3 setTitleEdgeInsets:titleInsets];
            
            [dropDownButton3 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),30)];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value3_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton3 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton3 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton3.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton3 setTag:3];
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton3 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value3_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value3_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value3_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton3.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton3.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton3.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton3.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton3.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton3.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton3.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton3.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton3.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            
            [dropDownButton3 setExclusiveTouch:YES];
            dropDownButton3.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton3.frame.size.width-dropDownButton3.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton3.frame.size.width-dropDownButton3.intrinsicContentSize.width-8.0));
                [dropDownButton3 setTitleEdgeInsets:titleInsets];
                dropDownButton3.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton3 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            
//            [[dropDownButton3 layer] setBorderWidth:0.5f];
//            [[dropDownButton3 layer] setBorderColor:[UIColor blackColor].CGColor];
            [dropDownButton3 setBottomBorderColor:[UIColor blackColor]];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton3.frame.size.width-20,filed_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];

            [self.childTemplate1ScrollView addSubview:dropDownButton3];
            
            next_Y_Position = distance_Y+dropDownButton3.frame.origin.y+dropDownButton3.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value3_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label3_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton3.titleLabel.text forKey:@"value"];
        }
        [validationsArray addObject:tempDictionary];
    }
    /*
     * This method is used to add ChildTemplate1 Field4(textfieldTitle_Label4,textfield4 and dropdownbutton4).
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_field4_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label4 = [[UILabel alloc] init];
        textfieldTitle_Label4.frame = CGRectMake(label_X_Position, next_Y_Position, childTemplate1ScrollView.frame.size.width-(label_X_Position*2), 21);
        textfieldTitle_Label4.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label4_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label4.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_label4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label4_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label4_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label4_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label4_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label4_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label4_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label4_TextColor)
                textfieldTitle_Label4.textColor = [UIColor colorWithRed:[[label4_TextColor objectAtIndex:0] floatValue] green:[[label4_TextColor objectAtIndex:1] floatValue] blue:[[label4_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label4_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label4_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label4_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label4_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label4.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label4.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label4.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label4.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label4_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label4_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label4_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label4_TextColor)
                textfieldTitle_Label4.textColor = [UIColor colorWithRed:[[label4_TextColor objectAtIndex:0] floatValue] green:[[label4_TextColor objectAtIndex:1] floatValue] blue:[[label4_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label4.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label4.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label4.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label4.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label4];
        filed_Y_Position = textfieldTitle_Label4.frame.origin.y+textfieldTitle_Label4.frame.size.height+distance_Y;

        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value4_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield4 = [[CustomTextField alloc] init];
//            textfield4.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            textfield4.frame = CGRectMake(filed_X_Position, filed_Y_Position, SCREEN_WIDTH-100, 30);
            [textfield4 setBorderStyle: UITextBorderStyleNone];
            
            if (isNewUIUpdationNeeded) {
                [textfield4 setBottomBorderColor:[UIColor grayColor]];
            }
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_value4_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield4.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield4 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_value4_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value4_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield4.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField4TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_value4_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField4TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value4_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField4TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField4TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField4TextColor)
                    textfield4.textColor = [UIColor colorWithRed:[[textField4TextColor objectAtIndex:0] floatValue] green:[[textField4TextColor objectAtIndex:1] floatValue] blue:[[textField4TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value4_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value4_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;

                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value4_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value4_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield4.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield4.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield4.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield4.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                
                /*
                 * This method is used to set ChildTemplate1 textfield4 default Properties for textfield Hint text color.
                 */
                // default Properties for textfield Hint text color
                if ([textfield4 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    if (textFieldHintColor)
                        textfield4.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                //Default Properties for textfiled textcolor
                
                NSArray *textField4TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField4TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField4TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField4TextColor)
                    textfield4.textColor = [UIColor colorWithRed:[[textField4TextColor objectAtIndex:0] floatValue] green:[[textField4TextColor objectAtIndex:1] floatValue] blue:[[textField4TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield4.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield4.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield4.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield4.font=[UIFont systemFontOfSize:[fontSize floatValue]];
       }
                   [textfield4 setTag:localTag];
            // property for KeyBoardType
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value4_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template1_value4_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield4.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield4.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield4.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield4.secureTextEntry=YES;
                textfield4.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield4.inputView = datePicker1;
                datePicker1.tag = textfield4.tag;
            }
            else
                textfield4.keyboardType = UIKeyboardTypeDefault;
            
            textfield4.inputAccessoryView = numberToolbar;
            textfield4.delegate = self;
            localTag++;
            
            [self.childTemplate1ScrollView addSubview:textfield4];
            if(isDebugging){
                [textfield4 setText:@"sampleGFathername"];
            }
            
            next_Y_Position = distance_Y+textfield4.frame.origin.y+textfield4.frame.size.height;
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value4_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value4_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"ifscCode"]) {
                ifscCode = textfield4;
                [tempDictionary setObject:PARAMETER20 forKey:@"value_inputtype"];
            }
            else
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value4_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Bank"]) {
                bank = textfield4;
                [tempDictionary setObject:@"bank" forKey:@"value_inputtype"];
            }
            else
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value4_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            [tempDictionary setObject:textfieldTitle_Label4.text forKey:@"labelName"];
            [tempDictionary setObject:[textfield4 text] forKey:@"value"];

        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value4_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton4 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton4 setTitleEdgeInsets:titleInsets];
            
            [dropDownButton4 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),30)];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value4_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton4 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton4 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton4.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton4 setTag:4];
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value4_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value4_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton4 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value4_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value4_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value4_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value4_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton4.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton4.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton4.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton4.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton4.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton4.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton4.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton4.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton4.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];

            }
            [dropDownButton4 setExclusiveTouch:YES];
            dropDownButton4.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton4.frame.size.width-dropDownButton4.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton4.frame.size.width-dropDownButton4.intrinsicContentSize.width-8.0));
                [dropDownButton4 setTitleEdgeInsets:titleInsets];
                dropDownButton4.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton4 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton4];
//            [[dropDownButton4 layer] setBorderWidth:0.5f];
//            [[dropDownButton4 layer] setBorderColor:[UIColor blackColor].CGColor];
            [dropDownButton4 setBottomBorderColor:[UIColor blackColor]];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton4.frame.size.width-20,filed_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];

            next_Y_Position = distance_Y+dropDownButton4.frame.origin.y+dropDownButton4.frame.size.height;

            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value4_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label4_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton4.titleLabel.text forKey:@"value"];
        }
        [validationsArray addObject:tempDictionary];
    }
    /*
     * This method is used to add ChildTemplate1 Field5(textfieldTitle_Label5,textfield5 and dropdownbutton5).
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_field5_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label5 = [[UILabel alloc] init];
        textfieldTitle_Label5.frame = CGRectMake(label_X_Position, next_Y_Position, childTemplate1ScrollView.frame.size.width-(label_X_Position*2), 21);
        textfieldTitle_Label5.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label5_text",propertyFileName,[NSBundle mainBundle], nil)];
        if (labelStr)
            textfieldTitle_Label5.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_label5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label5_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label5_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label5_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label5_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label5_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label5_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label5_TextColor)
                textfieldTitle_Label5.textColor = [UIColor colorWithRed:[[label5_TextColor objectAtIndex:0] floatValue] green:[[label5_TextColor objectAtIndex:1] floatValue] blue:[[label5_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label5_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label5_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label5_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label5_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label5.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label5.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label5.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label5.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            

          }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label5_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label5_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label5_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label5_TextColor)
                textfieldTitle_Label5.textColor = [UIColor colorWithRed:[[label5_TextColor objectAtIndex:0] floatValue] green:[[label5_TextColor objectAtIndex:1] floatValue] blue:[[label5_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label5.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label5.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label5.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label5.font=[UIFont systemFontOfSize:[fontSize floatValue]];

        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label5];
        filed_Y_Position = textfieldTitle_Label5.frame.origin.y+textfieldTitle_Label5.frame.size.height+distance_Y;

        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value5_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield5 = [[CustomTextField alloc] init];
//            textfield5.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            textfield5.frame = CGRectMake(filed_X_Position, filed_Y_Position, SCREEN_WIDTH-100, 30);
            [textfield5 setBorderStyle: UITextBorderStyleNone];
            
            if (isNewUIUpdationNeeded) {
                [textfield5 setBottomBorderColor:[UIColor grayColor]];
            }
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_value5_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield5.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield5 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_value5_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value5_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield5.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                // Properties for textField TextColor.
                
                NSArray *textField5TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_value5_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField5TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value5_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField5TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField5TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField5TextColor)
                    textfield5.textColor = [UIColor colorWithRed:[[textField5TextColor objectAtIndex:0] floatValue] green:[[textField5TextColor objectAtIndex:1] floatValue] blue:[[textField5TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value5_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value5_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value5_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value5_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield5.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield5.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield5.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield5.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to set ChildTemplate1 textfield5 default Properties for textfield Hint text color.
                 */
                // default Properties for textfield Hint text color
                if ([textfield5 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    if (textFieldHintColor)
                        textfield5.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                //Default Properties for textfiled textcolor
                
                NSArray *textField5TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField5TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField5TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField5TextColor)
                    textfield5.textColor = [UIColor colorWithRed:[[textField5TextColor objectAtIndex:0] floatValue] green:[[textField5TextColor objectAtIndex:1] floatValue] blue:[[textField5TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield5.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield5.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield5.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield5.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
              [textfield5 setTag:localTag];
            // property for KeyBoardType
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value5_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template1_value5_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield5.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield5.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield5.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield5.secureTextEntry=YES;
                textfield5.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield5.inputView = datePicker1;
                datePicker1.tag = textfield5.tag;
            }
            else
                textfield5.keyboardType = UIKeyboardTypeDefault;
            
            textfield5.inputAccessoryView = numberToolbar;
            textfield5.delegate = self;
            localTag++;
            
            [self.childTemplate1ScrollView addSubview:textfield5];
            if(isDebugging){
                [textfield5 setText:@"samplefamilyname"];
            }

            next_Y_Position = distance_Y+textfield5.frame.origin.y+textfield5.frame.size.height;
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value5_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value5_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"ifscCode"]) {
                ifscCode = textfield5;
                [tempDictionary setObject:PARAMETER20 forKey:@"value_inputtype"];
            }
            else
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value5_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Bank"]) {
                bank = textfield5;
                [tempDictionary setObject:@"bank" forKey:@"value_inputtype"];
            }
            else
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value5_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            [tempDictionary setObject:textfieldTitle_Label5.text forKey:@"labelName"];
            [tempDictionary setObject:[textfield5 text] forKey:@"value"];

        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value5_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton5 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton5 setTitleEdgeInsets:titleInsets];
            
            [dropDownButton5 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),30)];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value5_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton5 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton5 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton5.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton5 setTag:5];
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value5_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value5_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton5 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value5_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value5_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value5_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value5_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton5.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton5.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton5.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton5.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];

            }
            else
            {
                //Default Properties for dropdown textcolor
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton5.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton5.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton5.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton5.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton5.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            
            [dropDownButton5 setExclusiveTouch:YES];
            dropDownButton5.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton5.frame.size.width-dropDownButton5.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton5.frame.size.width-dropDownButton5.intrinsicContentSize.width-8.0));
                [dropDownButton5 setTitleEdgeInsets:titleInsets];
                dropDownButton5.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton5 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton5];
//            [[dropDownButton5 layer] setBorderWidth:0.5f];
//            [[dropDownButton5 layer] setBorderColor:[UIColor blackColor].CGColor];
            [dropDownButton5 setBottomBorderColor:[UIColor blackColor]];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton5.frame.size.width-20,filed_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];

            next_Y_Position = distance_Y+dropDownButton5.frame.origin.y+dropDownButton5.frame.size.height;

            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value5_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label5_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton5.titleLabel.text forKey:@"value"];
        }
        [validationsArray addObject:tempDictionary];
    }
    /*
     * This method is used to add ChildTemplate1 Field6(textfieldTitle_Label6,textfield6 and dropdownbutton6).
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_field6_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label6 = [[UILabel alloc] init];
        textfieldTitle_Label6.frame = CGRectMake(label_X_Position, next_Y_Position, childTemplate1ScrollView.frame.size.width-(label_X_Position*2), 21);
        textfieldTitle_Label6.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label6_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label6.text=labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_label6_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label6_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label6_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label6_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label6_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label6_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label6_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label6_TextColor)
                textfieldTitle_Label6.textColor = [UIColor colorWithRed:[[label6_TextColor objectAtIndex:0] floatValue] green:[[label6_TextColor objectAtIndex:1] floatValue] blue:[[label6_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label6_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label6_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label6_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label6_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label6.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label6.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label6.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label6.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label6_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label6_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label6_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label6_TextColor)
                textfieldTitle_Label6.textColor = [UIColor colorWithRed:[[label6_TextColor objectAtIndex:0] floatValue] green:[[label6_TextColor objectAtIndex:1] floatValue] blue:[[label6_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label6.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label6.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label6.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label6.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label6];
        filed_Y_Position = textfieldTitle_Label6.frame.origin.y+textfieldTitle_Label6.frame.size.height+distance_Y;

        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value6_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
        
            textfield6 = [[CustomTextField alloc] init];
//            textfield6.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            textfield6.frame = CGRectMake(filed_X_Position, filed_Y_Position, SCREEN_WIDTH-100, 30);
            [textfield6 setBorderStyle: UITextBorderStyleNone];
            
            if (isNewUIUpdationNeeded) {
                [textfield6 setBottomBorderColor:[UIColor grayColor]];
            }
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_value6_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
           if (textfield6)
                textfield6.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value6_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield6 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_value6_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value6_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield6.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField6TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_value6_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField6TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value6_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField6TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField6TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField6TextColor)
                    textfield6.textColor = [UIColor colorWithRed:[[textField6TextColor objectAtIndex:0] floatValue] green:[[textField6TextColor objectAtIndex:1] floatValue] blue:[[textField6TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value6_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value6_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value6_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value6_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield6.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield6.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield6.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield6.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            else
            {
                /*
                 * This method is used to set ChildTemplate1 textfield6 default Properties for textfield Hint text color.
                 */
                // default Properties for textfield Hint text color
                if ([textfield6 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    if (textFieldHintColor)
                        textfield6.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                //Default Properties for textfiled textcolor
                
                NSArray *textField6TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField6TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField6TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField6TextColor)
                    textfield6.textColor = [UIColor colorWithRed:[[textField6TextColor objectAtIndex:0] floatValue] green:[[textField6TextColor objectAtIndex:1] floatValue] blue:[[textField6TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield6.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield6.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield6.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield6.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
                   [textfield6 setTag:localTag];
            // property for KeyBoardType
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value6_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template1_value6_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield6.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield6.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield6.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield6.secureTextEntry=YES;
                textfield6.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield6.inputView = datePicker1;
                datePicker1.tag = textfield6.tag;
            }
            else
                textfield6.keyboardType = UIKeyboardTypeDefault;
            
            textfield6.inputAccessoryView = numberToolbar;
            textfield6.delegate = self;
            localTag++;
            [self.childTemplate1ScrollView addSubview:textfield6];
            if(isDebugging){
                [textfield6 setText:@"12/12/1956"];
            }

            next_Y_Position = distance_Y+textfield6.frame.origin.y+textfield6.frame.size.height;
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value6_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value6_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"ifscCode"]) {
                ifscCode = textfield6;
                [tempDictionary setObject:PARAMETER20 forKey:@"value_inputtype"];
            }
            else
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value6_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Bank"]) {
                bank = textfield6;
                [tempDictionary setObject:@"bank" forKey:@"value_inputtype"];
            }
            else
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value6_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:textfieldTitle_Label6.text forKey:@"labelName"];
            [tempDictionary setObject:[textfield6 text] forKey:@"value"];

        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value6_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton6 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton6 setTitleEdgeInsets:titleInsets];
            
            [dropDownButton6 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),30)];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value6_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton6 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton6 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton6.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton6 setTag:6];
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value6_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value6_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value6_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton6 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value6_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value6_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value6_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value6_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton6.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton6.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton6.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton6.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
               
            }
            else
            {
                //Default Properties for dropdown textcolor
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton6.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton6.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton6.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton6.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton6.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            [dropDownButton6 setExclusiveTouch:YES];
            dropDownButton6.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton6.frame.size.width-dropDownButton6.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton6.frame.size.width-dropDownButton6.intrinsicContentSize.width-8.0));
                [dropDownButton6 setTitleEdgeInsets:titleInsets];
                dropDownButton6.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton6 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton6];
//            [[dropDownButton6 layer] setBorderWidth:0.5f];
//            [[dropDownButton6 layer] setBorderColor:[UIColor blackColor].CGColor];
            [dropDownButton6 setBottomBorderColor:[UIColor blackColor]];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton6.frame.size.width-20,filed_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];

            next_Y_Position = distance_Y+dropDownButton6.frame.origin.y+dropDownButton6.frame.size.height;

            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value6_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label6_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton6.titleLabel.text forKey:@"value"];
        }
        [validationsArray addObject:tempDictionary];
    }
    /*
     * This method is used to add ChildTemplate1 Field7(textfieldTitle_Label7,textfield7 and dropdownbutton7).
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_field7_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label7 = [[UILabel alloc] init];
        textfieldTitle_Label7.frame = CGRectMake(label_X_Position, next_Y_Position, childTemplate1ScrollView.frame.size.width-(label_X_Position*2), 21);
        textfieldTitle_Label7.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label7_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label7.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_label7_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label7_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label7_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label7_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label7_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label7_TextColor)
                textfieldTitle_Label7.textColor = [UIColor colorWithRed:[[label7_TextColor objectAtIndex:0] floatValue] green:[[label7_TextColor objectAtIndex:1] floatValue] blue:[[label7_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label7_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label7_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label7_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label7_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label7.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label7.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label7.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label7.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label7_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label7_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label7_TextColor)
                textfieldTitle_Label7.textColor = [UIColor colorWithRed:[[label7_TextColor objectAtIndex:0] floatValue] green:[[label7_TextColor objectAtIndex:1] floatValue] blue:[[label7_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label7.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label7.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label7.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label7.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label7];
        filed_Y_Position = textfieldTitle_Label7.frame.origin.y+textfieldTitle_Label7.frame.size.height+distance_Y;

        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value7_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield7 = [[CustomTextField alloc] init];
//            textfield7.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            textfield7.frame = CGRectMake(filed_X_Position, filed_Y_Position, SCREEN_WIDTH-100, 30);
            [textfield7 setBorderStyle: UITextBorderStyleNone];
            
            if (isNewUIUpdationNeeded) {
                [textfield7 setBottomBorderColor:[UIColor grayColor]];
            }
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_value7_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield7.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value7_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([textfield7 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_value7_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value7_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield7.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }

                // Properties for textField TextColor.
                NSArray *textField7TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_value7_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value7_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField7TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField7TextColor)
                    textfield7.textColor = [UIColor colorWithRed:[[textField7TextColor objectAtIndex:0] floatValue] green:[[textField7TextColor objectAtIndex:1] floatValue] blue:[[textField7TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value7_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value7_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value7_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value7_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                    if ([textStyle isEqualToString:TEXT_STYLE_0])
                        textfield7.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                    else if ([textStyle isEqualToString:TEXT_STYLE_1])
                        textfield7.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                    else if ([textStyle isEqualToString:TEXT_STYLE_2])
                        textfield7.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                    else
                        textfield7.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            else
            {
                /*
                 * This method is used to set ChildTemplate1 textfield7 default Properties for textfield Hint text color.
                 */
                // default Properties for textfield Hint text color
                if ([textfield7 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    if (textFieldHintColor)
                        textfield7.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                //Default Properties for textfiled textcolor
                NSArray *textField7TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField7TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField7TextColor)
                    textfield7.textColor = [UIColor colorWithRed:[[textField7TextColor objectAtIndex:0] floatValue] green:[[textField7TextColor objectAtIndex:1] floatValue] blue:[[textField7TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield7.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield7.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield7.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield7.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
             [textfield7 setTag:localTag];
            // property for KeyBoardType
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value7_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template1_value7_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield7.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield7.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield7.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield7.secureTextEntry=YES;
                textfield7.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield7.inputView = datePicker1;
                datePicker1.tag = textfield7.tag;
            }
            else
                textfield7.keyboardType = UIKeyboardTypeDefault;
            
            textfield7.inputAccessoryView = numberToolbar;
            textfield7.delegate = self;
            localTag++;
            
            [self.childTemplate1ScrollView addSubview:textfield7];
            if(isDebugging){
                [textfield7 setText:@"sampleplaceofbirth"];
            }

            next_Y_Position = distance_Y+textfield7.frame.origin.y+textfield7.frame.size.height;
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value7_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value7_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"ifscCode"]) {
                ifscCode = textfield7;
                [tempDictionary setObject:PARAMETER20 forKey:@"value_inputtype"];
            }
            else
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value7_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Bank"]) {
                bank = textfield7;
                [tempDictionary setObject:@"bank" forKey:@"value_inputtype"];
            }
            else
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value7_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            [tempDictionary setObject:textfieldTitle_Label7.text forKey:@"labelName"];
            [tempDictionary setObject:[textfield7 text] forKey:@"value"];

        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value7_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton7 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton7 setTitleEdgeInsets:titleInsets];
            
            [dropDownButton7 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),30)];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value7_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton7 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton7 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton7.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton7 setTag:7];
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value7_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value7_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value7_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton7 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value7_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value7_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value7_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value7_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton7.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton7.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton7.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton7.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
               
            }
            else
            {
                 //Default Properties for dropdown textcolor
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton7.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton7.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton7.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton7.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton7.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            
            
            [dropDownButton7 setExclusiveTouch:YES];
            dropDownButton7.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton7.frame.size.width-dropDownButton7.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton7.frame.size.width-dropDownButton7.intrinsicContentSize.width-8.0));
                [dropDownButton7 setTitleEdgeInsets:titleInsets];
                dropDownButton7.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton7 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton7];
//            [[dropDownButton7 layer] setBorderWidth:0.5f];
//            [[dropDownButton7 layer] setBorderColor:[UIColor blackColor].CGColor];
            [dropDownButton7 setBottomBorderColor:[UIColor blackColor]];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton7.frame.size.width-20,filed_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];

            next_Y_Position = distance_Y+dropDownButton7.frame.origin.y+dropDownButton7.frame.size.height;

            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value7_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label7_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton7.titleLabel.text forKey:@"value"];
        }
        [validationsArray addObject:tempDictionary];
    }
    /*
     * This method is used to add ChildTemplate1 Field8(textfieldTitle_Label8,textfield8 and dropdownbutton8).
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_field8_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label8 = [[UILabel alloc] init];
        textfieldTitle_Label8.frame = CGRectMake(label_X_Position, next_Y_Position, childTemplate1ScrollView.frame.size.width-(label_X_Position*2), 21);
        textfieldTitle_Label8.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label8_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label8.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_label8_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
            // Properties for label TextColor.
            
            NSArray *label8_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label8_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label8_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label8_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label8_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label8_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label8_TextColor)
                textfieldTitle_Label8.textColor = [UIColor colorWithRed:[[label8_TextColor objectAtIndex:0] floatValue] green:[[label8_TextColor objectAtIndex:1] floatValue] blue:[[label8_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label8_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label8_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label8_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label8_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label8.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label8.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label8.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label8.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label8_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label8_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label8_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label8_TextColor)
                textfieldTitle_Label8.textColor = [UIColor colorWithRed:[[label8_TextColor objectAtIndex:0] floatValue] green:[[label8_TextColor objectAtIndex:1] floatValue] blue:[[label8_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label8.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label8.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label8.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label8.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label8];
        filed_Y_Position = textfieldTitle_Label8.frame.origin.y+textfieldTitle_Label8.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value8_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield8 = [[CustomTextField alloc] init];
//            textfield8.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            textfield8.frame = CGRectMake(filed_X_Position, filed_Y_Position, SCREEN_WIDTH-100, 30);
            [textfield8 setBorderStyle: UITextBorderStyleNone];
            
            if (isNewUIUpdationNeeded) {
                [textfield8 setBottomBorderColor:[UIColor grayColor]];
            }
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_value8_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield8.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value8_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield8 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_value8_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value8_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield8.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                
                // Properties for textField TextColor.
                
                NSArray *textField8TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_value8_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField8TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value8_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField8TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField8TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField8TextColor)
                    textfield8.textColor = [UIColor colorWithRed:[[textField8TextColor objectAtIndex:0] floatValue] green:[[textField8TextColor objectAtIndex:1] floatValue] blue:[[textField8TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value8_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value8_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value8_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value8_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield8.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield8.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield8.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield8.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to set ChildTemplate1 textfield8 default Properties for textfield Hint text color.
                 */
                // default Properties for textfield Hint text color
                if ([textfield8 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    if (textFieldHintColor)
                        textfield8.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                //Default Properties for textfiled textcolor
                
                NSArray *textField8TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField8TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField8TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField8TextColor)
                    textfield8.textColor = [UIColor colorWithRed:[[textField8TextColor objectAtIndex:0] floatValue] green:[[textField8TextColor objectAtIndex:1] floatValue] blue:[[textField8TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield8.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield8.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield8.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield8.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [textfield8 setTag:localTag];
            // property for KeyBoardType
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value8_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template1_value8_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield8.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield8.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield8.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield8.secureTextEntry=YES;
                textfield8.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield8.inputView = datePicker1;
                datePicker1.tag = textfield8.tag;
            }
            else
                textfield8.keyboardType = UIKeyboardTypeDefault;

            textfield8.inputAccessoryView = numberToolbar;
            textfield8.delegate = self;
            localTag++;
            [self.childTemplate1ScrollView addSubview:textfield8];
            
            next_Y_Position = distance_Y+textfield8.frame.origin.y+textfield8.frame.size.height;
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value8_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value8_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"ifscCode"]) {
                ifscCode = textfield8;
                [tempDictionary setObject:PARAMETER20 forKey:@"value_inputtype"];
            }
            else
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value8_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Bank"]) {
                bank = textfield8;
                [tempDictionary setObject:@"bank" forKey:@"value_inputtype"];
            }
            else
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value8_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            [tempDictionary setObject:textfieldTitle_Label8.text forKey:@"labelName"];
            [tempDictionary setObject:[textfield8 text] forKey:@"value"];

        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value8_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton8 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton8 setTitleEdgeInsets:titleInsets];
            
            [dropDownButton8 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-100,30)];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value8_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton8 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton8 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton8.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton8 setTag:8];
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value8_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value8_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value8_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton8 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value8_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value8_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value8_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value8_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton8.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton8.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton8.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton8.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton8.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton8.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton8.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton8.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton8.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            [dropDownButton8 setExclusiveTouch:YES];
            dropDownButton8.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton8.frame.size.width-dropDownButton8.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton8.frame.size.width-dropDownButton8.intrinsicContentSize.width-8.0));
                [dropDownButton8 setTitleEdgeInsets:titleInsets];
                dropDownButton8.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton8 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton8];
//            [[dropDownButton8 layer] setBorderWidth:0.5f];
//            [[dropDownButton8 layer] setBorderColor:[UIColor blackColor].CGColor];
            [dropDownButton8 setBottomBorderColor:[UIColor blackColor]];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton8.frame.size.width-5,filed_Y_Position+6,20,20);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];

            next_Y_Position = distance_Y+dropDownButton8.frame.origin.y+dropDownButton8.frame.size.height;

            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value8_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label8_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];

            [tempDictionary setObject:dropDownButton8.titleLabel.text forKey:@"value"];
        }
        [validationsArray addObject:tempDictionary];
    }
    /*
     * This method is used to add ChildTemplate1 Field9(textfieldTitle_Label9,textfield9 and dropdownbutton9).
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_field9_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label9 = [[UILabel alloc] init];
        textfieldTitle_Label9.frame = CGRectMake(label_X_Position, next_Y_Position, childTemplate1ScrollView.frame.size.width-(label_X_Position*2), 21);
        textfieldTitle_Label9.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label9_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label9.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_label9_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label9_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label9_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label9_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label9_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label9_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label9_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label9_TextColor)
                textfieldTitle_Label9.textColor = [UIColor colorWithRed:[[label9_TextColor objectAtIndex:0] floatValue] green:[[label9_TextColor objectAtIndex:1] floatValue] blue:[[label9_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label9_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label9_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label9_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label9_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label9.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label9.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label9.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label9.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label9_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label9_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label9_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label9_TextColor)
                textfieldTitle_Label9.textColor = [UIColor colorWithRed:[[label9_TextColor objectAtIndex:0] floatValue] green:[[label9_TextColor objectAtIndex:1] floatValue] blue:[[label9_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label9.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label9.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label9.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label9.font=[UIFont systemFontOfSize:[fontSize floatValue]];


        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label9];
        filed_Y_Position = textfieldTitle_Label9.frame.origin.y+textfieldTitle_Label9.frame.size.height+distance_Y;

        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value9_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
        
            textfield9 = [[CustomTextField alloc] init];
//            textfield9.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            textfield9.frame = CGRectMake(filed_X_Position, filed_Y_Position, SCREEN_WIDTH-100, 30);

            [textfield9 setBorderStyle: UITextBorderStyleNone];
            
            if (isNewUIUpdationNeeded) {
                [textfield9 setBottomBorderColor:[UIColor grayColor]];
            }
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_value9_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield9.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value9_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield9 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_value9_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value9_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield9.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                
                
                // Properties for textField TextColor.
                
                NSArray *textField9TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_value9_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField9TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value9_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField9TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField9TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField9TextColor)
                    textfield9.textColor = [UIColor colorWithRed:[[textField9TextColor objectAtIndex:0] floatValue] green:[[textField9TextColor objectAtIndex:1] floatValue] blue:[[textField9TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value9_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value9_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value9_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value9_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield9.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield9.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield9.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield9.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to set ChildTemplate1 textfield9 default Properties for textfield Hint text color.
                 */
                // default Properties for textfield Hint text color
                if ([textfield9 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    if (textFieldHintColor)
                        textfield9.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                //Default Properties for textfiled textcolor
                
                NSArray *textField9TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField9TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField9TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField9TextColor)
                    textfield9.textColor = [UIColor colorWithRed:[[textField9TextColor objectAtIndex:0] floatValue] green:[[textField9TextColor objectAtIndex:1] floatValue] blue:[[textField9TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield9.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield9.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield9.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield9.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
              [textfield9 setTag:localTag];
            // property for KeyBoardType
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value9_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template1_value9_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield9.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield9.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield9.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield9.secureTextEntry=YES;
                textfield9.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield9.inputView = datePicker1;
                datePicker1.tag = textfield9.tag;
            }
            else
                textfield9.keyboardType = UIKeyboardTypeDefault;
            
            textfield9.inputAccessoryView = numberToolbar;
            textfield9.delegate = self;
            localTag++;
            [self.childTemplate1ScrollView addSubview:textfield9];
            
            next_Y_Position = distance_Y+textfield9.frame.origin.y+textfield9.frame.size.height;
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value9_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value9_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"ifscCode"]) {
                ifscCode = textfield9;
                [tempDictionary setObject:PARAMETER20 forKey:@"value_inputtype"];
            }
            else
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value9_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Bank"]) {
                bank = textfield9;
                [tempDictionary setObject:@"bank" forKey:@"value_inputtype"];
            }
            else
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value9_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:textfieldTitle_Label9.text forKey:@"labelName"];
            [tempDictionary setObject:[textfield9 text] forKey:@"value"];

        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value9_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton9 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton9 setTitleEdgeInsets:titleInsets];
            
            [dropDownButton9 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-100,30)];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value9_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton9 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton9 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton9.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton9 setTag:9];
              if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value9_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
              {
                               // Properties for dropdown TextColor.
                    NSArray *dropDownTextColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value9_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value9_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                        dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                    
                    if (dropDownTextColor)
                        [dropDownButton9 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                    
                    // Properties for dropdown Textstyle.
                    
                    NSString *textStyle;
                    if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value9_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value9_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                        textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        textStyle = application_default_drop_down_value_text_style;
                    
                    
                    // Properties for dropdown Font size.
                    
                    NSString *fontSize;
                    if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value9_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value9_text_size",propertyFileName,[NSBundle mainBundle], nil);

                    else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                        fontSize =NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                    else
                        fontSize = application_default_drop_down_value_text_size;
                    
                  if ([textStyle isEqualToString:TEXT_STYLE_0])
                      [dropDownButton9.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                  
                  else if ([textStyle isEqualToString:TEXT_STYLE_1])
                      [dropDownButton9.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                  
                  else if ([textStyle isEqualToString:TEXT_STYLE_2])
                      [dropDownButton9.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                  
                  else
                      [dropDownButton9.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton9.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton9.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton9.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton9.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton9.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            [dropDownButton9 setExclusiveTouch:YES];
            dropDownButton9.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton9.frame.size.width-dropDownButton9.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton9.frame.size.width-dropDownButton9.intrinsicContentSize.width-8.0));
                [dropDownButton9 setTitleEdgeInsets:titleInsets];
                dropDownButton9.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton9 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton9];
//            [[dropDownButton9 layer] setBorderWidth:0.5f];
//            [[dropDownButton9 layer] setBorderColor:[UIColor blackColor].CGColor];
            [dropDownButton9 setBottomBorderColor:[UIColor blackColor]];
            
            UIImageView *imageView = [[UIImageView alloc] init];
//            imageView.frame=CGRectMake(dropDownButton9.frame.size.width-20,filed_Y_Position+12,15,8);
            imageView.frame=CGRectMake(dropDownButton9.frame.size.width-5,filed_Y_Position+6,20,20);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];

            next_Y_Position = distance_Y+dropDownButton9.frame.origin.y+dropDownButton9.frame.size.height;

            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value9_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label9_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton9.titleLabel.text forKey:@"value"];
        }
        [validationsArray addObject:tempDictionary];
    }
    /*
     * This method is used to add ChildTemplate1 Field10(textfieldTitle_Label10,textfield10 and dropdownbutton10).
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_field10_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label10 = [[UILabel alloc] init];
        textfieldTitle_Label10.frame = CGRectMake(label_X_Position, next_Y_Position, childTemplate1ScrollView.frame.size.width-(label_X_Position*2), 21);
        textfieldTitle_Label10.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label10_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label10.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
           // Properties for label TextColor.
            NSArray *label10_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label10_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label10_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label10_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label10_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label10_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label10_TextColor)
                textfieldTitle_Label10.textColor = [UIColor colorWithRed:[[label10_TextColor objectAtIndex:0] floatValue] green:[[label10_TextColor objectAtIndex:1] floatValue] blue:[[label10_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label10_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label10_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label10_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label10_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label10.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label10.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label10.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label10.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label10_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label10_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label10_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label10_TextColor)
                textfieldTitle_Label10.textColor = [UIColor colorWithRed:[[label10_TextColor objectAtIndex:0] floatValue] green:[[label10_TextColor objectAtIndex:1] floatValue] blue:[[label10_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label10.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label10.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label10.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label10.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label10];
        filed_Y_Position = textfieldTitle_Label10.frame.origin.y+textfieldTitle_Label10.frame.size.height+distance_Y;

        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value10_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield10 = [[CustomTextField alloc] init];
//            textfield10.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            textfield10.frame = CGRectMake(filed_X_Position, filed_Y_Position, SCREEN_WIDTH-100, 30);

            [textfield10 setBorderStyle: UITextBorderStyleNone];
            
            if (isNewUIUpdationNeeded) {
                [textfield10 setBottomBorderColor:[UIColor grayColor]];
            }
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_value10_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield10.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value10_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield10 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_value10_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value10_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield10.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                

                // Properties for textField TextColor.
                
                NSArray *textField10TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_value10_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField10TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value10_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField10TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField10TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField10TextColor)
                    textfield10.textColor = [UIColor colorWithRed:[[textField10TextColor objectAtIndex:0] floatValue] green:[[textField10TextColor objectAtIndex:1] floatValue] blue:[[textField10TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value10_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value10_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value10_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value10_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield10.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield10.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield10.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield10.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to set ChildTemplate1 textfield9 default Properties for textfield Hint text color.
                 */
                // default Properties for textfield Hint text color
                if ([textfield10 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    if (textFieldHintColor)
                        textfield10.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                //Default Properties for textfiled textcolor
                
                NSArray *textField10TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField10TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField10TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField10TextColor)
                    textfield10.textColor = [UIColor colorWithRed:[[textField10TextColor objectAtIndex:0] floatValue] green:[[textField10TextColor objectAtIndex:1] floatValue] blue:[[textField10TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield10.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield10.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield10.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield10.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [textfield10 setTag:localTag];
            // property for KeyBoardType
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value10_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template1_value10_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield10.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield10.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield10.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield10.secureTextEntry=YES;
                textfield10.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield10.inputView = datePicker1;
                datePicker1.tag = textfield10.tag;
            }
            else
                textfield10.keyboardType = UIKeyboardTypeDefault;

            textfield10.inputAccessoryView = numberToolbar;
            textfield10.delegate = self;
            localTag++;
            [self.childTemplate1ScrollView addSubview:textfield10];
            if(isDebugging){
                NSLog(@"debuging..");
                int lowerBound = 11111111;
                int upperBound = 99999999;
                int num = lowerBound + arc4random() % (upperBound - lowerBound);
                NSString *idStr = [NSString stringWithFormat:@"657%d", num];
                [textfield10 setText:idStr];
//                [textfield10 setText:@"700987066"];
            }


            next_Y_Position = distance_Y+textfield10.frame.origin.y+textfield10.frame.size.height;
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value10_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
           
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value10_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"ifscCode"]) {
                ifscCode = textfield10;
                [tempDictionary setObject:PARAMETER20 forKey:@"value_inputtype"];
            }
            else
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value10_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Bank"]) {
                bank = textfield10;
                [tempDictionary setObject:@"bank" forKey:@"value_inputtype"];
            }
            else
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value10_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            [tempDictionary setObject:textfieldTitle_Label10.text forKey:@"labelName"];
            [tempDictionary setObject:[textfield10 text] forKey:@"value"];

        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value10_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton10 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton10 setTitleEdgeInsets:titleInsets];
            [dropDownButton10 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),30)];
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value10_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton10 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton10 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton10.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton10 setTag:10];

            if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value10_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value10_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value10_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton10 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value10_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value10_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value10_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value10_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton10.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton10.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton10.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton10.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];

            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton10.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton10.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton10.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton10.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton10.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            [dropDownButton10 setExclusiveTouch:YES];
            dropDownButton10.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton10.frame.size.width-dropDownButton10.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton10.frame.size.width-dropDownButton10.intrinsicContentSize.width-8.0));
                [dropDownButton10 setTitleEdgeInsets:titleInsets];
                dropDownButton10.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton10 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton10];
//            [[dropDownButton10 layer] setBorderWidth:0.5f];
//            [[dropDownButton10 layer] setBorderColor:[UIColor blackColor].CGColor];
            [dropDownButton10 setBottomBorderColor:[UIColor blackColor]];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton10.frame.size.width-20,filed_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];

            next_Y_Position = distance_Y+dropDownButton10.frame.origin.y+dropDownButton10.frame.size.height;

            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value10_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label10_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton10.titleLabel.text forKey:@"value"];
        }
        [validationsArray addObject:tempDictionary];
        
        
    }
    
    //New UI Elements added by shri

    /*
     * This method is used to add ChildTemplate1 field11(textfieldTitle_Label11,textfield11 and dropdownbutton11).
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_field11_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label11 = [[UILabel alloc] init];
        textfieldTitle_Label11.frame = CGRectMake(label_X_Position, next_Y_Position, childTemplate1ScrollView.frame.size.width-(label_X_Position*2), 21);
        textfieldTitle_Label11.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label11_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label11.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
            // Properties for label TextColor.
            NSArray *label11_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label11_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label11_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label11_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label11_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label11_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label11_TextColor)
                textfieldTitle_Label11.textColor = [UIColor colorWithRed:[[label11_TextColor objectAtIndex:0] floatValue] green:[[label11_TextColor objectAtIndex:1] floatValue] blue:[[label11_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label11_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label11_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label11_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label11_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label11.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label11.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label11.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label11.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label11_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label11_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label11_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label11_TextColor)
                textfieldTitle_Label11.textColor = [UIColor colorWithRed:[[label11_TextColor objectAtIndex:0] floatValue] green:[[label11_TextColor objectAtIndex:1] floatValue] blue:[[label11_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label11.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label11.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label11.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label11.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label11];
        filed_Y_Position = textfieldTitle_Label11.frame.origin.y+textfieldTitle_Label11.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value11_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield11 = [[CustomTextField alloc] init];
//            textfield11.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            textfield11.frame = CGRectMake(filed_X_Position, filed_Y_Position, SCREEN_WIDTH-100, 30);

            [textfield11 setBorderStyle: UITextBorderStyleNone];
            
            if (isNewUIUpdationNeeded) {
                [textfield11 setBottomBorderColor:[UIColor grayColor]];
            }
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_value11_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield11.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value11_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield11 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_value11_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value11_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield11.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                
                // Properties for textField TextColor.
                
                NSArray *textfield11TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_value11_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textfield11TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value11_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textfield11TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textfield11TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textfield11TextColor)
                    textfield11.textColor = [UIColor colorWithRed:[[textfield11TextColor objectAtIndex:0] floatValue] green:[[textfield11TextColor objectAtIndex:1] floatValue] blue:[[textfield11TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value11_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value11_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value11_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value11_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield11.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield11.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield11.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield11.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to set ChildTemplate1 textfield9 default Properties for textfield Hint text color.
                 */
                // default Properties for textfield Hint text color
                if ([textfield11 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    if (textFieldHintColor)
                        textfield11.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                //Default Properties for textfiled textcolor
                
                NSArray *textfield11TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textfield11TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textfield11TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textfield11TextColor)
                    textfield11.textColor = [UIColor colorWithRed:[[textfield11TextColor objectAtIndex:0] floatValue] green:[[textfield11TextColor objectAtIndex:1] floatValue] blue:[[textfield11TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield11.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield11.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield11.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield11.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [textfield11 setTag:localTag];
            // property for KeyBoardType
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value11_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template1_value11_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield11.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield11.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield11.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield11.secureTextEntry=YES;
                textfield11.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield11.inputView = datePicker1;
                datePicker1.tag = textfield11.tag;
            }
            else
                textfield11.keyboardType = UIKeyboardTypeDefault;
            
            textfield11.inputAccessoryView = numberToolbar;
            textfield11.delegate = self;
            localTag++;
            [self.childTemplate1ScrollView addSubview:textfield11];
            if(isDebugging){
                [textfield11 setText:@"12/12/2025"];
            }

            next_Y_Position = distance_Y+textfield11.frame.origin.y+textfield11.frame.size.height;
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value11_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value11_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"ifscCode"]) {
                ifscCode = textfield11;
                [tempDictionary setObject:PARAMETER20 forKey:@"value_inputtype"];
            }
            else
                if ([NSLocalizedStringFromTableInBundle(@"child_template1_value11_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Bank"]) {
                    bank = textfield11;
                    [tempDictionary setObject:@"bank" forKey:@"value_inputtype"];
                }
                else
                    [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value11_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            [tempDictionary setObject:textfieldTitle_Label11.text forKey:@"labelName"];
            [tempDictionary setObject:[textfield11 text] forKey:@"value"];
            
        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value11_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton11 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton11 setTitleEdgeInsets:titleInsets];
            [dropDownButton11 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),30)];
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value11_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton11 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton11 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton11.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton11 setTag:10];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value11_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value11_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value11_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton11 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value11_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value11_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value11_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value11_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton11.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton11.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton11.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton11.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton11.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton11.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton11.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton11.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton11.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            [dropDownButton11 setExclusiveTouch:YES];
            dropDownButton11.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton11.frame.size.width-dropDownButton11.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton11.frame.size.width-dropDownButton11.intrinsicContentSize.width-8.0));
                [dropDownButton11 setTitleEdgeInsets:titleInsets];
                dropDownButton11.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton11 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton11];
            //            [[dropDownButton11 layer] setBorderWidth:0.5f];
            //            [[dropDownButton11 layer] setBorderColor:[UIColor blackColor].CGColor];
            [dropDownButton11 setBottomBorderColor:[UIColor blackColor]];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton11.frame.size.width-20,filed_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];
            
            next_Y_Position = distance_Y+dropDownButton11.frame.origin.y+dropDownButton11.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value11_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label11_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton11.titleLabel.text forKey:@"value"];
        }
        [validationsArray addObject:tempDictionary];
        
        
    }

    /*
     * This method is used to add ChildTemplate1 field12(textfieldTitle_Label12,textfield12 and dropdownbutton12).
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_field12_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label12 = [[UILabel alloc] init];
        textfieldTitle_Label12.frame = CGRectMake(label_X_Position, next_Y_Position, childTemplate1ScrollView.frame.size.width-(label_X_Position*2), 21);
        textfieldTitle_Label12.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label12_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label12.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
            // Properties for label TextColor.
            NSArray *label12_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label12_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label12_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label12_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label12_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label12_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label12_TextColor)
                textfieldTitle_Label12.textColor = [UIColor colorWithRed:[[label12_TextColor objectAtIndex:0] floatValue] green:[[label12_TextColor objectAtIndex:1] floatValue] blue:[[label12_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label12_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label12_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label12_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label12_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label12.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label12.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label12.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label12.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label12_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label12_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label12_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label12_TextColor)
                textfieldTitle_Label12.textColor = [UIColor colorWithRed:[[label12_TextColor objectAtIndex:0] floatValue] green:[[label12_TextColor objectAtIndex:1] floatValue] blue:[[label12_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label12.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label12.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label12.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label12.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label12];
        filed_Y_Position = textfieldTitle_Label12.frame.origin.y+textfieldTitle_Label12.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value12_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield12 = [[CustomTextField alloc] init];
//            textfield12.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            textfield12.frame = CGRectMake(filed_X_Position, filed_Y_Position, SCREEN_WIDTH-100, 30);

            [textfield12 setBorderStyle: UITextBorderStyleNone];
            
            if (isNewUIUpdationNeeded) {
                [textfield12 setBottomBorderColor:[UIColor grayColor]];
            }
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_value12_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield12.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value12_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield12 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_value12_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value12_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield12.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                
                // Properties for textField TextColor.
                
                NSArray *textfield12TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_value12_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textfield12TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value12_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textfield12TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textfield12TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textfield12TextColor)
                    textfield12.textColor = [UIColor colorWithRed:[[textfield12TextColor objectAtIndex:0] floatValue] green:[[textfield12TextColor objectAtIndex:1] floatValue] blue:[[textfield12TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value12_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value12_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value12_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value12_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield12.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield12.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield12.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield12.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to set ChildTemplate1 textfield9 default Properties for textfield Hint text color.
                 */
                // default Properties for textfield Hint text color
                if ([textfield12 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    if (textFieldHintColor)
                        textfield12.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                //Default Properties for textfiled textcolor
                
                NSArray *textfield12TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textfield12TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textfield12TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textfield12TextColor)
                    textfield12.textColor = [UIColor colorWithRed:[[textfield12TextColor objectAtIndex:0] floatValue] green:[[textfield12TextColor objectAtIndex:1] floatValue] blue:[[textfield12TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield12.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield12.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield12.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield12.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [textfield12 setTag:localTag];
            // property for KeyBoardType
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value12_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template1_value12_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield12.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield12.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield12.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield12.secureTextEntry=YES;
                textfield12.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield12.inputView = datePicker1;
                datePicker1.tag = textfield12.tag;
            }
            else
                textfield12.keyboardType = UIKeyboardTypeDefault;
            
            textfield12.inputAccessoryView = numberToolbar;
            textfield12.delegate = self;
            localTag++;
            [self.childTemplate1ScrollView addSubview:textfield12];
            
            next_Y_Position = distance_Y+textfield12.frame.origin.y+textfield12.frame.size.height;
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value12_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value12_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"ifscCode"]) {
                ifscCode = textfield12;
                [tempDictionary setObject:PARAMETER20 forKey:@"value_inputtype"];
            }
            else
                if ([NSLocalizedStringFromTableInBundle(@"child_template1_value12_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Bank"]) {
                    bank = textfield12;
                    [tempDictionary setObject:@"bank" forKey:@"value_inputtype"];
                }
                else
                    [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value12_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            [tempDictionary setObject:textfieldTitle_Label12.text forKey:@"labelName"];
            [tempDictionary setObject:[textfield12 text] forKey:@"value"];
            
        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value12_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton12 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton12 setTitleEdgeInsets:titleInsets];
            [dropDownButton12 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-100,30)];
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value12_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton12 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton12 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton12.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton12 setTag:12];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value12_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value12_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value12_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton12 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value12_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value12_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value12_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value12_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton12.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton12.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton12.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton12.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton12.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton12.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton12.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton12.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton12.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            [dropDownButton12 setExclusiveTouch:YES];
            dropDownButton12.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton12.frame.size.width-dropDownButton12.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton12.frame.size.width-dropDownButton12.intrinsicContentSize.width-8.0));
                [dropDownButton12 setTitleEdgeInsets:titleInsets];
                dropDownButton12.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton12 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton12];
            //            [[dropDownButton12 layer] setBorderWidth:0.5f];
            //            [[dropDownButton12 layer] setBorderColor:[UIColor blackColor].CGColor];
            [dropDownButton12 setBottomBorderColor:[UIColor blackColor]];
            
            UIImageView *imageView = [[UIImageView alloc] init];
//            imageView.frame=CGRectMake(dropDownButton12.frame.size.width-20,filed_Y_Position+12,15,8);
            imageView.frame=CGRectMake(dropDownButton12.frame.size.width-5,filed_Y_Position+6,20,20);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];
            
            next_Y_Position = distance_Y+dropDownButton12.frame.origin.y+dropDownButton12.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value12_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label12_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton12.titleLabel.text forKey:@"value"];
        }
        [validationsArray addObject:tempDictionary];
        
        
    }

    /*
     * This method is used to add ChildTemplate1 field13(textfieldTitle_Label13,textfield13 and dropdownbutton13).
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_field13_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label13 = [[UILabel alloc] init];
        textfieldTitle_Label13.frame = CGRectMake(label_X_Position, next_Y_Position, childTemplate1ScrollView.frame.size.width-(label_X_Position*2), 21);
        textfieldTitle_Label13.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label13_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label13.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
            // Properties for label TextColor.
            NSArray *label13_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label13_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label13_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label13_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label13_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label13_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label13_TextColor)
                textfieldTitle_Label13.textColor = [UIColor colorWithRed:[[label13_TextColor objectAtIndex:0] floatValue] green:[[label13_TextColor objectAtIndex:1] floatValue] blue:[[label13_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label13_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label13_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label13_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label13_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label13.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label13.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label13.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label13.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label13_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label13_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label13_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label13_TextColor)
                textfieldTitle_Label13.textColor = [UIColor colorWithRed:[[label13_TextColor objectAtIndex:0] floatValue] green:[[label13_TextColor objectAtIndex:1] floatValue] blue:[[label13_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label13.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label13.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label13.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label13.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label13];
        filed_Y_Position = textfieldTitle_Label13.frame.origin.y+textfieldTitle_Label13.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value13_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield13 = [[CustomTextField alloc] init];
//            textfield13.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            textfield13.frame = CGRectMake(filed_X_Position, filed_Y_Position, SCREEN_WIDTH-100, 30);

            [textfield13 setBorderStyle: UITextBorderStyleNone];
            
            if (isNewUIUpdationNeeded) {
                [textfield13 setBottomBorderColor:[UIColor grayColor]];
            }
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_value13_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield13.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value13_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield13 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_value13_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value13_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield13.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                
                // Properties for textField TextColor.
                
                NSArray *textfield13TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_value13_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textfield13TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value13_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textfield13TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textfield13TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textfield13TextColor)
                    textfield13.textColor = [UIColor colorWithRed:[[textfield13TextColor objectAtIndex:0] floatValue] green:[[textfield13TextColor objectAtIndex:1] floatValue] blue:[[textfield13TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value13_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value13_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value13_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value13_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield13.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield13.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield13.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield13.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to set ChildTemplate1 textfield9 default Properties for textfield Hint text color.
                 */
                // default Properties for textfield Hint text color
                if ([textfield13 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    if (textFieldHintColor)
                        textfield13.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                //Default Properties for textfiled textcolor
                
                NSArray *textfield13TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textfield13TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textfield13TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textfield13TextColor)
                    textfield13.textColor = [UIColor colorWithRed:[[textfield13TextColor objectAtIndex:0] floatValue] green:[[textfield13TextColor objectAtIndex:1] floatValue] blue:[[textfield13TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield13.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield13.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield13.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield13.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [textfield13 setTag:localTag];
            // property for KeyBoardType
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value13_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template1_value13_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield13.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield13.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield13.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield13.secureTextEntry=YES;
                textfield13.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield13.inputView = datePicker1;
                datePicker1.tag = textfield13.tag;
            }
            else
                textfield13.keyboardType = UIKeyboardTypeDefault;
            
            textfield13.inputAccessoryView = numberToolbar;
            textfield13.delegate = self;
            localTag++;
            [self.childTemplate1ScrollView addSubview:textfield13];
            
            next_Y_Position = distance_Y+textfield13.frame.origin.y+textfield13.frame.size.height;
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value13_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value13_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"ifscCode"]) {
                ifscCode = textfield13;
                [tempDictionary setObject:PARAMETER20 forKey:@"value_inputtype"];
            }
            else
                if ([NSLocalizedStringFromTableInBundle(@"child_template1_value13_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Bank"]) {
                    bank = textfield13;
                    [tempDictionary setObject:@"bank" forKey:@"value_inputtype"];
                }
                else
                    [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value13_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            [tempDictionary setObject:textfieldTitle_Label13.text forKey:@"labelName"];
            [tempDictionary setObject:[textfield13 text] forKey:@"value"];
            
        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value13_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton13 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton13 setTitleEdgeInsets:titleInsets];
            [dropDownButton13 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-100,30)];
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value13_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton13 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton13 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton13.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton13 setTag:13];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value13_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value13_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value13_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton13 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value13_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value13_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value13_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value13_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton13.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton13.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton13.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton13.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton13.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton13.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton13.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton13.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton13.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            [dropDownButton13 setExclusiveTouch:YES];
            dropDownButton13.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton13.frame.size.width-dropDownButton13.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton13.frame.size.width-dropDownButton13.intrinsicContentSize.width-8.0));
                [dropDownButton13 setTitleEdgeInsets:titleInsets];
                dropDownButton13.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton13 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton13];
            //            [[dropDownButton13 layer] setBorderWidth:0.5f];
            //            [[dropDownButton13 layer] setBorderColor:[UIColor blackColor].CGColor];
            [dropDownButton13 setBottomBorderColor:[UIColor blackColor]];
            
            UIImageView *imageView = [[UIImageView alloc] init];
//            imageView.frame=CGRectMake(dropDownButton13.frame.size.width-20,filed_Y_Position+13,15,8);
            imageView.frame=CGRectMake(dropDownButton13.frame.size.width-5,filed_Y_Position+6,20,20);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];
            
            next_Y_Position = distance_Y+dropDownButton13.frame.origin.y+dropDownButton13.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value13_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label13_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton13.titleLabel.text forKey:@"value"];
        }
        [validationsArray addObject:tempDictionary];
        
        
    }
    
    /*
     * This method is used to add ChildTemplate1 field14(textfieldTitle_Label14,textfield14 and dropdownbutton14).
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_field14_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label14 = [[UILabel alloc] init];
        textfieldTitle_Label14.frame = CGRectMake(label_X_Position, next_Y_Position, childTemplate1ScrollView.frame.size.width-(label_X_Position*2), 21);
        textfieldTitle_Label14.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label14_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label14.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
            // Properties for label TextColor.
            NSArray *label14_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label14_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label14_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label14_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label14_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label14_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label14_TextColor)
                textfieldTitle_Label14.textColor = [UIColor colorWithRed:[[label14_TextColor objectAtIndex:0] floatValue] green:[[label14_TextColor objectAtIndex:1] floatValue] blue:[[label14_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label14_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label14_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label14_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label14_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label14.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label14.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label14.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label14.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label14_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label14_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label14_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label14_TextColor)
                textfieldTitle_Label14.textColor = [UIColor colorWithRed:[[label14_TextColor objectAtIndex:0] floatValue] green:[[label14_TextColor objectAtIndex:1] floatValue] blue:[[label14_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label14.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label14.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label14.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label14.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label14];
        filed_Y_Position = textfieldTitle_Label14.frame.origin.y+textfieldTitle_Label14.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value14_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield14 = [[CustomTextField alloc] init];
//            textfield14.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            textfield14.frame = CGRectMake(filed_X_Position, filed_Y_Position, SCREEN_WIDTH-100, 30);

            [textfield14 setBorderStyle: UITextBorderStyleNone];
            
            if (isNewUIUpdationNeeded) {
                [textfield14 setBottomBorderColor:[UIColor grayColor]];
            }
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_value14_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield14.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value14_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield14 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_value14_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value14_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield14.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                
                // Properties for textField TextColor.
                
                NSArray *textfield14TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_value14_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textfield14TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value14_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textfield14TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textfield14TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textfield14TextColor)
                    textfield14.textColor = [UIColor colorWithRed:[[textfield14TextColor objectAtIndex:0] floatValue] green:[[textfield14TextColor objectAtIndex:1] floatValue] blue:[[textfield14TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value14_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value14_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value14_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value14_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield14.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield14.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield14.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield14.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to set ChildTemplate1 textfield9 default Properties for textfield Hint text color.
                 */
                // default Properties for textfield Hint text color
                if ([textfield14 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    if (textFieldHintColor)
                        textfield14.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                //Default Properties for textfiled textcolor
                
                NSArray *textfield14TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textfield14TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textfield14TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textfield14TextColor)
                    textfield14.textColor = [UIColor colorWithRed:[[textfield14TextColor objectAtIndex:0] floatValue] green:[[textfield14TextColor objectAtIndex:1] floatValue] blue:[[textfield14TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield14.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield14.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield14.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield14.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [textfield14 setTag:localTag];
            // property for KeyBoardType
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value14_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template1_value14_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield14.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield14.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield14.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield14.secureTextEntry=YES;
                textfield14.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield14.inputView = datePicker1;
                datePicker1.tag = textfield14.tag;
            }
            else
                textfield14.keyboardType = UIKeyboardTypeDefault;
            
            textfield14.inputAccessoryView = numberToolbar;
            textfield14.delegate = self;
            localTag++;
            [self.childTemplate1ScrollView addSubview:textfield14];
            if(isDebugging){
                [textfield14 setText:@"sample street"];
            }

            next_Y_Position = distance_Y+textfield14.frame.origin.y+textfield14.frame.size.height;
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value14_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value14_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"ifscCode"]) {
                ifscCode = textfield14;
                [tempDictionary setObject:PARAMETER20 forKey:@"value_inputtype"];
            }
            else
                if ([NSLocalizedStringFromTableInBundle(@"child_template1_value14_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Bank"]) {
                    bank = textfield14;
                    [tempDictionary setObject:@"bank" forKey:@"value_inputtype"];
                }
                else
                    [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value14_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            [tempDictionary setObject:textfieldTitle_Label14.text forKey:@"labelName"];
            [tempDictionary setObject:[textfield14 text] forKey:@"value"];
            
        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value14_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton14 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton14 setTitleEdgeInsets:titleInsets];
            [dropDownButton14 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),30)];
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value14_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton14 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton14 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton14.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton14 setTag:10];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value14_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value14_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value14_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton14 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value14_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value14_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value14_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value14_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton14.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton14.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton14.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton14.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton14.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton14.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton14.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton14.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton14.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            [dropDownButton14 setExclusiveTouch:YES];
            dropDownButton14.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton14.frame.size.width-dropDownButton14.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton14.frame.size.width-dropDownButton14.intrinsicContentSize.width-8.0));
                [dropDownButton14 setTitleEdgeInsets:titleInsets];
                dropDownButton14.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton14 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton14];
            //            [[dropDownButton14 layer] setBorderWidth:0.5f];
            //            [[dropDownButton14 layer] setBorderColor:[UIColor blackColor].CGColor];
            [dropDownButton14 setBottomBorderColor:[UIColor blackColor]];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton14.frame.size.width-20,filed_Y_Position+14,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];
            
            next_Y_Position = distance_Y+dropDownButton14.frame.origin.y+dropDownButton14.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value14_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label14_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton14.titleLabel.text forKey:@"value"];
        }
        [validationsArray addObject:tempDictionary];
        
        
    }

    /*
     * This method is used to add ChildTemplate1 field15(textfieldTitle_Label15,textfield15 and dropdownbutton15).
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_field15_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label15 = [[UILabel alloc] init];
        textfieldTitle_Label15.frame = CGRectMake(label_X_Position, next_Y_Position, childTemplate1ScrollView.frame.size.width-(label_X_Position*2), 21);
        textfieldTitle_Label15.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label15_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label15.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
            // Properties for label TextColor.
            NSArray *label15_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label15_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label15_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label15_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label15_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label15_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label15_TextColor)
                textfieldTitle_Label15.textColor = [UIColor colorWithRed:[[label15_TextColor objectAtIndex:0] floatValue] green:[[label15_TextColor objectAtIndex:1] floatValue] blue:[[label15_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label15_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label15_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label15_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label15_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label15.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label15.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label15.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label15.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label15_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label15_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label15_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label15_TextColor)
                textfieldTitle_Label15.textColor = [UIColor colorWithRed:[[label15_TextColor objectAtIndex:0] floatValue] green:[[label15_TextColor objectAtIndex:1] floatValue] blue:[[label15_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label15.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label15.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label15.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label15.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label15];
        filed_Y_Position = textfieldTitle_Label15.frame.origin.y+textfieldTitle_Label15.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_value15_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield15 = [[CustomTextField alloc] init];
//            textfield15.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            textfield15.frame = CGRectMake(filed_X_Position, filed_Y_Position, SCREEN_WIDTH-100, 30);

            [textfield15 setBorderStyle: UITextBorderStyleNone];
            
            if (isNewUIUpdationNeeded) {
                [textfield15 setBottomBorderColor:[UIColor grayColor]];
            }
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_value15_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield15.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value15_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield15 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_value15_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value15_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield15.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                
                // Properties for textField TextColor.
                
                NSArray *textfield15TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_value15_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textfield15TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_value15_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textfield15TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textfield15TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textfield15TextColor)
                    textfield15.textColor = [UIColor colorWithRed:[[textfield15TextColor objectAtIndex:0] floatValue] green:[[textfield15TextColor objectAtIndex:1] floatValue] blue:[[textfield15TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value15_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_value15_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_value15_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_value15_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield15.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield15.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield15.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield15.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to set ChildTemplate1 textfield9 default Properties for textfield Hint text color.
                 */
                // default Properties for textfield Hint text color
                if ([textfield15 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    if (textFieldHintColor)
                        textfield15.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                //Default Properties for textfiled textcolor
                
                NSArray *textfield15TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textfield15TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textfield15TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textfield15TextColor)
                    textfield15.textColor = [UIColor colorWithRed:[[textfield15TextColor objectAtIndex:0] floatValue] green:[[textfield15TextColor objectAtIndex:1] floatValue] blue:[[textfield15TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield15.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield15.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield15.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield15.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [textfield15 setTag:localTag];
            // property for KeyBoardType
            
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_value15_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template1_value15_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield15.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield15.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield15.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield15.secureTextEntry=YES;
                textfield15.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield15.inputView = datePicker1;
                datePicker1.tag = textfield15.tag;
            }
            else
                textfield15.keyboardType = UIKeyboardTypeDefault;
            
            textfield15.inputAccessoryView = numberToolbar;
            textfield15.delegate = self;
            localTag++;
            [self.childTemplate1ScrollView addSubview:textfield15];
            if(isDebugging){
                [textfield15 setText:@"sample zone"];
            }

            next_Y_Position = distance_Y+textfield15.frame.origin.y+textfield15.frame.size.height;
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value15_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_value15_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"ifscCode"]) {
                ifscCode = textfield15;
                [tempDictionary setObject:PARAMETER20 forKey:@"value_inputtype"];
            }
            else
                if ([NSLocalizedStringFromTableInBundle(@"child_template1_value15_param_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Bank"]) {
                    bank = textfield15;
                    [tempDictionary setObject:@"bank" forKey:@"value_inputtype"];
                }
                else
                    [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_value15_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            
            [tempDictionary setObject:textfieldTitle_Label15.text forKey:@"labelName"];
            [tempDictionary setObject:[textfield15 text] forKey:@"value"];
            
        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value15_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton15 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton15 setTitleEdgeInsets:titleInsets];
            [dropDownButton15 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),30)];
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value15_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton15 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton15 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton15.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton15 setTag:10];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value15_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value15_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value15_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton15 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value15_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value15_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value15_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value15_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton15.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton15.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton15.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton15.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton15.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    [dropDownButton15.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    [dropDownButton15.titleLabel setFont:[UIFont boldSystemFontOfSize:[fontSize floatValue]]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    [dropDownButton15.titleLabel setFont:[UIFont italicSystemFontOfSize:[fontSize floatValue]]];
                
                else
                    [dropDownButton15.titleLabel setFont:[UIFont systemFontOfSize:[fontSize floatValue]]];
            }
            [dropDownButton15 setExclusiveTouch:YES];
            dropDownButton15.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton15.frame.size.width-dropDownButton15.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton15.frame.size.width-dropDownButton15.intrinsicContentSize.width-8.0));
                [dropDownButton15 setTitleEdgeInsets:titleInsets];
                dropDownButton15.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton15 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton15];
            //            [[dropDownButton15 layer] setBorderWidth:0.5f];
            //            [[dropDownButton15 layer] setBorderColor:[UIColor blackColor].CGColor];
            [dropDownButton15 setBottomBorderColor:[UIColor blackColor]];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton15.frame.size.width-20,filed_Y_Position+15,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];
            
            next_Y_Position = distance_Y+dropDownButton15.frame.origin.y+dropDownButton15.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template1_drop_down_value15_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:@"" forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_label15_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton15.titleLabel.text forKey:@"value"];
        }
        [validationsArray addObject:tempDictionary];
    }
    
    //New UI Elements added by shri ends
    
    
    [childTemplate1ScrollView setContentSize:CGSizeMake(SCREEN_WIDTH-96, numberOfFields*(30+21+5)+40)];
    childTemplate1ScrollView.scrollEnabled=YES;
    
    /*
     * This method is used to add ChildTemplate1 Button1 and button2 both visibility true.
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_field11_visibilty",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ([NSLocalizedStringFromTableInBundle(@"child_template1_button1_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame && [NSLocalizedStringFromTableInBundle(@"child_template1_button2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            childButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
            [childButton1 setFrame:CGRectMake(6,self.frame.size.height-60,(self.frame.size.width/2)-10,50)];
            [childButton1 setCustomButtonType:ButtonTypeSubmit];
            /*
             * This method is used to add ChildTemplate1 button1 titlename.
             */
            NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (buttonStr)
                [childButton1 setTitle:buttonStr forState:UIControlStateNormal];
            
            /*
             * This method is used to add ChildTemplate1 button1 properties For Button backgroundColor.
             */
            // properties For Button backgroundColor
            NSArray *childbutton1Color;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_button1_background_color",propertyFileName,[NSBundle mainBundle], nil))
                childbutton1Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button1_background_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                childbutton1Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_default_button_background_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (childbutton1Color) {
//                childButton1.backgroundColor=[UIColor colorWithRed:[[childbutton1Color objectAtIndex:0] floatValue] green:[[childbutton1Color objectAtIndex:1] floatValue] blue:[[childbutton1Color objectAtIndex:2] floatValue] alpha:1.0f];
                childButton1.backgroundColor = [UIColor clearColor];
            }
            /*
             * This method is used to add ChildTemplate1 button1 text font attributes override.
             */
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_button1_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                /*
                 * This method is used to add ChildTemplate1 button1 Properties for Button textcolor.
                 */
                // Properties for Button TextColor.
                
                NSArray *childButton1TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    childButton1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    childButton1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    childButton1TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
                
                if (childButton1TextColor)
                    [childButton1 setTitleColor:[UIColor colorWithRed:[[childButton1TextColor objectAtIndex:0] floatValue] green:[[childButton1TextColor objectAtIndex:1] floatValue] blue:[[childButton1TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                /*
                 * This method is used to add ChildTemplate1 button1 Properties for Button textstyle.
                 */
                // Properties for Button Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_button_text_style;
                
                /*
                 * This method is used to add ChildTemplate1 button1 Properties for Button fontsize.
                 */
                // Properties for Button Font size.
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_button_text_size;
                
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childButton1.titleLabel.font =[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childButton1.titleLabel.font =[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childButton1.titleLabel.font =[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childButton1.titleLabel.font =[UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            else
            {
                /*
                 * This method is used to add ChildTemplate1 button1 Default Properties for Button textcolor.
                 */
                //Default Properties for Button textcolor
                NSArray *childButton1TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    childButton1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    childButton1TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
                if (childButton1TextColor)
                    [childButton1 setTitleColor:[UIColor colorWithRed:[[childButton1TextColor objectAtIndex:0] floatValue] green:[[childButton1TextColor objectAtIndex:1] floatValue] blue:[[childButton1TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                /*
                 * This method is used to add ChildTemplate1 button1 Default Properties for Button textstyle.
                 */
                //Default Properties for Button textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_button_text_style;
                /*
                 * This method is used to add ChildTemplate1 button1 Default Properties for Button fontSize.
                 */
                //Default Properties for Button fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize= application_default_button_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childButton1.titleLabel.font =[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childButton1.titleLabel.font =[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childButton1.titleLabel.font =[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childButton1.titleLabel.font =[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [childButton1 setTag:101];
            [childButton1 setExclusiveTouch:YES];
            [childButton1 addTarget:self action:@selector(childButton1Action:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:childButton1];
            
            childButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
            [childButton2 setFrame:CGRectMake((childButton1.frame.origin.x+childButton1.frame.size.width+9),self.frame.size.height-60,(self.frame.size.width/2)-10,50)];
            [childButton2 setCustomButtonType:ButtonTypeSubmit];
            
            
            NSString *buttonStr1=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template1_button2_text",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (buttonStr1)
                [childButton2 setTitle:buttonStr1 forState:UIControlStateNormal];
            
            // properties For Button backgroundColor
            NSArray *childButton2Color;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_button2_background_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton2Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button2_background_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                childButton2Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_default_button_background_color",propertyFileName,[NSBundle mainBundle], nil)];
            if (childButton2Color) {
                //childButton2.backgroundColor=[UIColor colorWithRed:[[childButton2Color objectAtIndex:0] floatValue] green:[[childButton2Color objectAtIndex:1] floatValue] blue:[[childButton2Color objectAtIndex:2] floatValue] alpha:1.0f];
                childButton2.backgroundColor = [UIColor clearColor];
            }
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_button2_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                /*
                 * This method is used to add ChildTemplate1 button2  Properties for Button TextColor.
                 */
                // Properties for Button TextColor.
                NSArray *childButton2TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_button2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    childButton2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    childButton2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    childButton2TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
                
                if (childButton2TextColor)
                    [childButton2 setTitleColor:[UIColor colorWithRed:[[childButton2TextColor objectAtIndex:0] floatValue] green:[[childButton2TextColor objectAtIndex:1] floatValue] blue:[[childButton2TextColor objectAtIndex:2] floatValue] alpha:1.0] forState:UIControlStateNormal];
                /*
                 * This method is used to add ChildTemplate1 button2  Properties for Button textstyle.
                 */
                // Properties for Button Textstyle.
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_button2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_button2_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_button_text_style;
                /*
                 * This method is used to add ChildTemplate1 button2  Properties for Button Font size.
                 */
                // Properties for Button Font size.
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_button2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_button2_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_button_text_size;
                
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childButton2.titleLabel.font =[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childButton2.titleLabel.font =[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childButton2.titleLabel.font =[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childButton2.titleLabel.font =[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to add ChildTemplate1 button2 Default Properties for Button textcolor.
                 */
                //Default Properties for Button textcolor
                NSArray *childButton2TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    childButton2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    childButton2TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
                if (childButton2TextColor)
                    [childButton2 setTitleColor:[UIColor colorWithRed:[[childButton2TextColor objectAtIndex:0] floatValue] green:[[childButton2TextColor objectAtIndex:1] floatValue] blue:[[childButton2TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                /*
                 * This method is used to add ChildTemplate1 button2 Default Properties for Button textstyle.
                 */
                //Default Properties for Button textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_button_text_style;
                
                /*
                 * This method is used to add ChildTemplate1 button2 Default Properties for Button text fontsize.
                 */
                //Default Properties for Button fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize= application_default_button_text_size;;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childButton2.titleLabel.font =[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childButton2.titleLabel.font =[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childButton2.titleLabel.font =[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childButton2.titleLabel.font =[UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            [childButton2 setTag:102];
            [childButton2 setExclusiveTouch:YES];
            [childButton2 addTarget:self action:@selector(childButton2Action:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:childButton2];
        }
        /*
         * This method is used to add ChildTemplate1 Button1 visibility true.
         */
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_button1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            childButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
            childButton1.frame = CGRectMake(10,self.frame.size.height-60, SCREEN_WIDTH-20, 40);

            [childButton1 setCustomButtonType:ButtonTypeSubmit];
            /*
             * This method is used to add ChildTemplate1 button1  Properties for Button backgroundColor.
             */
            // properties For Button backgroundColor
            NSArray *childButton1Color;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_button1_background_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton1Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button1_background_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                childButton1Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_default_button_background_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (childButton1Color) {
//                childButton1.backgroundColor=[UIColor colorWithRed:[[childButton1Color objectAtIndex:0] floatValue] green:[[childButton1Color objectAtIndex:1] floatValue] blue:[[childButton1Color objectAtIndex:2] floatValue] alpha:1.0f];
                childButton1.backgroundColor = [UIColor clearColor];
            }
            /*
             * This method is used to add ChildTemplate1 button1 title name.
             */
            NSString *buttonStr=[Localization languageSelectedStringForKey:(NSLocalizedStringFromTableInBundle(@"child_template1_button1_text",propertyFileName,[NSBundle mainBundle], nil))];
            
            if (buttonStr)
                [childButton1 setTitle:buttonStr forState:UIControlStateNormal];
            /*
             * This method is used to add ChildTemplate1 button1 text fontattributes override.
             */
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_button1_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                /*
                 * This method is used to add ChildTemplate1 button1  Properties for Button textcolor.
                 */
                // Properties for Button TextColor.
                
                NSArray *childButton1TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    childButton1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    childButton1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    childButton1TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
                
                if (childButton1TextColor)
                    [childButton1 setTitleColor:[UIColor colorWithRed:[[childButton1TextColor objectAtIndex:0] floatValue] green:[[childButton1TextColor objectAtIndex:1] floatValue] blue:[[childButton1TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                /*
                 * This method is used to add ChildTemplate1 button1  Properties for Button textstyle.
                 */
                // Properties for Button Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_button_text_style;
                
                /*
                 * This method is used to add ChildTemplate1 button1  Properties for Button fontsize.
                 */
                // Properties for Button Font size.
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_button_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childButton1.titleLabel.font =[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childButton1.titleLabel.font =[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childButton1.titleLabel.font =[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childButton1.titleLabel.font =[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to add ChildTemplate1 button1 Default Properties for Button textcolor.
                 */
                //Default Properties for Button textcolor
                NSArray *childButton1TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    childButton1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    childButton1TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
                if (childButton1TextColor)
                    [childButton1 setTitleColor:[UIColor colorWithRed:[[childButton1TextColor objectAtIndex:0] floatValue] green:[[childButton1TextColor objectAtIndex:1] floatValue] blue:[[childButton1TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                /*
                 * This method is used to add ChildTemplate1 button1 Default Properties for Button textstyle.
                 */
                //Default Properties for Button textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_button_text_style;
                
                /*
                 * This method is used to add ChildTemplate1 button1 Default Properties for Button fontSize.
                 */
                //Default Properties for Button fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize= application_default_button_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childButton1.titleLabel.font =[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childButton1.titleLabel.font =[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childButton1.titleLabel.font =[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childButton1.titleLabel.font =[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [childButton1 setTag:101];
            [childButton1 setExclusiveTouch:YES];
            [childButton1 addTarget:self action:@selector(childButton1Action:) forControlEvents:UIControlEventTouchUpInside];
            
            if([propertyFileName isEqualToString:@"MyTopUpsCT1_T1"]){
                childButton1.frame = CGRectMake(childButton1.frame.origin.x, childButton1.frame.origin.y+10, childButton1.frame.size.width, 50);
            }
            [self addSubview:childButton1];
        }
        /*
         * This method is used to add ChildTemplate1 Button2 visibility true.
         */
        else if ([NSLocalizedStringFromTableInBundle(@"child_template1_button2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            childButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
            
            if ([propertyFileName isEqualToString:@"CheckVelocityLimitsCT1_T3"]) {
                childButton2.frame = CGRectMake(label_X_Position, self.frame.size.height-60, self.frame.size.width-(label_X_Position*2), 50);
            }
            else {
                childButton2.frame = CGRectMake(10,self.frame.size.height-60, self.frame.size.width-(label_X_Position*4), 50);
                if([propertyFileName isEqualToString:@"CustomerCashinCT1_T1"]){
                    childButton2.frame = CGRectMake(10,self.frame.size.height-60, SCREEN_WIDTH-20, 40);
                }
            }
            [childButton2 setCustomButtonType:ButtonTypeSubmit];
            /*
             * This method is used to add ChildTemplate1 button2 title name.
             */
            NSString *buttonStr=[Localization languageSelectedStringForKey:(NSLocalizedStringFromTableInBundle(@"child_template1_button2_text",propertyFileName,[NSBundle mainBundle], nil))];
            
            if(buttonStr)
                [childButton2 setTitle:buttonStr forState:UIControlStateNormal];
            /*
             * This method is used to add ChildTemplate1 button2 Properties for Button backgroundColor.
             */
            // properties For Button backgroundColor
            NSArray *childButton2Color;
            if (NSLocalizedStringFromTableInBundle(@"child_template1_button2_background_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton2Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button2_background_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                childButton2Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_default_button_background_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (childButton2Color) {
                childButton2.backgroundColor=[UIColor   colorWithRed:[[childButton2Color objectAtIndex:0] floatValue] green:[[childButton2Color objectAtIndex:1] floatValue] blue:[[childButton2Color objectAtIndex:2] floatValue] alpha:1.0f];
                childButton2.backgroundColor = [UIColor clearColor];
            }
            /*
             * This method is used to add ChildTemplate1 button2 text fontattributes override.
             */
            if ([NSLocalizedStringFromTableInBundle(@"child_template1_button2_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                /*
                 * This method is used to add ChildTemplate1 button2 Properties for Button Texcolor.
                 */
                // Properties for Button TextColor.
                NSArray *childButton2TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template1_button2_background_color",propertyFileName,[NSBundle mainBundle], nil))
                    childButton2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    childButton2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    childButton2TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
                
                if (childButton2TextColor)
                    [childButton2 setTitleColor:[UIColor colorWithRed:[[childButton2TextColor objectAtIndex:0] floatValue] green:[[childButton2TextColor objectAtIndex:1] floatValue] blue:[[childButton2TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                /*
                 * This method is used to add ChildTemplate1 button2 Properties for Button Textstyle.
                 */
                // Properties for Button Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_button2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_button2_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_button_text_style;
                /*
                 * This method is used to add ChildTemplate1 button2 Properties for Button font size.
                 */
                // Properties for Button Font size.
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_button2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_button2_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_button_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childButton2.titleLabel.font =[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childButton2.titleLabel.font =[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childButton2.titleLabel.font =[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childButton2.titleLabel.font =[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to add ChildTemplate1 button2 Default Properties for Button textcolor.
                 */
                //Default Properties for Button textcolor
                NSArray *childButton2TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    childButton2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template1_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    childButton2TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
                if (childButton2TextColor)
                    [childButton2 setTitleColor:[UIColor colorWithRed:[[childButton2TextColor objectAtIndex:0] floatValue] green:[[childButton2TextColor objectAtIndex:1] floatValue] blue:[[childButton2TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                /*
                 * This method is used to add ChildTemplate1 button2 Default Properties for Button textStyle.
                 */
                //Default Properties for Button textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_button_text_style;
                
                /*
                 * This method is used to add ChildTemplate1 button2 Default Properties for Button fontSize.
                 */
                //Default Properties for Button fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize= application_default_button_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childButton2.titleLabel.font =[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childButton2.titleLabel.font =[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childButton2.titleLabel.font =[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childButton2.titleLabel.font =[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [childButton2 setTag:102];
            [childButton2 setExclusiveTouch:YES];
            [childButton2 addTarget:self action:@selector(childButton2Action:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:childButton2];
        }
    }
    /*
     * This method is used to add ChildTemplate1 activity indicator(progressbar).
     */
    activityIndicator = [[ActivityIndicator alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [[self superview] addSubview:activityIndicator];
    activityIndicator.hidden = YES;
    
    /*
     * This method is used to set ChildTemplate1 addpayee status.
     */
    if ([[valueDictionary valueForKey:@"addPayeeStatus"] integerValue] == 1) {
        for (int i=1; i<=10; i++) {
            NSString *propertyString = NSLocalizedStringFromTableInBundle(([NSString stringWithFormat:@"child_template1_value%d_param_type",i]),propertyFileName,[NSBundle mainBundle], nil);

            if ([valueDictionary objectForKey:propertyString]) {
                int tag = 100+(i-1);
                CustomTextField *txtField = (CustomTextField *)[self viewWithTag:tag];
                [txtField setText:[valueDictionary objectForKey:propertyString]];
            }
        }
        [valueDictionary setValue:0 forKey:@"addPayeeStatus"];
    }
    
    if (isNewUIUpdationNeeded){
        BackgroundViewTemplate1* backgroundView = [[BackgroundViewTemplate1 alloc] initWithFrame:CGRectMake(5, 15, childTemplate1ScrollView.bounds.size.width-10, numberOfFields*80)];
        [childTemplate1ScrollView insertSubview:backgroundView atIndex:0];
        
        CGSize contentSize = childTemplate1ScrollView.contentSize;
        contentSize.height = CGRectGetMaxY(backgroundView.frame) + 50;
        [childTemplate1ScrollView setContentSize:contentSize];
    }
    
    if ([NSLocalizedStringFromTableInBundle(@"child_template1_update_info_labels",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        [self updateInfoLabels];
    }
}


#pragma mark - Toolbar methods

/*
 * This method is used to set ChildTemplate1 toolbar cancel button action
 */
-(void)cancelNumberPad
{
    [activeField resignFirstResponder];
    activeField.text = @"";
}
/*
 * This method is used to set ChildTemplate1 toolbar Ok button action
 */
-(void)doneWithNumberPad
{
    if ([activeField isFirstResponder])
        [activeField resignFirstResponder];
    
    if ([activeField.inputView isKindOfClass:[UIDatePicker class]])
    {
        NSLog(@"%@",datePicker1.date);
        NSLog(@"%@",activeField.placeholder);
        
        [self datePickerValueChanged:(UIDatePicker *)activeField.inputView];
    }
    
    
}

#pragma mark Button methods
/*
 * This method is used to set ChildTemplate1 header button action(addcontact details).
 */
- (void)buttonAction:(id)sender
{
    NSLog(@"buttonAction:..");
    action_type = NSLocalizedStringFromTableInBundle(@"child_template1_header_label_action_type",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template1_header_label_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template1_header_label_next_template",propertyFileName,[NSBundle mainBundle], nil);
    
    Template *obj = [Template initWithactionType:action_type nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:propertyFileName ProcessorCode:nil Dictionary:nil contentArray:nil alertType:nil ValidationType:nil inClass:self withApiParameter:nil withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:nil withCurrentClass:CHILD_TEMPLATE_1];
    
    obj.target = self;
    obj.selector = @selector(dataUpdate:);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CT1_BUTTON2_ACTION object:obj];
    
}

/*
 * This method is used to set mainsegment action.
 */
-(void)mainSegmentControl:(id)sender{
    NSLog(@"selected segment : %ld", (long)[sender selectedSegmentIndex]);
}


/*
 * This method is used to set ChildTemplate1  button1 action.
 */
-(void)childButton1Action:(id)sender
{
    
    /*
     * This method is used to set ChildTemplate1 textfield keyboard wis disapper when user clicks button.
     */
    [activeField resignFirstResponder];
    if([propertyFileName isEqualToString:@"UploadDocumentCT1"]){
       selectedType = mainSegment.selectedSegmentIndex;
    }
    /*
     * This method is used to set ChildTemplate1 button1 action type.
     */
    action_type = NSLocalizedStringFromTableInBundle(@"child_template1_button1_action_type",propertyFileName,[NSBundle mainBundle], nil);
    /*
     * This method is used to set ChildTemplate1 fields validation type.
     */
    validation_Type = NSLocalizedStringFromTableInBundle(@"application_validation_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    /*
     * This method is used to set ChildTemplate1 button2 webservice api name(Processorcode and transactioncode).
     */
    data = NSLocalizedStringFromTableInBundle(@"child_template1_button1_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    processorCodeStr = [Template getProcessorCodeWithData:data];
    transactionType = [Template getTransactionCodeWithData:data];
    referenceParam = [Template getReferenceParameter:data];
    /*
     * This method is used to set ChildTemplate1 display alertview type(Ticker or popup).
     */
    alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    /*
     * This method is used to set ChildTemplate1 button1 nexttemplate properties file.
     */
    nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template1_button1_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    /*
     * This method is used to set ChildTemplate1 button1 nexttemplate.
     */
    nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template1_button1_next_template",propertyFileName,[NSBundle mainBundle], nil);
    
        NSLog(@"Property File Name is...%@,%@",nextTemplate,nextTemplateProperty);
    /*
     * This method is used to set ChildTemplate1 button1 fee calcaulation based on parameter type of fee.
     */
    paramType = nil;
    if (NSLocalizedStringFromTableInBundle(@"child_template1_button1_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil))
    {
        paramType = NSLocalizedStringFromTableInBundle(@"child_template1_button1_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil);
    }
    /*
     * This method is used to set ChildTemplate1 button1 transactiontype.
     */
    if (transactionType) {
        [datavalueDictionary setObject:transactionType forKey:PARAMETER13];
    }
    /*
     * This method is used to set ChildTemplate1 button1 referenceParam.
     */
    if (referenceParam) {
        [datavalueDictionary setObject:referenceParam forKey:PARAMETER38];
    }
    /*
     * This method is used to set ChildTemplate1 button1 processorCode.
     */
    [datavalueDictionary setObject:processorCodeStr forKey:PARAMETER15];
    
    
    currentClass = CHILD_TEMPLATE_1;
    buttonActionString = CT1_BUTTON1_ACTION;
    /*
     * This method is used to set ChildTemplate1 frame by using this method.
     */
    [self configureWithProcessorCode];
}

/*
 * This method is used to add ChildTemplate1 IFSC code to the particaular field.
 */
-(void) setIfsc
{
    NSUserDefaults *bankDefaults = [NSUserDefaults standardUserDefaults];
    ifscCode.text = [bankDefaults objectForKey:@"IFSC"];
    [bankDefaults setValue:@"" forKey:@"IFSC"];
    [bankDefaults synchronize];
}

/*
 * This method is used to set ChildTemplate1  button2 action.
 */
-(void)childButton2Action:(id)sender
{
    if([propertyFileName isEqualToString:@"RegistrationCT1_T1"]){
        NSString *msgString = @"";
        BOOL matchesLength = YES;
        NSInteger txtLength = textfield10.text.length;
        NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        if ([textfield10.text rangeOfCharacterFromSet:notDigits].location != NSNotFound)
        {
            PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@", [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]] andMessage:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_invalid_ID_number", nil)]] withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
            [self addSubview:popup];
            return;
        }
        if([docType isEqualToString:@"National ID"] && txtLength != 11){
            matchesLength = NO;
            msgString = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_national_id_length_mismatch", nil)]];
        }
        if([docType isEqualToString:@"Passport"] && txtLength != 8){
            matchesLength = NO;
            msgString = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_passport_length_mismatch", nil)]];
        }
        if([docType isEqualToString:@"Family Card"] && txtLength != 6){
            matchesLength = NO;
            msgString = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_family_card_length_mismatch", nil)]];
        }
        if(!matchesLength){
            PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]] andMessage:msgString withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
         [self addSubview:popup];
            return;
        }
    }

    if ([NSLocalizedStringFromTableInBundle(@"child_template1_validate_if_multi_docs_upload",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
//        SelectedDocType
        
        if([[selectedIDTypeDict objectForKey:@"typeName"] isEqualToString:@"Passport"]){
            //Passport requires only 1 image to upload, validate it here..
            if(!isImage1Obtained){
                PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]] andMessage:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_pls_add_images_to_upload", nil)]] withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                [[[self superview] superview] addSubview:popup];
                return;
            }
        }else{
            //            All other ID types require all the images to upload.
            if(!isImage1Obtained || !isImage2Obtained){
                PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]] andMessage:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_pls_add_images_to_upload", nil)] withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                [[[self superview] superview] addSubview:popup];
                return;
            }
        }
        // Validate for additional scanned copy to upload
        if(!isSignedRegistrationFormObtained){
            [mainSegment setSelectedSegmentIndex:2];
            PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]] andMessage:@"Please add Signed Registration Form to upload" withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
            [[[self superview] superview] addSubview:popup];
            return;
        }
    }

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:textfield1.text forKey:@"LastUsedMobileNumberForReg"];
    
    [[TemplateData sharedManager] setSelector:@selector(setIfsc)];
    [[TemplateData sharedManager] setTarget:self];
    
    /*
     * This method is used to set ChildTemplate1 textfield keyboard wis disapper when user clicks button.
     */
    [activeField resignFirstResponder];
    /*
     * This method is used to set ChildTemplate1 button2 action type.
     */
    action_type = NSLocalizedStringFromTableInBundle(@"child_template1_button2_action_type",propertyFileName,[NSBundle mainBundle], nil);
    /*
     * This method is used to set ChildTemplate1 fields validation type.
     */
    validation_Type = NSLocalizedStringFromTableInBundle(@"application_validation_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    /*
     * This method is used to set ChildTemplate1 button2 webservice api name(Processorcode and transactioncode).
     */
    data = NSLocalizedStringFromTableInBundle(@"child_template1_button2_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    processorCodeStr = [Template getProcessorCodeWithData:data];
    transactionType = [Template getTransactionCodeWithData:data];
    referenceParam = [Template getReferenceParameter:data];
    /*
     * This method is used to set ChildTemplate1 display alertview type(Ticker or popup).
     */
    alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    
    /*
     * This method is used to set ChildTemplate1 button2 nexttemplate properties file.
     */
    nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template1_button2_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    /*
     * This method is used to set ChildTemplate1 button2 nexttemplate.
     */
    nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template1_button2_next_template",propertyFileName,[NSBundle mainBundle], nil);
    paramType = nil;
    
    /*
     * This method is used to set ChildTemplate1 button2 fee calcaulation based on parameter type of fee.
     */
    if (NSLocalizedStringFromTableInBundle(@"child_template1_button2_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil))
    {
        paramType = NSLocalizedStringFromTableInBundle(@"child_template1_button2_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil);
    }
    /*
     * This method is used to set ChildTemplate1 button2 transactiontype.
     */
    if (transactionType) {
        [datavalueDictionary setObject:transactionType forKey:PARAMETER13];
    }
    /*
     * This method is used to set ChildTemplate1 button2 referenceParam.
     */
    if (referenceParam) {
        [datavalueDictionary setObject:referenceParam forKey:PARAMETER38];
    }
    
    
    /*
     * This method is used to set ChildTemplate1 button2 processorCode.
     */
    [datavalueDictionary setObject:processorCodeStr forKey:PARAMETER15];
    
    currentClass = CHILD_TEMPLATE_1;
    buttonActionString = CT1_BUTTON2_ACTION;
    /*
     * This method is used to set ChildTemplate1 frame by using this method.
     */
    [self configureWithProcessorCode];
}

/*
 * This method is used to add ChildTemplate1 dropdown button action.
 */
-(void)selectDropDown:(id)sender
{
    NSLog(@"selected drop down.. %@", sender);
    [self endEditing:YES];
    btn = (UIButton *)sender;

    if([propertyFileName isEqualToString:@"RegistrationCT1_T1"] && btn.tag == 13){
        if(!districts){
            PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]] andMessage:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_hint_governorate", nil)]] withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
            [self addSubview:popup];
            return;
        }
    }

    dropdownString = [NSString stringWithFormat:@"child_template1_drop_down_value%ld",(long)btn.tag];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData:) name:@"UpdateData" object:nil];
    DatabaseManager *dataManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
    if([dropdownString isEqualToString:@"child_template1_drop_down_value12"]){
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
        {
            dropDownDataArray = [[NSArray alloc] initWithArray:[self getGovernorates]];
        }else{
            dropDownDataArray = [[NSArray alloc] initWithArray:[self getGovernorates]];
        }
    }else if([dropdownString isEqualToString:@"child_template1_drop_down_value13"]){
        NSLog(@"Adding districts..");
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
        {
            dropDownDataArray = [[NSArray alloc] initWithArray:[self getDistricts]];
        }else{
            dropDownDataArray = [[NSArray alloc] initWithArray:[self getDistricts]];
        }
    }else{
        dropDownDataArray = [[NSArray alloc] initWithArray:[dataManager getAllDropDownDetailsForKey:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_data_webservice_name"],propertyFileName,[NSBundle mainBundle], nil)]];
    }

    nextTemplateProperty =  NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template_properties_file"],propertyFileName,[NSBundle mainBundle], nil);
    nextTemplate = NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template"],propertyFileName,[NSBundle mainBundle], nil);
    
    if ([NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template"],propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"1"]){
        nextTemplate=CHILD_TEMPLATE_3;
    }
    else if ([NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template"],propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"2"]){
        nextTemplate=POPUP_TEMPLATE_8;
    }
    
        if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
        {
            Class nextClass = NSClassFromString(nextTemplate);
            id object = nil;
            
            if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)])
            {
                object = [[nextClass alloc] initWithNibName:nextTemplate bundle:nil withPropertyName:nextTemplateProperty hasDidSelectFunction:NO withDataDictionary:nil withDataArray:dropDownDataArray withPIN:nil withProcessorCode:nil withType:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_data_webservice_name"],propertyFileName,[NSBundle mainBundle], nil) fromView:0 insideView:nil];
                [(ChildTemplate3 *)object setBaseSelector:@selector(updateData:)];
                [(ChildTemplate3 *)object setBasetarget:self];
                AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                UINavigationController *navCntrl = (UINavigationController *)delegate.window.rootViewController;
                if (navCntrl && [navCntrl isKindOfClass:[UINavigationController class]])
                {
                    [navCntrl pushViewController:object animated:NO];
                }
            }
            else if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray:withSubIndex:)])
            {
                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:0 withDataArray:dropDownDataArray?dropDownDataArray:nil withSubIndex:0];
                [object setBasetarget:self];
                [object setBaseSelector:@selector(updateData:)];
                [[[self superview] superview] addSubview:(UIView *)object];
            }
        }
}

/*
 * This method is used to add ChildTemplate1 Localdelegate for communicates one vie to another view.
 */
-(void)setLocalDelegate:(id)fromDelegate
{
}

#pragma mark - Update Contacts Data.
/*
 * This method is used to add ChildTemplate1 dropdown updated data to particular field.
 */
- (void)updateData:(id)object
{
    if ([[object objectForKey:DROP_DOWN_TYPE] isEqualToString:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_data_webservice_name"],propertyFileName,[NSBundle mainBundle], nil)])
    {
        NSString *dropDownValue = [object objectForKey:DROP_DOWN_TYPE_DESC];
        if (dropDownValue.length == 0) {
            dropDownValue = [object objectForKey:DROP_DOWN_TYPE_NAME];
        }
        
        [btn setTitle:dropDownValue forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setTitle:dropDownValue forState:UIControlStateHighlighted];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        
        NSString *typeName = [NSString stringWithFormat:@"%@_typeName",[object objectForKey:DROP_DOWN_TYPE]];
        NSString *typeDesc = [NSString stringWithFormat:@"%@_typeDesc",[object objectForKey:DROP_DOWN_TYPE]];
        
        if ([object objectForKey:@"typeName"]) {
            [datavalueDictionary setObject:[object objectForKey:@"typeName"] forKey:typeName];
        }
        
        if ([object objectForKey:@"typeDesc"]) {
            [datavalueDictionary setObject:[object objectForKey:@"typeDesc"] forKey:typeDesc];
        }
        
        [datavalueDictionary setObject:[object objectForKey:DROP_DOWN_TYPE_NAME] forKey:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_param_type"],propertyFileName,[NSBundle mainBundle], nil)];
    }

}
/*
 * This method is used to add ChildTemplate1 get addressbook contact details data.
 */
-(void) dataUpdate:(id)object
{
    ABRecordRef person = (__bridge ABRecordRef)(object);
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    NSString* firstName = (__bridge_transfer NSString*)ABRecordCopyValue(person,kABPersonFirstNameProperty);
    
    NSString* lastName = (__bridge_transfer NSString*)ABRecordCopyValue(person,kABPersonLastNameProperty);
    
    ABMultiValueRef phoneNumbers = ABRecordCopyValue(person,kABPersonPhoneProperty);
    NSString *phone = nil;
    
    if (ABMultiValueGetCount(phoneNumbers) > 0)
    {
        phone = (__bridge_transfer NSString*)ABMultiValueCopyValueAtIndex(phoneNumbers, 0);
    }
    CFRelease(phoneNumbers);
    
    if(phone && phone.length > 0)
    {
        phone = [[phone componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
    }
    
    NSString *inputTypeString = nil;
    for (int i = 1; i <= 10; i++)
    {
        inputTypeString = [NSString stringWithFormat:@"child_template1_value%d_param_type",i];
        
        if([NSLocalizedStringFromTableInBundle(inputTypeString,propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Reference2"] || [NSLocalizedStringFromTableInBundle(inputTypeString,propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"FirstName"])
        {
            if(firstName && firstName.length > 0)
                [dict setObject:firstName forKey:[NSString stringWithFormat:@"%d",i]];
        }
        else if ([NSLocalizedStringFromTableInBundle(inputTypeString,propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Reference3"] || [NSLocalizedStringFromTableInBundle(inputTypeString,propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"LastName"])
        {
            if (lastName && lastName.length > 0)
                [dict setObject:lastName forKey:[NSString stringWithFormat:@"%d",i]];
        }
        else if ([NSLocalizedStringFromTableInBundle(inputTypeString,propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"Reference4"] || [NSLocalizedStringFromTableInBundle(inputTypeString,propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"MyNumber"])
        {
            if(phone && phone.length > 0)
                [dict setObject:phone forKey:[NSString stringWithFormat:@"%d",i]];
        }
        
        if(dict.count == 3)
        {
            break;
        }
    }
    
    if (dict.count > 0)
    {
        CustomTextField *field = nil;
        for (NSString *key in dict)
        {
            int tag = 100 + (key.intValue - 1);
            field = (CustomTextField *)[self viewWithTag:tag];
            if(field && [field isKindOfClass:[UITextField class]])
                [field setText:[dict objectForKey:key]];
        }
    }
}


#pragma  mark - Text field Delegate Methods.
/*
 * This method is used to set ChildTemplate1 textfield delegate methods.
 */
- (void)textFieldDidBeginEditing:(CustomTextField *)textField
{
    activeField = textField;
    CGPoint scrollPoint;
    
    if(![presentingViewCode isEqualToString:PROCESSOR_CODE_CHECK_MY_VELOCITY])
        scrollPoint= CGPointMake(0, textField.frame.origin.y-20);
    
    else
        scrollPoint = CGPointMake(0, textField.frame.origin.y-35);
    
    [childTemplate1ScrollView setContentOffset:scrollPoint animated:YES];
}

- (void)textFieldDidEndEditing:(CustomTextField *)textField
{
//    [self moveScrollToCurrent:textField.frame];
    [textField resignFirstResponder];
    if(![propertyFileName isEqualToString:@"RegistrationCT1_T1"]){
        [childTemplate1ScrollView setContentOffset:CGPointZero animated:YES];
    }
}

- (BOOL)textFieldShouldBeginEditing:(CustomTextField *)textField
{
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    
    if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
    {
        textField.textAlignment=NSTextAlignmentRight;
    }
    else
    {
        textField.textAlignment=NSTextAlignmentLeft;
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(CustomTextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(CustomTextField *)textField
{
    return YES;
}

#pragma mark - Date picker method.
/**
 * This method is used For open date picker.
 */
-(void) datePickerValueChanged:(id)sender
{
    NSLog(@"datePickerValueChanged..");
    UIDatePicker *picker = (UIDatePicker*)sender;
    int viewTag = (int)picker.tag;
    NSLog(@"View Tag is....%d",viewTag);
    
    //remove maximum date for ID expiry date field
    if(viewTag != 108){
        [picker setMaximumDate:[NSDate date]];
    }

    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    [dateFormat setDateFormat:SETDATEFORMATDATEMONTHYEAR];
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    [(CustomTextField *)[childTemplate1ScrollView viewWithTag:viewTag] setText:dateString];
}

-(void)getGovernoratesFromWebService
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [reachability  currentReachabilityStatus];
    if (netStatus == NotReachable) {
        NSLog(@"Alert : No internet..");
    }
    else
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        NSString *processor_Code = PROCESSOR_CODE_GET_GOVERNORATES;
        NSString *transactionType = TRANSACTION_CODE_GET_GOVERNORATES;
        if (transactionType) {
            [dict setObject:transactionType forKey:PARAMETER13];
        }
        if (processor_Code) {
            [dict setObject:processor_Code forKey:PARAMETER15];
        }
        WebSericeUtils *webUtils = [[WebSericeUtils alloc] init];
        NSMutableDictionary *webUtilsValues = [[NSMutableDictionary alloc] initWithDictionary:[webUtils getGovernoratesBundle:dict]];
        WebServiceRequestFormation *webServiceRequest = [[WebServiceRequestFormation alloc] init];
        NSString *webRequest  = [webServiceRequest sendRequest:webUtilsValues];
        WebServiceRunning *webServiceRun = [[WebServiceRunning alloc] init];
        [webServiceRun startWebServiceWithRequest:webRequest withReqDictionary:webUtilsValues withDelegate:self];
    }
    
}

-(NSArray*)getGovernorates{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    if(governoratesDict){
        NSString *keys[4];
        keys[0] = @"_id";
        keys[1] = @"drop_down_type";
        keys[2] = @"typeDesc";
        keys[3] = @"typeName";
        int i = 8324;
        for (NSString *str in governorates) {
            NSString *values[4];
            values[0] = [NSString stringWithFormat:@"%d", i];
            values[1] = @"STATE";
            values[2] = str;
            values[3] = str;
            NSDictionary *dict = [NSDictionary dictionaryWithObjects:values forKeys:keys count:4];
            [arr addObject:dict];
        }
    }
    return arr;
}

-(NSArray*)getDistricts{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    NSString *keys[4];
    keys[0] = @"_id";
    keys[1] = @"drop_down_type";
    keys[2] = @"typeDesc";
    keys[3] = @"typeName";
    
    if(districts){
        int i = 8373;
        for (NSString* str in districts) {
            NSString *values1[4];
            values1[0] = [NSString stringWithFormat:@"%d", i];
            values1[1] = @"STATE";
            values1[2] = str;
            values1[3] = str;
            NSDictionary *dist = [NSDictionary dictionaryWithObjects:values1 forKeys:keys count:4];
            [arr addObject:dist];
        }
    }
    return arr;
}

- (void)pickerObtainedImage:(NSNotification *)notification{
    NSLog(@"Obtained image..");
    NSDictionary *imageDict = notification.userInfo;
    if(selectedType == 0){
        isImage1Obtained = true;
        [[NSUserDefaults standardUserDefaults] setObject:[imageDict objectForKey:@"base64ImageData"] forKey:@"UploadImageOne"];
//        [datavalueDictionary setObject:[imageDict objectForKey:@"base64ImageData"] forKey:PARAMETER19];
//        [datavalueDictionary setObject:imageDict forKey:PARAMETER19];
    }else if(selectedType == 1){
        isImage2Obtained = true;
        [[NSUserDefaults standardUserDefaults] setObject:[imageDict objectForKey:@"base64ImageData"] forKey:@"UploadImageTwo"];
//        [datavalueDictionary setObject:[imageDict objectForKey:@"base64ImageData"] forKey:PARAMETER20];
//        [datavalueDictionary setObject:imageDict forKey:PARAMETER20];
    }else if(selectedType == 2){
        isSignedRegistrationFormObtained = true;
//        [datavalueDictionary setObject:[imageDict objectForKey:@"base64ImageData"] forKey:PARAMETER34];
        [[NSUserDefaults standardUserDefaults] setObject:[imageDict objectForKey:@"base64ImageData"] forKey:@"UploadImageThree"];
//        [datavalueDictionary setObject:imageDict forKey:PARAMETER34];
    }
    [self updateInfoLabels];
}

-(void)idTypeSelected:(NSNotification *)notification{
    isImage1Obtained = false;
    isImage2Obtained = false;
    isSignedRegistrationFormObtained = false;
    infoLabel2.text = @"";
    infoLabel3.text = @"";
    
    NSDictionary *dict = notification.userInfo;
    if([[dict objectForKey:@"drop_down_type"] isEqualToString:@"IDENTIFICATION"]){
        docType = [dict objectForKey:@"typeName"];
    }
    if([propertyFileName isEqualToString:@"RegistrationCT1_T1"]){
        NSString *govt = [dict objectForKey:@"typeName"];
        if([governoratesDict objectForKey:govt]){
            NSLog(@"Selected governorate %@", govt);
            districts = [governoratesDict objectForKey:govt];
        }
    }
    
    if([[notification.userInfo objectForKey:@"drop_down_type"] isEqualToString:@"IDENTIFICATION"]){
        NSLog(@"selected doc : %@", notification.userInfo);
        [[NSUserDefaults standardUserDefaults] setObject:[notification.userInfo objectForKey:@"typeName"]  forKey:@"SelectedDocType"];
    }
    [mainSegment setSelectedSegmentIndex:0];
    [mainSegment setHidden:FALSE];
    selectedIDTypeDict = notification.userInfo;
    [self updateInfoLabels];
    //    [button setTitle:@"0 of 2 documents added" forState:UIControlStateNormal];
}

-(void)updateInfoLabels{
    [mainSegment setSelectedSegmentIndex:0];
    [mainSegment setEnabled:YES forSegmentAtIndex:1];
    int i = 0;
    if(isImage1Obtained){
        i++;
    }
    if(isImage2Obtained){
        i++;
    }
    if(isSignedRegistrationFormObtained){
        i++;
    }
    NSString *str = [NSString stringWithFormat:@"%d of 3 documents added", i];
    if([[selectedIDTypeDict objectForKey:@"typeName"] isEqualToString:@"Family Card"]){
        [mainSegment setTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_image_page", nil)] forSegmentAtIndex:0];
        [mainSegment setTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_detail_page", nil)] forSegmentAtIndex:1];
        if(isImage1Obtained && isImage2Obtained){
            infoLabel1.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_tap_upload", nil)];
            infoLabel2.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_tick_added_fc_image_page", nil)];
            infoLabel3.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_tick_added_fc_detail_page", nil)];
        }
        if(isImage1Obtained && !isImage2Obtained){
            infoLabel1.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_add_image_fc_detail_page", nil)];
            infoLabel2.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_added_fc_image_page", nil)];
            [mainSegment setSelectedSegmentIndex:1];
        }
        if(!isImage1Obtained && isImage2Obtained){
            infoLabel1.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_pls_add_pic_fc_image_page", nil)];
            infoLabel3.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_added_fc_detail_page", nil)];
        }
        if(!isImage1Obtained && !isImage2Obtained){
            infoLabel1.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_pls_add_pic_fc_image_and_detail_page", nil)];
        }
    }else if([[selectedIDTypeDict objectForKey:@"typeName"] isEqualToString:@"FOREIGN PASSPORT"]){
        [mainSegment setTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_detail_page", nil)] forSegmentAtIndex:0];
        [mainSegment setTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_visa_page", nil)] forSegmentAtIndex:1];
        if(isImage1Obtained && isImage2Obtained){
            infoLabel1.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_tap_upload", nil)];
            infoLabel2.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_added_passport_details_page_image", nil)];
            infoLabel3.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_added_visa_page", nil)];
        }
        if(isImage1Obtained && !isImage2Obtained){
            infoLabel1.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_pls_add_image_pass_visa_page", nil)];
            infoLabel2.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_added_pass_detail_page_image", nil)];
            [mainSegment setSelectedSegmentIndex:1];
        }
        if(!isImage1Obtained && isImage2Obtained){
            infoLabel1.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_pls_add_pass_detail_page", nil)];
            infoLabel2.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_added_visa_image", nil)];
        }
        if(!isImage1Obtained && !isImage2Obtained){
            infoLabel1.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_pls_add_images_pass_dp_vp", nil)];
        }
    }else if([[selectedIDTypeDict objectForKey:@"typeName"] isEqualToString:@"National ID"]){
        [mainSegment setTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_front_page", nil)] forSegmentAtIndex:0];
        [mainSegment setTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_back_page", nil)] forSegmentAtIndex:1];
        if(isImage1Obtained && isImage2Obtained){
            infoLabel1.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_tap_upload", nil)];
            infoLabel2.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_added_page1_image", nil)];
            infoLabel3.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_added_page2_image", nil)];
        }
        if(isImage1Obtained && !isImage2Obtained){
            infoLabel1.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_pls_add_national_id_page2", nil)];
            infoLabel2.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_added_page1_image", nil)];
            [mainSegment setSelectedSegmentIndex:1];
        }
        if(!isImage1Obtained && isImage2Obtained){
            infoLabel1.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_pls_add_national_id_page1", nil)];
            infoLabel2.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_added_page2_image", nil)];
        }
        if(!isImage1Obtained && !isImage2Obtained){
            infoLabel1.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_pls_add_imgs_national_id_page1_2", nil)];
        }
    }else if([[selectedIDTypeDict objectForKey:@"typeName"] isEqualToString:@"Passport"]){
        [mainSegment setEnabled:NO forSegmentAtIndex:1];
        //        [mainSegment setHidden:TRUE];
        if(!isImage1Obtained){
            infoLabel1.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_pls_add_img_pass_detail_page", nil)];
        }
        if(isImage1Obtained){
            infoLabel1.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_tap_upload", nil)];
            infoLabel2.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_added_pass_detail_page", nil)];
            if(!isSignedRegistrationFormObtained){
                infoLabel1.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_pls_add_sr_form", nil)];
                [mainSegment setSelectedSegmentIndex:2];
            }
        }
        str = [NSString stringWithFormat:@"%d of 2 documents added", i];
    }
    if(isImage1Obtained && isImage2Obtained){
        if(!isSignedRegistrationFormObtained){
            infoLabel1.text = [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_pls_add_sr_form", nil)];
            [mainSegment setTitle:@"" forSegmentAtIndex:1];
            [mainSegment setSelectedSegmentIndex:2];
        }
    }
    [button setTitle:str forState:UIControlStateNormal];
}

- (void)updateGovernorates:(NSNotification *)notification{
    NSString * jsonString = [[NSUserDefaults standardUserDefaults] objectForKey:@"GovernoratesDict"];
    NSStringEncoding  encoding = NSUTF8StringEncoding;
    NSData * jsonData = [jsonString dataUsingEncoding:encoding];
    NSError * error=nil;
    NSMutableDictionary * parsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    governoratesDict = parsedData;
    governorates = [governoratesDict allKeys];
}


-(void)moveScrollToCurrent:(CGRect)rect{
    NSLog(@"moving scroll to rect..");
//    [self.childTemplate1ScrollView scrollRectToVisible:CGRectMake(0, 320, rect.size.width, rect.size.height) animated:NO];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
