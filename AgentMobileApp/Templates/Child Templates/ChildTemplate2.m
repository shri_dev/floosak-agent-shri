//  ChildTemplate2.m
//  Consumer Client

//  Created by Integra Micro on 19/04/15.
//  Copyright (c) 2015 Integra. All rights reserved.


#import "ChildTemplate2.h"
#import "Constants.h"
#import "ValidationsClass.h"
#import "UIView+Toast.h"
#import "PopUpTemplate2.h"
#import "DatabaseConstatants.h"
#import "WebSericeUtils.h"
#import "WebServiceConstants.h"
#import "WebServiceRequestFormation.h"
#import "WebServiceDataObject.h"
#import "NotificationConstants.h"
#import "AppDelegate.h"
#import "ParentTemplate7.h"
#import "Localization.h"
#import "Template.h"
#import "AGPushNoteView.h"

@implementation ChildTemplate2
{
    NSString *transactionType;
}

@synthesize delegate,propertyFileName;
@synthesize array1,childButton;
@synthesize main_searchBar,childTemplate2_TableView,filteredArray,usernameLbl;
@synthesize keyLabel,valueLabel,alertLabel,borderLabel;
@synthesize isFiltered;

#pragma mark - ChildTemplate2 UIView.
/**
 * This method is used to set implemention of childTemplate2.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView
{
    
    NSLog(@"ProcessorCode in %@ : %@", propertyFile, processorCode);
    NSLog(@"PropertyFileName: %@", propertyFile);
    dataDict = [[NSMutableDictionary alloc] initWithDictionary:dataDictionary];
    self = [self initWithFrame:frame];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ct2SetValue:) name:CHILD_TEMPLATE2 object:nil];
        propertyFileName = propertyFile;
        hasSelectionOperation = YES;
        
        
        if (!processorCode) {
            /*
             * This method is used to add Childtemplate2 list data to load webserviceapi.
             */
            NSString *dataValue =  NSLocalizedStringFromTableInBundle(@"child_template2_list_data_to_load_webservice_api",propertyFileName,[NSBundle mainBundle], nil);
            /*
             * This method is used to add Childtemplate2 list data to load webserviceapi- Feature processorCode.
             */
            local_processorCode=[Template getProcessorCodeWithData:dataValue];
            /*
             * This method is used to add Childtemplate2 list data to load webserviceapi- Feature TransactionType.
             */
            transactionType = [Template getTransactionCodeWithData:dataValue];
        }
        else
            local_processorCode = processorCode;
        
        dataArray = [[NSMutableArray alloc] initWithArray:dataDictsArray];
        
        [self addControlesToView];
    }
    
    
    
    return self;
}

#pragma mark -  ChildTemplate2 UIConstraints creation.
/**
 * This method is used to set add UIconstraints Frame of ChildTemplate2.
 *@param Type- Label,Text field,TableView and Button
 * Set Text (Size,color and font size).
 */
-(void)addControlesToView
{
    activityIndicator=[[ActivityIndicator alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    
    // Child template TableView.
    childTemplate2_TableView = [[UITableView alloc] init];
    childTemplate2_TableView.delegate = self;
    childTemplate2_TableView.dataSource = self;
    [childTemplate2_TableView setTableFooterView:[UIView new]];
    //    childTemplate2_TableView.allowsSelection = true;
    childTemplate2_TableView.contentInset = UIEdgeInsetsMake(0, 0, 44, 0);
    childTemplate2_TableView.separatorColor = [UIColor clearColor];
    childTemplate2_TableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if([NSLocalizedStringFromTableInBundle(@"child_template2_list_longpress_action_type",propertyFileName,[NSBundle mainBundle], nil) rangeOfString:@"_"].location == NSNotFound)
    {
        UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc]init];
        longTap.minimumPressDuration = 1.0;
        longTap.delegate=self;
        [longTap addTarget:self action:@selector(cellLongTapped:)];
        [self.childTemplate2_TableView addGestureRecognizer:longTap];
    }
    [self addSubview:self.childTemplate2_TableView];
    
    // Alert Label
    alertLabel = [[UILabel alloc] init];
    nextY_position = 0.0;
    height_Position = self.frame.size.height;
    
    // searcH bar
    if ([NSLocalizedStringFromTableInBundle(@"child_template2_value1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        main_searchBar = [[UISearchBar alloc] init];
        main_searchBar.frame = CGRectMake(0,nextY_position,self.frame.size.width,45);
        
        main_searchBar.placeholder=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template2_value1_hint",propertyFileName,[NSBundle mainBundle], nil)];
        
        if([NSLocalizedStringFromTableInBundle(@"child_template2_value1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for textField TextColor.
            NSArray *textField1TextColor;
            if (NSLocalizedStringFromTableInBundle(@"child_template2_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                textField1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template2_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"child_template2_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                textField1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template2_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                textField1TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
            
            if (textField1TextColor)
                main_searchBar.tintColor = [UIColor colorWithRed:[[textField1TextColor objectAtIndex:0] floatValue] green:[[textField1TextColor objectAtIndex:1] floatValue] blue:[[textField1TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for textField Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template2_value1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"child_template2_value1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template2_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"child_template2_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_value_text_style;
            
            // Properties for textField font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template2_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template2_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template2_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize =NSLocalizedStringFromTableInBundle(@"child_template2_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                [[UITextField appearanceWhenContainedIn:[main_searchBar class], nil] setDefaultTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:[fontSize floatValue]],}];
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                [[UITextField appearanceWhenContainedIn:[main_searchBar class], nil] setDefaultTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:[fontSize floatValue]],}];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                [[UITextField appearanceWhenContainedIn:[main_searchBar class], nil] setDefaultTextAttributes:@{NSFontAttributeName: [UIFont italicSystemFontOfSize:[fontSize floatValue]],}];
            else
                [[UITextField appearanceWhenContainedIn:[main_searchBar class], nil] setDefaultTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:[fontSize floatValue]],}];
        }
        else
        {
            //Default Properties for textfiled textcolor
            
            NSArray *textField1TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template2_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                textField1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template2_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                textField1TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
            if (textField1TextColor)
                main_searchBar.tintColor = [UIColor colorWithRed:[[textField1TextColor objectAtIndex:0] floatValue] green:[[textField1TextColor objectAtIndex:1] floatValue] blue:[[textField1TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for textfiled textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template2_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"child_template2_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_text_style;
            
            // Default Properties for textField font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template2_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize =NSLocalizedStringFromTableInBundle(@"child_template2_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                [[UITextField appearanceWhenContainedIn:[main_searchBar class], nil] setDefaultTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:[fontSize floatValue]],}];
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                [[UITextField appearanceWhenContainedIn:[main_searchBar class], nil] setDefaultTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:[fontSize floatValue]],}];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                [[UITextField appearanceWhenContainedIn:[main_searchBar class], nil] setDefaultTextAttributes:@{NSFontAttributeName: [UIFont italicSystemFontOfSize:[fontSize floatValue]],}];
            else
                [[UITextField appearanceWhenContainedIn:[main_searchBar class], nil] setDefaultTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:[fontSize floatValue]],}];
        }
        
        // property for KeyBoardType
        NSString *keyboardType;
        if (NSLocalizedStringFromTableInBundle(@"child_template2_value1_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
            keyboardType = NSLocalizedStringFromTableInBundle(@"child_template2_value1_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
        else
            keyboardType = application_default_value_keyboard_type;
        
        if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
            main_searchBar.keyboardType = UIKeyboardTypeDefault;
        else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
            main_searchBar.keyboardType = UIKeyboardTypeDecimalPad;
        else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
            main_searchBar.keyboardType = UIKeyboardTypePhonePad;
        else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3])
            main_searchBar.keyboardType = UIKeyboardTypeNumberPad;
        else
            main_searchBar.keyboardType = UIKeyboardTypeDefault;
        
        main_searchBar.delegate = self;
        [main_searchBar setTag:1];
        [self addSubview:main_searchBar];
        
        nextY_position = main_searchBar.frame.origin.y+main_searchBar.frame.size.height;
        int pageHeaderPos=64;
        int buttonpos=55;
        int searchbarpos=45;
        
        height_Position = SCREEN_HEIGHT-((pageHeaderPos+buttonpos+searchbarpos)-nextY_position);
    }
    // Data For List View.
    if ([NSLocalizedStringFromTableInBundle(@"child_template2_list_data_source_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:LOCAL_DATA])
    {
        childTemplate2_TableView.frame = CGRectMake(0, nextY_position+5, SCREEN_WIDTH, height_Position-70);
        alertLabel.hidden=YES;
        dataManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
        NSArray *tempArray = [[NSArray alloc] initWithArray:[dataManager getAllDropDownDetails]];
        
        if ([tempArray count]>0)
        {
            [BaseViewController getDataFromProcessoreCode:local_processorCode andPropertyFileName:propertyFileName forArray:&dataArray];
            
            if ([dataArray count]>0)
            {
                childTemplate2_TableView.frame = CGRectMake(0, nextY_position+5, SCREEN_WIDTH , height_Position-70);
                alertLabel.hidden = YES;
            }
            else
            {
                childTemplate2_TableView.hidden=YES;
                [childTemplate2_TableView removeFromSuperview];
                alertLabel.frame = CGRectMake(10, (SCREEN_HEIGHT/3),self.frame.size.width-20, 50);
                alertLabel.text=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template2_recent_transactions_not_available",propertyFileName,[NSBundle mainBundle], nil)];
                alertLabel.textAlignment = NSTextAlignmentCenter;
                alertLabel.numberOfLines = 2;
                alertLabel.hidden = NO;
                [self addSubview:alertLabel];
            }
        }
        else
        {
            childTemplate2_TableView.hidden=YES;
            [childTemplate2_TableView removeFromSuperview];
            alertLabel.frame = CGRectMake(10, (SCREEN_HEIGHT/3),self.frame.size.width-20, 50);
            alertLabel.text=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template2_recent_transactions_not_available",propertyFileName,[NSBundle mainBundle], nil)];
            alertLabel.textAlignment = NSTextAlignmentCenter;
            alertLabel.numberOfLines = 2;
            alertLabel.hidden = NO;
            [self addSubview:alertLabel];
        }
    }
    else if ([NSLocalizedStringFromTableInBundle(@"child_template2_list_data_source_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:REMOTE_DATA] && [local_processorCode isEqualToString:PROCESSOR_CODE_VIEW_PAYEE])
    {
        NSString *profileID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"] objectForKey:PARAMETER36];
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus netStatus = [reachability  currentReachabilityStatus];
        if (![profileID isEqualToString:@"SMS"] || (netStatus == NotReachable))
        {
            NSMutableDictionary *webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
            [webServiceRequestInputDetails setObject:local_processorCode forKey:PARAMETER15];
            [webServiceRequestInputDetails setObject:transactionType forKey:PARAMETER13];
            [self commonWebServiceCallWithData:webServiceRequestInputDetails andProcessorCode:local_processorCode];
        }
    }
    else if ([NSLocalizedStringFromTableInBundle(@"child_template2_list_data_source_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:REMOTE_DATA] && [local_processorCode isEqualToString:PROCESSOR_CODE_CASHOUT_BANK_LIST])
    {
        [activityIndicator startActivityIndicator];
        [self bringSubviewToFront:activityIndicator];
        activityIndicator.hidden=NO;
        
        NSLog(@"Fetch Remote Data ");
        NSString *profileID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"] objectForKey:PARAMETER36];
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus netStatus = [reachability  currentReachabilityStatus];
        if (![profileID isEqualToString:@"SMS"] || (netStatus==NotReachable))
        {
            
            NSMutableDictionary *webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
            [webServiceRequestInputDetails setObject:local_processorCode forKey:PARAMETER15];
            [webServiceRequestInputDetails setObject:transactionType forKey:PARAMETER13];
            [self commonWebServiceCallWithData:webServiceRequestInputDetails andProcessorCode:local_processorCode];
        }
    }
    else if ([NSLocalizedStringFromTableInBundle(@"child_template2_list_data_source_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:REMOTE_DATA] && [local_processorCode isEqualToString:PROCESSOR_CODE_AGGREGATOR_LINKED_BANK_LIST])
    {
        NSLog(@"Fetch Remote Data ");
        NSString *profileID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"] objectForKey:PARAMETER36];
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus netStatus = [reachability  currentReachabilityStatus];
        
        if (![profileID isEqualToString:@"SMS"] || (netStatus==NotReachable))
        {
            NSMutableDictionary *webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
            [webServiceRequestInputDetails addEntriesFromDictionary:dataDict];
            [webServiceRequestInputDetails setObject:local_processorCode forKey:PARAMETER15];
            [webServiceRequestInputDetails setObject:transactionType forKey:PARAMETER13];
            [self commonWebServiceCallWithData:webServiceRequestInputDetails andProcessorCode:local_processorCode];
        }
    }
    else if ([NSLocalizedStringFromTableInBundle(@"child_template2_list_data_source_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:REMOTE_DATA] && [local_processorCode isEqualToString:PROCESSOR_CODE_FETCH_BENEFICIARY_BANK])
    {
        NSString *profileID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"] objectForKey:PARAMETER36];
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus netStatus = [reachability  currentReachabilityStatus];
        if (![profileID isEqualToString:@"SMS"] || (netStatus == NotReachable))
        {
            NSMutableDictionary *webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
            [webServiceRequestInputDetails setObject:local_processorCode forKey:PARAMETER15];
            [webServiceRequestInputDetails setObject:transactionType forKey:PARAMETER13];
            [self commonWebServiceCallWithData:webServiceRequestInputDetails andProcessorCode:local_processorCode];
        }
    }
    else if ([NSLocalizedStringFromTableInBundle(@"child_template2_list_data_source_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:REMOTE_DATA] && [local_processorCode isEqualToString:PROCESSOR_CODE_VIEW_ALL_ACTIVE_BUNDLES])
    {
        NSString *profileID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"] objectForKey:PARAMETER36];
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus netStatus = [reachability  currentReachabilityStatus];
        if (![profileID isEqualToString:@"SMS"] || (netStatus == NotReachable))
        {
            NSMutableDictionary *webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
            [webServiceRequestInputDetails setObject:local_processorCode forKey:PARAMETER15];
            [webServiceRequestInputDetails setObject:transactionType forKey:PARAMETER13];
            [self commonWebServiceCallWithData:webServiceRequestInputDetails andProcessorCode:local_processorCode];
        }
    }
    else{
        if ([BaseViewController getDataFromProcessoreCode:local_processorCode andPropertyFileName:propertyFileName forArray:&dataArray]){
            childTemplate2_TableView.frame = CGRectMake(0, nextY_position+5, SCREEN_WIDTH , height_Position-70);
            if ([dataArray count]>0){
                childTemplate2_TableView.frame = CGRectMake(0, nextY_position+5, SCREEN_WIDTH , height_Position-70);
                alertLabel.hidden = YES;
            }
            else{
                childTemplate2_TableView.hidden = YES;
                [childTemplate2_TableView removeFromSuperview];
                alertLabel.frame = CGRectMake(10, (SCREEN_HEIGHT/3),self.frame.size.width-20, 50);
                alertLabel.text=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template2_recent_transactions_not_available",propertyFileName,[NSBundle mainBundle], nil)];
                alertLabel.numberOfLines = 2;
                alertLabel.textAlignment = NSTextAlignmentCenter;
                alertLabel.hidden = NO;
                [self addSubview:alertLabel];
            }
        }
        else
        {
            childTemplate2_TableView.frame = CGRectMake(0, nextY_position+5, SCREEN_WIDTH-95 , height_Position-70);
            array1 = [[NSMutableArray alloc] init];
            for (int i=0; i<[dataArray count]; i++){
                if (![[[dataArray objectAtIndex:i] objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME] isEqualToString:@""]){
                    [array1 addObject:[[dataArray objectAtIndex:i] objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME]];
                }
                else if (![[[dataArray objectAtIndex:i] objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER] isEqualToString:@""])
                {
                    [array1 addObject:[[dataArray objectAtIndex:i] objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER]];
                }
            }
            if ([dataArray count]>0){
                childTemplate2_TableView.frame = CGRectMake(0, nextY_position+5, SCREEN_WIDTH , height_Position-70);
                alertLabel.hidden = YES;
            }
            else{
                childTemplate2_TableView.hidden=YES;
                [childTemplate2_TableView removeFromSuperview];
                alertLabel.frame = CGRectMake(10, (SCREEN_HEIGHT/3),self.frame.size.width-20, 50);
                alertLabel.text=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template2_recent_transactions_not_available",propertyFileName,[NSBundle mainBundle], nil)];
                alertLabel.numberOfLines = 2;
                alertLabel.textAlignment = NSTextAlignmentCenter;
                alertLabel.hidden = NO;
                [self addSubview:alertLabel];
            }
        }
    }
    // Field1(Key label and Value Label).
    if ([NSLocalizedStringFromTableInBundle(@"child_template2_labels_field1_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ([NSLocalizedStringFromTableInBundle(@"child_template2_label_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            keyLabel= [[UILabel alloc] init];
            keyLabel.frame = CGRectMake(5, 0, self.frame.size.width,40);
            keyLabel.backgroundColor = [UIColor whiteColor];
            keyLabel.textAlignment = NSTextAlignmentLeft;
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template2_label",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                keyLabel.text=valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template2_label_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                // Properties for label TextColor.
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template2_label_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template2_label_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"child_template2_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template2_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    keyLabel.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template2_label_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template2_label_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template2_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template2_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template2_label_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template2_label_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template2_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template2_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    keyLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    keyLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    keyLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    keyLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template2_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template2_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    keyLabel.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for label textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template2_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template2_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template2_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template2_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    keyLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    keyLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    keyLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    keyLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            nextY_position = keyLabel.frame.origin.y+keyLabel.frame.size.height;
            [self addSubview:keyLabel];
        }
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template2_value_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            valueLabel = [[UILabel alloc] init];
            valueLabel.frame = CGRectMake(keyLabel.intrinsicContentSize.width+15, 0,(SCREEN_WIDTH/2)-5,40);
            valueLabel.backgroundColor = [UIColor clearColor];
            valueLabel.textAlignment = NSTextAlignmentLeft;
            
            NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
            NSString *lang = [sta objectForKey:userLanguage];
            
            NSString *temp;
            for (int i=0; i< dataArray.count;i++)
            {
                if ([lang caseInsensitiveCompare:[[dataArray objectAtIndex:i] objectForKey:DROP_DOWN_TYPE_NAME]]==NSOrderedSame)
                {
                    temp=[[dataArray objectAtIndex:i] objectForKey:DROP_DOWN_TYPE_DESC];
                    break;
                }
            }
            
            user_Language=((temp!=nil)?temp:@"");
            
            if (user_Language)
                valueLabel.text = user_Language;
            
            else
                valueLabel.text = [[dataArray objectAtIndex:0] objectForKey:DROP_DOWN_TYPE_DESC];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template2_value_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template2_value_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template2_value_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"child_template2_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template2_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (label_TextColor)
                    valueLabel.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template2_value_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template2_value_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template2_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template2_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template2_value_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template2_value_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template2_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template2_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    valueLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    valueLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    valueLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template2_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template2_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    valueLabel.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for label textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template2_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template2_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                //Default Properties for label fontSize
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template2_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template2_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    valueLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    valueLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    valueLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [self addSubview:valueLabel];
            nextY_position = valueLabel.frame.origin.y+valueLabel.frame.size.height;
            childTemplate2_TableView.frame = CGRectMake(0, nextY_position+5, SCREEN_WIDTH , height_Position-70);
        }
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template2_labels_field1_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            borderLabel=[[UILabel alloc]init];
            borderLabel.frame=CGRectMake(5, nextY_position, self.frame.size.width-5, 1);
            borderLabel.backgroundColor=[UIColor blackColor];
            [self addSubview:borderLabel];
        }
    }
    // Buttons
    if ([NSLocalizedStringFromTableInBundle(@"child_template2_button_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        childButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [childButton setFrame:CGRectMake(6,self.frame.size.height-45, self.frame.size.width-12, 50)];
        
        NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template2_button_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (buttonStr)
            [childButton setTitle:buttonStr forState:UIControlStateNormal];
        
        // properties For Button backgroundColor
        NSArray *childbutton1Color;
        if (NSLocalizedStringFromTableInBundle(@"child_template2_button_back_ground_color",propertyFileName,[NSBundle mainBundle], nil))
            childbutton1Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template2_button_back_ground_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
            childbutton1Color=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        [childButton setBackgroundImage:[UIImage imageNamed:@"button.png"] forState:UIControlStateNormal];
        
        //        if (childbutton1Color)
        //            childButton.backgroundColor=[UIColor colorWithRed:[[childbutton1Color objectAtIndex:0] floatValue] green:[[childbutton1Color objectAtIndex:1] floatValue] blue:[[childbutton1Color objectAtIndex:2] floatValue] alpha:1.0f];
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template2_button1_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *childButton1TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template2_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template2_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template2_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template2_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                childButton1TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (childButton1TextColor)
                [childButton setTitleColor:[UIColor colorWithRed:[[childButton1TextColor objectAtIndex:0] floatValue] green:[[childButton1TextColor objectAtIndex:1] floatValue] blue:[[childButton1TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template2_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template2_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template2_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template2_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            
            // Properties for Button Font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template2_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template2_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template2_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template2_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                childButton.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                childButton.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                childButton.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                childButton.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        else
        {
            //Default Properties for Button textcolor
            NSArray *childButton1TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template2_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template2_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                childButton1TextColor=[ValidationsClass colorWithHexString: application_default_button_text_color];
            if (childButton1TextColor)
                [childButton setTitleColor:[UIColor colorWithRed:[[childButton1TextColor objectAtIndex:0] floatValue] green:[[childButton1TextColor objectAtIndex:1] floatValue] blue:[[childButton1TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template2_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template2_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template2_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template2_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                childButton.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                childButton.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                childButton.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                childButton.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [childButton setTag:1];
        [childButton setExclusiveTouch:YES];
        [childButton addTarget:self action:@selector(childButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:childButton];
        int pageHeaderPos=64;
        int buttonpos=55;
        int searchbarpos=45;
        height_Position = SCREEN_HEIGHT-((pageHeaderPos+buttonpos+searchbarpos)-nextY_position);
    }
}

#pragma mark - WebService Call method.
/*
 * This method is used to set childtemplate2 webservice calling common method.
 */
-(void)commonWebServiceCallWithData:(NSMutableDictionary *)inputDataDictionary andProcessorCode:(NSString *)processorCode
{
    NSLog(@"commonWebServiceCallWithData..");
    [self setActivityIndicator];
    WebSericeUtils *webUtils = [[WebSericeUtils alloc] init];
    NSMutableDictionary *webUtilsValues = [[NSMutableDictionary alloc] initWithDictionary:[webUtils getWebServiceBundleObjectWithProccessorCode:processorCode withInputDataModel:inputDataDictionary]];
    
    WebServiceRequestFormation *webServiceRequest = [[WebServiceRequestFormation alloc] init];
    NSString *webRequest  = [webServiceRequest sendRequest:webUtilsValues];
    
    WebServiceRunning *webServiceRun = [[WebServiceRunning alloc] init];
    [webServiceRun startWebServiceWithRequest:webRequest withReqDictionary:webUtilsValues withDelegate:self];
}

#pragma mark - child button Action.
/**
 * This method is used to set button1 action of ChildTemplate2.
 @prameters are defined in button action those are
 * Application display type(ticker,PopUp),validation type,next template,next template PropertyFile,Action type,
 Feature-(processorCode,transaction type).
 */
-(void)childButtonAction:(id)sender
{
    NSString *actionType = NSLocalizedStringFromTableInBundle(@"child_template2_button_action_type",propertyFileName,[NSBundle mainBundle], nil);
    
    if ([actionType isEqualToString:ACTION_TYPE_1])
    {
    }
    else if ([actionType isEqualToString:ACTION_TYPE_2])
    {
    }
    else if ([actionType isEqualToString:ACTION_TYPE_3])
    {
    }
    else if ([actionType isEqualToString:ACTION_TYPE_4])
    {
        NSLog(@"Pressed");
    }
    else if ([actionType isEqualToString:ACTION_TYPE_5])
    {
    }
    else if ([actionType isEqualToString:ACTION_TYPE_6])
    {
    }
    else if ([actionType isEqualToString:ACTION_TYPE_7])
    {
    }
    else if ([actionType isEqualToString:ACTION_TYPE_8])
    {
        nextTemplatePropertyFile =  NSLocalizedStringFromTableInBundle(@"child_template2_button_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
        nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template2_button_next_template",propertyFileName,[NSBundle mainBundle], nil);
        
        Class myclass = NSClassFromString(nextTemplate);
        if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
        {
            id obj = [[myclass alloc] initWithNibName:nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:nil withPropertyFile:nextTemplatePropertyFile withProcessorCode:nil dataArray:nil dataDictionary:nil];
            
            AppDelegate *apDlgt = (AppDelegate *)[[UIApplication sharedApplication]delegate];
            UINavigationController *navCntrl = (UINavigationController *)apDlgt.window.rootViewController;
            if (navCntrl && [navCntrl isKindOfClass:[UINavigationController class]])
            {
                [navCntrl pushViewController:obj animated:NO];
            }
        }
        
    }
    else if ([actionType isEqualToString:ACTION_TYPE_9])
    {
    }
    else if ([actionType isEqualToString:ACTION_TYPE_10])
    {
    }
    else if ([actionType isEqualToString:ACTION_TYPE_11])
    {
    }
}

#pragma mark - TABLE VIEW DATA SOURCE AND DELEGATE METHODS.
/**
 * This method is used to set add Tableview (list and delegate methods) of ChildTemplate2.
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([propertyFileName isEqualToString:@"PayBillMyBillersAddViewByCategoryCT2"]){
        return 50;
    }
    if (filteredArray) {
        return 60;
    }
    else
    {
        if ([NSLocalizedStringFromTableInBundle(@"child_template2_list_data_source_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:LOCAL_DATA]) {
            return 100;
        } else {
            if ([NSLocalizedStringFromTableInBundle(@"child_template2_list_item_text_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame) {
                return 60;
            } else {
                return 60;
            }
        }
        return 60;
    }
    
    return 50;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rowCount = 0;
    if ([NSLocalizedStringFromTableInBundle(@"child_template2_list_data_source_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:LOCAL_DATA])
    {
        rowCount = (int)[dataArray count];
    }
    else
    {
        if(isFiltered)
            rowCount = (int)[filteredArray count];
        else
            rowCount = (int)[dataArray count];
        
    }
    
    return rowCount;
}
/**
 * This method is used to set add Tableview for ChildTemplate2.
 *@param type- Declare Listview related UI(Labels,Images etc..base on Req).
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = [NSString stringWithFormat:@"%@%d",@"Cell",(int)indexPath.row];
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    UIImageView *userProfileImgView;
    NSString *dataVal = NSLocalizedStringFromTableInBundle(@"child_template2_list_data_to_load_webservice_api",propertyFileName,[NSBundle mainBundle], nil);
    NSString *process_code = [Template getProcessorCodeWithData:dataVal];
    NSString *apiParamName = NSLocalizedStringFromTableInBundle(@"child_template2_list_item_param_type",propertyFileName,[NSBundle mainBundle], nil);
    
    if (isFiltered) {
        if ([NSLocalizedStringFromTableInBundle(@"child_template2_list_item_text_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            usernameLbl = [[UILabel alloc]init];
            usernameLbl.frame=CGRectMake(10, 5,tableView.frame.size.width-20, 50);
            [usernameLbl setText:[NSString stringWithFormat:@" %@ %@",@" ",[[filteredArray objectAtIndex:indexPath.row] objectForKey:apiParamName]]];
            [usernameLbl setTextAlignment:NSTextAlignmentLeft];
            [usernameLbl setNumberOfLines:2];
            
            usernameLbl.backgroundColor = [UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1.0];
            usernameLbl.layer.cornerRadius = 25;
            usernameLbl.clipsToBounds = YES;
            usernameLbl.layer.borderWidth = 2;
            usernameLbl.layer.borderColor = [UIColor colorWithRed:110/255.0 green:25/255.0 blue:65/255.0 alpha:1.0].CGColor;
            
            //NSArray *colorsArray = [ValidationsClass colorWithHexString:    NSLocalizedStringFromTableInBundle(@"child_template2_list_item_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            [usernameLbl setTextColor:[UIColor blackColor]];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template2_list_item_text_style",propertyFileName,[NSBundle mainBundle], nil)  isEqualToString:@"1"])
                [usernameLbl setFont:[UIFont boldSystemFontOfSize:[NSLocalizedStringFromTableInBundle(@"child_template2_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil) floatValue]]];
            else
                [usernameLbl setFont:[UIFont systemFontOfSize:[NSLocalizedStringFromTableInBundle(@"child_template2_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil) floatValue]]];
        }
        [cell.contentView addSubview:usernameLbl];
        
    }
    else
    {
        if ([NSLocalizedStringFromTableInBundle(@"child_template2_list_data_source_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:LOCAL_DATA])
        {
            UILabel *borderLabel = [[UILabel alloc]init];
            borderLabel.frame = CGRectMake(10, 5, self.frame.size.width-20, 90);
            if([propertyFileName isEqualToString:@"PayBillMyBillersAddViewByCategoryCT2"]){
                borderLabel.frame = CGRectMake(10, 5, self.frame.size.width-20, 35);
            }
            borderLabel.backgroundColor = [UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1.0];
            borderLabel.layer.cornerRadius = 20;
            borderLabel.clipsToBounds = YES;
            borderLabel.layer.borderWidth = 2;
            borderLabel.layer.borderColor = [UIColor colorWithRed:110/255.0 green:25/255.0 blue:65/255.0 alpha:1.0].CGColor;
            //          [cell.contentView addSubview:borderLabel];
            
            UILabel *mobileLabel = [[UILabel alloc]init];
            mobileLabel.frame = CGRectMake(20, 15, self.frame.size.width-20, 20);
            mobileLabel.textColor = [UIColor blackColor];
            mobileLabel.text = @"Mobile Number";
            [mobileLabel setFont:[UIFont systemFontOfSize:[NSLocalizedStringFromTableInBundle(@"child_template2_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil) floatValue]]];
            //          [cell.contentView addSubview:mobileLabel];
            
            
            usernameLbl =[[UILabel alloc]init];
            usernameLbl.frame=CGRectMake(20, mobileLabel.frame.origin.y+mobileLabel.frame.size.height+5,tableView.frame.size.width-40, 40);
            if([propertyFileName isEqualToString:@"PayBillMyBillersAddViewByCategoryCT2"]){
                usernameLbl.frame=CGRectMake(10,5,tableView.frame.size.width-40, 40);
            }
            //NSArray *colorsArray = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template2_list_item_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            [usernameLbl setTextColor:[UIColor blackColor]];
            
            //            [usernameLbl setTextColor:[UIColor colorWithRed:[[colorsArray objectAtIndex:0] floatValue] green:[[colorsArray objectAtIndex:1] floatValue] blue:[[colorsArray objectAtIndex:2] floatValue] alpha:1.0f]];
            
            //            usernameLbl.backgroundColor = [UIColor whiteColor];
            [cell.contentView addSubview:usernameLbl];
            
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template2_list_item_text_style",propertyFileName,[NSBundle mainBundle], nil)  isEqualToString:@"1"])
                [usernameLbl setFont:[UIFont boldSystemFontOfSize:[NSLocalizedStringFromTableInBundle(@"child_template2_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil) floatValue]]];
            else
                [usernameLbl setFont:[UIFont systemFontOfSize:[NSLocalizedStringFromTableInBundle(@"child_template2_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil) floatValue]]];
            
            if ([data isEqualToString:@"LANGUAGE"] || [process_code isEqualToString:PROCESSOR_CODE_CHANGE_LANG])
            {
                [usernameLbl setText:[NSString stringWithFormat:@" %@ %@",@" ",[[dataArray objectAtIndex:indexPath.row] objectForKey:apiParamName]]];
                NSString *language = [[NSUserDefaults standardUserDefaults] objectForKey:userLanguage];
                NSString *uppercase = [[[dataArray objectAtIndex:indexPath.row] objectForKey:DROP_DOWN_TYPE_NAME] uppercaseString];
                if ([language isEqualToString:[[dataArray objectAtIndex:indexPath.row] objectForKey:DROP_DOWN_TYPE_NAME]]|| [language isEqualToString:uppercase])
                {
                    startIndexPath = indexPath;
                    UIImageView *myView = [[UIImageView alloc] init];
                    myView.frame=CGRectMake(SCREEN_WIDTH-50, 7.0f, 30.0f, 30.0f);
                    myView.image=[UIImage  imageNamed:SELECTED_CHECK_MARK_IMAGE];
                    [myView setTag:101];
                    [[cell contentView] addSubview:myView];
                }
            }
            else
            {
                if (![[[dataArray objectAtIndex:indexPath.row] objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME] isEqualToString:@"(null)"] && [[[dataArray objectAtIndex:indexPath.row] objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME] length] != 0 ) {
                    [usernameLbl setText:[NSString stringWithFormat:@"%@%@",@" ",[[dataArray objectAtIndex:indexPath.row] objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME]]];
                }
                else
                    [usernameLbl setText:[NSString stringWithFormat:@" %@%@",@" ",[[dataArray objectAtIndex:indexPath.row] objectForKey:apiParamName]]];
            }
            [ cell.contentView addSubview:usernameLbl];
            
        }
        else{
            
            float next_X_Position = 10;
            if ([NSLocalizedStringFromTableInBundle(@"child_template2_list_item_image_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                userProfileImgView = [[UIImageView alloc] init];
                userProfileImgView.frame=CGRectMake(next_X_Position, 5, 40, 40);
                userProfileImgView.image=[UIImage imageNamed:@"profilePic.jpg"];
                next_X_Position = userProfileImgView.frame.origin.x+userProfileImgView.frame.size.width;
            }
            if ([NSLocalizedStringFromTableInBundle(@"child_template2_list_item_text_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                if (array1.count != 0) {
                    usernameLbl=[[UILabel alloc]init];
                    usernameLbl.frame=CGRectMake(next_X_Position, 5,tableView.frame.size.width-20, 50);
                    [usernameLbl setText:[NSString stringWithFormat:@" %@%@",@" ",[array1 objectAtIndex:[indexPath row]]]];
                    usernameLbl.backgroundColor = [UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1.0];
                    usernameLbl.layer.cornerRadius = 25;
                    usernameLbl.clipsToBounds = YES;
                    usernameLbl.layer.borderWidth = 2;
                    usernameLbl.layer.borderColor = [UIColor colorWithRed:110/255.0 green:25/255.0 blue:65/255.0 alpha:1.0].CGColor;
                }
                else {
                    [activityIndicator stopActivityIndicator];
                    activityIndicator.hidden=YES;
                    usernameLbl=[[UILabel alloc]init];
                    usernameLbl.frame=CGRectMake(10, 5,tableView.frame.size.width-20, 50);
                    [usernameLbl setText:[NSString stringWithFormat:@" %@%@",@" ",[[dataArray objectAtIndex:indexPath.row] objectForKey:apiParamName]]];
                    usernameLbl.backgroundColor = [UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1.0];
                    usernameLbl.layer.cornerRadius = 25;
                    usernameLbl.clipsToBounds = YES;
                    //                    usernameLbl.layer.borderWidth = 2;
                    //                    usernameLbl.layer.borderColor = [UIColor colorWithRed:110/255.0 green:25/255.0 blue:65/255.0 alpha:1.0].CGColor;
                }
                [usernameLbl setTextAlignment:NSTextAlignmentLeft];
                
                NSArray *colorsArray = [ValidationsClass colorWithHexString: NSLocalizedStringFromTableInBundle(@"child_template2_list_item_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                [usernameLbl setTextColor:[UIColor blackColor]];
                
                if ([NSLocalizedStringFromTableInBundle(@"child_template2_list_item_text_style",propertyFileName,[NSBundle mainBundle], nil)  isEqualToString:@"1"])
                    [usernameLbl setFont:[UIFont boldSystemFontOfSize:[NSLocalizedStringFromTableInBundle(@"child_template2_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil) floatValue]]];
                else
                    [usernameLbl setFont:[UIFont systemFontOfSize:[NSLocalizedStringFromTableInBundle(@"child_template2_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil) floatValue]]];
                [[cell contentView] addSubview:usernameLbl];
            }
        }
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundView:[[UIImageView alloc] initWithImage:
                             [UIImage imageNamed:@"cellborder.png"]]];
    if([propertyFileName isEqualToString:@"SetLanguageCT2_T3"]){
        if(indexPath.row == 0){
            [usernameLbl setText:@"عربى"];
        }
        if(indexPath.row == 1){
            [usernameLbl setText:@"English"];
        }
    }
    return cell;
}

/**
 * This method is used to set add Tableview Delegate Method User selects List .
 *@param type- Declare Listview - (NextTemplate and Next template proprty file).
 *To show List Item Detailed Data etc..
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    currentlySelectedRow = indexPath.row;
    NSLog(@"Current processor code is %@ for template %@", local_processorCode, propertyFileName);
    [self removeActivityIndicator];
    selectedIndex = indexPath;
    selected = (int)indexPath.row;
    NSString *processorCode;
    NSString *type;
    if (!previousSelectedIndex)
    {
        previousSelectedIndex = indexPath;
    }
    
    [main_searchBar resignFirstResponder];
    
    if (hasSelectionOperation)
    {
        if ([local_processorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG_T3])
        {
            local_cell = [tableView cellForRowAtIndexPath:indexPath];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData:) name:@"UpdateData" object:nil];
            NSMutableDictionary *localdataDict = [[NSMutableDictionary alloc] init];
            
            nextTemplatePropertyFile =  NSLocalizedStringFromTableInBundle(@"child_template2_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
            nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template2_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
            userSelectedLanguage = [[dataArray objectAtIndex:indexPath.row]valueForKey:DROP_DOWN_TYPE_DESC];
            NSString *value=[[dataArray objectAtIndex:indexPath.row]valueForKey:DROP_DOWN_TYPE_NAME];
            NSMutableDictionary *postData = [[NSMutableDictionary alloc] init];
            [postData setObject:value forKey:DROP_DOWN_TYPE_NAME];
            [[NSNotificationCenter defaultCenter] postNotificationName:POST_CONTENT object:postData];
            
            if (![nextTemplatePropertyFile isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
            {
                Class myclass = NSClassFromString(nextTemplate);
                id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplatePropertyFile andDelegate:self withDataDictionary:localdataDict withProcessorCode:local_processorCode withTag:-3 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
                [[[self superview] superview] addSubview:(UIView *)obj];
            }
        }
        else if (![NSLocalizedStringFromTableInBundle(@"child_template2_list_data_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@""])
        {
            nextTemplatePropertyFile =  NSLocalizedStringFromTableInBundle(@"child_template2_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
            nextTemplate =  NSLocalizedStringFromTableInBundle(@"child_template2_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[filteredArray objectAtIndex:indexPath.row]];
            
            NSString *dataVal = NSLocalizedStringFromTableInBundle(@"child_template2_list_data_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
            processorCode = [Template getProcessorCodeWithData:dataVal];
            type = [Template getTransactionCodeWithData:dataVal];
            if(type)
                [dict setObject:type forKey:PARAMETER13];
            
            [dict setObject:processorCode forKey:PARAMETER15];
            
            if (![nextTemplatePropertyFile isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
            {
                NSMutableDictionary *webServiceRequestInputDetails = [[NSMutableDictionary alloc] initWithDictionary:dict];
                [webServiceRequestInputDetails setObject:[[dataArray objectAtIndex:indexPath.row] objectForKey:DROP_DOWN_TYPE_NAME] forKey:PARAMETER17];
                [webServiceRequestInputDetails setObject:NSLocalizedStringFromTableInBundle(@"child_template2_list_data_to_load_webservice_api", propertyFileName, [NSBundle mainBundle], nil) forKey:PARAMETER_TYPE];
                WebSericeUtils *webUtils = [[WebSericeUtils alloc] init];
                NSMutableDictionary *webUtilsValues = [[NSMutableDictionary alloc] initWithDictionary:[webUtils getWebServiceBundleObjectWithProccessorCode:processorCode withInputDataModel:webServiceRequestInputDetails]];
                NSLog(@"webUtil Values Aree..%@",webUtilsValues);
                NSString *profileID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"] objectForKey:PARAMETER36];
                
                Reachability *reachability = [Reachability reachabilityForInternetConnection];
                NetworkStatus netStatus = [reachability  currentReachabilityStatus];
                
                if ((netStatus == NotReachable) || [profileID isEqualToString:@"SMS"]) {
                    [self processDataForSMS:webUtilsValues withProcessCode:processorCode];
                }
                else
                {
                    WebServiceRequestFormation *webServiceRequest = [[WebServiceRequestFormation alloc] init];
                    NSString *webRequest  = [webServiceRequest sendRequest:webUtilsValues];
                    WebServiceRunning *webServiceRun = [[WebServiceRunning alloc] init];
                    [webServiceRun startWebServiceWithRequest:webRequest withReqDictionary:webUtilsValues withDelegate:self];
                }
            }
            
            NSLog(@"NextTemplate is ..%@,%@",nextTemplate,nextTemplatePropertyFile);
        }
        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_FETCH_BILLER_ALL])
        {
            if ([NSLocalizedStringFromTableInBundle(@"child_template2_get_area_codes",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                areaCodesRequired = NO;
                __block NSString *billerType;
                NSArray *currentArray;
                if (filteredArray) {
                    currentArray = [NSArray arrayWithArray:filteredArray];
                }else{
                    currentArray = [NSArray arrayWithArray:dataArray];
                }
                NSDictionary *temp = [currentArray objectAtIndex:indexPath.row];
                billerType = [temp objectForKey:BILLERID];
                if([temp objectForKey:@"billerA2"]){
                    areaCodesRequired = YES;
                }
                if(areaCodesRequired){
                    Reachability *reachability = [Reachability reachabilityForInternetConnection];
                    NetworkStatus netStatus = [reachability  currentReachabilityStatus];
                    if (netStatus == NotReachable) {
                        NSLog(@"Alert : No internet..");
                    }
                    else
                    {
                        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                        NSString *dataVal = NSLocalizedStringFromTableInBundle(@"child_template2_area_codes_web_service_api_name", propertyFileName, [NSBundle mainBundle], nil);
                        NSString *processorCode = [Template getProcessorCodeWithData:dataVal];
                        NSString *type = [Template getTransactionCodeWithData:dataVal];
                        [dict setObject:type forKey:PARAMETER13];
                        [dict setObject:processorCode forKey:PARAMETER15];
                        [dict setObject:[NSString stringWithFormat:@"FLAT|BillerID|%@", billerType] forKey:PARAMETER17];
                        WebSericeUtils *webUtils = [[WebSericeUtils alloc] init];
                        NSMutableDictionary *webUtilsValues = [[NSMutableDictionary alloc] initWithDictionary:[webUtils getWebServiceBundleObjectWithProccessorCode:processorCode withInputDataModel:dict]];
                        WebServiceRequestFormation *webServiceRequest = [[WebServiceRequestFormation alloc] init];
                        NSString *webRequest  = [webServiceRequest sendRequest:webUtilsValues];
                        WebServiceRunning *webServiceRun = [[WebServiceRunning alloc] init];
                        [webServiceRun startWebServiceWithRequest:webRequest withReqDictionary:webUtilsValues withDelegate:self];
                    }
                }else{
                    [self showDetailsPage:nil];
                }
            }else{
                nextTemplatePropertyFile =  NSLocalizedStringFromTableInBundle(@"child_template2_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
                nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template2_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
                
                if (![nextTemplatePropertyFile isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
                {
                    UIViewController *object;
                    if (isFiltered)
                    {
                        [main_searchBar resignFirstResponder];
                        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[filteredArray objectAtIndex:indexPath.row]];
                        object=[[NSClassFromString(nextTemplate)alloc]initWithNibName:nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:nextTemplatePropertyFile withProcessorCode:processorCode dataArray:nil dataDictionary:dict];
                    }
                    else
                    {
                        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[dataArray objectAtIndex:indexPath.row]];
                        
                        object=[[NSClassFromString(nextTemplate)alloc]initWithNibName:nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:nextTemplatePropertyFile withProcessorCode:processorCode dataArray:nil dataDictionary:dict];
                    }
                    
                    UIViewController *vc = [(UINavigationController *)self.window.rootViewController visibleViewController];
                    [vc.navigationController pushViewController:object animated:NO];
                }
                
            }
            
        }
        else if ([local_processorCode isEqualToString:PROCESSOR_CODE_BANK]){
            nextTemplatePropertyFile =  NSLocalizedStringFromTableInBundle(@"child_template2_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
            nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template2_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[dataArray objectAtIndex:indexPath.row]];
            Class myclass = NSClassFromString(nextTemplate);
            id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplatePropertyFile andDelegate:self withDataDictionary:dict withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
            
            [[[self superview] superview] addSubview:(UIView *)obj];
        }
        else
        {
            nextTemplatePropertyFile =  NSLocalizedStringFromTableInBundle(@"child_template2_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
            nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template2_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
            [dataDict addEntriesFromDictionary:[dataArray objectAtIndex:indexPath.row]];
            
            NSString *dataVal =  NSLocalizedStringFromTableInBundle(@"child_template2_list_data_to_load_webservice_api",propertyFileName,[NSBundle mainBundle], nil);
            
            NSString *typeName = [NSString stringWithFormat:@"%@_typeName",dataVal];
            NSString *typeDesc = [NSString stringWithFormat:@"%@_typeDesc",dataVal];
            
            if ([[dataArray objectAtIndex:indexPath.row] objectForKey:@"typeName"]) {
                [dataDict setObject:[[dataArray objectAtIndex:indexPath.row] objectForKey:@"typeName"] forKey:typeName];
            }
            
            if ([[dataArray objectAtIndex:indexPath.row] objectForKey:@"typeDesc"]) {
                [dataDict setObject:[[dataArray objectAtIndex:indexPath.row] objectForKey:@"typeDesc"] forKey:typeDesc];
            }
            NSLog(@"NextTemplate is...%@,%@,%@",nextTemplate,nextTemplatePropertyFile,[dataDict description]);
            Class myclass = NSClassFromString(nextTemplate);
            
            if ([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
            {
                id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplatePropertyFile andDelegate:self withDataDictionary:dataDict withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:(int)indexPath.row withDataArray:dataArray withSubIndex:0];
                [[[self superview] superview] addSubview:(UIView *)obj];
            }
            else if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
            {
                id obj = [[myclass alloc] initWithNibName:nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:nil withPropertyFile:nextTemplatePropertyFile withProcessorCode:nil dataArray:nil dataDictionary:dataDict];
                UIViewController *vc = [(UINavigationController *)self.window.rootViewController visibleViewController];
                [vc.navigationController pushViewController:obj animated:NO];
                
            }
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark Other methods
/*
 * This method is used to set childtemplate2 Local delegate.
 */
-(void)setLocalDelegate:(id)fromDelegate
{
    self.delegate = fromDelegate;
}


-(void)showDetailsPage:(WebServiceDataObject *)webServiceDataObject{
    nextTemplatePropertyFile =  NSLocalizedStringFromTableInBundle(@"child_template2_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template2_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
    
    if (![nextTemplatePropertyFile isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
    {
        UIViewController *object;
        if (isFiltered)
        {
            [main_searchBar resignFirstResponder];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[filteredArray objectAtIndex:currentlySelectedRow]];
            if(areaCodesRequired){
                [dict setObject:webServiceDataObject.PaymentDetails2 forKey:@"AreaCodes"];
            }
            object=[[NSClassFromString(nextTemplate)alloc]initWithNibName:nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:nextTemplatePropertyFile withProcessorCode:nil dataArray:nil dataDictionary:dict];
        }
        else
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[dataArray objectAtIndex:currentlySelectedRow]];
            if(areaCodesRequired){
                [dict setObject:webServiceDataObject.PaymentDetails2 forKey:@"AreaCodes"];
            }
            object=[[NSClassFromString(nextTemplate)alloc]initWithNibName:nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:nextTemplatePropertyFile withProcessorCode:nil dataArray:nil dataDictionary:dict];
        }
        UIViewController *vc = [(UINavigationController *)self.window.rootViewController visibleViewController];
        [vc.navigationController pushViewController:object animated:NO];
    }
}

#pragma mark - Update Data.
/*
 * This method is used to set childtemplate2 Remove notification data.
 */
- (void)updateData:(NSNotification*)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Searchbar Delegate method.
/**
 * This method is used to set add Searchbar Delegate method.
 */
-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        isFiltered = FALSE;
        [main_searchBar resignFirstResponder];
        [main_searchBar performSelector: @selector(resignFirstResponder)
                             withObject: nil
                             afterDelay: 0.1];
    }
    else
    {
        isFiltered = TRUE;
        filteredArray = [[NSMutableArray alloc] init];
        
        for (NSDictionary* local_Dictionary in dataArray)
        {
            NSRange nameRange = [[local_Dictionary objectForKey:BILLERNAME] rangeOfString:text options:NSCaseInsensitiveSearch];
            if(nameRange.length >0 )
            {
                [filteredArray addObject:local_Dictionary];
            }
        }
        NSLog(@"filteredArray: %@",filteredArray);
    }
    [childTemplate2_TableView reloadData];
}

- (void)hideKeyboardWithSearchBar:(UISearchBar *)searchBar{
    [main_searchBar resignFirstResponder];
}

#pragma mark - ProcessData For SMS.
/*
 * This method is used to set childtemplate2 listview webservices processing method for Sms Method.
 */
-(void)processDataForSMS:(NSMutableDictionary *)webServiceDataObject withProcessCode:(NSString *)processorCode
{
    [self removeActivityIndicator];
    if ([processorCode isEqualToString:PROCESSOR_CODE_FETCH_BILLER_ALL])
    {
        NSString *jsonDataStr =  NSLocalizedStringFromTableInBundle([Template getFormatedKey:webServiceDataObject],@"DropDownTemplate",[NSBundle mainBundle], nil);
        
        if (![jsonDataStr isEqualToString:[Template getFormatedKey:webServiceDataObject]] && !(jsonDataStr.length ==0))
        {
            
            NSData *jsonData = [jsonDataStr dataUsingEncoding:NSUTF8StringEncoding];
            
            NSError *jsonError;
            NSArray *local_dataArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError];
            NSLog(@"dataArray: %@, %d",local_dataArray,(int)[local_dataArray count]);
            
            if (![local_dataArray count])
            {
                [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_noresults_found_message", nil)] andErrorCode:nil];
            }
            else
            {
                NSString *bn = @"bn";
                NSString *type = @"type";
                NSString *sn = @"sn";
                NSString *bmi = @"bmi";
                NSString *a1 = @"A1";
                NSString *a2 = @"A2";
                NSString *a3 = @"A3";
                NSString *a4 = @"A4";
                NSString *a5 = @"A5";
                NSString *loc = @"loc";
                NSString *cat = @"cat";
                NSString *min = @"min";
                NSString *max = @"max";

                NSMutableArray *finalArray = [[NSMutableArray alloc] init];
                for (int i=0; i<[local_dataArray count]; i++)
                {
                    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                    
                    if ([[local_dataArray objectAtIndex:i] objectForKey:bn])
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:bn] forKey:BILLERNAME];
                    }
                    
                    if ([[local_dataArray objectAtIndex:i] objectForKey:type])
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:type] forKey:BILLERTYPE];
                    }
                    if ([[local_dataArray objectAtIndex:i] objectForKey:sn])
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:sn] forKey:BILLERSHORTNAME];
                    }
                    
                    if ([[local_dataArray objectAtIndex:i] objectForKey:bmi])
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:bmi] forKey:BILLERMASTERID];
                    }
                    
                    if ([[local_dataArray objectAtIndex:i] objectForKey:a1] )
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a1] forKey:BILLERA1];
                    }
                    if ([[local_dataArray objectAtIndex:i] objectForKey:a2] )
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a2] forKey:BILLERA2];
                    }
                    if ([[local_dataArray objectAtIndex:i] objectForKey:a3] )
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a3] forKey:BILLERA3];
                    }
                    if ([[local_dataArray objectAtIndex:i] objectForKey:a4] )
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a4] forKey:BILLERA4];
                    }
                    if ([[local_dataArray objectAtIndex:i] objectForKey:a5] )
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a5] forKey:BILLERA5];
                    }
                    
                    if ([[local_dataArray objectAtIndex:i] objectForKey:loc] )
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:loc] forKey:BILLERLOCATION];
                    }
                    if ([[local_dataArray objectAtIndex:i] objectForKey:cat] )
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:cat] forKey:BILLERCATEGORY];
                    }
                    if ([[local_dataArray objectAtIndex:i] objectForKey:min] )
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:min] forKey:@"min"];
                    }
                    if ([[local_dataArray objectAtIndex:i] objectForKey:max] )
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:max] forKey:@"max"];
                    }
                    if ([[local_dataArray objectAtIndex:i] objectForKey:@"A1"] )
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:@"A1"] forKey:@"A1"];
                    }

                    [finalArray addObject:tempDict];
                }
                
                UIViewController *object = [[NSClassFromString(nextTemplate) alloc] initWithNibName:nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:nextTemplatePropertyFile withProcessorCode:processorCode dataArray:finalArray dataDictionary:nil];
                UIViewController *vc = [(UINavigationController *)self.window.rootViewController visibleViewController];
                [vc.navigationController pushViewController:object animated:NO];
            }
        }
        else
        {
            [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_noresults_found_message", nil)] andErrorCode:nil];
            
        }
    }
    
}

#pragma mark - Webservice methods.
/*
 * This method is used to set childtemplate2 listview webservices processing method
 */
-(void)processData:(WebServiceDataObject *)webServiceDataObject withProcessCode:(NSString *)processCode
{
    [self removeActivityIndicator];
    if (webServiceDataObject.faultCode && webServiceDataObject.faultString)
    {
        [self showAlertForErrorWithMessage:webServiceDataObject.faultString andErrorCode:nil];
    }
    else
    {
        if ([NSLocalizedStringFromTableInBundle(@"child_template2_get_area_codes",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            NSLog(@"PaymentDetails2 : %@", webServiceDataObject.PaymentDetails2);
            [self showDetailsPage:webServiceDataObject];
        }else
            
            if ([processCode isEqualToString:PROCESSOR_CODE_AGGREGATOR_LINKED_BANK_LIST]) {
                
                NSData *jsonData = [webServiceDataObject.PaymentDetails2 dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSArray *local_dataArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError];
                
                if (local_dataArray.count == 0)
                {
                    childTemplate2_TableView.hidden=YES;
                    alertLabel.frame = CGRectMake(10, (SCREEN_HEIGHT/3),self.frame.size.width-20, 50);
                    alertLabel.text=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template2_recent_transactions_not_available",propertyFileName,[NSBundle mainBundle], nil)];
                    alertLabel.textAlignment = NSTextAlignmentCenter;
                    alertLabel.numberOfLines = 2;
                    alertLabel.hidden = NO;
                    [self addSubview:alertLabel];
                }
                else
                {
                    for (int i=0; i<[local_dataArray count]; i++)
                    {
                        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[local_dataArray objectAtIndex:i]];
                        [dict setObject:[dict objectForKey:DROP_DOWN_TYPE_DESC] forKey:TRANSACTION_DATA_TRANSACTION_CASHOUT_BANK_DESC];
                        [dict setObject:[dict objectForKey:DROP_DOWN_TYPE_NAME] forKey:TRANSACTION_DATA_TRANSACTION_CASHOUT_BANK_NAME];
                        [dataArray addObject:dict];
                    }
                    if(dataArray.count > 0)
                    {
                        childTemplate2_TableView.frame = CGRectMake(0, nextY_position+5, SCREEN_WIDTH , height_Position-70);
                        [childTemplate2_TableView reloadData];
                        alertLabel.hidden = YES;
                    }
                }
            }
            else if ([processCode isEqualToString:PROCESSOR_CODE_CASHOUT_BANK_LIST]) {
                NSData *jsonData;
                NSArray *local_dataArray;
                
                if ((webServiceDataObject.PaymentDetails2 !=nil) && !([webServiceDataObject.PaymentDetails2  isEqual:@"(null)"]))
                    jsonData= [webServiceDataObject.PaymentDetails2 dataUsingEncoding:NSUTF8StringEncoding];
                
                NSError *jsonError;
                if ((jsonData !=nil) && !([jsonData  isEqual:@"(null)"]))
                    local_dataArray= [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError];
                
                NSString *nickName = @"NickName";
                NSString *bankName = @"BankName";
                NSString *bankCode = @"BankCode";
                NSString *accountNo = @"AccNo";
                
                NSMutableArray *finalArray = [[NSMutableArray alloc] init];
                for (int i=0; i<[local_dataArray count]; i++)
                {
                    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                    if ([[local_dataArray objectAtIndex:i] objectForKey:nickName])
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:nickName] forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME];
                    }
                    if ([[local_dataArray objectAtIndex:i] objectForKey:bankName] )
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:bankName] forKey:TRANSACTION_DATA_TRANSACTION_CASHOUT_BANK_DESC];
                    }
                    if ([[local_dataArray objectAtIndex:i] objectForKey:bankCode] )
                    {
                        //For API Check bcoz of improper response
                        
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:bankCode] forKey:TRANSACTION_DATA_TRANSACTION_CASHOUT_BANK_NAME];
                    }
                    if ([[local_dataArray objectAtIndex:i] objectForKey:accountNo] )
                    {
                        [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:accountNo] forKey:TRANSACTION_DATA_TRANSACTION_ACCOUNT_NUMBER];
                    }
                    [finalArray addObject:tempDict];
                }
                [dataArray setArray:finalArray];
                
                if(dataArray.count > 0)
                {
                    childTemplate2_TableView.frame = CGRectMake(0, nextY_position+5, SCREEN_WIDTH , height_Position-70);
                    [childTemplate2_TableView reloadData];
                    alertLabel.hidden = YES;
                }
                else
                {
                    childTemplate2_TableView.hidden=YES;
                    alertLabel.frame = CGRectMake(10, (SCREEN_HEIGHT/3),self.frame.size.width-20, 50);
                    alertLabel.text=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template2_recent_transactions_not_available",propertyFileName,[NSBundle mainBundle], nil)];
                    alertLabel.textAlignment = NSTextAlignmentCenter;
                    alertLabel.numberOfLines = 2;
                    alertLabel.hidden = NO;
                    [self addSubview:alertLabel];
                }
            }
            else if ([processCode isEqualToString:PROCESSOR_CODE_FETCH_BILLER_ALL])
            {
                if (webServiceDataObject.PaymentDetails2)
                {
                    NSLog(@"PaymentDetails2: %@",webServiceDataObject.PaymentDetails2);
                    NSData *jsonData = [webServiceDataObject.PaymentDetails2 dataUsingEncoding:NSUTF8StringEncoding];
                    
                    NSError *jsonError;
                    NSArray *local_dataArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError];
                    NSLog(@"dataArray: %@, %d",local_dataArray,(int)[local_dataArray count]);
                    
                    if (![local_dataArray count])
                    {
                        //[self showAlertForErrorWithMessage:@"No results found" andErrorCode:nil];
                        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_noresults_found_message", nil)] andErrorCode:nil];
                        
                    }
                    else
                    {
                        NSString *bn = @"bn";
                        NSString *type = @"type";
                        NSString *sn = @"sn";
                        NSString *bmi = @"bmi";
                        NSString *a1 = @"A1";
                        NSString *a2 = @"A2";
                        NSString *a3 = @"A3";
                        NSString *a4 = @"A4";
                        NSString *a5 = @"A5";
                        NSString *loc = @"loc";
                        NSString *cat = @"cat";
                        NSString *billerId = @"billerId";
                        NSString *min = @"min";
                        NSString *max = @"max";

                        NSMutableArray *finalArray = [[NSMutableArray alloc] init];
                        for (int i=0; i<[local_dataArray count]; i++)
                        {
                            NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                            
                            if (![[[local_dataArray objectAtIndex:i] objectForKey:bn] isEqual:[NSNull null]])
                            {
                                [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:bn] forKey:BILLERNAME];
                            }
                            if (![[[local_dataArray objectAtIndex:i] objectForKey:billerId] isEqual:[NSNull null]])
                            {
                                [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:billerId] forKey:BILLERID];
                            }
                            if (![[[local_dataArray objectAtIndex:i] objectForKey:type] isEqual:[NSNull null]])
                            {
                                [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:type] forKey:BILLERTYPE];
                            }
                            
                            if (![[[local_dataArray objectAtIndex:i] objectForKey:sn] isEqual:[NSNull null]])
                            {
                                [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:sn] forKey:BILLERSHORTNAME];
                            }
                            
                            if (![[[local_dataArray objectAtIndex:i] objectForKey:bmi] isEqual:[NSNull null]])
                            {
                                [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:bmi] forKey:BILLERMASTERID];
                            }
                            
                            if (![[[local_dataArray objectAtIndex:i] objectForKey:a1] isEqual:[NSNull null]])
                            {
                                [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a1] forKey:BILLERA1];
                            }
                            if (![[[local_dataArray objectAtIndex:i] objectForKey:a2] isEqual:[NSNull null]])
                            {
                                [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a2] forKey:BILLERA2];
                            }
                            if (![[[local_dataArray objectAtIndex:i] objectForKey:a3] isEqual:[NSNull null]])
                            {
                                [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a3] forKey:BILLERA3];
                            }
                            if (![[[local_dataArray objectAtIndex:i] objectForKey:a4] isEqual:[NSNull null]])
                            {
                                [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a4] forKey:BILLERA4];
                            }
                            if (![[[local_dataArray objectAtIndex:i] objectForKey:a5] isEqual:[NSNull null]])
                            {
                                [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:a5] forKey:BILLERA5];
                            }
                            
                            if (![[[local_dataArray objectAtIndex:i] objectForKey:loc] isEqual:[NSNull null]])
                            {
                                [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:loc] forKey:BILLERLOCATION];
                            }
                            if (![[[local_dataArray objectAtIndex:i] objectForKey:cat] isEqual:[NSNull null]])
                            {
                                [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:cat] forKey:BILLERCATEGORY];
                            }
                            if ([[local_dataArray objectAtIndex:i] objectForKey:min] )
                            {
                                [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:min] forKey:@"min"];
                            }
                            if ([[local_dataArray objectAtIndex:i] objectForKey:max] )
                            {
                                [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:max] forKey:@"max"];
                            }
                            if ([[local_dataArray objectAtIndex:i] objectForKey:@"A1"] )
                            {
                                [tempDict setObject:[[local_dataArray objectAtIndex:i] objectForKey:@"A1"] forKey:@"A1"];
                            }
                            [finalArray addObject:tempDict];
                        }
                        UIViewController *object = [[NSClassFromString(nextTemplate) alloc] initWithNibName:nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:nil withPropertyFile:nextTemplatePropertyFile withProcessorCode:processCode dataArray:finalArray dataDictionary:nil];
                        UIViewController *vc = [(UINavigationController *)self.window.rootViewController visibleViewController];
                        [vc.navigationController pushViewController:object animated:NO];
                    }
                }
                else
                {
                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_noresults_found_message", nil)] andErrorCode:nil];
                }
            }
            else if (([webServiceDataObject.TransactionType isEqualToString:TRANSACTION_CODE_MY_PAYEE] || [webServiceDataObject.TransactionType isEqualToString:TRANSACTION_CODE_FETCH_BENEFICIARY_IFSC] || [webServiceDataObject.TransactionType isEqualToString:TRANSACTION_CODE_FETCH_BENEFICIARY_MMID] || [webServiceDataObject.TransactionType isEqualToString:TRANSACTION_CODE_FETCH_BENEFICIARY_NEFT]) && (![webServiceDataObject.ProcessorCode isEqualToString:PROCESSOR_CODE_AGGREGATOR_LINKED_BANK_LIST]))
            {
                if (webServiceDataObject.PaymentDetails2)
                {
                    NSData *jsonData = [webServiceDataObject.PaymentDetails2 dataUsingEncoding:NSUTF8StringEncoding];
                    
                    NSError *jsonError;
                    dictArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError];
                    
                    NSLog(@"Dict Array Count : %lu",(unsigned long)dictArray.count);
                    
                    for (int i=0; i<[dictArray count]; i++)
                    {
                        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                        if ([[dictArray objectAtIndex:i] objectForKey:PAYEE_NAME])
                        {
                            [tempDict setObject:[[dictArray objectAtIndex:i] objectForKey:PAYEE_NAME] forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FIRST_NAME];
                        }
                        
                        if ([[dictArray objectAtIndex:i] objectForKey:PAYEE_NICK_NAME])
                        {
                            [tempDict setObject:[[dictArray objectAtIndex:i] objectForKey:PAYEE_NICK_NAME] forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME];
                        }
                        if ([[dictArray objectAtIndex:i] objectForKey:PAYEE_DEVICE_NUMBER])
                        {
                            [tempDict setObject:[[dictArray objectAtIndex:i] objectForKey:PAYEE_DEVICE_NUMBER] forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER];
                        }
                        
                        if ([[dictArray objectAtIndex:i] objectForKey:PAYEE_FIRST_NAME])
                        {
                            [tempDict setObject:[[dictArray objectAtIndex:i] objectForKey:PAYEE_FIRST_NAME] forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_FIRST_NAME];
                        }
                        if ([[dictArray objectAtIndex:i] objectForKey:PAYEE_LAST_NAME])
                        {
                            [tempDict setObject:[[dictArray objectAtIndex:i] objectForKey:PAYEE_LAST_NAME] forKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_LAST_NAME];
                        }
                        if ([[dictArray objectAtIndex:i] objectForKey:PAYEE_BANK_NAME])
                        {
                            [tempDict setObject:[[dictArray objectAtIndex:i] objectForKey:PAYEE_BANK_NAME] forKey:TRANSACTION_DATA_BANK];
                        }
                        if ([[dictArray objectAtIndex:i] objectForKey:PAYEE_MMID])
                        {
                            [tempDict setObject:[[dictArray objectAtIndex:i] objectForKey:PAYEE_MMID] forKey:TRANSACTION_DATA_BANK_MMID];
                        }
                        if ([[dictArray objectAtIndex:i] objectForKey:PAYEE_IFSC])
                        {
                            [tempDict setObject:[[dictArray objectAtIndex:i] objectForKey:PAYEE_IFSC] forKey:TRANSACTION_DATA_BANK_IFSC_CODE];
                        }
                        if ([[dictArray objectAtIndex:i] objectForKey:PAYEE_ACCOUNT_NUMBER])
                        {
                            [tempDict setObject:[[dictArray objectAtIndex:i] objectForKey:PAYEE_ACCOUNT_NUMBER] forKey:TRANSACTION_DATA_TRANSACTION_ACCOUNT_NUMBER];
                        }
                        [dataArray addObject:tempDict];
                        
                    }
                    if(dataArray.count > 0)
                    {
                        childTemplate2_TableView.frame = CGRectMake(0, nextY_position+5, SCREEN_WIDTH , height_Position-70);
                        alertLabel.hidden = YES;
                    }
                    activityIndicator.hidden = YES;
                    [activityIndicator stopActivityIndicator];
                }
                else
                {
                    //Show Label here
                    [childTemplate2_TableView removeFromSuperview];
                    childTemplate2_TableView.hidden=YES;
                    alertLabel.frame = CGRectMake(10, (SCREEN_HEIGHT/3),self.frame.size.width-20, 50);
                    alertLabel.text=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template2_recent_transactions_not_available",propertyFileName,[NSBundle mainBundle], nil)];
                    alertLabel.textAlignment = NSTextAlignmentCenter;
                    alertLabel.numberOfLines = 2;
                    alertLabel.hidden = NO;
                    [self addSubview:alertLabel];
                    
                }
            }
            else if ([processCode isEqualToString:PROCESSOR_CODE_VIEW_ALL_ACTIVE_BUNDLES])
            {
                NSLog(@"PaymentDetails3 ...%@",webServiceDataObject.PaymentDetails3);
                
                NSArray *transactionHistoryRecords = [webServiceDataObject.PaymentDetails3 componentsSeparatedByString:@";"];
                transactionHistoryRecords = [webServiceDataObject.PaymentDetails3 componentsSeparatedByString:@","];
                NSLog(@"transactionHistoryRecords: %@",transactionHistoryRecords);
                
                int noOfRecords = (int)[transactionHistoryRecords count];
                NSLog(@"noOfRecords: %d",noOfRecords);
                NSMutableDictionary *otherDetails = [[NSMutableDictionary alloc] init];
                NSMutableArray *transActionHistoryArrayList = [[NSMutableArray alloc] init];
                NSMutableDictionary *tempDictionary;
                for (int index = 0; index < noOfRecords; index++)
                {
                    tempDictionary = [[NSMutableDictionary alloc] initWithDictionary:otherDetails];
                    
                    NSMutableArray *transactionFileds = (NSMutableArray *)[[transactionHistoryRecords objectAtIndex:index] componentsSeparatedByString:@"|"];
                    
                    if ([transactionFileds count] > 1)
                    {
                        NSLog(@"transactionFileds at %d: %@",index,transactionFileds);
                        [tempDictionary setObject:[NSString stringWithFormat:@"%d",index+1] forKey:MINI_STATEMENT_COLOUMN_ID];
                        
                        if ([transactionFileds objectAtIndex:0])
                            [tempDictionary setObject:[transactionFileds objectAtIndex:0] forKey:BUNDLE_TRANSACTION_TYPE];
                        else
                            [tempDictionary setObject:@"" forKey:BUNDLE_TRANSACTION_TYPE];
                        
                        if ([transactionFileds objectAtIndex:1])
                            [tempDictionary setObject:[transactionFileds objectAtIndex:1] forKey:BUNDLE_TRANSACTION_AMOUNT];
                        else
                            [tempDictionary setObject:@"" forKey:BUNDLE_TRANSACTION_AMOUNT];
                        
                        if ([transactionFileds objectAtIndex:2])
                            [tempDictionary setObject:[transactionFileds objectAtIndex:2] forKey:BUNDLE_TRANSACTION_VALIDITY];
                        else
                            [tempDictionary setObject:@"" forKey:BUNDLE_TRANSACTION_VALIDITY];
                        
                        if ([transactionFileds objectAtIndex:3])
                            [tempDictionary setObject:[transactionFileds objectAtIndex:3] forKey:BUNDLE_FREE_TRANSACTIONS];
                        else
                            [tempDictionary setObject:@"" forKey:BUNDLE_FREE_TRANSACTIONS];
                        
                        [transactionFileds removeObject:@""];
                        [transActionHistoryArrayList addObject:tempDictionary];
                    }
                    [dataArray setArray:transActionHistoryArrayList];
                    if(dataArray.count > 0)
                    {
                        childTemplate2_TableView.frame = CGRectMake(0, nextY_position+5, SCREEN_WIDTH , height_Position-70);
                        [childTemplate2_TableView reloadData];
                        alertLabel.hidden = YES;
                    }
                    else
                    {
                        childTemplate2_TableView.hidden=YES;
                        alertLabel.frame = CGRectMake(10, (SCREEN_HEIGHT/3),self.frame.size.width-20, 50);
                        alertLabel.text=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template2_recent_transactions_not_available",propertyFileName,[NSBundle mainBundle], nil)];
                        alertLabel.textAlignment = NSTextAlignmentCenter;
                        alertLabel.numberOfLines = 2;
                        alertLabel.hidden = NO;
                        [self addSubview:alertLabel];
                    }
                }
                NSLog(@"TempDictionary...%@",[tempDictionary description]);
                NSLog(@"TempDictionary...%@",dataArray);
            }
    }
}

#pragma mark - Long Tap Recognizer Selector
/*
 * This method is used to set childtemplate2 listview Long press Show The list details Data.
 */
- (void)cellLongTapped : (UILongPressGestureRecognizer *)sender
{
    CGPoint p = [sender locationInView:self.childTemplate2_TableView];
    NSIndexPath *indexPath = [self.childTemplate2_TableView indexPathForRowAtPoint:p];
    
    if (sender.state == UIGestureRecognizerStateBegan) {
        
        NSString *nextProperty = NSLocalizedStringFromTableInBundle(@"child_template2_list_longpress_next_template", propertyFileName, [NSBundle mainBundle], nil);
        NSString *nextPropertyFileName = NSLocalizedStringFromTableInBundle(@"child_template2_list_longpress_next_template_properties_file", propertyFileName, [NSBundle mainBundle], nil);
        
        Class myclass = NSClassFromString(nextProperty);
        if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
        {
            id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextPropertyFileName andDelegate:self withDataDictionary:[dataArray objectAtIndex:indexPath.row] withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
            [[[self superview] superview] addSubview:(UIView *)obj];
        }
    }
}

#pragma mark - Button actions
/*
 * This method is used to set childtemplate2 listview Selcted Language value notification.
 */
//- (void) ct2SetValue:(NSNotification *)notification
//{
//    if ([notification.object isEqualToString:PROCESSOR_CODE_CHANGE_LANG]) {
//        NSArray *viewsArray = [[NSArray alloc] initWithArray:[[[self superview] superview] subviews]];
//        for (int i = 0; i<[viewsArray count]; i++)
//        {
//            UIView *lView = [viewsArray objectAtIndex:i];
//            if (lView.tag == -3)
//            {
//                [lView removeFromSuperview];
//            }
//        }
//        if (startIndexPath)
//        {
//            [[[[childTemplate2_TableView cellForRowAtIndexPath:startIndexPath] contentView] viewWithTag:101] removeFromSuperview];
//        }
//
//        if (previousSelectedIndex)
//        {
//            [[[[childTemplate2_TableView cellForRowAtIndexPath:previousSelectedIndex] contentView] viewWithTag:100] removeFromSuperview];
//            previousSelectedIndex = selectedIndex;
//        }
//
//        UIImageView *myView = [[UIImageView alloc] init];
//        myView.frame=CGRectMake(SCREEN_WIDTH-50, 7.0f, 30.0f, 30.0f);
//        myView.image=[UIImage  imageNamed:SELECTED_CHECK_MARK_IMAGE];
//        [myView setTag:100];
//        [local_cell.contentView addSubview:myView];
//        valueLabel.text = userSelectedLanguage;
//
//        NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
//        NSString *value=[[dataArray objectAtIndex:selectedIndex.row]valueForKey:DROP_DOWN_TYPE_NAME];
//        [sta setObject:value forKey:userLanguage];
//        [sta synchronize];
//
//        NSString *dataValuestr =  NSLocalizedStringFromTableInBundle(@"child_template2_list_data_to_load_webservice_api",propertyFileName,[NSBundle mainBundle], nil);
//        NSString *local_processorCodestr=[Template getProcessorCodeWithData:dataValuestr];
//        NSString *transactionTypestr = [Template getTransactionCodeWithData:dataValuestr];
//        NSString *formatString = [NSString stringWithFormat:@"%@_%@",local_processorCodestr,transactionTypestr];
//
//
//        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString(formatString, nil)] andErrorCode:nil];
//
//        [self performSelector:@selector(poptoRootView) withObject:nil afterDelay:2.0];
//
//    }
//    if ([notification.object isEqualToString:PROCESSOR_CODE_BANK]) {
//        [BaseViewController getDataFromProcessoreCode:notification.object andPropertyFileName:propertyFileName forArray:&dataArray];
//        [childTemplate2_TableView reloadData];
//    }
//}
///*
// * This method is used to set move to Previous View.
// */
//-(void)poptoRootView{
//    AppDelegate *apDlgt = (AppDelegate *)[[UIApplication sharedApplication]delegate];
//
//    NSString *dataVal = NSLocalizedStringFromTableInBundle(@"child_template2_list_data_to_load_webservice_api",propertyFileName,[NSBundle mainBundle], nil);
//    NSString *process_code = [Template getProcessorCodeWithData:dataVal];
//
//    if ([process_code isEqualToString:PROCESSOR_CODE_CHANGE_LANG])
//    {
//        UIWindow *window=apDlgt.window;
//        NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
//        if ([[userDefaults valueForKey:userLanguage] isEqualToString:ar])
//        {
//
//            window.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
//            PUSH_VIEW.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
//        }
//        else
//        {
//
//            window.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
//            PUSH_VIEW.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
//        }
//        AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate] ;
//        [appDelegate performSelector:@selector(getDropdownList) withObject:nil afterDelay:1.0];
//    }
//
//    UINavigationController *navCntrl = (UINavigationController *)apDlgt.window.rootViewController;
//    if (navCntrl && [navCntrl isKindOfClass:[UINavigationController class]])
//    {
//        [navCntrl popViewControllerAnimated:NO];
//    }
//
//}
#pragma mark - Button actions
/*
 * This method is used to set childtemplate2 listview Selcted Language value notification.
 */
- (void) ct2SetValue:(NSNotification *)notification
{
    if ([notification.object isEqualToString:PROCESSOR_CODE_CHANGE_LANG]) {
        NSArray *viewsArray = [[NSArray alloc] initWithArray:[[[self superview] superview] subviews]];
        for (int i = 0; i<[viewsArray count]; i++)
        {
            UIView *lView = [viewsArray objectAtIndex:i];
            if (lView.tag == -3)
            {
                [lView removeFromSuperview];
            }
        }
        if (startIndexPath)
        {
            [[[[childTemplate2_TableView cellForRowAtIndexPath:startIndexPath] contentView] viewWithTag:101] removeFromSuperview];
        }
        
        if (previousSelectedIndex)
        {
            [[[[childTemplate2_TableView cellForRowAtIndexPath:previousSelectedIndex] contentView] viewWithTag:100] removeFromSuperview];
            previousSelectedIndex = selectedIndex;
        }
        
        UIImageView *myView = [[UIImageView alloc] init];
        myView.frame=CGRectMake(SCREEN_WIDTH-50, 7.0f, 30.0f, 30.0f);
        myView.image=[UIImage  imageNamed:SELECTED_CHECK_MARK_IMAGE];
        [myView setTag:100];
        [local_cell.contentView addSubview:myView];
        
        valueLabel.text = userSelectedLanguage;
        NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
        [sta setObject:userSelectedLanguage forKey:userLanguage];
        [sta synchronize];
        
        NSString *dataValuestr =  NSLocalizedStringFromTableInBundle(@"child_template2_list_data_to_load_webservice_api",propertyFileName,[NSBundle mainBundle], nil);
        NSString *local_processorCodestr=[Template getProcessorCodeWithData:dataValuestr];
        NSString *transactionTypestr = [Template getTransactionCodeWithData:dataValuestr];
        NSString *formatString = [NSString stringWithFormat:@"%@_%@",local_processorCodestr,transactionTypestr];
        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString(formatString, nil)] andErrorCode:nil];
        [self performSelector:@selector(poptoRootView) withObject:nil afterDelay:3.0];
    }
    if ([notification.object isEqualToString:PROCESSOR_CODE_CHANGE_LANG_T3]) {
        NSArray *viewsArray = [[NSArray alloc] initWithArray:[[[self superview] superview] subviews]];
        for (int i = 0; i<[viewsArray count]; i++)
        {
            UIView *lView = [viewsArray objectAtIndex:i];
            if (lView.tag == -3)
            {
                [lView removeFromSuperview];
            }
        }
        if (startIndexPath)
        {
            [[[[childTemplate2_TableView cellForRowAtIndexPath:startIndexPath] contentView] viewWithTag:101] removeFromSuperview];
        }
        
        if (previousSelectedIndex)
        {
            [[[[childTemplate2_TableView cellForRowAtIndexPath:previousSelectedIndex] contentView] viewWithTag:100] removeFromSuperview];
            previousSelectedIndex = selectedIndex;
        }
        
        UIImageView *myView = [[UIImageView alloc] init];
        myView.frame=CGRectMake(SCREEN_WIDTH-50, 7.0f, 30.0f, 30.0f);
        myView.image=[UIImage  imageNamed:SELECTED_CHECK_MARK_IMAGE];
        [myView setTag:100];
        NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
        
        [local_cell.contentView addSubview:myView];
        valueLabel.text = userSelectedLanguage;
        NSString *value=[[dataArray objectAtIndex:selectedIndex.row]valueForKey:DROP_DOWN_TYPE_NAME];
        [sta setObject:value forKey:userLanguage];
        [sta synchronize];
        
        NSString *dataValuestr =  NSLocalizedStringFromTableInBundle(@"child_template2_list_data_to_load_webservice_api",propertyFileName,[NSBundle mainBundle], nil);
        NSString *local_processorCodestr=[Template getProcessorCodeWithData:dataValuestr];
        NSString *transactionTypestr = [Template getTransactionCodeWithData:dataValuestr];
        NSString *formatString = [NSString stringWithFormat:@"%@_%@",local_processorCodestr,transactionTypestr];
        [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString(formatString, nil)] andErrorCode:nil];
        [self performSelector:@selector(poptoRootView) withObject:nil afterDelay:2.0];
    }
    if ([notification.object isEqualToString:PROCESSOR_CODE_BANK]) {
        [BaseViewController getDataFromProcessoreCode:notification.object andPropertyFileName:propertyFileName forArray:&dataArray];
        [childTemplate2_TableView reloadData];
    }
}
/*
 * This method is used to set move to Previous View.
 */
-(void)poptoRootView{
    AppDelegate *apDlgt = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSString *dataVal = NSLocalizedStringFromTableInBundle(@"child_template2_list_data_to_load_webservice_api",propertyFileName,[NSBundle mainBundle], nil);
    NSString *process_code = [Template getProcessorCodeWithData:dataVal];
    
    if ([process_code isEqualToString:PROCESSOR_CODE_CHANGE_LANG_T3])
    {
        UIWindow *window=apDlgt.window;
        NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
        if ([[userDefaults valueForKey:userLanguage] isEqualToString:ar])
        {
            window.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
            PUSH_VIEW.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
        }
        else
        {
            window.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
            PUSH_VIEW.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
        }
        
        
        AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate] ;
        [appDelegate performSelector:@selector(getDropdownList) withObject:nil afterDelay:1.0];
        
    }
    
    
    
    
    UINavigationController *navCntrl = (UINavigationController *)apDlgt.window.rootViewController;
    if (navCntrl && [navCntrl isKindOfClass:[UINavigationController class]])
    {
        [navCntrl popViewControllerAnimated:NO];
    }
}


#pragma mark - Xml Error Handling methods.
/*
 * This method is used to set process data error handling.
 */

-(void)errorCodeHandlingInDBWithCode:(NSString *)errorCode withProcessorCode:(NSString *)processorCode
{
    [self removeActivityIndicator];
}

-(void)errorCodeHandlingInXML:(NSString *)errorCode andMessgae:(NSString *)message withProcessorCode:(NSString *)processorCode
{
    [self removeActivityIndicator];
    [self showAlertForErrorWithMessage:message andErrorCode:errorCode];
}

-(void)errorCodeHandlingInWebServiceRunningWithErrorCode:(NSString *)errorCode
{
    [self removeActivityIndicator];
    [self showAlertForErrorWithMessage:nil andErrorCode:errorCode];
}
/*
 * This method is used to set process data error handling show alert based on type(Ticker or popup).
 */
-(void)showAlertForErrorWithMessage:(NSString *)message andErrorCode:(NSString *)errorCode
{
    [self removeActivityIndicator];
    NSString *errorMessage = nil;
    
    if (errorCode)
        errorMessage = NSLocalizedStringFromTableInBundle(errorCode,[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil);
    else
        errorMessage = message;
    
    alertview_Type=NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    
    NSLog(@"AlertView Type is...%@",alertview_Type);
    
    if([alertview_Type compare:TICKER_TEXT] == NSOrderedSame)
    {
        NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
        
        NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
        
        [[[self superview] superview] makeToast:errorMessage duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
    }
    else if ([alertview_Type compare:POPUP_TEXT] == NSOrderedSame)
    {
        PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]] andMessage:errorMessage withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
        
        
        NSString *dataVal = NSLocalizedStringFromTableInBundle(@"child_template2_list_data_to_load_webservice_api",propertyFileName,[NSBundle mainBundle], nil);
        NSString *process_code = [Template getProcessorCodeWithData:dataVal];
        
        if ([process_code isEqualToString:PROCESSOR_CODE_CHANGE_LANG])
        {
            NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
            if ([[userDefaults valueForKey:userLanguage] isEqualToString:ar]||[[userDefaults valueForKey:userLanguage] isEqualToString:en])
            {
                popup.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
            }
        }
        
        [[[self superview] superview] addSubview:popup];
    }
    
}
/*
 * This method is used to remove Notification.
 */
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end


