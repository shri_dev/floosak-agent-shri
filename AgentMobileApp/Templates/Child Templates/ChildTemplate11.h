//
//  ChildTemplate11.h
//  Consumer Client
//
//  Created by android on 7/1/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityIndicator.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "CustomTextField.h"
/**
 * This class used to handle functionality and View of Childtemplate11
 *
 * @author Integra
 *
 */
@interface ChildTemplate11 : UIView<UITextFieldDelegate>
{
    NSMutableDictionary *local_DataDictionary;
    /**
     Declare a toolbar for pickerview
     */
    UIToolbar *numberToolbar;
    CustomTextField *activeField;
    NSMutableArray *validationsArray;
    NSMutableArray *labelValidationsArray;
    ActivityIndicator *activityIndicator;
    
    NSString *dropdownNextTemplateProperty;
    NSString *dropdownNextTemplate;
    
    NSString *dropdownString;
    UIButton *btn;
    NSArray *dropDownDataArray;
    NSMutableDictionary *datavalueDictionary;
    
    NSString *areaString;
    NSString *selectedAreaCode;
    NSString *selectedAreaName;
    

}
/**
 * declarations are used to set the UIConstraints Of ChildTemplate11.
 * Label,Value label,Border label,Dropdown and Buttons.
 */
@property(nonatomic,strong) NSString *propertyFileName;
@property(nonatomic,strong) UILabel *childTemplateLabel;
@property(nonatomic,strong) UILabel *valueLabel;
@property(nonatomic,strong) UIScrollView *childTemplateScrollView;

@property(nonatomic,strong) UILabel *headingTitle_label;
@property(nonatomic,strong) UILabel *headingTitle_border_label;

@property(nonatomic,strong) UILabel *key_label1;
@property(nonatomic,strong) UILabel *value_label1;
@property(nonatomic,strong) UILabel *value_border_label1;

@property(nonatomic,strong) UILabel *key_label2;
@property(nonatomic,strong) UILabel *value_label2;
@property(nonatomic,strong) UILabel *value_border_label2;

@property(nonatomic,strong) UILabel *key_label3;
@property(nonatomic,strong) UILabel *value_label3;
@property(nonatomic,strong) UILabel *value_border_label3;

@property(nonatomic,strong) UILabel *key_label4;
@property(nonatomic,strong) UILabel *value_label4;
@property(nonatomic,strong) UILabel *value_border_label4;

@property(nonatomic,strong) UILabel *key_label5;
@property(nonatomic,strong) UILabel *value_label5;
@property(nonatomic,strong) UILabel *value_border_label5;

@property(nonatomic,strong) UILabel *key_label6;
@property(nonatomic,strong) UILabel *value_label6;
@property(nonatomic,strong) UILabel *value_border_label6;

@property(nonatomic,strong) UILabel *key_label7;
@property(nonatomic,strong) UILabel *value_label7;
@property(nonatomic,strong) UILabel *value_border_label7;

@property(nonatomic,strong) UILabel *key_label8;
@property(nonatomic,strong) UILabel *value_label8;
@property(nonatomic,strong) UILabel *value_border_label8;

@property(nonatomic,strong) UILabel *key_label9;
@property(nonatomic,strong) UILabel *value_label9;
@property(nonatomic,strong) UILabel *value_border_label9;

@property(nonatomic,strong) UILabel *key_label10;
@property(nonatomic,strong) UILabel *value_label10;
@property(nonatomic,strong) UILabel *value_border_label10;

@property(nonatomic,strong) UILabel *input_key_label1;
@property(nonatomic,strong) CustomTextField *input_value_field1;
@property(nonatomic,strong) UILabel *input_value_border_label1;
@property(nonatomic,strong) UIButton *dropDownButton1;

@property(nonatomic,strong) UILabel *input_key_label2;
@property(nonatomic,strong) CustomTextField *input_value_field2;
@property(nonatomic,strong) UILabel *input_value_border_label2;
@property(nonatomic,strong) UIButton *dropDownButton2;

@property(nonatomic,strong) UILabel *input_key_label3;
@property(nonatomic,strong) CustomTextField *input_value_field3;
@property(nonatomic,strong) UILabel *input_value_border_label3;
@property(nonatomic,strong) UIButton *dropDownButton3;

@property(nonatomic,strong) UILabel *input_key_label4;
@property(nonatomic,strong) CustomTextField *input_value_field4;
@property(nonatomic,strong) UILabel *input_value_border_label4;
@property(nonatomic,strong) UIButton *dropDownButton4;

@property(nonatomic,strong) UILabel *input_key_label5;
@property(nonatomic,strong) CustomTextField *input_value_field5;
@property(nonatomic,strong) UILabel *input_value_border_label5;
@property(nonatomic,strong) UIButton *dropDownButton5;

@property(nonatomic,strong) UIButton *button1;
@property(nonatomic,strong) UIButton *button2;

/**
 * This method is  used for Method Initialization of ChildTemplate11.
 */
// Method For initialization.
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView;
@end
