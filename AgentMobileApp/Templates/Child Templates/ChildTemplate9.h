//
//  ChildTemplate9.h
//  Consumer Client
//
//  Created by android on 6/18/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XmlParserHandler.h"
#import "ActivityIndicator.h"
#import "UIView+Toast.h"
#import "DatabaseManager.h"
#import "DatabaseConstatants.h"
/**
 * This class used to handle functionality and View of Childtemplate9
 *
 * @author Integra
 *
 */
@interface ChildTemplate9 : UIView <UIWebViewDelegate>
{
    ActivityIndicator *activityIndicator;
    NSString *alertview_Type;
    
    NSString *nextTemplateProperty;
    NSString *nextTemplate;

    NSArray *local_dataArray;
    NSMutableDictionary *local_dataDictionary;
    
    
    NSString *local_processorCode;
    DatabaseManager *dataManager;
    
    int labelX_Position;
    int nextY_Position;
    int distanceY;

}

@property (strong,nonatomic)NSString *controllerName;
@property (strong,nonatomic)NSString *propertyFileName;
/**
 * declarations are used to set the UIConstraints Of ChildTemplate9.
 * Label,Value label,Border label,Dropdown and Buttons.
 */
@property (strong,nonatomic)UILabel *headerTitle;
@property (strong,nonatomic)UIWebView *contentWebView;
@property (strong,nonatomic)UIButton *childButton1;
@property (strong,nonatomic)UIButton *childButton2;

/**
 * This method is  used for Method Initialization of ChildTemplate9.
 */
// Method For initialization.
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView;
/*
 * This method is used to add childtemplate9 Button action declaration.
 */
// Button Action
-(void)button1Action:(id)sender;
-(void)button2Action:(id)sender;

@end
