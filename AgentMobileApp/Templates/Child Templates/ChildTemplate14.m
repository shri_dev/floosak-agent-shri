//
//  ChildTemplate14.m
//  Consumer Client
//
//  Created by test on 29/10/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "ChildTemplate14.h"
#import "Constants.h"
#import "ValidationsClass.h"
#import "BaseViewController.h"
#import "ParentTemplate1.h"
#import "ParentTemplate2.h"
#import "AppDelegate.h"
#import "UIView+Toast.h"


@implementation ChildTemplate14

@synthesize propertyFileName,childTableView,dataArray,headerLabel;

#pragma mark - ChildTemplate14 UIView.
/**
 * This method is used to set implemention of childTemplate14.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView
{
    
    NSLog(@"PropertyFileName:%@",propertyFile);
    self = [self initWithFrame:frame];
    
    if (self){
        propertyFileName=propertyFile;
        superComponent = superView;
        [self addControlsForView];
    }
    
    return self;
}

#pragma mark - ChildTemplate14 UIConstraints Creation.
/**
 * This method is used to set add UIconstraints Frame of ChildTemplate14.
 @param Type- Tableview
 * Set label Text (Size,color and font size).
 */
-(void)addControlsForView{
    
    int x_Position=0;
    int y_Position=0;
    
   if ([NSLocalizedStringFromTableInBundle(@"child_template14_header_text_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame) {
       headerLabel=[[UILabel alloc]init];
       headerLabel.frame=CGRectMake(x_Position, y_Position, 200, 20);
       headerLabel.text=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template14_header_text",propertyFileName,[NSBundle mainBundle], nil)];
       
       headerLabel.textAlignment=NSTextAlignmentCenter;
       headerLabel.lineBreakMode = NSLineBreakByTruncatingTail;
       
       if ([NSLocalizedStringFromTableInBundle(@"child_template14_header_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
       {
           // Properties for label TextColor.
           
           NSArray *valueLabelTextColor;
           
           if (NSLocalizedStringFromTableInBundle(@"child_template14_header_text_color",propertyFileName,[NSBundle mainBundle], nil))
               valueLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template14_header_text_color",propertyFileName,[NSBundle mainBundle], nil)];
           
           else if (NSLocalizedStringFromTableInBundle(@"child_template14_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
               valueLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template14_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
           
           else
               valueLabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
           
           if (valueLabelTextColor)
               headerLabel.textColor = [UIColor colorWithRed:[[valueLabelTextColor objectAtIndex:0] floatValue] green:[[valueLabelTextColor objectAtIndex:1] floatValue] blue:[[valueLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
           
           // Properties for label Textstyle.
           
           NSString *textStyle;
           if(NSLocalizedStringFromTableInBundle(@"child_template14_header_text_font",propertyFileName,[NSBundle mainBundle], nil))
               textStyle =NSLocalizedStringFromTableInBundle(@"child_template14_header_text_font",propertyFileName,[NSBundle mainBundle], nil);
           else if(NSLocalizedStringFromTableInBundle(@"child_template14_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
               textStyle =NSLocalizedStringFromTableInBundle(@"child_template14_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
           else
               textStyle =application_default_text_style;
           
           
           // Properties for label Font size.
           
           NSString *fontSize;
           if(NSLocalizedStringFromTableInBundle(@"child_template14_header_text_size",propertyFileName,[NSBundle mainBundle], nil))
               fontSize =NSLocalizedStringFromTableInBundle(@"child_template14_header_text_size",propertyFileName,[NSBundle mainBundle], nil);
           else if(NSLocalizedStringFromTableInBundle(@"child_template14_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
               fontSize =NSLocalizedStringFromTableInBundle(@"child_template14_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
           else
               fontSize =application_default_text_size;
           
           if ([textStyle isEqualToString:TEXT_STYLE_0])
               headerLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
           
           else if ([textStyle isEqualToString:TEXT_STYLE_1])
               headerLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
           
           else if ([textStyle isEqualToString:TEXT_STYLE_2])
               headerLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
           
           else
               headerLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
       }
       else{
           //Default Properties for label textcolor
           
           NSArray *valueLabelTextColor ;
           if (NSLocalizedStringFromTableInBundle(@"child_template14_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
               valueLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template14_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
           else
               valueLabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
           if (valueLabelTextColor)
               headerLabel.textColor = [UIColor colorWithRed:[[valueLabelTextColor objectAtIndex:0] floatValue] green:[[valueLabelTextColor objectAtIndex:1] floatValue] blue:[[valueLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
           
           
           //Default Properties for label textStyle
           
           NSString *textStyle;
           if(NSLocalizedStringFromTableInBundle(@"child_template14_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
               textStyle =NSLocalizedStringFromTableInBundle(@"child_template14_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
           else
               textStyle =application_default_text_style;
           
           //Default Properties for label fontSize
           
           NSString *fontSize;
           if(NSLocalizedStringFromTableInBundle(@"child_template14_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
               fontSize =NSLocalizedStringFromTableInBundle(@"child_template14_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
           else
               fontSize =application_default_text_size;
           
           if ([textStyle isEqualToString:TEXT_STYLE_0])
               headerLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
           
           else if ([textStyle isEqualToString:TEXT_STYLE_1])
               headerLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
           
           else if ([textStyle isEqualToString:TEXT_STYLE_2])
               headerLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
           
           else
               headerLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
       }
       headerLabel.numberOfLines=0;
       y_Position=headerLabel.frame.origin.y+headerLabel.frame.size.height;
       [self addSubview:headerLabel];
    }
    childTableView=[[UITableView alloc]init];
    childTableView.delegate=self;
    childTableView.dataSource=self;
    childTableView.frame=CGRectMake(x_Position,y_Position, SCREEN_WIDTH, SCREEN_HEIGHT-64);
    childTableView.backgroundColor=[UIColor clearColor];
    [childTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [childTableView setTableFooterView:[UIView new]];
    [self addSubview:childTableView];
    
    NSString *templateId = NSLocalizedStringFromTableInBundle(@"child_template14_list_item_id",propertyFileName,[NSBundle mainBundle], nil);
    if (![templateId isEqualToString:@""]) {
        templateIdArray = [templateId componentsSeparatedByString:@","];
    }
    dataArray= [NSLocalizedStringFromTableInBundle(@"child_template14_list_item_name",propertyFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","];
}

#pragma mark - TableView dataSource And Delegate method.
/**
 * This method is used to set add Tableview (list and delegate methods) of ChildTemplate14.
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [dataArray count];
}
/**
 * This method is used to set add Tableview for ChildTemplate14.
 *@param type- Declare Listview related UI(Labels,Images etc..base on Req).
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = [NSString stringWithFormat:@"%@%d",@"Cell",(int)indexPath.row];
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    int nextY_postion=15.0;
    int labelX_position=5.0;
    
    UILabel *valueLabel1=[[UILabel alloc]init];
    if ([NSLocalizedStringFromTableInBundle(@"child_template14_list_item_label_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        valueLabel1.frame=CGRectMake(labelX_position+20, nextY_postion-8, SCREEN_WIDTH-96-labelX_position, 40);
        valueLabel1.text=[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:[dataArray objectAtIndex:indexPath.row]]];
        valueLabel1.lineBreakMode = NSLineBreakByTruncatingTail;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template14_list_item_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *valueLabelTextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template14_listitem_label_color",propertyFileName,[NSBundle mainBundle], nil))
                valueLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template14_listitem_label_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template14_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                valueLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template14_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                valueLabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (valueLabelTextColor)
                valueLabel1.textColor = [UIColor colorWithRed:[[valueLabelTextColor objectAtIndex:0] floatValue] green:[[valueLabelTextColor objectAtIndex:1] floatValue] blue:[[valueLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            // Properties for label Textstyle.
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template14_listitem_label_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"child_template14_listitem_label_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template14_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"child_template14_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template14_listitem_label_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize =NSLocalizedStringFromTableInBundle(@"child_template14_listitem_label_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template14_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize =NSLocalizedStringFromTableInBundle(@"child_template14_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize =application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                valueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                valueLabel1.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                valueLabel1.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                valueLabel1.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        else{
            //Default Properties for label textcolor
            
            NSArray *valueLabelTextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template14_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                valueLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template14_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                valueLabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (valueLabelTextColor)
                valueLabel1.textColor = [UIColor colorWithRed:[[valueLabelTextColor objectAtIndex:0] floatValue] green:[[valueLabelTextColor objectAtIndex:1] floatValue] blue:[[valueLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template14_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle =NSLocalizedStringFromTableInBundle(@"child_template14_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template14_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize =NSLocalizedStringFromTableInBundle(@"child_template14_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize =application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                valueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                valueLabel1.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                valueLabel1.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                valueLabel1.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        valueLabel1.numberOfLines=0;
        valueLabel1.textAlignment=NSTextAlignmentJustified;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell.contentView addSubview:valueLabel1];
    }
    cell.backgroundView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"cellborder.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0]];
    
    return cell;
}
/**
 * This method is used to set add Tableview Delegate Method User selects List .
 *@param type- Declare Listview - (NextTemplate and Next template proprty file).
 *To show List Item Detailed Data etc..
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *templateArray=[(NSLocalizedStringFromTableInBundle(@"child_template14_list_next_template",propertyFileName,[NSBundle mainBundle], nil)) componentsSeparatedByString:@","];
    NSArray *nextTemplateArray = [(NSLocalizedStringFromTableInBundle(@"child_template14_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil)) componentsSeparatedByString:@","];
    Class myclass = NSClassFromString([templateArray objectAtIndex:indexPath.row]);
    
    int selected = (int)indexPath.row;
    
    //To be removed
    NSUserDefaults *userDefaultsData=[NSUserDefaults standardUserDefaults];
    [userDefaultsData setValue:[Localization languageSelectedStringForKey:[dataArray objectAtIndex:selected]] forKey:@"topupType"];
    
    //adding Static Id
    NSString *labelID = [dataArray objectAtIndex:selected];
//    [userDefaultsData setValue:[Localization languageSelectedStringForKey:labelID] forKey:@"staticListValue"];
    [userDefaultsData setValue:labelID forKey:@"staticListValue"];
    
    [userDefaultsData setValue:[templateIdArray objectAtIndex:indexPath.row]  forKey:@"staticListId"];
    [userDefaultsData synchronize];
    
    //To be removed
    [dictionary setObject:[userDefaultsData objectForKey:@"topupType"] forKey:@"topupType"];
    
    id obj = nil;
    if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
    {
        obj = [[myclass alloc] initWithNibName:[templateArray objectAtIndex:indexPath.row] bundle:nil withSelectedIndex:(int)[[NSUserDefaults standardUserDefaults] integerForKey:@"selected_index"] fromView:0 withFromView:[NSString stringWithFormat:@"%d",(int)(indexPath.row+1)]  withPropertyFile:[nextTemplateArray objectAtIndex:indexPath.row] withProcessorCode:nil dataArray:nil dataDictionary:dictionary?dictionary:nil];
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UINavigationController *navController = (UINavigationController *)delegate.window.rootViewController;
        [navController pushViewController:obj animated:NO];
    }
      else if ([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray:withSubIndex:)])
      {
        
//          Reachability *reachability = [Reachability reachabilityForInternetConnection];
//          NetworkStatus netStatus = [reachability  currentReachabilityStatus];
//          NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
//          NSString  *mode = [[standardUserDefaults objectForKey:@"userDetails"] objectForKey:PARAMETER36];
//
              obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:[nextTemplateArray objectAtIndex:indexPath.row] andDelegate:self withDataDictionary:dictionary?dictionary:nil withProcessorCode:nil withTag:-2 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
              //[self removeFromSuperview];
              [[[self superview] superview] addSubview:(UIView *)obj];

          
//        else if (netStatus == NotReachable || [mode isEqualToString:@"SMS"])
//          {
//
//              NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
//              NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
//              NSString *appendingString = [[[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_label_prefix", nil)] stringByAppendingString:[NSString stringWithFormat:@"_1005"]] stringByAppendingString:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_error_code_error_msg_seperator", nil)]];
//              [[[self superview] superview] makeToast:[NSString stringWithFormat:@"%@%@",appendingString,NSLocalizedStringFromTableInBundle(@"_1005",[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil)] duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
//
//
//          }
//          else
//          {
//              obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:[nextTemplateArray objectAtIndex:indexPath.row] andDelegate:self withDataDictionary:dictionary?dictionary:nil withProcessorCode:nil withTag:-2 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
//              //[self removeFromSuperview];
//              [[[self superview] superview] addSubview:(UIView *)obj];
//          }
          
      }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


/*
 * This method is used to add ChildTemplate1 Localdelegate for communicates one vie to another view.
 */
-(void)setLocalDelegate:(id)fromDelegate
{
}


@end
