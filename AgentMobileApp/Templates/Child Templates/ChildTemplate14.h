//
//  ChildTemplate14.h
//  Consumer Client
//
//  Created by test on 29/10/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Localization.h"

/**
 * This class used to handle functionality and View of Childtemplate14.
 *
 * @author Integra
 *
 */
@interface ChildTemplate14 : UIView<UITableViewDataSource,UITableViewDelegate>
{
    UIView *superComponent;
    NSMutableDictionary *dictionary;
    NSArray *templateIdArray;
}
/**
 * declarations are used to set the UIConstraints Of ChildTemplate14.
 * Label and TableView.
 */
@property(nonatomic,strong)NSString *propertyFileName;
@property(nonatomic,strong)NSArray *dataArray;
@property(nonatomic,strong)UILabel *headerLabel;
@property(nonatomic,strong)UITableView *childTableView;

/**
 * This method is  used for Method Initialization of ChildTemplate14.
 */
// Method For initialization.
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView;

/*
 * This method is used to add Childtemplate14 Delegate method.
 */
-(void)setLocalDelegate:(id)fromDelegate;



@end
