//
//  ChildTemplate4.m
//  Consumer Client
//
//  Created by Integra Micro on 06/05/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//

#import "ChildTemplate4.h"
#import "Constants.h"
#import "ValidationsClass.h"
#import "Localization.h"
#import "WebSericeUtils.h"
#import "WebServiceConstants.h"
#import "WebServiceRequestFormation.h"
#import "WebServiceRunning.h"
#import "WebServiceDataObject.h"
#import "UIView+Toast.h"
#import "DatabaseConstatants.h"
#import "PopUpTemplate2.h"
#import "PopupTemplate6.h"
#import "ParentTemplate6.h"
#import "Template.h"
#import "NotificationConstants.h"
#import "ParentTemplate6.h"
#import "AppDelegate.h"

@implementation ChildTemplate4

@synthesize delegate,propertyFileName;
@synthesize childButton;
@synthesize headerTitle_Label,headerborder_Label,childTemplate4_TableView;
@synthesize childTemplate4_Label1,childTemplate4_ValueLabel1,childTemplate4_ValueLabel2,childTemplate4_ValueLabel3;
@synthesize alertLabel;

#pragma mark - ChildTemplate4 UIView.
/**
 * This method is used to set implemention of childTemplate4.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView
{
    NSLog(@"PropertyFileName:%@",propertyFile);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ct4SetValue:) name:CHILD_TEMPLATE4 object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ct4Cancel:) name:CHILD_TEMPLATE4_CANCEL object:nil];
    
    self = [super initWithFrame:frame];
    if (self)
    {
        propertyFileName = propertyFile;
        hasSelectionOperation = hasDidSelectFunction;
        dictArray = dataDictsArray;
        pin = MPIN;
        
        NSLog(@"Mpin Is...%@",pin);
        /*
         * This method is used to add childtemplate4 frame  UIconstraints.
         */
        [self addControlesToView];
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
        NSLog(@"Subviews in ct4 : %@", [self subviews]);
        if([propertyFileName isEqualToString:@"PayBillMyBillersCT4"]){
//            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshView:) name:@"PayBillNewBillerAdded" object:nil];
        }
    }
    return self;
}

#pragma mark - ChildTemplate4 UIConstraints creation.
/**
 * This method is used to set add UIconstraints Frame of ChildTemplate2.
 @param Type- label and Tableview
 * Set label Text (Size,color and font size).
 */
-(void)addControlesToView
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"refreshView"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    labelX_Position = 0.0;
    labelX_Position=10.0;
    alertLabel = [[UILabel alloc] init];
    alertLabel.frame=CGRectMake(10,(self.frame.size.height/2),self.frame.size.width-20,50);
    /*
     * This method is used to add childtemplate4 alert label.
     */
    alertLabel.text=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template4_recent_transactions_not_available",propertyFileName,[NSBundle mainBundle], nil)];
    
    NSLog(@"Property file : %@ ",propertyFileName);
    NSLog(@"property name : %@",NSLocalizedStringFromTableInBundle(@"child_template4_recent_transactions_not_available",propertyFileName,[NSBundle mainBundle], nil));
    
    alertLabel.numberOfLines = 2;
    alertLabel.textAlignment = NSTextAlignmentCenter;
    [alertLabel sizeToFit];
    if([finalTranArray count]==0){
        [self addSubview:alertLabel];
    }
    
    /*
     * This method is used to add childtemplate4 headerTitle Label Visibility.
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template4_header_text_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        headerTitle_Label=[[UILabel alloc]init];
//        headerTitle_Label.frame=CGRectMake(0,0, SCREEN_WIDTH, 60);
        headerTitle_Label.frame=CGRectMake(0,-20, SCREEN_WIDTH, 60);
        /*
         * This method is used to add childtemplate4 headerTitle titlename.
         */
        NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template4_header_text",propertyFileName,[NSBundle mainBundle], nil)];
        if (headerStr)
            headerTitle_Label.text=headerStr;
        /*
         * This method is used to add childtemplate4 headerTitle fontattributes override.
         */
        if ([NSLocalizedStringFromTableInBundle(@"child_template4_header_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            /*
             * This method is used to add childtemplate4 Properties for label TextColor..
             */
            // Properties for label TextColor.
            NSArray *labelTextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template4_header_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_header_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelTextColor)
                headerTitle_Label.textColor = [UIColor colorWithRed:[[labelTextColor objectAtIndex:0] floatValue] green:[[labelTextColor objectAtIndex:1] floatValue] blue:[[labelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            /*
             * This method is used to add childtemplate4 Properties for label Textstyle.
             */
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template4_header_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_header_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            /*
             * This method is used to add childtemplate4 Properties for label Font size.
             */
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template4_header_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_header_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerTitle_Label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerTitle_Label.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerTitle_Label.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerTitle_Label.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            /*
             * This method is used to add childtemplate4 Default Properties for label textcolor.
             */
            //Default Properties for label textcolor
            NSArray *valuelabelTextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                valuelabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                valuelabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (valuelabelTextColor)
                headerTitle_Label.textColor = [UIColor colorWithRed:[[valuelabelTextColor objectAtIndex:0] floatValue] green:[[valuelabelTextColor objectAtIndex:1] floatValue] blue:[[valuelabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            /*
             * This method is used to add childtemplate4 Default Properties for label textstyle.
             */
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            /*
             * This method is used to add childtemplate4 Default Properties for label fontSize.
             */
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerTitle_Label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerTitle_Label.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerTitle_Label.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerTitle_Label.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        headerTitle_Label.textAlignment=NSTextAlignmentCenter;
        [self addSubview:headerTitle_Label];
        
        /*
         * This method is used to add childtemplate4 headerborder visibility.
         */
        if ([NSLocalizedStringFromTableInBundle(@"child_template4_header_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            headerborder_Label=[[UILabel alloc]init];
            headerborder_Label.frame=CGRectMake(labelX_Position, headerTitle_Label.frame.size.height-1, headerTitle_Label.frame.size.width, 1);
            headerborder_Label.backgroundColor=[UIColor blackColor];
            [self addSubview:headerborder_Label];
        }
    }
    nextY_Position = nextY_Position+headerTitle_Label.frame.size.height+headerTitle_Label.frame.origin.y;
    /*
     * This method is used to add childtemplate4 tableview.
     */
    childTemplate4_TableView = [[UITableView alloc] init];
    childTemplate4_TableView.frame = CGRectMake(0,nextY_Position,SCREEN_WIDTH, SCREEN_HEIGHT-210);
    childTemplate4_TableView.delegate = self;
    childTemplate4_TableView.dataSource = self;
    nextY_Position=nextY_Position+childTemplate4_TableView.frame.size.height+20;
    [childTemplate4_TableView setTableFooterView:[UIView new]];
    
    /*
     * This method is used to add childtemplate4 button1 visibility.
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template4_button1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        childButton = [UIButton buttonWithType:UIButtonTypeCustom];
        childButton.frame = CGRectMake(6,nextY_Position, self.frame.size.width-12, 40);
        /*
         * This method is used to set childtemplate4 button1 titlename.
         */
        NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template4_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (buttonStr)
            [childButton setTitle:buttonStr forState:UIControlStateNormal];
        
        childButton.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        /*
         * This method is used to set childtemplate4 properties For Button backgroundColor.
         */
        // properties For Button backgroundColor
        
        NSArray *button1_BackgroundColor;
        
        if (NSLocalizedStringFromTableInBundle(@"child_template4_button1_back_ground_color",propertyFileName,[NSBundle mainBundle], nil))
            button1_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_button1_back_ground_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            button1_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button1_BackgroundColor)
            childButton.backgroundColor=[UIColor colorWithRed:[[button1_BackgroundColor objectAtIndex:0] floatValue] green:[[button1_BackgroundColor objectAtIndex:1] floatValue] blue:[[button1_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template4_button1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            /*
             * This method is used to set childtemplate4 properties For Button textcolor.
             */
            // Properties for Button TextColor.
            NSArray *childButtonTextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template4_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                childButtonTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"child_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                childButtonTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                childButtonTextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (childButtonTextColor)
                [childButton setTitleColor:[UIColor colorWithRed:[[childButtonTextColor objectAtIndex:0] floatValue] green:[[childButtonTextColor objectAtIndex:1] floatValue] blue:[[childButtonTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            /*
             * This method is used to set childtemplate4 properties For Button textstyle.
             */
            // Properties for Button Textstyle.
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template4_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            /*
             * This method is used to set childtemplate4 properties For Button fontsize.
             */
            // Properties for Button Font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template4_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                childButton.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                childButton.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                childButton.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                childButton.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            /*
             * This method is used to set childtemplate4 Default Properties for Button textcolor.
             */
            //Default Properties for Button textcolor
            
            NSArray *button1_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button1_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button1_TextColor)
                [childButton setTitleColor:[UIColor colorWithRed:[[button1_TextColor objectAtIndex:0] floatValue] green:[[button1_TextColor objectAtIndex:1] floatValue] blue:[[button1_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            /*
             * This method is used to set childtemplate4 Default Properties for Button textStyle.
             */
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            /*
             * This method is used to set childtemplate4 Default Properties for Button fontSize.
             */
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                childButton.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                childButton.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                childButton.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                childButton.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childButton setTag:1];
        [childButton setExclusiveTouch:YES];
        [childButton addTarget:self action:@selector(childButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        /*
         * This method is used to set childtemplate4 button add to view.
         */
        if([propertyFileName isEqualToString:@"PayBillMyBillersCT4"]){
            childButton.frame = CGRectMake(20,self.frame.size.height-60, self.frame.size.width-100, 50);
            childButton.layer.zPosition = 1;
            childButton = [UIButton buttonWithType:UIButtonTypeCustom];
            childButton.frame = CGRectMake(12,self.frame.size.height-50, SCREEN_WIDTH-20, 50);
            /*
             * This method is used to set childtemplate4 button1 titlename.
             */
            NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template4_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (buttonStr)
                [childButton setTitle:buttonStr forState:UIControlStateNormal];
            
            childButton.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            /*
             * This method is used to set childtemplate4 properties For Button backgroundColor.
             */
            // properties For Button backgroundColor
            
            NSArray *button1_BackgroundColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template4_button1_back_ground_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_button1_back_ground_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button1_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
            
            [childButton setBackgroundImage:[UIImage imageNamed:@"button.png"] forState:UIControlStateNormal];
            
            //        if (button1_BackgroundColor)
            //            childButton.backgroundColor=[UIColor colorWithRed:[[button1_BackgroundColor objectAtIndex:0] floatValue] green:[[button1_BackgroundColor objectAtIndex:1] floatValue] blue:[[button1_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template4_button1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                /*
                 * This method is used to set childtemplate4 properties For Button textcolor.
                 */
                // Properties for Button TextColor.
                NSArray *childButtonTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template4_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    childButtonTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"child_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    childButtonTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    childButtonTextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
                
                if (childButtonTextColor)
                    [childButton setTitleColor:[UIColor colorWithRed:[[childButtonTextColor objectAtIndex:0] floatValue] green:[[childButtonTextColor objectAtIndex:1] floatValue] blue:[[childButtonTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                /*
                 * This method is used to set childtemplate4 properties For Button textstyle.
                 */
                // Properties for Button Textstyle.
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_button_text_style;
                /*
                 * This method is used to set childtemplate4 properties For Button fontsize.
                 */
                // Properties for Button Font size.
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_button_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childButton.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childButton.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childButton.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childButton.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to set childtemplate4 Default Properties for Button textcolor.
                 */
                //Default Properties for Button textcolor
                
                NSArray *button1_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    button1_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
                if (button1_TextColor)
                    [childButton setTitleColor:[UIColor colorWithRed:[[button1_TextColor objectAtIndex:0] floatValue] green:[[button1_TextColor objectAtIndex:1] floatValue] blue:[[button1_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                /*
                 * This method is used to set childtemplate4 Default Properties for Button textStyle.
                 */
                //Default Properties for Button textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_button_text_style;
                /*
                 * This method is used to set childtemplate4 Default Properties for Button fontSize.
                 */
                //Default Properties for Button fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize= application_default_button_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childButton.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childButton.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childButton.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childButton.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [childButton setTag:1];
//            [childButton setExclusiveTouch:YES];
            [childButton addTarget:self action:@selector(childButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        }
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        [button addTarget:self
//                   action:@selector(childButtonAction:)
//         forControlEvents:UIControlEventTouchUpInside];
////        [button setTitle:@"Show View" forState:UIControlStateNormal];
//        button.frame = CGRectMake(childButton.frame.origin.x, childButton.frame.origin.y, childButton.frame.size.width, childButton.frame.size.height);
////        button.backgroundColor = [UIColor grayColor];
//        button.alpha = 0.5;
//        button.layer.zPosition = 2;
//        if(![childButton isHidden]){
//            [self addSubview:button];
//        }
        [self addSubview:childButton];
        
    }
    else
    {
        childTemplate4_TableView.frame = CGRectMake(0,(headerTitle_Label.frame.size.height+headerTitle_Label.frame.origin.y), SCREEN_WIDTH , SCREEN_HEIGHT-144);
    }
    
    if ([pin isEqualToString:@""])
    {
        childTemplate4_TableView.hidden = YES;
    }
    else if ([finalTranArray count]==0) {
        childTemplate4_TableView.hidden = YES;
        alertLabel.hidden = NO;
    }
    
    
    activityIndicator = [[ActivityIndicator alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [[self superview] addSubview:activityIndicator];
    activityIndicator.hidden = YES;
    /*
     * This method is used to set childtemplate4 tableview datasource type.
     */
    if ([NSLocalizedStringFromTableInBundle(@"child_template4_list_data_source_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:LOCAL_DATA])
    {
        databaseManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
        NSArray *dataArray = [databaseManager getAllPayBillTransactionDetails];
        finalTranArray = [[NSArray alloc] initWithArray:dataArray];
        NSLog(@"Final transaction Array...%@",finalTranArray);
        if ([finalTranArray count] > 0)
        {
            [childTemplate4_TableView reloadData];
            childTemplate4_TableView.hidden = NO;
            alertLabel.hidden = YES;
        }
        else
        {
            childTemplate4_TableView.hidden = YES;
            alertLabel.hidden = NO;
        }
    }
    else if ([NSLocalizedStringFromTableInBundle(@"child_template4_list_data_source_type",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:REMOTE_DATA])
    {
        activityIndicator.hidden = NO;
        [activityIndicator startActivityIndicator];
        
        NSMutableDictionary *webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
        /*
         * This method is used to set childtemplate4 Mpin popupTemplate.
         */
        if (pin.length == 0 && ![propertyFileName isEqualToString:@"TransactionHistoryCT4"])
        {
            NSString *profileID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"] objectForKey:PARAMETER36];
            Reachability *reachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus netStatus = [reachability  currentReachabilityStatus];
            
            if (![profileID isEqualToString:@"SMS"] || (netStatus == NotReachable)) {
                /*
                 * This method is used to set childtemplate4 listdata to load webservice api
                 */
                NSString *data = NSLocalizedStringFromTableInBundle(@"child_template4_list_data_to_load_webservice_api",propertyFileName,[NSBundle mainBundle], nil);
                NSString *processorCode = [Template getProcessorCodeWithData:data];
                NSString *transactionType = [Template getTransactionCodeWithData:data];
                [webServiceRequestInputDetails setObject:processorCode forKey:PARAMETER15];
                [webServiceRequestInputDetails setObject:transactionType forKey:PARAMETER13];
                
                //by shri..
                [self commonWebServiceCallWithData:webServiceRequestInputDetails andProcessorCode:processorCode];
                [[NSUserDefaults standardUserDefaults]setObject:webServiceRequestInputDetails forKey:@"MyBillersPC"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                //by shri ends
            }
        }
    }
    childTemplate4_TableView.frame = CGRectMake(childTemplate4_TableView.frame.origin.x, childTemplate4_TableView.frame.origin.y, childTemplate4_TableView.frame.size.width, childTemplate4_TableView.frame.size.height-(childButton.frame.size.height+20));
    
    [self addSubview:childTemplate4_TableView];
    [childTemplate4_TableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

#pragma mark - Common Webservice Call Method.
/*
 * This method is used to set childtemplate4 webservice calling common method.
 */
-(void)commonWebServiceCallWithData:(NSMutableDictionary *)inputDataDictionary andProcessorCode:(NSString *)processorCode
{
    [self setActivityIndicator];
    WebSericeUtils *webUtils = [[WebSericeUtils alloc] init];
    NSMutableDictionary *webUtilsValues = [[NSMutableDictionary alloc] initWithDictionary:[webUtils getWebServiceBundleObjectWithProccessorCode:processorCode withInputDataModel:inputDataDictionary]];
    
    WebServiceRequestFormation *webServiceRequest = [[WebServiceRequestFormation alloc] init];
    NSString *webRequest  = [webServiceRequest sendRequest:webUtilsValues];
    
    WebServiceRunning *webServiceRun = [[WebServiceRunning alloc] init];
    [webServiceRun startWebServiceWithRequest:webRequest withReqDictionary:webUtilsValues withDelegate:self];
}

#pragma mark - TABLE VIEW DATA SOURCE AND DELEGATE METHODS
/*
 * This method is used to set childtemplate4 tableview required delegate method.
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([NSLocalizedStringFromTableInBundle(@"child_template4_txn_history_list_view_background_view",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        return 200.0;
    }else{
        return 80;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [finalTranArray count];
}
/**
 * This method is used to set add Tableview for ChildTemplate4.
 *@param type- Declare Listview related UI(Labels,Images etc..base on Req).
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *valueLable1;
    NSString *valueLabel2;
    NSString *valuelable3;
    NSString *cellIdentifier = [NSString stringWithFormat:@"%@%d",@"Cell",(int)indexPath.row];
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //    NSString *string = [finalTranArray objectAtIndex:indexPath.row];
    //    [cell.textLabel setText:string];
    childTemplate4_ValueLabel1=[[UILabel alloc]init];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        labelX_Position = 10.0;
        
        childTemplate4_Label1=[[UILabel alloc]init];
        NSString *concatenate_str;
        /*
         * This method is used to set childtemplate4 tableview list item label visibility.
         */
        // value label1
        if ([NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            childTemplate4_Label1.frame=CGRectMake(labelX_Position, 4.0, childTemplate4_TableView.frame.size.width-(labelX_Position*2), 30);
            childTemplate4_Label1.backgroundColor = [UIColor clearColor];
            /*
             * This method is used to set childtemplate4 tableview list item label titlename.
             */
            if ([[[finalTranArray objectAtIndex:indexPath.row] valueForKey:NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label_param_type",propertyFileName,[NSBundle mainBundle], nil)] length]>0)
            {
                childTemplate4_Label1.text = [[finalTranArray objectAtIndex:indexPath.row] valueForKey:NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label_param_type",propertyFileName,[NSBundle mainBundle], nil)];
            }
            else
            {
                /*
                 * This method is used to set childtemplate4 tableview list item label titlename is equal to null bydefault NA.
                 */
                childTemplate4_Label1.text = application_default_no_value_available;
            }
            
            NSLog(@"Filtered array is..%@",[[finalTranArray objectAtIndex:indexPath.row] valueForKey:NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label_param_type",propertyFileName,[NSBundle mainBundle], nil)]);
            childTemplate4_Label1.lineBreakMode = NSLineBreakByTruncatingTail;
            /*
             * This method is used to set childtemplate4 tableview list item label fontattributes override.
             */
            if ([NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                /*
                 * This method is used to set childtemplate4 tableview list item label Properties for label TextColor.
                 */
                // Properties for label TextColor.
                
                NSArray *labelTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    labelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    labelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    labelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (labelTextColor)
                    childTemplate4_Label1.textColor = [UIColor colorWithRed:[[labelTextColor objectAtIndex:0] floatValue] green:[[labelTextColor objectAtIndex:1] floatValue] blue:[[labelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                /*
                 * This method is used to set childtemplate4 tableview list item label Properties for label Textstyle.
                 */
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                /*
                 * This method is used to set childtemplate4 tableview list item label Properties for label Font size.
                 */
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childTemplate4_Label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childTemplate4_Label1.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childTemplate4_Label1.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childTemplate4_Label1.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to set childtemplate4 tableview list item label Default Properties for label textcolor.
                 */
                //Default Properties for label textcolor
                NSArray *valuelabelTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    valuelabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    valuelabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (valuelabelTextColor)
                    childTemplate4_Label1.textColor = [UIColor colorWithRed:[[valuelabelTextColor objectAtIndex:0] floatValue] green:[[valuelabelTextColor objectAtIndex:1] floatValue] blue:[[valuelabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                /*
                 * This method is used to set childtemplate4 tableview list item label Default Properties for label textstyle.
                 */
                //Default Properties for label textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                /*
                 * This method is used to set childtemplate4 tableview list item label Default Properties for label fontsize.
                 */
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childTemplate4_Label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childTemplate4_Label1.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childTemplate4_Label1.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childTemplate4_Label1.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            /*
             * This method is used to set childtemplate4 tableview list item label frame add to cell.
             */
            
            [cell.contentView addSubview:childTemplate4_Label1];
        }
        
        nextY_Position = childTemplate4_Label1.frame.size.height;
        childTemplate4_ValueLabel1.frame = CGRectMake(labelX_Position, nextY_Position, self.frame.size.width, 30);
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template4_set_cell_background_image",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            childTemplate4_Label1.frame = CGRectMake(25, 10.0, SCREEN_WIDTH, 30);
            childTemplate4_Label1.text = [childTemplate4_Label1.text capitalizedString];
            [cell setBackgroundView:[[UIImageView alloc] initWithImage:
                                     [UIImage imageNamed:@"cellborder.png"]]];
            
        }

        /*
         * This method is used to set childtemplate4 tableview list item label2 Visibility.
         */
        // value label2
        if ([NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            if ([NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label3_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                /*
                 * This method is used to set childtemplate4 tableview list item label2 textvalue.
                 */
                // Label TextValue
                
                NSLocale *localeStr = [[NSLocale alloc] initWithLocaleIdentifier:[NSString stringWithFormat:@"%@_%@",NSLocalizedStringFromTableInBundle(@"application_amount_format_language_code",@"GeneralSettings",[NSBundle mainBundle], nil),NSLocalizedStringFromTableInBundle(@"application_amount_format_country_code",@"GeneralSettings",[NSBundle mainBundle], nil)]];
                
                NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
                [currencyStyle setLocale:localeStr];
                currencyStyle.numberStyle = NSNumberFormatterCurrencyStyle;
                
                if (NSLocalizedStringFromTableInBundle(@"application_amount_decimal_places",@"GeneralSettings",[NSBundle mainBundle], nil)) {
                    [currencyStyle setMaximumFractionDigits:[NSLocalizedStringFromTableInBundle(@"application_amount_decimal_places",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue]];
                }
                else{
                    [currencyStyle setMaximumFractionDigits:0];
                }
                [currencyStyle setCurrencySymbol:@""];
                
                //Application Amount format
                /*
                 * This method is used to set Application Amount format.
                 */
                [currencyStyle setCurrencyCode:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"application_amount_format",@"GeneralSettings",[NSBundle mainBundle], nil)]];
                
                NSNumber *amountStr = [NSNumber numberWithDouble:[[[finalTranArray objectAtIndex:indexPath.row] valueForKey:NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label2_param_type",propertyFileName,[NSBundle mainBundle], nil)] doubleValue]];
                
                //Application Amount format
                /*
                 * This method is used to set Application Amount format.
                 */
                NSMutableDictionary *userDeatils=[[NSMutableDictionary alloc]initWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"]];
                NSString *currency;
                
                if ([userDeatils objectForKey:PARAMETER22]){
                    currency = [userDeatils objectForKey:PARAMETER22];
                    //                    NSLog(@"Currency Value is...%@",currency);
                }
                //Application Amount format
                else if ([userDeatils objectForKey:PARAMETER22] == (id)[NSNull null] || [[userDeatils objectForKey:PARAMETER22]length] == 0){
                    currency=[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"application_amount_format",@"GeneralSettings",[NSBundle mainBundle], nil)];
                }
                valueLable1 = [NSString stringWithFormat:@"%@ %@",currency,[currencyStyle stringFromNumber:amountStr]];
                if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
                {
                    valueLable1 = [NSString stringWithFormat:@"%@ %@",[currencyStyle stringFromNumber:amountStr], currency];
                }

            }
            else
            {
                /*
                 * This method is used to set childtemplate4 listitem label param type contains.
                 */
                concatenate_str = [NSString stringWithFormat:@"%@",[[finalTranArray objectAtIndex:indexPath.row] valueForKey:NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label2_param_type",propertyFileName,[NSBundle mainBundle], nil)]];
                valueLable1 = concatenate_str;
            }
            /*
             * This method is used to set childtemplate4 listitem label text fontattributes override.
             */
            if ([NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label2_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                /*
                 * This method is used to set childtemplate4 listitem label text Properties for label TextColor.
                 */
                // Properties for label TextColor.
                
                NSArray *valuelabelTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    valuelabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"child_template4_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    valuelabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    valuelabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (valuelabelTextColor)
                    childTemplate4_ValueLabel1.textColor = [UIColor colorWithRed:[[valuelabelTextColor objectAtIndex:0] floatValue] green:[[valuelabelTextColor objectAtIndex:1] floatValue] blue:[[valuelabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                /*
                 * This method is used to set childtemplate4 listitem label text Properties for label Textstyle.
                 */
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label2_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template4_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                /*
                 * This method is used to set childtemplate4 listitem label text Properties for label fontsize.
                 */
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label2_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template4_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childTemplate4_ValueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childTemplate4_ValueLabel1.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childTemplate4_ValueLabel1.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childTemplate4_ValueLabel1.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to set childtemplate4 listitem label text Default Properties for label textcolor.
                 */
                //Default Properties for label textcolor
                NSArray *valuelabelTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template4_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    valuelabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    valuelabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (valuelabelTextColor)
                    childTemplate4_ValueLabel1.textColor = [UIColor colorWithRed:[[valuelabelTextColor objectAtIndex:0] floatValue] green:[[valuelabelTextColor objectAtIndex:1] floatValue] blue:[[valuelabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                /*
                 * This method is used to set childtemplate4 listitem label text Default Properties for label textStyle.
                 */
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                /*
                 * This method is used to set childtemplate4 listitem label text Default Properties for label fontsize.
                 */
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childTemplate4_ValueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childTemplate4_ValueLabel1.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childTemplate4_ValueLabel1.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childTemplate4_ValueLabel1.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
        }
        /*
         * This method is used to set childtemplate4 listitem label Visibility.
         */
        // value label3
        if ([NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label3_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            /*
             * This method is used to set childtemplate4 listitem label titlename.
             */
            NSString *concatenate_str1 =[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label3_text",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (concatenate_str1)
                valueLabel2 = concatenate_str1;
            /*
             * This method is used to set childtemplate4 listitem label text fontattributes override.
             */
            if ([NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label3_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                /*
                 * This method is used to set childtemplate4 listitem label text Properties for label TextColor.
                 */
                // Properties for label TextColor.
                
                NSArray *valuelabelTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    valuelabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"child_template4_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    valuelabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    valuelabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (valuelabelTextColor)
                    childTemplate4_ValueLabel2.textColor = [UIColor colorWithRed:[[valuelabelTextColor objectAtIndex:0] floatValue] green:[[valuelabelTextColor objectAtIndex:1] floatValue] blue:[[valuelabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                /*
                 * This method is used to set childtemplate4 listitem label text Properties for label Textstyle.
                 */
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label3_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template4_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                /*
                 * This method is used to set childtemplate4 listitem label text Properties for label Font size.
                 */
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label3_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label3_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template4_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childTemplate4_ValueLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childTemplate4_ValueLabel2.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childTemplate4_ValueLabel2.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childTemplate4_ValueLabel2.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to set childtemplate4 listitem label text Default Properties for label textcolor.
                 */
                //Default Properties for label textcolor
                NSArray *valuelabelTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template4_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    valuelabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    valuelabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (valuelabelTextColor)
                    childTemplate4_ValueLabel2.textColor = [UIColor colorWithRed:[[valuelabelTextColor objectAtIndex:0] floatValue] green:[[valuelabelTextColor objectAtIndex:1] floatValue] blue:[[valuelabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                /*
                 * This method is used to set childtemplate4 listitem label text Default Properties for label textstyle.
                 */
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                /*
                 * This method is used to set childtemplate4 listitem label text Default Properties for label fontSize.
                 */
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childTemplate4_ValueLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childTemplate4_ValueLabel2.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childTemplate4_ValueLabel2.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childTemplate4_ValueLabel2.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            if(childTemplate4_ValueLabel2.text.length > 24)
                childTemplate4_ValueLabel2.font = [UIFont systemFontOfSize:14.0];
            childTemplate4_ValueLabel2.lineBreakMode=NSLineBreakByCharWrapping;
            childTemplate4_ValueLabel2.numberOfLines=0;
            [childTemplate4_ValueLabel2 sizeToFit];
            /*
             * This method is used to set childtemplate4 listitem label add to cell.
             */
            [cell.contentView addSubview:childTemplate4_ValueLabel2];
        }
        /*
         * This method is used to set childtemplate4 listitem label Visibility.
         */
        // value label4
        if ([NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label4_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            childTemplate4_ValueLabel3.frame = CGRectMake((labelX_Position*4)+childTemplate4_ValueLabel2.intrinsicContentSize.width, nextY_Position,childTemplate4_TableView.frame.size.width, 30);
            /*
             * This method is used to set childtemplate4 listitem label titlename.
             */
            if ([[[finalTranArray objectAtIndex:indexPath.row] valueForKey:NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label4_param_type",propertyFileName,[NSBundle mainBundle], nil)] length] > 0)
            {
                concatenate_str = [[finalTranArray objectAtIndex:indexPath.row] valueForKey:NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label4_param_type",propertyFileName,[NSBundle mainBundle], nil)];
                valuelable3 = concatenate_str;
            }
            /*
             * This method is used to set childtemplate4 listitem label text fontattributes override.
             */
            if([NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label4_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                /*
                 * This method is used to set childtemplate4 listitem label text Properties for label TextColor.
                 */
                // Properties for label TextColor.
                NSArray *valuelabelTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label4_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    valuelabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label4_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template4_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    valuelabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    valuelabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (valuelabelTextColor)
                    childTemplate4_ValueLabel3.textColor = [UIColor colorWithRed:[[valuelabelTextColor objectAtIndex:0] floatValue] green:[[valuelabelTextColor objectAtIndex:1] floatValue] blue:[[valuelabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                /*
                 * This method is used to set childtemplate4 listitem label text Properties for label Textstyle.
                 */
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label4_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label4_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template4_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                /*
                 * This method is used to set childtemplate4 listitem label text Properties for label font size.
                 */
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label4_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label4_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template4_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childTemplate4_ValueLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childTemplate4_ValueLabel3.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childTemplate4_ValueLabel3.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childTemplate4_ValueLabel3.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                /*
                 * This method is used to set childtemplate4 listitem label text default Properties for label text color.
                 */
                //Default Properties for label textcolor
                NSArray *valuelabelTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template4_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    valuelabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template4_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    valuelabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (valuelabelTextColor)
                    childTemplate4_ValueLabel3.textColor = [UIColor colorWithRed:[[valuelabelTextColor objectAtIndex:0] floatValue] green:[[valuelabelTextColor objectAtIndex:1] floatValue] blue:[[valuelabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                /*
                 * This method is used to set childtemplate4 listitem label text default Properties for label textStyle.
                 */
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template4_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                /*
                 * This method is used to set childtemplate4 listitem label text default Properties for label fontsize.
                 */
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template4_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template4_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    childTemplate4_ValueLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    childTemplate4_ValueLabel3.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    childTemplate4_ValueLabel3.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    childTemplate4_ValueLabel3.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            if(childTemplate4_ValueLabel3.text.length > 24)
                childTemplate4_ValueLabel3.font = [UIFont systemFontOfSize:14.0];
            childTemplate4_ValueLabel3.lineBreakMode=NSLineBreakByCharWrapping;
            childTemplate4_ValueLabel3.numberOfLines=0;
            [childTemplate4_ValueLabel3 sizeToFit];
            /*
             * This method is used to set childtemplate4 listitem label add to cell.
             */
            [cell.contentView addSubview:childTemplate4_ValueLabel3];
        }
        
        NSString *value = [[NSString stringWithFormat:@"%@ %@ %@",valueLable1,valueLabel2,valuelable3] stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
        childTemplate4_ValueLabel1.text = value;
        childTemplate4_ValueLabel1.numberOfLines=0;
        [childTemplate4_ValueLabel1 sizeToFit];
        [cell.contentView addSubview:childTemplate4_ValueLabel1];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    if ([NSLocalizedStringFromTableInBundle(@"child_template4_txn_history_list_view_background_view",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        cellIdentifier = [NSString stringWithFormat:@"%@%d",@"Cell",(int)indexPath.row];
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(5, 5, self.frame.size.width-20, 180)];
        backView.backgroundColor=[UIColor lightGrayColor];
        backView.layer.cornerRadius = 20.0;
        backView.layer.borderColor = [UIColor colorWithRed:110/255.0 green:25/255.0 blue:65/255.0 alpha:1.0].CGColor;
        //[[UIColor brownColor] CGColor];
        backView.layer.borderWidth = 2.0;
        
        UILabel *popupTemplateLabelBorder = [[UILabel alloc] init];
        popupTemplateLabelBorder.frame = CGRectMake(2, 2, backView.frame.size.width-4 , backView.frame.size.height-4);
        popupTemplateLabelBorder.layer.cornerRadius = 20;
        popupTemplateLabelBorder.clipsToBounds = YES;
        popupTemplateLabelBorder.layer.borderWidth = 3;
        popupTemplateLabelBorder.layer.borderColor = [UIColor whiteColor].CGColor;
        popupTemplateLabelBorder.backgroundColor = [UIColor colorWithRed:227/255.0 green:227/255.0 blue:227/255.0 alpha:1.0];
        [backView addSubview:popupTemplateLabelBorder];
        
        //        [cell.contentView addSubview:backView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.contentView addSubview:backView];
        //        childTemplate4_Label1.frame = CGRectMake(120,10,backView.frame.size.width/3,30);
        UILabel *titleLabel = [[UILabel alloc] initWithFrame: CGRectMake(120,10,backView.frame.size.width/3,30)];
        [titleLabel setTextColor:[UIColor blackColor]];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        NSString *str = [[finalTranArray objectAtIndex:indexPath.row] valueForKey:NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label_param_type",propertyFileName,[NSBundle mainBundle], nil)];
        [titleLabel setText:str];
        NSLog(@"titlelabel : %@", childTemplate4_Label1.text);
        [backView addSubview:titleLabel];
        
        UILabel *amountLabel = [[UILabel alloc] initWithFrame: CGRectMake(10,40,backView.frame.size.width/3,30)];
        [amountLabel setText:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:@"label_id_home_amount"]]];
        [amountLabel setTextColor:[UIColor blackColor]];
        [amountLabel setBackgroundColor:[UIColor clearColor]];
        [amountLabel setTextAlignment:NSTextAlignmentCenter];
        [backView addSubview:amountLabel];
        
        UILabel *amountValueLabel = [[UILabel alloc] initWithFrame: CGRectMake(120,40,backView.frame.size.width/2,30)];
        {
            NSLocale *localeStr = [[NSLocale alloc] initWithLocaleIdentifier:[NSString stringWithFormat:@"%@_%@",NSLocalizedStringFromTableInBundle(@"application_amount_format_language_code",@"GeneralSettings",[NSBundle mainBundle], nil),NSLocalizedStringFromTableInBundle(@"application_amount_format_country_code",@"GeneralSettings",[NSBundle mainBundle], nil)]];
            
            NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
            [currencyStyle setLocale:localeStr];
            currencyStyle.numberStyle = NSNumberFormatterCurrencyStyle;
            
            if (NSLocalizedStringFromTableInBundle(@"application_amount_decimal_places",@"GeneralSettings",[NSBundle mainBundle], nil)) {
                [currencyStyle setMaximumFractionDigits:[NSLocalizedStringFromTableInBundle(@"application_amount_decimal_places",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue]];
            }
            else{
                [currencyStyle setMaximumFractionDigits:0];
            }
            [currencyStyle setCurrencySymbol:@""];
            
            //Application Amount format
            /*
             * This method is used to set Application Amount format.
             */
            [currencyStyle setCurrencyCode:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"application_amount_format",@"GeneralSettings",[NSBundle mainBundle], nil)]];
            
            NSNumber *amountStr = [NSNumber numberWithDouble:[[[finalTranArray objectAtIndex:indexPath.row] valueForKey:NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label2_param_type",propertyFileName,[NSBundle mainBundle], nil)] doubleValue]];
            
            //Application Amount format
            /*
             * This method is used to set Application Amount format.
             */
            NSMutableDictionary *userDeatils=[[NSMutableDictionary alloc]initWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"]];
            NSString *currency;
            
            if ([userDeatils objectForKey:PARAMETER22]){
                currency = [userDeatils objectForKey:PARAMETER22];
                NSLog(@"Currency Value is...%@",currency);
            }
            //Application Amount format
            else if ([userDeatils objectForKey:PARAMETER22] == (id)[NSNull null] || [[userDeatils objectForKey:PARAMETER22]length] == 0){
                currency=[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"application_amount_format",@"GeneralSettings",[NSBundle mainBundle], nil)];
            }
            valueLable1 = [NSString stringWithFormat:@"%@ %@",currency,[currencyStyle stringFromNumber:amountStr]];
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
            {
                valueLable1 = [NSString stringWithFormat:@"%@ %@",[currencyStyle stringFromNumber:amountStr], currency];
            }
        }
        [amountValueLabel setText:valueLable1];
        [amountValueLabel setTextColor:[UIColor blackColor]];
        [amountValueLabel setBackgroundColor:[UIColor whiteColor]];
        [amountValueLabel setTextAlignment:NSTextAlignmentCenter];
        [amountValueLabel setFont:[UIFont systemFontOfSize:12]];
        [backView addSubview:amountValueLabel];
        
        UILabel *dateLabel = [[UILabel alloc] initWithFrame:
                              CGRectMake(10,80,backView.frame.size.width/3,30)];
        [dateLabel setText:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:@"label_id_home_date"]]];
        [dateLabel setTextColor:[UIColor blackColor]];
        [dateLabel setBackgroundColor:[UIColor clearColor]];
        [dateLabel setTextAlignment:NSTextAlignmentCenter];
        [backView addSubview:dateLabel];
        
        if ([[[finalTranArray objectAtIndex:indexPath.row] valueForKey:NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label4_param_type",propertyFileName,[NSBundle mainBundle], nil)] length] > 0)
        {
            valuelable3 = [[finalTranArray objectAtIndex:indexPath.row] valueForKey:NSLocalizedStringFromTableInBundle(@"child_template4_list_item_label4_param_type",propertyFileName,[NSBundle mainBundle], nil)];
        }
        
        UILabel *dateValueLabel = [[UILabel alloc] initWithFrame: CGRectMake(120,80,backView.frame.size.width/2,30)];
        [dateValueLabel setText:[valuelable3 substringWithRange:NSMakeRange(0, 10)]];
        [dateValueLabel setTextColor:[UIColor blackColor]];
        [dateValueLabel setBackgroundColor:[UIColor whiteColor]];
        [dateValueLabel setTextAlignment:NSTextAlignmentCenter];
        [dateValueLabel setFont:[UIFont systemFontOfSize:12]];
        [backView addSubview:dateValueLabel];
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:
                              CGRectMake(10,120,backView.frame.size.width/3,30)];
        [timeLabel setText:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:@"label_id_home_time"]]];
        [timeLabel setTextColor:[UIColor blackColor]];
        [timeLabel setBackgroundColor:[UIColor clearColor]];
        [timeLabel setTextAlignment:NSTextAlignmentCenter];
        [backView addSubview:timeLabel];
        
        UILabel *timeValueLabel = [[UILabel alloc] initWithFrame: CGRectMake(120,120,backView.frame.size.width/2,30)];
        [timeValueLabel setText:[valuelable3 substringFromIndex:[valuelable3 length] - 5]];
        [timeValueLabel setTextColor:[UIColor blackColor]];
        [timeValueLabel setBackgroundColor:[UIColor whiteColor]];
        [timeValueLabel setTextAlignment:NSTextAlignmentCenter];
        [timeValueLabel setFont:[UIFont systemFontOfSize:12]];
        
        [backView addSubview:timeValueLabel];
        
        
            if([propertyFileName isEqualToString:@"TransactionHistoryCT4"]){
                if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
                {
                    titleLabel.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
                    amountLabel.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
                    amountValueLabel.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
                    amountValueLabel.text = [amountValueLabel.text stringByReplacingOccurrencesOfString:@"YER" withString:@"ر.ي"];
                    dateLabel.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
                    dateValueLabel.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
                    timeLabel.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
                    timeValueLabel.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
                }
            }
    }
    
    if([propertyFileName isEqualToString:@"PayBillMyBillersCT4"]){
        CGRect rect = childTemplate4_ValueLabel1.frame;
        CGRect rect2 = childTemplate4_Label1.frame;
        [childTemplate4_ValueLabel1 setFrame:CGRectMake(rect2.origin.x, CGRectGetMaxY(rect2), rect.size.width, rect.size.height)];
    }
    return cell;
}
/**
 * This method is used to set add Tableview Delegate Method User selects List .
 *@param type- Declare Listview - (NextTemplate and Next template proprty file).
 *To show List Item Detailed Data etc..
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
    NSString *proccessorCode;
    NSString *transactionType;
    /*
     * This method is used to set childtemplate4 listdata webserviceapi name.
     */
    if (![NSLocalizedStringFromTableInBundle(@"child_template4_list_data_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"child_template4_list_data_web_service_api_name"] && !([NSLocalizedStringFromTableInBundle(@"child_template4_list_data_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil) compare:@"(null)"] == NSOrderedSame) && ([NSLocalizedStringFromTableInBundle(@"child_template4_list_data_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil) length] != 0)) {
        NSString *data =  NSLocalizedStringFromTableInBundle(@"child_template4_list_data_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
        /*
         * This method is used to set childtemplate4 listdata - Feature processorcode.
         */
        proccessorCode =[Template getProcessorCodeWithData:data];
        /*
         * This method is used to set childtemplate4 listdata - Feature TransactionCode.
         */
        transactionType = [Template getTransactionCodeWithData:data];
        
        [webServiceRequestInputDetails setObject:proccessorCode forKey:PARAMETER15];
        [webServiceRequestInputDetails setObject:transactionType forKey:PARAMETER13];
    }
    
    if ([proccessorCode isEqualToString:PROCESSOR_CODE_PENDING_BILLS])
    {
        if ([[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID] || [[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_FIXED])
        {
            
            if ([[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID])
            {
                if ([[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERNICKNAME])
                {
                    [webServiceRequestInputDetails setObject:[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERNICKNAME] forKey:PARAMETER18];
                    /*
                     * This method is used to set childtemplate4 listdata - Feature processorcode
                     */
                    NSString *proccessorCode = NSLocalizedStringFromTableInBundle(@"child_template4_list_data_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
                    
                    [self commonWebServiceCallWithData:webServiceRequestInputDetails andProcessorCode:proccessorCode];
                }
                else
                {
                    /*
                     * This method is used to set childtemplate4 list nexttemplate properties file.
                     */
                    NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
                    /*
                     * This method is used to set childtemplate4 list nexttemplate.
                     */
                    NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
                    if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
                    {
                        Class myclass = NSClassFromString(nextTemplate);
                        id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:nil withProcessorCode:proccessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:(int)indexPath.row withDataArray:finalTranArray withSubIndex:0];
                        
                        [[[self superview] superview] addSubview:(UIView *)obj];
                    }
                }
            }
            else
            {
                [webServiceRequestInputDetails setObject:[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERNICKNAME] forKey:PARAMETER18];
                [self commonWebServiceCallWithData:webServiceRequestInputDetails andProcessorCode:proccessorCode];
            }
        }
        else if ([[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_VARIABLE])
        {
            /*
             * This method is used to set childtemplate4 list nexttemplate properties file.
             */
            NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
            /*
             * This method is used to set childtemplate4 list nexttemplate.
             */
            NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
            
            if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
            {
                Class myclass = NSClassFromString(nextTemplate);
                id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:nil withProcessorCode:proccessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:(int)indexPath.row withDataArray:finalTranArray withSubIndex:0];
                [[[self superview] superview] addSubview:(UIView *)obj];
            }
        }
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_MINI_STATEMENT])
    {
        /*
         * This method is used to set childtemplate4 list nexttemplate properties file.
         */
        NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
        /*
         * This method is used to set childtemplate4 list nexttemplate.
         */
        NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
        
        if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
        {
            Class myclass = NSClassFromString(nextTemplate);
            id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:nil withProcessorCode:proccessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:(int)indexPath.row withDataArray:finalTranArray withSubIndex:0];
            [[[self superview] superview] addSubview:(UIView *)obj];
        }
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_TXN_HISTORY_T3] && [transactionType isEqualToString:TRANSACTION_CODE_TXN_HISTORY_T3])
    {
        NSLog(@"Final Array is.... %@",finalTranArray);
        /*
         * This method is used to set childtemplate4 list nexttemplate properties file.
         */
        NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
        /*
         * This method is used to set childtemplate4 list nexttemplate.
         */
        NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
        
        if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
        {
            Class myclass = NSClassFromString(nextTemplate);
            id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:nil withProcessorCode:proccessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:(int)indexPath.row withDataArray:finalTranArray withSubIndex:0];
            [[[self superview] superview] addSubview:(UIView *)obj];
        }
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_BANK_TXN_HISTORY_T3] && [transactionType isEqualToString:TRANSACTION_CODE_BANK_TXN_HISTORY_T3])
    {
        NSLog(@"Final Array is.... %@",finalTranArray);
        
        /*
         * This method is used to set childtemplate4 list nexttemplate properties file.
         */
        NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
        /*
         * This method is used to set childtemplate4 list nexttemplate.
         */
        NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
        
        if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
        {
            Class myclass = NSClassFromString(nextTemplate);
            id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:nil withProcessorCode:proccessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:(int)indexPath.row withDataArray:finalTranArray withSubIndex:0];
            [[[self superview] superview] addSubview:(UIView *)obj];
        }
    }
    else if ([local_processorCode isEqualToString:PROCESSOR_CODE_FETCH_MY_BILLERS]){
        
        if ([[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID] || [[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_FIXED])
        {
            if ([[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID])
            {
                if ([[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERNICKNAME])
                {
                    [webServiceRequestInputDetails setObject:[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERNICKNAME] forKey:PARAMETER18];
                    [self commonWebServiceCallWithData:webServiceRequestInputDetails andProcessorCode:proccessorCode];
                }
                else
                {
                    /*
                     * This method is used to set childtemplate4 list nexttemplate properties file.
                     */
                    NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
                    /*
                     * This method is used to set childtemplate4 list nexttemplate.
                     */
                    NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
                    
                    if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
                    {
                        Class myclass = NSClassFromString(nextTemplate);
                        id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:nil withProcessorCode:proccessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:(int)indexPath.row withDataArray:finalTranArray withSubIndex:0];
                        
                        [[[self superview] superview] addSubview:(UIView *)obj];
                    }
                }
            }
            else
            {
                [webServiceRequestInputDetails setObject:[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERNICKNAME] forKey:PARAMETER18];
                [self commonWebServiceCallWithData:webServiceRequestInputDetails andProcessorCode:proccessorCode];
            }
        }
        else if ([[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_VARIABLE])
        {
            /*
             * This method is used to set childtemplate4 list nexttemplate properties file.
             */
            NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
            /*
             * This method is used to set childtemplate4 list nexttemplate.
             */
            NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
            if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
            {
                Class myclass = NSClassFromString(nextTemplate);
                id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:[finalTranArray objectAtIndex:indexPath.row] withProcessorCode:proccessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:(int)indexPath.row withDataArray:finalTranArray withSubIndex:0];
                [[[self superview] superview] addSubview:(UIView *)obj];
            }
        }
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_FETCH_BILLS])
    {
        if ([[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID] || [[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_FIXED])
        {
            [webServiceRequestInputDetails setObject:[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERNICKNAME] forKey:PARAMETER18];
            [self commonWebServiceCallWithData:webServiceRequestInputDetails andProcessorCode:proccessorCode];
        }
        else if ([[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID])
        {
            if ([[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERNICKNAME])
            {
                [webServiceRequestInputDetails setObject:[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERNICKNAME] forKey:PARAMETER18];
                [self commonWebServiceCallWithData:webServiceRequestInputDetails andProcessorCode:proccessorCode];
            }
            else
            {
                /*
                 * This method is used to set childtemplate4 list nexttemplate properties file.
                 */
                NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
                /*
                 * This method is used to set childtemplate4 list nexttemplate.
                 */
                NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
                
                if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
                {
                    Class myclass = NSClassFromString(nextTemplate);
                    id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:[finalTranArray objectAtIndex:indexPath.row] withProcessorCode:proccessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:(int)indexPath.row withDataArray:finalTranArray withSubIndex:0];
                    [[[self superview] superview] addSubview:(UIView *)obj];
                }
            }
        }
        else if ([[[finalTranArray objectAtIndex:indexPath.row] objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_VARIABLE])
        {
            /*
             * This method is used to set childtemplate4 list nexttemplate properties file.
             */
            NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
            /*
             * This method is used to set childtemplate4 list nexttemplate.
             */
            NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
            
            if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
            {
                Class myclass = NSClassFromString(nextTemplate);
                id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:[finalTranArray objectAtIndex:indexPath.row] withProcessorCode:proccessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:(int)indexPath.row withDataArray:finalTranArray withSubIndex:0];
                
                [[[self superview] superview] addSubview:(UIView *)obj];
            }
        }
    }
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
/*
 * This method is used to set childtemplate4 localdelegate
 */
-(void)setLocalDelegate:(id)fromDelegate{
    self.delegate = fromDelegate;
}

#pragma mark - BaseView process data.
/*
 * This method is used to set childtemplate4 processdata notification.
 */
-(void)processData
{
    //Process Data
}

#pragma mark -  CHILD BUTTON ACTION

/**
 * This method is used to set button1 action of ChildTemplate4.
 * @prameters are defined in button action those are
 Feature-(processorCode,transaction type).
 */
-(void)childButtonAction:(id)sender
{
    UIButton *tempButton = sender;
    if (tempButton.tag == 1)
    {
        NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template4_button1_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
        NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template4_button1_next_template",propertyFileName,[NSBundle mainBundle], nil);
        
        if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
        {
            UIViewController *object = [[NSClassFromString(nextTemplate) alloc] initWithNibName:nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:nil withPropertyFile:nextTemplateProperty withProcessorCode:nil dataArray:nil dataDictionary:nil];
            
            UIViewController *vc = [(UINavigationController *)self.window.rootViewController visibleViewController];
            [vc.navigationController pushViewController:object animated:NO];
        }
    }
}

#pragma mark - Web Service Call methods.
/*
 * This method is used to set childtemplate4 listview webservices processing method
 */
-(void)processData:(WebServiceDataObject *)webServiceDataObject withProcessCode:(NSString *)processCode
{
    [activityIndicator stopActivityIndicator];
    [self removeActivityIndicator];
    activityIndicator.hidden = YES;
    
    if (webServiceDataObject.faultCode && webServiceDataObject.faultString)
    {
        [self showAlertForErrorWithMessage:webServiceDataObject.faultString andErrorCode:nil];
    }
    else
    {      // Pending bills list data dont move to Base
        if ([processCode isEqualToString:PROCESSOR_CODE_PENDING_BILLS])
        {
            if (webServiceDataObject.Reference3)
            {
                childTemplate4_TableView.hidden = NO;
                local_processorCode = processCode;
                
                NSData *jsonData = [webServiceDataObject.Reference3 dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError];
                
                if (![dataArray count])
                {
                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_mybillers_list_not_available", nil)] andErrorCode:nil];
                }
                else
                {
                    NSString *nn = @"nn";
                    NSString *dd = @"dd";
                    NSString *am = @"am";
                    NSString *bpc = @"bpc";
                    NSString *bt = @"bt";
                    NSString *bn = @"bn";
                    NSString *a1 = @"A1";
                    NSString *a2 = @"A2";
                    NSString *a3 = @"A3";
                    NSString *a4 = @"A4";
                    NSString *a5 = @"A5";
                    NSString *ref1 = @"Ref1";
                    NSString *ref2 = @"Ref2";
                    NSString *ref3 = @"Ref3";
                    NSString *ref4 = @"Ref4";
                    NSString *ref5 = @"Ref5";
                    
                    NSMutableArray *finalArray = [[NSMutableArray alloc] init];
                    for (int i=0; i<[dataArray count]; i++)
                    {
                        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                        
                        if ([[dataArray objectAtIndex:i] objectForKey:nn])
                        {
                            [tempDict setObject:[[dataArray objectAtIndex:i] objectForKey:nn] forKey:BILLERNICKNAME];
                        }
                        
                        if ([[dataArray objectAtIndex:i] objectForKey:dd])
                        {
                            [tempDict setObject:[[dataArray objectAtIndex:i] objectForKey:dd] forKey:BILLERDUEDATE];
                        }
                        
                        if ([[dataArray objectAtIndex:i] objectForKey:am])
                        {
                            [tempDict setObject:[[dataArray objectAtIndex:i] objectForKey:am] forKey:PARAMETER21];
                        }
                        
                        if ([[dataArray objectAtIndex:i] objectForKey:bpc])
                        {
                            [tempDict setObject:[[dataArray objectAtIndex:i] objectForKey:bpc] forKey:BILLPAYCODE];
                        }
                        
                        if ([[dataArray objectAtIndex:i] objectForKey:bt])
                        {
                            [tempDict setObject:[[dataArray objectAtIndex:i] objectForKey:bt] forKey:BILLERTYPE];
                        }
                        
                        if ([[dataArray objectAtIndex:i] objectForKey:bn])
                        {
                            [tempDict setObject:[[dataArray objectAtIndex:i] objectForKey:bn] forKey:BILLERNAME];
                        }
                        
                        if ([[dataArray objectAtIndex:i] objectForKey:a1] )
                        {
                            [tempDict setObject:[[dataArray objectAtIndex:i] objectForKey:a1] forKey:BILLERA1];
                        }
                        if ([[dataArray objectAtIndex:i] objectForKey:a2] )
                        {
                            [tempDict setObject:[[dataArray objectAtIndex:i] objectForKey:a2] forKey:BILLERA2];
                        }
                        if ([[dataArray objectAtIndex:i] objectForKey:a3] )
                        {
                            [tempDict setObject:[[dataArray objectAtIndex:i] objectForKey:a3] forKey:BILLERA3];
                        }
                        if ([[dataArray objectAtIndex:i] objectForKey:a4])
                        {
                            [tempDict setObject:[[dataArray objectAtIndex:i] objectForKey:a4] forKey:BILLERA4];
                        }
                        if ([[dataArray objectAtIndex:i] objectForKey:a5] )
                        {
                            [tempDict setObject:[[dataArray objectAtIndex:i] objectForKey:a5] forKey:BILLERA5];
                        }
                        
                        if ([[dataArray objectAtIndex:i] objectForKey:ref1] )
                        {
                            [tempDict setObject:[[dataArray objectAtIndex:i] objectForKey:ref1] forKey:BILLERREFERENCE1];
                        }
                        if ([[dataArray objectAtIndex:i] objectForKey:ref2] )
                        {
                            [tempDict setObject:[[dataArray objectAtIndex:i] objectForKey:ref2] forKey:BILLERREFERENCE2];
                        }
                        if ([[dataArray objectAtIndex:i] objectForKey:ref3] )
                        {
                            [tempDict setObject:[[dataArray objectAtIndex:i] objectForKey:ref3] forKey:BILLERREFERENCE3];
                        }
                        if ([[dataArray objectAtIndex:i] objectForKey:ref4] )
                        {
                            [tempDict setObject:[[dataArray objectAtIndex:i] objectForKey:ref4] forKey:BILLERREFERENCE4];
                        }
                        if ([[dataArray objectAtIndex:i] objectForKey:ref5] )
                        {
                            [tempDict setObject:[[dataArray objectAtIndex:i] objectForKey:ref5] forKey:BILLERREFERENCE5];
                        }
                        
                        [finalArray addObject:tempDict];
                    }
                    finalTranArray = [[NSArray alloc] initWithArray:finalArray];
                    activityIndicator.hidden = YES;
                    
                    if ([finalTranArray count] == 0)
                    {
                        childTemplate4_TableView.hidden = YES;
                        alertLabel.hidden = NO;
                    }
                    else
                    {
                        childTemplate4_TableView.hidden = NO;
                        alertLabel.hidden = YES;
                        [childTemplate4_TableView reloadData];
                    }
                }
            }
            else
            {
                childTemplate4_TableView.hidden = YES;
            }
        }
        else if ([processCode isEqualToString:PROCESSOR_CODE_FETCH_BILLS])
        {
            if (webServiceDataObject.PaymentDetails2)
            {
                NSData *jsonData = [webServiceDataObject.PaymentDetails2 dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError];
                
                if (![dataDictionary count])
                {
                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_noresults_found_message", nil)] andErrorCode:nil];
                }
                else
                {
                    NSString *nn = @"nn";
                    NSString *dd = @"dd";
                    NSString *am = @"am";
                    NSString *bpc = @"bpc";
                    NSString *bt = @"bt";
                    NSString *bn = @"bn";
                    NSString *bi = @"bi";
                    NSString *loc = @"loc";
                    NSString *a1 = @"A1";
                    NSString *a2 = @"A2";
                    NSString *a3 = @"A3";
                    NSString *a4 = @"A4";
                    NSString *a5 = @"A5";
                    NSString *ref1 = @"Ref1";
                    NSString *ref2 = @"Ref2";
                    NSString *ref3 = @"Ref3";
                    NSString *ref4 = @"Ref4";
                    NSString *ref5 = @"Ref5";
                    
                    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                    
                    if ([dataDictionary objectForKey:nn])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:nn] forKey:BILLERNICKNAME];
                    }
                    if ([dataDictionary objectForKey:bn])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:bn] forKey:BILLERNAME];
                    }
                    if ([dataDictionary objectForKey:dd])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:dd] forKey:BILLERDUEDATE];
                    }
                    
                    if ([dataDictionary objectForKey:am])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:am] forKey:PARAMETER21];
                    }
                    
                    if ([dataDictionary objectForKey:bpc])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:bpc] forKey:BILLPAYCODE];
                    }
                    
                    if ([dataDictionary objectForKey:bt])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:bt] forKey:BILLERTYPE];
                    }
                    if ([dataDictionary objectForKey:bi])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:bi] forKey:BILLERID];
                    }
                    
                    if ([dataDictionary objectForKey:loc])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:loc] forKey:BILLERLOCATION];
                    }
                    
                    if ([dataDictionary objectForKey:a1])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:a1] forKey:BILLERA1];
                    }
                    if ([dataDictionary objectForKey:a2])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:a2] forKey:BILLERA2];
                    }
                    if ([dataDictionary objectForKey:a3])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:a3] forKey:BILLERA3];
                    }
                    if ([dataDictionary objectForKey:a4] )
                    {
                        [tempDict setObject:[dataDictionary objectForKey:a4] forKey:BILLERA4];
                    }
                    if ([dataDictionary objectForKey:a5])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:a5] forKey:BILLERA5];
                    }
                    
                    if ([dataDictionary objectForKey:ref1])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:ref1] forKey:BILLERREFERENCE1];
                    }
                    if ([dataDictionary objectForKey:ref2])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:ref2] forKey:BILLERREFERENCE2];
                    }
                    if ([dataDictionary objectForKey:ref3])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:ref3] forKey:BILLERREFERENCE3];
                    }
                    if ([dataDictionary objectForKey:ref4])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:ref4] forKey:BILLERREFERENCE4];
                    }
                    if ([dataDictionary objectForKey:ref5])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:ref5] forKey:BILLERREFERENCE5];
                    }
                    
                    if ([dataDictionary objectForKey:ref5])
                    {
                        [tempDict setObject:[dataDictionary objectForKey:ref5] forKey:BILLERREFERENCE5];
                    }
                    NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
                    NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template4_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
                    if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
                    {
                        Class myclass = NSClassFromString(nextTemplate);
                        id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:tempDict withProcessorCode:webServiceDataObject.ProcessorCode withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:finalTranArray withSubIndex:0];
                        
                        [[[self superview] superview] addSubview:(UIView *)obj];
                    }
                }
            }
            else
            {
                childTemplate4_TableView.hidden = YES;
            }
        }
        else if ([processCode isEqualToString:PROCESSOR_CODE_FETCH_MY_BILLERS]) // My billers list data dont move to Base
        {
            if (webServiceDataObject.PaymentDetails2)
            {
                childTemplate4_TableView.hidden = NO;
                local_processorCode = processCode;
                
                NSData *jsonData = [webServiceDataObject.PaymentDetails2 dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSArray *dataDictionaryArray = [[NSArray alloc] initWithArray:[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError]];
                
                if (![dataDictionaryArray count])
                {
                    [self showAlertForErrorWithMessage:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_noresults_found_message", nil)] andErrorCode:nil];
                }
                else
                {
                    NSString *bnn = @"bnn";
                    NSString *bn = @"bn";
                    NSString *type = @"type";
                    NSString *bi = @"bi";
                    NSString *mbi = @"mbi";
                    NSString *a1 = @"A1";
                    NSString *a2 = @"A2";
                    NSString *a3 = @"A3";
                    NSString *a4 = @"A4";
                    NSString *a5 = @"A5";
                    NSString *ref1 = @"Ref1";
                    NSString *ref2 = @"Ref2";
                    NSString *ref3 = @"Ref3";
                    NSString *ref4 = @"Ref4";
                    NSString *ref5 = @"Ref5";
                    
                    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
                    for (int i = 0; i<[dataDictionaryArray count]; i++)
                    {
                        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                        
                        if (![[[dataDictionaryArray objectAtIndex:i] objectForKey:bnn] isEqual:[NSNull null]])
                        {
                            [tempDict setObject:[[dataDictionaryArray objectAtIndex:i] objectForKey:bnn] forKey:BILLERNICKNAME];
                        }
                        
                        if (![[[dataDictionaryArray objectAtIndex:i] objectForKey:bn] isEqual:[NSNull null]])
                        {
                            [tempDict setObject:[[dataDictionaryArray objectAtIndex:i] objectForKey:bn] forKey:BILLERNAME];
                        }
                        if (![[[dataDictionaryArray objectAtIndex:i] objectForKey:type] isEqual:[NSNull null]])
                        {
                            [tempDict setObject:[[dataDictionaryArray objectAtIndex:i] objectForKey:type] forKey:BILLERTYPE];
                        }
                        if (![[[dataDictionaryArray objectAtIndex:i] objectForKey:bi] isEqual:[NSNull null]])
                        {
                            [tempDict setObject:[[dataDictionaryArray objectAtIndex:i] objectForKey:bi] forKey:BILLERID];
                        }
                        
                        if (![[[dataDictionaryArray objectAtIndex:i] objectForKey:mbi] isEqual:[NSNull null]])
                        {
                            [tempDict setObject:[[dataDictionaryArray objectAtIndex:i] objectForKey:mbi] forKey:BILLERMEMBERBILLERID];
                        }
                        
                        if (![[[dataDictionaryArray objectAtIndex:i] objectForKey:a1] isEqual:[NSNull null]])
                        {
                            [tempDict setObject:[[dataDictionaryArray objectAtIndex:i] objectForKey:a1] forKey:BILLERA1];
                        }
                        if (![[[dataDictionaryArray objectAtIndex:i] objectForKey:a2] isEqual:[NSNull null]])
                        {
                            [tempDict setObject:[[dataDictionaryArray objectAtIndex:i] objectForKey:a2] forKey:BILLERA2];
                        }
                        if (![[[dataDictionaryArray objectAtIndex:i] objectForKey:a3] isEqual:[NSNull null]])
                        {
                            [tempDict setObject:[[dataDictionaryArray objectAtIndex:i] objectForKey:a3] forKey:BILLERA3];
                        }
                        if (![[[dataDictionaryArray objectAtIndex:i] objectForKey:a4] isEqual:[NSNull null]])
                        {
                            [tempDict setObject:[[dataDictionaryArray objectAtIndex:i] objectForKey:a4] forKey:BILLERA4];
                        }
                        if (![[[dataDictionaryArray objectAtIndex:i] objectForKey:a5] isEqual:[NSNull null]])
                        {
                            [tempDict setObject:[[dataDictionaryArray objectAtIndex:i] objectForKey:a5] forKey:BILLERA5];
                        }
                        
                        if (![[[dataDictionaryArray objectAtIndex:i] objectForKey:ref1] isEqual:[NSNull null]])
                        {
                            [tempDict setObject:[[dataDictionaryArray objectAtIndex:i] objectForKey:ref1] forKey:BILLERREFERENCE1];
                        }
                        if (![[[dataDictionaryArray objectAtIndex:i] objectForKey:ref2] isEqual:[NSNull null]])
                        {
                            [tempDict setObject:[[dataDictionaryArray objectAtIndex:i] objectForKey:ref2] forKey:BILLERREFERENCE2];
                        }
                        if (![[[dataDictionaryArray objectAtIndex:i] objectForKey:ref3] isEqual:[NSNull null]])
                        {
                            [tempDict setObject:[[dataDictionaryArray objectAtIndex:i] objectForKey:ref3] forKey:BILLERREFERENCE3];
                        }
                        if (![[[dataDictionaryArray objectAtIndex:i] objectForKey:ref4] isEqual:[NSNull null]])
                        {
                            [tempDict setObject:[[dataDictionaryArray objectAtIndex:i] objectForKey:ref4] forKey:BILLERREFERENCE4];
                        }
                        if (![[[dataDictionaryArray objectAtIndex:i] objectForKey:ref5] isEqual:[NSNull null]])
                        {
                            [tempDict setObject:[[dataDictionaryArray objectAtIndex:i] objectForKey:ref5] forKey:BILLERREFERENCE5];
                        }
                        
                        if (![[[dataDictionaryArray objectAtIndex:i] objectForKey:ref5] isEqual:[NSNull null]])
                        {
                            [tempDict setObject:[[dataDictionaryArray objectAtIndex:i] objectForKey:ref5] forKey:BILLERREFERENCE5];
                        }
                        
                        [dataArray addObject:tempDict];
                    }
                    
                    finalTranArray = [[NSArray alloc] initWithArray:dataArray];
                    activityIndicator.hidden = YES;
                    [activityIndicator stopActivityIndicator];
                    
                    [childTemplate4_TableView reloadData];
                }
            }
            else
            {
                childTemplate4_TableView.hidden = YES;
            }
        }
    }
}


#pragma mark - Xml Error handling methods.
/*
 * This method is used to set process data error handling.
 */
-(void)errorCodeHandlingInDBWithCode:(NSString *)errorCode withProcessorCode:(NSString *)processorCode
{
    [self removeActivityIndicator];
}

-(void)errorCodeHandlingInXML:(NSString *)errorCode andMessgae:(NSString *)message withProcessorCode:(NSString *)processorCode
{
    [self removeActivityIndicator];
    
    if ([processorCode isEqualToString:PROCESSOR_CODE_MINI_STATEMENT])
        childTemplate4_TableView.hidden = YES;
    if ([processorCode isEqualToString:PROCESSOR_CODE_TXN_HISTORY_T3])
        childTemplate4_TableView.hidden = YES;
    
    [self showAlertForErrorWithMessage:message andErrorCode:errorCode];
}

-(void)errorCodeHandlingInWebServiceRunningWithErrorCode:(NSString *)errorCode
{
    [self removeActivityIndicator];
    
    [self showAlertForErrorWithMessage:nil andErrorCode:errorCode];
}
/*
 * This method is used to set process data error handling show alert based on type(Ticker or popup).
 */
-(void)showAlertForErrorWithMessage:(NSString *)message andErrorCode:(NSString *)errorCode
{
    [self removeActivityIndicator];
    
    alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    
    NSString *errorMessage = nil;
    
    if (errorCode)
        errorMessage = NSLocalizedStringFromTableInBundle(errorCode,[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil);
    else
        errorMessage = message;
    
    if([alertview_Type compare:TICKER_TEXT] == NSOrderedSame)
    {
        NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
        
        NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
        
        [[[self superview] superview] makeToast:errorMessage duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom
                                backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
    }
    else if ([alertview_Type compare:POPUP_TEXT] == NSOrderedSame)
    {
        PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:0 withTitle:nil andMessage:errorMessage withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
        [[[self superview] superview] addSubview:popup];
    }
    
    alertLabel.hidden = NO;
}

#pragma mark - ChildTemplate4 Values.
/*
 * This method is used to set Childtemplate4 data from Database.
 */
- (void)ct4SetValue:(NSNotification *)notification
{
    databaseManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
    finalTranArray = [[NSArray alloc] initWithArray:[databaseManager getAllMiniStatementDetails]];
    NSLog(@"Final Transaction array is....%@",finalTranArray);
    if ([finalTranArray count] > 0)
    {
        [childTemplate4_TableView reloadData];
        childTemplate4_TableView.hidden = NO;
        alertLabel.hidden = YES;
    }
    else
    {
        childTemplate4_TableView.hidden = YES;
        alertLabel.hidden = NO;
    }
}

- (void) ct4Cancel:(NSNotification *)notification
{
    childTemplate4_TableView.hidden = YES;
    alertLabel.hidden = NO;
}

#pragma mark - Activity Indicator set/Remove
/*
 * This method is used to set Childtemplate4 add Activity indicator.
 */
-(void) setActivityIndicator
{
    [[[[UIApplication sharedApplication] delegate] window] addSubview:activityIndicator];
    activityIndicator.hidden = NO;
    [activityIndicator startActivityIndicator];
}
/*
 * This method is used to set Childtemplate4 remove  Activity indicator.
 */
-(void) removeActivityIndicator
{
    [activityIndicator removeFromSuperview];
    [activityIndicator stopActivityIndicator];
}

- (void)refreshView:(NSNotification *)notification{
//    [childTemplate4_TableView removeFromSuperview];
//    [self addControlesToView];
}


/*
 * This method is used to set Childtemplate4 remove notifications.
 */
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
