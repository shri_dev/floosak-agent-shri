//
//  ChildTemplate4.h
//  Consumer Client
//
//  Created by Integra Micro on 06/05/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopUpTemplate5.h"
#import "XmlParserHandler.h"
#import "ActivityIndicator.h"
#import "DatabaseManager.h"

@protocol ChildTempalte4Delegate <NSObject>
@required
-(void)childTempalte4SelectionAction:(NSDictionary *)dataDictionary withTempalte:(NSString *)template1 withPropertyFile:(NSString *)propertyFile;
@end
/**
 * This class used to handle functionality and View of Childtemplate4.
 *
 * @author Integra
 *
 */
@interface ChildTemplate4 : UIView<UITableViewDataSource,UITableViewDelegate,XMLParserHandlerDelegate>
{
    BOOL hasSelectionOperation;
    
    int labelX_Position;
    int nextY_Position;
    
    NSMutableArray *tableviewData_Arr;
    NSMutableDictionary *tableviewData_Dict;
    
    PopUpTemplate5 *popuptemplate;
    NSArray *dictvalues;
    
    int selectedRow;
    int from;
    
    NSString *pin;
    
    ActivityIndicator *activityIndicator;
    DatabaseManager *databaseManager;
    NSArray *dictArray;
    
    NSString *alertview_Type;
    NSArray *finalTranArray;
    
    NSString *local_processorCode;
    
    NSString *str;
    NSString *str1;
    UILabel *alert_Label;
    
    BOOL isChildButtonAdded;
}

@property(nonatomic,strong) UILabel *headerTitle_Label;
@property(nonatomic,strong) UILabel *headerborder_Label;
@property(nonatomic,strong) UIViewController *mViewController;
@property(nonatomic,strong) UITableView *childTemplate4_TableView;
@property(nonatomic,strong) UILabel *childTemplate4_Label1;
@property(nonatomic,strong) UILabel *childTemplate4_ValueLabel1;
@property(nonatomic,strong) UILabel *childTemplate4_ValueLabel2;
@property(nonatomic,strong) UILabel *childTemplate4_ValueLabel3;
/**
 * declarations are used to set the UIConstraints Of ChildTemplate4.
 * Label and TableView.
 */
@property (nonatomic, assign)NSObject <ChildTempalte4Delegate> *delegate;
@property(nonatomic,strong) NSString *propertyFileName;
@property(nonatomic,strong) UIButton *childButton;

@property (strong, nonatomic) UILabel *alertLabel;

/**
 * This method is  used for Method Initialization of ChildTemplate4.
 */
// Method For initialization.
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView;
/*
 * This method is used to set childtemplate4 Delegate.
 */
-(void)setLocalDelegate:(id)fromDelegate;
-(void)childButtonAction:(id)sender;
@end
