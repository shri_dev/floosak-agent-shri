//
//  ChildTemplate12.m
//  Consumer Client
//
//  Created by android on 9/1/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//
#import "ChildTemplate12.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"
#import "ValidationsClass.h"
#import "DatabaseConstatants.h"
#import "Template.h"
#import "Localization.h"

#import "DatabaseManager.h"
#import "ChildTemplate3.h"
#import "AppDelegate.h"
#import "PopUpTemplate8.h"
#import "WebSericeUtils.h"
#import "WebServiceRequestFormation.h"
#import "NotificationConstants.h"
#import "CustomTextField.h"
@interface ChildTemplate12()
@end
@implementation ChildTemplate12

@synthesize propertyFileName,processor_Code;
@synthesize childTemplate1ScrollView;
@synthesize popupTemplateLabel;
@synthesize titleLabel,titleBorderLabel;
@synthesize key_label1,value_label1,value_border_label1;
@synthesize key_label2,value_label2,value_border_label2;
@synthesize key_label3,value_label3,value_border_label3;
@synthesize key_label4,value_label4,value_border_label4;
@synthesize key_label5,value_label5,value_border_label5;
@synthesize key_label6,value_label6,value_border_label6;
@synthesize key_label7,value_label7,value_border_label7;
@synthesize key_label8,value_label8,value_border_label8;
@synthesize key_label9,value_label9,value_border_label9;
@synthesize key_label10,value_label10,value_border_label10;


@synthesize textfieldTitle_Label1,textfield1,dropDownButton1,textfieldborder_Label1;
@synthesize textfieldTitle_Label2,textfield2,dropDownButton2,textfieldborder_Label2;
@synthesize textfieldTitle_Label3,textfield3,dropDownButton3,textfieldborder_Label3;
@synthesize textfieldTitle_Label4,textfield4,dropDownButton4,textfieldborder_Label4;
@synthesize textfieldTitle_Label5,textfield5,dropDownButton5,textfieldborder_Label5;

@synthesize childButton1,childButton2;

#pragma mark - ChildTemplate12 UIView.
/**
 * This method is used to set implemention of ChildTemplate12.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView
{
    
    NSLog(@"PropertyFileName:%@",propertyFile);
    self = [super initWithFrame:frame];
    if (self)
    {
        propertyFileName = propertyFile;
        localDictionary = [[NSMutableDictionary alloc] initWithDictionary:dataDictionary];
        localArray = dataDictsArray;
        [self addControlesToView];
    }
    
    return self;
}

#pragma mark - ChildTemplate12 UIConstraints Creation.
/**
 * This method is used to set add UIconstraints Frame of ChildTemplate12.
 *@param Type- Label,Text field,drop down and Button
 * Set Text (Size,color and font size).
 */
-(void) addControlesToView
{
    int localTag = 100;
    numberOfFields = 0;
    
    validationsArray = [[NSMutableArray alloc] init];
    
    label_X_Position = 5.0;
    label_Y_Position = 5.0;
    distance_Y = 5.0;
    
    filed_X_Position = 5.0;
    filed_Y_Position = 0.0;
    
    childTemplate1ScrollView = [[UIScrollView alloc] init];
    /*
     * This method is used to add ChildTemplate12 ToolBar (Cancel,Ok buttons).
     */
    numberToolbar = [[UIToolbar alloc] init];
    numberToolbar.frame=CGRectMake(0, 0, SCREEN_WIDTH, 40);
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self  action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Ok" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    
    NSString *selectedIconBtncolors = application_branding_color_theme;
    NSArray *selectedIconBtncolorsArray = [ValidationsClass colorWithHexString:selectedIconBtncolors];
    numberToolbar.tintColor = [UIColor colorWithRed:[[selectedIconBtncolorsArray objectAtIndex:0] floatValue] green:[[selectedIconBtncolorsArray objectAtIndex:1] floatValue] blue:[[selectedIconBtncolorsArray objectAtIndex:2] floatValue] alpha:1.0f];
    
    float scroll_Y_Position = 0;
    
    // Title label Visibility.
    if ([NSLocalizedStringFromTableInBundle(@"child_template12_title_label_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        scroll_Y_Position=30;
        
        // key label Frame declaration.
        titleLabel = [[UILabel alloc] init];
        titleLabel.frame=CGRectMake(0,0,self.frame.size.width,29);
        titleLabel.backgroundColor = [UIColor clearColor];
        
        // Key label String value.
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_title_label",propertyFileName,[NSBundle mainBundle], nil)];
        
        if(labelStr)
            titleLabel.text = labelStr;
        
        // key label alignment.
        titleLabel.textAlignment = NSTextAlignmentCenter;
        
        // key Label font attributes override.
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_title_label_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label Text color.
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template12_title_label_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_title_label_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                titleLabel.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_title_label_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_title_label_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_title_label_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_title_label_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_title_label_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_title_label_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                titleLabel.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        filed_Y_Position = titleLabel.frame.origin.y+titleLabel.frame.size.height;
        [self addSubview:titleLabel];
        
        // Child Template12  Title Border Visibility.
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_title_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            titleBorderLabel = [[UILabel alloc] init];
            titleBorderLabel.frame = CGRectMake(0, filed_Y_Position, self.frame.size.width, 1);
            titleBorderLabel.backgroundColor = [UIColor blackColor];
            [self addSubview:titleBorderLabel];
        }
        
        numberOfFields++;
    }
    childTemplate1ScrollView.backgroundColor = [UIColor whiteColor];
    [self addSubview:childTemplate1ScrollView];
    
    int label_localTag = 101;
       //--------------------------Non-editable Field Starts---------------------//
    
    // Field 1(Key label and Value label) Visibility.
    if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field1_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_value1_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        
           // key label Frame declaration.
            key_label1 = [[UILabel alloc] init];
            key_label1.frame = CGRectMake(label_X_Position, label_Y_Position, self.frame.size.width-(label_X_Position*2), 21);
            key_label1.backgroundColor = [UIColor clearColor];
        
             // Key label String value.
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_label1",propertyFileName,[NSBundle mainBundle], nil)];
            
            if(labelStr)
                key_label1.text = labelStr;
        
            // key label alignment.
            key_label1.textAlignment = NSTextAlignmentLeft;
        
           // key Label font attributes override.
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_label1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                       // Properties for label Text color.
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label1_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label1_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label1_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label1_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label1_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label1_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label1.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label1.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label1.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label1.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [childTemplate1ScrollView addSubview:key_label1];
            filed_Y_Position = key_label1.frame.origin.y+key_label1.frame.size.height+distance_Y;
    
            value_label1 = [[UILabel alloc] init];
            value_label1.frame =  CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 40);
            value_label1.tag = label_localTag;
            label_localTag++;
            value_label1.backgroundColor = [UIColor clearColor];
            
            if (![[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value1_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""])
            {
                dropDownSetArr = [[localDictionary objectForKey:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value1_param_type",propertyFileName,[NSBundle mainBundle], nil)]] componentsSeparatedByString:@"|"];
                if (dropDownSetArr.count > 1) {
                    value_label1.text = [dropDownSetArr objectAtIndex:1];
                }
                else
                    value_label1.text = [dropDownSetArr objectAtIndex:0];
            }
            else
                value_label1.text = application_default_no_value_available;
            
            
            value_label1.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_value1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value1_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value1_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value1_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value1_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value1_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value1_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label1.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label1.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label1.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label1.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            if (value_label1.text)
                [tempDictionary setObject:value_label1.text forKey:@"value"];
               filed_Y_Position = value_label1.frame.origin.y+value_label1.frame.size.height+distance_Y;
            [childTemplate1ScrollView addSubview:value_label1];
        
             // Child Template12  Field Border Visibility.
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field1_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                value_border_label1 = [[UILabel alloc] init];
                value_border_label1.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 1);
                value_border_label1.backgroundColor = [UIColor blackColor];
                filed_Y_Position = filed_Y_Position+value_border_label1.frame.size.height;
                [childTemplate1ScrollView addSubview:value_border_label1];
            }
        
        [labelValidationsArray addObject:tempDictionary];
        numberOfFields++;
    }
    // field2
    if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field2_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_value2_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];

            key_label2 = [[UILabel alloc] init];
            key_label2.frame = CGRectMake(label_X_Position, filed_Y_Position, self.frame.size.width-(label_X_Position*2), 21);
            key_label2.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_label2",propertyFileName,[NSBundle mainBundle], nil)];
            
            if(labelStr)
                key_label2.text = labelStr;
            
            key_label2.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_label2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label2_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label2_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label2_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label2_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label2_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label2_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label2.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label2.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label2.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label2.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label2.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label2.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label2.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label2.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [childTemplate1ScrollView addSubview:key_label2];
            filed_Y_Position =  key_label2.frame.origin.y+key_label2.frame.size.height+distance_Y;
       
        
            value_label2 = [[UILabel alloc] init];
            value_label2.frame =  CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            value_label2.tag = label_localTag;
            label_localTag++;
            value_label2.backgroundColor = [UIColor clearColor];
            
            if (![[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value2_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""])
            {
                dropDownSetArr = [[localDictionary objectForKey:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value2_param_type",propertyFileName,[NSBundle mainBundle], nil)]] componentsSeparatedByString:@"|"];
                if (dropDownSetArr.count > 1) {
                    value_label2.text = [dropDownSetArr objectAtIndex:1];
                }
                else
                    value_label2.text = [localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value2_param_type",propertyFileName,[NSBundle mainBundle], nil)];
            }
            else
                value_label2.text = application_default_no_value_available;
          
            value_label2.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_value2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value2_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value2_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value2_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value2_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value2_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value2_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label2.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label2.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label2.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label2.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label2.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label2.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label2.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label2.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            if (value_label2.text)
                [tempDictionary setObject:value_label2.text forKey:@"value"];
        
            filed_Y_Position = value_label2.frame.origin.y+value_label2.frame.size.height+distance_Y;
            [childTemplate1ScrollView addSubview:value_label2];
        
        // Child Template12 Field  Border Visibility.
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field2_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label2 = [[UILabel alloc] init];
            value_border_label2.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 1);
            value_border_label2.backgroundColor = [UIColor blackColor];
            filed_Y_Position = filed_Y_Position+value_border_label2.frame.size.height;
            [childTemplate1ScrollView addSubview:value_border_label2];
        }

        [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
    //field3
    if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field3_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_value3_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];

            key_label3 = [[UILabel alloc] init];
            key_label3.frame = CGRectMake(label_X_Position, filed_Y_Position, self.frame.size.width-(label_X_Position*2), 21);
            key_label3.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_label3",propertyFileName,[NSBundle mainBundle], nil)];
            
            if(labelStr)
                key_label3.text = labelStr;
            
            key_label3.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label3_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label3_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label3_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label3_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label3_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label3_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label3.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label3.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label3.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label3.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [childTemplate1ScrollView addSubview:key_label3];
            filed_Y_Position = key_label3.frame.origin.y+key_label3.frame.size.height+distance_Y;
        
           value_label3 = [[UILabel alloc] init];
            value_label3.frame =  CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            value_label3.tag = label_localTag;
            label_localTag++;
            value_label3.backgroundColor = [UIColor clearColor];
            
            if (![[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value3_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""])
            {
                dropDownSetArr = [[localDictionary objectForKey:[Localization languageSelectedStringForKey: NSLocalizedStringFromTableInBundle(@"child_template12_value3_param_type",propertyFileName,[NSBundle mainBundle], nil)]] componentsSeparatedByString:@"|"];
                if (dropDownSetArr.count > 1) {
                    value_label3.text = [dropDownSetArr objectAtIndex:1];
                }
                else
                    value_label3.text = [localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value3_param_type",propertyFileName,[NSBundle mainBundle], nil)];
            }
            else
                value_label3.text =application_default_no_value_available;
            
            value_label3.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_value3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value3_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value3_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value3_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value3_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value3_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value3_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label3.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label3.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label3.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label3.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            if (value_label3.text)
                [tempDictionary setObject:value_label3.text forKey:@"value"];
        
            filed_Y_Position = value_label3.frame.origin.y+value_label3.frame.size.height+distance_Y;
            [childTemplate1ScrollView addSubview:value_label3];
        
        // Child Template12 Field  Border Visibility.
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field3_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label3 = [[UILabel alloc] init];
            value_border_label3.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 1);
            value_border_label3.backgroundColor = [UIColor blackColor];
            filed_Y_Position = filed_Y_Position+value_border_label3.frame.size.height;
            [childTemplate1ScrollView addSubview:value_border_label3];
        }
 
        [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
      //field4
    if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field4_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_value4_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        
            key_label4 = [[UILabel alloc] init];
            key_label4.frame = CGRectMake(label_X_Position, filed_Y_Position, self.frame.size.width-(label_X_Position*2), 21);
            key_label4.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_label4",propertyFileName,[NSBundle mainBundle], nil)];
            
            if(labelStr)
                key_label4.text = labelStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_label4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label4_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label4_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label4.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label4_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label4_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label4_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label4_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label4.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label4.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label4.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label4.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label4.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label4.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label4.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label4.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label4.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [childTemplate1ScrollView addSubview:key_label4];
            filed_Y_Position = key_label4.frame.origin.y+key_label4.frame.size.height+distance_Y;
        

            value_label4 = [[UILabel alloc] init];
            value_label4.frame =  CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            value_label4.tag = label_localTag;
            label_localTag++;
            value_label4.backgroundColor = [UIColor clearColor];
            
            if (![[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value4_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""])
            {
                dropDownSetArr = [[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value4_param_type",propertyFileName,[NSBundle mainBundle], nil)] componentsSeparatedByString:@"|"];
                if (dropDownSetArr.count > 1) {
                    value_label4.text = [dropDownSetArr objectAtIndex:1];
                }
                else
                    value_label4.text = [localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value4_param_type",propertyFileName,[NSBundle mainBundle], nil)];
            }
            else
                value_label4.text = application_default_no_value_available;
            
            
            value_label4.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_value4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value4_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value4_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label4.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value4_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value4_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value4_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value4_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label4.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label4.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label4.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label4.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label4.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label4.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label4.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label4.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label4.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
   
            if (value_label4.text)
                [tempDictionary setObject:value_label4.text forKey:@"value"];
        
          filed_Y_Position = value_label4.frame.origin.y+value_label4.frame.size.height+distance_Y;
            [childTemplate1ScrollView addSubview:value_label4];
        
        // Child Template12 Field  Border Visibility.
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field4_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label4 = [[UILabel alloc] init];
            value_border_label4.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 1);
            value_border_label4.backgroundColor = [UIColor blackColor];
            filed_Y_Position = filed_Y_Position+value_border_label4.frame.size.height;
            [childTemplate1ScrollView addSubview:value_border_label4];
        }
            [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
    //field5.
    if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field5_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_value5_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        
            key_label5 = [[UILabel alloc] init];
            key_label5.frame = CGRectMake(label_X_Position, filed_Y_Position, self.frame.size.width-(label_X_Position*2), 21);
            key_label5.backgroundColor = [UIColor clearColor];
 
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_label5",propertyFileName,[NSBundle mainBundle], nil)];
            
            if(labelStr)
                key_label5.text = labelStr;
            
            key_label5.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_label1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label5_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label5_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label5.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label5_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label5_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label5_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label5_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label5.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label5.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label5.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label5.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label5.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label5.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label5.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label5.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label5.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [childTemplate1ScrollView addSubview:key_label5];
            filed_Y_Position = key_label5.frame.origin.y+key_label5.frame.size.height+distance_Y;
        
    
            value_label5 = [[UILabel alloc] init];
            value_label5.frame =  CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            value_label5.tag = label_localTag;
            label_localTag++;
            value_label5.backgroundColor = [UIColor clearColor];
            
            if (![[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value5_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""])
            {
                dropDownSetArr = [[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value5_param_type",propertyFileName,[NSBundle mainBundle], nil)] componentsSeparatedByString:@"|"];
                if (dropDownSetArr.count > 1) {
                    value_label5.text = [dropDownSetArr objectAtIndex:1];
                }
                else
                    value_label5.text = [localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value5_param_type",propertyFileName,[NSBundle mainBundle], nil)];
            }
            else
                value_label5.text = application_default_no_value_available;
            
            value_label5.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_value5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value5_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value5_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label5.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value5_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value5_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value5_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value5_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label5.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label5.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label5.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label5.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label5.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label5.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label5.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label5.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label5.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            if (value_label5.text)
                [tempDictionary setObject:value_label5.text forKey:@"value"];
            filed_Y_Position = value_label5.frame.origin.y+value_label5.frame.size.height+distance_Y;
        
            [childTemplate1ScrollView addSubview:value_label5];
    

        
        // Child Template12 Field  Border Visibility.
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field5_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label5 = [[UILabel alloc] init];
            value_border_label5.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 1);
            value_border_label5.backgroundColor = [UIColor blackColor];
            filed_Y_Position = filed_Y_Position+value_border_label5.frame.size.height;
            [childTemplate1ScrollView addSubview:value_border_label5];
        }
        [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
    //field6
    if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field6_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_value6_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        

            key_label6 = [[UILabel alloc] init];
            key_label6.frame = CGRectMake(label_X_Position, filed_Y_Position, self.frame.size.width-(label_X_Position*2), 21);
            key_label6.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_label6",propertyFileName,[NSBundle mainBundle], nil)];
            
            if(labelStr)
                key_label6.text = labelStr;
            
            key_label6.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_label6_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label6_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label6_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label6.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label6_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label6_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label6_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label6_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label6.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label6.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label6.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label6.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label6.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label6.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label6.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label6.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label6.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [childTemplate1ScrollView addSubview:key_label6];
            filed_Y_Position = key_label6.frame.origin.y+key_label6.frame.size.height+distance_Y;
        

            value_label6 = [[UILabel alloc] init];
            value_label6.frame =  CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            value_label6.tag = label_localTag;
            label_localTag++;
            value_label6.backgroundColor = [UIColor clearColor];
            
            if (![[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value6_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""])
            {
                dropDownSetArr = [[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value6_param_type",propertyFileName,[NSBundle mainBundle], nil)] componentsSeparatedByString:@"|"];
                if (dropDownSetArr.count > 1) {
                    value_label6.text = [dropDownSetArr objectAtIndex:1];
                }
                else
                    value_label6.text = [localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value6_param_type",propertyFileName,[NSBundle mainBundle], nil)];
            }
            else
                value_label6.text = application_default_no_value_available;
            
            value_label6.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_value6_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value6_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value6_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label6.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value6_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value6_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value6_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value6_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label6.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label6.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label6.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label6.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label6.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label6.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label6.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label6.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label6.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            if (value_label6.text)
                [tempDictionary setObject:value_label6.text forKey:@"value"];
        
            filed_Y_Position = value_label6.frame.origin.y+value_label6.frame.size.height+distance_Y;

            [childTemplate1ScrollView addSubview:value_label6];

        // Child Template12 Field  Border Visibility.
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field6_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label6 = [[UILabel alloc] init];
            value_border_label6.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 1);
            value_border_label6.backgroundColor = [UIColor blackColor];
            filed_Y_Position = filed_Y_Position+value_border_label6.frame.size.height;
            [childTemplate1ScrollView addSubview:value_border_label6];
        }
            [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
    //field7
    if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field7_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_value7_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        
    
            key_label7 = [[UILabel alloc] init];
            key_label7.frame = CGRectMake(label_X_Position, filed_Y_Position, self.frame.size.width-(label_X_Position*2), 21);
            key_label7.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_label7",propertyFileName,[NSBundle mainBundle], nil)];
            
            if(labelStr)
                key_label7.text = labelStr;
            
            key_label7.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_label7_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label7_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label7_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label7.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label7_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label7_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label7_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label7_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                   key_label7.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label7.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label7.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label7.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label7.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label7.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label7.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label7.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label7.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [childTemplate1ScrollView addSubview:key_label7];
            filed_Y_Position = key_label7.frame.origin.y+key_label7.frame.size.height+distance_Y;
        
      
            value_label7 = [[UILabel alloc] init];
            value_label7.frame =  CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            value_label7.tag = label_localTag;
            label_localTag++;
            value_label7.backgroundColor = [UIColor clearColor];
            
            if (![[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value7_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""])
            {
                dropDownSetArr = [[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value7_param_type",propertyFileName,[NSBundle mainBundle], nil)] componentsSeparatedByString:@"|"];
                if (dropDownSetArr.count > 1) {
                    value_label7.text = [dropDownSetArr objectAtIndex:1];
                }
                else
                    value_label7.text = [localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value7_param_type",propertyFileName,[NSBundle mainBundle], nil)];
            }
            else
                value_label7.text = application_default_no_value_available;

            
            value_label7.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_value7_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value7_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value7_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label7.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value7_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value7_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value7_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value7_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label7.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label7.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label7.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label7.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label7.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label7.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label7.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label7.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label7.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            if (value_label7.text)
                [tempDictionary setObject:value_label7.text forKey:@"value"];
        
            filed_Y_Position = value_label7.frame.origin.y+value_label7.frame.size.height+distance_Y;

            [childTemplate1ScrollView addSubview:value_label7];
        
        // Child Template12 Field  Border Visibility.
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field7_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label7 = [[UILabel alloc] init];
            value_border_label7.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 1);
            value_border_label7.backgroundColor = [UIColor blackColor];
            filed_Y_Position = filed_Y_Position+value_border_label7.frame.size.height;
            [childTemplate1ScrollView addSubview:value_border_label7];
        }

        [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
    
    //field8
    if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field8_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_value8_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        

            key_label8 = [[UILabel alloc] init];
            key_label8.frame = CGRectMake(label_X_Position, filed_Y_Position, self.frame.size.width-(label_X_Position*2), 21);
            key_label8.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_label8",propertyFileName,[NSBundle mainBundle], nil)];
            
            if(labelStr)
                key_label8.text = labelStr;
            
            key_label8.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_label8_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label8_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label8_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label8.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label8_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label8_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label8_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label8_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label8.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label8.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label8.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label8.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label8.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label8.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label8.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label8.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label8.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [childTemplate1ScrollView addSubview:key_label8];
            filed_Y_Position = key_label8.frame.origin.y+key_label8.frame.size.height+distance_Y;
        
    
            value_label8 = [[UILabel alloc] init];
            value_label8.frame =  CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            value_label8.tag = label_localTag;
            label_localTag++;
            value_label8.backgroundColor = [UIColor clearColor];
            
            if (![[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value8_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""])
            {
                dropDownSetArr = [[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value8_param_type",propertyFileName,[NSBundle mainBundle], nil)] componentsSeparatedByString:@"|"];
                if (dropDownSetArr.count > 1) {
                    value_label8.text = [dropDownSetArr objectAtIndex:1];
                }
                else
                    value_label8.text = [localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value8_param_type",propertyFileName,[NSBundle mainBundle], nil)];
            }
            else
                value_label8.text = application_default_no_value_available;
            
            value_label8.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_value8_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value8_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value8_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label8.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value8_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value8_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value8_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value8_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label8.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label8.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label8.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label8.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label8.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label8.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label8.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label8.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label8.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            if (value_label8.text)
                [tempDictionary setObject:value_label8.text forKey:@"value"];
        
          filed_Y_Position = value_label8.frame.origin.y+value_label8.frame.size.height+distance_Y;
            [childTemplate1ScrollView addSubview:value_label8];
        
        // Child Template12 Field  Border Visibility.
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field8_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label8 = [[UILabel alloc] init];
            value_border_label8.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 1);
            value_border_label8.backgroundColor = [UIColor blackColor];
            filed_Y_Position = filed_Y_Position+value_border_label8.frame.size.height;
            [childTemplate1ScrollView addSubview:value_border_label8];
        }

        [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
    //field9
    if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field9_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_value9_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        
    
            key_label9 = [[UILabel alloc] init];
            key_label9.frame = CGRectMake(label_X_Position, filed_Y_Position, self.frame.size.width-(label_X_Position*2), 21);
            key_label9.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_label9",propertyFileName,[NSBundle mainBundle], nil)];
            
            if(labelStr)
                key_label9.text = labelStr;
            
            key_label9.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_label9_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label9_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label9_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label9.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label9_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label9_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label9_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label9_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label9.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label9.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label9.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label9.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label9.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label9.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label9.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label9.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label9.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
        
            [childTemplate1ScrollView addSubview:key_label9];
            filed_Y_Position = key_label9.frame.origin.y+key_label9.frame.size.height+distance_Y;
        
    
            value_label9 = [[UILabel alloc] init];
            value_label9.frame =  CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            value_label9.tag = label_localTag;
            label_localTag++;
            value_label9.backgroundColor = [UIColor clearColor];
            
            if (![[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value9_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""])
            {
                dropDownSetArr = [[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value9_param_type",propertyFileName,[NSBundle mainBundle], nil)] componentsSeparatedByString:@"|"];
                if (dropDownSetArr.count > 1) {
                    value_label9.text = [dropDownSetArr objectAtIndex:1];
                }
                else
                    value_label9.text = [localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value9_param_type",propertyFileName,[NSBundle mainBundle], nil)];
            }
            else
                value_label9.text = application_default_no_value_available;
            
            value_label9.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_value9_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value9_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value9_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label9.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value9_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value9_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value9_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value9_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label9.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label9.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label9.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label9.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label9.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label9.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label9.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label9.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label9.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            if (value_label9.text)
                [tempDictionary setObject:value_label9.text forKey:@"value"];
        
             filed_Y_Position = value_label9.frame.origin.y+value_label9.frame.size.height+distance_Y;
            [childTemplate1ScrollView addSubview:value_label9];
        // Child Template12 Field  Border Visibility.
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field9_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label9 = [[UILabel alloc] init];
            value_border_label9.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 1);
            value_border_label9.backgroundColor = [UIColor blackColor];
            filed_Y_Position = filed_Y_Position+value_border_label9.frame.size.height;
            [childTemplate1ScrollView addSubview:value_border_label9];
        }
        [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
    //field10.
    if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field10_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:@"" forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_value10_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:@"" forKey:@"validation_message"];
        
    
            key_label10 = [[UILabel alloc] init];
            key_label10.frame = CGRectMake(label_X_Position, filed_Y_Position, self.frame.size.width-(label_X_Position*2), 21);
            key_label10.backgroundColor = [UIColor clearColor];
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_label10",propertyFileName,[NSBundle mainBundle], nil)];
            
            if(labelStr)
                key_label10.text =labelStr;
            
            key_label10.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_label10_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label10_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label10_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    key_label10.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label10_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label10_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label10_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label10_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label10.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label10.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label10.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label10.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    key_label10.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label10.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label10.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label10.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label10.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [childTemplate1ScrollView addSubview:key_label10];
            filed_Y_Position = key_label10.frame.origin.y+key_label10.frame.size.height+distance_Y;
        

            value_label10 = [[UILabel alloc] init];
            value_label10.frame =  CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            value_label10.tag = label_localTag;
            label_localTag++;
            value_label10.backgroundColor = [UIColor clearColor];
            
            if (![[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value10_param_type",propertyFileName,[NSBundle mainBundle], nil)] isEqualToString:@""])
            {
                dropDownSetArr = [[localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value10_param_type",propertyFileName,[NSBundle mainBundle], nil)] componentsSeparatedByString:@"|"];
                if (dropDownSetArr.count > 1) {
                    value_label10.text = [dropDownSetArr objectAtIndex:1];
                }
                else
                    value_label10.text = [localDictionary objectForKey:NSLocalizedStringFromTableInBundle(@"child_template12_value10_param_type",propertyFileName,[NSBundle mainBundle], nil)];
            }
            else
                value_label10.text = application_default_no_value_available;
            
            value_label10.textAlignment = NSTextAlignmentLeft;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_value10_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value10_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value10_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label_TextColor)
                    value_label10.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value10_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value10_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value10_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value10_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label10.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label10.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label10.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label10.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                
                NSArray *label_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label_TextColor)
                    value_label10.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label10.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label10.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label10.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label10.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            if (value_label10.text)
                [tempDictionary setObject:value_label10.text forKey:@"value"];
        
             filed_Y_Position = value_label10.frame.origin.y+value_label10.frame.size.height+distance_Y;
            [childTemplate1ScrollView addSubview:value_label10];

        // Child Template12 Field  Border Visibility.
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_labels_field10_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label10 = [[UILabel alloc] init];
            value_border_label10.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 1);
            value_border_label10.backgroundColor = [UIColor blackColor];
            filed_Y_Position = filed_Y_Position+value_border_label10.frame.size.height;
            [childTemplate1ScrollView addSubview:value_border_label10];
        }
        [labelValidationsArray addObject:tempDictionary];
        
        numberOfFields++;
    }
    
    //--------------------------Editable Field Starts---------------------//
    // Inut field1 (Label,TextField,dropDown and border label).
    if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_field1_visibilty",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label1 = [[UILabel alloc] init];
        
        textfieldTitle_Label1.frame = CGRectMake(label_X_Position, filed_Y_Position, self.frame.size.width-(label_X_Position*2), 21);
        
        textfieldTitle_Label1.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_label1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
           textfieldTitle_Label1.text =labelStr ;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_label1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label1_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template12_input_label1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_label1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label1_TextColor)
                textfieldTitle_Label1.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_input_label1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_label1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_input_label1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_label1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label1.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label1.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label1_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label1_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label1_TextColor)
                textfieldTitle_Label1.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label1.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label1.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label1];
        filed_Y_Position = textfieldTitle_Label1.frame.origin.y+textfieldTitle_Label1.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield1 = [[CustomTextField alloc] init];
            textfield1.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            [textfield1 setBorderStyle:UITextBorderStyleBezel];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (textfield1)
                textfield1.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield1 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield1.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField1TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField1TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField1TextColor)
                    textfield1.textColor = [UIColor colorWithRed:[[textField1TextColor objectAtIndex:0] floatValue] green:[[textField1TextColor objectAtIndex:1] floatValue] blue:[[textField1TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                //
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield1.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield1.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField1TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField1TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField1TextColor)
                    textfield1.textColor = [UIColor colorWithRed:[[textField1TextColor objectAtIndex:0] floatValue] green:[[textField1TextColor objectAtIndex:1] floatValue] blue:[[textField1TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield1.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield1.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield1.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            // property for KeyBoardType
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield1.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield1.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield1.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield1.secureTextEntry=YES;
                textfield1.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:NUMBERKEYBOARDTYPE_4])
                textfield1.keyboardType = UIKeyboardTypeNumberPad;
            else
                textfield1.keyboardType = UIKeyboardTypeDefault;
            
            textfield1.inputAccessoryView = numberToolbar;
            textfield1.delegate = self;
            [textfield1 setTag:100];
            localTag++;
            [self.childTemplate1ScrollView addSubview:textfield1];
            
            filed_Y_Position =  textfield1.frame.origin.y+textfield1.frame.size.height+distance_Y;//textfieldTitle_Label1.frame.origin.y+textfield1.frame.origin.y+textfield1.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_value1_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label1 text] forKey:@"labelName"];
            [tempDictionary setObject:[textfield1 text] forKey:@"value"];
        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value1_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton1 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton1 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),30)];
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton1 setTitleEdgeInsets:titleInsets];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value1_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton1 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton1 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }

            dropDownButton1.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton1 setTag:1];
            localTag++;
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton1 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value1_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton1.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton1.titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton1.titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton1.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    dropDownButton1.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton1.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton1.titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton1.titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton1.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            [dropDownButton1 setExclusiveTouch:YES];
            dropDownButton1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton1.frame.size.width-dropDownButton1.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton1.frame.size.width-dropDownButton1.intrinsicContentSize.width-8.0));
                [dropDownButton1 setTitleEdgeInsets:titleInsets];
                dropDownButton1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton1 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton1];
            
            [[dropDownButton1 layer] setBorderWidth:0.5f];
            [[dropDownButton1 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton1.frame.size.width-20,filed_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];
            filed_Y_Position = dropDownButton1.frame.origin.y+dropDownButton1.frame.size.height+distance_Y;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value1_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value1_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_label1_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton1.titleLabel.text forKey:@"value"];
        }
        
        // Child Template12 input Field Border Visibility.
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_field1_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfieldborder_Label1 = [[UILabel alloc] init];
            textfieldborder_Label1.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),1);
            textfieldborder_Label1.backgroundColor = [UIColor blackColor];
            filed_Y_Position = filed_Y_Position+textfieldborder_Label1.frame.size.height;
            [childTemplate1ScrollView addSubview:textfieldborder_Label1];
        }
        [validationsArray addObject:tempDictionary];
    }
    // Inut field2.
    if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_field2_visibilty",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label2 = [[UILabel alloc] init];
        
        textfieldTitle_Label2.frame = CGRectMake(label_X_Position, filed_Y_Position, self.frame.size.width-(label_X_Position*2), 22);
        
        textfieldTitle_Label2.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_label2_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label2.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_label2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label2_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template12_input_label2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_label2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label2_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label2_TextColor)
                textfieldTitle_Label2.textColor = [UIColor colorWithRed:[[label2_TextColor objectAtIndex:0] floatValue] green:[[label2_TextColor objectAtIndex:1] floatValue] blue:[[label2_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_input_label2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_label2_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_input_label2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_label2_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label2.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label2.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label2.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label2.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label2_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label2_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label2_TextColor)
                textfieldTitle_Label2.textColor = [UIColor colorWithRed:[[label2_TextColor objectAtIndex:0] floatValue] green:[[label2_TextColor objectAtIndex:1] floatValue] blue:[[label2_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label2.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label2.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label2.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label2.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label2];
        
        filed_Y_Position = textfieldTitle_Label2.frame.origin.y+textfieldTitle_Label2.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield2 = [[CustomTextField alloc] init];
            textfield2.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            [textfield2 setBorderStyle:UITextBorderStyleBezel];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (textfield2)
                textfield2.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield2 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield2.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:2.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField2TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField2TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField2TextColor)
                    textfield2.textColor = [UIColor colorWithRed:[[textField2TextColor objectAtIndex:0] floatValue] green:[[textField2TextColor objectAtIndex:1] floatValue] blue:[[textField2TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                //
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield2.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield2.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield2.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield2.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField2TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField2TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField2TextColor)
                    textfield2.textColor = [UIColor colorWithRed:[[textField2TextColor objectAtIndex:0] floatValue] green:[[textField2TextColor objectAtIndex:1] floatValue] blue:[[textField2TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield2.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield2.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield2.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield2.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            // property for KeyBoardType
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield2.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield2.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield2.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield2.secureTextEntry=YES;
                textfield2.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:NUMBERKEYBOARDTYPE_4])
                textfield2.keyboardType = UIKeyboardTypeNumberPad;
            else
                textfield2.keyboardType = UIKeyboardTypeDefault;
            
            textfield2.inputAccessoryView = numberToolbar;
            textfield2.delegate = self;
            [textfield2 setTag:101];
            localTag++;
            [self.childTemplate1ScrollView addSubview:textfield2];
            
            filed_Y_Position = textfield2.frame.origin.y+textfield2.frame.size.height+distance_Y;//textfieldTitle_Label2.frame.origin.y+textfield2.frame.origin.y+textfield2.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_value2_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label2 text] forKey:@"labelName"];
            [tempDictionary setObject:[textfield2 text] forKey:@"value"];
        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton2 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton2 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),30)];
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton2 setTitleEdgeInsets:titleInsets];
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value2_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton2 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton2 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton2.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            //            [dropDownButton2 setTag:localTag];
            [dropDownButton2 setTag:2];
            localTag++;
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton2 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value2_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value2_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton2.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton2.titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton2.titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton2.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton2.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton2.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton2.titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                   dropDownButton2.titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton2.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            [dropDownButton2 setExclusiveTouch:YES];
            dropDownButton2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton2.frame.size.width-dropDownButton2.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton2.frame.size.width-dropDownButton2.intrinsicContentSize.width-8.0));
                [dropDownButton2 setTitleEdgeInsets:titleInsets];
                dropDownButton2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton2 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton2];
            
            [[dropDownButton2 layer] setBorderWidth:0.5f];
            [[dropDownButton2 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton2.frame.size.width-20,filed_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];
            filed_Y_Position = dropDownButton2.frame.origin.y+dropDownButton2.frame.size.height+distance_Y;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value2_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value2_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_label2_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton2.titleLabel.text forKey:@"value"];
        }
        // Child Template12 input  Field Border Visibility.
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_field2_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfieldborder_Label2 = [[UILabel alloc] init];
            textfieldborder_Label2.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),1);
            textfieldborder_Label2.backgroundColor = [UIColor blackColor];
            filed_Y_Position = filed_Y_Position+textfieldborder_Label2.frame.size.height;
            [childTemplate1ScrollView addSubview:textfieldborder_Label2];
        }
        
        [validationsArray addObject:tempDictionary];
    }
    // Inut field3.
    if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_field3_visibilty",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label3 = [[UILabel alloc] init];
        
        textfieldTitle_Label3.frame = CGRectMake(label_X_Position, filed_Y_Position, self.frame.size.width-(label_X_Position*2), 22);
        
        textfieldTitle_Label3.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_label3_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label3.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label3_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template12_input_label3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label3_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_label3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label3_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label3_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label3_TextColor)
                textfieldTitle_Label3.textColor = [UIColor colorWithRed:[[label3_TextColor objectAtIndex:0] floatValue] green:[[label3_TextColor objectAtIndex:1] floatValue] blue:[[label3_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_input_label3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_label3_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_input_label3_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_label3_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label3.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label3.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label2_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label2_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label2_TextColor)
                textfieldTitle_Label3.textColor = [UIColor colorWithRed:[[label2_TextColor objectAtIndex:0] floatValue] green:[[label2_TextColor objectAtIndex:1] floatValue] blue:[[label2_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label3.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label3.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label3];
        
        filed_Y_Position = textfieldTitle_Label3.frame.origin.y+textfieldTitle_Label3.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield3 = [[CustomTextField alloc] init];
            textfield3.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            [textfield3 setBorderStyle:UITextBorderStyleBezel];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (textfield3)
                textfield3.placeholder =valueStr ;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield3 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield3.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:2.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField2TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField2TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField2TextColor)
                    textfield3.textColor = [UIColor colorWithRed:[[textField2TextColor objectAtIndex:0] floatValue] green:[[textField2TextColor objectAtIndex:1] floatValue] blue:[[textField2TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                //
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield3.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield3.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField2TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField2TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField2TextColor)
                    textfield3.textColor = [UIColor colorWithRed:[[textField2TextColor objectAtIndex:0] floatValue] green:[[textField2TextColor objectAtIndex:1] floatValue] blue:[[textField2TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield3.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield3.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield3.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            // property for KeyBoardType
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield3.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield3.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield3.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield3.secureTextEntry=YES;
                textfield3.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:NUMBERKEYBOARDTYPE_4])
                textfield3.keyboardType = UIKeyboardTypeNumberPad;
            else
                textfield3.keyboardType = UIKeyboardTypeDefault;
            
            textfield3.inputAccessoryView = numberToolbar;
            textfield3.delegate = self;
            [textfield3 setTag:102];
            localTag++;
            [self.childTemplate1ScrollView addSubview:textfield3];
            
            filed_Y_Position = textfield3.frame.origin.y+textfield3.frame.size.height+distance_Y;//textfieldTitle_Label3.frame.origin.y+textfield3.frame.origin.y+textfield3.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_value3_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label3 text] forKey:@"labelName"];
            [tempDictionary setObject:[textfield3 text] forKey:@"value"];
        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value3_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton3 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton3 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),30)];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value3_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton3 setTitleEdgeInsets:titleInsets];
            if (dropDownStr) {
                [dropDownButton3 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton3 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }

            dropDownButton3.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            //            [dropDownButton3 setTag:localTag];
            [dropDownButton3 setTag:3];
            localTag++;
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton3 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value3_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value3_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value3_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton3.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton3.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton3.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton3.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton3.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton3.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton3.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton3.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton3.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            [dropDownButton3 setExclusiveTouch:YES];
            dropDownButton3.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton3.frame.size.width-dropDownButton3.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton3.frame.size.width-dropDownButton3.intrinsicContentSize.width-8.0));
                [dropDownButton3 setTitleEdgeInsets:titleInsets];
                dropDownButton3.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton3 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton3];
            
            [[dropDownButton3 layer] setBorderWidth:0.5f];
            [[dropDownButton3 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton3.frame.size.width-20,filed_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];
            filed_Y_Position = dropDownButton3.frame.origin.y+dropDownButton3.frame.size.height+distance_Y;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value3_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value3_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];

            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_label3_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];

            [tempDictionary setObject:dropDownButton3.titleLabel.text forKey:@"value"];
        }
        // Child Template12 input  Field Border Visibility.
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_field3_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfieldborder_Label3 = [[UILabel alloc] init];
            textfieldborder_Label3.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),1);
            textfieldborder_Label3.backgroundColor = [UIColor blackColor];
            filed_Y_Position = filed_Y_Position+textfieldborder_Label3.frame.size.height;
            [childTemplate1ScrollView addSubview:textfieldborder_Label3];
        }
        
        [validationsArray addObject:tempDictionary];
    }
    // Inut field4.
      if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_field4_visibilty",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label4 = [[UILabel alloc] init];
        
        textfieldTitle_Label4.frame = CGRectMake(label_X_Position, filed_Y_Position, self.frame.size.width-(label_X_Position*2), 22);
        
        textfieldTitle_Label4.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_label4_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label4.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_label4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label4_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template12_input_label4_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label4_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_label4_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label4_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label4_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label4_TextColor)
                textfieldTitle_Label4.textColor = [UIColor colorWithRed:[[label4_TextColor objectAtIndex:0] floatValue] green:[[label4_TextColor objectAtIndex:1] floatValue] blue:[[label4_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_input_label4_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_label4_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_input_label4_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_label4_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label4.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label4.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label4.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label2_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label2_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label2_TextColor)
                textfieldTitle_Label4.textColor = [UIColor colorWithRed:[[label2_TextColor objectAtIndex:0] floatValue] green:[[label2_TextColor objectAtIndex:1] floatValue] blue:[[label2_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label4.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label4.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label4.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label4];
        
        filed_Y_Position = textfieldTitle_Label4.frame.origin.y+textfieldTitle_Label4.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield4 = [[CustomTextField alloc] init];
            textfield4.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            [textfield4 setBorderStyle:UITextBorderStyleBezel];
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (textfield4)
                textfield4.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield4 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield4.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:2.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField2TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField2TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField2TextColor)
                    textfield4.textColor = [UIColor colorWithRed:[[textField2TextColor objectAtIndex:0] floatValue] green:[[textField2TextColor objectAtIndex:1] floatValue] blue:[[textField2TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                //
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield4.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield4.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield4.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField2TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField2TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField2TextColor)
                    textfield4.textColor = [UIColor colorWithRed:[[textField2TextColor objectAtIndex:0] floatValue] green:[[textField2TextColor objectAtIndex:1] floatValue] blue:[[textField2TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield4.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield4.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield4.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            // property for KeyBoardType
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield4.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield4.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield4.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield4.secureTextEntry=YES;
                textfield4.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:NUMBERKEYBOARDTYPE_4])
                textfield4.keyboardType = UIKeyboardTypeNumberPad;
            else
                textfield4.keyboardType = UIKeyboardTypeDefault;
            
            textfield4.inputAccessoryView = numberToolbar;
            textfield4.delegate = self;
            [textfield4 setTag:103];
            localTag++;
            [self.childTemplate1ScrollView addSubview:textfield4];
            
            filed_Y_Position = textfield4.frame.origin.y+textfield4.frame.size.height+distance_Y;//textfieldTitle_Label3.frame.origin.y+textfield3.frame.origin.y+textfield3.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_value4_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label4 text] forKey:@"labelName"];
            [tempDictionary setObject:[textfield4 text] forKey:@"value"];
        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value4_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton4 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton4 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),40)];
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton4 setTitleEdgeInsets:titleInsets];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value4_hint",propertyFileName,[NSBundle mainBundle], nil)];
            if (dropDownStr) {
                [dropDownButton4 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton4 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            dropDownButton4.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            //            [dropDownButton4 setTag:localTag];
            [dropDownButton4 setTag:4];
            localTag++;
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value4_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value4_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton4 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value4_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value4_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value4_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value4_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton4.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton4.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton4.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton4.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton4.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton4.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton4.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton4.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton4.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            [dropDownButton4 setExclusiveTouch:YES];
            dropDownButton4.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton4.frame.size.width-dropDownButton4.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton4.frame.size.width-dropDownButton4.intrinsicContentSize.width-8.0));
                [dropDownButton4 setTitleEdgeInsets:titleInsets];
                dropDownButton4.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton4 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton4];
            
            [[dropDownButton4 layer] setBorderWidth:0.5f];
            [[dropDownButton4 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton4.frame.size.width-20,filed_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];
            filed_Y_Position = dropDownButton4.frame.origin.y+dropDownButton4.frame.size.height+distance_Y;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value4_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value4_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_label4_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];

            [tempDictionary setObject:dropDownButton4.titleLabel.text forKey:@"value"];
        }
        // Child Template12  input Field Border Visibility.
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_field4_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfieldborder_Label4 = [[UILabel alloc] init];
            textfieldborder_Label4.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),1);
            textfieldborder_Label4.backgroundColor = [UIColor blackColor];
            filed_Y_Position = filed_Y_Position+textfieldborder_Label4.frame.size.height;
            [childTemplate1ScrollView addSubview:textfieldborder_Label4];
        }
        
        [validationsArray addObject:tempDictionary];
    }
    // Inut field5.
    if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_field5_visibilty",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        numberOfFields++;
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label5 = [[UILabel alloc] init];
        
        textfieldTitle_Label5.frame = CGRectMake(label_X_Position, filed_Y_Position, self.frame.size.width-(label_X_Position*2), 22);
        
        textfieldTitle_Label5.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_label5_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label5.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_label5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label5_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template12_input_label5_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label5_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_label5_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label5_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label5_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label5_TextColor)
                textfieldTitle_Label5.textColor = [UIColor colorWithRed:[[label5_TextColor objectAtIndex:0] floatValue] green:[[label5_TextColor objectAtIndex:1] floatValue] blue:[[label5_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_input_label5_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_label5_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_input_label5_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_label5_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label5.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label5.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label5.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label2_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label2_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label2_TextColor)
                textfieldTitle_Label5.textColor = [UIColor colorWithRed:[[label2_TextColor objectAtIndex:0] floatValue] green:[[label2_TextColor objectAtIndex:1] floatValue] blue:[[label2_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label5.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label5.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label5.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childTemplate1ScrollView addSubview:textfieldTitle_Label5];
        
        filed_Y_Position = textfieldTitle_Label5.frame.origin.y+textfieldTitle_Label5.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield5 = [[CustomTextField alloc] init];
            textfield5.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2), 30);
            [textfield5 setBorderStyle:UITextBorderStyleBezel];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (textfield5)
                textfield5.placeholder=valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield5 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield5.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:2.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField2TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField2TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField2TextColor)
                    textfield5.textColor = [UIColor colorWithRed:[[textField2TextColor objectAtIndex:0] floatValue] green:[[textField2TextColor objectAtIndex:1] floatValue] blue:[[textField2TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                //
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield5.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield5.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield5.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField2TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField2TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField2TextColor)
                    textfield5.textColor = [UIColor colorWithRed:[[textField2TextColor objectAtIndex:0] floatValue] green:[[textField2TextColor objectAtIndex:1] floatValue] blue:[[textField2TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template1_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield5.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield5.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield5.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            // property for KeyBoardType
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield5.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield5.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield5.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield5.secureTextEntry=YES;
                textfield5.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:NUMBERKEYBOARDTYPE_4])
                textfield5.keyboardType = UIKeyboardTypeNumberPad;
            else
                textfield5.keyboardType = UIKeyboardTypeDefault;
            
            textfield5.inputAccessoryView = numberToolbar;
            textfield5.delegate = self;
            [textfield5 setTag:104];
            localTag++;
            [self.childTemplate1ScrollView addSubview:textfield5];
            
            filed_Y_Position = textfield5.frame.origin.y+textfield5.frame.size.height+distance_Y;//textfieldTitle_Label5.frame.origin.y+textfield5.frame.origin.y+textfield5.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_value5_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label5 text] forKey:@"labelName"];
            [tempDictionary setObject:[textfield5 text] forKey:@"value"];
        }
        else if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value5_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
             dropDownButton5 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton5 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),50)];
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton5 setTitleEdgeInsets:titleInsets];
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value5_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton5 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton5 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }

            dropDownButton5.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            //            [dropDownButton5 setTag:localTag];
            [dropDownButton5 setTag:5];
            localTag++;
            if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value5_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value5_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton5 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value5_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value5_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value5_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value5_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize =NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton5.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton5.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton5.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton5.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton5.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton5.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton5.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton5.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton5.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            [dropDownButton5 setExclusiveTouch:YES];
            dropDownButton5.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton5.frame.size.width-dropDownButton5.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton5.frame.size.width-dropDownButton5.intrinsicContentSize.width-8.0));
                [dropDownButton5 setTitleEdgeInsets:titleInsets];
                dropDownButton5.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton5 addTarget:self action:@selector(selectDropDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.childTemplate1ScrollView addSubview:dropDownButton5];
            
            [[dropDownButton5 layer] setBorderWidth:0.5f];
            [[dropDownButton5 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton5.frame.size.width-20,filed_Y_Position+12,15,8);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [self.childTemplate1ScrollView addSubview:imageView];
            filed_Y_Position = dropDownButton5.frame.origin.y+dropDownButton5.frame.size.height+distance_Y;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value5_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"child_template12_input_dropdown_value5_param_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_input_label5_text",propertyFileName,[NSBundle mainBundle], nil)] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton5.titleLabel.text forKey:@"value"];
        }
        // Child Template12  input Field Border Visibility.
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_input_field5_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfieldborder_Label5 = [[UILabel alloc] init];
            textfieldborder_Label5.frame = CGRectMake(filed_X_Position, filed_Y_Position, self.frame.size.width-(filed_X_Position*2),1);
            textfieldborder_Label5.backgroundColor = [UIColor blackColor];
            filed_Y_Position = filed_Y_Position+textfieldborder_Label5.frame.size.height;
            [childTemplate1ScrollView addSubview:textfieldborder_Label5];
        }
        [validationsArray addObject:tempDictionary];
    }
    [childTemplate1ScrollView setContentSize:CGSizeMake(SCREEN_WIDTH-96, numberOfFields*(30+21+5)+30)];
    childTemplate1ScrollView.frame = CGRectMake(0, scroll_Y_Position, self.frame.size.width , self.frame.size.height-100);
    
    if (filed_Y_Position>180)
        childTemplate1ScrollView.scrollEnabled = YES;
    else
        childTemplate1ScrollView.scrollEnabled = NO;
    
    //-------------------------Buttons------------------//
    // Button 1 And Button2 both Visibility true.
    if ([NSLocalizedStringFromTableInBundle(@"child_template12_button1_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame && [NSLocalizedStringFromTableInBundle(@"child_template12_button2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        childButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [childButton1 setFrame:CGRectMake(6,self.frame.size.height-50,(self.frame.size.width/2)-10,40)];
        
        NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if(buttonStr)
            [childButton1 setTitle:buttonStr forState:UIControlStateNormal];
        
        // properties For Button backgroundColor
        NSArray *childbutton1Color;
        if (NSLocalizedStringFromTableInBundle(@"child_template12_button1_background_color",propertyFileName,[NSBundle mainBundle], nil))
            childbutton1Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_button1_background_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
            childbutton1Color=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (childbutton1Color)
            childButton1.backgroundColor=[UIColor colorWithRed:[[childbutton1Color objectAtIndex:0] floatValue] green:[[childbutton1Color objectAtIndex:1] floatValue] blue:[[childbutton1Color objectAtIndex:2] floatValue] alpha:1.0f];
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_button1_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *childButton1TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template12_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template12_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                childButton1TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (childButton1TextColor)
                [childButton1 setTitleColor:[UIColor colorWithRed:[[childButton1TextColor objectAtIndex:0] floatValue] green:[[childButton1TextColor objectAtIndex:1] floatValue] blue:[[childButton1TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            
            // Properties for Button Font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                childButton1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                childButton1.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                childButton1.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                childButton1.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        else
        {
            //Default Properties for Button textcolor
            NSArray *childButton1TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template12_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                childButton1TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (childButton1TextColor)
                [childButton1 setTitleColor:[UIColor colorWithRed:[[childButton1TextColor objectAtIndex:0] floatValue] green:[[childButton1TextColor objectAtIndex:1] floatValue] blue:[[childButton1TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                childButton1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                childButton1.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                childButton1.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                childButton1.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        
        [childButton1 setTag:101];
        [childButton1 setExclusiveTouch:YES];
        [childButton1 addTarget:self action:@selector(childButton1Action:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:childButton1];
        
        childButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
        [childButton2 setFrame:CGRectMake((childButton1.frame.origin.x+childButton1.frame.size.width+9),self.frame.size.height-50,(self.frame.size.width/2)-10,40)];
        
        NSString *buttonStr1=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_button2_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if(buttonStr1)
            [childButton2 setTitle:buttonStr1 forState:UIControlStateNormal];
        
        // properties For Button backgroundColor
        NSArray *childButton2Color;
        if (NSLocalizedStringFromTableInBundle(@"child_template12_button2_background_color",propertyFileName,[NSBundle mainBundle], nil))
            childButton2Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_button2_background_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            childButton2Color=[ValidationsClass colorWithHexString:application_default_button_background_color];
        if (childButton2Color)
            childButton2.backgroundColor=[UIColor colorWithRed:[[childButton2Color objectAtIndex:0] floatValue] green:[[childButton2Color objectAtIndex:1] floatValue] blue:[[childButton2Color objectAtIndex:2] floatValue] alpha:1.0f];
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_button2_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            NSArray *childButton2TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template12_button2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_button2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template12_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                childButton2TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (childButton2TextColor)
                [childButton2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_button2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_button2_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            // Properties for Button Font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_button2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_button2_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                childButton2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                childButton2.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                childButton2.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                childButton2.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for Button textcolor
            NSArray *childButton2TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template12_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                childButton2TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (childButton2TextColor)
                [childButton2 setTitleColor:[UIColor colorWithRed:[[childButton2TextColor objectAtIndex:0] floatValue] green:[[childButton2TextColor objectAtIndex:1] floatValue] blue:[[childButton2TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                childButton2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                childButton2.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                childButton2.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                childButton2.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [childButton2 setTag:102];
        [childButton2 setExclusiveTouch:YES];
        [childButton2 addTarget:self action:@selector(childButton2Action:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:childButton2];
    }
    // Button 1  Visibility true.
    else if ([NSLocalizedStringFromTableInBundle(@"child_template12_button1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        childButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
        childButton1.frame = CGRectMake(10,self.frame.size.height-50, self.frame.size.width-20, 40);
        
        // properties For Button backgroundColor
        NSArray *childButton1Color;
        if (NSLocalizedStringFromTableInBundle(@"child_template12_button1_background_color",propertyFileName,[NSBundle mainBundle], nil))
            childButton1Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_button1_background_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            childButton1Color=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (childButton1Color)
            childButton1.backgroundColor=[UIColor colorWithRed:[[childButton1Color objectAtIndex:0] floatValue] green:[[childButton1Color objectAtIndex:1] floatValue] blue:[[childButton1Color objectAtIndex:2] floatValue] alpha:1.0f];
        
        NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if(buttonStr)
            [childButton1 setTitle:buttonStr forState:UIControlStateNormal];
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_button1_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *childButton1TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template12_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template12_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                childButton1TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (childButton1TextColor)
                [childButton1 setTitleColor:[UIColor colorWithRed:[[childButton1TextColor objectAtIndex:0] floatValue] green:[[childButton1TextColor objectAtIndex:1] floatValue] blue:[[childButton1TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            
            // Properties for Button Font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                childButton1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                childButton1.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                childButton1.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                childButton1.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        else
        {
            //Default Properties for Button textcolor
            NSArray *childButton1TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template12_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                childButton1TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (childButton1TextColor)
                [childButton1 setTitleColor:[UIColor colorWithRed:[[childButton1TextColor objectAtIndex:0] floatValue] green:[[childButton1TextColor objectAtIndex:1] floatValue] blue:[[childButton1TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                childButton1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                childButton1.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                childButton1.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                childButton1.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childButton1 setTag:101];
        [childButton1 setExclusiveTouch:YES];
        [childButton1 addTarget:self action:@selector(childButton1Action:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:childButton1];
    }
    // Button 2  Visibility true.
    else if ([NSLocalizedStringFromTableInBundle(@"child_template12_button2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        childButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
        childButton2.frame = CGRectMake(10,self.frame.size.height-60, childTemplate1ScrollView.frame.size.width-(label_X_Position*4), 40);
        
        NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template12_button2_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if(buttonStr)
            [childButton2 setTitle:buttonStr forState:UIControlStateNormal];
        
        // properties For Button backgroundColor
        NSArray *childButton2Color;
        if (NSLocalizedStringFromTableInBundle(@"child_template12_button2_background_color",propertyFileName,[NSBundle mainBundle], nil))
            childButton2Color=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_button2_background_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            childButton2Color=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (childButton2Color)
            childButton2.backgroundColor=[UIColor   colorWithRed:[[childButton2Color objectAtIndex:0] floatValue] green:[[childButton2Color objectAtIndex:1] floatValue] blue:[[childButton2Color objectAtIndex:2] floatValue] alpha:1.0f];
        
        if ([NSLocalizedStringFromTableInBundle(@"child_template12_button2_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            NSArray *childButton2TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template12_button2_background_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_button2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template12_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                childButton2TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (childButton2TextColor)
                [childButton2 setTitleColor:[UIColor colorWithRed:[[childButton2TextColor objectAtIndex:0] floatValue] green:[[childButton2TextColor objectAtIndex:1] floatValue] blue:[[childButton2TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_button2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_button2_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            
            // Properties for Button Font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_button2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_button2_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template12_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                childButton2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                childButton2.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                childButton2.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                childButton2.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for Button textcolor
            NSArray *childButton2TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template12_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                childButton2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template12_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                childButton2TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (childButton2TextColor)
                [childButton2 setTitleColor:[UIColor colorWithRed:[[childButton2TextColor objectAtIndex:0] floatValue] green:[[childButton2TextColor objectAtIndex:1] floatValue] blue:[[childButton2TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template12_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template12_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template12_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                childButton2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                childButton2.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                childButton2.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                childButton2.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childButton2 setTag:102];
        [childButton2 setExclusiveTouch:YES];
        [childButton2 addTarget:self action:@selector(childButton2Action:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:childButton2];
    }
    // To set ActiVity indicator for this view.
   activityIndicator = [[ActivityIndicator alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [[self superview] addSubview:activityIndicator];
    activityIndicator.hidden = YES;
}


#pragma mark - Get DropDown Data Template.
/*
 * This method is used to add ChildTemplate12 dropdown button action.
 */
-(void)selectDropDown:(id)sender
{
       [self endEditing:YES];
    btn = (UIButton *)sender;
    dropdownString = [NSString stringWithFormat:@"child_template12_input_dropdown_value%ld",(long)btn.tag];
    
    DatabaseManager *dataManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
    dropDownDataArray = [[NSArray alloc] initWithArray:[dataManager getAllDropDownDetailsForKey:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_data_webservice_name"],propertyFileName,[NSBundle mainBundle], nil)]];
    
    if (dropDownDataArray.count == 0) {
        dropDownDataArray = localArray;
    }
    nextTemplateProperty =  NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template_properties_file"],propertyFileName,[NSBundle mainBundle], nil);
    
        nextTemplate = NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template"],propertyFileName,[NSBundle mainBundle], nil);
    
    if ([NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template"],propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"1"]){
        nextTemplate=CHILD_TEMPLATE_3;
    }
    else if ([NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template"],propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"2"]){
        nextTemplate=POPUP_TEMPLATE_8;
    }

    if (dropDownDataArray.count > 0) {
        if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
        {
            Class nextClass = NSClassFromString(nextTemplate);
            id object = nil;
            
            if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)])
            {
                object = [[nextClass alloc] initWithNibName:nextTemplate bundle:nil withPropertyName:nextTemplateProperty hasDidSelectFunction:NO withDataDictionary:nil withDataArray:dropDownDataArray withPIN:nil withProcessorCode:nil withType:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_data_webservice_name"],propertyFileName,[NSBundle mainBundle], nil) fromView:0 insideView:nil];
                [(ChildTemplate3 *)object setBaseSelector:@selector(update:)];
                [(ChildTemplate3 *)object setBasetarget:self];
                AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                UINavigationController *navCntrl = (UINavigationController *)delegate.window.rootViewController;
                if (navCntrl && [navCntrl isKindOfClass:[UINavigationController class]])
                {
                    [navCntrl pushViewController:object animated:NO];
                }
            }
            else if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray:withSubIndex:)])
            {
                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:0 withDataArray:dropDownDataArray?dropDownDataArray:nil withSubIndex:0];
                [object setBasetarget:self];
                [object setBaseSelector:@selector(update:)];
                [[[self superview] superview] addSubview:(UIView *)object];
            }
        }
    }
}


#pragma mark - Notification delegates.
/*
 * This method is used to add ChildTemplate12 dropdown updated data to particular field.
 */
- (void)update:(id)object
{
    NSString *dropDownValue = [object objectForKey:DROP_DOWN_TYPE_DESC];
    
    if (dropDownValue.length == 0) {
        dropDownValue = [object objectForKey:DROP_DOWN_TYPE_NAME];
    }
    
    [btn setTitle:dropDownValue forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setTitle:dropDownValue forState:UIControlStateHighlighted];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    
    NSString *typeName = [NSString stringWithFormat:@"%@_typeName",NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_data_webservice_name"],propertyFileName,[NSBundle mainBundle], nil)];
    NSString *typeDesc = [NSString stringWithFormat:@"%@_typeDesc",NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_data_webservice_name"],propertyFileName,[NSBundle mainBundle], nil)];
    
    if ([object objectForKey:@"typeName"]) {
        [localDictionary setObject:[object objectForKey:@"typeName"] forKey:typeName];
    }
    
    if ([object objectForKey:@"typeDesc"]) {
        [localDictionary setObject:[object objectForKey:@"typeDesc"] forKey:typeDesc];
    }
    
    [localDictionary setObject:[object objectForKey:DROP_DOWN_TYPE_NAME] forKey:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_param_type"],propertyFileName,[NSBundle mainBundle], nil)];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UpdateData" object:nil];
}

#pragma mark - Button Action methods.
/**
 * This method is used to set button1 action of ChildTemplate12.
 @prameters are defined in button action those are
 * Application display type(ticker,PopUp),validation type,next template,next template PropertyFile,Action type,
 Feature-(processorCode,transaction type).
 */
-(void)childButton1Action:(id)sender
{
    [activeField resignFirstResponder];
    
    action_type = NSLocalizedStringFromTableInBundle(@"child_template12_button1_action_type",propertyFileName,[NSBundle mainBundle], nil);
    validation_Type = NSLocalizedStringFromTableInBundle(@"application_validation_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    
    data = NSLocalizedStringFromTableInBundle(@"child_template12_button1_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    processorCodeStr = [Template getProcessorCodeWithData:data];
    transactionType = [Template getTransactionCodeWithData:data];
    referenceParam = [Template getReferenceParameter:data];

    alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template12_button1_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template12_button1_next_template",propertyFileName,[NSBundle mainBundle], nil);
    paramType = nil;
    if (NSLocalizedStringFromTableInBundle(@"child_template12_button1_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil))
    {
        paramType = NSLocalizedStringFromTableInBundle(@"child_template12_button1_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil);
    }
    NSString *parameterValue = [Template getReferenceParameter:data];
    
    if (transactionType) {
        [localDictionary setObject:transactionType forKey:PARAMETER13];
    }
    if (referenceParam) {
        [localDictionary setObject:referenceParam forKey:PARAMETER38];
    }
    [localDictionary setObject:processorCodeStr forKey:PARAMETER15];
    
    if (parameterValue) {
        [localDictionary setObject:parameterValue forKey:PARAMETER38];
    }
    
    [datavalueDictionary addEntriesFromDictionary:localDictionary];
    currentClass = CHILD_TEMPLATE_12;
    buttonActionString = CT12_BUTTON1_ACTION;
    
    [self configureWithProcessorCode];
}
/**
 * This method is used to set button2 action of ChildTemplate12.
 */
-(void)childButton2Action:(id)sender
{
    [activeField resignFirstResponder];
    
    action_type = NSLocalizedStringFromTableInBundle(@"child_template12_button2_action_type",propertyFileName,[NSBundle mainBundle], nil);
    validation_Type = NSLocalizedStringFromTableInBundle(@"application_validation_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    
    data = NSLocalizedStringFromTableInBundle(@"child_template12_button2_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    processorCodeStr = [Template getProcessorCodeWithData:data];
    transactionType = [Template getTransactionCodeWithData:data];
    referenceParam = [Template getReferenceParameter:data];
    
    alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template12_button2_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template12_button2_next_template",propertyFileName,[NSBundle mainBundle], nil);
    paramType = nil;
    if (NSLocalizedStringFromTableInBundle(@"child_template12_button2_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil))
    {
        paramType = NSLocalizedStringFromTableInBundle(@"child_template12_button2_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil);
    }
    
    if (transactionType) {
        [localDictionary setObject:transactionType forKey:PARAMETER13];
    }
    if (referenceParam) {
        [localDictionary setObject:referenceParam forKey:PARAMETER38];
    }
    [localDictionary setObject:processorCodeStr forKey:PARAMETER15];
    
    datavalueDictionary  = [[NSMutableDictionary alloc] init];
    [datavalueDictionary addEntriesFromDictionary:localDictionary];
    currentClass = CHILD_TEMPLATE_12;
    buttonActionString = CT12_BUTTON2_ACTION;
    [self configureWithProcessorCode];
}


#pragma mark - Toolbar methods.
/**
 * This method is used to cancel button action of tool bar.
 */
-(void)cancelNumberPad
{
    [activeField resignFirstResponder];
    activeField.text = @"";
}

/**
 * This method is used to ok button action of tool bar.
 */
-(void)doneWithNumberPad
{
    if ([activeField isFirstResponder])
        [activeField resignFirstResponder];
}

#pragma mark - TextField Delegate Methods.
/**
 * This method is used to keybord show and hide method(resign,become responder).
 */
- (void)textFieldDidBeginEditing:(CustomTextField *)textField
{
    activeField = textField;
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-20);
    [childTemplate1ScrollView setContentOffset:scrollPoint animated:YES];
}

- (void)textFieldDidEndEditing:(CustomTextField *)textField
{
    [textField resignFirstResponder];
    [childTemplate1ScrollView setContentOffset:CGPointZero animated:YES];
}

- (BOOL)textFieldShouldBeginEditing:(CustomTextField *)textField
{
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    
    if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
    {
        textField.textAlignment=NSTextAlignmentRight;
    }
    else
    {
        textField.textAlignment=NSTextAlignmentLeft;
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(CustomTextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(CustomTextField *)textField
{
    return YES;
}
@end
