//
//  ChildTemplate9.m
//  Consumer Client
//
//  Created by android on 6/18/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "ChildTemplate9.h"
#import "Localization.h"
#import "Constants.h"
#import "PopUpTemplate2.h"
#import "ValidationsClass.h"
#import "NotificationConstants.h"
#import "WebServiceConstants.h"
#import "WebSericeUtils.h"
#import "WebServiceConstants.h"
#import "WebServiceRequestFormation.h"
#import "WebServiceRunning.h"
#import "WebServiceDataObject.h"
#import "Template.h"



@implementation ChildTemplate9

@synthesize headerTitle,controllerName,contentWebView;
@synthesize childButton1,childButton2,propertyFileName;

#pragma mark - ChildTemplate9 UIView.
/**
 * This method is used to set implemention of childTemplate9.
 */
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView
{
   NSLog(@"PropertyFileName:%@",propertyFile);
    self = [super initWithFrame:frame];
    if (self)
    {
        // Custom initialization
        propertyFileName=propertyFile;
        local_processorCode = processorCode;
        if ([dataDictsArray count]>0)
            local_dataArray = [[NSArray alloc] initWithArray:dataDictsArray];
        
        if ([dataDictionary count]>0)
            local_dataDictionary = [[NSMutableDictionary alloc] initWithDictionary:dataDictionary];
        
        /*
         * This method is used to add childtemplate9 frame UIConstraints.
         */
        [self addViewControls];
    }
    return self;
}

#pragma mark - ChildTemplate9 UI Constraints Creation.
/**
 * This method is used to set add UIconstraints Frame of ChildTemplate2.
 *@param Type- Label and Button
 * Set Text (Size,color and font size).
 */
-(void)addViewControls
{
    labelX_Position=10;
    distanceY=5.0;
    /*
     * This method is used to add childtemplate9 headertitle visibility.
     */
     // Label1
    if([NSLocalizedStringFromTableInBundle(@"child_template9_label1_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        headerTitle=[[UILabel alloc]init] ;
        headerTitle.frame = CGRectMake(labelX_Position, 4, SCREEN_WIDTH-40, 30);
        headerTitle.backgroundColor=[UIColor clearColor];
        headerTitle.lineBreakMode=NSLineBreakByTruncatingTail;
        /*
         * This method is used to add childtemplate9 headertitle titlename.
         */
        NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template9_label1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if(headerStr)
            headerTitle.text= headerStr;
        /*
         * This method is used to add childtemplate9 headertitle fontattributes override.
         */
        if ([NSLocalizedStringFromTableInBundle(@"child_template9_label1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            /*
             * This method is used to add childtemplate9 headertitle Properties for label Textcolor.
             */
            // Properties for label TextColor.
            
            NSArray *headerLabelTextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template9_label1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                headerLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template9_label1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                headerLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                headerLabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (headerLabelTextColor)
                headerTitle.textColor = [UIColor colorWithRed:[[headerLabelTextColor objectAtIndex:0] floatValue] green:[[headerLabelTextColor objectAtIndex:1] floatValue] blue:[[headerLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            /*
             * This method is used to add childtemplate9 headertitle Properties for label Textstyle.
             */
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template9_label1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template9_label1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            /*
             * This method is used to add childtemplate9 headertitle Properties for label Font size.
             */
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template9_label1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template9_label1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerTitle.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerTitle.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerTitle.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerTitle.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        else{
             /*
              * This method is used to add childtemplate9 headertitle Default Properties for label textcolor.
              */
            //Default Properties for label textcolor
            
            NSArray *headerLabelTextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                headerLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                headerLabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (headerLabelTextColor)
                headerTitle.textColor = [UIColor colorWithRed:[[headerLabelTextColor objectAtIndex:0] floatValue] green:[[headerLabelTextColor objectAtIndex:1] floatValue] blue:[[headerLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            /*
             * This method is used to add childtemplate9 headertitle Default Properties for label textStyle.
             */
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            /*
             * This method is used to add childtemplate9 headertitle Default Properties for label fontSize.
             */
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerTitle.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerTitle.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerTitle.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerTitle.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        headerTitle.textAlignment=NSTextAlignmentLeft;
        nextY_Position=headerTitle.frame.origin.y+headerTitle.frame.size.height;
        /*
         * This method is used to add childtemplate9 headertitle to view.
         */
        [self addSubview:headerTitle];
    }
    
    /*
     * This method is used to set childtemplate9 label2 visibility.
     */
    // Label2
    if([NSLocalizedStringFromTableInBundle(@"child_template9_label2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if(!contentWebView)
            contentWebView = [[UIWebView alloc]init];
        
        contentWebView.frame = CGRectMake(10, nextY_Position, SCREEN_WIDTH-20,  SCREEN_HEIGHT-170);
        contentWebView.delegate = self;
        contentWebView.scrollView.scrollEnabled = YES;
        [contentWebView setBackgroundColor:[UIColor clearColor]];
        contentWebView.opaque=NO;
        contentWebView.scalesPageToFit=YES;
        NSString *helpText=nil;
        NSError* error = nil;
        /*
         * This method is used to set childtemplate9 label2 content.
         */
        
        NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
        NSString *strvalue=[NSString stringWithFormat:@"%@_%@",[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template9_label2",propertyFileName,[NSBundle mainBundle], nil)],[[[NSUserDefaults standardUserDefaults] objectForKey:userLanguage]uppercaseString]];
        NSString *path = [[NSBundle mainBundle] pathForResource:strvalue ofType:@"strings"];
        if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
        {
            helpText = [NSString stringWithContentsOfFile:path encoding:NSUnicodeStringEncoding error: &error];
        }else{
            helpText = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error: &error];
        }
        NSString *loadHTMLStr=[NSString stringWithFormat:@"<div style='text-align:Justify; font-size:%@;font-family:Helvetica;color:#ff;'%@",[UIFont fontWithName:@"Helvetica" size:26],helpText];

        NSURL *url = [[NSBundle mainBundle]  URLForResource:strvalue withExtension:@"html"];
        [contentWebView loadRequest:[NSURLRequest requestWithURL:url]];

        
//        if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
//        {
//            NSURL *url = [[NSBundle mainBundle]  URLForResource:strvalue withExtension:@"html"];
//            [contentWebView loadRequest:[NSURLRequest requestWithURL:url]];
//        }else{
//            [contentWebView loadHTMLString:loadHTMLStr baseURL:nil];
//        }
        
       // [contentWebView stringByEvaluatingJavaScriptFromString:@"document.open();document.close()"];
        /*
         * This method is used to set childtemplate9 html content add toview.
         */
        [self addSubview:contentWebView];
    }
    
    nextY_Position=CGRectGetMaxY(contentWebView.frame) + 10;
    
    /*
     * This method is used to set childtemplate9 button1 visibility.
     */
    // Button1
    if([NSLocalizedStringFromTableInBundle(@"child_template9_button1_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
        
        childButton1=[UIButton buttonWithType:UIButtonTypeCustom];
        childButton1.frame = CGRectMake(10, nextY_Position, ((SCREEN_WIDTH-20)/2-5), 40);
        
        /*
         * This method is used to set childtemplate9 button1 titlename.
         */
        NSString *buttonTitle = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template9_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (buttonTitle)
              [childButton1 setTitle:buttonTitle forState:UIControlStateNormal];
        
        childButton1.titleLabel.lineBreakMode=NSLineBreakByTruncatingTail;
        
        /*
         * This method is used to set childtemplate9 button1 properties For Button backgroundColor.
         */
        // properties For Button backgroundColor
        
        NSArray *button1_BackgroundColor;
        if (NSLocalizedStringFromTableInBundle(@"child_template9_button1_background_color",propertyFileName,[NSBundle mainBundle], nil))
            button1_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template9_button1_background_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            button1_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button1_BackgroundColor)
            childButton1.backgroundColor=[UIColor colorWithRed:[[button1_BackgroundColor objectAtIndex:0] floatValue] green:[[button1_BackgroundColor objectAtIndex:1] floatValue] blue:[[button1_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        if([NSLocalizedStringFromTableInBundle(@"child_template9_button1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            /*
             * This method is used to set childtemplate9 button1 properties For Button textcolor.
             */
            // Properties for Button TextColor.
            
            NSArray *button1_TextColor;
            if (NSLocalizedStringFromTableInBundle(@"child_template9_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template9_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template9_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template9_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                button1_TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button1_TextColor)
                [childButton1 setTitleColor:[UIColor colorWithRed:[[button1_TextColor objectAtIndex:0] floatValue] green:[[button1_TextColor objectAtIndex:1] floatValue] blue:[[button1_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            /*
             * This method is used to set childtemplate9 button1 properties For Button textstyle.
             */
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template9_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template9_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template9_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template9_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            /*
             * This method is used to set childtemplate9 button1 properties For Button fontsize.
             */
            // Properties for Button Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template9_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template9_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template9_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template9_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                childButton1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                childButton1.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                childButton1.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                childButton1.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }else{

            /*
             * This method is used to set childtemplate9 button1 Default Properties for Button textcolor.
             */
            //Default Properties for Button textcolor
            
            NSArray *button1_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template9_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template9_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button1_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button1_TextColor)
                [childButton1 setTitleColor:[UIColor colorWithRed:[[button1_TextColor objectAtIndex:0] floatValue] green:[[button1_TextColor objectAtIndex:1] floatValue] blue:[[button1_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            /*
             * This method is used to set childtemplate9 button1 Default Properties for Button textstyle.
             */
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template9_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template9_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            /*
             * This method is used to set childtemplate9 button1 Default Properties for Button fontsize.
             */
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template9_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template9_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                childButton1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                childButton1.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                childButton1.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                childButton1.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [childButton1 setTag:900];
        [childButton1 setExclusiveTouch:YES];
        [childButton1 addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
        /*
         * This method is used to set childtemplate9 button1 add toview.
         */
        [self addSubview:childButton1];
    }
    else
    {
       contentWebView.frame = CGRectMake(10, headerTitle.frame.origin.y+headerTitle.frame.size.height, SCREEN_WIDTH-20, SCREEN_HEIGHT-10);
    }
    
    /*
     * This method is used to set childtemplate9 button2 visibility true.
     */
    // Button2
    if([NSLocalizedStringFromTableInBundle(@"child_template9_button2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        childButton2=[UIButton buttonWithType:UIButtonTypeCustom];
        childButton2.frame = CGRectMake(childButton1.frame.origin.x+childButton1.frame.size.width+10, nextY_Position, ((SCREEN_WIDTH-20)/2-5), 40);
        
        /*
         * This method is used to set childtemplate9 button2 titlename.
         */
        NSString *buttonTitle = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"child_template9_button2_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (buttonTitle)
            [childButton2 setTitle:buttonTitle forState:UIControlStateNormal];
        
        childButton2.titleLabel.lineBreakMode=NSLineBreakByTruncatingTail;
        
        /*
         * This method is used to set childtemplate9 button2 properties For Button backgroundColor.
         */
        // properties For Button backgroundColor
        
        NSArray *button2_BackgroundColor;
        
        if (NSLocalizedStringFromTableInBundle(@"child_template9_button2_background_color",propertyFileName,[NSBundle mainBundle], nil))
            button2_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template9_button2_background_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
            button2_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        
        if (button2_BackgroundColor)
            childButton2.backgroundColor=[UIColor colorWithRed:[[button2_BackgroundColor objectAtIndex:0] floatValue] green:[[button2_BackgroundColor objectAtIndex:1] floatValue] blue:[[button2_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        /*
         * This method is used to set childtemplate9 button2 fontattributes override.
         */
        if([NSLocalizedStringFromTableInBundle(@"child_template9_button2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            /*
             * This method is used to set childtemplate9 button2Properties for Button TextColor.
             */
            // Properties for Button TextColor.
            NSArray *button2_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"child_template9_button2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template9_button2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"child_template9_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template9_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                button2_TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button2_TextColor)
                [childButton2 setTitleColor:[UIColor colorWithRed:[[button2_TextColor objectAtIndex:0] floatValue] green:[[button2_TextColor objectAtIndex:1] floatValue] blue:[[button2_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            /*
             * This method is used to set childtemplate9 button2Properties for Button textstyle.
             */
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template9_button2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template9_button2_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template9_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template9_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            /*
             * This method is used to set childtemplate9 button2Properties for Button font size.
             */
            // Properties for Button Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template9_button2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template9_button2_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"child_template9_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template9_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                childButton2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                childButton2.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                childButton2.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                childButton2.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            /*
             * This method is used to set childtemplate9 button2 Default Properties for Button textcolor
             */
            //Default Properties for Button textcolor
            
            NSArray *button2_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"child_template9_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"child_template9_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button2_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button2_TextColor)
                [childButton2 setTitleColor:[UIColor colorWithRed:[[button2_TextColor objectAtIndex:0] floatValue] green:[[button2_TextColor objectAtIndex:1] floatValue] blue:[[button2_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            /*
             * This method is used to set childtemplate9 button2 Default Properties for Button textstyle.
             */
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"child_template9_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"child_template9_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            /*
             * This method is used to set childtemplate9 button2 Default Properties for Button fontsize.
             */
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"child_template9_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"child_template9_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                 childButton2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                 childButton2.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                 childButton2.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                childButton2.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        
        [childButton2 setTag:901];
        [childButton2 setExclusiveTouch:YES];
        [childButton2 addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
        /*
         * This method is used to set childtemplate9 button2 add toview.
         */
        [self addSubview:childButton2];
    }
    else
    {
        contentWebView.frame = CGRectMake(10, headerTitle.frame.origin.y+headerTitle.frame.size.height, SCREEN_WIDTH-20, SCREEN_HEIGHT-10);
    }
}


#pragma  mark - Button Action 
/**
 * This method is used to set button1 action of ChildTemplate9.
 */
-(void)button1Action:(id)sender
{
    /*
     * This method is used to set childtemplate9 button1 action.
     */
    UIViewController *vc = [(UINavigationController *)self.window.rootViewController visibleViewController];
    [vc.navigationController popViewControllerAnimated:NO];
}
/**
 * This method is used to set button2 action of ChildTemplate9.
 @prameters are defined in button action those are
 * Application display type(ticker,PopUp),validation type,next template,next template PropertyFile,Action type,
 Feature-(processorCode,transaction type).
 */
-(void)button2Action:(id)sender
{
    /*
     * This method is used to set alert display type(Ticker or popup).
     */
    alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    /*
     * This method is used to set childtemplate9 button2 nexttemplate properties file.
     */
    nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"child_template9_button2_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    
    /*
     * This method is used to set childtemplate9 button2 nexttemplate.
     */
    nextTemplate = NSLocalizedStringFromTableInBundle(@"child_template9_button2_next_template",propertyFileName,[NSBundle mainBundle], nil);
    /*
     * This method is used to set childtemplate9 button2 webservice api name.
     */
    NSString *data = NSLocalizedStringFromTableInBundle(@"child_template9_button2_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    local_processorCode = [Template getProcessorCodeWithData:data];
    NSString *transactionType = [Template getTransactionCodeWithData:data];
    /*
     * This method is used to set childtemplate9 button2 - Feature transactionType.
     */
    if (transactionType) {
        [local_dataDictionary setObject:transactionType forKey:PARAMETER13];
    }
    /*
     * This method is used to set childtemplate9 button2 - Feature processorCode.
     */
    [local_dataDictionary setObject:local_processorCode forKey:PARAMETER15];
    /*
     * This method is used to set childtemplate9 button2 action type.
     */
    NSString *actionType = NSLocalizedStringFromTableInBundle(@"child_template9_button2_action_type",propertyFileName,[NSBundle mainBundle], nil);
    Template *obj = [Template initWithactionType:actionType nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:propertyFileName ProcessorCode:local_processorCode Dictionary:local_dataDictionary contentArray:(NSMutableArray *)local_dataArray alertType:alertview_Type ValidationType:nil   inClass:self withApiParameter:nil withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:nil withCurrentClass:CHILD_TEMPLATE_9];
    obj.selector = @selector(processData);
    obj.target = self;
    [[NSNotificationCenter defaultCenter] postNotificationName:CT9_BUTTON2_ACTION object:obj];
}

#pragma mark - BaseView Process Data.
/**
 * This method is used to set baseviewcontroller Process data notification.
 */
-(void) processData
{
    //Process Data
}

#pragma mark - WebView Delegate.
/*
 * This method is used to set Webview delegate method.
 */
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    // Optional method If u want Use Based On requirement.
}

@end
