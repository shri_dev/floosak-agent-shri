//
//  OptionsMenu.m
//  Consumer Client
//
//  Created by jagadeeshk on 15/05/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "OptionsMenu.h"
#import "Constants.h"
#import "ValidationsClass.h"
#import "Localization.h"

@implementation OptionsMenu
{
    NSString *loggedIn;
}
@synthesize optionsMenuItemsArray,optionsTableView,optionNameLabel,propertyFileName;
@synthesize optionsMenuArrayString,strName,status_Label,status_TimeLabel;


#pragma mark - optionsMenu Frame Initialization.
/**
 * This method is used to set implemention of option Menu.
 */
- (id)initWithFrame:(CGRect)frame withPropertyFileName:(NSString *)propertyFile withIndex:(int)tag
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setBackgroundColor:[UIColor clearColor]];
        self.backgroundView=[[UIView alloc]init];
        self.backgroundView.frame=CGRectMake(0, 0, SCREEN_WIDTH-80, SCREEN_HEIGHT);
        self.backgroundView.backgroundColor=[UIColor whiteColor];
        [self addSubview:self.backgroundView];
        optionsTableView =[[UITableView alloc] init];
        propertyFileName=@"OptionsMenuFeature_T1";
        NSLog(@"PropertyFileName:%@",propertyFileName);
        UITapGestureRecognizer *letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightLetter:)];
        letterTapRecognizer.numberOfTapsRequired = 1;
        letterTapRecognizer.delegate = self;
        [self addGestureRecognizer:letterTapRecognizer];
        
        UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeft:)];
        [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
        swipeLeft.delegate=self;
        [self addGestureRecognizer:swipeLeft];
        [self addViewControls];
    }
    return self;
}

#pragma mark - Add Uiconstraints for Options menu.
/**
 * This method is used to set add UIconstraints Frame of Option menu.
 */
-(void)addViewControls
{
    
    // Get Login State 0- User Not logged into app. 1- User logged in.

    NSLog(@"adding options menu..");
    loggedIn = [[NSUserDefaults standardUserDefaults] objectForKey:@"userLogged"];
    
    nextY_Position = 0;
    
    // frame initialization for view.
    self.labelView = [[UIView alloc]init];
    self.labelView.frame = CGRectMake(5, 2, self.backgroundView.frame.size.width-10, self.backgroundView.frame.size.height);
    
    // set Background color for Options Menu.
    
    NSArray *label_BgColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"optionsmenu_template_header_background_color",propertyFileName,[NSBundle mainBundle], nil)];
    
    self.labelView.backgroundColor=[UIColor colorWithRed:[[label_BgColor objectAtIndex:0] floatValue] green:[[label_BgColor objectAtIndex:1] floatValue] blue:[[label_BgColor objectAtIndex:2] floatValue] alpha:1.0f];
    [self addSubview:self.labelView];
    
    if ([loggedIn integerValue] == 1)
    {
        // Header label If user Logged in State.
        
        if([NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            status_Label=[[UILabel alloc]init];
            status_Label.layer.zPosition = 1;
            status_Label.frame=CGRectMake(self.labelView.frame.origin.x+2, self.labelView.frame.origin.y+2, (self.labelView.frame.size.width)/2, 20);
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label1_text",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                status_Label.text=labelStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label1_TextColor;
                if (NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label1_TextColor)
                    status_Label.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label1_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label1_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label1_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    status_Label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    status_Label.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    status_Label.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    status_Label.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label1_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label1_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label1_TextColor)
                    status_Label.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    status_Label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    status_Label.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    status_Label.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    status_Label.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }

            
            UIView *view = [[UIView alloc] initWithFrame:(CGRectMake(self.labelView.frame.origin.x, self.labelView.frame.origin.y, self.labelView.frame.size.width, self.labelView.frame.size.height/3))];
            
            UIGraphicsBeginImageContext(view.frame.size);
            [[UIImage imageNamed:@"head.png"] drawInRect:view.bounds];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            view.backgroundColor = [UIColor colorWithPatternImage:image];
            nextY_Position=view.frame.size.height+view.frame.origin.y;
            [self addSubview:view];

            status_Label.frame=CGRectMake(self.labelView.frame.origin.x+2, nextY_Position, (self.labelView.frame.size.width), 20);
            nextY_Position=status_Label.frame.size.height+status_Label.frame.origin.y;
            [self addSubview:status_Label];
        }
        // Header label for user Last login State.
        if([NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            status_TimeLabel=[[UILabel alloc]init];
            status_TimeLabel.layer.zPosition = 1;
            status_TimeLabel.frame=CGRectMake(self.labelView.frame.origin.x+2, nextY_Position, (self.labelView.frame.size.width)-4, 24);
            
            NSString *timeStr=[[[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"] objectForKey:@"TransactionDate"];
            
            NSString *finalTimeStr = [ValidationsClass convertTorequiredFormatte:timeStr withFormate:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"application_date_and_time_format",@"GeneralSettings",[NSBundle mainBundle], nil)]];
            
            if (finalTimeStr == (id)[NSNull null] || finalTimeStr.length == 0)
                status_TimeLabel.text=@"";
            else
                status_TimeLabel.text=[NSString stringWithFormat:@"%@",finalTimeStr];
            
            
            if ([NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                NSArray *label1_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label1_TextColor)
                    status_TimeLabel.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                status_TimeLabel.textColor = [UIColor blackColor];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label2_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label2_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label2_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    status_TimeLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    status_TimeLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    status_TimeLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    status_TimeLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label1_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label1_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label1_TextColor)
                    status_TimeLabel.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    status_TimeLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    status_TimeLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    status_TimeLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    status_TimeLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            status_TimeLabel.numberOfLines=0;
            [status_TimeLabel sizeToFit];
            [self addSubview:status_TimeLabel];
            
            nextY_Position=status_TimeLabel.frame.origin.y+status_TimeLabel.frame.size.height;
        }
    }
    
    
    // To load Option menu List items based on user login state.
    
    if ([loggedIn integerValue] == 1){
        optionsMenuArrayString = NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_after_login_item_text",propertyFileName,[NSBundle mainBundle], nil);
    }
    
  else if ([loggedIn integerValue] == 0) {
        int userLevel = (int)[NSLocalizedStringFromTableInBundle(@"application_default_user_support_mode",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
        
        if (userLevel == 0) {
            optionsMenuArrayString=NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_single_user_item_text",propertyFileName,[NSBundle mainBundle], nil);
        }
        else {
            optionsMenuArrayString=NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_item_text",propertyFileName,[NSBundle mainBundle], nil);
        }
    }
    // option menu list array.
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"SelectListData"]count]>0  && ([loggedIn integerValue] == 1))
        optionsMenuItemsArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"SelectListData"]];
    
    else
        optionsMenuItemsArray = [[NSMutableArray alloc] initWithArray:[optionsMenuArrayString componentsSeparatedByString:@","]];
    
    if ([optionsMenuItemsArray containsObject:@"label_id_listview"]) {
        [optionsMenuItemsArray removeObject:@"label_id_listview"];
    }
    if ([optionsMenuItemsArray containsObject:@"label_id_gridview"]) {
        [optionsMenuItemsArray removeObject:@"label_id_gridview"];
    }

    // Options menu Table View frame Initialization.
    
    NSLog(@"Options Menu Array...%@",optionsMenuItemsArray);
    
//    optionsTableView.frame=CGRectMake(0, nextY_Position,SCREEN_WIDTH-80, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS-status_Label.frame.size.height);

    UIView *view = [[UIView alloc] initWithFrame:(CGRectMake(self.labelView.frame.origin.x, self.labelView.frame.origin.y, self.labelView.frame.size.width, self.labelView.frame.size.height/3))];
    UIGraphicsBeginImageContext(view.frame.size);
    [[UIImage imageNamed:@"head.png"] drawInRect:view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    view.backgroundColor = [UIColor colorWithPatternImage:image];
    nextY_Position=view.frame.size.height+view.frame.origin.y;
    [self addSubview:view];
    if([loggedIn integerValue] ==1)
        optionsTableView.frame=CGRectMake(0, nextY_Position+40,SCREEN_WIDTH-80, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS-status_Label.frame.size.height);
    else
        optionsTableView.frame=CGRectMake(0, nextY_Position,SCREEN_WIDTH-80, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS-status_Label.frame.size.height);
    
    optionsTableView.dataSource = self;
    optionsTableView.delegate = self;
    self.optionsTableView.bounces = YES;
    optionsTableView.contentInset = UIEdgeInsetsMake(0, 0, 64, 0);
    [self addSubview:optionsTableView];
    
    [optionsTableView reloadData];
}

#pragma mark -  TAP GESTURE DELEGATE METHOD
/**
 * This method is used to set Custom delegate For Removing Option Menu from view.
 */
- (void)highlightLetter:(UITapGestureRecognizer*)sender
{
    if (self.delegate != nil)
    {
        [self.delegate optionsMenu_tapGestureCalled];
    }
}
- (void)handleSwipeLeft:(UITapGestureRecognizer*)sender
{
    if (self.delegate != nil)
    {
        [self.delegate optionsMenu_tapGestureCalled];
    }
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:optionsTableView])
    {
        return NO;
    }
    return YES;
}


#pragma  mark - TABLEVIEW DATASOURCE AND DELEGATE Methods
/**
 * This method is used to set Tabelview (list and Delegate methods) Of Option Menu.
 *@param - Features Provided (Befor Login and AfterLogin)
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return optionsMenuItemsArray.count;
}
/**
 * This method is used to set add Tableview for OptionsMenu.
 *@param type- Declare Listview related UI(Labels,Images etc..base on Req).
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = [NSString stringWithFormat:@"%@%d",@"Cell",(int)indexPath.row];
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        //Cell Label
        if([NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_item_text_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            optionNameLabel = [[UILabel alloc] init];
            [optionNameLabel setFrame:CGRectMake(36, 0, CGRectGetWidth(tableView.frame)-40,40)];
            [optionNameLabel setBackgroundColor:[UIColor clearColor]];
            [optionNameLabel setTextAlignment:NSTextAlignmentLeft];
            optionNameLabel.tag = 100;
            optionNameLabel.text = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:[optionsMenuItemsArray objectAtIndex:indexPath.row]]];

            UIView *view = [[UIView alloc] initWithFrame:(CGRectMake(6, 10, 18, 18))];
            UIGraphicsBeginImageContext(view.frame.size);
            if(([loggedIn integerValue] == 1)){
                if(indexPath.row == 0)
                    [[UIImage imageNamed:@"6.png"] drawInRect:view.bounds];
                if(indexPath.row == 1)
                    [[UIImage imageNamed:@"7.png"] drawInRect:view.bounds];
                if(indexPath.row == 2)
                    [[UIImage imageNamed:@"1.png"] drawInRect:view.bounds];
                if(indexPath.row == 3)
                    [[UIImage imageNamed:@"2.png"] drawInRect:view.bounds];
                if(indexPath.row == 4)
                    [[UIImage imageNamed:@"3.png"] drawInRect:view.bounds];
                if(indexPath.row == 5)
                    [[UIImage imageNamed:@"8.png"] drawInRect:view.bounds];
            }else{
                if(indexPath.row == 0)
                    [[UIImage imageNamed:@"1.png"] drawInRect:view.bounds];
                if(indexPath.row == 1)
                    [[UIImage imageNamed:@"2.png"] drawInRect:view.bounds];
                if(indexPath.row == 2)
                    [[UIImage imageNamed:@"3.png"] drawInRect:view.bounds];
            }
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            view.backgroundColor = [UIColor colorWithPatternImage:image];
            [cell addSubview:view];
            
            [optionNameLabel setTextAlignment:NSTextAlignmentLeft];
            if ([NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_item_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                
                NSArray *label1_TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_item_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_item_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (label1_TextColor)
                    optionNameLabel.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_item_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_item_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    optionNameLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    optionNameLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    optionNameLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    optionNameLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                
                NSArray *label1_TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    label1_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                if (label1_TextColor)
                    optionNameLabel.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_fontStyle",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"optionmenu_template_header_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    optionNameLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    optionNameLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    optionNameLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    optionNameLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [self addSubview:optionNameLabel];
        }
        
        [cell.contentView addSubview:optionNameLabel];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}
/**
 * This method is used to set add Tableview Delegate Method User selects List .
 *@param type- Declare Listview - (NextTemplate and Next template proprty file).
 *To show List Item Detailed Data etc..
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedRow = indexPath.row;
    nameSelected = [optionsMenuItemsArray objectAtIndex:indexPath.row];
//    NSLog(@"Selected Feature Menu Type ...%@",nameSelected);
    
    if ([nameSelected isEqualToString:@"label_id_gridview"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"Grid" forKey:@"HomePageType"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if ([optionsMenuItemsArray containsObject:@"label_id_gridview"]) {
//            [optionsMenuItemsArray removeObject:@"label_id_gridview"];
//            [optionsMenuItemsArray insertObject:@"label_id_listview" atIndex:selectedRow];
        }
    }
    else if ([nameSelected isEqualToString:@"label_id_listview"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"List" forKey:@"HomePageType"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if ([optionsMenuItemsArray containsObject:@"label_id_listview"]) {
//            [optionsMenuItemsArray removeObject:@"label_id_listview"];
//            [optionsMenuItemsArray insertObject:@"label_id_gridview" atIndex:selectedRow];
        }
    }
    if (([loggedIn integerValue] == 1)) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:optionsMenuItemsArray forKey:@"SelectListData"];
        [userDefaults synchronize];
    }
    
    NSString *templateNamesString;
    NSString *templatePropertyNamesString;
    
    if ([loggedIn integerValue] == 1) {
        templateNamesString = NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_after_login_next_template",propertyFileName,[NSBundle mainBundle], nil);
        templatePropertyNamesString = NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_after_login_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    }
    
    if ([loggedIn integerValue] == 0) {
        
        int userLevel = (int)[NSLocalizedStringFromTableInBundle(@"application_default_user_support_mode",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
        
        if (userLevel == 0) {
            templateNamesString = NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_single_user_next_template",propertyFileName,[NSBundle mainBundle], nil);
            templatePropertyNamesString = NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_single_user_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
        }
        else {
            templateNamesString = NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
            templatePropertyNamesString = NSLocalizedStringFromTableInBundle(@"optionmenu_template_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
        }
    }
    
    NSArray *templateNamesArray = [[NSArray alloc] initWithArray:[templateNamesString componentsSeparatedByString:@","]];
    NSArray *templatePropertyNamesArray = [[NSArray alloc] initWithArray:[templatePropertyNamesString componentsSeparatedByString:@","]];
    
    if (self.delegate != nil)
    {
        [self.delegate optionsMenu_didSelectRowAtIndexPath:indexPath.row withPropertyFile:[templatePropertyNamesArray objectAtIndex:indexPath.row] nextTemplate:[templateNamesArray objectAtIndex:indexPath.row] andProcessorCode:nil];
    }
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
@end
