//
//  OptionsMenu.h
//  Consumer Client
//
//  Created by jagadeeshk on 15/05/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * This method is  used for Custom delegate Method for Option Menu.
 */
@protocol OptionsMenuDelegate <NSObject>

// OptionsMenu Required methods Declaration.
@required
// Options Menu Selected List item .
-(void)optionsMenu_didSelectRowAtIndexPath:(NSInteger)indexpath withPropertyFile:(NSString *)propertyFileName nextTemplate:(NSString *)nextTemplateName andProcessorCode:(NSString *)processorCode;
// options Menu View remove from View.
-(void)optionsMenu_tapGestureCalled;
@end

/**
 * This class used to handle functionality and view of Option Menu.
 *
 * @author Integra
 *
 */
// Options Menu View.
@interface OptionsMenu : UIView<UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate>
{
    int nextY_Position;
    
    NSInteger selectedRow;
    NSString *nameSelected;
    
}

/**
 * declarations are used to set the UIConstraints Of Popup template1.
 * @params are - Optionmenu list.
 * Label and Tableview.
 */
// optionsmenu constarints.
@property (nonatomic, assign) NSObject <OptionsMenuDelegate> *delegate;
@property (strong,nonatomic) NSString *propertyFileName;
@property (strong,nonatomic) NSString *strName;
@property (strong,nonatomic) NSString *optionsMenuArrayString;
@property (strong,nonatomic) UILabel *status_Label;
@property (strong,nonatomic) UILabel *status_TimeLabel;
@property (strong,nonatomic) UIView *backgroundView;
@property (strong,nonatomic) UIView *labelView;
@property (nonatomic, strong) NSMutableArray *optionsMenuItemsArray;
@property (nonatomic,strong)  UILabel *optionNameLabel;
@property (nonatomic, strong) UITableView *optionsTableView;

/**
 * This method is used to add Option Menu View Method For initialization..
 */
 // Method For initialization.
- (id)initWithFrame:(CGRect)frame  withPropertyFileName:(NSString *)propertyFile withIndex:(int)tag;

@end
