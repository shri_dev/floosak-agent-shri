//
//  ParentTemplate8.h
//  Consumer Client
//
//  Created by android on 9/1/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

/**
 * This class used to handle functionality and ViewController of ParentTemplate8
 *
 * @author Integra
 *
 */
@interface ParentTemplate8 : BaseViewController
{
    NSString *propertyFileName;
    int labelX_Position;
    int distanceY;
    int nextY_Position;
}
/**
 * declarations are used to set the UIConstraints Of Popup template1.
 *Label,Image and Buttons.
 */
@property(strong,nonatomic) NSString *proprtyFileName;
@property(strong,nonatomic) UILabel *headerTitleLabel;
@property(strong,nonatomic) UILabel *messageLabel;
@property(strong,nonatomic) UIWebView *contentWebView;
@property(strong,nonatomic) UIImageView *activationLogoImage_Icon;
@property(strong,nonatomic) UILabel *documentUploadLabel;


@property (weak, nonatomic) IBOutlet UIButton *uploadKYCButton;
@property (weak, nonatomic) IBOutlet UIButton *pushToHomeButton;
@property (weak, nonatomic) IBOutlet UILabel *uploadKYCMessage;
@property (weak, nonatomic) IBOutlet UILabel *uploadKYCMessage2;

- (IBAction)push2Home:(id)sender;
- (IBAction)uploadKYCDocuments:(id)sender;


/**
 * This method is  used for Method Initialization of ParentTemplate8.
 */
// Method for Initialization
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view
         withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode
            dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary;
@end
