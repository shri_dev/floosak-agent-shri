//
//  ParentTemplate2.h
//  Consumer Client
//
//  Created by Integra Micro on 17/04/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "PageHeaderView.h"
#import "FeaturesMenu.h"
#import "PopUpTemplate1.h"
/**
 * This class used to handle functionality and ViewController of ParentTemplate6
 *
 * @author Integra
 *
 */
// Class Parent template2.
@interface ParentTemplate2 : BaseViewController<PageHeaderViewButtonDelegate,FeaturesMenuDelegate>
{
    int selectedSideMenuButtonIndex;
    NSArray *propertiesArray;
    NSString *propertyFileName;    
    int from;
}
/**
 * declarations are used to set the UIConstraints Of ParentTemplate6.
 */
@property (assign)NSInteger selected_index;
@property (strong, nonatomic) IBOutlet UIView *parentSubView;

/**
 * This method is  used for Method Initialization of ParentTemplate2.
 */
// Method For initialization.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view  withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode  dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary;

@end
