//
//  ParentTemplate1.h
//  Consumer Client
//
//  Created by Integra Micro on 17/04/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PageHeaderView.h"
#import "FeaturesMenu.h"
#import "ActivityIndicator.h"
#import "ChildTemplate2.h"
#import "ChildTemplate4.h"
#import "BaseViewController.h"
#import "PopUpTemplate1.h"

// Class Parent template1
/**
 * This class used to handle functionality and viewController of  ParentTemplate1
 *
 * @author Integra
 *
 */
@interface ParentTemplate1 : BaseViewController<PageHeaderViewButtonDelegate,FeaturesMenuDelegate>
{
    NSArray *propertiesArray;
    NSArray *buttonTextColor;
    NSArray *selectedButtonTextColor;
    NSArray *selectionLabelBackGroundColor;
    
    NSArray *templatesArray;
    NSArray *templateTitleArray;
    NSArray *templatePropertyFilesArray;
    
    int tabsCount;
    int selectedButtonIndex,selectedSideMenuButtonIndex;
}

/**
 * declarations are used to set the UIConstraints Of Parenttemplate1.
 *Label,Value label,Border label and Buttons.
 */
@property (assign)NSInteger selected_index;
@property (retain, nonatomic) UIButton *buttonOne;
@property (retain, nonatomic) UILabel *buttonOneSelectedlabel;
@property (retain, nonatomic) UIButton *buttonTwo;
@property (retain, nonatomic) UILabel *buttonTwoSelectedlabel;
@property (retain, nonatomic) UIButton *buttonThree;
@property (retain, nonatomic) UILabel *buttonThreeSelectedlabel;
@property (retain, nonatomic) UILabel *seperatorLabel;
@property (nonatomic,strong) NSArray *buttonAction_Arr;

/*
 * This method is used to set Parenttemplate1 Method For initialization.
 */
// Method For initialization.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary;

@end

















