//  ParentTemplate5.m
//  Consumer Client
//
//  Created by PrasadSD on 07/05/15.
//  Copyright (c) 2015 Soumya. All rights reserved.


#import <Foundation/Foundation.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import "AppDelegate.h"
#import "Localization.h"
#import "ParentTemplate5.h"
#import "ValidationsClass.h"
#import "UIView+Toast.h"
#import "Constants.h"
#import "ParentTemplate1.h"
#import "ParentTemplate3.h"
#import "ParentTemplate4.h"
#import "ParentTemplate7.h"
#import "WebSericeUtils.h"
#import "WebServiceConstants.h"
#import "WebServiceRequestFormation.h"
#import "WebServiceRunning.h"
#import "WebServiceDataObject.h"
#import "UIImage+RTL.h"
#include "NotificationConstants.h"
#import "DatabaseConstatants.h"
#import "Template.h"
//#import "Log.h"

#define kRecentViewRecentButtonTag 110
#define kRecentViewFloosakButtonTag 111

@interface ParentTemplate5 ()
@end

@implementation ParentTemplate5

@synthesize parentSubView;
@synthesize userDeatils,actionSheet;
@synthesize refreshSubView,refresh_updatedLabel,refresh_refreshButton,recentSeperatorLabel3;

@synthesize balanceSubView,balance_userNameLabel,balance_seperatorLabel,alertLabel,balance_userImageViewButton,balance_userImageView,balance_balanceLabel,balance_currencyLabel,balance_valueLabel,balance_updatedLabel,balance_refreshButton,balance_seperatorLabel2;

@synthesize recentSubView,recentTableView,recent_titleLabel,recenttitle_borderLabel,recent_valueLabel,recent_currencyLabel,recent_descriptionLabel,recent_updatedLabel,recent_refreshButton,recent_seperatorLabel,recentValueLabel1,recentValueLabel2,recentValueLabel3;

@synthesize recent_dataArray,recent_descriptionArray;

#pragma mark - ParentTemplate5 UIViewController.
/**
 * This method is used to set implemention of ParentTemplate5.
 */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode
            dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary
{
    
     NSLog(@"PropertyFileName:%@",propertyFileArray);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        propertyFile = propertyFileArray;
    }
    return self;
}

#pragma mark - ViewController life Cycle.
-(void)viewDidLoad
{
//  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateRecentTransactions:) name:@"UpdateRecentTransactions" object:nil];
    
    currentImplementationClass = PARENT_TEMPLATE_1;
    
    // ParentTemplate Background Color.
    NSArray *viewBackgroundColor;
    if (![NSLocalizedStringFromTableInBundle(@"parent_template5_back_ground_color",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template5_back_ground_color"] && NSLocalizedStringFromTableInBundle(@"parent_template5_back_ground_color",propertyFile,[NSBundle mainBundle], nil).length != 0)
    {
        viewBackgroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_back_ground_color",propertyFile,[NSBundle mainBundle], nil)];
    }else{
        viewBackgroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_default_backgorund_color",@"GeneralSettings",[NSBundle mainBundle], nil)];
    }
    self.view.backgroundColor=[UIColor colorWithRed:[[viewBackgroundColor objectAtIndex:0] floatValue] green:[[viewBackgroundColor objectAtIndex:1] floatValue] blue:[[viewBackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"HomePageType"]length]>0) {
        NSString *str=[NSString stringWithFormat:@"%@ view",[[NSUserDefaults standardUserDefaults] objectForKey:@"HomePageType"]];
        [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"DisplayType"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else{
        NSString *homepageView=[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"application_home_page_display_type",@"GeneralSettings",[NSBundle mainBundle], nil)];
        [[NSUserDefaults standardUserDefaults] setObject:homepageView forKey:@"DisplayType"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

#pragma  mark - ParentTemplate5 UIContraints Creation.
/**
 * This method is used to set add UIconstraints Frame of ParentTemplate5 .
 */
-(void)addControls
{
    fromScreen = 0;
    alertLabel=[[UILabel alloc]init];
    
    databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
    
    parentSubView = [[UIView alloc] init];
    
    [self addPageHeaderToView];
    [self addBalanceView];
    [self addRecentView];
    [self addGridViewToHomePage];

    /* As per new design GridView should be shown alway so commenting following logic
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"DisplayType"] isEqualToString:@"Grid view"]){
        
        [self addGridViewToHomePage];
        gridcollectionView.hidden=NO;
        [featuresMenu setHidden:YES];
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"DisplayType"] isEqualToString:@"List view"]){
        
        [self addFeaturemenuToView];
        [featuresMenu setHidden:NO];
        gridcollectionView.hidden=YES;
    }*/
    
    
    /* Have reformatted the below conditions if everyting works well following can be deleted
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"HomePageType"]length]>0)
    {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"DisplayType"] isEqualToString:@"Grid view"]) {
            [self addBalanceView];
            [self addGridViewToHomePage];
            [featuresMenu setHidden:YES];
            gridcollectionView.hidden=NO;
        }
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"DisplayType"] isEqualToString:@"Grid view"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"HomePageType"]length]==0){
            [self addBalanceView];
            [self addGridViewToHomePage];
            [featuresMenu setHidden:YES];
            gridcollectionView.hidden=NO;
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"DisplayType"] isEqualToString:@"List view"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"HomePageType"]length]==0){
        [self addBalanceView];
        gridcollectionView.hidden=YES;
        [featuresMenu setHidden:NO];
    }

    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"HomePageType"]length]>0)
    {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"DisplayType"] isEqualToString:@"List view"])
        {
            [self addRecentView];
            gridcollectionView.hidden=YES;
            [featuresMenu setHidden:NO];
        }
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"DisplayType"] isEqualToString:@"List view"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"HomePageType"]length]==0){
        [self addRecentView];
        gridcollectionView.hidden=YES;
        [featuresMenu setHidden:NO];
    }

    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"HomePageType"]length]>0)
    {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"DisplayType"] isEqualToString:@"List view"])
        {
            [self addFeaturemenuToView];
            [featuresMenu setHidden:NO];
        }
    }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"DisplayType"] isEqualToString:@"List view"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"HomePageType"]length]==0){
        [self addFeaturemenuToView];
        [featuresMenu setHidden:NO];
    }*/
    
    bgLbl = [[UILabel alloc] init];
    bgLbl.frame=CGRectMake(self.view.frame.origin.x, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH, SCREEN_HEIGHT);
    [bgLbl setBackgroundColor:[UIColor blackColor]];
    bgLbl.alpha=0.0;
    [self.view addSubview:bgLbl];
    //Approach type 1- Button refresh for Both(Balance and recent). 2-Button refresh for balace and recent
    if ([NSLocalizedStringFromTableInBundle(@"parent_template5_approach_type", propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"1"])
    {
        refreshSubView = [[UIView alloc] init];
        refreshSubView.frame = CGRectMake(pageHeader.frame.origin.x+5,0, parentSubView.frame.size.width, 40);
        refreshSubView.backgroundColor = [UIColor clearColor];
        
        NSArray *recentBackGroundColor;
        if(![NSLocalizedStringFromTableInBundle(@"parent_template5_unique_last_updated_layout_back_ground_color",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template5_unique_last_updated_layout_back_ground_color"] && NSLocalizedStringFromTableInBundle(@"parent_template5_unique_last_updated_layout_back_ground_color",propertyFile,[NSBundle mainBundle], nil).length != 0)
            recentBackGroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_unique_last_updated_layout_back_ground_color",propertyFile,[NSBundle mainBundle], nil)];
        
        if (recentBackGroundColor)
            refreshSubView.backgroundColor = [UIColor colorWithRed:[[recentBackGroundColor objectAtIndex:0] floatValue] green:[[recentBackGroundColor objectAtIndex:1] floatValue] blue:[[recentBackGroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        [parentSubView addSubview:refreshSubView];
        
        refresh_updatedLabel = [[UILabel alloc] init];
        refresh_updatedLabel.frame = CGRectMake(0,0,refreshSubView.frame.size.width-30,40);
        refresh_updatedLabel.backgroundColor = [UIColor clearColor];
        refresh_updatedLabel.textColor = [UIColor grayColor];
        
        NSString *updateLabel = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template5_balance_unique_last_updated_text",propertyFile,[NSBundle mainBundle], nil)];
        refresh_updatedLabel.text = [NSString stringWithFormat:@"LAST UPDATED %@ MINS AGO",updateLabel];
        NSArray *labelColorArr;
        
        
        if (![NSLocalizedStringFromTableInBundle(@"parent_template5_balance_unique_last_updated_text_color",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template5_balance_unique_last_updated_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template5_balance_unique_last_updated_text_color",propertyFile,[NSBundle mainBundle], nil).length != 0)
            labelColorArr=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_balance_unique_last_updated_text_color",propertyFile,[NSBundle mainBundle], nil)];
        
        if (labelColorArr)
            refresh_updatedLabel.textColor=[UIColor colorWithRed:[[labelColorArr objectAtIndex:0] floatValue] green:[[labelColorArr objectAtIndex:1] floatValue] blue:[[labelColorArr objectAtIndex:2] floatValue] alpha:1.0f];
        
        
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template5_balance_unique_last_updated_text_style",propertyFile,[NSBundle mainBundle], nil)  isEqualToString:@"1"])
        {
            refresh_updatedLabel.font = [UIFont boldSystemFontOfSize:[NSLocalizedStringFromTableInBundle(@"parent_template5_balance_unique_last_updated_text_size",propertyFile,[NSBundle mainBundle], nil) floatValue]];
        }
        else
        {
            refresh_updatedLabel.font = [UIFont systemFontOfSize:[NSLocalizedStringFromTableInBundle(@"parent_template5_balance_unique_last_updated_text_size",propertyFile,[NSBundle mainBundle], nil) floatValue]];
        }
        refresh_updatedLabel.textAlignment = NSTextAlignmentLeft;
        
        [refreshSubView addSubview:refresh_updatedLabel];
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template5_balance_refresh_image_visibility",propertyFile,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT]==NSOrderedSame)
        {
            refresh_refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
            refresh_refreshButton.frame = CGRectMake(refresh_updatedLabel.frame.origin.x+refresh_updatedLabel.frame.size.width+5,7.5,25,25);
            [refresh_refreshButton setTag:101];
            [refresh_refreshButton setExclusiveTouch:YES];
            [refresh_refreshButton addTarget:self action:@selector(refreshBalance:) forControlEvents:UIControlEventTouchUpInside];
            
            UIImage *refreshBtnImage = [UIImage imageNamed:NSLocalizedStringFromTableInBundle(@"parent_template5_balance_refresh_image",propertyFile,[NSBundle mainBundle], nil) inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil];
            [refresh_refreshButton setImage:refreshBtnImage forState:UIControlStateNormal];
            [refresh_refreshButton setImage:refreshBtnImage forState:UIControlStateSelected];
            [refreshSubView addSubview:refresh_refreshButton];
        }
        
        
    }
    
    
    NSArray *finalTranArray = [[NSArray alloc] initWithArray:[databaseManager getAllLoginInformation]];
    
    if (finalTranArray.count != 0) {
        NSDictionary *myDictionary = [finalTranArray objectAtIndex:0];
        NSString *strImagePath = [myDictionary valueForKey:LOGIN_USER_IMAGE_PATH];
        if(strImagePath.length > 0)
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            userImagePath = [documentsDirectory stringByAppendingPathComponent:strImagePath];
        }
    }
    
    
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"HomePageType"]length]>0)
//    {
//        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"DisplayType"] isEqualToString:@"List view"])
//        {
//            [self addBalanceView];
//            gridcollectionView.hidden=YES;
//            [featuresMenu setHidden:NO];
//        }
//    }
//    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"DisplayType"] isEqualToString:@"List view"] && [[[NSUserDefaults standardUserDefaults] objectForKey:@"HomePageType"]length]==0){
//        [self addBalanceView];
//        gridcollectionView.hidden=YES;
//        [featuresMenu setHidden:NO];
//    }
   
    
    [self loadOptionsMenu];
    activityIndicator = [[ActivityIndicator alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self.view addSubview:activityIndicator];
    activityIndicator.hidden = YES;
}

-(void)setRecentBalanceValue{
    if([balance_valueLabel.text isEqualToString:@"0"] ||
       [balance_valueLabel.text isEqualToString:@""] ||
       [balance_valueLabel.text isEqualToString:@"0.00"]){
        NSString *temp = [[NSUserDefaults standardUserDefaults] objectForKey:@"latest_balance"];
        if(temp)
            balance_valueLabel.text = temp;
    }
    balance_valueLabel.text = [balance_valueLabel.text stringByReplacingOccurrencesOfString:@".00" withString:@""];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];

    BOOL isTransformed = [[NSUserDefaults standardUserDefaults] boolForKey:@"isScreenTransformed"];
    if([propertyFile isEqualToString:@"LaunchPagePT5_T1"]
       && isTransformed){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TransformScreenAr" object:self userInfo:nil];
    }
    
    [self setRecentBalanceValue];
    if(!userImagePath){
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        userImagePath = [documentsDirectory stringByAppendingPathComponent:@"test.png" ];
        
    }
    NSLog(@"viewwillappear userImagePath %@", userImagePath);

    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    
    if(!isRefreshedOnce){
        isRefreshedOnce = YES;
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self performSelector:@selector(refreshBalance:) withObject:balance_refreshButton afterDelay:2.0];
//            [self performSelector:@selector(refreshRecentTransactions:) withObject:recent_refreshButton];
        });
    }
    profileID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"] objectForKey:PARAMETER36];
    
    [self addControls];
    balance_updatedLabel.text = [self getTimeDifferencebetweenDateForBalance];
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:FIRST_LOGIN] == 1) {
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:FIRST_LOGIN];
        recent_updatedLabel.text = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_last_updated_not_yet", nil)]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"isMiniStatementUpdated"] isEqualToString:@"YES"] && ([[NSUserDefaults standardUserDefaults] integerForKey:FIRST_LOGIN] != 1))
        recent_updatedLabel.text = [self getTimeDifferencebetweenDateForRecent];
    
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"statementUpdated"];
    NSInteger inte = [[NSUserDefaults standardUserDefaults] integerForKey:FIRST_LOGIN];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"statementUpdated"] && ([[NSUserDefaults standardUserDefaults] integerForKey:FIRST_LOGIN] != 1)){
        databaseManager = [[DatabaseManager alloc] initWithDatabaseName :DATABASE_NAME];
        [recent_dataArray removeAllObjects];
        [recentTableView reloadData];
        recent_dataArray = [[NSMutableArray alloc] initWithArray:(NSMutableArray *)[databaseManager getAllMiniStatementDetails]];
        NSLog(@"Recent data array : %@", recent_dataArray);
        if (recent_dataArray.count > 0)
        {
            recentTableView.hidden = NO;
            alertLabel.hidden = YES;
        }
        else
        {
            alertLabel.hidden = NO;
            recentTableView.hidden = YES;
        }
        NSLog(@"View details are..%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"statementUpdated"]);

        recentTableView.delegate = self;
        recentTableView.dataSource = self;
        [recentTableView reloadData];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"statementUpdated"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(NSMutableArray*)getRecentDataArray{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"LatestMiniStatement"];
    if(!str)
        return nil;
    else{
        NSArray *temp = [str componentsSeparatedByString:@";"];
//        "transaction_amount" = 10;
//        "transaction_date" = "17/03/2018 14:11";
//        "transaction_type" = "Bill Pay";
        for (NSString *str in temp) {
            NSArray *temp2 = [str componentsSeparatedByString:@"|"];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setObject:[temp2 objectAtIndex:2] forKey:@"transaction_amount"];
            [dict setObject:[temp2 objectAtIndex:4] forKey:@"transaction_date"];
            [dict setObject:[temp2 objectAtIndex:0] forKey:@"transaction_type"];
            [arr addObject:dict];
        }
    }
    NSLog(@"final array -> %@", arr);
    return arr;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self setRecentBalanceValue];
    recent_dataArray = [[NSMutableArray alloc] initWithArray:(NSMutableArray *)[databaseManager getAllMiniStatementDetails]];
//    if(recent_dataArray && [recent_dataArray count] == 0){
//        NSMutableArray *temp = [self getRecentDataArray];
//        if(temp)
//            recent_dataArray = [[NSMutableArray alloc] initWithArray:temp];
//    }
    if (recent_dataArray.count != 0) {
        recentTableView.hidden = NO;
        alertLabel.hidden = YES;
        [recentTableView reloadData];
    }
    else
    {
        recentTableView.hidden = YES;
        alertLabel.hidden = NO;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popUpTemplate1Action:) name:PARENT_TEMPLATE5 object:nil];
    [super viewDidAppear:YES];
    
    isRecentListUpdated = NO;

    if([propertyFile isEqualToString:@"LaunchPagePT5"]){
        if([balance_valueLabel.text isEqualToString:@"0"]){
            balance_valueLabel.text = @"";
        }
    }
}

-(void) viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PARENT_TEMPLATE5 object:nil];
    [super viewDidDisappear:YES];
    isRecentListUpdated = NO;
}

#pragma mark - User image Button Action
/*
 * This method is used to add Camera interface when user selects profile pic.
 */

-(void)userImageViewButtonAction:(id)sender
{
    actionSheet = [[UIActionSheet alloc] initWithTitle: nil delegate:self cancelButtonTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancelbutton", nil)] destructiveButtonTitle: nil otherButtonTitles:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_take_Photo", nil)],[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_choose_from_gallery", nil)], nil];
    [actionSheet showInView:[[[UIApplication sharedApplication] delegate] window]];
}

#pragma mark - UIImagePickerController delegate Methods.
/**
 * This method is used to set by using this delegate Open The camera Source Type.
 *@param type- Image Source Type(JPEG or PNG)
 *           - Image Size(KB).
 */
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:NULL];
    NSData* data = UIImagePNGRepresentation([info objectForKey:UIImagePickerControllerEditedImage]);
    
    if (data)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        userImagePath = [documentsDirectory stringByAppendingPathComponent:
                         @"test.png" ];
        [[NSUserDefaults standardUserDefaults] setObject:userImagePath forKey:@"userImagePath"];
        [data writeToFile:userImagePath atomically:YES];
    }
    
    [databaseManager updateTableDataInLoginDetails:@"test.png"];
    [balance_userImageViewButton setImage:[UIImage imageWithData:data] forState:UIControlStateNormal];

    [profileImageView setImage:[UIImage imageWithData:data]];
    profileImageView.center = [backgImageView convertPoint:backgImageView.center fromView:backgImageView.superview];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark Uploading Image Methods.
/**
 * This method is used to set Open camera when user tap on Upload button.
 *@param type - Select From Options(Take photo and Select From gallery).
 */
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self takeNewPhotoFromCamera];
            break;
        case 1:
            [self choosePhotoFromExistingImages];
            break;
            
        default:
            break;
    }
}

- (void)takeNewPhotoFromCamera{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]){
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.allowsEditing = YES;
        controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeCamera];
        controller.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
        controller.delegate = self;
        [self.navigationController presentViewController:controller animated:YES completion:^{
            [self transformScreen];
        }];
//        [self.navigationController presentViewController: controller animated: YES completion: nil];
    }
    else{
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_consumer_client", nil)]] message:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_no_camera_found_in_this_device", nil)]] delegate:nil cancelButtonTitle:
//                            [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancel", nil)] otherButtonTitles:nil, nil];
//        [alert show];
        
        
        UIAlertController *alert= [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_consumer_client", nil)]] message:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_no_camera_found_in_this_device", nil)]] preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancel", nil)] style:UIAlertActionStyleCancel handler:nil]];
        
        UIViewController *topController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
        while (topController.presentedViewController)
        {
            topController = topController.presentedViewController;
        }
        [topController presentViewController:alert animated:NO completion:nil];
        
    }
}

-(void)transformScreen{
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TransformScreen" object:self userInfo:nil];
    }
}

-(void)choosePhotoFromExistingImages
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary]){
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        controller.allowsEditing = YES;
        controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
        controller.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
        controller.delegate = self;
        [self.navigationController presentViewController: controller animated: YES completion:^{
            [self transformScreen];
        }];
    }
    else if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        controller.allowsEditing = YES;
        controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeSavedPhotosAlbum];
        controller.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
        controller.delegate = self;
//        [self.navigationController presentViewController: controller animated: YES completion: nil];
        [self.navigationController presentViewController:controller animated:YES completion:^{
            [self transformScreen];
        }];
    }
}


#pragma  mark - Add Balance,Recent  View
/**
 * This method is used to set add UIconstraints Frame of ParentTemplate5(Balance View) .
 */
-(void)addBalanceView
{
    //Balance View frame
    balanceSubView = [[UIView alloc] init];
    float height = (self.view.frame.size.height*30)/100;
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template5_approach_type", propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"1"])
        balanceSubView.frame = CGRectMake(pageHeader.frame.origin.x+5,refreshSubView.frame.origin.y+refreshSubView.frame.size.height, parentSubView.frame.size.width, height);
    else
        balanceSubView.frame = CGRectMake(pageHeader.frame.origin.x, pageHeader.frame.size.height, SCREEN_WIDTH, height);
    
    // BackgroundImage
    
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:balanceSubView.bounds];
    [bgImageView setImage:[UIImage imageNamed:@"home-screen-bg.jpg" inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil]];
    [balanceSubView addSubview:bgImageView];

    NSArray *balanceBackGroundColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_balance_layout_back_ground_color", propertyFile,[NSBundle mainBundle], nil)];
    
    if (balanceBackGroundColor)
        balanceSubView.backgroundColor = [UIColor colorWithRed:[[balanceBackGroundColor objectAtIndex:0] floatValue] green:[[balanceBackGroundColor objectAtIndex:1] floatValue] blue:[[balanceBackGroundColor objectAtIndex:2] floatValue] alpha:1.0f];
    else
        balanceSubView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:balanceSubView];
    
    // UserName Label
    balance_userNameLabel = [[UILabel alloc] init];
    balance_userNameLabel.frame = CGRectMake(balanceSubView.frame.size.width/2, balanceSubView.frame.size.height-(balanceSubView.frame.size.height*.3), balanceSubView.frame.size.width/2, (balanceSubView.frame.size.height/5));
    userDeatils=[[NSMutableDictionary alloc]init];
    userDeatils = [[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"];
   // NSLog(@"Userdetails Are...%@",userDeatils);
    
    NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_prefix_login_user", nil)];
    NSString *nameStr=[NSString stringWithFormat:@"%@",[userDeatils objectForKey:PARAMETER11]];
    
    if (!((nameStr == (id)[NSNull null] || nameStr.length == 0 || [nameStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) || [nameStr compare:@"(null)"]== NSOrderedSame))
    {
        balance_userNameLabel.text=[NSString stringWithFormat:@"%@%@",headerStr,[nameStr stringByReplacingOccurrencesOfString:@"(null)" withString:@""]];
    }
    else
    {
        balance_userNameLabel.text = [NSString stringWithFormat:@"%@%@",headerStr,@""];
    }
    
    balance_userNameLabel.numberOfLines = 2;
    NSArray *balance_userNameLabel_bgColor = [ValidationsClass colorWithHexString:parent_template5_login_user_name_back_ground_color];
    
    if (balance_userNameLabel_bgColor) {
        balance_userNameLabel.backgroundColor = [UIColor clearColor];
        //balance_userNameLabel.backgroundColor = [UIColor colorWithRed:[[balance_userNameLabel_bgColor objectAtIndex:0] floatValue] green:[[balance_userNameLabel_bgColor objectAtIndex:1] floatValue] blue:[[balance_userNameLabel_bgColor objectAtIndex:2] floatValue] alpha:1.0f];
    }
    else
        balance_userNameLabel.backgroundColor = [UIColor greenColor];
    
    // Properties for label TextColor.
    NSArray *label1_TextColor;
    
    if (NSLocalizedStringFromTableInBundle(@"parent_template5_login_user_name_text_color",propertyFile,[NSBundle mainBundle], nil))
        label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_login_user_name_text_color",propertyFile,[NSBundle mainBundle], nil)];
    
    else if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
        label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
    else
        label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
    
    if (label1_TextColor)
        
        balance_userNameLabel.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
    
    // Properties for label Textstyle.
    
    NSString *textStyle;
    if(NSLocalizedStringFromTableInBundle(@"parent_template5_login_user_name_text_style",propertyFile,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_login_user_name_text_style",propertyFile,[NSBundle mainBundle], nil);
    else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
    else
        textStyle = application_default_text_style;
    
    
    // Properties for label Font size.
    
    NSString *fontSize;
    if(NSLocalizedStringFromTableInBundle(@"parent_template5_login_user_name_text_size",propertyFile,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_login_user_name_text_size",propertyFile,[NSBundle mainBundle], nil);
    else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
    else
        fontSize = application_default_text_size;
    
    if ([textStyle isEqualToString:TEXT_STYLE_0])
        balance_userNameLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_1])
        balance_userNameLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_2])
        balance_userNameLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
    
    else
        balance_userNameLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    balance_userNameLabel.textAlignment=NSTextAlignmentCenter;
    [balanceSubView addSubview:balance_userNameLabel];

    //Seperator label
    balance_seperatorLabel= [[UILabel alloc] init];
    balance_seperatorLabel.frame = CGRectMake(0,balance_userNameLabel.frame.size.height, balanceSubView.frame.size.width,0);
    balance_seperatorLabel.backgroundColor = [UIColor clearColor];
    [balanceSubView addSubview:balance_seperatorLabel];
    
    //User ImageView
    float imageButtonHeight = balanceSubView.frame.size.height-(balance_userNameLabel.frame.size.height+balance_seperatorLabel.frame.size.height);
    
    imageButtonHeight = imageButtonHeight-20;
    
    balance_userImageViewButton = [UIButton buttonWithType:UIButtonTypeCustom];

    CGRect rect = CGRectMake(balanceSubView.frame.size.width - (balanceSubView.frame.size.width*.3) - 50, balanceSubView.frame.size.height-(balanceSubView.frame.size.height*.85),(balanceSubView.frame.size.width*.3), (balanceSubView.frame.size.width*.3));
    balance_userImageViewButton.frame = rect;
    
    backgImageView =  [[UIImageView alloc] initWithFrame:rect];
    [backgImageView setImage:[UIImage imageNamed:@"Right-image" inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil]];
    CGRect rect2 = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width-15, rect.size.height-15);
    profileImageView =  [[UIImageView alloc] initWithFrame:rect2];
    profileImageView.layer.cornerRadius = profileImageView.frame.size.width/2;
    profileImageView.layer.masksToBounds = YES;
    userImageViewButton = [[UIButton alloc] initWithFrame:rect];
    [userImageViewButton addTarget:self action:@selector(userImageViewButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    balance_userImageViewButton.frame = CGRectMake(balanceSubView.frame.size.width - (balanceSubView.frame.size.width*.3) - 50, balanceSubView.frame.size.height-(balanceSubView.frame.size.height*.85),(balanceSubView.frame.size.width*.3), (balanceSubView.frame.size.width*.3));
    balance_userImageViewButton.layer.cornerRadius = balance_userImageViewButton.bounds.size.height/2;
    balance_userImageViewButton.backgroundColor=[UIColor clearColor];
    [balance_userImageViewButton setTag:101];
    [balance_userImageViewButton addTarget:self action:@selector(userImageViewButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    selectedImage = [UIImage imageWithContentsOfFile:userImagePath];
    selectedImage=[UIImage imageFlipWithSourceImage:selectedImage];
    [balance_userImageViewButton setImage:selectedImage?selectedImage:[UIImage imageNamed:@"logo-overlay" inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];

    [profileImageView setImage:selectedImage?selectedImage:[UIImage imageNamed:@"logo-overlay" inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil]];
    if(!selectedImage){
        profileImageView.frame = CGRectMake(rect2.origin.x, rect2.origin.y, rect2.size.height-15, rect2.size.width-15);
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
        {
            NSLog(@"Reversing profile image..");
            profileImageView.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
        }
    }else{
        profileImageView.frame = rect2;
    }
    profileImageView.center = [backgImageView convertPoint:backgImageView.center fromView:backgImageView.superview];

    
    [balance_userImageViewButton setImageEdgeInsets:UIEdgeInsetsMake(20, 20, 20, 20)];
    [balance_userImageViewButton setBackgroundImage:[UIImage imageNamed:@"Right-image" inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil] forState:UIControlStateNormal];

    [balanceSubView addSubview:backgImageView];
    [balanceSubView addSubview:profileImageView];
    [balanceSubView addSubview:userImageViewButton];
    
    nextY_Position=balance_userImageViewButton.frame.origin.y-5;
    
    //user name label
    userNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(balance_userImageViewButton.frame.origin.x-30,balance_userImageViewButton.frame.size.height+20, balance_userImageViewButton.frame.size.width+30, 45)];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *userDetails =[[NSMutableDictionary alloc] initWithDictionary:[userDefaults objectForKey:@"userDetails"]];
    NSLog(@"User details : %@", userDetails);
    NSString *firstName = [userDetails objectForKey:@"SenderFirstName"];
    if([firstName isEqualToString:@"(null)"])
        firstName = @"";
    NSString *lastName = [userDetails objectForKey:@"SenderLastName"];
    if([lastName isEqualToString:@"(null)"])
        lastName = @"";
    NSString *fullName = [NSString stringWithFormat:@"%@, %@ %@", [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_welcome", nil)], firstName, lastName];
    fullName = [fullName stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    [userNameLabel setText:fullName];
    [userNameLabel setAdjustsFontSizeToFitWidth:YES];
    [userNameLabel setNumberOfLines:2];
    [userNameLabel setTextAlignment:NSTextAlignmentCenter];
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
    {
        userNameLabel.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
    }
    [balanceSubView addSubview:userNameLabel];
    
    //Balance Label
    balance_balanceLabel = [[UILabel alloc] init];
    balance_balanceLabel.frame = CGRectMake(0, 40, balanceSubView.frame.size.width*.5, 20);
    balance_balanceLabel.backgroundColor = [UIColor clearColor];
    
    NSString *balanceStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template5_balance_header_text",propertyFile,[NSBundle mainBundle], nil)];
    
    if (balanceStr)
        balance_balanceLabel.text=balanceStr;
    
    // Properties for label TextColor.
    NSArray *label1_TextColor1;
    
    if (NSLocalizedStringFromTableInBundle(@"parent_template5_balance_header_text_color",propertyFile,[NSBundle mainBundle], nil))
        label1_TextColor1 = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_balance_header_text_color",propertyFile,[NSBundle mainBundle], nil)];
    else if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
        
        label1_TextColor1 = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
    else
        label1_TextColor1 = [ValidationsClass colorWithHexString:application_default_text_color];
    
    if (label1_TextColor1)
        balance_balanceLabel.textColor = [UIColor colorWithRed:[[label1_TextColor1 objectAtIndex:0] floatValue] green:[[label1_TextColor1 objectAtIndex:1] floatValue] blue:[[label1_TextColor1 objectAtIndex:2] floatValue] alpha:1.0f];
    
    // Properties for label Textstyle.
    
    NSString *textStyle1;
    if(NSLocalizedStringFromTableInBundle(@"parent_template5_balance_header_text_style",propertyFile,[NSBundle mainBundle], nil))
        textStyle1 = NSLocalizedStringFromTableInBundle(@"parent_template5_balance_header_text_style",propertyFile,[NSBundle mainBundle], nil);
    else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
        textStyle1 = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
    else
        textStyle1 = application_default_text_style;
    
    // Properties for label Font size.
    
    NSString *fontSize1;
    if(NSLocalizedStringFromTableInBundle(@"parent_template5_balance_header_text_size",propertyFile,[NSBundle mainBundle], nil))
        fontSize1 = NSLocalizedStringFromTableInBundle(@"parent_template5_balance_header_text_size",propertyFile,[NSBundle mainBundle], nil);
    else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
        fontSize1 = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
    else
        fontSize1 = application_default_text_size;
    
    if ([textStyle isEqualToString:TEXT_STYLE_0])
        balance_balanceLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_1])
        balance_balanceLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_2])
        balance_balanceLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
    
    else
        balance_balanceLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    balance_balanceLabel.textAlignment = NSTextAlignmentCenter;
    [balanceSubView addSubview:balance_balanceLabel];
    
    nextY_Position=(SCREEN_HEIGHT>=667)?(balance_balanceLabel.frame.origin.y+balance_balanceLabel.frame.size.height)+5:(balance_balanceLabel.frame.origin.y+balance_balanceLabel.frame.size.height);
    
    // Balance Value Label
    balance_valueLabel = [[UILabel alloc] init];
    balance_valueLabel.frame = CGRectMake(0, CGRectGetMaxY(balance_balanceLabel.frame) + 10, balanceSubView.frame.size.width*.5,25);
    balance_valueLabel.backgroundColor = [UIColor clearColor];
    NSString *amount;
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"updatedBalance_T3"])
    {
        amount = [[NSUserDefaults standardUserDefaults] objectForKey:@"updatedBalance_T3"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"updatedBalance"];
    }
    // 14230413 Stack overflow
    NSNumberFormatter *numberFormat = [[NSNumberFormatter alloc] init];
    numberFormat.numberStyle = NSNumberFormatterCurrencyStyle;
    [numberFormat setCurrencySymbol:@""];
    
    if (NSLocalizedStringFromTableInBundle(@"application_amount_decimal_places",@"GeneralSettings",[NSBundle mainBundle], nil)) {
        [numberFormat setMaximumFractionDigits:[NSLocalizedStringFromTableInBundle(@"application_amount_decimal_places",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue]];
    }
    else{
        [numberFormat setMaximumFractionDigits:0];
    }
    [numberFormat setCurrencyCode:currency];
    NSNumber *amountStr = [NSNumber numberWithDouble:amount.doubleValue];
    //NSLog(@"Formatted Currency Value : %@",[numberFormat stringFromNumber:amountStr]);
    
    NSCharacterSet *charactersToRemove = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopq‌​rstuvwxyz"];
    NSString *resultvalue = [[[numberFormat stringFromNumber:amountStr] componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
    
    if (![amount isEqualToString:@""]) {
        if (!((resultvalue == (id)[NSNull null] || resultvalue.length == 0 || [resultvalue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) || [resultvalue compare:@"(null)"]== NSOrderedSame))
            balance_valueLabel.text = [NSString stringWithFormat:@"%@",resultvalue];
    }
    else
        balance_valueLabel.text =@"";
    
    // Properties for label TextColor.
    NSArray *label1_TextColorArray;
    
    if (NSLocalizedStringFromTableInBundle(@"parent_template5_balance_value_text_color",propertyFile,[NSBundle mainBundle], nil))
        
        label1_TextColorArray = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_balance_value_text_color",propertyFile,[NSBundle mainBundle], nil)];
    
    else if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
        
        label1_TextColorArray = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
    else
        label1_TextColorArray = [ValidationsClass colorWithHexString:application_default_text_color];
    
    if (label1_TextColorArray)
        balance_valueLabel.textColor = [UIColor colorWithRed:[[label1_TextColorArray objectAtIndex:0] floatValue] green:[[label1_TextColorArray objectAtIndex:1] floatValue] blue:[[label1_TextColorArray objectAtIndex:2] floatValue] alpha:1.0f];
    
    // Properties for label Textstyle.
    NSString *textStyleStrVal;
    if(NSLocalizedStringFromTableInBundle(@"parent_template5_balance_value_text_style",propertyFile,[NSBundle mainBundle], nil))
        textStyleStrVal = NSLocalizedStringFromTableInBundle(@"parent_template5_balance_value_text_style",propertyFile,[NSBundle mainBundle], nil);
    else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
        textStyleStrVal = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
    else
        textStyleStrVal = application_default_text_style;
    
    
    // Properties for label Font size.
    
    NSString *fontSizeVal;
    if(NSLocalizedStringFromTableInBundle(@"parent_template5_balance_value_text_size",propertyFile,[NSBundle mainBundle], nil))
        fontSizeVal = NSLocalizedStringFromTableInBundle(@"parent_template5_balance_value_text_size",propertyFile,[NSBundle mainBundle], nil);
    else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
        fontSizeVal = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
    else
        fontSizeVal = application_default_text_size;
    
    if ([textStyle isEqualToString:TEXT_STYLE_0])
        balance_valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_1])
        balance_valueLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_2])
        balance_valueLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
    
    else
        balance_valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    balance_valueLabel.textAlignment =NSTextAlignmentCenter;
    Xposition=balance_userImageViewButton.frame.size.width+10;
    nextY_Position=(balance_valueLabel.frame.origin.y+5);
    [balanceSubView addSubview:balance_valueLabel];

    refreshButton = [[UIButton alloc] initWithFrame:CGRectMake(balance_valueLabel.frame.origin.x, balance_valueLabel.frame.origin.y, balance_valueLabel.frame.size.width, balance_valueLabel.frame.size.height)];
    [refreshButton addTarget:self action:@selector(refreshBalance:) forControlEvents:UIControlEventTouchUpInside];
    [balanceSubView addSubview:refreshButton];

    // Currency Label
    balance_currencyLabel = [[UILabel alloc] init];
    balance_currencyLabel.frame = CGRectMake(0, CGRectGetMaxY(balance_valueLabel.frame)+10, balanceSubView.frame.size.width*.5, 20);
    balance_currencyLabel.backgroundColor = [UIColor clearColor];
    currency=[NSString stringWithFormat:@"%@",(NSLocalizedStringFromTableInBundle(@"parent_template5_balance_currency_symbol_text",propertyFile,[NSBundle mainBundle], nil))];
    
    if ([userDeatils objectForKey:PARAMETER22]){
        currency = [userDeatils objectForKey:PARAMETER22];
    }
    //Application Amount format
    else if ([userDeatils objectForKey:PARAMETER22] == (id)[NSNull null] || [[userDeatils objectForKey:PARAMETER22]length] == 0){
        currency=[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"application_amount_format",@"GeneralSettings",[NSBundle mainBundle], nil)];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
    {
        currency = @"ر.ي";
    }
    balance_currencyLabel.text = [NSString stringWithFormat:@"%@",currency];
    
    //Default Properties for label textcolor
    NSArray *label1_TextColorArr;
    if (NSLocalizedStringFromTableInBundle(@"parent_template5_balance_currency_symbol_text_color",propertyFile,[NSBundle mainBundle], nil))
        label1_TextColorArr = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_balance_currency_symbol_text_color",propertyFile,[NSBundle mainBundle], nil)];
    else
        label1_TextColorArr=[ValidationsClass colorWithHexString:application_default_text_color];
    if (label1_TextColorArr)
        balance_currencyLabel.textColor = [UIColor colorWithRed:[[label1_TextColorArr objectAtIndex:0] floatValue] green:[[label1_TextColorArr objectAtIndex:1] floatValue] blue:[[label1_TextColorArr objectAtIndex:2] floatValue] alpha:1.0f];
    
    
    //Default Properties for label textStyle
    
    NSString *textStyleStr;
    if(NSLocalizedStringFromTableInBundle(@"parent_template5_balance_currency_symbol_text_style",propertyFile,[NSBundle mainBundle], nil))
        textStyleStr = NSLocalizedStringFromTableInBundle(@"parent_template5_balance_currency_symbol_text_style",propertyFile,[NSBundle mainBundle], nil);
    else
        textStyleStr = application_default_text_style;
    
    //Default Properties for label fontSize
    
    NSString *fontSizeValue;
    if(NSLocalizedStringFromTableInBundle(@"parent_template5_balance_currency_symbol_text_size",propertyFile,[NSBundle mainBundle], nil))
        fontSizeValue = NSLocalizedStringFromTableInBundle(@"parent_template5_balance_currency_symbol_text_size",propertyFile,[NSBundle mainBundle], nil);
    else
        fontSizeValue = application_default_text_size;
    
    if ([textStyle isEqualToString:TEXT_STYLE_0])
        balance_currencyLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_1])
        balance_currencyLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_2])
        balance_currencyLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
    
    else
        balance_currencyLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    balance_currencyLabel.textAlignment = NSTextAlignmentCenter;
    [balanceSubView addSubview:balance_currencyLabel];
    
    nextY_Position=(SCREEN_HEIGHT>=667)?(balance_currencyLabel.frame.origin.y+balance_currencyLabel.frame.size.height+10):(balance_currencyLabel.frame.origin.y+balance_currencyLabel.frame.size.height);
    
    Xposition=balance_userImageViewButton.frame.size.width;
    

    // Updated text label
    if ([NSLocalizedStringFromTableInBundle(@"parent_template5_approach_type", propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"2"])
    {
        balance_updatedLabel = [[UILabel alloc] init];
       // balance_updatedLabel.frame = CGRectMake(balance_userImageViewButton.frame.origin.x+balance_userImageViewButton.frame.size.width+5,nextY_Position+10,balanceSubView.frame.size.width-(balance_userImageViewButton.frame.size.width+35),25);
        balance_updatedLabel.backgroundColor = [UIColor clearColor];
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus netStatus = [reachability  currentReachabilityStatus];
        
        if([profileID isEqualToString:@"SMS"] || (netStatus == NotReachable))
            balance_updatedLabel.text = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_last_updated_not_yet", nil)]];
        else
            balance_updatedLabel.text = [self getTimeDifferencebetweenDateForBalance];
        
        balance_updatedLabel.numberOfLines = 3;
        [balance_updatedLabel sizeToFit];
        
        
        // Properties for label TextColor.
        NSArray *label1_TextColor;
        
        if (NSLocalizedStringFromTableInBundle(@"parent_template5_balance_last_updated_text_color",propertyFile,[NSBundle mainBundle], nil))
            
            label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_balance_last_updated_text_color",propertyFile,[NSBundle mainBundle], nil)];
        
        else if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
            
            label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
        else
            label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
        
        if (label1_TextColor)
            balance_updatedLabel.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        // Properties for label Textstyle.
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template5_balance_last_updated_text_style",propertyFile,[NSBundle mainBundle], nil))
            textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_balance_last_updated_text_style",propertyFile,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
            textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
        else
            textStyle = application_default_text_style;
        
        
        // Properties for label Font size.
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template5_balance_last_updated_text_size",propertyFile,[NSBundle mainBundle], nil))
            fontSize=NSLocalizedStringFromTableInBundle(@"parent_template5_balance_last_updated_text_size",propertyFile,[NSBundle mainBundle], nil);
        
        else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
            fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
        else
            fontSize = application_default_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
            balance_updatedLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
            balance_updatedLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
            balance_updatedLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
            balance_updatedLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        balance_updatedLabel.textAlignment = NSTextAlignmentLeft;
       Xposition=(balance_updatedLabel.frame.size.width+balance_updatedLabel.frame.origin.x);
        nextY_Position=balance_updatedLabel.frame.origin.y+12;
        //Xposition=(balance_userImageViewButton.frame.size.width+balance_updatedLabel.intrinsicContentSize.width);
        
       // [balanceSubView addSubview:balance_updatedLabel];
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template5_balance_refresh_image_visibility",propertyFile,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT]==NSOrderedSame)
        {
            //Balance refresh icon image
            balance_refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
            balance_refreshButton.frame = CGRectMake(Xposition,nextY_Position,25,25);
            [balance_refreshButton setTag:101];
            [balance_refreshButton setExclusiveTouch:YES];
            [balance_refreshButton addTarget:self action:@selector(refreshBalance:) forControlEvents:UIControlEventTouchUpInside];
            UIImage *refreshBtnImage = [UIImage imageNamed:NSLocalizedStringFromTableInBundle(@"parent_template5_balance_refresh_image",propertyFile,[NSBundle mainBundle], nil) inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil];
            [balance_refreshButton setImage:refreshBtnImage forState:UIControlStateNormal];
            [balance_refreshButton setImage:refreshBtnImage forState:UIControlStateSelected];
            [balanceSubView addSubview:balance_refreshButton];
        }
        
        
    }
    
    balance_seperatorLabel2 = [[UILabel alloc] init];
    balance_seperatorLabel2.frame = CGRectMake(0,balanceSubView.frame.size.height-3, balanceSubView.frame.size.width,3);
    
    balance_seperatorLabel2.backgroundColor = [UIColor greenColor];
    NSArray *balance_seperatorLabel2_bgColor = [ValidationsClass colorWithHexString:parent_template5_login_user_name_back_ground_color];
    
    if (balance_seperatorLabel2_bgColor)
        balance_seperatorLabel2.backgroundColor = [UIColor colorWithRed:[[balance_seperatorLabel2_bgColor objectAtIndex:0] floatValue] green:[[balance_seperatorLabel2_bgColor objectAtIndex:1] floatValue] blue:[[balance_seperatorLabel2_bgColor objectAtIndex:2] floatValue] alpha:1.0f];
    else
        balance_seperatorLabel2.backgroundColor = [UIColor clearColor];
    
    [balanceSubView addSubview:balance_seperatorLabel2];
}

- (void)addNewRecentView {
    
    // New RecentView
    UIView *newRecentView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(balanceSubView.frame), self.view.bounds.size.width, 45.0)];
    [newRecentView setBackgroundColor: [UIColor clearColor]];
    
    // Recent Button
    UIButton *recentButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [recentButton setFrame: CGRectMake(0, 0, newRecentView.bounds.size.width/2, newRecentView.bounds.size.height)];
    [recentButton setTag:kRecentViewRecentButtonTag];
    
    [recentButton setTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_recent", nil)] forState:UIControlStateNormal];
    [recentButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    NSArray *recentBackGroundColor = [ValidationsClass colorWithHexString:@"#FF9000"];
    [recentButton setBackgroundColor:[UIColor colorWithRed:[[recentBackGroundColor objectAtIndex:0] floatValue] green:[[recentBackGroundColor objectAtIndex:1] floatValue] blue:[[recentBackGroundColor objectAtIndex:2] floatValue] alpha:1.0f]];
    
    [recentButton addTarget:self action:@selector(recentViewButtonAction:) forControlEvents:UIControlEventTouchDown];
    [newRecentView addSubview:recentButton];
    
    // Floosak Button
    UIButton *floosakButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [floosakButton setFrame: CGRectMake(CGRectGetMaxX(recentButton.frame), 0, newRecentView.bounds.size.width/2, newRecentView.bounds.size.height)];
    [floosakButton setTag:kRecentViewFloosakButtonTag];
    
    [floosakButton setTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_floosak", nil)] forState:UIControlStateNormal];
    [floosakButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

    recentBackGroundColor = [ValidationsClass colorWithHexString:@"#FFAE00"];
    [floosakButton setBackgroundColor:[UIColor colorWithRed:[[recentBackGroundColor objectAtIndex:0] floatValue] green:[[recentBackGroundColor objectAtIndex:1] floatValue] blue:[[recentBackGroundColor objectAtIndex:2] floatValue] alpha:1.0f]];
    
    [floosakButton addTarget:self action:@selector(recentViewButtonAction:) forControlEvents:UIControlEventTouchDown];
    [newRecentView addSubview:floosakButton];
    
    self.recentSubView = newRecentView;
    [self.view addSubview:newRecentView];
    {
        recentTableView = [[UITableView alloc] init];
        recentTableView.delegate = self;
        recentTableView.dataSource = self;
      recentTableView.frame = CGRectMake(5, CGRectGetMaxY(newRecentView.frame), SCREEN_WIDTH-10, SCREEN_HEIGHT/2);
//      recentTableView.frame = CGRectMake(newRecentView.frame.origin.x, 50, newRecentView.frame.size.width, newRecentView.frame.size.height);
        recentTableView.scrollEnabled = YES;
        recentTableView.userInteractionEnabled = YES;
        recentTableView.bounces = YES;
//        [[UIApplication sharedApplication].keyWindow addSubview:recentTableView];
        [self.view addSubview:recentTableView];
//        recentTableView.layer.zPosition = 10;
        if([recent_dataArray count] == 0)
        {
            recentTableView.hidden=YES;
            //        alertLabel.frame = CGRectMake(10,(newRecentView.frame.size.height/2),newRecentView.frame.size.width-20,50);
            alertLabel.frame = CGRectMake(10, 10, 100, 25);
            alertLabel.text =[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transactions_not_available",propertyFile,[NSBundle mainBundle], nil)];
            alertLabel.numberOfLines=2;
            alertLabel.textAlignment = NSTextAlignmentCenter;
//            [recentSubView addSubview:alertLabel];
        }
        else
        {
            recentTableView.hidden = NO;
            [recentTableView reloadData];
        }
    }
    NSLog(@"recent view order : %@", [newRecentView subviews]);
    NSLog(@"recent view order2 : %@", [recentSubView subviews]);
    
}

- (void)recentViewButtonAction:(id)sender {
    NSLog(@"Recent ButtonAction: %@", sender);
    UIButton *btn = (UIButton *)sender;
    if(btn.tag == 110 && !isRecentListUpdated){
        [self updateRecentTransactions];
        isRecentListUpdated = YES;
    }
    NSArray *recentBackGroundColor = [ValidationsClass colorWithHexString:@"#FF9000"];
    [btn setBackgroundColor:[UIColor colorWithRed:[[recentBackGroundColor objectAtIndex:0] floatValue] green:[[recentBackGroundColor objectAtIndex:1] floatValue] blue:[[recentBackGroundColor objectAtIndex:2] floatValue] alpha:1.0f]];
    if(btn.tag == kRecentViewRecentButtonTag) {
        UIButton *oldBtn = [self.view viewWithTag:kRecentViewFloosakButtonTag];
        NSArray *recentBackGroundColor = [ValidationsClass colorWithHexString:@"#FFAE00"];
        [oldBtn setBackgroundColor:[UIColor colorWithRed:[[recentBackGroundColor objectAtIndex:0] floatValue] green:[[recentBackGroundColor objectAtIndex:1] floatValue] blue:[[recentBackGroundColor objectAtIndex:2] floatValue] alpha:1.0f]];
//        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 120, 120, 30)];
//        [label setText:@"Test text"];
//        [recentSubView addSubview:label];
        [recentTableView setHidden:false];
        [gridcollectionView setHidden:true];
    } else {
        UIButton *oldBtn = [self.view viewWithTag:kRecentViewRecentButtonTag];
        NSArray *recentBackGroundColor = [ValidationsClass colorWithHexString:@"#FFAE00"];
        [oldBtn setBackgroundColor:[UIColor colorWithRed:[[recentBackGroundColor objectAtIndex:0] floatValue] green:[[recentBackGroundColor objectAtIndex:1] floatValue] blue:[[recentBackGroundColor objectAtIndex:2] floatValue] alpha:1.0f]];
        
        [recentTableView setHidden:true];
        [gridcollectionView setHidden:false];
    }
}


/**
 * This method is used to set add UIconstraints Frame of ParentTemplate5(Recent View) .
 */

-(void)addRecentView
{
    [self addNewRecentView]; // Skipping Old Desing
    return;
    
    // Recent Subview
    //float height = self.view.frame.size.height-(pageHeader.frame.size.height+balanceSubView.frame.size.height);
    float height = SCREEN_HEIGHT*0.15;
    recentSubView = [[UIView alloc] init];
    [recentSubView setClipsToBounds:true];
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template5_approach_type", propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"1"])
        recentSubView.frame = CGRectMake(pageHeader.frame.origin.x+5, balanceSubView.frame.origin.y+balanceSubView.frame.size.height+10, parentSubView.frame.size.width, height-20);
    else
        recentSubView.frame = CGRectMake(0, CGRectGetMaxY(balanceSubView.frame), SCREEN_WIDTH, height);
    
    // Recent BackgroundColor.
    NSArray *recentBackGroundColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_recent_layout_back_ground_color",propertyFile,[NSBundle mainBundle], nil)];
    
    if (recentBackGroundColor)
        recentSubView.backgroundColor = [UIColor colorWithRed:[[recentBackGroundColor objectAtIndex:0] floatValue] green:[[recentBackGroundColor objectAtIndex:1] floatValue] blue:[[recentBackGroundColor objectAtIndex:2] floatValue] alpha:1.0f];
    else
        recentSubView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:recentSubView];
    

    
    recent_titleLabel=[[UILabel alloc]init];
    recent_titleLabel.frame=CGRectMake(10, 5, recentSubView.frame.size.width-20,(recentSubView.frame.size.height)/9);
    
    NSString *recentStr=[Localization languageSelectedStringForKey: NSLocalizedStringFromTableInBundle(@"parent_template5_recent_header_text",propertyFile,[NSBundle mainBundle], nil)];
    if (recentStr)
        recent_titleLabel.text = recentStr;
    
    
    // Properties for label TextColor.
    NSArray *label1_TextColor;
    
    if (NSLocalizedStringFromTableInBundle(@"parent_template5_recent_header_text_color",propertyFile,[NSBundle mainBundle], nil))
        
        label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_recent_header_text_color",propertyFile,[NSBundle mainBundle], nil)];
    
    else if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
        
        label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
    else
        label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
    
    if (label1_TextColor)
        recent_titleLabel.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
    
    // Properties for label Textstyle.
    
    NSString *textStyle;
    if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_header_text_style",propertyFile,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_recent_header_text_style",propertyFile,[NSBundle mainBundle], nil);
    else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
    else
        textStyle = application_default_text_style;
    
    
    // Properties for label Font size.
    
    NSString *fontSize;
    if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_header_text_size",propertyFile,[NSBundle mainBundle], nil))
        fontSize=NSLocalizedStringFromTableInBundle(@"parent_template5_recent_header_text_size",propertyFile,[NSBundle mainBundle], nil);
    else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
    else
        fontSize = application_default_text_size;
    
    if ([textStyle isEqualToString:TEXT_STYLE_0])
        recent_titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_1])
        recent_titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_2])
        recent_titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
    
    else
        recent_titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    [recentSubView addSubview:recent_titleLabel];
    
    nextY_Position=recent_titleLabel.frame.origin.y+recent_titleLabel.frame.size.height;
    
    recenttitle_borderLabel=[[UILabel alloc]init];
    recenttitle_borderLabel.frame=CGRectMake(0, nextY_Position, recentSubView.frame.size.width,3);
    
    NSArray *borderColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_transaction_list_view_seperator_back_ground_color",propertyFile,[NSBundle mainBundle], nil)];
    if (borderColor)
        recenttitle_borderLabel.backgroundColor = [UIColor colorWithRed:[[borderColor objectAtIndex:0] floatValue] green:[[borderColor objectAtIndex:1] floatValue] blue:[[borderColor objectAtIndex:2] floatValue] alpha:1.0f];
    
     nextY_Position = ((recentSubView.frame.size.height)/9)+10;
    [recentSubView addSubview:recenttitle_borderLabel];
    
    // Recent TableView
    recentTableView = [[UITableView alloc] init];
    recentTableView.delegate = self;
    recentTableView.dataSource = self;
    recentTableView.frame = CGRectMake(0, nextY_Position,recentSubView.frame.size.width, (recentSubView.frame.size.height-nextY_Position-5));
    [recentSubView addSubview:recentTableView];
    
    if([recent_dataArray count] == 0)
    {
        recentTableView.hidden=YES;
//        alertLabel.frame = CGRectMake(10,(recentSubView.frame.size.height/2),recentSubView.frame.size.width-20,50);
        alertLabel.frame = CGRectMake(10, 10, 100, 25);
        alertLabel.text =[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transactions_not_available",propertyFile,[NSBundle mainBundle], nil)];
        alertLabel.numberOfLines=2;
        alertLabel.textAlignment = NSTextAlignmentCenter;
        [recentSubView addSubview:alertLabel];
    }
    else
    {
        recentTableView.hidden = NO;
        [recentTableView reloadData];
    }
    
    //    recentSeperatorLabel3
    recentSeperatorLabel3=[[UILabel alloc]init];
   // recentSeperatorLabel3.frame=CGRectMake(0, recentSubView.frame.size.height-50, recentSubView.frame.size.width,2);
    
    NSArray *borderColor1= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_transaction_list_view_seperator_back_ground_color",propertyFile,[NSBundle mainBundle], nil)];
    if (borderColor1)
        recentSeperatorLabel3.backgroundColor = [UIColor colorWithRed:[[borderColor1 objectAtIndex:0] floatValue] green:[[borderColor1 objectAtIndex:1] floatValue] blue:[[borderColor1 objectAtIndex:2] floatValue] alpha:1.0f];
    
    //[recentSubView addSubview:recentSeperatorLabel3];
    
    
    if (SCREEN_HEIGHT>667)
    {
        nextY_Position=recentTableView.frame.origin.y+recentTableView.frame.size.height+40;
    }
    else if (SCREEN_HEIGHT==667)
    {
        nextY_Position=recentTableView.frame.origin.y+recentTableView.frame.size.height+30;
    }
    else
    {
        nextY_Position=recentTableView.frame.origin.y+recentTableView.frame.size.height+20;
    }
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template5_approach_type", propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"2"])
    {
        recent_updatedLabel=[[UILabel alloc]init];
      //  recent_updatedLabel.frame = CGRectMake(10, (recentSubView.frame.size.height-50),(recentSubView.frame.size.width-50), 40);
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus netStatus = [reachability  currentReachabilityStatus];
        
        if([profileID isEqualToString:@"SMS"] || (netStatus == NotReachable))
            recent_updatedLabel.text = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_last_updated_not_yet", nil)]];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        if ([userDefaults integerForKey:FIRST_LOGIN] == 1) {
            [userDefaults setInteger:2 forKey:FIRST_LOGIN];
            recent_updatedLabel.text = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_last_updated_not_yet", nil)]];
        }
        else
            recent_updatedLabel.text = [self getTimeDifferencebetweenDateForRecent];
        
        recent_updatedLabel.backgroundColor = [UIColor clearColor];
        
        // Properties for label TextColor.
        NSArray *label1_TextColor;
        
        if (NSLocalizedStringFromTableInBundle(@"parent_template5_recent_last_updated_text_color",propertyFile,[NSBundle mainBundle], nil))
            
            label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_recent_last_updated_text_color",propertyFile,[NSBundle mainBundle], nil)];
        
        else if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
            
            label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
        else
            label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
        
        if (label1_TextColor)
            recent_updatedLabel.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        // Properties for label Textstyle.
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_last_updated_text_style",propertyFile,[NSBundle mainBundle], nil))
            textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_recent_last_updated_text_style",propertyFile,[NSBundle mainBundle], nil);
        
        else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
            textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
        else
            textStyle = application_default_text_style;
        
        
        // Properties for label Font size.
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_last_updated_text_size",propertyFile,[NSBundle mainBundle], nil))
            fontSize=NSLocalizedStringFromTableInBundle(@"parent_template5_recent_last_updated_text_size",propertyFile,[NSBundle mainBundle], nil);
        
        else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
            fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
        else
            fontSize = application_default_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
            recent_updatedLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
            recent_updatedLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
            recent_updatedLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
            recent_updatedLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        
        recent_updatedLabel.numberOfLines=2;
        //            [recent_updatedLabel sizeToFit];
        //[recentSubView addSubview:recent_updatedLabel];
        
        if (SCREEN_HEIGHT>667)
        {
            Xposition=264;
        }
        else if (SCREEN_HEIGHT==667)
        {
            Xposition=240;
        }
        else
        {
            Xposition=190;
        }
        if ([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_refresh_image_visibility",propertyFile,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT]==NSOrderedSame)
        {
            recent_refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
            recent_refreshButton.frame = CGRectMake(Xposition,(recentSubView.frame.size.height-40),25,25);
            [recent_refreshButton setTag:101];
            [recent_refreshButton setExclusiveTouch:YES];
            [recent_refreshButton addTarget:self action:@selector(refreshRecentTransactions:) forControlEvents:UIControlEventTouchUpInside];
            
            UIImage *refreshBtnImage = [UIImage imageNamed:NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_refresh_image",propertyFile,[NSBundle mainBundle], nil) inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil];
            [recent_refreshButton setImage:refreshBtnImage forState:UIControlStateNormal];
            [recent_refreshButton setImage:refreshBtnImage forState:UIControlStateSelected];
            [recentSubView addSubview:recent_refreshButton];
        }
        
        recent_seperatorLabel = [[UILabel alloc]init];
        recent_seperatorLabel.frame=CGRectMake(0,recentSubView.frame.size.height-3, recentSubView.frame.size.width, 3);
        NSArray *recent_seperatorLabel_bgColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_transaction_list_view_seperator_back_ground_color",propertyFile,[NSBundle mainBundle], nil)];
        
        
        if (recent_seperatorLabel_bgColor)
            recent_seperatorLabel.backgroundColor = [UIColor colorWithRed:[[recent_seperatorLabel_bgColor objectAtIndex:0] floatValue] green:[[recent_seperatorLabel_bgColor objectAtIndex:1] floatValue] blue:[[recent_seperatorLabel_bgColor objectAtIndex:2] floatValue] alpha:1.0f];
        else
            recent_seperatorLabel.backgroundColor = [UIColor clearColor];
        
        [recentSubView addSubview:recent_seperatorLabel];
    }
    else
    {
        recentSubView.frame = CGRectMake(0, 0, recentSubView.frame.size.width, 3);
        recentSubView.backgroundColor = [UIColor whiteColor];
        recent_seperatorLabel = [[UILabel alloc]init];
        recent_seperatorLabel.frame=CGRectMake(0,recentSubView.frame.size.height-3, recentSubView.frame.size.width, 3);
        NSArray *recent_seperatorLabel_bgColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_transaction_list_view_seperator_back_ground_color",propertyFile,[NSBundle mainBundle], nil)];
        if (recent_seperatorLabel_bgColor)
            recent_seperatorLabel.backgroundColor = [UIColor colorWithRed:[[recent_seperatorLabel_bgColor objectAtIndex:0] floatValue] green:[[recent_seperatorLabel_bgColor objectAtIndex:1] floatValue] blue:[[recent_seperatorLabel_bgColor objectAtIndex:2] floatValue] alpha:1.0f];
        else
            recent_seperatorLabel.backgroundColor = [UIColor clearColor];
        
        [recentSubView addSubview:recent_seperatorLabel];
    }
}

#pragma mark - Load options Menu
/**
 * This method is used to set add OptionsMenu for ParentTemplate5.
 */
-(void)loadOptionsMenu
{
    optionsMenu = [[OptionsMenu alloc] initWithFrame:CGRectMake(-SCREEN_WIDTH, SIDE_POPUP_VIEW_YPOS,SCREEN_WIDTH, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS) withPropertyFileName:nil withIndex:0];
    optionsMenu.delegate = self;
    [self.view addSubview:optionsMenu];
}



#pragma mark - Add FeatureMenu View
-(void)addFeaturemenuToView
{
    // To set Add FeatureMenu For ParentTemplate5.
    featuresMenu = [[FeaturesMenu alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(recentSubView.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(recentSubView.frame)) withSelectedIndex:-1 fromView:-1 withSelectedCategory:0 andSetSubTemplates:false withParentIndex:-1];
    featuresMenu.delegate = self;
    
    NSArray *balanceBackGroundColor;
    
    if(![NSLocalizedStringFromTableInBundle(@"parent_template5_balance_layout_back_ground_color",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template5_balance_layout_back_ground_color"] && NSLocalizedStringFromTableInBundle(@"parent_template5_balance_layout_back_ground_color",propertyFile,[NSBundle mainBundle], nil).length != 0)
        balanceBackGroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_balance_layout_back_ground_color", propertyFile,[NSBundle mainBundle], nil)];
    
    if (balanceBackGroundColor)
        featuresMenu.backgroundColor  = [UIColor colorWithRed:[[balanceBackGroundColor objectAtIndex:0] floatValue] green:[[balanceBackGroundColor objectAtIndex:1] floatValue] blue:[[balanceBackGroundColor objectAtIndex:2] floatValue] alpha:1.0];
    [self.view addSubview:featuresMenu];
}


#pragma mark - Add PageHeaderView To ParenttTemplate5
-(void)addPageHeaderToView
{
    //PageHeader
    
    NSString *imageNameStr;
    NSString *labelStr;
    NSArray *titleLabelTextColor;
    NSString *textStyle;
    NSString *fontSize;
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template5_titlebar_image_visibility",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
        imageNameStr=HEADER_IMAGE_NAME;
    }
    if ([NSLocalizedStringFromTableInBundle(@"parent_template5_titlebar_label_visibility",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if(![NSLocalizedStringFromTableInBundle(@"parent_template5_titlebar_label_text",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template5_titlebar_label_text"] && NSLocalizedStringFromTableInBundle(@"parent_template5_titlebar_label_text",propertyFile,[NSBundle mainBundle], nil).length != 0)
            labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template5_titlebar_label_text",propertyFile,[NSBundle mainBundle], nil)];
        if ([NSLocalizedStringFromTableInBundle(@"parent_template5_titlebar_label_font_attributes_override",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            if (![NSLocalizedStringFromTableInBundle(@"parent_template5_titlebar_label_text_color",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template5_titlebar_label_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template5_titlebar_label_text_color",propertyFile,[NSBundle mainBundle], nil).length != 0)
                titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_titlebar_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
            
            else if (![NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template5_label_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil).length != 0)
                titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
            else
                titleLabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            // Properties for label Textstyle.
            
            if(![NSLocalizedStringFromTableInBundle(@"parent_template5_titlebar_label_text_style",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template5_titlebar_label_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template5_titlebar_label_text_style",propertyFile,[NSBundle mainBundle], nil).length != 0)
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_titlebar_label_text_style",propertyFile,[NSBundle mainBundle], nil);
            
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template5_label_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil).length != 0)
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            if(![NSLocalizedStringFromTableInBundle(@"parent_template5_titlebar_label_text_size",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template5_titlebar_label_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template5_titlebar_label_text_size",propertyFile,[NSBundle mainBundle], nil).length != 0)
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_titlebar_label_text_size",propertyFile,[NSBundle mainBundle], nil);
            
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template5_label_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil).length != 0)
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
        }
        else
        {
            //Default Properties for label textcolor
            if (![NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template5_label_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil).length != 0)
                titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
            else
                titleLabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            
            //Default Properties for label textStyle
            if(![NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template5_label_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil).length != 0)
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            if(![NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template5_label_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil).length != 0)
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
        }
    }
    // To set Add pageHeader For ParentTemplate5.
    pageHeader = [[PageHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64 ) withHeaderTitle:labelStr?labelStr:@""  withLeftbarBtn1Image_IconName:MENU_IMAGE_NAME withLeftbarBtn2Image_IconName:nil withHeaderButtonImage:imageNameStr  withRightbarBtn1Image_IconName:nil withRightbarBtn2Image_IconName:nil withRightbarBtn3Image_IconName:nil withTag:0];
    pageHeader.delegate = self;
    if (titleLabelTextColor)
        pageHeader.header_titleLabel.textColor = [UIColor colorWithRed:[[titleLabelTextColor objectAtIndex:0] floatValue] green:[[titleLabelTextColor objectAtIndex:1] floatValue] blue:[[titleLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
    
    if ([textStyle isEqualToString:TEXT_STYLE_0])
        pageHeader.header_titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_1])
        pageHeader.header_titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_2])
        pageHeader.header_titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
    
    else
        pageHeader.header_titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    [self.view addSubview:pageHeader];
}

#pragma mark - Add GridView To ParentTEmplate5
-(void)addGridViewToHomePage
{
    // To set Add GridView For ParentTemplate5.
    
    gridcollectionView = [[GridView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(recentSubView.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(recentSubView.frame)) withSelectedIndex:-1 fromView:-1 withSelectedCategory:0 andSetSubTemplates:false withParentIndex:-1];
    
    [gridcollectionView setBackgroundColor: [UIColor whiteColor]];
    gridcollectionView.delegate = self;
    [self.view addSubview:gridcollectionView];
}


#pragma mark - Tableview Data Source and Delegate methods
/**
 * This method is used to set add Tableview for ParentTemplate5.
 *@param type- Declare Tableview (Required Delegate and Data source methods).
 *Ref:  numberOfRowsInSection and cellForRowAtIndexPath.
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200.0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [recent_dataArray count];
}

/**
 * This method is used to set add Tableview for ParentTemplate5.
 *@param type- Declare Listview related UI(Labels,Images etc..base on Req).
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = [NSString stringWithFormat:@"%@%d",@"Cell",(int)indexPath.row];
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(5, 5, self.view.frame.size.width-20, 180)];
    backView.backgroundColor=[UIColor lightGrayColor];
    backView.layer.cornerRadius = 20.0;
    backView.layer.borderColor = [UIColor colorWithRed:110/255.0 green:25/255.0 blue:65/255.0 alpha:1.0].CGColor;
    //    [[UIColor brownColor] CGColor];
    backView.layer.borderWidth = 2.0;
    
    UILabel *popupTemplateLabelBorder = [[UILabel alloc] init];
    popupTemplateLabelBorder.frame = CGRectMake(2, 2, backView.frame.size.width-4 , backView.frame.size.height-4);
    popupTemplateLabelBorder.layer.cornerRadius = 20;
    popupTemplateLabelBorder.clipsToBounds = YES;
    popupTemplateLabelBorder.layer.borderWidth = 3;
    popupTemplateLabelBorder.layer.borderColor = [UIColor whiteColor].CGColor;
    popupTemplateLabelBorder.backgroundColor = [UIColor colorWithRed:227/255.0 green:227/255.0 blue:227/255.0 alpha:1.0];
    [backView addSubview:popupTemplateLabelBorder];
    
    [cell.contentView addSubview:backView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    Xposition=10;
    // Transaction type Label
    
    if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_visibility",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        recent_descriptionLabel = [[UILabel alloc] init];
        recent_descriptionLabel.frame = CGRectMake(10,10,backView.frame.size.width-20,30);
        [recent_descriptionLabel setTextAlignment:NSTextAlignmentCenter];
        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_param_type",propertyFile,[NSBundle mainBundle], nil) length] > 0) {
            recent_descriptionLabel.text = [[recent_dataArray objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_param_type",propertyFile,[NSBundle mainBundle], nil)]];
        }
        else
            recent_descriptionLabel.text =[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_text",propertyFile,[NSBundle mainBundle], nil)];
        
        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_text_font_attributes_override",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *label1_TextColor;
            if (NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_text_color",propertyFile,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
            else
                label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label1_TextColor)
                recent_descriptionLabel.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_text_style",propertyFile,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_text_style",propertyFile,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            
            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_text_size",propertyFile,[NSBundle mainBundle], nil))
                fontSize=NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_text_size",propertyFile,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                recent_descriptionLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                recent_descriptionLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                recent_descriptionLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                recent_descriptionLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label1_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
            else
                label1_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label1_TextColor)
                recent_descriptionLabel.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                recent_descriptionLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                recent_descriptionLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                recent_descriptionLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                recent_descriptionLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        recent_descriptionLabel.textColor = [UIColor blackColor];
        recent_descriptionLabel.numberOfLines=0;
        //        [recent_descriptionLabel sizeToFit];
        
        nextY_Position = (recent_descriptionLabel.frame.origin.y+recent_descriptionLabel.frame.size.height)+5;
        
        [backView addSubview:recent_descriptionLabel];
    }
    
    // Amount label
    
    UILabel *amountLabel = [[UILabel alloc] initWithFrame: CGRectMake(Xposition,nextY_Position,backView.frame.size.width/3,30)];
    
    [amountLabel setText:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:@"label_id_home_amount"]]];
    
    
    [amountLabel setTextColor:[UIColor blackColor]];
    [amountLabel setBackgroundColor:[UIColor clearColor]];
    [amountLabel setTextAlignment:NSTextAlignmentCenter];
    
    [backView addSubview:amountLabel];
    if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_visibility",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        recent_valueLabel = [[UILabel alloc] init];
        recent_valueLabel.backgroundColor = [UIColor whiteColor];
        recent_valueLabel.frame = CGRectMake(amountLabel.frame.origin.x+amountLabel.frame.size.width,(recent_descriptionLabel.frame.origin.y+recent_descriptionLabel.frame.size.height)+5,((backView.frame.size.width/3)*2)-25,25);
        [recent_valueLabel setTextAlignment:NSTextAlignmentCenter];
        
        // Label TextValue
        NSLocale *localeStr = [[NSLocale alloc] initWithLocaleIdentifier:[NSString stringWithFormat:@"%@_%@",NSLocalizedStringFromTableInBundle(@"application_amount_format_language_code",@"GeneralSettings",[NSBundle mainBundle], nil),NSLocalizedStringFromTableInBundle(@"application_amount_format_country_code",@"GeneralSettings",[NSBundle mainBundle], nil)]];
        
        NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
        [currencyStyle setLocale:localeStr];
        currencyStyle.numberStyle = NSNumberFormatterCurrencyStyle;
        // [currencyStyle setCurrencySymbol:@""];
        
        if (NSLocalizedStringFromTableInBundle(@"application_amount_decimal_places",@"GeneralSettings",[NSBundle mainBundle], nil)) {
            [currencyStyle setMaximumFractionDigits:[NSLocalizedStringFromTableInBundle(@"application_amount_decimal_places",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue]];
        }
        else{
            [currencyStyle setMaximumFractionDigits:0];
        }
        [currencyStyle setCurrencySymbol:@""];
        
        //Application Amount format
        [currencyStyle setCurrencyCode:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"application_amount_format",@"GeneralSettings",[NSBundle mainBundle], nil)]];
        
        NSNumber *amountStr;
        NSString *amountValueStr;
        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_param_type",propertyFile,[NSBundle mainBundle], nil) length] > 0)
        {
            amountValueStr=[[recent_dataArray objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_param_type",propertyFile,[NSBundle mainBundle], nil)]];
        }
        
        else
            amountValueStr =[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_text",propertyFile,[NSBundle mainBundle], nil)];
        
        amountStr=[NSNumber numberWithDouble:[amountValueStr doubleValue]];
        
        NSString *valueLable1 = [NSString stringWithFormat:@"%@ %@",currency,[currencyStyle stringFromNumber:amountStr]];
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
        {
            valueLable1 = [NSString stringWithFormat:@"%@ %@",[currencyStyle stringFromNumber:amountStr], currency];
        }
        recent_valueLabel.text=[NSString stringWithFormat:@"%@",valueLable1];
        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_view_balance_currency_text_font_attributes_override",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *label1_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_text_color",propertyFile,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_text_color",propertyFile,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
            else
                label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label1_TextColor)
                recent_valueLabel.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_text_style",propertyFile,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_text_style",propertyFile,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_text_size",propertyFile,[NSBundle mainBundle], nil))
                fontSize=NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_text_size",propertyFile,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                recent_valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                recent_valueLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                recent_valueLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                recent_valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label1_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
            else
                label1_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label1_TextColor)
                recent_valueLabel.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                recent_valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                recent_valueLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                recent_valueLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                recent_valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        
        //[recent_valueLabel sizeToFit];
        Xposition=Xposition+recent_valueLabel.intrinsicContentSize.width;
        nextY_Position = recent_valueLabel.frame.origin.y + recent_valueLabel.frame.size.height+5;
        recent_valueLabel.numberOfLines=0;
        recent_valueLabel.lineBreakMode=NSLineBreakByTruncatingTail;
        [backView addSubview:recent_valueLabel];
    }
    
    //    // Static label
    //    if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_visibility",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    //    {
    //        recentValueLabel1 = [[UILabel alloc] init];
    //        recentValueLabel1.frame = CGRectMake(Xposition,(recent_descriptionLabel.frame.origin.y+recent_descriptionLabel.frame.size.height)+5,20,25);
    //
    //        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text",propertyFile,[NSBundle mainBundle], nil) length] >0)
    //        {
    //            if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_param_type",propertyFile,[NSBundle mainBundle], nil) length] > 0)
    //            {
    //                if ([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_param_type",propertyFile,[NSBundle mainBundle], nil) compare:MINI_STATEMENT_STATIC_TEXT])
    //                {
    //
    //                    recentValueLabel1.text=[Localization languageSelectedStringForKey:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text",propertyFile,[NSBundle mainBundle], nil)]];
    //                }
    //                else
    //                    recentValueLabel1.text =[Localization languageSelectedStringForKey:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text",propertyFile,[NSBundle mainBundle], nil)]];
    //            }
    //        }
    //
    //        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_font_attributes_override",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    //        {
    //            // Properties for label TextColor.
    //            NSArray *label1_TextColor;
    //
    //            if (NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_color",propertyFile,[NSBundle mainBundle], nil))
    //                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_color",propertyFile,[NSBundle mainBundle], nil)];
    //            else if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
    //                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
    //            else
    //                label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
    //
    //            if (label1_TextColor)
    //                recentValueLabel1.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
    //
    //            // Properties for label Textstyle.
    //
    //            NSString *textStyle;
    //            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_style",propertyFile,[NSBundle mainBundle], nil))
    //                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_style",propertyFile,[NSBundle mainBundle], nil);
    //
    //            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
    //                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
    //            else
    //                textStyle = application_default_text_style;
    //
    //
    //            // Properties for label Font size.
    //
    //            NSString *fontSize;
    //            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_size",propertyFile,[NSBundle mainBundle], nil))
    //                fontSize=NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_size",propertyFile,[NSBundle mainBundle], nil);
    //
    //            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
    //                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
    //            else
    //                fontSize = application_default_text_size;
    //
    //            if ([textStyle isEqualToString:TEXT_STYLE_0])
    //                recentValueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    //
    //            else if ([textStyle isEqualToString:TEXT_STYLE_1])
    //                recentValueLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
    //
    //            else if ([textStyle isEqualToString:TEXT_STYLE_2])
    //                recentValueLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
    //
    //            else
    //                recentValueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    //        }
    //        else
    //        {
    //            //Default Properties for label textcolor
    //
    //            NSArray *label1_TextColor ;
    //            if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
    //                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
    //            else
    //                label1_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
    //            if (label1_TextColor)
    //                recentValueLabel1.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
    //
    //            //Default Properties for label textStyle
    //
    //            NSString *textStyle;
    //            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
    //                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
    //            else
    //                textStyle = application_default_text_style;
    //
    //            //Default Properties for label fontSize
    //
    //            NSString *fontSize;
    //            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
    //                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
    //            else
    //                fontSize = application_default_text_size;
    //
    //            if ([textStyle isEqualToString:TEXT_STYLE_0])
    //                recentValueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    //
    //            else if ([textStyle isEqualToString:TEXT_STYLE_1])
    //                recentValueLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
    //
    //            else if ([textStyle isEqualToString:TEXT_STYLE_2])
    //                recentValueLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
    //
    //            else
    //                recentValueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    //        }
    //
    //        [recentValueLabel1 sizeToFit];
    //        Xposition = Xposition+recentValueLabel1.intrinsicContentSize.width;
    //        recentValueLabel1.numberOfLines=0;
    //        [backView addSubview:recentValueLabel1];
    //    }
    
    // Date label
    
    UILabel *dateLabel = [[UILabel alloc] initWithFrame: CGRectMake(10,80,backView.frame.size.width/3,30)];
    ;
    [dateLabel setText:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:@"label_id_home_date"]]];
    [dateLabel setTextColor:[UIColor blackColor]];
    [dateLabel setBackgroundColor:[UIColor clearColor]];
    [dateLabel setTextAlignment:NSTextAlignmentCenter];
    
    [backView addSubview:dateLabel];
    
    // Date Label.
    if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label4_visibility",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        recentValueLabel2 = [[UILabel alloc] init];
        [recentValueLabel2 setBackgroundColor:[UIColor whiteColor]];
        recentValueLabel2.frame = CGRectMake(dateLabel.frame.origin.x+dateLabel.frame.size.width,80,(((backView.frame.size.width/3)*2)-25),30);
        [recentValueLabel2 setTextAlignment:NSTextAlignmentCenter];
        
        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_param_type",propertyFile,[NSBundle mainBundle], nil) length] > 0)
        {
            //            recentValueLabel2.text=[[recent_dataArray objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label4_param_type",propertyFile,[NSBundle mainBundle], nil)]];
            NSString *str = [[recent_dataArray objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label4_param_type",propertyFile,[NSBundle mainBundle], nil)]];
            NSArray *timeArr = [str componentsSeparatedByString:@" "];
            recentValueLabel2.text = [timeArr objectAtIndex:0];
            
        }
        else
            recentValueLabel2.text =[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label4_text",propertyFile,[NSBundle mainBundle], nil)];
        
        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label4_text_font_attributes_override",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *label1_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_color",propertyFile,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_color",propertyFile,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
            else
                label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label1_TextColor)
                recentValueLabel2.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_style",propertyFile,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_style",propertyFile,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_size",propertyFile,[NSBundle mainBundle], nil))
                fontSize=NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_size",propertyFile,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                recentValueLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                recentValueLabel2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                recentValueLabel2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                recentValueLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label1_TextColor;
            if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
            else
                label1_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label1_TextColor)
                recentValueLabel2.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                recentValueLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                recentValueLabel2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                recentValueLabel2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                recentValueLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        Xposition=recentValueLabel2.intrinsicContentSize.width+2;
        recentValueLabel2.numberOfLines=2;
        //        [recentValueLabel2 sizeToFit];
        [backView addSubview:recentValueLabel2];
    }
    
    
    // Time label
    
    UILabel *timeLabel = [[UILabel alloc] initWithFrame: CGRectMake(10,120,backView.frame.size.width/3,30)];
    
    [timeLabel setText:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:@"label_id_home_time"]]];
    //    [timeLabel setText:@"Time"];
    [timeLabel setTextColor:[UIColor blackColor]];
    [timeLabel setTextAlignment:NSTextAlignmentCenter];
    
    [timeLabel setBackgroundColor:[UIColor clearColor]];
    [backView addSubview:timeLabel];
    
    // Time Label.
    if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label4_visibility",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        recentValueLabel3 = [[UILabel alloc] init];
        [recentValueLabel3 setBackgroundColor:[UIColor whiteColor]];
        recentValueLabel3.frame = CGRectMake(timeLabel.frame.origin.x+timeLabel.frame.size.width,120,(((backView.frame.size.width/3)*2)-25),30);
        [recentValueLabel3 setTextAlignment:NSTextAlignmentCenter];
        
        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_param_type",propertyFile,[NSBundle mainBundle], nil) length] > 0)
        {
            NSString *str = [[recent_dataArray objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label4_param_type",propertyFile,[NSBundle mainBundle], nil)]];
            NSArray *timeArr = [str componentsSeparatedByString:@" "];
            //            recentValueLabel3.text=[[recent_dataArray objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label4_param_type",propertyFile,[NSBundle mainBundle], nil)]];
            recentValueLabel3.text = [timeArr objectAtIndex:1];
            NSLog(@"%@",recentValueLabel3.text);
        }
        else
            recentValueLabel3.text =[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label4_text",propertyFile,[NSBundle mainBundle], nil)];
        NSLog(@"%@",recentValueLabel3.text);
        
        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label4_text_font_attributes_override",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *label1_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_color",propertyFile,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_color",propertyFile,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
            else
                label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label1_TextColor)
                recentValueLabel3.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_style",propertyFile,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_style",propertyFile,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_size",propertyFile,[NSBundle mainBundle], nil))
                fontSize=NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_size",propertyFile,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                recentValueLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                recentValueLabel3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                recentValueLabel3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                recentValueLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label1_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
            else
                label1_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label1_TextColor)
                recentValueLabel2.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                recentValueLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                recentValueLabel3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                recentValueLabel3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                recentValueLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        Xposition=recentValueLabel3.intrinsicContentSize.width+2;
        recentValueLabel3.numberOfLines=2;
        //        [recentValueLabel3 sizeToFit];
        [backView addSubview:recentValueLabel3];
        if([propertyFile isEqualToString:@"LaunchPagePT5_T1"]){
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
            {
                dateLabel.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
                amountLabel.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
                timeLabel.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
                recent_descriptionLabel.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
            }
        }
    }
//    NSLog(@"cell width in pt5: %f", cell.frame.size.width);
    return cell;
}

//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell *cell = nil;
//    NSString *cellIdentifier = [NSString stringWithFormat:@"%@%d",@"Cell",(int)indexPath.row];
//    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//    cell.backgroundColor=[UIColor clearColor];
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    Xposition=10;
//    // Transaction type Label
//
//    if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_visibility",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
//    {
//        recent_descriptionLabel = [[UILabel alloc] init];
//        recent_descriptionLabel.frame = CGRectMake(Xposition,10,recentTableView.frame.size.width-20,30);
//
//
//        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_param_type",propertyFile,[NSBundle mainBundle], nil) length] > 0) {
//            recent_descriptionLabel.text = [[recent_dataArray objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_param_type",propertyFile,[NSBundle mainBundle], nil)]];
//        }
//        else
//            recent_descriptionLabel.text =[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_text",propertyFile,[NSBundle mainBundle], nil)];
//
//        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_text_font_attributes_override",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
//        {
//            // Properties for label TextColor.
//            NSArray *label1_TextColor;
//            if (NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_text_color",propertyFile,[NSBundle mainBundle], nil))
//                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
//            else if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
//                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
//            else
//                label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
//
//            if (label1_TextColor)
//                recent_descriptionLabel.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
//
//            // Properties for label Textstyle.
//
//            NSString *textStyle;
//            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_text_style",propertyFile,[NSBundle mainBundle], nil))
//                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_text_style",propertyFile,[NSBundle mainBundle], nil);
//
//            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
//                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
//            else
//                textStyle = application_default_text_style;
//
//
//            // Properties for label Font size.
//
//            NSString *fontSize;
//
//            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_text_size",propertyFile,[NSBundle mainBundle], nil))
//                fontSize=NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label_text_size",propertyFile,[NSBundle mainBundle], nil);
//
//            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
//                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
//            else
//                fontSize = application_default_text_size;
//
//            if ([textStyle isEqualToString:TEXT_STYLE_0])
//                recent_descriptionLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
//
//            else if ([textStyle isEqualToString:TEXT_STYLE_1])
//                recent_descriptionLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
//
//            else if ([textStyle isEqualToString:TEXT_STYLE_2])
//                recent_descriptionLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
//
//            else
//                recent_descriptionLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
//        }
//        else
//        {
//            //Default Properties for label textcolor
//
//            NSArray *label1_TextColor ;
//            if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
//                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
//            else
//                label1_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
//            if (label1_TextColor)
//                recent_descriptionLabel.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
//
//
//            //Default Properties for label textStyle
//
//            NSString *textStyle;
//            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
//                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
//            else
//                textStyle = application_default_text_style;
//
//            //Default Properties for label fontSize
//
//            NSString *fontSize;
//            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
//                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
//            else
//                fontSize = application_default_text_size;
//
//            if ([textStyle isEqualToString:TEXT_STYLE_0])
//                recent_descriptionLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
//
//            else if ([textStyle isEqualToString:TEXT_STYLE_1])
//                recent_descriptionLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
//
//            else if ([textStyle isEqualToString:TEXT_STYLE_2])
//                recent_descriptionLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
//
//            else
//                recent_descriptionLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
//        }
//        recent_descriptionLabel.numberOfLines=0;
//        [recent_descriptionLabel sizeToFit];
//
//        nextY_Position = (recent_descriptionLabel.frame.origin.y+recent_descriptionLabel.frame.size.height)+5;
//
//        [cell.contentView addSubview:recent_descriptionLabel];
//    }
//
//    // Amount label
//    if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_visibility",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
//    {
//        recent_valueLabel = [[UILabel alloc] init];
//        recent_valueLabel.frame = CGRectMake(Xposition,(recent_descriptionLabel.frame.origin.y+recent_descriptionLabel.frame.size.height)+5,((recentTableView.frame.size.width/3)-20),25);
//
//        // Label TextValue
//        NSLocale *localeStr = [[NSLocale alloc] initWithLocaleIdentifier:[NSString stringWithFormat:@"%@_%@",NSLocalizedStringFromTableInBundle(@"application_amount_format_language_code",@"GeneralSettings",[NSBundle mainBundle], nil),NSLocalizedStringFromTableInBundle(@"application_amount_format_country_code",@"GeneralSettings",[NSBundle mainBundle], nil)]];
//
//        NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
//        [currencyStyle setLocale:localeStr];
//        currencyStyle.numberStyle = NSNumberFormatterCurrencyStyle;
//        // [currencyStyle setCurrencySymbol:@""];
//
//        if (NSLocalizedStringFromTableInBundle(@"application_amount_decimal_places",@"GeneralSettings",[NSBundle mainBundle], nil)) {
//            [currencyStyle setMaximumFractionDigits:[NSLocalizedStringFromTableInBundle(@"application_amount_decimal_places",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue]];
//        }
//        else{
//            [currencyStyle setMaximumFractionDigits:0];
//        }
//        [currencyStyle setCurrencySymbol:@""];
//
//        //Application Amount format
//        [currencyStyle setCurrencyCode:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"application_amount_format",@"GeneralSettings",[NSBundle mainBundle], nil)]];
//
//        NSNumber *amountStr;
//        NSString *amountValueStr;
//        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_param_type",propertyFile,[NSBundle mainBundle], nil) length] > 0)
//        {
//            amountValueStr=[[recent_dataArray objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_param_type",propertyFile,[NSBundle mainBundle], nil)]];
//        }
//
//        else
//            amountValueStr =[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_text",propertyFile,[NSBundle mainBundle], nil)];
//
//
//        amountStr=[NSNumber numberWithDouble:[amountValueStr doubleValue]];
//
//        NSString *valueLable1 = [NSString stringWithFormat:@"%@ %@",currency,[currencyStyle stringFromNumber:amountStr]];
//
//        recent_valueLabel.text=[NSString stringWithFormat:@"%@",valueLable1];
//        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_view_balance_currency_text_font_attributes_override",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
//        {
//            // Properties for label TextColor.
//            NSArray *label1_TextColor;
//
//            if (NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_text_color",propertyFile,[NSBundle mainBundle], nil))
//                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_text_color",propertyFile,[NSBundle mainBundle], nil)];
//            else if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
//                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
//            else
//                label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
//
//            if (label1_TextColor)
//                recent_valueLabel.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
//
//            // Properties for label Textstyle.
//
//            NSString *textStyle;
//            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_text_style",propertyFile,[NSBundle mainBundle], nil))
//                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_text_style",propertyFile,[NSBundle mainBundle], nil);
//
//            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
//                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
//            else
//                textStyle = application_default_text_style;
//
//
//            // Properties for label Font size.
//
//            NSString *fontSize;
//            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_text_size",propertyFile,[NSBundle mainBundle], nil))
//                fontSize=NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label2_text_size",propertyFile,[NSBundle mainBundle], nil);
//
//            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
//                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
//            else
//                fontSize = application_default_text_size;
//
//            if ([textStyle isEqualToString:TEXT_STYLE_0])
//                recent_valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
//
//            else if ([textStyle isEqualToString:TEXT_STYLE_1])
//                recent_valueLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
//
//            else if ([textStyle isEqualToString:TEXT_STYLE_2])
//                recent_valueLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
//
//            else
//                recent_valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
//        }
//        else
//        {
//            //Default Properties for label textcolor
//
//            NSArray *label1_TextColor ;
//            if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
//                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
//            else
//                label1_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
//            if (label1_TextColor)
//                recent_valueLabel.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
//
//            //Default Properties for label textStyle
//
//            NSString *textStyle;
//            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
//                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
//            else
//                textStyle = application_default_text_style;
//
//            //Default Properties for label fontSize
//
//            NSString *fontSize;
//            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
//                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
//            else
//                fontSize = application_default_text_size;
//
//            if ([textStyle isEqualToString:TEXT_STYLE_0])
//                recent_valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
//
//            else if ([textStyle isEqualToString:TEXT_STYLE_1])
//                recent_valueLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
//
//            else if ([textStyle isEqualToString:TEXT_STYLE_2])
//                recent_valueLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
//
//            else
//                recent_valueLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
//        }
//
//        [recent_valueLabel sizeToFit];
//        Xposition=Xposition+recent_valueLabel.intrinsicContentSize.width;
//        nextY_Position = recent_valueLabel.frame.origin.y + recent_valueLabel.frame.size.height+5;
//        recent_valueLabel.numberOfLines=0;
//        recent_valueLabel.lineBreakMode=NSLineBreakByTruncatingTail;
//        [cell.contentView addSubview:recent_valueLabel];
//    }
//
//    // Static label
//    if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_visibility",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
//    {
//        recentValueLabel1 = [[UILabel alloc] init];
//        recentValueLabel1.frame = CGRectMake(Xposition,(recent_descriptionLabel.frame.origin.y+recent_descriptionLabel.frame.size.height)+5,20,25);
//
//        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text",propertyFile,[NSBundle mainBundle], nil) length] >0)
//        {
//            if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_param_type",propertyFile,[NSBundle mainBundle], nil) length] > 0)
//            {
//                if ([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_param_type",propertyFile,[NSBundle mainBundle], nil) compare:MINI_STATEMENT_STATIC_TEXT])
//                {
//
//                    recentValueLabel1.text=[Localization languageSelectedStringForKey:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text",propertyFile,[NSBundle mainBundle], nil)]];
//                }
//                else
//                    recentValueLabel1.text =[Localization languageSelectedStringForKey:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text",propertyFile,[NSBundle mainBundle], nil)]];
//            }
//        }
//
//        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_font_attributes_override",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
//        {
//            // Properties for label TextColor.
//            NSArray *label1_TextColor;
//
//            if (NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_color",propertyFile,[NSBundle mainBundle], nil))
//                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_color",propertyFile,[NSBundle mainBundle], nil)];
//            else if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
//                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
//            else
//                label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
//
//            if (label1_TextColor)
//                recentValueLabel1.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
//
//            // Properties for label Textstyle.
//
//            NSString *textStyle;
//            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_style",propertyFile,[NSBundle mainBundle], nil))
//                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_style",propertyFile,[NSBundle mainBundle], nil);
//
//            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
//                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
//            else
//                textStyle = application_default_text_style;
//
//
//            // Properties for label Font size.
//
//            NSString *fontSize;
//            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_size",propertyFile,[NSBundle mainBundle], nil))
//                fontSize=NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_size",propertyFile,[NSBundle mainBundle], nil);
//
//            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
//                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
//            else
//                fontSize = application_default_text_size;
//
//            if ([textStyle isEqualToString:TEXT_STYLE_0])
//                recentValueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
//
//            else if ([textStyle isEqualToString:TEXT_STYLE_1])
//                recentValueLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
//
//            else if ([textStyle isEqualToString:TEXT_STYLE_2])
//                recentValueLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
//
//            else
//                recentValueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
//        }
//        else
//        {
//            //Default Properties for label textcolor
//
//            NSArray *label1_TextColor ;
//            if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
//                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
//            else
//                label1_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
//            if (label1_TextColor)
//                recentValueLabel1.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
//
//            //Default Properties for label textStyle
//
//            NSString *textStyle;
//            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
//                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
//            else
//                textStyle = application_default_text_style;
//
//            //Default Properties for label fontSize
//
//            NSString *fontSize;
//            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
//                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
//            else
//                fontSize = application_default_text_size;
//
//            if ([textStyle isEqualToString:TEXT_STYLE_0])
//                recentValueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
//
//            else if ([textStyle isEqualToString:TEXT_STYLE_1])
//                recentValueLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
//
//            else if ([textStyle isEqualToString:TEXT_STYLE_2])
//                recentValueLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
//
//            else
//                recentValueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
//        }
//
//        [recentValueLabel1 sizeToFit];
//        Xposition = Xposition+recentValueLabel1.intrinsicContentSize.width;
//        recentValueLabel1.numberOfLines=0;
//        [cell.contentView addSubview:recentValueLabel1];
//    }
//
//    // Date Label.
//    if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label4_visibility",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
//    {
//        recentValueLabel2 = [[UILabel alloc] init];
//        recentValueLabel2.frame = CGRectMake(Xposition,(recent_descriptionLabel.frame.origin.y+recent_descriptionLabel.frame.size.height)+5,((recentTableView.frame.size.width/1.5)-20),30);
//
//        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_param_type",propertyFile,[NSBundle mainBundle], nil) length] > 0)
//        {
//            recentValueLabel2.text=[[recent_dataArray objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label4_param_type",propertyFile,[NSBundle mainBundle], nil)]];
//        }
//        else
//            recentValueLabel2.text =[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label4_text",propertyFile,[NSBundle mainBundle], nil)];
//
//        if([NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label4_text_font_attributes_override",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
//        {
//            // Properties for label TextColor.
//            NSArray *label1_TextColor;
//
//            if (NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_color",propertyFile,[NSBundle mainBundle], nil))
//                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_color",propertyFile,[NSBundle mainBundle], nil)];
//            else if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
//                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
//            else
//                label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
//
//            if (label1_TextColor)
//                recentValueLabel2.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
//
//            // Properties for label Textstyle.
//
//            NSString *textStyle;
//            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_style",propertyFile,[NSBundle mainBundle], nil))
//                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_style",propertyFile,[NSBundle mainBundle], nil);
//
//            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
//                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
//            else
//                textStyle = application_default_text_style;
//
//
//            // Properties for label Font size.
//
//            NSString *fontSize;
//            if(NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_size",propertyFile,[NSBundle mainBundle], nil))
//                fontSize=NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_list_item_label3_text_size",propertyFile,[NSBundle mainBundle], nil);
//
//            else if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
//                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
//            else
//                fontSize = application_default_text_size;
//
//            if ([textStyle isEqualToString:TEXT_STYLE_0])
//                recentValueLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
//
//            else if ([textStyle isEqualToString:TEXT_STYLE_1])
//                recentValueLabel2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
//
//            else if ([textStyle isEqualToString:TEXT_STYLE_2])
//                recentValueLabel2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
//
//            else
//                recentValueLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
//        }
//        else
//        {
//            //Default Properties for label textcolor
//
//            NSArray *label1_TextColor ;
//            if (NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil))
//                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
//            else
//                label1_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
//            if (label1_TextColor)
//                recentValueLabel2.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
//
//            //Default Properties for label textStyle
//
//            NSString *textStyle;
//            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil))
//                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_style",propertyFile,[NSBundle mainBundle], nil);
//            else
//                textStyle = application_default_text_style;
//
//            //Default Properties for label fontSize
//
//            NSString *fontSize;
//            if(NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil))
//                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template5_label_text_size",propertyFile,[NSBundle mainBundle], nil);
//            else
//                fontSize = application_default_text_size;
//
//            if ([textStyle isEqualToString:TEXT_STYLE_0])
//                recentValueLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
//
//            else if ([textStyle isEqualToString:TEXT_STYLE_1])
//                recentValueLabel2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
//
//            else if ([textStyle isEqualToString:TEXT_STYLE_2])
//                recentValueLabel2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
//
//            else
//                recentValueLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
//
//        }
//       Xposition=recentValueLabel2.intrinsicContentSize.width+2;
//        recentValueLabel2.numberOfLines=2;
//        [recentValueLabel2 sizeToFit];
//        [cell.contentView addSubview:recentValueLabel2];
//    }
//
//    return cell;
//}


#pragma mark -Feature menu button Action
/**
 * This method is used to set Delegate Method of FeatureMenu For ParentTemplate5.
 *@Param type - Features(Send,Bank etc)
 *Based on User selected Feature will be shown Next View with Highilighted.
 *  Feature Name with Next Template and NextTemplate Property file.
 */
-(void)featuresMenuButtonAction:(int)tag
{
    NSString *nextTemplate;
    NSString *mainTemplate;
    
    if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"0"])
    {
        //Changed Impln
        mainTemplate = [[[Template getNextFeatureTemplateWithIndex:tag] objectForKey:@"templates"] objectAtIndex:tag-1];
        nextTemplate = [[[Template getNextFeatureTemplateWithIndex:tag] objectForKey:SUB_TEMPLATES_PROPERTIES] objectAtIndex:tag-1];
        
    }
    else if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"1"])
    {
        [[NSUserDefaults standardUserDefaults] setInteger:tag forKey:@"topLevelNavigation"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        mainTemplate = [[[Template getNextFeatureTemplateWithIndex:tag] objectForKey:CATEGORIES_NAMES] objectAtIndex:tag-1];
        NSLog(@"Category Template Property file names : %@",[[Template getNextFeatureTemplateWithIndex:tag] objectForKey:KEY_CATEGORY_TEMPLATE_PROPERTY_FILES]);
        NSLog(@"Tag Value : %d",tag);
        nextTemplate = [[[Template getNextFeatureTemplateWithIndex:tag] objectForKey:@"category_template_property_files"] objectAtIndex:tag-1];
    }
    
    if (![nextTemplate isEqualToString:@""])
    {
        //Changed Impln
        Class myclass = NSClassFromString(mainTemplate);
        id obj = nil;
        
        [[NSUserDefaults standardUserDefaults] setInteger:tag forKey:@"selected_index"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
        {
            obj = [[myclass alloc] initWithNibName:mainTemplate bundle:nil withSelectedIndex:tag fromView:0 withFromView:nil withPropertyFile:nextTemplate withProcessorCode:nil dataArray:nil dataDictionary:nil];
            [self.navigationController pushViewController:(UIViewController*)obj animated:NO];
        }
        else if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
        {
            obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplate andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:tag withDataArray:nil withSubIndex:0];
            [self.view addSubview:(UIView *)obj];
        }
    }
}

#pragma mark - Option Menu delegate
/**
 * This method is used to set Delegate Method of Option menu For ParentTemplate5.
 *@Param type - Options menu (Terms And conditions,Set language etc)
 *Based on User selected options menu feature will be shown Next View with Highilighted.
 *  Options menu feature Name with Next Template and NextTemplate Property file.
 */
-(void)optionsMenu_didSelectRowAtIndexPath:(NSInteger)indexpath withPropertyFile:(NSString *)propertyFileName nextTemplate:(NSString *)nextTemplateName andProcessorCode:(NSString *)processorCode
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    optionsMenu.frame = CGRectMake(-SCREEN_WIDTH, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS);
    bgLbl.alpha=0.0;
    [UIView commitAnimations];
    if (![propertyFileName isEqualToString:@""] && ![nextTemplateName isEqualToString:@""])
    {
        Class myclass = NSClassFromString(nextTemplateName);
        
        id obj = nil;
        if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
        {
            obj = [[myclass alloc] initWithNibName:nextTemplateName bundle:nil withSelectedIndex:(int)indexpath fromView:0 withFromView:propertyFile withPropertyFile:propertyFileName withProcessorCode:processorCode dataArray:nil dataDictionary:nil];
            [self.navigationController pushViewController:(UIViewController*)obj animated:NO];
        }
        else if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
        {
            obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:propertyFileName andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
            [self.view addSubview:(UIView *)obj];
        }
    }
}

#pragma mark - Options menu Delegate Method.
/**
 * This method is used to set Delegate Method used for Hiding  Option menu From View.
 */
-(void)optionsMenu_tapGestureCalled
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    optionsMenu.frame = CGRectMake(-SCREEN_WIDTH, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS);
    bgLbl.alpha=0.0;
    [UIView commitAnimations];
}
-(void)gridView_didSelectRowAtIndexPath:(NSInteger)indexpath
{
    NSString *nextTemplate;
    NSString *mainTemplate;
    
    if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"0"])
    {
        //Changed Impln
        mainTemplate = [[[Template getNextFeatureTemplateWithIndex:(int)indexpath] objectForKey:@"templates"] objectAtIndex:indexpath];
        nextTemplate = [[[Template getNextFeatureTemplateWithIndex:(int)indexpath] objectForKey:SUB_TEMPLATES_PROPERTIES] objectAtIndex:indexpath];
        
    }
    else if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"1"])
    {
        [[NSUserDefaults standardUserDefaults] setInteger:(int)indexpath forKey:@"topLevelNavigation"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        mainTemplate = [[[Template getNextFeatureTemplateWithIndex:(int)indexpath] objectForKey:CATEGORIES_NAMES] objectAtIndex:indexpath];
        NSLog(@"Category Template Property file names : %@",[[Template getNextFeatureTemplateWithIndex:(int)indexpath] objectForKey:KEY_CATEGORY_TEMPLATE_PROPERTY_FILES]);
        NSLog(@"Tag Value : %ld",(long)indexpath);
        nextTemplate = [[[Template getNextFeatureTemplateWithIndex:(int)indexpath] objectForKey:@"category_template_property_files"] objectAtIndex:indexpath];
    }
    
    if (![nextTemplate isEqualToString:@""])
    {
        //Changed Impln
        Class myclass = NSClassFromString(mainTemplate);
        id obj = nil;
        
        [[NSUserDefaults standardUserDefaults] setInteger:(int)indexpath forKey:@"selected_index"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
        {
            obj = [[myclass alloc] initWithNibName:mainTemplate bundle:nil withSelectedIndex:(int)indexpath fromView:0 withFromView:nil withPropertyFile:nextTemplate withProcessorCode:nil dataArray:nil dataDictionary:nil];
            [self.navigationController pushViewController:(UIViewController*)obj animated:NO];
        }
        else if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
        {
            obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplate andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:(int)indexpath withDataArray:nil withSubIndex:0];
            [self.view addSubview:(UIView *)obj];
        }
    }
}
#pragma mark - Menu button Action.
/**
 * This method is used to set Delegate Method of PageHeaderView Button Action(Pop to Previous view).
 */
-(void)menuBtn_Action
{
    [self.view endEditing:YES];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    if (optionsMenu.frame.origin.x == 0)
    {
        optionsMenu.frame = CGRectMake(-SCREEN_WIDTH, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS);
        bgLbl.alpha=0.0;
    }
    else
    {
        optionsMenu.frame = CGRectMake(0, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS);
        bgLbl.alpha=0.4;
    }
    [optionsMenu  setHidden:NO];
    [UIView commitAnimations];
}

#pragma mark - Button Actions For recent and balance.
/**
 * This method is used to set Button action for refresh Icon.
 */
-(void)refreshAll:(id)sender
{
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    
    NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"parent_template5_balance_refresh_image_next_template_properties_file",propertyFile,[NSBundle mainBundle], nil);
    NSLog(@"nextTemplateProperty: %@",nextTemplateProperty);
    
    NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"parent_template5_balance_refresh_image_next_template",propertyFile,[NSBundle mainBundle], nil);
    NSLog(@"nextTemplate: %@",nextTemplate);
    
    // Popup 1 calling
    if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
    {
        Class myclass = NSClassFromString(nextTemplate);
        id obj = [[myclass alloc] initWithFrame:CGRectMake(0,140,SCREEN_WIDTH,SCREEN_HEIGHT-200) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:dataDict withProcessorCode:nil withTag:0 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
        [self.view addSubview:(UIView *)obj];
    }
}

/**
 * This method is used to set Button action for refresh Icon for getting User balance.
 */
-(void)refreshBalance:(id)sender
{
    fromScreen = 1;
    NSMutableDictionary *localDataDictionary = [[NSMutableDictionary alloc] init];
    NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"parent_template5_balance_refresh_image_next_template_properties_file",propertyFile,[NSBundle mainBundle], nil);
    NSLog(@"nextTemplateProperty: %@,%@",nextTemplateProperty,propertyFile);
    NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"parent_template5_balance_refresh_image_next_template",propertyFile,[NSBundle mainBundle], nil);
    NSLog(@"nextTemplate: %@",nextTemplate);
    
    // Popup 1 calling
//    if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
//    {
//        Class myclass = NSClassFromString(nextTemplate);
//        id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT)  withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:dataDict withProcessorCode:nil withTag:-3 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
//        [self.view addSubview:(UIView *)obj];
//    }
    
    
    alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    NSString *validation_Type = NSLocalizedStringFromTableInBundle(@"application_validation_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    NSString *actionType =  NSLocalizedStringFromTableInBundle(@"parent_template5_balance_refresh_image_action_type",propertyFile,[NSBundle mainBundle], nil);
    NSString *apiParamType = NSLocalizedStringFromTableInBundle(@"parent_template5_balance_refresh_image_web_service_fee_parameter_name",propertyFile,[NSBundle mainBundle], nil);
    
    NSString *data = NSLocalizedStringFromTableInBundle(@"parent_template5_balance_refresh_image_web_service_api_name",propertyFile,[NSBundle mainBundle], nil);
    NSString *processor_Code = [Template getProcessorCodeWithData:data];
    NSString *transactionType = [Template getTransactionCodeWithData:data];
    
    if (transactionType) {
        [localDataDictionary setObject:transactionType forKey:PARAMETER13];
    }
    
    if (processor_Code){
        [localDataDictionary setObject:processor_Code forKey:PARAMETER15];
    }
    Template *obj = [Template initWithactionType:actionType nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:propertyFile ProcessorCode:processor_Code Dictionary:localDataDictionary contentArray:nil alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:apiParamType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:transactionType withCurrentClass:PARENT_TEMPLATE5];
    obj.selector = NULL;
    obj.target = self;
    [[NSNotificationCenter defaultCenter] postNotificationName:PPT1_BUTTON2_ACTION object:obj];
    
}
/**
 * This method is used to set Button action for refresh Icon for getting User Recent transactions.
 */
-(void)refreshRecentTransactions:(id)sender
{
    fromScreen = 2;
    
    NSMutableDictionary *localDataDictionary = [[NSMutableDictionary alloc] init];
    NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_refresh_image_next_template_properties_file",propertyFile,[NSBundle mainBundle], nil);
    NSLog(@"nextTemplateProperty: %@",nextTemplateProperty);
    NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_refresh_image_next_template",propertyFile,[NSBundle mainBundle], nil);
    NSLog(@"nextTemplate: %@",nextTemplate);
    
    // Popup 1 calling
//    if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
//    {
//        Class myclass = NSClassFromString(nextTemplate);
//        id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:dataDict withProcessorCode:nil withTag:-3 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
//        [self.view addSubview:(UIView *)obj];
//    }
 
    alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
   NSString *validation_Type = NSLocalizedStringFromTableInBundle(@"application_validation_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    NSString *actionType =  NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_refresh_image_action_type",propertyFile,[NSBundle mainBundle], nil);
    NSString *apiParamType = NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_refresh_image_web_service_fee_parameter_name",propertyFile,[NSBundle mainBundle], nil);
    
    NSString *data = NSLocalizedStringFromTableInBundle(@"parent_template5_recent_transaction_refresh_image_web_service_api_name",propertyFile,[NSBundle mainBundle], nil);
    NSString *processor_Code = [Template getProcessorCodeWithData:data];
    NSString *transactionType = [Template getTransactionCodeWithData:data];
    
    if (transactionType) {
        [localDataDictionary setObject:transactionType forKey:PARAMETER13];
    }
    
    if (processor_Code) {
        [localDataDictionary setObject:processor_Code forKey:PARAMETER15];
    }
    
    Template *obj = [Template initWithactionType:actionType nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:propertyFile ProcessorCode:processor_Code Dictionary:localDataDictionary contentArray:nil alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:apiParamType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:transactionType withCurrentClass:PARENT_TEMPLATE5];
    obj.selector = NULL;
    obj.target = self;
    [[NSNotificationCenter defaultCenter] postNotificationName:PPT1_BUTTON2_ACTION object:obj];
    
    
    
}

#pragma mark - Updated Time Balance
/**
 * This method is used to get the time difference between Balance Updated.
 *@param type - varies Time diff in (Seconds,Minutes etc...)
 */

-(NSString *)getTimeDifferencebetweenDateForBalance
{
    NSString *resultStr=nil;
    
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [reachability  currentReachabilityStatus];
    
    NSInteger secondsDiff;
    
    if([profileID isEqualToString:@"SMS"] || (netStatus == NotReachable))
    {
        NSDate *myDate = [NSDate date];
        [[NSUserDefaults standardUserDefaults] setObject:myDate forKey:@"balanceDate"];
        resultStr = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_last_updated_not_yet", nil)]];
    }
    else
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"balanceDate"])
        {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"application_date_and_time_format",@"GeneralSettings",[NSBundle mainBundle], nil)]];
            
            // Way 2
            secondsDiff = [[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults] objectForKey:@"balanceDate"]];
            
            double minutes = secondsDiff / 60;
            double hours = minutes / 60;
            double seconds = secondsDiff;
            double days = minutes / 1440;
            if (seconds<=59)
            {
                resultStr=[NSString stringWithFormat:@"%@ %.0f %@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_last_updated_on", nil)],seconds,[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_seconds_ago", nil)]];
            }
            else if (minutes<=59)
            {
                resultStr=[NSString stringWithFormat:@"%@ %.0f %@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_last_updated_on", nil)],minutes,[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_minutes_ago", nil)]];
            }
            else if (hours<=23)
            {
                resultStr=[NSString stringWithFormat:@"%@ %.0f %@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_last_updated_on", nil)],hours,[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_hours_ago", nil)]];
            }
            else if (days<=7)
            {
                
                resultStr=[NSString stringWithFormat:@"%@ %.0f %@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_last_updated_on", nil)],days,[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_days_ago", nil)]];
                
            }
        }
        else
        {
            NSDate *myDate = [NSDate date];
            [[NSUserDefaults standardUserDefaults] setObject:myDate forKey:@"balanceDate"];
            resultStr = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_last_updated_not_yet", nil)]];
        }
    
    return resultStr;
}

#pragma mark - Updated Time Recent
/**
 * This method is used to get the time difference between Recent transcactions Updated.
 *@param type - varies Time diff in (Seconds,Minutes etc...)
 */

-(NSString *)getTimeDifferencebetweenDateForRecent
{
    NSString *resultStr=nil;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [reachability  currentReachabilityStatus];
    
    NSInteger secondsDiff;
    if([profileID isEqualToString:@"SMS"] || (netStatus == NotReachable))
    {
        NSDate *myDate = [NSDate date];
        [[NSUserDefaults standardUserDefaults] setObject:myDate forKey:@"miniStatementDate"];
        resultStr = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_last_updated_not_yet", nil)]];
    }
    else
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"miniStatementDate"])
        {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:[NSString stringWithFormat:@"%@",NSLocalizedStringFromTableInBundle(@"application_date_and_time_format",@"GeneralSettings",[NSBundle mainBundle], nil)]];
            // Way 2
            secondsDiff = [[NSDate date] timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults] objectForKey:@"miniStatementDate"]];
            
            double minutes = secondsDiff / 60;
            double hours = minutes / 60;
            double seconds = secondsDiff;
            double days = minutes / 1440;
            
            if (seconds<=59)
            {
                resultStr=[NSString stringWithFormat:@"%@ %.0f %@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_last_updated_on", nil)],seconds,[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_seconds_ago", nil)]];
            }
            else if (minutes<=59)
            {
                
                resultStr=[NSString stringWithFormat:@"%@ %.0f %@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_last_updated_on", nil)],minutes,[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_minutes_ago", nil)]];
            }
            else if (hours<=23)
            {
                resultStr=[NSString stringWithFormat:@"%@ %.0f %@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_last_updated_on", nil)],hours,[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_hours_ago", nil)]];
            }
            else if (days<=7)
            {
                
                resultStr=[NSString stringWithFormat:@"%@ %.0f %@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_last_updated_on", nil)],days,[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_days_ago", nil)]];
                
            }
        }
        else
        {
            NSDate *myDate = [NSDate date];
            [[NSUserDefaults standardUserDefaults] setObject:myDate forKey:@"miniStatementDate"];
            resultStr = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_last_updated_not_yet", nil)]];
        }
    
    return resultStr;
}


#pragma mark - PopUp Template Action Method.
/**
 * This method is used to add refresh button action get balance and Recent transactions.
 */
-(void)popUpTemplate1Action :(NSNotification *)notification
{
    statusUpdate = true;
    alertLabel.hidden = YES;
    recentTableView.hidden = NO;
    
    if (notification.object) {
        notifObjStr=notification.object;
        
        // 14230413 Stack overflow
        NSNumberFormatter *numberFormat = [[NSNumberFormatter alloc] init];
        numberFormat.numberStyle = NSNumberFormatterCurrencyStyle;
        
        [numberFormat setCurrencySymbol:@""];
        
        if (NSLocalizedStringFromTableInBundle(@"application_amount_decimal_places",@"GeneralSettings",[NSBundle mainBundle], nil)) {
            [numberFormat setMaximumFractionDigits:[NSLocalizedStringFromTableInBundle(@"application_amount_decimal_places",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue]];
        }
        else{
            [numberFormat setMaximumFractionDigits:0];
        }
        [numberFormat setCurrencyCode:currency];
        NSNumber *amountStr = [NSNumber numberWithDouble:notifObjStr.doubleValue];
        
        NSCharacterSet *charactersToRemove = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopq‌​rstuvwxyz"];
        NSString *resultvalue = [[[numberFormat stringFromNumber:amountStr] componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
        
        if (!((resultvalue == (id)[NSNull null] || resultvalue.length == 0 || [resultvalue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) || [resultvalue compare:@"(null)"]== NSOrderedSame)) {
            [[NSUserDefaults standardUserDefaults] setObject:resultvalue forKey:@"updateBalance"];
            balance_valueLabel.text = [NSString stringWithFormat:@"%@",resultvalue];
            [[NSUserDefaults standardUserDefaults] setObject:balance_valueLabel.text forKey:@"latest_balance"];
        }
        else
            balance_valueLabel.text =@"";
    }
    NSArray *viewsArray = [[NSArray alloc] initWithArray:[self.view subviews]];
    for (int i = 0; i<[viewsArray count]; i++)
    {
        UIView *lView = [viewsArray objectAtIndex:i];
        if (lView.tag == -3)
        {
            [lView removeFromSuperview];
        }
    }
    [recent_dataArray removeAllObjects];
        [recentTableView reloadData];
    recent_dataArray = [[NSMutableArray alloc] initWithArray:(NSMutableArray *)[databaseManager getAllMiniStatementDetails]];
    
    if (recent_dataArray.count > 0)
    {
        recentTableView.hidden = NO;
        alertLabel.hidden = YES;
    }
    else
    {
        alertLabel.hidden = NO;
        recentTableView.hidden = YES;
    }
    recentTableView.delegate = self;
    recentTableView.dataSource = self;
    
    [recentTableView reloadData];
    
}

-(void)updateRecentTransactions{
    NSLog(@"Updating recent txns..");
     [self performSelector:@selector(refreshRecentTransactions:) withObject:recent_refreshButton];
}

#pragma mark - Default memory warning method.
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
