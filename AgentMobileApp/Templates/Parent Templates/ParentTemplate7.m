//
//  ParentTemplate7.m
//  Consumer Client
//
//  Created by android on 6/18/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "ParentTemplate7.h"
#import "ChildTemplate4.h"
#import "Constants.h"
#import "WebServiceConstants.h"
#import "ChildTemplate1.h"
#import "ChildTemplate9.h"
#import "ChildTemplate11.h"
#import "PopUpTemplate6.h"
#import "Localization.h"
#import "ValidationsClass.h"
#import "NotificationConstants.h"

@interface ParentTemplate7 ()
@end
@implementation ParentTemplate7
@synthesize headerImage_Icon,headerNameLabel;

#pragma mark - ParentTemplate7 Frame.
/**
 * This method is used to set implemention of ParentTemplate7.
 */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view
         withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode
            dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary
{
    
     NSLog(@"PropertyFileName:%@",propertyFileArray);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        propertyFile = propertyFileArray;
        processorCodeStr = processorCode;
        
        if ([dataArray count]>0)
        {
            contentArray = [[NSMutableArray alloc] initWithArray:dataArray];
        }
        
        if ([dataDictionary count]>0)
        {
            contentDictionary = [[NSMutableDictionary alloc] initWithDictionary:dataDictionary];
            local_dataDictionary = [[NSMutableDictionary alloc] initWithDictionary:dataDictionary];
        }
    }
    
    return self;
}

#pragma mark - ViewController Life Cycle.
/**
 * This method is used to set add UIconstraints Frame of ParentTemplate7.
 */
- (void)viewDidLoad
{
    
    // Parent Template7 Child Template.
    if(NSLocalizedStringFromTableInBundle(@"parent_template7_child_template",propertyFile,[NSBundle mainBundle], nil).length != 0)
        nextTemplate =NSLocalizedStringFromTableInBundle(@"parent_template7_child_template",propertyFile,[NSBundle mainBundle], nil);
    
//    if([propertyFile isEqualToString:@"UploadDocumentPT7"]){
//        nextTemplate = @"ChildTemplate1";
//    }
    // Parent Template7 Child Template Property File.
    if(NSLocalizedStringFromTableInBundle(@"parent_template7_child_template_property_file",propertyFile,[NSBundle mainBundle], nil).length != 0)
        nextTemplatePropertyFile =  NSLocalizedStringFromTableInBundle(@"parent_template7_child_template_property_file",propertyFile,[NSBundle mainBundle], nil);

//    if([propertyFile isEqualToString:@"UploadDocumentPT7"]){
//        nextTemplatePropertyFile = @"UploadDocumentCT1";
//    }
    
    currentImplementationClass = PARENT_TEMPLATE_7;
    
    NSLog(@"NextTemplate is...%@,%@",nextTemplate,nextTemplatePropertyFile);
    
    if ([nextTemplatePropertyFile isEqualToString:@"BankStatementCT4_T3"]) {
        nextTemplate=@"PopUpTemplate1";
        nextTemplatePropertyFile=@"BankStatementPPT1_T3";
    }
    [self reloadViewUI];
}
/**
 * This method is used to set add NextTemplate of  ParentTemplate7.
 */
-(void)reloadViewUI
{
    
    //For BaseClass
    local_nextTemplate = nextTemplate;
    local_nextTemplatePropertyFileName = nextTemplatePropertyFile;
    local_processorCode = processorCodeStr;
    local_ContentArray = contentArray;
    local_dataDictionary = contentDictionary;
    
    // Parent Background Color.
    NSArray *viewBackgroundColor;
    
    if (![NSLocalizedStringFromTableInBundle(@"parent_template7_back_ground_color",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template7_back_ground_color"] && NSLocalizedStringFromTableInBundle(@"parent_template7_back_ground_color",propertyFile,[NSBundle mainBundle], nil).length != 0)
    {
        viewBackgroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template7_back_ground_color",propertyFile,[NSBundle mainBundle], nil)];
    }
    else{
        viewBackgroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_default_backgorund_color",@"GeneralSettings",[NSBundle mainBundle], nil)];
    }
    
    self.view.backgroundColor=[UIColor colorWithRed:[[viewBackgroundColor objectAtIndex:0] floatValue] green:[[viewBackgroundColor objectAtIndex:1] floatValue] blue:[[viewBackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
    
    //PageHeader
    
    NSString *imageNameStr;
    NSString *labelStr;
    NSArray *titleLabelTextColor;
    NSString *textStyle;
    NSString *fontSize;
    
    // Title Bar Image.
    if ([NSLocalizedStringFromTableInBundle(@"parent_template7_titlebar_image_visibility",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
        imageNameStr=HEADER_IMAGE_NAME;
    }
    // Title bar Label.
    if ([NSLocalizedStringFromTableInBundle(@"parent_template7_titlebar_label_visibility",propertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
        
        if(![NSLocalizedStringFromTableInBundle(@"parent_template7_titlebar_label_text",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template7_titlebar_label_text"] && NSLocalizedStringFromTableInBundle(@"parent_template7_titlebar_label_text",propertyFile,[NSBundle mainBundle], nil).length != 0)
            labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template7_titlebar_label_text",propertyFile,[NSBundle mainBundle], nil)];
        
        // Properties for label TextColor.
        
        if (![NSLocalizedStringFromTableInBundle(@"parent_template7_titlebar_label_text_color",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template7_titlebar_label_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template7_titlebar_label_text_color",propertyFile,[NSBundle mainBundle], nil).length != 0)
            titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template7_titlebar_label_text_color",propertyFile,[NSBundle mainBundle], nil)];
        else
            titleLabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
        
        // Properties for label Textstyle.
        
        if(![NSLocalizedStringFromTableInBundle(@"parent_template7_titlebar_label_text_style",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template7_titlebar_label_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template7_titlebar_label_text_style",propertyFile,[NSBundle mainBundle], nil).length != 0)
            textStyle = NSLocalizedStringFromTableInBundle(@"parent_template7_titlebar_label_text_style",propertyFile,[NSBundle mainBundle], nil);
        else
            textStyle = application_default_text_style;
        
        // Properties for label Font size.
        
        if(![NSLocalizedStringFromTableInBundle(@"parent_template7_titlebar_label_text_size",propertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template7_titlebar_label_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template7_titlebar_label_text_size",propertyFile,[NSBundle mainBundle], nil).length != 0)
            fontSize = NSLocalizedStringFromTableInBundle(@"parent_template7_titlebar_label_text_size",propertyFile,[NSBundle mainBundle], nil);
        
        else
            fontSize = application_default_text_size;
    }
    // To set Add pageHeader For ParentTemplate7.
    pageHeader = [[PageHeaderView alloc]initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,64) withHeaderTitle:labelStr?labelStr:@""  withLeftbarBtn1Image_IconName:BACK_BUTTON_IMAGE withLeftbarBtn2Image_IconName:nil withHeaderButtonImage:imageNameStr?imageNameStr:nil  withRightbarBtn1Image_IconName:nil withRightbarBtn2Image_IconName:nil withRightbarBtn3Image_IconName:nil withTag:0];
    pageHeader.delegate = self;
    
    if (titleLabelTextColor)
        pageHeader.header_titleLabel.textColor = [UIColor colorWithRed:[[titleLabelTextColor objectAtIndex:0] floatValue] green:[[titleLabelTextColor objectAtIndex:1] floatValue] blue:[[titleLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
    
    if ([textStyle isEqualToString:TEXT_STYLE_0])
        pageHeader.header_titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_1])
        pageHeader.header_titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_2])
        pageHeader.header_titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
    
    else
        pageHeader.header_titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
    
    [self.view addSubview:pageHeader];
    
    [self setParentTemplatewithHeader:pageHeader];

}

-(void)viewWillAppear:(BOOL)animated
{
//    [self addViewControls];
    BOOL isTransformed = [[NSUserDefaults standardUserDefaults] boolForKey:@"isScreenTransformed"];
    if([propertyFile isEqualToString:@"UploadDocumentPT7"]
       && isTransformed){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TransformScreenAr" object:self userInfo:nil];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    //    [super viewWillDisappear:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    /*
     * This method is used to set parentTemplate2 Button Actions Of MPIN popup Template
     */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popUpTemplate1Button1Action:) name:PARENT_TEMPLATE7 object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popUpTemplate2Button1Action:) name:PARENT_TEMPLATE7_CANCEL object:nil];
    
    [super viewDidAppear:YES];
}

-(void) viewDidDisappear:(BOOL)animated
{
    /*
     * This method is used to set remove notifications.
     */
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidDisappear:YES];
}

#pragma mark - Menu Button Action
/**
 * This method is used to set Delegate Method of PageHeaderView Button Action(Pop to Previous view).
 */
-(void)menuBtn_Action{
    if([propertyFile isEqualToString:@"UploadDocumentPT7"]){
        [self.navigationController popToRootViewControllerAnimated:NO];
    }else{
        UINavigationController *navigationController = self.navigationController;
        NSLog(@"Views in hierarchy PT7: %@", [self.navigationController viewControllers]);
        [navigationController popViewControllerAnimated:NO];
    }
}


#pragma mark - Removing Previous class and Clear field Data.
/**
 * This method is used to set Removing Previous class and Clear field Data Of parentTemplate7.
 */
-(void) removeViewsWithClassType:(BOOL) clear
{
    NSString *processorCode = local_processorCode;
    NSString *displayText = DigitalKYCUploadString;
    
    if (clear) {
        NSArray *viewsToRemove = [self.view subviews];
        for (UIView *view in viewsToRemove) {
            [view removeFromSuperview];
        }
        [super removeViewsWithClassType:YES];
        [self reloadViewUI];
    }
    else
    {
        [super removeViewsWithClassType:NO];
    }
    if ([processorCode isEqualToString:PROCESSOR_CODE_SELF_REG_DIGITAL_KYC]) {
        if ([[self.view viewWithTag:999] isKindOfClass:[UIButton class]]) {
            [(UIButton *)[self.view viewWithTag:999] setEnabled:YES];
            [(UIButton *)[self.view viewWithTag:999] setTitle:displayText forState:UIControlStateNormal];
            DigitalKYCUploadString = nil;
        }
        else
        {
            [(UILabel *)[self.view viewWithTag:999] setText:displayText];
            DigitalKYCUploadString = nil;
            
        }
    }
}

/*
 * This method is used to set ParentTemplate2 Mpin popupTemplateButton Action..
 */
-(void)popUpTemplate1Button1Action :(NSNotification *)notification
{
    NSString *childTemplate = NSLocalizedStringFromTableInBundle(@"parent_template7_child_template",propertyFile,[NSBundle mainBundle], nil);
    NSString *childFeatureTemplate = NSLocalizedStringFromTableInBundle(@"parent_template7_child_template_property_file",propertyFile,[NSBundle mainBundle], nil);
    
    Class myclass = NSClassFromString(childTemplate);
    
    id obj=nil;
    
    if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)])
    {
        obj = [[myclass alloc] initWithFrame:CGRectMake(0,64,self.view.frame.size.width,self.view.frame.size.height) withPropertyName:childFeatureTemplate hasDidSelectFunction:YES withDataDictionary:nil withDataArray:nil withPIN:nil withProcessorCode:nil withType:nil fromView:0 insideView:nil];
        
        [obj setLocalDelegate:self];
        [self.view addSubview:(UIView *)obj];
    }
    else if ([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
    {
        obj = [[myclass alloc] initWithFrame:CGRectMake(0,64,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:childFeatureTemplate andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:local_selectedIndex withDataArray:local_ContentArray ? local_ContentArray : nil withSubIndex:0];
        [self.view addSubview:(UIView *)obj];
        
    }
    //    }
    /*
     * This method is used to set ParentTemplate2 Removing views.
     */
    NSArray *viewsArray = [self.view subviews];
    NSLog(@"Subviews of array is...%@",viewsArray);
    for (int i = 0; i<[viewsArray count]; i++)
    {
        UIView *lView = [viewsArray objectAtIndex:i];
        NSLog(@"Subviews of view Tag  is...%ld",(long)lView.tag);
        if (lView.tag == 1)
        {
            [lView removeFromSuperview];
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:CHILD_TEMPLATE4 object:nil];
}

/*
 * This method is used to set ParentTemplate2 Mpin popupTemplateButton Action..
 */
-(void)popUpTemplate2Button1Action :(NSNotification *)notification
{
        NSString *childTemplate = NSLocalizedStringFromTableInBundle(@"parent_template7_child_template",propertyFile,[NSBundle mainBundle], nil);
        NSString *childFeatureTemplate = NSLocalizedStringFromTableInBundle(@"parent_template7_child_template_property_file",propertyFile,[NSBundle mainBundle], nil);
    
        Class myclass = NSClassFromString(childTemplate);
        
        id obj=nil;
        
        if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)])
        {
            obj = [[myclass alloc] initWithFrame:CGRectMake(0,64,self.view.frame.size.width,self.view.frame.size.height) withPropertyName:childFeatureTemplate hasDidSelectFunction:YES withDataDictionary:nil withDataArray:nil withPIN:nil withProcessorCode:nil withType:nil fromView:0 insideView:nil];
            
            [obj setLocalDelegate:self];
            [self.view addSubview:(UIView *)obj];
        }
        else if ([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
        {
            obj = [[myclass alloc] initWithFrame:CGRectMake(0,64,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:childFeatureTemplate andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:local_selectedIndex withDataArray:local_ContentArray ? local_ContentArray : nil withSubIndex:0];
            [self.view addSubview:(UIView *)obj];
            
        }
//    }
    /*
     * This method is used to set ParentTemplate2 Removing views.
     */
    NSArray *viewsArray = [self.view subviews];
    NSLog(@"Subviews of array is...%@",viewsArray);
    for (int i = 0; i<[viewsArray count]; i++)
    {
        UIView *lView = [viewsArray objectAtIndex:i];
        NSLog(@"Subviews of view Tag  is...%ld",(long)lView.tag);
        if (lView.tag == 1)
        {
            [lView removeFromSuperview];
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:CHILD_TEMPLATE4_CANCEL object:nil];
}

/*
 * This method is used to Remove notification object.
 */
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Default memory warning method.

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
