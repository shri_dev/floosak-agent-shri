//  ParentTemplate2.m
//  Consumer Client
//
//  Created by Integra Micro on 17/04/15.
//  Copyright (c) 2015 Integra. All rights reserved.

#import "ParentTemplate2.h"
#import "ValidationsClass.h"
#import "Constants.h"
#import "DatabaseConstatants.h"
#import "ChildTemplate1.h"
#import "ChildTemplate2.h"
#import "ChildTemplate4.h"
#import "NotificationConstants.h"
#import "Template.h"
#import "Localization.h"

@interface ParentTemplate2 ()
{
    FeaturesMenu *featuresMenu;
    PageHeaderView *pageHeader;
    NSArray *propertyDataArray;
    NSString *category;
    NSDictionary *templatePropertyDictionary;
    int parentTag;
}

@end

@implementation ParentTemplate2

@synthesize selected_index;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary
{
   
    NSLog(@"PropertyFileName:%@",propertyFileArray);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        category = fromView;
        propertyDataArray = dataArray;
        
        if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"0"])
        {
            if (propertyFileArray) {
                propertyFileName = propertyFileArray;
                parentTag = selectedIndex;
                fromView = propertyFileArray;
                selectedSideMenuButtonIndex = selectedIndex;
            }
            else
            {
                templatePropertyDictionary = [Template getNextFeatureTemplateWithIndex:selectedIndex];
                parentTag = selectedIndex;
                propertiesArray = [templatePropertyDictionary objectForKey:@"templateProperties"];
                fromView = [propertiesArray objectAtIndex:selectedIndex-1];
                selectedSideMenuButtonIndex = selectedIndex;
            }
            
        }
        else if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"1"])
        {
            if (propertyFileArray) {
                propertyFileName = propertyFileArray;
                parentTag = selectedIndex;
                //                int fView = [fromView intValue];
                fromView=propertyFileArray;
                selectedSideMenuButtonIndex = selectedIndex;
            }
            else
            {
                templatePropertyDictionary = [Template getNextFeatureTemplateWithIndex:selectedIndex];
                parentTag = selectedIndex;
                int fView = [fromView intValue];
                propertiesArray = [templatePropertyDictionary objectForKey:KEY_CATEGORY_TEMPLATE_PROPERTY_FILES];
                fromView=[propertiesArray objectAtIndex:fView-1];
                //                selectedSideMenuButtonIndex = fView;
                selectedSideMenuButtonIndex = selectedIndex;
            }
        }
        
        if (fromView && fromView.length > 0 && [fromView rangeOfString:@"TransactionHistory"].location == NSNotFound)
        {
            from = -1;
        }
        else
        {
            from = view;
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [[NSUserDefaults standardUserDefaults] setObject:PARENT_TEMPLATE_2 forKey:CURRENT_IMPLEMENTATION_TEMPLATE];
    [[NSUserDefaults standardUserDefaults] synchronize];
    

    
    //PageHeader
    if (!propertyFileName) {
        propertyFileName=[propertiesArray objectAtIndex:selectedSideMenuButtonIndex-1];
    }
    
    NSString *imageNameStr;
    NSString *labelStr;
    NSArray *titleLabelTextColor;
    NSString *textStyle;
    NSString *fontSize;
    if ([NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_image_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
        imageNameStr=HEADER_IMAGE_NAME;
    }
    if ([NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
        labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text",propertyFileName,[NSBundle mainBundle], nil)];
        if ([NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_default_text_color",@"GeneralSettings",[NSBundle mainBundle], nil)];
            
            // Properties for label Textstyle.
            
            if(NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = NSLocalizedStringFromTableInBundle(@"application_default_text_style",@"GeneralSettings",[NSBundle mainBundle], nil);
            
            // Properties for label Font size.
            
            if(NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = NSLocalizedStringFromTableInBundle(@"application_default_text_size",@"GeneralSettings",[NSBundle mainBundle], nil);
        }
        else
        {
            //Default Properties for label textcolor
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                titleLabelTextColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_default_text_color",@"GeneralSettings",[NSBundle mainBundle], nil)];
            
            //Default Properties for label textStyle
            
            if(NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = NSLocalizedStringFromTableInBundle(@"application_default_text_style",@"GeneralSettings",[NSBundle mainBundle], nil);
            
            //Default Properties for label fontSize
            if(NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template2_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = NSLocalizedStringFromTableInBundle(@"application_default_text_size",@"GeneralSettings",[NSBundle mainBundle], nil);
        }
    }
    pageHeader = [[PageHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64 ) withHeaderTitle:labelStr?labelStr:@""  withLeftbarBtn1Image_IconName:BACK_BUTTON_IMAGE withLeftbarBtn2Image_IconName:nil withHeaderButtonImage:imageNameStr  withRightbarBtn1Image_IconName:nil withRightbarBtn2Image_IconName:nil withRightbarBtn3Image_IconName:nil withTag:0];
    pageHeader.delegate = self;
    
    if (titleLabelTextColor)
        pageHeader.header_titleLabel.textColor = [UIColor colorWithRed:[[titleLabelTextColor objectAtIndex:0] floatValue] green:[[titleLabelTextColor objectAtIndex:1] floatValue] blue:[[titleLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
    
//    if ([textStyle isEqualToString:TEXT_STYLE_1])
//        pageHeader.header_titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
//    else
//        pageHeader.header_titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    
        [self.view addSubview:pageHeader];
    
    self.parentSubView.frame = CGRectMake(self.parentSubView.frame.origin.x, self.parentSubView.frame.origin.y, SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(pageHeader.frame)-110);
    [self.parentSubView setClipsToBounds:true];
    
    if (from != 0)
    {
        NSArray *propertyArray = [[NSArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(@"parent_template2_child_property_files_list",propertyFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
        NSString *actionString = NSLocalizedStringFromTableInBundle(@"parent_template2_child_template",propertyFileName,[NSBundle mainBundle], nil);
        NSArray *actionArray = [[NSArray alloc] initWithArray:[actionString componentsSeparatedByString:@","]];
        
        Class myclass;
        
        if (![[propertyArray objectAtIndex:0] isEqualToString:@""] && ![[actionArray objectAtIndex:0] isEqualToString:@""])
        {
            myclass = NSClassFromString([actionArray objectAtIndex:0]);
            
            id obj=nil;
            
            if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)])
            {
                obj = [[myclass alloc] initWithFrame:CGRectMake(0,0, self.parentSubView.frame.size.width,self.parentSubView.frame.size.height) withPropertyName:[propertyArray objectAtIndex:0] hasDidSelectFunction:YES withDataDictionary:nil withDataArray:nil withPIN:nil withProcessorCode:nil withType:nil fromView:0 insideView:nil];
                
                [obj setLocalDelegate:self];
                [self.parentSubView  addSubview:(UIView *)obj];
            }
            else if ([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
            {
                obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:[propertyArray objectAtIndex:0] andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:local_selectedIndex withDataArray:local_ContentArray ? local_ContentArray : nil withSubIndex:0];
                [self.view addSubview:(UIView *)obj];
                
            }
        }
    }
    
    NSArray *viewBackgroundColor;
    
    if (NSLocalizedStringFromTableInBundle(@"parent_template1_back_ground_color",propertyFileName,[NSBundle mainBundle], nil)) {
        viewBackgroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template2_back_ground_color",propertyFileName,[NSBundle mainBundle], nil)];
    }
    else{
        viewBackgroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_default_backgorund_color",@"GeneralSettings",[NSBundle mainBundle], nil)];
    }
    
    self.view.backgroundColor = [UIColor colorWithRed:[[viewBackgroundColor objectAtIndex:0] floatValue] green:[[viewBackgroundColor objectAtIndex:1] floatValue] blue:[[viewBackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
    
    featuresMenu = [[FeaturesMenu alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.parentSubView.frame), self.view.bounds.size.width, SCREEN_HEIGHT - CGRectGetMaxY(self.parentSubView.frame)) withSelectedIndex:selectedSideMenuButtonIndex fromView:1 withSelectedCategory:0 andSetSubTemplates:true withParentIndex:(int)[[NSUserDefaults standardUserDefaults ] objectForKey:@"topLevelNavigation"]];
    [featuresMenu setBackgroundColor: [UIColor groupTableViewBackgroundColor]];
    featuresMenu.delegate = self;
    [self.view addSubview:featuresMenu];
    
//    self.parentSubView.frame = CGRectMake(self.parentSubView.frame.origin.x, self.parentSubView.frame.origin.y, self.parentSubView.frame.size.width, SCREEN_HEIGHT - 80);
    
    NSArray *childbackGroundColorArray = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template2_child_back_ground_color",propertyFileName,[NSBundle mainBundle], nil)];
    
    self.parentSubView.backgroundColor = [UIColor colorWithRed:[[childbackGroundColorArray objectAtIndex:0] floatValue] green:[[childbackGroundColorArray objectAtIndex:1] floatValue] blue:[[childbackGroundColorArray objectAtIndex:2] floatValue] alpha:1.0f];
    [super viewWillAppear:YES];
    
    if(from == 0)
    {
        Class myclass = NSClassFromString(@"PopUpTemplate1");
        id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT)  withPropertyName:@"TransactionHistoryMPINPPT1" andDelegate:self  withDataDictionary:nil withProcessorCode:PROCESSOR_CODE_MINI_STATEMENT withTag:-3 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
        [self.view addSubview:(UIView *)obj];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popUpTemplate1Button1Action:) name:PARENT_TEMPLATE2 object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popUpTemplate2Button1Action:) name:PARENT_TEMPLATE2_CANCEL object:nil];
    [super viewDidAppear:YES];
    
//    DepositeMoneyPT2_T1 = 4
//    RegistrationPT2_T3 = 5
//    MyAccountsPT2_T1 = 6
    NSLog(@"prop file in pt2 : %@", propertyFileName);
    if([propertyFileName isEqualToString:@"DepositeMoneyPT2_T1"]){
        [featuresMenu scrollToPosition:4];
    }
    if([propertyFileName isEqualToString:@"RegistrationPT2_T3"]){
        [featuresMenu scrollToPosition:5];
    }
    if([propertyFileName isEqualToString:@"MyAccountsPT2_T1"]){
        [featuresMenu scrollToPosition:6];
    }
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    //    [super viewWillDisappear:YES];
}

-(void) viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidDisappear:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
   
}


#pragma mark - Menu Button action

-(void)menuBtn_Action
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark - Feature menu Action
-(void)featuresMenuButtonAction:(int)tag
{
    NSString *nextTemplate;
    NSString *mainTemplate;
    [local_dataDictionary removeAllObjects];
    
    if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"0"])
    {
        //Changed Impln
        mainTemplate = [[[Template getNextFeatureTemplateWithIndex:tag] objectForKey:@"templates"] objectAtIndex:tag];
        
        nextTemplate = [[[Template getNextFeatureTemplateWithIndex:tag] objectForKey:SUB_TEMPLATES_PROPERTIES] objectAtIndex:tag];
        
    }
    else if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"1"])
    {
        //        mainTemplate = [[[Template getNextFeatureTemplateWithIndex:tag] objectForKey:CATEGORIES_NAMES] objectAtIndex:tag-1];
        //        nextTemplate = [[[Template getNextFeatureTemplateWithIndex:tag] objectForKey:KEY_CATEGORY_TEMPLATE_PROPERTY_FILES] objectAtIndex:tag-1];
        mainTemplate = [[[Template getNextFeatureTemplateWithIndex:tag] objectForKey:KEY_CATEGORY_TEMPLATES] objectAtIndex:tag];
        nextTemplate = [[[Template getNextFeatureTemplateWithIndex:tag] objectForKey:KEY_CATEGORY_TEMPLATE_PROPERTY_FILES] objectAtIndex:tag];
    }
    
    if (![nextTemplate isEqualToString:@""])
    {
        Class myclass = NSClassFromString(mainTemplate);
        id obj = nil;
        [[NSUserDefaults standardUserDefaults] setInteger:tag forKey:@"selected_index"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
        {
            obj = [[myclass alloc] initWithNibName:mainTemplate bundle:nil withSelectedIndex:tag fromView:0 withFromView:nil withPropertyFile:nextTemplate withProcessorCode:nil dataArray:nil dataDictionary:nil];
            [self.navigationController pushViewController:(UIViewController*)obj animated:NO];
        }
        else if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
        {
            obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplate andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:tag withDataArray:nil withSubIndex:0];
            [self.view addSubview:(UIView *)obj];
        }
    }
}

-(void)popUpTemplate1Button1Action :(NSNotification *)notification
{
    NSArray *propertyArray = [[NSArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(@"parent_template2_child_property_files_list",propertyFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
    NSString *actionString = NSLocalizedStringFromTableInBundle(@"parent_template2_child_template",propertyFileName,[NSBundle mainBundle], nil);
    NSArray *actionArray = [[NSArray alloc] initWithArray:[actionString componentsSeparatedByString:@","]];
    Class myclass;
    
    if (![[propertyArray objectAtIndex:0] isEqualToString:@""] && ![[actionArray objectAtIndex:0] isEqualToString:@""])
    {
        myclass = NSClassFromString([actionArray objectAtIndex:0]);
        id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0, self.parentSubView.frame.size.width,self.parentSubView.frame.size.height) withPropertyName:[propertyArray objectAtIndex:0] hasDidSelectFunction:NO withDataDictionary:nil withDataArray:nil withPIN:notification.object withProcessorCode:nil withType:nil fromView:0 insideView:nil];
        [self.parentSubView addSubview:(UIView *)obj];
    }
    
    
    DatabaseManager *databaseManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
    NSArray *finalTranArray = [[NSArray alloc] initWithArray:[databaseManager getAllMiniStatementDetails]];
    if ([finalTranArray count] > 0)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"statementUpdated"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"isMiniStatementUpdated"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    NSArray *viewsArray = [self.view subviews];
    for (int i = 0; i<[viewsArray count]; i++)
    {
        UIView *lView = [viewsArray objectAtIndex:i];
        if (lView.tag == -3)
        {
            [lView removeFromSuperview];
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:CHILD_TEMPLATE4 object:nil];
}

-(void)popUpTemplate2Button1Action :(NSNotification *)notification
{
    NSArray *propertyArray = [[NSArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(@"parent_template2_child_property_files_list",propertyFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
    NSString *actionString = NSLocalizedStringFromTableInBundle(@"parent_template2_child_template",propertyFileName,[NSBundle mainBundle], nil);
    NSArray *actionArray = [[NSArray alloc] initWithArray:[actionString componentsSeparatedByString:@","]];
    
    Class myclass;
    
    if (![[propertyArray objectAtIndex:0] isEqualToString:@""] && ![[actionArray objectAtIndex:0] isEqualToString:@""])
    {
        myclass = NSClassFromString([actionArray objectAtIndex:0]);
        
        id obj=nil;
        
        if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)])
        {
            obj = [[myclass alloc] initWithFrame:CGRectMake(0,0, self.parentSubView.frame.size.width,self.parentSubView.frame.size.height) withPropertyName:[propertyArray objectAtIndex:0] hasDidSelectFunction:YES withDataDictionary:nil withDataArray:nil withPIN:nil withProcessorCode:nil withType:nil fromView:0 insideView:nil];
            
            [obj setLocalDelegate:self];
            [self.parentSubView addSubview:(UIView *)obj];
        }
        else if ([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
        {
            obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:[propertyArray objectAtIndex:0] andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:local_selectedIndex withDataArray:local_ContentArray ? local_ContentArray : nil withSubIndex:0];
            [self.view addSubview:(UIView *)obj];
            
        }
    }
    
    NSArray *viewsArray = [self.view subviews];
    for (int i = 0; i<[viewsArray count]; i++)
    {
        UIView *lView = [viewsArray objectAtIndex:i];
        if (lView.tag == -3)
        {
            [lView removeFromSuperview];
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CHILD_TEMPLATE4_CANCEL object:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void) removeViewsWithClassType:(BOOL) clear
{
    if(clear)
        [local_id removeFromSuperview];
    else
        [super removeViewsWithClassType:clear];
    
    if (from != 0 && clear)
    {
        NSArray *propertyArray = [[NSArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(@"parent_template2_child_property_files_list",propertyFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
        NSString *actionString = NSLocalizedStringFromTableInBundle(@"parent_template2_child_template",propertyFileName,[NSBundle mainBundle], nil);
        NSArray *actionArray = [[NSArray alloc] initWithArray:[actionString componentsSeparatedByString:@","]];
        
        Class myclass;
        
        if (![[propertyArray objectAtIndex:0] isEqualToString:@""] && ![[actionArray objectAtIndex:0] isEqualToString:@""])
        {
            myclass = NSClassFromString([actionArray objectAtIndex:0]);
            
            id obj=nil;
            
            if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)])
            {
                obj = [[myclass alloc] initWithFrame:CGRectMake(0,0, self.parentSubView.frame.size.width,self.parentSubView.frame.size.height) withPropertyName:[propertyArray objectAtIndex:0] hasDidSelectFunction:YES withDataDictionary:nil withDataArray:nil withPIN:nil withProcessorCode:nil withType:nil fromView:0 insideView:nil];
                
                [obj setLocalDelegate:self];
                [self.parentSubView addSubview:(UIView *)obj];
            }
            else if ([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
            {
                obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:[propertyArray objectAtIndex:0] andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:local_selectedIndex withDataArray:local_ContentArray ? local_ContentArray : nil withSubIndex:0];
                [self.view addSubview:(UIView *)obj];
            }
        }
    }
}

@end

