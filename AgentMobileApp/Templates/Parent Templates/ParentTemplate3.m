//  ParentTemplate3.m
//  Consumer Client
//
//  Created by Integra Micro on 05/05/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//
#import "ParentTemplate3.h"
#import "Constants.h"
#import "ValidationsClass.h"
#import "ParentTemplate4.h"
#import "ParentTemplate7.h"
#import "Localization.h"
#import "UIImage+RTL.h"
#import "AppDelegate.h"
#import "NotificationConstants.h"
//#import "Log.h"

@interface ParentTemplate3()

@end

@implementation ParentTemplate3

@synthesize headerLabel1,headerLabel2,applicationLogoImage_Icon;
@synthesize pt3Button1,pt3Button2,pt3Button3,pt3Button4;

#pragma mark - ParentTemplate3 UIViewController.
/**
 * This method is used to set implemention of ParentTemplate3.
 */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view
         withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode
            dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary
{
    
    NSLog(@"PropertyFileName:%@",propertyFileArray);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        propertyFileName = propertyFileArray;
    }
     
    
    return self;
}

#pragma mark - UIViewController lifeCycle.
/**
 * This method is used to set add UIconstraints Frame of ParentTemplate4.
 @param Type- Labels,Image  and Button
 * Set Labels and button (TextSize,color and font size).
 */
- (void)viewDidLoad
{
    /*
     * This method is used to Declare parentTemplate3 BackgroundColor.
     */
    
//        Class myclass = NSClassFromString(@"ParentTemplate8");
//        id obj = nil;
//        if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
//        {
//            obj = [[myclass alloc] initWithNibName:@"ParentTemplate8" bundle:nil withSelectedIndex:0 fromView:0 withFromView:nil withPropertyFile:@"SignUpSuccessPT8" withProcessorCode:nil dataArray:nil dataDictionary:nil];
//            [self.navigationController pushViewController:(UIViewController*)obj animated:NO];
//            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:obj];
//            [[[UIApplication sharedApplication] delegate] window].rootViewController = navController;
//        }
//        return;

    // Parent Background Color.
    NSArray *viewBackgroundColor;
    
    if(![NSLocalizedStringFromTableInBundle(@"parent_template3_back_ground_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_back_ground_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_back_ground_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
    {
        /*
         * This method is used to set parentTemplate3 BackgroundColor.
         */
        viewBackgroundColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_back_ground_color",propertyFileName,[NSBundle mainBundle], nil)];
    }
    else{
        /*
         * This method is used to set Default parentTemplate3 BackgroundColor.
         */
        viewBackgroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_default_backgorund_color",@"GeneralSettings",[NSBundle mainBundle], nil)];
    }
    
    /*
     * This method is used to add parenttemplate3 view Background color.
     */
    self.view.backgroundColor=[UIColor colorWithRed:[[viewBackgroundColor objectAtIndex:0] floatValue] green:[[viewBackgroundColor objectAtIndex:1] floatValue] blue:[[viewBackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
    
    /*
     * This method is used to add parenttemplate3 PageHeaderView UiConstraints.
     */
    
    //PageHeader
    NSString *imageNameStr;
    NSString *labelStr;
    NSArray *titleLabelTextColor;
    NSString *textStyle;
    NSString *fontSize;
    
    /*
     * This method is used to add parenttemplate3 PageHeaderView Image name.
     */
    if ([NSLocalizedStringFromTableInBundle(@"parent_template3_titlebar_image_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
        imageNameStr=HEADER_IMAGE_NAME;
    }
    
    /*
     * This method is used to add parenttemplate3 PageHeaderView Title Bar title visibility.
     */
    if ([NSLocalizedStringFromTableInBundle(@"parent_template3_titlebar_label_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
        
        if(![NSLocalizedStringFromTableInBundle(@"parent_template3_titlebar_label_text",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_titlebar_label_text"] && NSLocalizedStringFromTableInBundle(@"parent_template3_titlebar_label_text",propertyFileName,[NSBundle mainBundle], nil).length != 0)
        /*
         * This method is used to add parenttemplate3 PageHeaderView Title String.
         */
            labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template3_titlebar_label_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        /*
         * This method is used to set parenttemplate3 PageHeaderView Default Properties for label textcolor.
         */
        //Default Properties for label textcolor
        
        if(![NSLocalizedStringFromTableInBundle(@"parent_template3_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_titlebar_label_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
            titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            titleLabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
        
        /*
         * This method is used to set parenttemplate3 PageHeaderView Default Properties for label textstyle.
         */
        //Default Properties for label textStyle
        
        if(![NSLocalizedStringFromTableInBundle(@"parent_template3_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_titlebar_label_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template3_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
            textStyle = NSLocalizedStringFromTableInBundle(@"parent_template3_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
            textStyle = application_default_text_style;
        
        /*
         * This method is used to set parenttemplate3 PageHeaderView Default Properties for label fontSize.
         */
        //Default Properties for label fontSize
        if(![NSLocalizedStringFromTableInBundle(@"parent_template3_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_titlebar_label_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template3_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
            fontSize = NSLocalizedStringFromTableInBundle(@"parent_template3_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
            fontSize = application_default_text_size;
    }
    /*
     * This method is used to add parenttemplate3 PageHeaderView Frame.
     */
    pageHeader = [[PageHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64 ) withHeaderTitle:labelStr?labelStr:@""  withLeftbarBtn1Image_IconName:MENU_IMAGE_NAME withLeftbarBtn2Image_IconName:nil withHeaderButtonImage:imageNameStr  withRightbarBtn1Image_IconName:nil withRightbarBtn2Image_IconName:nil withRightbarBtn3Image_IconName:nil withTag:0];
    pageHeader.delegate = self;
    if (titleLabelTextColor)
        pageHeader.header_titleLabel.textColor = [UIColor colorWithRed:[[titleLabelTextColor objectAtIndex:0] floatValue] green:[[titleLabelTextColor objectAtIndex:1] floatValue] blue:[[titleLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
    
    /*
     * This method is used to add parenttemplate3 PageHeaderView TextStyle.
     */
    if ([textStyle isEqualToString:TEXT_STYLE_0])
        pageHeader.header_titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_1])
        pageHeader.header_titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_2])
        pageHeader.header_titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
    
    else
        pageHeader.header_titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
    
    /*
     * This method is used to add parenttemplate3 PageHeaderView.
     */
    [self.view addSubview:pageHeader];
    
    //Constant Width and Height positions
    distanceY=10.0;
    numberOfButtons=0;
    height_Position=0;
    
    nextY_Position=pageHeader.frame.size.height;
    
    /*
     * This method is used to get databaseManager.
     */
    dataBaseManager= [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
    
    /*
     * This method is used to set ParentTemplate3 Headerlabel visibility.
     */
    // Header Label1
    if ([NSLocalizedStringFromTableInBundle(@"parent_template3_label0_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        headerLabel1=[[UILabel alloc]init];
        headerLabel1.frame=CGRectMake(distanceY,nextY_Position, SCREEN_WIDTH-(distanceY*2), 30);
        
        /*
         * This method is used to get ParentTemplate3 Headerlabel Text.
         */
        NSString *headerLabel1Text = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template3_label0_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if(![headerLabel1Text isEqualToString:@"parent_template3_label0_text"] && headerLabel1Text.length != 0)
            headerLabel1.text = headerLabel1Text;
        
        
        /*
         * This method is used to set ParentTemplate3 Headerlabel TextColor.
         */
        // Properties for label TextColor.
        NSArray *label_TextColor;
        
        if(![NSLocalizedStringFromTableInBundle(@"parent_template3_label0_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_label0_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_label0_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
            label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_label0_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
        
        if (label_TextColor)
            headerLabel1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        /*
         * This method is used to set ParentTemplate3 Headerlabel TextColor.
         */
        // Properties for label Textstyle.
        
        NSString *textStyle;
        if(![NSLocalizedStringFromTableInBundle(@"parent_template3_label0_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_label0_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template3_label0_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
            textStyle =NSLocalizedStringFromTableInBundle(@"parent_template3_label0_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
            textStyle =application_default_text_style;
        
        // Properties for label Font size.
        NSString *fontSize;
        if(![NSLocalizedStringFromTableInBundle(@"parent_template3_label0_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_label0_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template3_label0_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
            fontSize =NSLocalizedStringFromTableInBundle(@"parent_template3_label0_text_size",propertyFileName,[NSBundle mainBundle], nil);
        
        else
            fontSize =application_default_text_size;
        
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
            headerLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
            headerLabel1.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
            headerLabel1.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
            headerLabel1.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        
        headerLabel1.textAlignment=NSTextAlignmentCenter;
        nextY_Position=headerLabel1.frame.size.height+headerLabel1.frame.origin.y;
        [self.view addSubview:headerLabel1];
    }
    else
    {
        height_Position= height_Position + headerLabel1.frame.size.height;
    }
    
    if (!([NSLocalizedStringFromTableInBundle(@"parent_template3_label1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame))
    {
        height_Position = height_Position +  30;
    }
    if (!([NSLocalizedStringFromTableInBundle(@"parent_template3_button1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame))
    {
        height_Position = height_Position +  25;
    }
    if (!([NSLocalizedStringFromTableInBundle(@"parent_template3_button2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame))
    {
        height_Position = height_Position +  25;
    }
    if ([NSLocalizedStringFromTableInBundle(@"parent_template3_label1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] != NSOrderedSame && [NSLocalizedStringFromTableInBundle(@"parent_template3_label2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] != NSOrderedSame)
    {
        height_Position = height_Position +  25;
    }
    
    float xPosition=0;
    //Images Array
    NSArray *marketingImagesArr=[[NSArray alloc] initWithArray:[dataBaseManager getAllLaunchAPIInformation]];
    NSLog(@"Getting marketingImagesArr..%@", marketingImagesArr);
//    if (!([marketingImagesArr count]>0))
//    {
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
//        NSString *documentsDirectory = [paths objectAtIndex:0];
//        NSString *imageDirectory = [documentsDirectory stringByAppendingString:@"/SplashImages"];
//        for (int i = 0; i < marketingImagesArr.count; i++){
//            if([[[marketingImagesArr objectAtIndex:i] objectForKey:MARKET_IMAGE_LOCAL_URLS] length] > 0 )
//            {
//                if(!imagesArray)
//                    imagesArray = [[NSMutableArray alloc]initWithObjects:[imageDirectory stringByAppendingPathComponent:[[marketingImagesArr objectAtIndex:i] objectForKey:MARKET_IMAGE_LOCAL_URLS]], nil];
//                else
//                    [imagesArray addObject:[imageDirectory stringByAppendingPathComponent:[[marketingImagesArr objectAtIndex:i] objectForKey:MARKET_IMAGE_LOCAL_URLS]]];
//            }
//        }
//    }
//    else
//    {
        imagesArray = [[NSMutableArray alloc]initWithObjects:@"YTS_App_736x600_P2",@"YTS_App_736x600_P3",@"YTS_App_736x600_P4", @"YTS_App_736x600_P5",@"YTS_App_736x600_P6",nil];
//    }
    
    if(imagesArray.count < 5)
    {
        NSArray *tempArr = [[NSArray alloc]initWithObjects:@"YTS_App_736x600_P2",@"YTS_App_736x600_P3",@"YTS_App_736x600_P4", @"YTS_App_736x600_P5",@"YTS_App_736x600_P6", nil];
        int numberOfStaticImages = (int)(tempArr.count - imagesArray.count);
        for (int i = 0; i < numberOfStaticImages; i ++)
        {
            [imagesArray addObject:[tempArr objectAtIndex:i]];
        }
    }
    
    // Scroll View
    scroll = [[UIScrollView alloc] init];
//    scroll.frame=CGRectMake(0,nextY_Position, SCREEN_WIDTH,(SCREEN_HEIGHT-220)+height_Position);
    scroll.frame=CGRectMake(0,nextY_Position, SCREEN_WIDTH,(SCREEN_HEIGHT-320)+height_Position);
    [self.view addSubview:scroll];
    
    scroll.backgroundColor=[UIColor clearColor];
    scroll.delegate=self;
    scroll.pagingEnabled=YES;
    [scroll setContentSize:CGSizeMake(scroll.frame.size.width*[imagesArray count], scroll.frame.size.height)];
    //Image frame
    xPosition=40;
    for(int i=0;i<[imagesArray count];i++)
    {
        UIImage *image = nil;
        NSString *imagePath = [imagesArray objectAtIndex:i];
        applicationLogoImage_Icon=[[UIImageView alloc]init];
//        applicationLogoImage_Icon.frame=CGRectMake(xPosition,0, scroll.frame.size.width-80, scroll.frame.size.height);
        applicationLogoImage_Icon.frame=CGRectMake(xPosition-40,0, scroll.frame.size.width-1, scroll.frame.size.height);
        if([imagePath rangeOfString:@"/"].location == NSNotFound)
        {
            image = [UIImage imageNamed:imagePath];
        }
        else
        {
            if([[NSFileManager defaultManager] fileExistsAtPath:imagePath])
            {
                image = [UIImage imageWithContentsOfFile:imagePath];
                image=[UIImage imageFlipWithSourceImage:image];
            }
            else {
                NSURL *imageUrl = [NSURL URLWithString:[[marketingImagesArr objectAtIndex:i] objectForKey:MARKET_IMAGE_SERVER_URLS]];
                NSData *imageData = [[NSData alloc] initWithContentsOfURL:imageUrl];
                [imageData writeToURL:[NSURL URLWithString:imagePath] atomically:NO];
                image = [UIImage imageWithData:imageData];
            }
        }
        
        if(!image)
        {
            image = [UIImage imageNamed:@"head.png"];
            [applicationLogoImage_Icon setImage:image];
            applicationLogoImage_Icon.contentMode = UIViewContentModeScaleAspectFit;
            applicationLogoImage_Icon.tag = i;
            [scroll addSubview:applicationLogoImage_Icon];
            xPosition+=scroll.frame.size.width;
        }
    }
    nextY_Position=nextY_Position+applicationLogoImage_Icon.frame.size.height+5;
    scroll.showsHorizontalScrollIndicator = NO;
    
    // Header label2
    if ([NSLocalizedStringFromTableInBundle(@"parent_template3_label_prelogin_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        headerLabel2=[[UILabel alloc]init];
        headerLabel2.frame=CGRectMake(10,nextY_Position,self.view.frame.size.width-20, 30);
        
        NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template3_label_prelogin_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (![headerStr isEqualToString:@"parent_template3_label_text_size"] && headerStr.length != 0)
            headerLabel2.text=headerStr;
        
        // Properties for label TextColor.
        NSArray *label_TextColor;
        if(![NSLocalizedStringFromTableInBundle(@"parent_template3_label_prelogin_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_label_prelogin_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_label_prelogin_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
            label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_label_prelogin_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
            label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
        
        if (label_TextColor)
            headerLabel2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        // Properties for label Textstyle.
        
        NSString *textStyle;
        if(![NSLocalizedStringFromTableInBundle(@"parent_template3_label_prelogin_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_label_prelogin_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template3_label_prelogin_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
            textStyle =NSLocalizedStringFromTableInBundle(@"parent_template3_label_prelogin_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
            textStyle =application_default_text_style;
        
        // Properties for label Font size.
        
        NSString *fontSize;
        if(![NSLocalizedStringFromTableInBundle(@"parent_template3_label_prelogin_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_label_prelogin_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template3_label_prelogin_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
            fontSize =NSLocalizedStringFromTableInBundle(@"parent_template3_label_prelogin_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
            fontSize =application_default_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
            headerLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
            headerLabel2.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
            headerLabel2.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
            headerLabel2.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        
        
        headerLabel2.textAlignment = NSTextAlignmentJustified;
        headerLabel2.lineBreakMode=NSLineBreakByWordWrapping;
        headerLabel2.numberOfLines = 0;
        [headerLabel2 sizeToFit];
        nextY_Position=headerLabel2.frame.origin.y+headerLabel2.frame.size.height;
        [self.view addSubview:headerLabel2];
    }
    
    // Next Y_Position
    if (([NSLocalizedStringFromTableInBundle(@"parent_template3_button1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame) &&([NSLocalizedStringFromTableInBundle(@"parent_template3_button2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame) && (([NSLocalizedStringFromTableInBundle(@"parent_template3_label1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame) ||([NSLocalizedStringFromTableInBundle(@"parent_template3_label2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)))
        nextY_Position=SCREEN_HEIGHT-130;
    else if ((([NSLocalizedStringFromTableInBundle(@"parent_template3_button1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame) ||([NSLocalizedStringFromTableInBundle(@"parent_template3_button2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)) && ((([NSLocalizedStringFromTableInBundle(@"parent_template3_label1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame) ||([NSLocalizedStringFromTableInBundle(@"parent_template3_label2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame))))
        nextY_Position=SCREEN_HEIGHT-90;
    
    else if(([NSLocalizedStringFromTableInBundle(@"parent_template3_button1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame) && ([NSLocalizedStringFromTableInBundle(@"parent_template3_button2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame))
        nextY_Position=SCREEN_HEIGHT-90;
    else if(([NSLocalizedStringFromTableInBundle(@"parent_template3_button1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame) ||([NSLocalizedStringFromTableInBundle(@"parent_template3_button2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame))
        nextY_Position=SCREEN_HEIGHT-45;
    else
        nextY_Position=SCREEN_HEIGHT-45;
    
    
    // Button 1
    if ([NSLocalizedStringFromTableInBundle(@"parent_template3_button1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        pt3Button1=[UIButton buttonWithType:UIButtonTypeCustom];
//        pt3Button1.frame=CGRectMake(10,nextY_Position, SCREEN_WIDTH-20, 40);
        pt3Button1.frame=CGRectMake(10,nextY_Position-40, SCREEN_WIDTH-20, 40);
        
        NSString *button1Title;
        if ([NSLocalizedStringFromTableInBundle(@"parent_template3_button1_text_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            if (![NSLocalizedStringFromTableInBundle(@"parent_template3_button1_text",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button1_text"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button1_text",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                button1Title = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template3_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
        }
        [pt3Button1 setTitle:button1Title forState:UIControlStateNormal];
        pt3Button1.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        
        // properties For Button backgroundColor
        NSArray *button1_BgColor;
        
        if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button1_background_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button1_background_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button1_background_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
            button1_BgColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_button1_background_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
            button1_BgColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        
        if (button1_BgColor)
            pt3Button1.backgroundColor=[UIColor colorWithRed:[[button1_BgColor objectAtIndex:0] floatValue] green:[[button1_BgColor objectAtIndex:1] floatValue] blue:[[button1_BgColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        pt3Button1.backgroundColor = [ValidationsClass colorWithHexStringValue:@"922f4e"];
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template3_button1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *button1_TitleColor;
            
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button1_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button1_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button1_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                button1_TitleColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                button1_TitleColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                button1_TitleColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button1_TitleColor)
                [pt3Button1 setTitleColor:[UIColor colorWithRed:[[button1_TitleColor objectAtIndex:0] floatValue] green:[[button1_TitleColor objectAtIndex:1] floatValue] blue:[[button1_TitleColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button1_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button1_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button1_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template3_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_button_text_style;
            
            // Properties for Button Font size.
            
            NSString *fontSize;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button1_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button1_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button1_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template3_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize =application_default_button_text_size;
            
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                pt3Button1.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                pt3Button1.titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                pt3Button1.titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                pt3Button1.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for Button textcolor
            
            NSArray *button1_TextColor ;
            if (![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button1_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button1_TextColor)
                [pt3Button1 setTitleColor:[UIColor colorWithRed:[[button1_TextColor objectAtIndex:0] floatValue] green:[[button1_TextColor objectAtIndex:1] floatValue] blue:[[button1_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize=application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                pt3Button1.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                pt3Button1.titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                pt3Button1.titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                pt3Button1.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [pt3Button1 addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
        [pt3Button1 setTag:0];
        nextY_Position = pt3Button1.frame.size.height+pt3Button1.frame.origin.y+5;
        [self.view addSubview:pt3Button1];
        numberOfButtons++;
    }
    //    if (!([NSLocalizedStringFromTableInBundle(@"parent_template3_button1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT]==NSOrderedSame)) {
    //        nextY_Position=SCREEN_HEIGHT-70;
    //    }
    if ([NSLocalizedStringFromTableInBundle(@"parent_template3_button2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        pt3Button2=[UIButton buttonWithType:UIButtonTypeCustom];
        pt3Button2.frame=CGRectMake(10, nextY_Position, SCREEN_WIDTH-20, 42);
        NSString *button2Title;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template3_button2_text_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            if (![NSLocalizedStringFromTableInBundle(@"parent_template3_button2_text",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button2_text"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button2_text",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                button2Title = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template3_button2_text",propertyFileName,[NSBundle mainBundle], nil)];
        }
        [pt3Button2 setTitle:button2Title forState:UIControlStateNormal];
        pt3Button2.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        
        // properties For Button backgroundColor
        
        NSArray *button2_BgColor;
        
        if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button2_background_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button2_background_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button2_background_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
            button2_BgColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_button2_background_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
            button2_BgColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        
        if (button2_BgColor)
            pt3Button2.backgroundColor=[UIColor colorWithRed:[[button2_BgColor objectAtIndex:0] floatValue] green:[[button2_BgColor objectAtIndex:1] floatValue] blue:[[button2_BgColor objectAtIndex:2] floatValue] alpha:1.0f];
        
            pt3Button2.backgroundColor = [ValidationsClass colorWithHexStringValue:@"e3e3e3"];
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template3_button2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *button1_TitleColor;
            
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button2_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button2_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button2_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                button1_TitleColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_button2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                button1_TitleColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                button1_TitleColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button1_TitleColor)
                [pt3Button2 setTitleColor:[UIColor colorWithRed:[[button1_TitleColor objectAtIndex:0] floatValue] green:[[button1_TitleColor objectAtIndex:1] floatValue] blue:[[button1_TitleColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button2_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button2_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button2_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template3_button2_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_button_text_style;
            
            
            // Properties for Button Font size.
            
            NSString *fontSize;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button2_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button2_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button2_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template3_button2_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize =application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                pt3Button2.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                pt3Button2.titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                pt3Button2.titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                pt3Button2.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else{
            
            //Default Properties for Button textcolor
            NSArray *button2_TextColor ;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button2_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button2_TextColor)
                [pt3Button2 setTitleColor:[UIColor colorWithRed:[[button2_TextColor objectAtIndex:0] floatValue] green:[[button2_TextColor objectAtIndex:1] floatValue] blue:[[button2_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            

            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize=application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                pt3Button2.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                pt3Button2.titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                pt3Button2.titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                pt3Button2.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        
        [pt3Button2 addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
        [pt3Button2 setTag:2];
        nextY_Position = pt3Button2.frame.size.height+pt3Button2.frame.origin.y+5;
        [self.view addSubview:pt3Button2];
        [pt3Button2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        numberOfButtons++;
    }
    
    //    if (!([NSLocalizedStringFromTableInBundle(@"parent_template3_button1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT]==NSOrderedSame) || ([NSLocalizedStringFromTableInBundle(@"parent_template3_button1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT]==NSOrderedSame)) {
    //        nextY_Position=SCREEN_HEIGHT-30;
    //    }
    // Button3
    if ([NSLocalizedStringFromTableInBundle(@"parent_template3_label1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        pt3Button3=[UIButton buttonWithType:UIButtonTypeCustom];
        pt3Button3.frame=CGRectMake(10, nextY_Position+5, SCREEN_WIDTH-20, 30);
        
        
        NSString *button3Title;
        
        if(![NSLocalizedStringFromTableInBundle(@"parent_template3_label1_text",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_label1_text"] && NSLocalizedStringFromTableInBundle(@"parent_template3_label1_text",propertyFileName,[NSBundle mainBundle], nil).length != 0)
            button3Title= [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template3_label1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        [pt3Button3 setTitle:button3Title forState:UIControlStateNormal];
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template3_label1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            NSArray *button_TitleColor;
            
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_label1_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_label1_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_label1_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                button_TitleColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_label1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                button_TitleColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button_TitleColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button_TitleColor)
                [pt3Button3 setTitleColor:[UIColor colorWithRed:[[button_TitleColor objectAtIndex:0] floatValue] green:[[button_TitleColor objectAtIndex:1] floatValue] blue:[[button_TitleColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_label1_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_label1_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template3_label1_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template3_label1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_button_text_style;
            
            // Properties for Button Font size.
            
            NSString *fontSize;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_label1_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_label1_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template3_label1_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template3_label1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize =application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                pt3Button3.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                pt3Button3.titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                pt3Button3.titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                pt3Button3.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for Button textcolor
            NSArray *button_TextColor ;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                button_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button_TextColor)
                [pt3Button3 setTitleColor:[UIColor colorWithRed:[[button_TextColor objectAtIndex:0] floatValue] green:[[button_TextColor objectAtIndex:1] floatValue] blue:[[button_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            NSString *textStyle;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize=application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                pt3Button3.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                pt3Button3.titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                pt3Button3.titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                pt3Button3.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [pt3Button3 setTag:3];
        [pt3Button3 addTarget:self action:@selector(button3Action:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:pt3Button3];
    }
    
    // Button4
    if ([NSLocalizedStringFromTableInBundle(@"parent_template3_label2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        pt3Button4=[UIButton buttonWithType:UIButtonTypeCustom];
        pt3Button4.frame=CGRectMake(10, nextY_Position+5, SCREEN_WIDTH-20, 30);
        NSString *button3Title;
        
        if (![NSLocalizedStringFromTableInBundle(@"parent_template3_label2_text",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_label2_text"] && NSLocalizedStringFromTableInBundle(@"parent_template3_label2_text",propertyFileName,[NSBundle mainBundle], nil).length != 0)
            button3Title= [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template3_label2_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        [pt3Button4 setTitle:button3Title forState:UIControlStateNormal];
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template3_label2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            NSArray *button_TitleColor;
            
            if (![NSLocalizedStringFromTableInBundle(@"parent_template3_label2_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_label2_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_label2_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                button_TitleColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_label2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                button_TitleColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button_TitleColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button_TitleColor)
                [pt3Button4 setTitleColor:[UIColor colorWithRed:[[button_TitleColor objectAtIndex:0] floatValue] green:[[button_TitleColor objectAtIndex:1] floatValue] blue:[[button_TitleColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_label2_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_label3_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template3_label2_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template3_label3_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_button_text_style;
            
            // Properties for Button Font size.
            
            NSString *fontSize;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_label2_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_label3_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template3_label2_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template3_label2_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize =application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                pt3Button4.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                pt3Button4.titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                pt3Button4.titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                pt3Button4.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for Button textcolor
            NSArray *button_TextColor ;
            if (![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                button_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button_TextColor)
                [pt3Button4 setTitleColor:[UIColor colorWithRed:[[button_TextColor objectAtIndex:0] floatValue] green:[[button_TextColor objectAtIndex:1] floatValue] blue:[[button_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            NSString *textStyle;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template3_button_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template3_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize=application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                pt3Button4.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                pt3Button4.titleLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                pt3Button4.titleLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                pt3Button4.titleLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [pt3Button4 setTag:4];
        [pt3Button4 addTarget:self action:@selector(button4Action:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:pt3Button4];
    }
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template3_label1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame && [NSLocalizedStringFromTableInBundle(@"parent_template3_label2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        [pt3Button3 setFrame:CGRectMake(10, nextY_Position, (SCREEN_WIDTH-20)/2 , 30)];
        [pt3Button4 setFrame:CGRectMake(CGRectGetMaxX(pt3Button3.frame) + 5, nextY_Position, (SCREEN_WIDTH-20)/2 , 30)];
    }
    
    bgLbl = [[UILabel alloc] init];
    bgLbl.frame=CGRectMake(self.view.frame.origin.x, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH, SCREEN_HEIGHT);
    [bgLbl setBackgroundColor:[UIColor blackColor]];
    bgLbl.alpha=0.0;
    [self.view addSubview:bgLbl];
}

-(void)viewWillDisappear:(BOOL)animated
{
    //    [super viewWillDisappear:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    //    [self addControls];
    [super viewWillAppear:YES];
    [self loadOptionsMenu];
    
}


#pragma mark  - SIDE POP MENU VIEW
/**
 * This method is used to set reload OptionsMenu for ParentTemplate3.
 */
-(void)loadOptionsMenu
{
    optionsMenu = [[OptionsMenu alloc] initWithFrame:CGRectMake(-SCREEN_WIDTH, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS) withPropertyFileName:nil withIndex:0];
    optionsMenu.delegate = self;
    [self.view addSubview:optionsMenu];
}

#pragma mark - SELECTED SIDE MENU INDEX
/**
 * This method is used to set Delegate Method of Option menu For ParentTemplate3.
 *@Param type - Options menu (Terms And conditions,Set language etc)
 *Based on User selected options menu feature will be shown Next View with Highilighted.
 *  Options menu feature Name with Next Template and NextTemplate Property file.
 */
-(void)optionsMenu_didSelectRowAtIndexPath:(NSInteger)indexpath withPropertyFile:(NSString *)propertyFile nextTemplate:(NSString *)nextTemplateName andProcessorCode:(NSString *)processorCode
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    optionsMenu.frame = CGRectMake(-SCREEN_WIDTH, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS);
    bgLbl.alpha=0.0;
    [UIView commitAnimations];
    if (![propertyFile isEqualToString:@""] && ![nextTemplateName isEqualToString:@""])
    {
        Class myclass = NSClassFromString(nextTemplateName);
        
        id obj = nil;
        if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
        {
            obj = [[myclass alloc] initWithNibName:nextTemplateName bundle:nil withSelectedIndex:(int)indexpath fromView:0 withFromView:nil withPropertyFile:propertyFile withProcessorCode:nil dataArray:nil dataDictionary:nil];
            [self.navigationController pushViewController:(UIViewController*)obj animated:NO];
        }
        else if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
        {
            obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:propertyFile andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
            [self.view addSubview:(UIView *)obj];
        }
        
    }
}

#pragma mark  - TAPGESTURE FOR HIDING SIDE MENU VIEW

/**
 * This method is used to set Delegate Method used for Hiding  Option menu From View.
 */
-(void)optionsMenu_tapGestureCalled
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    optionsMenu.frame = CGRectMake(-SCREEN_WIDTH, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS);
    bgLbl.alpha=0.0;
    [UIView commitAnimations];
}

#pragma mark - MENU BUTTON ACTION.
/**
 * This method is used to set Delegate Method of PageHeaderView Button Action(Pop to Previous view).
 */
- (void)menuBtn_Action
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    if (optionsMenu.frame.origin.x == 0)
    {
        optionsMenu.frame = CGRectMake(-SCREEN_WIDTH, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS);
        bgLbl.alpha=0.0;
    }
    else
    {
        optionsMenu.frame = CGRectMake(0, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS);
        bgLbl.alpha=0.4;
    }
    [UIView commitAnimations];
    
}

#pragma mark  - BUTTON ACTIONS.

-(void)commonButtonAction
{
    [optionsMenu removeFromSuperview];
    local_actionType = action_type;
    local_nextTemplate = nextTemplate;
    local_nextTemplatePropertyFileName = nextTemplatePropertyFile;
    local_processorCode = processCode;
    [self performAction];
}
/**
 * This method is used to set button1 action of ParenTtemplate3.
 @prameters are defined in button action those are
 * Application display type(ticker,PopUp),validation type,next template,next template PropertyFile,Action type,
 Feature-(processorCode,transaction type).
 */
-(void)button1Action:(id)sender
{
    action_type = NSLocalizedStringFromTableInBundle(@"parent_template3_button1_action_type",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplate = NSLocalizedStringFromTableInBundle(@"parent_template3_button1_next_template",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplatePropertyFile = NSLocalizedStringFromTableInBundle(@"parent_template3_button1_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    processCode = NSLocalizedStringFromTableInBundle(@"parent_template3_button1_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    [self commonButtonAction];
}
/**
 * This method is used to set button2 action of ParenTtemplate3.
 */
-(void)button2Action:(id)sender
{
    action_type = NSLocalizedStringFromTableInBundle(@"parent_template3_button2_action_type",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplate = NSLocalizedStringFromTableInBundle(@"parent_template3_button2_next_template",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplatePropertyFile = NSLocalizedStringFromTableInBundle(@"parent_template3_button2_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    processCode = NSLocalizedStringFromTableInBundle(@"parent_template3_button2_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    [self commonButtonAction];
}
/**
 * This method is used to set button3 action of ParenTtemplate3.
 */
-(void)button3Action:(id)sender
{
    action_type = NSLocalizedStringFromTableInBundle(@"parent_template3_label1_action_type",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplate = NSLocalizedStringFromTableInBundle(@"parent_template3_label1_next_template",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplatePropertyFile = NSLocalizedStringFromTableInBundle(@"parent_template3_label1_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    processCode = NSLocalizedStringFromTableInBundle(@"parent_template3_label1_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    [self commonButtonAction];
}
/**
 * This method is used to set button4 action of ParenTtemplate3.
 */
-(void)button4Action:(id)sender
{
    action_type = NSLocalizedStringFromTableInBundle(@"parent_template3_label2_action_type",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplate = NSLocalizedStringFromTableInBundle(@"parent_template3_label2_next_template",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplatePropertyFile = NSLocalizedStringFromTableInBundle(@"parent_template3_label2_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    processCode = NSLocalizedStringFromTableInBundle(@"parent_template3_label2_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    [self commonButtonAction];
}

#pragma mark - scrollview delegate method
/**
 * This method is used to set Scrollview Delegate.
 *@param type - Based on Image Width  sc roll position Set here.
 */
-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    float scrollContentSizeWidth = scrollView.contentSize.width;
    float scrollOffset = scrollView.contentOffset.x;
    
    if ((scrollOffset + (SCREEN_WIDTH - 80)) > scrollContentSizeWidth)
    {
        [scrollView scrollRectToVisible:CGRectMake(0,0,SCREEN_WIDTH-80,scrollView.frame.size.height) animated:NO];
        pageControl.currentPage = 0;
        return;
    }
    
    if (scrollOffset < -80)
    {
        [scrollView scrollRectToVisible:CGRectMake(scrollView.contentSize.width - SCREEN_WIDTH,0,SCREEN_WIDTH,scrollView.frame.size.height) animated:NO];
        pageControl.currentPage = 0;
        return;
    }
}
#pragma mark - Default memory warning method.
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

