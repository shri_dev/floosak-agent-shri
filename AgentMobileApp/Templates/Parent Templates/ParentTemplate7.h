//
//  ParentTemplate7.h
//  Consumer Client
//
//  Created by android on 6/18/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageHeaderView.h"
#import "ChildTemplate2.h"
#import "BaseViewController.h"
/**
 * This class used to handle functionality and ViewController of ParentTemplate7.
 *
 * @author Integra
 *
 */
@interface ParentTemplate7 : BaseViewController<PageHeaderViewButtonDelegate,UIAlertViewDelegate>
{
    PageHeaderView *pageHeader;
    NSString *headerlabelStr;
    NSString *propertyFile;
    NSString *processorCodeStr;
    NSString *nextTemplate;
    NSString *nextTemplatePropertyFile;
    NSMutableArray *contentArray;
    NSMutableDictionary *contentDictionary;
    bool isTransformed;
}
/**
 * declarations are used to set the UIConstraints Of ParentTemplate7.
 *Label and Pageheader.
 */
@property(nonatomic,strong) UILabel *headerNameLabel;
@property(nonatomic,strong) UIImageView *headerImage_Icon;

/**
 * This method is  used for Method Initialization of ParentTemplate7.
 */
// MethodInitialization.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary;
@end
