//
//  ParentTemplate6.h
//  Consumer Client
//
//  Created by Integra Micro on 07/05/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageHeaderView.h"
#import "BaseViewController.h"
#import "ChildTemplate2.h"
#import "ParentTemplate2.h"
/**
 * This class used to handle functionality and ViewController of ParentTemplate6
 *
 * @author Integra
 *
 */

#define kToggleButtonNormalColor     [ValidationsClass colorWithHexString:@"#FF9000"]


@interface ParentTemplate6 : BaseViewController<PageHeaderViewButtonDelegate>
{
    int selectedButtonIndex,selectedSideMenuButtonIndex;
    NSString *propertyFileName;
    int tabsCount;
    PageHeaderView *pageHeader;
    
    UIScrollView *buttonsSubView;
    NSArray *buttonTextColor;
    NSArray *selectedButtonTextColor;
    NSArray *selectionLabelBackGroundColor;
    
    UIView *parentSubView;
    
    UILabel *seperaterlabel;
    UILabel *buttonSelectedlabel;
    
    NSArray *templatesArray;
    NSArray *templateTitleArray;
    NSArray *templatePropertyFilesArray;
}
/**
 * declarations are used to set the UIConstraints Of ParentTemplate6.
 */
@property (assign)NSInteger selected_index;

/**
 * This method is  used for Method Initialization of ParentTemplate6.
 */
// Method For initialization.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary;
@end
