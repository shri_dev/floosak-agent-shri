//
//  ParentTemplate9.m
//  Consumer Client
//
//  Created by android on 9/9/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//
#import "ParentTemplate9.h"
#import "Constants.h"
#import "DatabaseManager.h"
#import "DatabaseConstatants.h"
#import "ParentTemplate6.h"
#import "PopUpTemplate6.h"
#import "Template.h"
#import "ValidationsClass.h"
#import "Localization.h"
#import "NotificationConstants.h"

@interface ParentTemplate9 ()

@property(nonatomic,strong) NSMutableArray *templateProperty;
@end

@implementation ParentTemplate9

@synthesize propertyFileName,featureNameLabel,parentSubview;

#pragma mark - ParentTemplate9 UIViewController.
/**
 * This method is used to set implemention of ParentTemplate9.
 */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view  withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary
{
   
     NSLog(@"PropertyFileName:%@",propertyFileArray);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        tagVal = selectedIndex;
        
        templateDictionary = [Template getNextFeatureTemplateWithIndex:selectedIndex];
        
        if(fromView)
        {
            NSUInteger index1 = [[templateDictionary objectForKey:KEY_CATEGORY_TEMPLATES] indexOfObject:fromView];
            if( index1 != NSNotFound)
            {
                selectedSideMenuButtonIndex = (int)index1 + 1;
            }
            else
            {
                selectedSideMenuButtonIndex = 1;
            }
        }
        else
        {
            selectedSideMenuButtonIndex = selectedIndex;
        }
    }
    
    NSArray *propertyArray = [[NSArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(@"category_template_property_files",@"Features_List",[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
    currentPropertyFile =  [propertyArray objectAtIndex:(selectedSideMenuButtonIndex - 1)];
    
    return self;
}

#pragma mark - ViewController Methods.
/**
 * This method is used to set add UIconstraints Frame of ParentTemplate9.
 */
- (void)viewDidLoad {
    
    // Parent Background Color.
    NSArray *viewBackgroundColor;
    
    if (NSLocalizedStringFromTableInBundle(@"parent_template9_back_ground_color",propertyFileName,[NSBundle mainBundle], nil)) {
        viewBackgroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template9 _back_ground_color",propertyFileName,[NSBundle mainBundle], nil)];
    }
    else{
        viewBackgroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_default_backgorund_color",@"GeneralSettings",[NSBundle mainBundle], nil)];
    }
    
    //Page Header
    [[NSUserDefaults standardUserDefaults] setObject:PARENT_TEMPLATE_1 forKey:CURRENT_IMPLEMENTATION_TEMPLATE];
    [[NSUserDefaults standardUserDefaults] synchronize];

    NSString *imageNameStr;
    NSString *labelStr;
    NSArray *titleLabelTextColor;
    NSString *textStyle;
    NSString *fontSize;
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_image_visibility",currentPropertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
        imageNameStr=HEADER_IMAGE_NAME;
    }
    if ([NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_visibility",currentPropertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
        
        labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text",currentPropertyFile,[NSBundle mainBundle], nil)];
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_font_attributes_override",currentPropertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_color",currentPropertyFile,[NSBundle mainBundle], nil))
                titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_color",currentPropertyFile,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_color",currentPropertyFile,[NSBundle mainBundle], nil))
                titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_color",currentPropertyFile,[NSBundle mainBundle], nil)];
            else
                titleLabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            // Properties for label Textstyle.
            
            if(NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_style",currentPropertyFile,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_style",currentPropertyFile,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_style",currentPropertyFile,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_style",currentPropertyFile,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            if(NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_size",currentPropertyFile,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_size",currentPropertyFile,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_size",currentPropertyFile,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_size",currentPropertyFile,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
        }
        else
        {
            //Default Properties for label textcolor
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_color",currentPropertyFile,[NSBundle mainBundle], nil))
                titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_color",currentPropertyFile,[NSBundle mainBundle], nil)];
            else
                titleLabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            
            //Default Properties for label textStyle
            
            if(NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_style",currentPropertyFile,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_style",currentPropertyFile,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            if(NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_size",currentPropertyFile,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template9_titlebar_label_text_size",currentPropertyFile,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
        }
    }
    
    
    // To set Add pageHeader For ParentTemplate9.
    pageHeader = [[PageHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64 ) withHeaderTitle:labelStr?labelStr:@""  withLeftbarBtn1Image_IconName:BACK_BUTTON_IMAGE withLeftbarBtn2Image_IconName:nil withHeaderButtonImage:imageNameStr?imageNameStr:nil  withRightbarBtn1Image_IconName:nil withRightbarBtn2Image_IconName:nil withRightbarBtn3Image_IconName:nil withTag:0];
    pageHeader.delegate = self;
    if (titleLabelTextColor)
        pageHeader.header_titleLabel.textColor = [UIColor colorWithRed:[[titleLabelTextColor objectAtIndex:0] floatValue] green:[[titleLabelTextColor objectAtIndex:1] floatValue] blue:[[titleLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
    
    if ([textStyle isEqualToString:TEXT_STYLE_0])
        pageHeader.header_titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_1])
        pageHeader.header_titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_2])
        pageHeader.header_titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
    
    else
        pageHeader.header_titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    [self.view addSubview:pageHeader];
    // To set Add FeatureMenu For ParentTemplate9.
    featureMenu = [[FeaturesMenu alloc] initWithFrame:CGRectMake(0, pageHeader.frame.size.height, 80, SCREEN_HEIGHT - pageHeader.frame.size.height) withSelectedIndex:selectedSideMenuButtonIndex fromView:1 withSelectedCategory:0 andSetSubTemplates:false withParentIndex:selectedSideMenuButtonIndex];
    featureMenu.delegate = self;
    [self.view addSubview:featureMenu];
    
     // To Set ParentSubview for Adding SubView.
    parentSubview=[[UIView alloc]init];
    parentSubview.frame=CGRectMake(featureMenu.frame.size.width, pageHeader.frame.size.height, SCREEN_WIDTH-featureMenu.frame.size.width, SCREEN_HEIGHT-pageHeader.frame.size.height);
    parentSubview.backgroundColor=[UIColor clearColor];
    [self.view addSubview:parentSubview];
    
    // To add next template Property file for parent template9.
    NSString *nextTemplatePropertyFileName = NSLocalizedStringFromTableInBundle(@"parent_template9_child_template_property_file",currentPropertyFile,[NSBundle mainBundle], nil);
    NSString *nextTemplateProperty = NSLocalizedStringFromTableInBundle(@"parent_template9_child_template",currentPropertyFile,[NSBundle mainBundle], nil);
    
    // To add next template for parent template9.
    Class myclass;
    
    if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplatePropertyFileName isEqualToString:@""])
    {
        myclass = NSClassFromString(nextTemplateProperty);
        
        id obj=nil;
        
        if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)])
        {
            obj = [[myclass alloc] initWithFrame:CGRectMake(0,0, parentSubview.frame.size.width,parentSubview.frame.size.height) withPropertyName:nextTemplatePropertyFileName hasDidSelectFunction:YES withDataDictionary:nil withDataArray:nil withPIN:nil withProcessorCode:nil withType:nil fromView:tagVal insideView:self.view];
            [parentSubview addSubview:(UIView *)obj];
        }
        else if ([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray:withSubIndex:)])
        {
            obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplatePropertyFileName andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil withSelectedIndexPath:selectedSideMenuButtonIndex withDataArray:nil withSubIndex:0];
            [self.view addSubview:(UIView *)obj];
        }
    }
}

#pragma mark - Child template4 delegate
/**
 * This method is used to set Delegate Method of Childtemplate4 if its required use this Delegate method.
 */
-(void)childTempalte4SelectionAction:(NSDictionary *)dataDictionary withTempalte:(NSString *)nextTemplate withPropertyFile:(NSString *)nextTemplatePropertyFile
{
    //Used when needed
}

#pragma mark - FeatureMenu button Action.
/**
 * This method is used to set Delegate Method of FeatureMenu For ParentTemplate9.
 *@Param type - Features(Send,Bank etc)
 *Based on User selected Feature will be shown Next View with Highilighted.
 *  Feature Name with Next Template and NextTemplate Property file.
 */
-(void)featuresMenuButtonAction:(int)tag
{
    // To save template navigation Type locally For reference.

    [[NSUserDefaults standardUserDefaults] setInteger:tag forKey:@"topLevelNavigation"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    templateDictionary = [Template getNextFeatureTemplateWithIndex:tag];
    NSString *nextTemplate = [[templateDictionary objectForKey:CATEGORIES_NAMES] objectAtIndex:tag-1];
    
        // To set next template Name sbased on categories.
    if (![nextTemplate isEqualToString:@""])
    {
        Class myclass = NSClassFromString(nextTemplate);
        
        id obj = nil;
        if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
        {
            obj = [[myclass alloc] initWithNibName:nextTemplate bundle:nil withSelectedIndex:tag fromView:0 withFromView:nil withPropertyFile:nil withProcessorCode:nil dataArray:nil dataDictionary:nil];
            [self.navigationController pushViewController:(UIViewController*)obj animated:NO];
        }
        else if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
        {
            obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplate andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:tag-1 withDataArray:nil withSubIndex:0];
            [self.view addSubview:(UIView *)obj];
        }
        else if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)])
        {
            obj =[[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplate hasDidSelectFunction:YES withDataDictionary:nil withDataArray:nil withPIN:nil withProcessorCode:nil withType:nil fromView:1 insideView:nil];
            
            [self.view addSubview:(UIView *)obj];
        }
    }
}

#pragma mark - MenuButton Action
/**
 * This method is used to set Delegate Method of PageHeaderView Button Action(Pop to Previous view).
 */
-(void)menuBtn_Action
{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

@end