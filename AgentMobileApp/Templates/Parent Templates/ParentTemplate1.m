//
//  ParentTemplate1.m
//  Consumer Client
//
//  Created by Integra Micro on 17/04/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//

#import "ParentTemplate1.h"
#import "UIView+Toast.h"
#import <QuartzCore/QuartzCore.h>
#import "ValidationsClass.h"
#import "Constants.h"
#import "ChildTemplate1.h"
#import "Localization.h"
#import "PopUpTemplate2.h"
#import "DatabaseManager.h"
#import "DatabaseConstatants.h"
#import "ParentTemplate6.h"
#import "PopUpTemplate6.h"
#import "Template.h"
#import "NotificationConstants.h"
#import "Constants.h"
//#import "Log.h"

#define kToggleButtonSelectedColor     [ValidationsClass colorWithHexString:@"#FFAE00"]
#define kToggleButtonNormalColor     [ValidationsClass colorWithHexString:@"#FF9000"]


@interface ParentTemplate1 ()
{
    FeaturesMenu *featuresMenu;
    PageHeaderView *pageHeader;
    UIView *parentSubView;
    UIScrollView *buttonsSubView;
    NSString *localPropertyFile;
    NSArray *contentDataArray;
    NSArray *propertyDataArray;
    NSString *category;
    NSDictionary *templatePropertyDictionary;
    int parentTag;
    int fView;
    int tabIndex;
}
@property(nonatomic,strong)NSMutableArray *templateProperty;

@end

@implementation ParentTemplate1

@synthesize buttonOne,buttonOneSelectedlabel,buttonTwo,buttonTwoSelectedlabel,buttonThree,buttonThreeSelectedlabel,seperatorLabel;
@synthesize selected_index,templateProperty;

#pragma mark - ParentTemplate1 UIViewController.
/*
 * This method is used to set The frame for parent template1.
 */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view  withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary
{
    
    NSLog(@"PropertyFileName:%@",propertyFileArray);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

    
    if (self) {
        local_dataDictionary = [[NSMutableDictionary alloc] initWithDictionary:dataDictionary];
        category = fromView;
        propertyDataArray = dataArray;
        /*
         * This method is used to set The feature menu  to get type 0 features.
         */
        
        if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"0"])
        {
            if (propertyFileArray) {
                parentTag = selectedIndex;
                fromView = propertyFileArray;
                category = propertyFileArray;
                selectedSideMenuButtonIndex = selectedIndex;
            }
            else
            {
                templatePropertyDictionary = [Template getNextFeatureTemplateWithIndex:selectedIndex];
                parentTag = selectedIndex;
                propertiesArray = [templatePropertyDictionary objectForKey:@"templateProperties"];
                fromView = [propertiesArray objectAtIndex:selectedIndex-1];
                category = fromView;
                selectedSideMenuButtonIndex = selectedIndex;
            }
        }
        /*
         * This method is used to set The feature menu  to get type 1 features.
         */
        else if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"1"])
        {
            if (propertyFileArray) {
                category = propertyFileArray;
                fView = [fromView intValue];
                selectedSideMenuButtonIndex = selectedIndex;
            }
            else
            {
                templatePropertyDictionary = [Template getNextFeatureTemplateWithIndex:selectedIndex];
                parentTag = selectedIndex;
                fView = [fromView intValue];
                propertiesArray = [templatePropertyDictionary objectForKey:KEY_CATEGORY_TEMPLATE_PROPERTY_FILES];
                fromView=[propertiesArray objectAtIndex:fView-1];
                category = fromView;
                selectedSideMenuButtonIndex = fView;
            }
        }

    }
    return self;
}

#pragma mark - UIViewController Delegate Methods.
/**
 * This method is used to set add UIconstraints Frame of ParentTemplate6.
 */
- (void)viewDidLoad
{
    /*
     * This method is used to set The feature menu  to get type 1 features List Saving.
     */
    [[NSUserDefaults standardUserDefaults] setObject:PARENT_TEMPLATE_1 forKey:CURRENT_IMPLEMENTATION_TEMPLATE];
    [[NSUserDefaults standardUserDefaults] synchronize];
    selectedButtonIndex = 1;
    
   localPropertyFile = category;
    
    // Parent Background Color.
    NSArray *viewBackgroundColor;
    /*
     * This method is used to set parentTemplate1 BackgroundColor.
     */
    if(![NSLocalizedStringFromTableInBundle(@"parent_template1_back_ground_color",localPropertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template1_back_ground_color"] && NSLocalizedStringFromTableInBundle(@"parent_template1_back_ground_color",localPropertyFile,[NSBundle mainBundle], nil).length != 0)
    {
        viewBackgroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template1_back_ground_color",localPropertyFile,[NSBundle mainBundle], nil)];
    }
    else{
        /*
         * This method is used to set parentTemplate1 BackgroundColor is not declared in property file background color get from General settings.
         */
        viewBackgroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_default_backgorund_color",@"GeneralSettings",[NSBundle mainBundle], nil)];
    }
    self.view.backgroundColor = [UIColor colorWithRed:[[viewBackgroundColor objectAtIndex:0] floatValue] green:[[viewBackgroundColor objectAtIndex:1] floatValue] blue:[[viewBackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
    
    //Page Header
    /*
     * This method is used to set PageHeader properties.
     */
    NSString *imageNameStr;
    NSString *labelStr;
    NSArray *titleLabelTextColor;
    NSString *textStyle;
    NSString *fontSize;
  
    if ([NSLocalizedStringFromTableInBundle(@"parent_template1_titlebar_label_visibility",localPropertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
        
        /*
         * This method is used to set PageHeader title text.
         */
        if(![NSLocalizedStringFromTableInBundle(@"parent_template1_titlebar_label_text",localPropertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template1_titlebar_label_text"] && NSLocalizedStringFromTableInBundle(@"parent_template1_titlebar_label_text",localPropertyFile,[NSBundle mainBundle], nil).length != 0)
             labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template1_titlebar_label_text",localPropertyFile,[NSBundle mainBundle], nil)];
        /*
         * This method is used to set PageHeader title Font Attributes .
         */
        if ([NSLocalizedStringFromTableInBundle(@"parent_template1_titlebar_label_font_attributes_override",localPropertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            /*
             * This method is used to set PageHeader title TextColor .
             */
            
            // Properties for label TextColor.
            
            if(![NSLocalizedStringFromTableInBundle(@"parent_template1_titlebar_label_text_color",localPropertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template1_titlebar_label_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template1_titlebar_label_text_color",localPropertyFile,[NSBundle mainBundle], nil).length != 0)
                titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template1_titlebar_label_text_color",localPropertyFile,[NSBundle mainBundle], nil)];
            else
                titleLabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
           
            /*
             * This method is used to set PageHeader title Textstyle.
             */
            // Properties for label Textstyle.
            
            if(![NSLocalizedStringFromTableInBundle(@"parent_template1_titlebar_label_text_style",localPropertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template1_titlebar_label_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template1_titlebar_label_text_style",localPropertyFile,[NSBundle mainBundle], nil).length != 0)
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template1_titlebar_label_text_style",localPropertyFile,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            /*
             * This method is used to set PageHeader title Font size..
             */
            // Properties for label Font size.
          
            if(![NSLocalizedStringFromTableInBundle(@"parent_template1_titlebar_label_text_size",localPropertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template1_titlebar_label_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template1_titlebar_label_text_size",localPropertyFile,[NSBundle mainBundle], nil).length != 0)
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template1_titlebar_label_text_size",localPropertyFile,[NSBundle mainBundle], nil);
    
            else
                fontSize = application_default_text_size;
        }
      else
        {
            /*
             * This method is used to set PageHeader title Default textcolor.
             */
            //Default Properties for label textcolor.
            titleLabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            /*
             * This method is used to set PageHeader title Default textStyle.
             */
            
            //Default Properties for label textStyle
            textStyle = application_default_text_style;
            
            /*
             * This method is used to set PageHeader title Font size..
             */
            //Default Properties for label fontSize
            fontSize = application_default_text_size;
        }
    }
    /*
     * This method is used to set PageHeader Image Name.
     */
    
    // Page Header Image Name.
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template1_titlebar_image_visibility",localPropertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
        imageNameStr=HEADER_IMAGE_NAME;
    }
    /*
     * This method is used to set PageHeader Initilization.
     */
    pageHeader = [[PageHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64 ) withHeaderTitle:labelStr?labelStr:@""  withLeftbarBtn1Image_IconName:BACK_BUTTON_IMAGE withLeftbarBtn2Image_IconName:nil withHeaderButtonImage:imageNameStr  withRightbarBtn1Image_IconName:nil withRightbarBtn2Image_IconName:nil withRightbarBtn3Image_IconName:nil withTag:0];
    pageHeader.delegate = self;
    
    /*
     * This method is used to set PageHeader Title textcolor.
     */
    if (titleLabelTextColor)
        pageHeader.header_titleLabel.textColor = [UIColor colorWithRed:[[titleLabelTextColor objectAtIndex:0] floatValue] green:[[titleLabelTextColor objectAtIndex:1] floatValue] blue:[[titleLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
    /*
     * This method is used to set PageHeader Title Textstyle.
     */
    if ([textStyle isEqualToString:TEXT_STYLE_0])
        pageHeader.header_titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_1])
        pageHeader.header_titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_2])
        pageHeader.header_titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
    
    else
        pageHeader.header_titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
    /*
     * This method is used to set add PageHeader to ParentTemplate1 View.
     */
    [self.view addSubview:pageHeader];
    
    /*
     * This method is used to set parent Subview For parenttenmplate1.
     */
   // parent Subview For parenttenmplate1.
    parentSubView = [[UIView alloc] init];
    //parentSubView.frame=CGRectMake(featuresMenu.frame.size.width, pageHeader.frame.size.height, SCREEN_WIDTH-featuresMenu.frame.size.width, SCREEN_HEIGHT-pageHeader.frame.size.height);
    parentSubView.frame=CGRectMake(0, pageHeader.frame.size.height, self.view.bounds.size.width, SCREEN_HEIGHT-CGRectGetMaxY(pageHeader.frame)-110);
    /*
     * This method is used to set parent Subview(ChildTemplate) Backgroundcolor.
     */
    NSArray *childBackgroundColor;
    
    if (![NSLocalizedStringFromTableInBundle(@"parent_template1_child_back_ground_color",localPropertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template1_child_back_ground_color"] && NSLocalizedStringFromTableInBundle(@"parent_template1_child_back_ground_color",localPropertyFile,[NSBundle mainBundle], nil).length != 0)
         childBackgroundColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template1_child_back_ground_color",localPropertyFile,[NSBundle mainBundle], nil)];
    
     parentSubView.backgroundColor = [UIColor colorWithRed:[[childBackgroundColor objectAtIndex:0] floatValue] green:[[childBackgroundColor objectAtIndex:1] floatValue] blue:[[childBackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
    /*
     * This method is used to set add parent Subview For parenttenmplate1.
     */
    [self.view addSubview:parentSubView];
    
    /*
     * This method is used to set Initialize feature menu.
     */
    // Feature menu.
    //(0, CGRectGetMaxY(self.parentSubView.frame), self.view.bounds.size.width, SCREEN_HEIGHT - CGRectGetMaxY(self.parentSubView.frame))
    featuresMenu = [[FeaturesMenu alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(parentSubView.frame)+25, self.view.bounds.size.width, SCREEN_HEIGHT - CGRectGetMaxY(parentSubView.frame)) withSelectedIndex:selectedSideMenuButtonIndex fromView:1 withSelectedCategory:0 andSetSubTemplates:true withParentIndex:(int)[[NSUserDefaults standardUserDefaults ] objectForKey:@"topLevelNavigation"]];
    
    /*
     * This method is used to set Parent Template1 feature menu List backgroundColor.
     */
    NSArray *featureListBgColor;
    
    if(![NSLocalizedStringFromTableInBundle(@"parent_template1_list_back_ground_color",localPropertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template1_list_back_ground_color"] && NSLocalizedStringFromTableInBundle(@"parent_template1_list_back_ground_color",localPropertyFile,[NSBundle mainBundle], nil).length != 0)
    {
        featureListBgColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template1_list_back_ground_color",localPropertyFile,[NSBundle mainBundle], nil)];
    }
    else{
        /*
         * This method is used to set Parent Template1 feature menu List backgroundColor not declared in property file directly get from General settings .
         */
        featureListBgColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_default_backgorund_color",@"GeneralSettings",[NSBundle mainBundle], nil)];
    }
    
    featuresMenu.mainScrollView.backgroundColor = [UIColor colorWithRed:[[featureListBgColor objectAtIndex:0] floatValue] green:[[featureListBgColor objectAtIndex:1] floatValue] blue:[[featureListBgColor objectAtIndex:2] floatValue] alpha:1.0f];
    /*
     * This method is used to set feature menu Delegate to parent Template view.
     */
    featuresMenu.delegate = self;
    [self.view addSubview:featuresMenu];
    
    /*
     * This method is used to set parenttenmplate1 Tabs textColor.
     */
    if(![NSLocalizedStringFromTableInBundle(@"parent_template1_tab_text_color",localPropertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template1_tab_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template1_tab_text_color",localPropertyFile,[NSBundle mainBundle], nil).length != 0)
    {
        buttonTextColor =  [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template1_tab_text_color",localPropertyFile,[NSBundle mainBundle], nil)];
    }
    else
    {
        /*
         * This method is used to set Parent Template1 Tabs textColor not declared in property file directly get from General settings .
         */
        buttonTextColor =  [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_tab_background_color",@"GeneralSettings",[NSBundle mainBundle], nil)];
    }
    /*
     * This method is used to set parenttenmplate1 Selected Tabs textColor.
     */
    
      if(![NSLocalizedStringFromTableInBundle(@"parent_template1_tab_selected_text_color",localPropertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template1_tab_selected_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template1_tab_selected_text_color",localPropertyFile,[NSBundle mainBundle], nil).length != 0)
    {
           selectedButtonTextColor =  [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template1_tab_selected_text_color",localPropertyFile,[NSBundle mainBundle], nil)];
      }
      else{
          /*
           * This method is used to set Parent Template1 selectedButtonTextColor not declared in property file directly get from General settings .
           */
          selectedButtonTextColor =  [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_tab_indicator_color",localPropertyFile,[NSBundle mainBundle], nil)];
      }
    
    /*
     * This method is used to set parenttenmplate1  Tabs selectionLabelBackGroundColor.
     */
    if (![NSLocalizedStringFromTableInBundle(@"parent_template1_tab_selection_color",localPropertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template1_tab_selection_color"] && NSLocalizedStringFromTableInBundle(@"parent_template1_tab_selection_color",localPropertyFile,[NSBundle mainBundle], nil).length != 0)
          selectionLabelBackGroundColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template1_tab_selection_color",localPropertyFile,[NSBundle mainBundle], nil)];
    
    /*
     * This method is used to set parenttenmplate1  ChildTemplate List.
     */
    if (![NSLocalizedStringFromTableInBundle(@"parent_template1_child_template_list",localPropertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template1_child_template_list"] && NSLocalizedStringFromTableInBundle(@"parent_template1_child_template_list",localPropertyFile,[NSBundle mainBundle], nil).length != 0)
            templatesArray = [NSLocalizedStringFromTableInBundle(@"parent_template1_child_template_list",localPropertyFile,[NSBundle mainBundle], nil) componentsSeparatedByString:@","];
    /*
     * This method is used to set parenttenmplate1  parenttemplates title List.
     */
    if (![NSLocalizedStringFromTableInBundle(@"parent_template1_title_list",localPropertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template1_title_list"] && NSLocalizedStringFromTableInBundle(@"parent_template1_title_list",localPropertyFile,[NSBundle mainBundle], nil).length != 0)
            templateTitleArray = [NSLocalizedStringFromTableInBundle(@"parent_template1_title_list",localPropertyFile,[NSBundle mainBundle], nil) componentsSeparatedByString:@","];
    /*
     * This method is used to set parenttenmplate1  childtemplate property file list.
     */
    if (![NSLocalizedStringFromTableInBundle(@"parent_template1_child_template_property_file_list",localPropertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template1_child_template_property_file_list"] && NSLocalizedStringFromTableInBundle(@"parent_template1_child_template_property_file_list",localPropertyFile,[NSBundle mainBundle], nil).length != 0)
            templatePropertyFilesArray = [NSLocalizedStringFromTableInBundle(@"parent_template1_child_template_property_file_list",localPropertyFile,[NSBundle mainBundle], nil) componentsSeparatedByString:@","];
    
    /*
     * This method is used to set parenttenmplate1   Buttons Subview(Tabs) Frame initialization.
     */
    // Buttons Subview(Tabs)
    buttonsSubView = [[UIScrollView alloc] init];
    buttonsSubView.frame=CGRectMake(0,0,parentSubView.frame.size.width, 55);
    buttonsSubView.backgroundColor = [UIColor whiteColor];
    buttonsSubView.tag = 1;
    buttonsSubView.showsHorizontalScrollIndicator = NO;

    
    /*
     * This method is used to set parenttenmplate1   Buttons Subview(Tabs) Template title Based on index.
     */
    if ([templateTitleArray count] < [templatesArray count])
    {
        if ([templateTitleArray count] < [templatePropertyFilesArray count])
        {
            tabsCount = (int)[templateTitleArray count];
        }
        else
        {
            tabsCount = (int)[templatePropertyFilesArray count];
        }
    }
    else
    {
        if ([templatesArray count] < [templatePropertyFilesArray count])
        {
            tabsCount = (int)[templatesArray count];
        }
        else
        {
            tabsCount = (int)[templatePropertyFilesArray count];
        }
    }
    
    float x = 0;
    
    for (int i=0; i<tabsCount; i++)
    {
        UIButton *local_Button = [UIButton buttonWithType:UIButtonTypeCustom];
        if(tabsCount > 2)
        {
            [local_Button setFrame:CGRectMake(x, 0, (parentSubView.frame.size.width - ((parentSubView.frame.size.width / 3) - 10))/2, 50)];
        }
        else
        {
            [local_Button setFrame:CGRectMake(x, 0, (parentSubView.frame.size.width/tabsCount), 50)];
        }
        
        [local_Button setTitle:[Localization languageSelectedStringForKey:[templateTitleArray objectAtIndex:i]] forState:UIControlStateNormal];
        local_Button.titleLabel.numberOfLines=3;
        [local_Button.titleLabel sizeToFit];
        [local_Button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        [local_Button setTitleColor:[UIColor colorWithRed:[[buttonTextColor objectAtIndex:0] floatValue] green:[[buttonTextColor objectAtIndex:1] floatValue] blue:[[buttonTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
        [local_Button.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
        [local_Button setExclusiveTouch:YES];
        local_Button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [local_Button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [local_Button setTag:i+10];
        [local_Button setBackgroundColor:[UIColor colorWithRed:[[kToggleButtonNormalColor objectAtIndex:0] floatValue] green:[[kToggleButtonNormalColor objectAtIndex:1] floatValue] blue:[[kToggleButtonNormalColor objectAtIndex:2] floatValue] alpha:1.0f]];
        [buttonsSubView addSubview:local_Button];
        
        UILabel *buttonSelectedlabel1 = [[UILabel alloc] init];
        buttonSelectedlabel1.frame=CGRectMake(x, 51,CGRectGetWidth(local_Button.frame), 3);
        [buttonSelectedlabel1 setTag:i+100];
        buttonSelectedlabel1.backgroundColor = [UIColor colorWithRed:[[kToggleButtonNormalColor objectAtIndex:0] floatValue] green:[[kToggleButtonNormalColor objectAtIndex:1] floatValue] blue:[[kToggleButtonNormalColor objectAtIndex:2] floatValue] alpha:1.0f];
        [buttonsSubView addSubview:buttonSelectedlabel1];
        
        x = x+local_Button.frame.size.width+2;
        
        if(i == 0)
        {
//            [local_Button setTitleColor:[UIColor colorWithRed:[[selectedButtonTextColor objectAtIndex:0] floatValue] green:[[selectedButtonTextColor objectAtIndex:1] floatValue] blue:[[selectedButtonTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
           
            [local_Button setBackgroundColor:[UIColor colorWithRed:[[kToggleButtonSelectedColor objectAtIndex:0] floatValue] green:[[kToggleButtonSelectedColor objectAtIndex:1] floatValue] blue:[[kToggleButtonSelectedColor objectAtIndex:2] floatValue] alpha:1.0f]];

            
            buttonSelectedlabel1.backgroundColor = [UIColor colorWithRed:[[selectionLabelBackGroundColor objectAtIndex:0] floatValue] green:[[selectionLabelBackGroundColor objectAtIndex:1] floatValue] blue:[[selectionLabelBackGroundColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            [self buttonAction:local_Button];
        }
    }
    
    [buttonsSubView setContentSize:CGSizeMake(x, CGRectGetHeight(buttonsSubView.frame))];
    [parentSubView addSubview:buttonsSubView];
    
    /*
     * This method is used to set parenttenmplate1   Buttons Subview(Tabs) seperator backgroundcolor.
     */
    NSArray *seperatorLabelBackGroundColor;
    
    if(![NSLocalizedStringFromTableInBundle(@"parent_template1_seperator_back_ground_color",localPropertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template1_seperator_back_ground_color"] && NSLocalizedStringFromTableInBundle(@"parent_template1_seperator_back_ground_color",localPropertyFile,[NSBundle mainBundle], nil).length != 0)
            seperatorLabelBackGroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template1_seperator_back_ground_color",localPropertyFile,[NSBundle mainBundle], nil)];
    /*
     * This method is used to set parenttenmplate1   Buttons Subview(Tabs) seperator Label backgroundcolor.
     */
    seperatorLabel = [[UILabel alloc] init];
    seperatorLabel.frame=CGRectMake(0, 53, parentSubView.frame.size.width, 3);
    seperatorLabel.backgroundColor = [UIColor colorWithRed:[[seperatorLabelBackGroundColor objectAtIndex:0] floatValue] green:[[seperatorLabelBackGroundColor objectAtIndex:1] floatValue] blue:[[seperatorLabelBackGroundColor objectAtIndex:2] floatValue] alpha:1.0f];
    seperatorLabel.tag = 2;
    [parentSubView addSubview:seperatorLabel];
    
    /*
     * This method is used to set parenttenmplate1   ActiVity Indicator View.
     */
    activityIndicator = [[ActivityIndicator alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self.view  addSubview:activityIndicator];
    activityIndicator.hidden = YES;
    [self.view sendSubviewToBack:activityIndicator];
}

-(void)viewWillAppear:(BOOL)animated{
//    post notification to add childbutton if does not exist..
    //BOOL b = [[NSUserDefaults standardUserDefaults] boolForKey:@"refreshView"];
    
//    if(b==FALSE)
//        [self removeViewsWithClassType:YES];
    /*
     * This method is used to set parenttenmplate1 Refresh view method For loading listView .
     */        
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"refreshView"])
    {
        if (![NSLocalizedStringFromTableInBundle(@"parent_template1_child_template_property_file_refresh_value_check",localPropertyFile,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
            [self removeViewsWithClassType:YES];
        }
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"refreshView"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
//    [super viewWillDisappear:YES];
}

#pragma mark - Tab Action Method.
/*
 * This method is used to set parenttenmplate1 Buttons(Tabs) Action.
 *@param Type- Selcted Tab (next template and next Template property file).
 */
-(void)buttonAction:(id)sender
{
    [local_dataDictionary removeAllObjects];
    [self removeSubViews];
    
    UIButton *tempButton = sender;
    tabIndex = (int)tempButton.tag-10;
    
    for (int i=0; i<tabsCount; i++)
    {
        if (tempButton.tag == i+10)
        {
            if ([[buttonsSubView viewWithTag:i+10] isKindOfClass:[UIButton class]])
            {
                UIButton *local_Button = (UIButton *)[buttonsSubView viewWithTag:i+10];
//                [local_Button setTitleColor:[UIColor colorWithRed:[[selectedButtonTextColor objectAtIndex:0] floatValue] green:[[selectedButtonTextColor objectAtIndex:1] floatValue] blue:[[selectedButtonTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                [local_Button setBackgroundColor:[UIColor colorWithRed:[[kToggleButtonSelectedColor objectAtIndex:0] floatValue] green:[[kToggleButtonSelectedColor objectAtIndex:1] floatValue] blue:[[kToggleButtonSelectedColor objectAtIndex:2] floatValue] alpha:1.0f]];
            }
            
            if ([[buttonsSubView viewWithTag:i+100] isKindOfClass:[UILabel class]])
            {
                UILabel *local_Label = (UILabel *)[buttonsSubView viewWithTag:i+100];
                local_Label.backgroundColor = [UIColor colorWithRed:[[selectionLabelBackGroundColor objectAtIndex:0] floatValue] green:[[selectionLabelBackGroundColor objectAtIndex:1] floatValue] blue:[[selectionLabelBackGroundColor objectAtIndex:2] floatValue] alpha:1.0f];
            }
            /*
             * This method is used to set parenttenmplate1 Childtemplate Frame.
             */
            NSString *nextTempalte = [templatesArray objectAtIndex:i];
            NSString *nextTempaltePropertyFile = [templatePropertyFilesArray objectAtIndex:i];
            
            if (![nextTempaltePropertyFile isEqualToString:@""] && ![nextTempalte isEqualToString:@""])
            {
                Class myclass = NSClassFromString(nextTempalte);

                id obj = [[myclass alloc] initWithFrame:CGRectMake(0,buttonsSubView.frame.size.height,parentSubView.frame.size.width,parentSubView.frame.size.height-(buttonsSubView.frame.size.height)) withPropertyName:nextTempaltePropertyFile hasDidSelectFunction:YES withDataDictionary:local_dataDictionary withDataArray:nil withPIN:nil withProcessorCode:nil withType:nil fromView:0 insideView:nil];
                
                if([obj respondsToSelector:@selector(setLocalDelegate:)])
                    [obj setLocalDelegate:self];
                [parentSubView addSubview:(UIView *)obj];
            }
        }
        else
        {
            if ([[buttonsSubView viewWithTag:i+10] isKindOfClass:[UIButton class]])
            {
                UIButton *local_Button = (UIButton *)[buttonsSubView viewWithTag:i+10];
//                [local_Button setTitleColor:[UIColor colorWithRed:[[buttonTextColor objectAtIndex:0] floatValue] green:[[buttonTextColor objectAtIndex:1] floatValue] blue:[[buttonTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                [local_Button setBackgroundColor:[UIColor colorWithRed:[[kToggleButtonNormalColor objectAtIndex:0] floatValue] green:[[kToggleButtonNormalColor objectAtIndex:1] floatValue] blue:[[kToggleButtonNormalColor objectAtIndex:2] floatValue] alpha:1.0f]];
            }
            
            if ([[buttonsSubView viewWithTag:i+100] isKindOfClass:[UILabel class]])
            {
                UILabel *local_Label = (UILabel *)[buttonsSubView viewWithTag:i+100];
                local_Label.backgroundColor = [UIColor clearColor];
            }
        }
    }
    [self makeSelectedButtonVisible:tempButton];
}

/*
 * This method is used to set parenttenmplate1 Buttons(Tabs) Scrollable method.
 */
- (void)makeSelectedButtonVisible : (UIButton *)selectedButton
{
    [buttonsSubView scrollRectToVisible:selectedButton.frame animated:YES];
}

#pragma mark - Menu button Action
/**
 * This method is used to set Delegate Method of PageHeaderView Button Action(Pop to Previous view).
 */
-(void)menuBtn_Action
{
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"isMiniStatementUpdated"];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}
/*
 * This method is used to set parenttenmplate1 Activity indicator Remove from View.
 */
-(void)removeLoading
{
    activityIndicator.hidden = YES;
    [self.view sendSubviewToBack:activityIndicator];
    [activityIndicator stopActivityIndicator];
}

/*
 * This method is used to set parenttenmplate1 Remove Subviews.
 */
-(void)removeSubViews
{
    for (UIView *subview in [parentSubView subviews])
    {
        if (subview.tag == 1)
        {
        }
        else if (subview.tag == 2)
        {
        }
        else
        {
            [subview removeFromSuperview];
        }
    }
}
/*
 * This method is used to set parenttenmplate1 Child template2 delegate Method.
 */
//-(void)childTempalte2SelectionAction:(NSDictionary *)dataDictionary withTempalte:(NSString *)template withPropertyFile:(NSString *)propertyFile
//{
//    activityIndicator.hidden = YES;
//    [self.view sendSubviewToBack:activityIndicator];
//    [activityIndicator stopActivityIndicator];
//    
//    // Popup 1 calling
//    if (![propertyFile isEqualToString:@""] && ![template isEqualToString:@""])
//    {
//        Class myclass = NSClassFromString(template);
//
//        id obj = [[myclass alloc] initWithFrame:CGRectMake(self.view.frame.origin.x,self.view.frame.origin.x,self.view.frame.size.width,self.view.frame.size.height)  withPropertyName:propertyFile andDelegate:self withDataDictionary:dataDictionary withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
//        [self.view addSubview:(UIView *)obj];
//    }
//}
/*
 * This method is used to set parenttenmplate1 Child template4 delegate Method.
 */
-(void)childTempalte4SelectionAction:(NSDictionary *)dataDictionary withTempalte:(NSString *)nextTemplate withPropertyFile:(NSString *)nextTemplatePropertyFile
{
    if (![nextTemplatePropertyFile isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
    {
        activityIndicator.hidden = YES;
        [self.view sendSubviewToBack:activityIndicator];
        [activityIndicator stopActivityIndicator];
        
        UIViewController *object = [[NSClassFromString(nextTemplate) alloc] initWithNibName:nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:nil withPropertyFile:nextTemplatePropertyFile withProcessorCode:nil dataArray:nil dataDictionary:nil];
        
        [self.navigationController pushViewController:object animated:NO];
    }
}

#pragma mark - Feature menu button Action
/**
 * This method is used to set Delegate Method of FeatureMenu For ParentTemplate1.
 *@Param type - Features(Send,Bank etc)
 *Based on User selected Feature will be shown Next View with Highilighted.
 *  Feature Name with Next Template and NextTemplate Property file.
 */
-(void)featuresMenuButtonAction:(int)tag
{
    NSString *nextTemplate;
    NSString *mainTemplate;
    [local_dataDictionary removeAllObjects];
    
    /*
     * This method is used to set  Feature menu List type get from general settings based on Type feature menu list load to Parenttemplate1 .
     */
    if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"0"])
    {
        mainTemplate = [[[Template getNextFeatureTemplateWithIndex:tag] objectForKey:@"templates"] objectAtIndex:tag];
        nextTemplate = [[[Template getNextFeatureTemplateWithIndex:tag] objectForKey:SUB_TEMPLATES_PROPERTIES] objectAtIndex:tag];
        
    }
    /*
     * This method is used to set  Feature menu List type get from general settings based on Type feature menu list load to Parenttemplate1 .
     */
    else if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"1"])
    {
        mainTemplate = [[[Template getNextFeatureTemplateWithIndex:tag] objectForKey:KEY_CATEGORY_TEMPLATES] objectAtIndex:tag];
        nextTemplate = [[[Template getNextFeatureTemplateWithIndex:tag] objectForKey:KEY_CATEGORY_TEMPLATE_PROPERTY_FILES] objectAtIndex:tag];
    }
    
    /*
     * This method is used to set  Feature menu List Next Template PropertyFile .
     */
    if (![nextTemplate isEqualToString:@""])
    {
        Class myclass = NSClassFromString(mainTemplate);
        id obj = nil;
        [[NSUserDefaults standardUserDefaults] setInteger:tag forKey:@"selected_index"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
        {
            obj = [[myclass alloc] initWithNibName:mainTemplate bundle:nil withSelectedIndex:tag fromView:0 withFromView:nil withPropertyFile:nextTemplate withProcessorCode:nil dataArray:nil dataDictionary:nil];
            [self.navigationController pushViewController:(UIViewController*)obj animated:NO];

        }
        else if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
        {
            obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplate andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:tag withDataArray:nil withSubIndex:0];
            [self.view addSubview:(UIView *)obj];
        }
    }
}

#pragma mark - Reload the view Method.
/*
 * This method is used to set Parenttemplate1 remove subviews.
 */
-(void) removeViewsWithClassType:(BOOL) clear
{
    if(clear)
    {
        NSArray *viewsToRemove = [self.view subviews];
        for (int i= 0 ; i < viewsToRemove.count; i++) {
            UIView *subview = [self.view viewWithTag:-1];
            [subview removeFromSuperview];
        }
        [local_id removeFromSuperview];
    }
    else
     [super removeViewsWithClassType:NO];
    
    /*
     * This method is used to set Parenttemplate1 remove subviews with clear TextField data.
     */
    if (clear)
    {
        NSArray *propertyArray;
        if (![NSLocalizedStringFromTableInBundle(@"parent_template1_child_template_property_file_list",localPropertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template1_child_template_property_file_list"] && NSLocalizedStringFromTableInBundle(@"parent_template1_child_template_property_file_list",localPropertyFile,[NSBundle mainBundle], nil).length != 0)
               propertyArray = [[NSArray alloc] initWithArray:[NSLocalizedStringFromTableInBundle(@"parent_template1_child_template_property_file_list",category,[NSBundle mainBundle], nil) componentsSeparatedByString:@","]];
        
        NSString *actionString;
        if (![NSLocalizedStringFromTableInBundle(@"parent_template1_child_template_list",localPropertyFile,[NSBundle mainBundle], nil) isEqualToString:@"parent_template1_child_template_list"] && NSLocalizedStringFromTableInBundle(@"parent_template1_child_template_list",localPropertyFile,[NSBundle mainBundle], nil).length != 0)
                actionString= NSLocalizedStringFromTableInBundle(@"parent_template1_child_template_list",category,[NSBundle mainBundle], nil);
        
        NSArray *actionArray = [[NSArray alloc] initWithArray:[actionString componentsSeparatedByString:@","]];
        /*
         * This method is used to set Parenttemplate1 SubView.
         */
        Class myclass;
        
        if (![[propertyArray objectAtIndex:0] isEqualToString:@""] && ![[actionArray objectAtIndex:0] isEqualToString:@""])
        {
            myclass = NSClassFromString([actionArray objectAtIndex:tabIndex]);
            if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)])
            {
                id obj = [[myclass alloc] initWithFrame:CGRectMake(0,buttonsSubView.frame.size.height,parentSubView.frame.size.width,parentSubView.frame.size.height-(buttonsSubView.frame.size.height)) withPropertyName:[propertyArray objectAtIndex:tabIndex] hasDidSelectFunction:YES withDataDictionary:nil withDataArray:nil withPIN:nil withProcessorCode:nil withType:nil fromView:0 insideView:nil];
                if([obj respondsToSelector:@selector(setLocalDelegate:)])
                    [obj setLocalDelegate:self];
                [parentSubView addSubview:(UIView *)obj];
            }
        }
    }
}


#pragma mark - Default memory warning method.

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
