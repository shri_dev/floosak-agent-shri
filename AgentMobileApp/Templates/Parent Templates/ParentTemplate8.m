//
//  ParentTemplate8.m
//  Consumer Client
//
//  Created by android on 9/1/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "ParentTemplate8.h"
#import "Constants.h"
#import "Localization.h"
#import "ValidationsClass.h"
#import "WebServiceConstants.h"
#import "ParentTemplate3.h"
#import "ParentTemplate4.h"
#import "PageHeaderView.h"
#import "OptionsMenu.h"
#import "ParentTemplate7.h"
#import "Template.h"
#import "NotificationConstants.h"
#import "AppDelegate.h"

@interface ParentTemplate8 ()
@end

@implementation ParentTemplate8
@synthesize headerTitleLabel,messageLabel;
@synthesize contentWebView,activationLogoImage_Icon,documentUploadLabel;

#pragma mark - ParenTTemplate8 UIViewController .
/**
 * This method is used to set implemention of ParentTemplate8.
 */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view
         withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode
            dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary{
    
  
     NSLog(@"PropertyFileName:%@",propertyFileArray);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        propertyFileName = propertyFileArray;
        local_processorCode = processorCode;
    }
    return self;
}

#pragma mark - ViewController lifeCycle.
/**
 * This method is used to set add UIconstraints Frame of ParentTemplate8.
 */
- (void)viewDidLoad
{
    // Parent Background Color.
    NSArray *viewBackgroundColor;
    
    if(![NSLocalizedStringFromTableInBundle(@"parent_template8_back_ground_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_back_ground_color"] && NSLocalizedStringFromTableInBundle(@"parent_template8_back_ground_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
    {
        viewBackgroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template8_back_ground_color",propertyFileName,[NSBundle mainBundle], nil)];
    }
    else{
        viewBackgroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_default_backgorund_color",@"GeneralSettings",[NSBundle mainBundle], nil)];
    }
    
    self.view.backgroundColor=[UIColor colorWithRed:[[viewBackgroundColor objectAtIndex:0] floatValue] green:[[viewBackgroundColor objectAtIndex:1] floatValue] blue:[[viewBackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
    
    
    labelX_Position = 20.0;
    nextY_Position = 64.0;
    distanceY = 5.0;
    float local_height = 0;
    
    // Header Label
    if([NSLocalizedStringFromTableInBundle(@"parent_template8_status_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        headerTitleLabel=[[UILabel alloc]init] ;
        headerTitleLabel.frame=CGRectMake(labelX_Position, nextY_Position, SCREEN_WIDTH-40, 30);
        headerTitleLabel.backgroundColor=[UIColor clearColor];
        headerTitleLabel.lineBreakMode=NSLineBreakByTruncatingTail;
        
        NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template8_loading_status_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (headerStr)
            headerTitleLabel.text=headerStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template8_status_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *headerLabelTextColor;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template8_status_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_status_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template8_status_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                  headerLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template8_status_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_label_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                headerLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                headerLabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (headerLabelTextColor)
                headerTitleLabel.textColor = [UIColor colorWithRed:[[headerLabelTextColor objectAtIndex:0] floatValue] green:[[headerLabelTextColor objectAtIndex:1] floatValue] blue:[[headerLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template8_status_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_status_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template8_status_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template8_status_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_label_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template8_status_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_status_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template8_status_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template8_status_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_label_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize =application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerTitleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerTitleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerTitleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerTitleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *headerLabelTextColor ;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_label_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                headerLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                headerLabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (headerLabelTextColor)
                headerTitleLabel.textColor = [UIColor colorWithRed:[[headerLabelTextColor objectAtIndex:0] floatValue] green:[[headerLabelTextColor objectAtIndex:1] floatValue] blue:[[headerLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_label_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_label_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize =application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerTitleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerTitleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerTitleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerTitleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        headerTitleLabel.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:headerTitleLabel];
        nextY_Position = headerTitleLabel.frame.origin.y+headerTitleLabel.frame.size.height;
        local_height = headerTitleLabel.frame.size.height;
    }
      // Message Label
    if([NSLocalizedStringFromTableInBundle(@"parent_template8_message_text_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        messageLabel=[[UILabel alloc]init] ;
        messageLabel.frame=CGRectMake(10, nextY_Position, SCREEN_WIDTH-20, 60);
        messageLabel.backgroundColor=[UIColor clearColor];
        messageLabel.lineBreakMode=NSLineBreakByTruncatingTail;
        
        NSString *headerStr;
        if (![NSLocalizedStringFromTableInBundle(@"parent_template8_loading_message_text",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_loading_message_text"] && NSLocalizedStringFromTableInBundle(@"parent_template8_loading_message_text",propertyFileName,[NSBundle mainBundle], nil).length != 0)
              headerStr =[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template8_loading_message_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (headerStr)
            messageLabel.text=headerStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template8_message_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelTextColor;
            
            if(![NSLocalizedStringFromTableInBundle(@"parent_template8_message_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_message_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template8_message_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                labelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template8_message_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_label_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                labelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelTextColor)
                messageLabel.textColor = [UIColor colorWithRed:[[labelTextColor objectAtIndex:0] floatValue] green:[[labelTextColor objectAtIndex:1] floatValue] blue:[[labelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template8_message_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_message_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template8_message_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template8_message_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_label_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template8_message_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_message_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template8_message_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template8_message_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(![NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_label_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize =application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                messageLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                messageLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                messageLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                messageLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *labelTextColor ;
            if (![NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_label_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                labelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelTextColor)
                messageLabel.textColor = [UIColor colorWithRed:[[labelTextColor objectAtIndex:0] floatValue] green:[[labelTextColor objectAtIndex:1] floatValue] blue:[[labelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_label_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle =NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle =application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(![NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_label_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize =NSLocalizedStringFromTableInBundle(@"parent_template8_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize =application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                messageLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                messageLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                messageLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                messageLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
         messageLabel.textAlignment = NSTextAlignmentCenter;
        [messageLabel setNumberOfLines:0];
        nextY_Position = messageLabel.frame.origin.y+messageLabel.frame.size.height;//+(distanceY*3);
        [self.view addSubview:messageLabel];
        local_height = local_height+messageLabel.frame.size.height+80;
    }
        // To set image  visibility for Parent template8.
    if ([NSLocalizedStringFromTableInBundle(@"parent_template8_image_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        float xPosition = 15;
        float width = SCREEN_WIDTH-(xPosition*2);
        float height =SCREEN_HEIGHT-(local_height);
        
        activationLogoImage_Icon = [[UIImageView alloc]init];
        activationLogoImage_Icon.frame = CGRectMake(xPosition, nextY_Position, width,height);
        
        if (![NSLocalizedStringFromTableInBundle(@"parent_template8_image_name",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_image_name"] && NSLocalizedStringFromTableInBundle(@"parent_template8_image_name",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                activationLogoImage_Icon.image = [UIImage imageNamed:NSLocalizedStringFromTableInBundle(@"parent_template8_image_name",propertyFileName,[NSBundle mainBundle], nil)];
        
        activationLogoImage_Icon.contentMode = UIViewContentModeScaleToFill;
        [self.view addSubview:activationLogoImage_Icon];
        nextY_Position = CGRectGetMaxY(activationLogoImage_Icon.frame);
    }
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template8_prompt_document_upload_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        [self.uploadKYCButton setHidden:NO];
        [self.uploadKYCButton setTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_yes", nil)] forState:UIControlStateNormal];
        [self.pushToHomeButton setHidden:NO];
        [self.pushToHomeButton setTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_later", nil)] forState:UIControlStateNormal];
        
//        NSArray *uploadButtonColor = [ValidationsClass colorWithHexString:@"#792744"];
//        NSArray *backButtonColor = [ValidationsClass colorWithHexString:@"#f79429"];
//
//        [self.uploadKYCButton setBackgroundColor:[UIColor colorWithRed:[[uploadButtonColor objectAtIndex:0] floatValue] green:[[uploadButtonColor objectAtIndex:1] floatValue] blue:[[uploadButtonColor objectAtIndex:2] floatValue] alpha:1]];
//        [self.pushToHomeButton setBackgroundColor:[UIColor colorWithRed:[[backButtonColor objectAtIndex:0] floatValue] green:[[backButtonColor objectAtIndex:1] floatValue] blue:[[backButtonColor objectAtIndex:2] floatValue] alpha:1]];
        
        [self.uploadKYCMessage2 setHidden:NO];
        [self.uploadKYCMessage2 setText:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_upload_docs_msg", nil)]];
        [self.uploadKYCMessage2 setNumberOfLines:3];
        
        [self.uploadKYCMessage setHidden:NO];
        [self.uploadKYCMessage setText:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_prompt_upload_docs", nil)]];

        if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
        {
            self.uploadKYCMessage.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
            self.uploadKYCMessage2.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
            self.uploadKYCButton.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
            self.pushToHomeButton.layer.affineTransform = CGAffineTransformMakeScale(-1.0, 1.0);
        }

        
    }

    
            [NSTimer scheduledTimerWithTimeInterval:[NSLocalizedStringFromTableInBundle(@"parent_template8_duration_after_success",propertyFileName,[NSBundle mainBundle], nil)  floatValue] target:self selector:@selector(navigate_activationScreen) userInfo:nil repeats:NO];
    }
    
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if([propertyFileName isEqualToString:@"SignUpSuccessPT8"]){
        
    }
}


#pragma mark - Navigate Screen.
/**
 * This method is used to set navigate Push to Add viewController based on TimeInterval.
 *@Params Type - Time ,nextTemplate And Next template propertyFile name.
 */
-(void)navigate_activationScreen
{
    // To set Nexttemplate Propertyfile.
    NSString *nextTemplateProperty;
    
    if (![NSLocalizedStringFromTableInBundle(@"parent_template8_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_next_template_properties_file"] && NSLocalizedStringFromTableInBundle(@"parent_template8_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil).length != 0)
        nextTemplateProperty=NSLocalizedStringFromTableInBundle(@"parent_template8_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    
     // To set Nexttemplate.
    NSString * nextTemplate;
    
    if(![NSLocalizedStringFromTableInBundle(@"parent_template8_next_template",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template8_next_template"] && NSLocalizedStringFromTableInBundle(@"parent_template8_next_template",propertyFileName,[NSBundle mainBundle], nil).length != 0)
            nextTemplate= NSLocalizedStringFromTableInBundle(@"parent_template8_next_template",propertyFileName,[NSBundle mainBundle], nil);
    
    if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
    {
            UIViewController *object = [[NSClassFromString(nextTemplate) alloc] initWithNibName:nextTemplate bundle:nil withSelectedIndex:0 fromView:2 withFromView:PROCESSOR_CODE_LOGIN withPropertyFile:nextTemplateProperty withProcessorCode:nil dataArray:nil dataDictionary:nil];
            [self.navigationController pushViewController:object animated:NO];
    }

}


- (IBAction)push2Home:(id)sender {
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    UINavigationController *navCntrl = (UINavigationController *)delegate.window.rootViewController;
    if (navCntrl && [navCntrl isKindOfClass:[UINavigationController class]])
    {
        [navCntrl popToRootViewControllerAnimated:NO];
    }
}

- (IBAction)uploadKYCDocuments:(id)sender {
    Class myclass = NSClassFromString(@"ParentTemplate7");
    
    id obj = nil;
    if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
    {
        obj = [[myclass alloc] initWithNibName:@"ParentTemplate7" bundle:nil withSelectedIndex:0 fromView:0 withFromView:nil withPropertyFile:@"UploadDocumentPT7" withProcessorCode:nil dataArray:nil dataDictionary:nil];
        [self.navigationController pushViewController:(UIViewController*)obj animated:NO];
    }
}


#pragma mark - Default memory warning method.
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

