//
//  PushNotifiactionViewController.m
//  Consumer Client
//
//  Created by test on 01/02/16.
//  Copyright (c) 2016 Soumya. All rights reserved.
//

#import "PushNotifiactionViewController.h"

@interface PushNotifiactionViewController ()
@end
@implementation PushNotifiactionViewController
@synthesize headerNameLabel;

#pragma mark - PushNotifiactionViewController UIViewController.
/**
 * This method is used to set implemention of childTemplate3.
 */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil dataDictionary:(NSDictionary *)dataDictionary;
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        if ([dataDictionary count]>0)
        {
            contentDictionary = [[NSMutableDictionary alloc] initWithDictionary:dataDictionary];
        }
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addControlsToView];
}

#pragma mark - Add UIcontraints To View.
/**
 * This method is used to set add UIconstraints Frame of PushNotifiactionViewController.
 @param Type- Label
 * Set label Text (Size,color and font size).
 */
-(void)addControlsToView
{
    self.view.backgroundColor=[UIColor whiteColor];
    
    //Header Label
    int X_position=10.0;
    int Y_position=10.0;
    
    headerNameLabel=[[UILabel alloc]init];
    headerNameLabel.frame=CGRectMake(X_position,Y_position,(SCREEN_WIDTH-2*X_position) , 40);
    headerNameLabel.text=[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_confirm_merchant_payment", nil)];
    headerNameLabel.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:headerNameLabel];
    
    Class nextClass = NSClassFromString(CHILD_TEMPLATE_11);
    id object = nil;
    object = [[nextClass alloc] initWithFrame:CGRectMake(0,Y_position+headerNameLabel.frame.size.height+2, SCREEN_WIDTH, SCREEN_HEIGHT-headerNameLabel.frame.size.height) withPropertyName:NOTIFICATION_CHILD_TEMPLATE hasDidSelectFunction:YES withDataDictionary:contentDictionary withDataArray:nil withPIN:nil withProcessorCode:nil withType:nil fromView:0 insideView:nil];
    [self.view addSubview:(UIView *)object];
}

#pragma mark - Default memory warning method.

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
