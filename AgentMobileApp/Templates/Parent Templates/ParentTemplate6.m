//
//  ParentTemplate6.m
//  Consumer Client
//
//  Created by Integra Micro on 07/05/15.
//  Copyright (c) 2015 Integra. All rights reserved.


#import "ParentTemplate6.h"
#import "Constants.h"
#import "ValidationsClass.h"
#import "Localization.h"
#import "NotificationConstants.h"

@interface ParentTemplate6 ()

@end

@implementation ParentTemplate6

#pragma mark - ParenTtemplate6 UIViewController.
/**
 * This method is used to set implemention of ParentTemplate6.
 */

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary
{
    NSLog(@"PropertyFileName:%@",propertyFileArray);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        propertyFileName = propertyFileArray;
        }
    return self;
}

#pragma mark - UIViewController LIfeCycle Method.
/**
 * This method is used to set add UIconstraints Frame of ParentTemplate6.
 */
- (void)viewDidLoad
{
    // Parent Background Color.
    NSArray *viewBackgroundColor;
    if(![NSLocalizedStringFromTableInBundle(@"parent_template6_back_ground_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template6_back_ground_color"] && NSLocalizedStringFromTableInBundle(@"parent_template6_back_ground_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
    {
        viewBackgroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template6_back_ground_color",propertyFileName,[NSBundle mainBundle], nil)];
    }
    else{
        viewBackgroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_default_backgorund_color",@"GeneralSettings",[NSBundle mainBundle], nil)];
    }
    
    self.view.backgroundColor=[UIColor colorWithRed:[[viewBackgroundColor objectAtIndex:0] floatValue] green:[[viewBackgroundColor objectAtIndex:1] floatValue] blue:[[viewBackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
    
    //PageHeader
    
    NSString *imageNameStr;
    NSString *labelStr;
    NSArray *titleLabelTextColor;
    NSString *textStyle;
    NSString *fontSize;
    // titlebar image Name
    if ([NSLocalizedStringFromTableInBundle(@"parent_template6_titlebar_image_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
        imageNameStr=HEADER_IMAGE_NAME;
    }
    // title bar label name.
    if ([NSLocalizedStringFromTableInBundle(@"parent_template6_titlebar_label_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
        
        if (![NSLocalizedStringFromTableInBundle(@"parent_template6_titlebar_label_text",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template6_titlebar_label_text"] && NSLocalizedStringFromTableInBundle(@"parent_template6_titlebar_label_text",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template6_titlebar_label_text",propertyFileName,[NSBundle mainBundle], nil)];
      
            // Properties for label TextColor.
            
            if (![NSLocalizedStringFromTableInBundle(@"parent_template6_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template6_titlebar_label_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template6_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template6_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
            else
                titleLabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            // Properties for label Textstyle.
            
            if(![NSLocalizedStringFromTableInBundle(@"parent_template6_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template6_titlebar_label_text_style"] && NSLocalizedStringFromTableInBundle(@"parent_template6_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template6_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            if(![NSLocalizedStringFromTableInBundle(@"parent_template6_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template6_titlebar_label_text_size"] && NSLocalizedStringFromTableInBundle(@"parent_template6_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil).length != 0)
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template6_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
    }
      // To set Add pageHeader For ParentTemplate6.
    
    pageHeader = [[PageHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64 ) withHeaderTitle:labelStr?labelStr:@""  withLeftbarBtn1Image_IconName:BACK_BUTTON_IMAGE withLeftbarBtn2Image_IconName:nil withHeaderButtonImage:imageNameStr  withRightbarBtn1Image_IconName:nil withRightbarBtn2Image_IconName:nil withRightbarBtn3Image_IconName:nil withTag:0];
    pageHeader.delegate = self;
    if (titleLabelTextColor)
        pageHeader.header_titleLabel.textColor = [UIColor colorWithRed:[[titleLabelTextColor objectAtIndex:0] floatValue] green:[[titleLabelTextColor objectAtIndex:1] floatValue] blue:[[titleLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
    
    if ([textStyle isEqualToString:TEXT_STYLE_0])
        pageHeader.header_titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_1])
        pageHeader.header_titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_2])
        pageHeader.header_titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
    
    else
        pageHeader.header_titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    [self.view addSubview:pageHeader];
    
    // To set parentSubview for adding Subview.
    
    parentSubView = [[UIView alloc] init];
    parentSubView.frame=CGRectMake(0, pageHeader.frame.size.height, SCREEN_WIDTH, SCREEN_HEIGHT);
    NSArray *childBackgroundColor;
    
    if(![NSLocalizedStringFromTableInBundle(@"parent_template6_child_back_ground_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template6_child_back_ground_color"] && NSLocalizedStringFromTableInBundle(@"parent_template6_child_back_ground_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
         childBackgroundColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template6_child_back_ground_color",propertyFileName,[NSBundle mainBundle], nil)];
    
    parentSubView.backgroundColor = [UIColor colorWithRed:[[childBackgroundColor objectAtIndex:0] floatValue] green:[[childBackgroundColor objectAtIndex:1] floatValue] blue:[[childBackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
    [self.view addSubview:parentSubView];
    
    // To add ChildTemplates for parentTemplate6.
    
    if(![NSLocalizedStringFromTableInBundle(@"parent_template6_child_template_list",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template6_child_template_list"] && NSLocalizedStringFromTableInBundle(@"parent_template6_child_template_list",propertyFileName,[NSBundle mainBundle], nil).length != 0)
          templatesArray = [NSLocalizedStringFromTableInBundle(@"parent_template6_child_template_list",propertyFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","];
    
    // To add ChildTemplates Titles for parentTemplate6.
    
    NSString *templateTitleStr;
    
    if(![NSLocalizedStringFromTableInBundle(@"parent_template6_title_list",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template6_title_list"] && NSLocalizedStringFromTableInBundle(@"parent_template6_title_list",propertyFileName,[NSBundle mainBundle], nil).length != 0)
             templateTitleStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template6_title_list",propertyFileName,[NSBundle mainBundle], nil)];
    
       // To add ChildTemplates property files  for parentTemplate6.
    templateTitleArray = [templateTitleStr componentsSeparatedByString:@","];
    
    if(![NSLocalizedStringFromTableInBundle(@"parent_template6_child_template_property_file_list",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template6_child_template_property_file_list"] && NSLocalizedStringFromTableInBundle(@"parent_template6_child_template_property_file_list",propertyFileName,[NSBundle mainBundle], nil).length != 0)
          templatePropertyFilesArray = [NSLocalizedStringFromTableInBundle(@"parent_template6_child_template_property_file_list",propertyFileName,[NSBundle mainBundle], nil) componentsSeparatedByString:@","];
    
    if(![NSLocalizedStringFromTableInBundle(@"parent_template6_tab_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template6_tab_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template6_tab_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
          buttonTextColor =  [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template6_tab_text_color",propertyFileName,[NSBundle mainBundle], nil)];
    
    if(![NSLocalizedStringFromTableInBundle(@"parent_template6_tab_selected_text_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template6_tab_selected_text_color"] && NSLocalizedStringFromTableInBundle(@"parent_template6_tab_selected_text_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
           selectedButtonTextColor =  [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template6_tab_selected_text_color",propertyFileName,[NSBundle mainBundle], nil)];
    
    if (![NSLocalizedStringFromTableInBundle(@"parent_template6_tab_selection_color",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"parent_template6_tab_selection_color"] && NSLocalizedStringFromTableInBundle(@"parent_template6_tab_selection_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
        selectionLabelBackGroundColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template6_tab_selection_color",propertyFileName,[NSBundle mainBundle], nil)];
    
    
    // To set button Subview for adding Tabs .
    buttonsSubView = [[UIScrollView alloc] init];
    buttonsSubView.frame=CGRectMake(0,0,parentSubView.frame.size.width, 55);
    buttonsSubView.backgroundColor = [UIColor whiteColor];
    buttonsSubView.showsHorizontalScrollIndicator= NO;
    
    float x = 0;
    
    if ([templateTitleArray count] < [templatesArray count])
    {
        if ([templateTitleArray count] < [templatePropertyFilesArray count])
        {
            tabsCount = (int)[templateTitleArray count];
        }
        else
        {
            tabsCount = (int)[templatePropertyFilesArray count];
        }
    }
    else
    {
        if ([templatesArray count] < [templatePropertyFilesArray count])
        {
            tabsCount = (int)[templatesArray count];
        }
        else
        {
            tabsCount = (int)[templatePropertyFilesArray count];
        }
    }
    for (int i=0; i<tabsCount; i++)
    {
        UIButton *local_Button = [UIButton buttonWithType:UIButtonTypeCustom];
        [local_Button setFrame:CGRectMake(x, 0, (SCREEN_WIDTH/tabsCount), 50)];
        
        NSString *nameStr=[Localization languageSelectedStringForKey:[templateTitleArray objectAtIndex:i]];
        
        if (nameStr)
              [local_Button setTitle:nameStr forState:UIControlStateNormal];
        
        [local_Button setTitleColor:[UIColor colorWithRed:[[buttonTextColor objectAtIndex:0] floatValue] green:[[buttonTextColor objectAtIndex:1] floatValue] blue:[[buttonTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
        [local_Button.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
        [local_Button setExclusiveTouch:YES];
        local_Button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [local_Button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [local_Button setTag:i+1];
        [local_Button setBackgroundColor:[UIColor clearColor]];
        if([propertyFileName isEqualToString:@"PayBillMyBillersPT6"]){
            [local_Button setBackgroundColor:[UIColor colorWithRed:[[kToggleButtonNormalColor objectAtIndex:0] floatValue] green:[[kToggleButtonNormalColor objectAtIndex:1] floatValue] blue:[[kToggleButtonNormalColor objectAtIndex:2] floatValue] alpha:1.0f]];
        }
        [buttonsSubView addSubview:local_Button];
        
        UILabel *buttonSelectedlabel1 = [[UILabel alloc] init];
        buttonSelectedlabel1.frame=CGRectMake(x, buttonsSubView.frame.size.height-5,(SCREEN_WIDTH/tabsCount), 3);
        [buttonSelectedlabel1 setTag:i+100];
        buttonSelectedlabel1.backgroundColor = [UIColor clearColor];
        [buttonsSubView addSubview:buttonSelectedlabel1];

        x = x+local_Button.frame.size.width+2;
        
        if(i==0)
        {
            [local_Button setTitleColor:[UIColor colorWithRed:[[selectedButtonTextColor objectAtIndex:0] floatValue] green:[[selectedButtonTextColor objectAtIndex:1] floatValue] blue:[[selectedButtonTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            buttonSelectedlabel1.backgroundColor = [UIColor colorWithRed:[[selectionLabelBackGroundColor objectAtIndex:0] floatValue] green:[[selectionLabelBackGroundColor objectAtIndex:1] floatValue] blue:[[selectionLabelBackGroundColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            [self buttonAction:local_Button];
        }
    }
    
    [buttonsSubView setContentSize:CGSizeMake(x, CGRectGetHeight(buttonsSubView.frame))];
    [parentSubView addSubview:buttonsSubView];
    
    // label  back ground color
    if(NSLocalizedStringFromTableInBundle(@"parent_template6_seperator_back_ground_color",propertyFileName,[NSBundle mainBundle], nil).length != 0)
            selectionLabelBackGroundColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template6_seperator_back_ground_color",propertyFileName,[NSBundle mainBundle], nil)];
    
    // Seperator Label
    seperaterlabel = [[UILabel alloc] init];
    seperaterlabel.frame=CGRectMake(0,buttonsSubView.frame.size.height-3,SCREEN_WIDTH,3);
    seperaterlabel.backgroundColor = [UIColor colorWithRed:[[selectionLabelBackGroundColor objectAtIndex:0] floatValue] green:[[selectionLabelBackGroundColor objectAtIndex:1] floatValue] blue:[[selectionLabelBackGroundColor objectAtIndex:2] floatValue] alpha:1.0f];
    [parentSubView addSubview:seperaterlabel];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
//    [super viewWillDisappear:YES];
}

#pragma mark - Tab Selected Button.
/*
 * This method is used to set parenttenmplate6 Buttons(Tabs) Action.
 *@param Type- Selcted Tab (next template and next Template property file).
 */
-(void)buttonAction:(id)sender
{
    UIButton *tempButton = sender;
    [self.view endEditing:YES];
    for (int i=0; i<tabsCount; i++)
    {
        if (tempButton.tag == i+1)
        {
            if ([[buttonsSubView viewWithTag:i+1] isKindOfClass:[UIButton class]])
            {
                UIButton *local_Button = (UIButton *)[buttonsSubView viewWithTag:i+1];
                [local_Button setTitleColor:[UIColor colorWithRed:[[selectedButtonTextColor objectAtIndex:0] floatValue] green:[[selectedButtonTextColor objectAtIndex:1] floatValue] blue:[[selectedButtonTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            }
            
            if ([[buttonsSubView viewWithTag:i+100] isKindOfClass:[UILabel class]])
            {
                UILabel *local_Label = (UILabel *)[buttonsSubView viewWithTag:i+100];
                local_Label.backgroundColor = [UIColor colorWithRed:[[selectionLabelBackGroundColor objectAtIndex:0] floatValue] green:[[selectionLabelBackGroundColor objectAtIndex:1] floatValue] blue:[[selectionLabelBackGroundColor objectAtIndex:2] floatValue] alpha:1.0f];
            }
            
            NSString *nextTempalte = [templatesArray objectAtIndex:i];
            NSString *nextTempaltePropertyFile = [templatePropertyFilesArray objectAtIndex:i];
            
            if (![nextTempaltePropertyFile isEqualToString:@""] && ![nextTempalte isEqualToString:@""])
            {
                Class myclass = NSClassFromString(nextTempalte);

                id obj = [[myclass alloc] initWithFrame:CGRectMake(0,pageHeader.frame.size.height+buttonsSubView.frame.size.height,SCREEN_WIDTH,SCREEN_HEIGHT-(pageHeader.frame.size.height+buttonsSubView.frame.size.height)) withPropertyName:nextTempaltePropertyFile hasDidSelectFunction:YES withDataDictionary:nil withDataArray:nil withPIN:nil withProcessorCode:nil withType:nil fromView:0 insideView:nil];
                [obj setLocalDelegate:self];
                [self.view addSubview:(UIView *)obj];
            }
        }
        else
        {
            if ([[buttonsSubView viewWithTag:i+1] isKindOfClass:[UIButton class]])
            {
                UIButton *local_Button = (UIButton *)[buttonsSubView viewWithTag:i+1];
                [local_Button setTitleColor:[UIColor colorWithRed:[[buttonTextColor objectAtIndex:0] floatValue] green:[[buttonTextColor objectAtIndex:1] floatValue] blue:[[buttonTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                local_Button.titleLabel.numberOfLines=6;
                [local_Button.titleLabel sizeToFit];
            }
            
            if ([[buttonsSubView viewWithTag:i+100] isKindOfClass:[UILabel class]]){
                UILabel *local_Label = (UILabel *)[buttonsSubView viewWithTag:i+100];
                local_Label.backgroundColor = [UIColor clearColor];
            }
        }
    }
    
    [self makeSelectedButtonVisible:tempButton];
}
/*
 * This method is used to set parenttenmplate6 Buttons(Tabs) Scrollable method.
 */
- (void)makeSelectedButtonVisible : (UIButton *)selectedButton
{
    [buttonsSubView scrollRectToVisible:selectedButton.frame animated:YES];
}
#pragma mark - PageHeader delegate Method.
/**
 * This method is used to set Delegate Method of PageHeaderView Button Action(Pop to Previous view).
 */
-(void)menuBtn_Action
{
//    topViewController
//    UINavigationController *navigationController = self.navigationController;
//    NSLog(@"Views in hierarchy  Parent6: %@", [navigationController viewControllers]);
//    NSString *className=NSStringFromClass([[self.navigationController visibleViewController] class]);
//    NSLog(@"class name is: %@", className);
//    
//    [navigationController popViewControllerAnimated:NO];
    
//    UINavigationController *nav = self.navigationController;
//    [self.navigationController popToRootViewControllerAnimated:NO];
//    [nav pushViewController:viewThreadController animated:YES];
    
//    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
//    for (UIViewController *aViewController in allViewControllers) {
//        if ([aViewController isKindOfClass:[ParentTemplate2 class]]) {
//            [self.navigationController popToViewController:aViewController animated:NO];
//        }
//    }
    
    [self.navigationController popViewControllerAnimated:YES];
    


}

#pragma mark - Default memory warning method.
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
