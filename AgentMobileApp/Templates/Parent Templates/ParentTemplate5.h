//
//  ParentTemplate5.h
//  Consumer Client
//
//  Created by PrasadSD on 07/05/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageHeaderView.h"
#import "FeaturesMenu.h"
#import "PopUpTemplate1.h"
#import "PopUpTemplate2.h"
#import "XmlParserHandler.h"
#import "ActivityIndicator.h"
#import "DatabaseManager.h"
#import "BaseViewController.h"
#import "OptionsMenu.h"
#import "GridView.h"

/**
 * This class used to handle functionality and ViewController of ParentTemplate5
 *
 * @author Integra
 *
 */
@interface ParentTemplate5 : BaseViewController<PageHeaderViewButtonDelegate,FeaturesMenuDelegate,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,OptionsMenuDelegate,GridViewDelegate>
{
    NSString *propertyFile;
    int selectedSideMenuButtonIndex;
    NSString *alertview_Type;
    int fromScreen;
    DatabaseManager *databaseManager;
    float Xposition;
    UIImagePickerController *imagePicker;
    UIImage *selectedImage;
    
    OptionsMenu *optionsMenu;
    FeaturesMenu *featuresMenu;
    PageHeaderView *pageHeader;
    UILabel *bgLbl;
    NSString *profileID;
        
    BOOL statusUpdate;
    int nextY_Position;
    NSString *userImagePath;
    NSString *notifObjStr;
    NSString *currency;
    
    GridView *gridcollectionView;
    
    BOOL isRefreshedOnce;
    UILabel *recentViewLabel;
    UIButton *refreshButton;
    
    UIImageView *backgImageView;
    UIImageView *profileImageView;
    UIButton *userImageViewButton;
    UILabel *userNameLabel;
    BOOL isRecentListUpdated;
}
/**
 * declarations are used to set the UIConstraints Of ParentTemplate5.
 * Label,Value label,Border label,Image,TableView and Buttons.
 */
//UserDefaults data
@property (strong,nonatomic) NSMutableDictionary *userDeatils;
@property (strong,nonatomic) UIActionSheet *actionSheet;
@property (strong, nonatomic) UIView *parentSubView;
@property (strong, nonatomic) UIView *refreshSubView;
@property (strong, nonatomic) UILabel *refresh_updatedLabel;
@property (strong, nonatomic) UIButton *refresh_refreshButton;

@property (strong, nonatomic) UIView *balanceSubView;
@property (strong, nonatomic) UILabel *balance_userNameLabel;
@property (strong, nonatomic) UILabel *balance_seperatorLabel;
@property (retain, nonatomic) UIButton *balance_userImageViewButton;
@property (strong, nonatomic) UIImageView *balance_userImageView;

@property (strong, nonatomic) UILabel *balance_balanceLabel;
@property (strong, nonatomic) UILabel *balance_currencyLabel;
@property (strong, nonatomic) UILabel *balance_valueLabel;
@property (strong, nonatomic) UILabel *balance_updatedLabel;
@property (strong, nonatomic) UIButton *balance_refreshButton;
@property (strong, nonatomic) UILabel *balance_seperatorLabel2;

@property (strong, nonatomic) UIView *recentSubView;
@property (strong,nonatomic)  UILabel *recentSeperatorLabel3;
@property (strong, nonatomic) UITableView *recentTableView;
@property (strong, nonatomic) UILabel *alertLabel;
@property (strong, nonatomic) UILabel *recent_titleLabel;
@property (strong, nonatomic) UILabel *recenttitle_borderLabel;
@property (strong, nonatomic) UILabel *recent_valueLabel;
@property (strong,nonatomic) UILabel *recentValueLabel1;
@property (strong,nonatomic) UILabel *recentValueLabel2;
@property (strong,nonatomic) UILabel *recentValueLabel3;
@property (strong, nonatomic) UILabel *recent_currencyLabel;
@property (strong, nonatomic) UILabel *recent_descriptionLabel;
@property (strong, nonatomic) UILabel *recent_updatedLabel;
@property (strong, nonatomic) UIButton *recent_refreshButton;
@property (strong, nonatomic) UILabel *recent_seperatorLabel;

@property  (strong, nonatomic) NSMutableArray *recent_dataArray;
@property  (strong, nonatomic) NSMutableArray *recent_descriptionArray;

/**
 * This method is  used for Method Initialization of ParentTemplate5.
 */
// Method For initialization.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary;

@end
