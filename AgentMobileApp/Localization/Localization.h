//
//  Localization.h
//  Consumer Client
//
//  Created by android on 6/25/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Localization : NSObject

+(NSString*) languageSelectedStringForKey:(NSString*) key;

@end
