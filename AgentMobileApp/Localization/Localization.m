//
//  Localization.m
//  Consumer Client
//
//  Created by android on 6/25/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "Localization.h"
#import "Constants.h"

@implementation Localization

+(NSString*) languageSelectedStringForKey:(NSString*) key
{
    NSString *returnString = NSLocalizedStringFromTableInBundle(key,[self getLocalizationPropertyFile],[NSBundle mainBundle], nil);
    return returnString;
}

+(NSString *)getLocalizationPropertyFile
{
    NSString *fileString = [[NSUserDefaults standardUserDefaults] objectForKey:userLanguage];
    NSString *staticFileString = LOCALIZATION;
    NSString *localizationPropertyFileName = [NSString stringWithFormat:@"%@_%@",staticFileString,fileString.uppercaseString];
    return localizationPropertyFileName;
}


@end