//
//  AppDelegate.h
//  Consumer Client
//
//  Created by Integra Micro on 1/27/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XmlParserHandler.h"
#import "ActivityIndicator.h"
#import "DatabaseManager.h"
#import "Reachability.h"
#import "WebServiceRunning.h"
#import <UserNotifications/UserNotifications.h>

static NSString *deviceIdentifier = nil;
static BOOL notificationStatus;


/*
 * This method is used to set Default AppDelegate.
 */
// Application AppDelegate(Default).
@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate, XMLParserHandlerDelegate,WebServiceRunningDelegate>
{
    // Progress bar Symbol.
    ActivityIndicator *activityIndicator;
    // DataBase Manager.
    DatabaseManager *databaseManager;
    //  Internet Status
    Reachability *internetReach;
}
@property (strong, nonatomic) UIWindow *window;

/*
 * This method is used to get LaunchApiResponseData.
 */
-(void)getLaunchApiResponseData;
/*
 * This method is used to get DeviceIdentifier.
 */
+(NSString *)getDeviceIdentifier;
/*
 * This method is used to get NotificationStatus.
 */
+(BOOL)getNotificationStatus;
/*
 * This method is used to get ExternalErrorMessagePropertyFile.
 */
+(NSString *)getExternalErrorMessagePropertyFile;
/*
 * This method is used to get DropdownList.
 */
-(void)getDropdownList;
@end


