//
//  ConvertDetailsToSrverRequestFormate.m
//  Consumer Client
//
//  Created by android on 6/16/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "ConvertDetailsToSrverRequestFormate.h"

#import "WebServiceConstants.h"
#import "DatabaseConstatants.h"

@implementation ConvertDetailsToSrverRequestFormate

-(NSDictionary *)convertTheFetchFeeValues:(NSDictionary *)inputDictionary
{
    NSMutableDictionary *resultDictionary = [[NSMutableDictionary alloc] initWithDictionary:inputDictionary];
     NSLog(@"inputDictionary: %@",inputDictionary);
    if ([inputDictionary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER])
    {
        [resultDictionary setObject:[inputDictionary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER] forKey:PARAMETER9];
    }
    
    if ([inputDictionary objectForKey:TRANSACTION_DATA_TRANSACITON_AMOUNT])
    {
        if ([inputDictionary objectForKey:PARAMETER21]) {
            [resultDictionary setObject:[inputDictionary objectForKey:PARAMETER21] forKey:TRANSACTION_DATA_TRANSACITON_AMOUNT];
        }
        else
        [resultDictionary setObject:[inputDictionary objectForKey:TRANSACTION_DATA_TRANSACITON_AMOUNT] forKey:PARAMETER21];

    }
    if ([inputDictionary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME])
    {
        [resultDictionary setObject:[inputDictionary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_NICK_NAME] forKey:PARAMETER18];
    }
    if ([inputDictionary objectForKey:PARAMETER19])
    {
        [resultDictionary setObject:[inputDictionary objectForKey:PARAMETER19] forKey:PARAMETER19];
    }
    if ([inputDictionary objectForKey:PARAMETER6])
    {
        [resultDictionary setObject:[inputDictionary objectForKey:PARAMETER6] forKey:PARAMETER18];
    }
        
    if ([inputDictionary objectForKey:BILLERNICKNAME])
    {
        [resultDictionary setObject:[inputDictionary objectForKey:BILLERNICKNAME] forKey:PARAMETER9];
    }
    
    NSLog(@"resultDictionary: %@",resultDictionary);
   
    return resultDictionary;
}


-(NSDictionary *)convertTheP2PValues:(NSDictionary *)inputDictionary
{
    NSLog(@"inputDictionary: %@",inputDictionary);
    
    NSMutableDictionary *resultDictionary = [[NSMutableDictionary alloc] initWithDictionary:inputDictionary];
    
    if ([inputDictionary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER])
    {
        [resultDictionary setObject:[inputDictionary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER] forKey:PARAMETER9];
    }
    if ([inputDictionary objectForKey:TRANSACTION_DATA_TRANSACITON_AMOUNT])
    {
        [resultDictionary setObject:[inputDictionary objectForKey:TRANSACTION_DATA_TRANSACITON_AMOUNT] forKey:PARAMETER21];
    }
    if ([inputDictionary objectForKey:PARAMETER17])
    {
        [resultDictionary setObject:[inputDictionary objectForKey:PARAMETER17] forKey:PARAMETER17];
    }
    if ([inputDictionary objectForKey:PARAMETER19])
    {
        [resultDictionary setObject:[inputDictionary objectForKey:PARAMETER19] forKey:PARAMETER19];
    }
    
    NSLog(@"resultDictionary: %@",resultDictionary);
    return resultDictionary;
}

-(NSDictionary *)convertThePayBillValues:(NSDictionary *)inputDictionary
{
    NSLog(@"inputDictionary: %@",inputDictionary);
    
    NSMutableDictionary *resultDictionary = [[NSMutableDictionary alloc] initWithDictionary:inputDictionary];
        
    if ([[inputDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_FIXED] || [[inputDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID])
    {
        if ([[inputDictionary objectForKey:BILLPAYCODE] length]>0)
        {
            [resultDictionary setObject:[NSString stringWithFormat:@"%@",[inputDictionary objectForKey:BILLPAYCODE]] forKey:PARAMETER18];
        }
        else
        {
            if ([inputDictionary objectForKey:BILLERMASTERID])
                [resultDictionary setObject:[NSString stringWithFormat:@"%@",[inputDictionary objectForKey:BILLERMASTERID]] forKey:PARAMETER18];
        }
        
    }
    else if ([[inputDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_VARIABLE])
    {
        if ([inputDictionary objectForKey:BILLERMASTERID])
            [resultDictionary setObject:[NSString stringWithFormat:@"%@",[inputDictionary objectForKey:BILLERMASTERID]] forKey:PARAMETER18];
    }
    
    NSMutableString *tempString = [[NSMutableString alloc] init];
    if ([inputDictionary objectForKey:BILLERREFERENCE1])
    {
        [tempString appendString:[inputDictionary objectForKey:BILLERREFERENCE1]];
        NSLog(@"tempString: %@",tempString);
    }
    else
    {
        [tempString appendString:@""];
        NSLog(@"tempString: %@",tempString);
    }
    if ([inputDictionary objectForKey:BILLERREFERENCE2])
    {
        [tempString appendFormat:@"|%@",[inputDictionary objectForKey:BILLERREFERENCE2]];
        NSLog(@"tempString: %@",tempString);
    }
    else
    {
        [tempString appendString:@"|"];
        NSLog(@"tempString: %@",tempString);
    }
    if ([inputDictionary objectForKey:BILLERREFERENCE3])
    {
        [tempString appendFormat:@"|%@",[inputDictionary objectForKey:BILLERREFERENCE3]];
        NSLog(@"tempString: %@",tempString);
    }
    else
    {
        [tempString appendString:@"|"];
        NSLog(@"tempString: %@",tempString);
    }
    if ([inputDictionary objectForKey:BILLERREFERENCE4])
    {
        [tempString appendFormat:@"|%@",[inputDictionary objectForKey:BILLERREFERENCE4]];
        NSLog(@"tempString: %@",tempString);
    }
    else
    {
        [tempString appendString:@"|"];
        NSLog(@"tempString: %@",tempString);
    }
    if ([inputDictionary objectForKey:BILLERREFERENCE5])
    {
        [tempString appendFormat:@"|%@",[inputDictionary objectForKey:BILLERREFERENCE5]];
        NSLog(@"tempString: %@",tempString);
    }
    else
    {
        [tempString appendString:@"|"];
        NSLog(@"tempString: %@",tempString);
    }
    
    [resultDictionary setObject:tempString forKey:PARAMETER19];

    if ([inputDictionary objectForKey:BILLERNICKNAME])
    {
        [resultDictionary setObject:[inputDictionary objectForKey:BILLERNICKNAME] forKey:PARAMETER20];
    }
    
    
    NSLog(@"resultDictionary: %@",resultDictionary);
    
    return resultDictionary;
}

-(NSDictionary *)convertTheAddBillerValues:(NSDictionary *)inputDictionary
{
    NSLog(@"inputDictionary: %@",inputDictionary);
    
    NSMutableDictionary *resultDictionary = [[NSMutableDictionary alloc] initWithDictionary:inputDictionary];
    
//    if ([[inputDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_FIXED] || [[inputDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_HYBRID])
//    {
        if ([inputDictionary objectForKey:BILLPAYCODE])
        {
            [resultDictionary setObject:[NSString stringWithFormat:@"%@",[inputDictionary objectForKey:BILLPAYCODE]] forKey:PARAMETER18];
        }
//    }
//    else if ([[inputDictionary objectForKey:BILLERTYPE] isEqualToString:BILLER_TYPE_VARIABLE])
//    {
        if ([inputDictionary objectForKey:BILLERMASTERID])
            [resultDictionary setObject:[NSString stringWithFormat:@"%@",[inputDictionary objectForKey:BILLERMASTERID]] forKey:PARAMETER18];
//    }
    
//    if ([inputDictionary objectForKey:BILLERREFERENCE1])
//    {
//        [resultDictionary setObject:[inputDictionary objectForKey:BILLERREFERENCE1] forKey:PARAMETER19];
//    }
    
    NSMutableString *tempString = [[NSMutableString alloc] init];
    if ([inputDictionary objectForKey:BILLERREFERENCE1])
    {
        [tempString appendString:[inputDictionary objectForKey:BILLERREFERENCE1]];
        NSLog(@"tempString: %@",tempString);
    }
    else
    {
        [tempString appendString:@""];
        NSLog(@"tempString: %@",tempString);
    }
    if ([inputDictionary objectForKey:BILLERREFERENCE2])
    {
        [tempString appendFormat:@"|%@",[inputDictionary objectForKey:BILLERREFERENCE2]];
        NSLog(@"tempString: %@",tempString);
    }
    else
    {
        [tempString appendString:@"|"];
        NSLog(@"tempString: %@",tempString);
    }
    if ([inputDictionary objectForKey:BILLERREFERENCE3])
    {
        [tempString appendFormat:@"|%@",[inputDictionary objectForKey:BILLERREFERENCE3]];
        NSLog(@"tempString: %@",tempString);
    }
    else
    {
        [tempString appendString:@"|"];
        NSLog(@"tempString: %@",tempString);
    }
    if ([inputDictionary objectForKey:BILLERREFERENCE4])
    {
        [tempString appendFormat:@"|%@",[inputDictionary objectForKey:BILLERREFERENCE4]];
        NSLog(@"tempString: %@",tempString);
    }
    else
    {
        [tempString appendString:@"|"];
        NSLog(@"tempString: %@",tempString);
    }
    if ([inputDictionary objectForKey:BILLERREFERENCE5])
    {
        [tempString appendFormat:@"|%@",[inputDictionary objectForKey:BILLERREFERENCE5]];
        NSLog(@"tempString: %@",tempString);
    }
    else
    {
        [tempString appendString:@"|"];
        NSLog(@"tempString: %@",tempString);
    }
    
    [resultDictionary setObject:tempString forKey:PARAMETER19];

    
    NSLog(@"resultDictionary: %@",resultDictionary);
    
    return resultDictionary;
}

-(NSDictionary *)convertTheCASHINValues:(NSDictionary *)inputDictionary{
    NSMutableDictionary *resultDictionary = [[NSMutableDictionary alloc] initWithDictionary:inputDictionary];
    
    if ([inputDictionary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER])
    {
        [resultDictionary setObject:[inputDictionary objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER] forKey:PARAMETER6];
    }
    if ([inputDictionary objectForKey:TRANSACTION_DATA_TRANSACITON_AMOUNT])
    {
        if ([inputDictionary objectForKey:PARAMETER21]) {
            [resultDictionary setObject:[inputDictionary objectForKey:PARAMETER21] forKey:TRANSACTION_DATA_TRANSACITON_AMOUNT];
        }
        else
            [resultDictionary setObject:[inputDictionary objectForKey:TRANSACTION_DATA_TRANSACITON_AMOUNT] forKey:PARAMETER21];
        
    }
    if ([inputDictionary objectForKey:PARAMETER7])
    {
        [resultDictionary setObject:[inputDictionary objectForKey:PARAMETER7] forKey:PARAMETER7];
    }
    if ([inputDictionary objectForKey:PARAMETER19])
    {
        [resultDictionary setObject:[inputDictionary objectForKey:PARAMETER19] forKey:PARAMETER19];
    }
    NSLog(@"resultDictionary: %@",resultDictionary);
    
    return resultDictionary;
    
}

@end
