//
//  GenerateOTP.h
//  Consumer Client
//
//  Created by android on 12/14/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GenerateOTP : NSObject

/**
 * This method is used to generate OTP.
 @Param Type - (MSISDN,MPIN,Amount,MerchantCode<serverTime and KeyLength)
 */
+ (NSString *)getValue:(NSString *)MSISDN
          withNSString:(NSString *)MPIN
          withNSString:(NSString *)Amount
          withNSString:(NSString *)MerchantCode
              withLong:(long)ServerTime
               withInt:(int)KeyLength;
/**
 * This method is used to Get current System time.
 */
+(NSString *)getTime;
/**
 * This method is used to Get current System time in String.
 */
+(NSString*)toStringFromDateTime:(NSDate*)datetime;

@end
