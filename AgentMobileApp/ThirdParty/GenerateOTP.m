//
//  GenerateOTP.m
//  Consumer Client
//
//  Created by android on 12/14/15.
//  Copyright (c) 2015 Soumya. All rights reserved.

#import "GenerateOTP.h"
#import "ValidationsClass.h"
#import "Constants.h"

@implementation GenerateOTP

#pragma mark - Generate OTP
/**
 * This method is used to generate OTP.
 @Param Type - (MSISDN,MPIN,Amount,MerchantCode<serverTime and KeyLength)
 */
+ (NSString *)getValue:(NSString *)MSISDN
          withNSString:(NSString *)MPIN
          withNSString:(NSString *)Amount
          withNSString:(NSString *)MerchantCode
              withLong:(long)ServerTime
               withInt:(int)KeyLength {
    
    NSDate *lastServerResponseTime = [[NSUserDefaults standardUserDefaults] objectForKey:SERVER_LAST_TRANSACTION_TIME];
    NSDate *lastDeviceResponseTime = [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_LAST_TRANSACTION_TIME];
    
    NSDate *currentDate = [NSDate date];
    NSTimeInterval diff = [currentDate timeIntervalSinceDate:lastDeviceResponseTime];
    NSDate *finalDate = [lastServerResponseTime dateByAddingTimeInterval:diff];
    NSTimeInterval time = [finalDate timeIntervalSince1970];
    NSTimeInterval lTimeInMins = time/60;
    
    NSString * generatedKey;
    NSString * sTemp;
    
    NSData *s11 = [MSISDN dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str1;
    if ([s11 respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        str1 = [s11 base64EncodedStringWithOptions:kNilOptions];
    } else {
        str1 = [s11 base64Encoding];
    }
    
    int h1 = 0;
    h1=[self getHashValue:str1];
    int s12 =abs(h1);
    
    NSData *s21 = [MPIN dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str2;
    if ([s21 respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        str2 = [s21 base64EncodedStringWithOptions:kNilOptions];
    } else {
        str2 = [s21 base64Encoding];
    }
    int h2=0;
    h2=[self getHashValue:str2];
    int s22 =abs(h2);
    
    NSData *s31 = [Amount dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str3;
    if ([s31 respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        str3 = [s31 base64EncodedStringWithOptions:kNilOptions];
    } else {
        str3 = [s31 base64Encoding];
    }
    int h3=0;
    h3=[self getHashValue:str3];
    int s32 = abs(h3);
    
    NSData *s41 = [MerchantCode dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str4;
    if ([s41 respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        str4 = [s41 base64EncodedStringWithOptions:kNilOptions];
    } else {
        str4 = [s41 base64Encoding];
    }
    int h4=0;
    h4=[self getHashValue:str4];
    int s42 = abs(h4);
    
    NSString* sTimeInMinsStr = [NSString stringWithFormat:@"%.00f", lTimeInMins];
    
    NSData *s51 = [sTimeInMinsStr dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *str5;
    if ([s51 respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        str5 = [s51 base64EncodedStringWithOptions:kNilOptions];
    } else {
        
        str5 = [s51 base64Encoding];
    }
    int h5=0;
    h5=[self getHashValue:str5];
    int s52 = abs(h5);
    
    NSString *s91 = [NSString stringWithFormat: @"%i%i%i%i%i", s12, s22, s32, s42, s52];
    NSData *s92 = [s91 dataUsingEncoding:NSUTF8StringEncoding];
    NSString *basekeyStr;
    if ([s91 respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        basekeyStr = [s92 base64EncodedStringWithOptions:kNilOptions];
    } else {
        basekeyStr = [s92 base64Encoding];
    }
    
    int h6=0;
    h6=[self getHashValue:basekeyStr];
    int s93 = abs(h6);
    
    NSString* myNewString = [NSString stringWithFormat:@"%i", s93];
    int len=(int)[myNewString length];
    
    if(len< KeyLength)
    {
        sTemp=nil;
        int iRequiredZeros = KeyLength - len;
        for(int iCount = 0; iCount < iRequiredZeros; iCount++){
            sTemp = [NSString stringWithFormat:@"%@%d",sTemp,0];
        }
        myNewString =[NSString stringWithFormat:@"%@%@",sTemp,myNewString];
    }
    
    int len1=(int)[myNewString length];
    int len2=len1-KeyLength;
    generatedKey=[myNewString substringFromIndex:(len2)];
    return generatedKey;
}
/**
 * This method is used to Get Hash value.
 */
+(int)getHashValue:(NSString *)strValue;
{
    int h=0;
    for (int i=0; i< [strValue length] ; i++) {
        unichar charAsciiValue =[strValue characterAtIndex:i];
        h = 31*h + (charAsciiValue++);
    }
    return h;
}

/**
 * This method is used to Get current System time.
 */
+(NSString *)getTime
{
    NSString *myString;
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:SERVER_DATE_FORMAT];
    myString = [dateFormatter stringFromDate:now];
    return myString;
}

+(NSString *)getTime : (NSDate  *)date
{
    NSString *myString;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:SERVER_DATE_FORMAT];
    myString = [dateFormatter stringFromDate:date];
    return myString;
}
/**
 * This method is used to Get current System time in String.
 */
+ (NSString*)toStringFromDateTime:(NSDate*)datetime
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    NSString *timeZone = NSLocalizedStringFromTableInBundle(@"applicaiton_time_zone",@"GeneralSettings",[NSBundle mainBundle], nil);
    [dateFormatter  setTimeZone:[NSTimeZone timeZoneWithAbbreviation:timeZone]];
    [dateFormatter setDateFormat:SERVER_DATE_FORMAT];
    NSString* dateTime = [dateFormatter stringFromDate:datetime];
    return dateTime;
}

+ (NSDate*)toDateFromString:(NSString *)datetime
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:SERVER_DATE_FORMAT];
    NSString *timeZone = NSLocalizedStringFromTableInBundle(@"applicaiton_time_zone",@"GeneralSettings",[NSBundle mainBundle], nil);
    [dateFormatter  setTimeZone:[NSTimeZone timeZoneWithAbbreviation:timeZone]];
    NSDate* dateTime = [dateFormatter dateFromString:datetime];
    return dateTime;
}

+ (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

@end
